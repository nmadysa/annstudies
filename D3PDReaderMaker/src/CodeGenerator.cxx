// System include(s):
#include <ctime>
#include <sys/stat.h>
#include <iostream>

// STL include(s):
#include <fstream>
#include <algorithm>

// Local include(s):
#include "CodeGenerator.h"
#include "CodeGenerator_constants.h"

/// A little header for all the generated source files
static const char* CODE_COMMENT =
  "// -------------------------------------------------------------\n"
  "//             Code produced by D3PDReaderMaker\n"
  "//\n"
  "//  author: Christian Gumpert <cgumpert@cern.ch>\n"
  "// -------------------------------------------------------------";

namespace D3PDReaderMaker
{

  /**
   * This function is used to "extract" the template argument of an std::vector
   * type declaration. So for instance it makes from "std::vector<int>" the
   * string "int". It tries to be as smart as possible, so you can give it
   * types with fairly complicated template parameters.
   *
   * @param type The original vector type name
   * @param ok A flag that is set to @c false if the extraction
   *           wasn't successful
   * @returns the template parameter of the vector type
   */
  std::string vectorTemplateArgument(const std::string& type,bool& ok)
  {
    // Check that the type declaration begins with "std::vector<" or "vector<":
    ok = true;
    std::string result = type;
    if(result.find("std::vector<") == 0)
      result.replace(0,12,"");
    else
    {
      if(result.find("vector<") == 0)
	result.replace(0,7,"");
      else
      {
	ok = false;
	return "";
      }
    }

    // Now go through the characters one-by-one, and try to find the closing
    // bracket of the template argument. Remember that the template argument can
    // itself be a templated type (vector of a vector for instance), so the code
    // has to remember how many brackets were opened and closed.
    int brackets = 0;
    for(size_t i = 0; i < result.size(); ++i)
    {
      switch(result[i])
      {
      case '<':
	++brackets;
	break;
      case '>':
	if(!brackets)
	{
	  if(result[i - 1] == ' ')
	    return result.substr( 0, i - 1 );
	  else
	    return result.substr( 0, i );
	}
	--brackets;
	break;
      default:
	break;
      }
    }

    ok = false;
    return "";
  }

  /**
   * This function is used to add, as needed, STL header includes to the generated
   * files.
   *
   * @param out Stream into which the code is written
   * @param name Name of the STL type (and header file)
   * @param vars The variables that will be added to the generated class
   */
  void addSTLHeader(std::ostream& out,const char* name,const std::vector<D3PDVariable>& vars)
  {
    // Check if the STL name appears in the variable type names:
    bool header_needed = false;
    std::vector<D3PDVariable>::const_iterator itr = vars.begin();
    std::vector<D3PDVariable>::const_iterator end = vars.end();
    for(; itr != end; ++itr)
    {
      if(itr->type.find(name) != std::string::npos)
      {
	header_needed = true;
	break;
      }
    }

    // If it does, let's include it:
    if(header_needed)
    {
      out << "#include <" << name << ">" << std::endl;
      // This statement is here to make sure that ROOT's type names such as
      // "vector<float>" will be compilable as well:
      out << "using std::" << name << ";" << std::endl;
    }

    return;
  }

  /**
   * This function is used in the code generator to determine from a type name whether
   * it's a primitive type or not. Some parts of the generated code have to be
   * written differently for primitive and complex types.
   *
   * @param type The type name that we want to evaluate
   * @returns @c true if the type name is a known primitive type,
   *          @c false otherwise
   */
  bool isPrimitive(const std::string& type)
  {
    // Create an internal list of primitive type names. This
    // only has to be done once.
    static std::vector< std::string > primitives;
    if(!primitives.size())
    {
      // Normal C++ type names:
      primitives.push_back("bool");
      primitives.push_back("char");
      primitives.push_back("unsigned char");
      primitives.push_back("short");
      primitives.push_back("unsigned short");
      primitives.push_back("int");
      primitives.push_back("unsigned int");
      primitives.push_back("long");
      primitives.push_back("unsigned long");
      primitives.push_back("long long");
      primitives.push_back("unsigned long long");
      primitives.push_back("float");
      primitives.push_back("double");
      // ROOT type names:
      primitives.push_back("Bool_t");
      primitives.push_back("Char_t");
      primitives.push_back("UChar_t");
      primitives.push_back("Short_t");
      primitives.push_back("UShort_t");
      primitives.push_back("Int_t");
      primitives.push_back("UInt_t");
      primitives.push_back("Long_t");
      primitives.push_back("ULong_t");
      primitives.push_back("Long64_t");
      primitives.push_back("ULong64_t");
      primitives.push_back("Float_t");
      primitives.push_back("Double_t");
    }

    return (std::find(primitives.begin(),primitives.end(),type) != primitives.end());
  }

  /**
   *
   * This function can be used to create the D3PDReader::VarHandle class's
   * source files. The created class is used throughout the D3PDReader code
   * to access the branches of the underlying TTree.
   *
   * @param dir Directory name where the source files should be put
   * @return @c true if the creation was successfull, otherwise @c false
   */
  bool writeVarHandle(const std::string& dir)
  {
    // Construct the file names as they will be needed in a few places:
    const std::string headerName = dir + "/" + VARHANDLE_HEADER_NAME;
    const std::string implName   = dir + "/" + VARHANDLE_IMPL_NAME;
    const std::string defName   = dir + "/" + VARHANDLE_DEF_NAME;

    // Only create the header if it doesn't exist:
    struct stat fileInfo;
    if(stat(headerName.c_str(),&fileInfo))
    {
      // Let everyone know what we're doing:
      std::cout << "Generating file: " << headerName << std::endl;
      // Open the header file (overwriting a possibly existing file):
      std::fstream header( headerName.c_str(),std::fstream::out | std::fstream::trunc );
      // Write the header file:
      header << VARHANDLE_HEADER << std::endl;
      header.close();
    }

    // Only create the implementation if it doesn't exist:
    if(stat(implName.c_str(),&fileInfo))
    {
      // Let everyone know what we're doing:
      std::cout  << "Generating file: " << implName << std::endl;
      // Open the header file (overwriting a possibly existing file):
      std::fstream impl( implName.c_str(),std::fstream::out | std::fstream::trunc );
      // Write the header file:
      impl << VARHANDLE_IMPL << std::endl;
      impl.close();
    }

    // Only create the definition if it doesn't exist:
    if(stat(defName.c_str(),&fileInfo))
    {
      // Let everyone know what we're doing:
      std::cout  << "Generating file: " << defName << std::endl;
      // Open the header file (overwriting a possibly existing file):
      std::fstream def( defName.c_str(),std::fstream::out | std::fstream::trunc );
      // Write the header file:
      def << VARHANDLE_DEF << std::endl;
      def.close();
    }

    return true;
  }

  /**
   * This function is used to create the header of the class describing a set of
   * D3PD variables. This might be an one-per-event type (e.g. event information)
   * or a collection class (e.g. holding all muon branches).
   *
   * @param classname The name of the class to be generated
   * @param dir The directory where the header file should be put
   * @param prefix The common prefix of the variable names
   * @param vars A vector of the variable descriptions
   *
   * @returns @c true if header file was generated successfully, otherwise @c false
   */
  bool WriteD3PDObjectHeader(const std::string& classname,const std::string& dir,
			     const std::string& prefix,const std::vector<D3PDVariable>& vars)
  {
    // Construct the file name:
    const std::string fileName = dir + "/" + classname + ".h";

    // If the file already exists, let's not overwrite it:
    struct stat fileInfo;
    if(!stat(fileName.c_str(),&fileInfo))
    {
      std::cout << "file " << fileName << " already exists" << std::endl;
      return false;
    }

    // Let everyone know what we're doing:
    std::cout << "Generating file: " << fileName << std::endl;

    // Open the header file
    std::ofstream header(fileName.c_str());

    // Write a header for the file:
    header << CODE_COMMENT << std::endl;
    header << "#ifndef D3PDREADER_" << classname << "_H" << std::endl;
    header << "#define D3PDREADER_" << classname << "_H" << std::endl << std::endl;

    // Include some STL headers if they're needed:
    header << "// STL include(s)" << std::endl;
    header << "#include <string>" << std::endl;
    header << "using std::string;" << std::endl;
    addSTLHeader(header,"vector",vars);
    addSTLHeader(header,"map",vars);
    addSTLHeader(header,"list",vars);
    header << std::endl;

    // ROOT include(s):
    header << "// ROOT include(s)" << std::endl;
    header << "#include <TObject.h>" << std::endl;
    header << "#include <TString.h>" << std::endl << std::endl;

    // Local include(s):
    header << "// custom include(s)" << std::endl;
    header << "#include \"VarHandle.h\"" << std::endl;
    header << std::endl;

    // Forward declaration(s):
    header << "class TTree;" << std::endl << std::endl;

    // Declare the namespace:
    header << "namespace D3PDReader\n{" << std::endl;

    // Declare the reader class
    header << "  /**" << std::endl;
    header << "   * Code generated by CodeGenerator on:" << std::endl;
    time_t rawtime = time(NULL);
    header << "   * time = " << ctime(&rawtime);
    header << "   */" << std::endl;
    header << "  class " << classname << " : public TObject" << std::endl;
    header << "  {" << std::endl;
    header << "  public:" << std::endl;

    // Declare the constructor:
    header << "    /// Constructor used when reading from a TTree" << std::endl;
    header << "    " << classname << "(const long int& master,const std::string& prefix = \"" << prefix << "\");" << std::endl;
    header << "    /// Constructor when the object is only used for writing data out" << std::endl;
    header << "    " << classname << "(const std::string& prefix = \"" << prefix << "\");" << std::endl;
    header << "    /// standard destructor" << std::endl;
    header << "    ~" << classname << "() {}" << std::endl;

    // Declare some functions:
    header << "    /// Get the currently configured prefix value" << std::endl;
    header << "    const std::string& GetPrefix() const {return m_sPrefix;}" << std::endl;
    header << "    /// Connect the object to an input TTree" << std::endl;
    header << "    void ReadFrom(TTree* tree);" << std::endl;
    header << "    /// Connect the object to an output TTree" << std::endl;
    header << "    void WriteTo(TTree* tree);" << std::endl;
    header << "    /// Turn all (available) branches either on or off" << std::endl;
    header << "    void SetActive(bool active = true);" << std::endl;
    header << "    /// Read in all the variables that we need to write out as well" << std::endl;
    header << "    void ReadAllActive();" << std::endl;
    header << "    /// Read in all the variables that we need to write out as well" << std::endl;
    header << "    void RemoveIndex(unsigned int index);" << std::endl;
    header << std::endl;

    // Declare each variable responsible for reading a branch:
    std::vector<D3PDVariable>::const_iterator itr = vars.begin();
    std::vector<D3PDVariable>::const_iterator end = vars.end();
    for(; itr != end; ++itr)
    {
      if(itr->doc != "")
	header << "    /// " << itr->doc << std::endl;
      header << "    VarHandle<" << itr->type << " > " << itr->varname << ";"
	     << std::endl;
    }

    // Declare the additional member variable(s):
    header << std::endl << "  private:" << std::endl;
    header << "    const std::string m_sPrefix; ///< common prefix to the branch names" << std::endl
	   << std::endl;

    // Close the class definition:
    header << "    ClassDef(" << classname <<",0)" << std::endl;
    header << "  }; // class " << classname << std::endl << std::endl;
    header << "} // namespace D3PDReader" << std::endl << std::endl;
    header << "#endif // D3PDREADER_" << classname << "_H" << std::endl;

    header.close();

    return true;
  }

  /**
   * This function is used to generate the source file of a D3PDReader class.
   *
   * @param classname The name of the class to be generated
   * @param dir The directory where the src file should be put
   * @param vars A vector of the variable descriptions
   *
   * @returns @c true if header file was generated successfully, otherwise @c false
   */
  bool WriteD3PDObjectSource(const std::string& classname,const std::string& dir,
			     const std::vector<D3PDVariable>& vars)
  {
    // Construct the file name:
    const std::string fileName = dir + "/" + classname + ".cxx";

    // If the file already exists, let's not overwrite it:
    struct stat fileInfo;
    if(!stat(fileName.c_str(),&fileInfo))
    {
      std::cout << "file " << fileName << " already exists" << std::endl;
      return false;
    }

    // Let everyone know what we're doing:
    std::cout << "Generating file: " << fileName << std::endl;

    // Open the header file
    std::ofstream source( fileName.c_str() );

    // Add the common header to the file:
    source << CODE_COMMENT << std::endl << std::endl;
    source << "#ifndef D3PDREADER_" << classname << "_CXX" << std::endl;
    source << "#define D3PDREADER_" << classname << "_CXX" << std::endl << std::endl;

    // Include the class's header:
    source << "// ROOT include(s)" << std::endl;
    source << "#include \"TTree.h\"" << std::endl << std::endl;
    source << "// custom header(s)" << std::endl;
    source << "#include \"" << classname << ".h\"" << std::endl << std::endl;

    source << "ClassImp(D3PDReader::" << classname << ")" << std::endl << std::endl;

    source << "namespace D3PDReader" << std::endl;
    source << "{" << std::endl;

    // Produce the (input) constructor:
    source << "  /**" << std::endl;
    source << "   * This constructor should be used when the object will be used to read"
	   << std::endl;
    source << "   * variables from an existing ntuple. The object will also be able to"
	   << std::endl;
    source << "   * output variables, but it will also need to read them from somewhere."
	   << std::endl;
    source << "   *" << std::endl;
    source << "   * @param master Reference to the variable holding the current "
	   << "event number" << std::endl;
    source << "   * @param prefix Prefix of the variables in the D3PD" << std::endl;
    source << "   */" << std::endl;
    source << "  " << classname << "::" << classname
	   << "(const long int& master,const std::string& prefix):" << std::endl;
    source << "    TObject(),"
	   << std::endl;
    std::vector<D3PDVariable>::const_iterator itr = vars.begin();
    std::vector<D3PDVariable>::const_iterator end = vars.end();
    for(;itr != end;++itr)
    {
      source << "    " << itr->varname << "(prefix + "
	     << "\"" << itr->name << "\",&master)," << std::endl;
    }
    source << "    m_sPrefix(prefix)" << std::endl;
    source << "  {}" << std::endl << std::endl;

    // Produce the (output) constructor:
    source << "  /**" << std::endl;
    source << "   * This constructor can be used when the object will only have to output"
	   << std::endl;
    source << "   * (and temporarily store) new information into an output ntuple. For"
	   << std::endl;
    source << "   * instance when one wants to create a selected/modified list of information."
	   << std::endl;
    source << "   *" << std::endl;
    source << "   * @param prefix Prefix of the variables in the D3PD" << std::endl;
    source << "   */" << std::endl;
    source << "  " << classname << "::" << classname
	   << "(const std::string& prefix):" << std::endl;
    source << "    TObject(),"
	   << std::endl;
    itr = vars.begin();
    end = vars.end();
    for( ; itr != end; ++itr ) {
      source << "    " << itr->varname << "(prefix + "
	     << "\"" << itr->name << "\",0)," << std::endl;
    }
    source << "    m_sPrefix(prefix)" << std::endl;
    source << "  {}" << std::endl << std::endl;

    // Produce the ReadFrom(...) function:
    source << "  /**" << std::endl;
    source << "   * This function should be called every time a new TFile is opened"
	   << std::endl;
    source << "   * by your analysis code." << std::endl;
    source << "   *" << std::endl;
    source << "   * @param tree Pointer to the TTree with the variables" << std::endl;
    source << "   */" << std::endl;
    source << "  void " << classname << "::ReadFrom(TTree* tree)" << std::endl;
    source << "  {" << std::endl;
    itr = vars.begin();
    end = vars.end();
    for(; itr != end; ++itr)
      source << "    " << itr->varname << ".ReadFrom(tree);" << std::endl;
    
    source << "  }" << std::endl << std::endl;

    // Produce the WriteTo(...) function:
    source << "  /**" << std::endl;
    source << "   * This function can be called to connect the active variables of the object"
	   << std::endl;
    source << "   * to an output TTree. It can be called multiple times, then the variables"
	   << std::endl;
    source << "   * will be written to multiple TTrees." << std::endl;
    source << "   *" << std::endl;
    source << "   * @param tree Pointer to the TTree where the variables should be written"
	   << std::endl;
    source << "   */" << std::endl;
    source << "  void " << classname << "::WriteTo(TTree* tree)" << std::endl;
    source << "  {" << std::endl;
    itr = vars.begin();
    end = vars.end();
    for(; itr != end; ++itr)
      source << "    " << itr->varname << ".WriteTo(tree);" << std::endl;
    
    source << "  }" << std::endl << std::endl;

    // Produce the SetActive(...) function:
    source << "  /**" << std::endl;
    source << "   * This is a convenience function for turning all the branches active or"
	   << std::endl;
    source << "   * inactive at the same time. If the parameter is set to @c true"
	   << std::endl;
    source << "   * then all the branches available from the input are turned active."
	   << std::endl;
    source << "   * When it's set to @c true then all the variables are turned"
	   << std::endl;
    source << "   * inactive." << std::endl;
    source << "   *" << std::endl;
    source << "   * @param active Flag behaving as explained above" << std::endl;
    source << "   */" << std::endl;
    source << "  void " << classname << "::SetActive(bool active)" << std::endl;
    source << "  {" << std::endl;
    source << "    if(active)" << std::endl;
    source << "    {" << std::endl;
    itr = vars.begin();
    end = vars.end();
    for(;itr != end; ++itr)
      source << "     if(" << itr->varname << ".IsAvailable()) "
	     << itr->varname << ".SetActive(active);" << std::endl;
    
    source << "    }" << std::endl;
    source << "    else" << std::endl;
    source << "    {" << std::endl;
    itr = vars.begin();
    end = vars.end();
    for(; itr != end; ++itr)
      source << "      " << itr->varname << ".SetActive(active);" << std::endl;
    
    source << "    }" << std::endl << std::endl;
    source << "  }" << std::endl << std::endl;

    // Produce the ReadAllActive(...) function:
    source << "  /**" << std::endl;
    source << "   * This function can be used to read in all the branches from the input" << std::endl;
    source << "   * TTree which are set active for writing out. This can simplify writing" << std::endl;
    source << "   * event selector codes immensely. Remember to set the desired variable" << std::endl;
    source << "   * active before calling this function." << std::endl;
    source << "   */" << std::endl;
    source << "  void " << classname << "::ReadAllActive()" << std::endl;
    source << "  {" << std::endl;
    itr = vars.begin();
    end = vars.end();
    for(; itr != end; ++itr)
      source << "    if(" << itr->varname << ".IsActive()) "
	     << itr->varname << "();" << std::endl;
    
    source << "  }" << std::endl << std::endl;

    // Produce the RemoveIndex(...) function:
    source << "  /**" << std::endl;
    source << "   * This function can be used to remove the element with the given index" << std::endl;
    source << "   * from all available branches of type std::vector<...>*." << std::endl;
    source << "   */" << std::endl;
    source << "  void " << classname << "::RemoveIndex(unsigned int index)" << std::endl;
    source << "  {" << std::endl;
    itr = vars.begin();
    end = vars.end();
    for(; itr != end; ++itr)
    {
      if(itr->varname == "n")
      {
        source << "    assert((unsigned int)" << itr->varname << "() > index);" << std::endl;
        source << "    --" << itr->varname << "();" << std::endl;
	continue;
      }
      
      if((itr->type.find("vector") == std::string::npos) || (itr->type.find("*") == std::string::npos))
	continue;
      
      source << "    if(" << itr->varname << ".IsAvailable()) " << std::endl;
      source << "    {" << std::endl;
      source << "      " << itr->varname << "();" << std::endl;
      source << "      " << itr->varname << "()->erase(" << itr->varname << "()->begin() + index);" << std::endl;
      source << "    }" << std::endl;
      source << std::endl;
    }
    
    source << "  }" << std::endl << std::endl;

    source << "} // namespace D3PDReader" << std::endl;
    source << "#endif // D3PDREADER_" << classname << "_CXX" << std::endl;

    source.close();

    return true;
  }

  /// write the header file for class representing one element of a D3PDCollection
  bool WriteD3PDElementHeader(const std::string& classname,const std::string& dir,
			      const std::string& collectionclass,
			      const std::vector<D3PDVariable>& vars,
			      bool bIsParticle)
  {
    // Construct the file name:
    const std::string fileName = dir + "/" + classname + ".h";

    // If the file already exists, let's not overwrite it:
    struct stat fileInfo;
    if(!stat(fileName.c_str(),&fileInfo))
    {
      std::cout << "file " << fileName << " already exists" << std::endl;
      return false;
    }

    // Let everyone know what we're doing:
    std::cout << "Generating file: " << fileName << std::endl;

    // Open the header file
    std::ofstream header(fileName.c_str());

    // Write a header for the file:
    header << CODE_COMMENT << std::endl;
    header << "#ifndef D3PDREADER_" << classname << "_H" << std::endl;
    header << "#define D3PDREADER_" << classname << "_H" << std::endl << std::endl;

    // include particle base class
    if(bIsParticle)
    {
      header << "//include generic particle class" << std::endl;
      header << "#include \"Particle.h\"" << std::endl;
    }
    
    // include collection class
    header << "// custom include(s)" << std::endl;
    header << "#include \"" << collectionclass << ".h\"\n" << std::endl;
    // Declare the namespace:
    header << "namespace D3PDReader\n{" << std::endl;

    // Declare the element class
    header << "  /**" << std::endl;
    header << "   * Code generated by CodeGenerator on:" << std::endl;
    time_t rawtime = time(NULL);
    header << "   * time = " << ctime(&rawtime);
    header << "   */" << std::endl;
    header << "  class " << classname;
    if(bIsParticle)
      header << " : public Particle,";
    else
      header << " :";
    header << " public TObject" << std::endl;
    header << "  {" << std::endl;
    header << "  public:" << std::endl;

    // declare constructors
    header << "    " << classname << "(unsigned int index," << collectionclass << "& parent):" << std::endl;
    if(bIsParticle) header << "      Particle()," << std::endl;
    header << "      TObject()," << std::endl;
    header << "      m_iIndex(index)," << std::endl;
    header << "      m_pParent(&parent)" << std::endl;
    if(bIsParticle)
    {
      header << "    {" << std::endl;
      header << "       //initialise TLV properly" << std::endl;
      header << "       TLV().SetPtEtaPhiM(0,0,0,1);" << std::endl;
      header << "       //initialise charge properly" << std::endl;
      header << "       m_iCharge = 0;" << std::endl;
      header << "    }" << std::endl; 
    }
    else
      header << "    {}" << std::endl << std::endl;

    header << "    " << classname << "(const " << classname << "& copy):" << std::endl;
    if(bIsParticle) header << "      Particle(copy)," << std::endl;
    header << "      TObject(copy)," << std::endl;
    header << "      m_iIndex(copy.m_iIndex)," << std::endl;
    header << "      m_pParent(copy.m_pParent)" << std::endl;
    header << "    {}" << std::endl << std::endl;

    // declare assignement operator
    header << "    " << classname << "& operator=(const " << classname << "& rhs)" << std::endl;
    header << "    {" << std::endl;
    header << "      m_iIndex = rhs.m_iIndex;" << std::endl;
    header << "      m_pParent = rhs.m_pParent;" << std::endl;
    if(bIsParticle)
    {
      header << std::endl << "      Particle::operator=(rhs);" << std::endl << std::endl;
    }      
    header << "      return *this;" << std::endl;
    header << "    }" << std::endl << std::endl;

    // declare comparison operator based on index
    header << "    bool operator==(unsigned int& rhs) {return (m_iIndex == rhs);}" << std::endl;
    header << "    bool operator!=(unsigned int& rhs) {return !operator==(rhs);}" << std::endl << std::endl;
    // declare GetIndex method
    header << "    unsigned int GetIndex() const {return m_iIndex;}" << std::endl;
    
    std::vector<D3PDVariable>::const_iterator it = vars.begin();
    bool bCheckedType = false;
    std::string templateArg;
    for(; it != vars.end(); ++it)
    {
      // Ignore the container size variable:
      if((it->varname == "n") || (it->varname == "N"))
	continue;   
      bCheckedType = false;
      templateArg = vectorTemplateArgument(it->type,bCheckedType);
      // var type was a vector
      if(bCheckedType)
      {
	header << "    " << templateArg << "& " << it->varname << "() {return m_pParent->" << it->varname <<"()->at(m_iIndex);}" << std::endl;
	header << "    const " << templateArg << "& " << it->varname << "() const {return m_pParent->" << it->varname <<"()->at(m_iIndex);}" << std::endl;
      }
      // var type was not a vector -> use raw type
      else
      {
	templateArg = it->type;
	header << "    " << templateArg << "& " << it->varname << "() {return m_pParent->" << it->varname <<"();}" << std::endl;
	header << "    const " << templateArg << "& " << it->varname << "() const {return m_pParent->" << it->varname <<"();}" << std::endl;\
      }
    }
    
    // add private member variables
    header << "\n  private:" << std::endl;
    header << "    unsigned int m_iIndex;" << std::endl;
    header << "    " << collectionclass << "* m_pParent;" << std::endl << std::endl;
    
    // Close the class definition:
    header << "    ClassDef(" << classname <<",0)" << std::endl;
    header << "  }; // class " << classname << std::endl << std::endl;
    header << "} // namespace D3PDReader" << std::endl << std::endl;
    header << "#endif // D3PDREADER_" << classname << "_H" << std::endl;

    header.close();

    return true;
  }  
} // namespace D3PDReaderMake
