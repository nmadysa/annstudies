// STL include(s):
#include <iostream>
#include <vector>
#include <string>

// Boost include(s):
#include <boost/program_options.hpp>

// ROOT include(s)
#include "TFile.h"
#include "TTree.h"

// Local include(s):
#include "D3PDVariable.h"
#include "D3PDVariableExtractor.h"
#include "CodeGenerator.h"

/// The program description that's printed with the available parameters
static const char* PROGRAM_DESC =
  "This application can be used to generate D3PDReader classes\n"
  "based on an existing D3PD file and some additional command\n"
  "line options";

/// A greeting text that's printed when the application starts up
static const char* PROGRAM_GREETING =
  "*******************************************************************\n"
  "*                                                                 *\n"
  "*               D3PDReader standalone code generator              *\n"
  "*                                                                 *\n"
  "*******************************************************************";

/// Formatted printing for vector objects
/**
 * The code has to print a vector in at least one place. To make it easily
 * readable in the code, I like to use such an output operator. It can be
 * used for any kind of vector as long as the template type can also be
 * printed with the << operator.
 *
 * @param out An STL output stream
 * @param vec The vector that should be printed
 * @returns The same output stream that the operator received
 */
template< typename T >
std::ostream& operator<<( std::ostream& out, const std::vector< T >& vec ) {
  out << "[";
  typename std::vector< T >::const_iterator itr = vec.begin();
  typename std::vector< T >::const_iterator end = vec.end();
  for( ; itr != end; ++itr ) {
    out << *itr;
    if( ++itr != end ) {
      out << ", ";
    }
    --itr;
  }
  out << "]";
  return out;
}

/// A convenience declaration to save myself some typeing
namespace po = boost::program_options;

int main(int argc,char* argv[])
{
  // Construct the object describing all the command line parameters of the
  // application:
  po::options_description desc( PROGRAM_DESC );
  desc.add_options()
    ( "help,h","Give some help with the program usage")
    ( "classname,n", po::value<std::string>()->default_value("D3PDReader"),
      "Generated class name")
    ( "d3pd-file,f", po::value<std::string>(),"Input D3PD file")
    ( "tree,t", po::value<std::string>(), "Name of the D3PD tree" )
    ( "variables,v", po::value<std::vector<std::string> >()->multitoken(),
      "Variable names to consider")
    ( "prefix,p", po::value<std::string>()->default_value(""),
      "Variable name prefix" )
    ( "output,o", po::value<std::string>()->default_value("./"),
      "Output directory for the generated source files" )
    ( "collection,c", "the generated class describes a D3PD collection and a class representing single elements is created" )
    ( "elementclass,e", po::value<std::string>()->default_value(""),
      "class name of generated element class, is only used if option -c is specified" )
    ( "particle,4", "the generated class has 4-vector properties" );
    

  // Interpret the command line options provided by the user:
  po::variables_map vm;
  try
  {
    po::store( po::parse_command_line( argc, argv, desc ), vm );
    po::notify( vm );
  }
  catch(const std::exception& ex)
  {
    std::cout << "error during parsing the command line options" << std::endl;
    return 255;
  }

  // If the user asked for some help, let's print it:
  if(vm.count("help"))
  {
    std::cout << desc << std::endl;
    return 0;
  }

  // Greet the user, and print all the parameters that the application
  // received:
  std::cout << PROGRAM_GREETING << std::endl;
  std::cout << "Program will be executed with the options:" << std::endl;

  // Get the name of the class to be generated:
  std::string class_name = vm["classname"].as<std::string>();
  std::cout << "  Generating class with name: \"D3PDReader::" << class_name << "\""
	    << std::endl;

  // Get the input D3PD file:
  std::string d3pd_file_name = "";
  if(vm.count("d3pd-file"))
  {
    d3pd_file_name = vm["d3pd-file"].as<std::string>();
    std::cout << "  D3PD file specified: " << d3pd_file_name << std::endl;
  }
  else
  {
    std::cout << "D3PD file was not specified" << std::endl;
    return 255;
  }

  // Get the name of the D3PD TTree:
  std::string d3pd_tree_name = vm["tree"].as< std::string >();
  if(d3pd_tree_name != "")
    std::cout << "  Using D3PD tree: " << d3pd_tree_name << std::endl;
  else
  {
    std::cout << "  D3PD tree was not specified" << std::endl;
    return 255;
  }

  // Get the variable name patterns:
  std::vector<std::string> var_names;
  if(vm.count("variables"))
  {
    var_names = vm[ "variables" ].as<std::vector<std::string> >();
    std::cout << "  Only considering variables with names: " << var_names << std::endl;
  }
  else
    std::cout << "  Considering all variables from the input file" << std::endl;

  // Get the common prefix of the variable names:
  std::string prefix = vm["prefix"].as<std::string>();
  std::cout << "  Common variable prefix: \"" << prefix << "\"" << std::endl;

  // Get the output directory:
  std::string output = vm["output"].as<std::string>();
  std::cout << "  Output directory: " << output << std::endl;

  // is D3PD collection
  bool bIsCollection = vm.count("collection");
  bool bIsParticle = vm.count("particle");

  // name for element class
  std::string elementclass = "";
  if(bIsCollection)
  {
    elementclass = vm["elementclass"].as<std::string>();
    if(elementclass == "")
    {
      std::cout << "no name for class representing a collection element" << std::endl;
      std::cout << "if the class does not represent a collection, turn off the option -c" << std::endl;
      return 255;
    }
  }
  
  
  // open D3PD file
  TFile* f = TFile::Open(d3pd_file_name.c_str());
  if(!f)
  {
    std::cout << "could not open D3pD file with name " << d3pd_file_name << std::endl;
    return 255;
  }

  // get TTree
  TTree* t = (TTree*)f->Get(d3pd_tree_name.c_str());
  if(!t)
  {
    std::cout << "could not retrieve TTree with name " << d3pd_tree_name << " in file "
	      << d3pd_file_name << std::endl;
    return 255;
  }
  
  // Extract the variable descriptions from the D3PD file:
  D3PDReaderMaker::D3PDVariableExtractor cVarExtractor;
  std::vector<D3PDReaderMaker::D3PDVariable> variables;
  if(cVarExtractor.ExtractVariables(*t,var_names,variables) == -1)
  {
    std::cout << "Couldn't extract the variable descriptions from the specified file!" << std::endl;
    return 255;
  }
  
  cVarExtractor.RemovePrefix(variables,prefix);

  // Generate the VarHandle class's source:
  if(!D3PDReaderMaker::writeVarHandle(output))
    std::cout << "Couldn't create the VarHandle class source" << std::endl;

  // Generate the header of the D3PDReader class:
  if(!D3PDReaderMaker::WriteD3PDObjectHeader(class_name,output,prefix,variables))
    std::cout << "Couldn't generate the D3PDReader class header" << std::endl;

  // Generate the source of the D3PDReader class:
  if(!D3PDReaderMaker::WriteD3PDObjectSource(class_name,output,variables))
    std::cout << "Couldn't generate the D3PDReader class source" << std::endl;

  // Generate the code for collection element if necessary:
  if(bIsCollection)
  {
    if(!D3PDReaderMaker::WriteD3PDElementHeader(elementclass,output,class_name,variables,bIsParticle))
      std::cout << "Couldn't generate the D3PDCollection element class header" << std::endl;
  }
  
  return 0;
}
