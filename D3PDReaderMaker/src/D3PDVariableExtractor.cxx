#ifndef D3PDVariableExtractor_CXX
#define D3PDVariableExtractor_CXX 1

// STL include(s)
#include <vector>
#include <string>
#include <iostream>

// BOOST include(s)
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

// ROOT include(s)
#include "TObjArray.h"
#include "TBranch.h"
#include "TBranchElement.h"
#include "TLeaf.h"
#include "TTree.h"

// custom include(s)
#include "D3PDVariableExtractor.h"

namespace D3PDReaderMaker
{
  /**
   * @short extract variables from TTree
   *
   * The passed TTree is scanned for branches matching one of the given patterns.
   * For each match a D3PDVariable is created.
   *
   * @param t input TTree from which the variables should be extracted
   * @param patterns only branches matching one the given expressions are extracted
   * @param vars containing all extracted variables
   *
   * @result number of extracted variables, -1 if an error occured
   */
  int D3PDVariableExtractor::ExtractVariables(TTree& t,const std::vector<std::string>& patterns,
					      std::vector<D3PDVariable>& vars)
  {
    unsigned int iVarCount = 0;
    // Look at all the branches in the tree:
    TObjArray* branches = t.GetListOfBranches();
    for(Int_t i = 0; i < branches->GetSize(); ++i)
    {
      if(!branches->At(i))
	continue;
      
      // Only consider the branches that match one of the specified patterns:
      bool pass = (patterns.size() == 0);
      std::vector<std::string>::const_iterator itr = patterns.begin();
      std::vector<std::string>::const_iterator end = patterns.end();
      for(; itr != end; ++itr)
      {
	if(boost::regex_search(branches->At(i)->GetName(),
			       boost::basic_regex<char>(itr->c_str())))
	{
	  pass = true;
	  break;
	}
      }
      if(!pass)
	continue;

      // Construct a new D3PD variable description:
      D3PDVariable var;
      var.type = "";
      var.fullname = branches->At(i)->GetName();
      var.doc = branches->At(i)->GetTitle();
      var.name = branches->At(i)->GetName();
      var.varname = branches->At(i)->GetName();
      var.primitive = true;
      // Replace all the whitespaces from the c++ variable name. Unfortunately
      // some developers do tend to make such typos in the branch names...
      boost::replace_all(var.varname," ","_");

      std::cout << "variable found: "<< var.name << std::endl;
      ++iVarCount;
    
      // Extract the type of the variable. This has to be done a little differently for
      // primitive types and objects (STL containers).
      std::string branch_type(branches->At(i)->IsA()->GetName());
      if(branch_type == "TBranch")
      {
	// If it's a branch describing a primitive type, then the name of the type
	// has to be extracted from the single leaf of the branch:
	var.primitive = true;
	TBranch* branch = dynamic_cast<TBranch*>(branches->At(i));
	if(!branch)
	{
	  std::cout << "Could not cast object into a TBranch!" << std::endl;
	  return -1;
	}
	TObjArray* leaves = branch->GetListOfLeaves();
	bool leaf_found = false;
	for(Int_t j = 0; j < leaves->GetSize(); ++j)
	{
	  TLeaf* leaf = dynamic_cast<TLeaf*>(leaves->At(0));
	  if(leaf)
	  {
	    var.type = leaf->GetTypeName();
	    leaf_found = true;
	    break;
	  }
	}
	if(!leaf_found)
	{
	  std::cout << "Couldn't find the TLeaf describing a primitive type!" << std::endl;
	  return -1;
	}
      }
      else
	if(branch_type == "TBranchElement")
	{
	  // If it's a branch describing an object, then the situation is a bit simpler.
	  // In this case we can ask the TBranchElement directly for the class name.
	  // Remember however that this is a "simplified" name of the class, like
	  // "vector<float>".
	  var.primitive = false;
	  TBranchElement* branch = dynamic_cast<TBranchElement*>(branches->At(i));
	  if(!branch)
	  {
	    std::cout << "Could not cast object into a TBranchElement!" << std::endl;
	    return -1;
	  }
	  var.type = std::string(branch->GetClassName()) + "*";
	}

      // Add the variable to the list:
      vars.push_back( var );
    }

    return iVarCount;
  }

  /**
   * @short remove a common prefix from variable names
   *
   * This function is meant to truncate the common prefix from the names of the variables. Only the fields
   * D3PDVariable::varname and D3PDVariable::name are affected.
   *
   * @param vars The variable descriptions we want to update
   * @param prefix The common prefix which should be removed from the variable names
   */
  void D3PDVariableExtractor::RemovePrefix(std::vector<D3PDVariable>& vars,const std::string& prefix)
  {
    // Return right away if there is no prefix defined:
    if(prefix == "") return;

    std::vector<D3PDVariable>::const_iterator itr = vars.begin();
    std::vector<D3PDVariable>::const_iterator end = vars.end();
    for(; itr != end; ++itr)
    {
      if((itr->varname.find(prefix) != 0) ||
	 (itr->name.find(prefix) != 0))
      {
	std::cout << "The specified prefix (\"" << prefix
		  << "\") is not valid for variable: "
		  << itr->fullname << std::endl;
	continue;
      }
      for(size_t i = 0; i < prefix.size(); ++i)
      {
	itr->name.erase(itr->name.begin());
	itr->varname.erase(itr->varname.begin());
      }
    }
  }
} // namespace D3PDReaderMaker
#endif // D3PDVariableExtractor_CXX
