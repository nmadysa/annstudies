#ifndef D3PDMAKERREADER_D3PDVARIABLE_H
#define D3PDMAKERREADER_D3PDVARIABLE_H

// STL include(s):
#include <string>

/**
 * @short tools for extracting variable information from TTree and generate classes
 *
 * This namespace contains a couple of useful methods for extracting variable information
 * from a TTree and wrap them in custom-designed classes.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
namespace D3PDReaderMaker
{
  /**
   *  @short Structure describing one D3PD variable
   *
   *         This structure is used to provide information to the code generator
   *         functions about a single D3PD variable.
   *
   * @author Christian Gumpert <cgumpert@cern.ch>
   */
  struct D3PDVariable
  {
    std::string           type; ///< Full type name of the variable
    std::string           fullname; ///< Full name of the variable in the D3PD
    std::string           doc; ///< Variable documentation string
    mutable std::string   name; ///< Variable name without prefix
    mutable std::string   varname; ///< Variable name without prefix and whitespaces
    bool                  primitive; ///< Flag showing whether variable is a primitive
  }; // struct D3PDVariable
  
} // namespace D3PDReaderMaker

#endif // D3PDMAKERREADER_D3PDVARIABLE_H
