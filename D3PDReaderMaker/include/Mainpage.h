/**
 * @mainpage D3PDReaderMaker
 *
 * This tool should simplify the extraction of information about variables stored in a tree.
 *
 * @section Installation Installation
 *
 * You can retrieve this tool by doing
 *
 * <tt>bzr branch /soft/users/bzrrepos/D3PDReaderMaker your/path/to/D3PDReaderMaker</tt>
 *
 * In order to compile and run this tool, the global variabels @c BOOST_INC and @c BOOST_LIB have to bet set and point
 * to the directory containing the LOKI header files and its libraries respectively. In addition, the library path as to
 * be appended to the @c LD_LIBRARY_PATH variable. At our institute all this can be achieved by calling the following
 * command on the pktf**
 *
 * @code
 * module load boost
 * @endcode
 *
 * Then you can use the Makefile to create the executable.
 *
 * @section Idea The basic idea
 *
 * This tool creates classes (so-called D3PDCollections) which contain wrapper around the actual branches in the tree.
 * Each D3PDCollection contains a list of VarHandle classes which act as a wrapper around a certain branches. The VarHandle
 * class takes as template argument the underlying type of the associated branch. When invoking the operator() of a VarHandle
 * object, the current value of the branch is returned.
 *
 * Example: VarHandle<Int_t> EventNumber; EventNumber() would return the current event number from the loaded entry in the tree.
 *
 * The main advantage is the access-on-demand. One does not have to specify which branches should be read when a new entry
 * of the tree is loaded. Instead, the branches are read in if the corresponding VarHandle object is asked for its value. Which
 * branch is read in is decided at run-time and not at compile-time any more.
 *
 * Most often the trees are so-called flat ntuples which means that all objects are split into basic types. The disadvantage is that
 * you do not find an Electron object inside a tree but a list of pt, eta, phi... values which has to be assembled to electron objects.
 * Classes describing single elements of such a D3PDCollection can be created using the option -c (see below).
 *
 * @section Usage Usage
 *
 * The executable @c D3PDReaderMaker takes the following command line arguments:
 *
 *  - h: prints the help
 *  - n: name of generated class
 *  - f: root file containing the tree
 *  - t: name of the TTree object
 *  - v: list or RegEx pattern for variables which should be extracted (omitting this option will extract all variables)
 *  - p: common prefix which is stripped from all variable names (make sure that this is really a common prefix for all variables)
 *  - o: output directory where all generated files are placed
 *  - c: creates a class describing a single element of the D3PDCollection
 *  - e: name of the single element class
 *
 *
 * A common application is the creation of electron objects which would look like this:
 *
 * @code
 * ./D3PDReaderMaker -n ElectronD3PDCollection -f myRootfile.root -t myTree -v ^el_ -p el_ -o outdir/ -c -e Electron
 * @endcode
 *
 * If you have any comments, questions, suggestions, please contact me.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
