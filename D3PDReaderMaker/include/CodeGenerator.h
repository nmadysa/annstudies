#ifndef D3PDMAKERREADER_CODEGENERATOR_V2_H
#define D3PDMAKERREADER_CODEGENERATOR_V2_H 

// STL include(s):
#include <string>
#include <vector>

// Local include(s):
#include "D3PDVariable.h"

/**
 * @short namespace containing all needed function to generate the source code of D3PDReader classes
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
namespace D3PDReaderMaker
{
  /// generates the source code of the D3PDReader::VarHandle class  
  bool writeVarHandle(const std::string& dir);
  /// write the header file for the custom-designed class
  bool WriteD3PDObjectHeader(const std::string& classname,const std::string& dir,
			     const std::string& prefix,const std::vector<D3PDVariable>& vars);
  /// write the source file for the custom-designed class
  bool WriteD3PDObjectSource(const std::string& classname,const std::string& dir,
			    const std::vector<D3PDVariable>& vars);
  /// write the header file for class representing one element of a D3PDCollection
  bool WriteD3PDElementHeader(const std::string& classname,const std::string& dir,
			      const std::string& collectionclass,const std::vector<D3PDVariable>& vars,
			      bool bIsParticle);
  /// add STL header if it appears in the used variables
  void addSTLHeader(std::ostream& out,const char* name,const std::vector<D3PDVariable>& vars);
  /// extract template argument of a vector
  std::string vectorTemplateArgument(const std::string& type,bool& ok);
  /// check type whether it is a primitive C++ type
  bool isPrimitive(const std::string& type);
} // namespace D3PDReaderMaker

#endif // D3PDMAKERREADER_CODEGENERATOR_V2_H
