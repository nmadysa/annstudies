#ifndef D3PDVariableExtractor_H
#define D3PDVariableExtractor_H 

// ROOT include(s)
class TTree;

// custom include(s)
#include "D3PDVariable.h"

namespace D3PDReaderMaker
{
  /**
   * @short Class encapsulating functions for retrieving D3PD variables from TTree
   *
   *        This class encapsulates methods for retrieving and manipulating D3PD
   *        variables from a TTree.
   *
   * @author Christian Gumpert <cgumpert@cern.ch>
   */
  class D3PDVariableExtractor
  {
  public:
    /// extract information about branches stored in a tree
    int ExtractVariables(TTree& t,const std::vector<std::string>& patterns,
			 std::vector<D3PDVariable>& vars);

    /// strip common prefix from variable names
    void RemovePrefix(std::vector<D3PDVariable>& vars,const std::string& prefix);
  }; // class D3PDVariableExtractor
  
} // D3PDReaderMaker

#endif // D3PDVariableExtractor_H
