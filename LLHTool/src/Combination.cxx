#ifndef Combination_CXX
#define Combination_CXX

//TauCommon include(s)
#include "LLHTau.h"

//custom include(s)
#include "Combination.h"

template <>
void Combination<D3PDReader::LLHTau*>::Fill(D3PDReader::LLHTau* const particle, const double& score, bool IsData){
    if(!IsData) {
		//std::cout << "";
		m_LHScore->Fill(particle->TruthPt()/1000., score, particle->NVertices());
	}
    else
        m_LHScore->Fill(particle->pt()/1000., score, particle->NVertices());
}
#endif

