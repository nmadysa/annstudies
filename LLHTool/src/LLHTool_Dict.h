/********************************************************************
* src/LLHTool_Dict.h
* CAUTION: DON'T CHANGE THIS FILE. THIS FILE IS AUTOMATICALLY GENERATED
*          FROM HEADER FILES LISTED IN G__setup_cpp_environmentXXX().
*          CHANGE THOSE HEADER FILES AND REGENERATE THIS FILE.
********************************************************************/
#ifdef __CINT__
#error src/LLHTool_Dict.h/C is only for compilation. Abort cint.
#endif
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define G__ANSIHEADER
#define G__DICTIONARY
#define G__PRIVATE_GVALUE
#include "G__ci.h"
#include "FastAllocString.h"
extern "C" {
extern void G__cpp_setup_tagtableLLHTool_Dict();
extern void G__cpp_setup_inheritanceLLHTool_Dict();
extern void G__cpp_setup_typetableLLHTool_Dict();
extern void G__cpp_setup_memvarLLHTool_Dict();
extern void G__cpp_setup_globalLLHTool_Dict();
extern void G__cpp_setup_memfuncLLHTool_Dict();
extern void G__cpp_setup_funcLLHTool_Dict();
extern void G__set_cpp_environmentLLHTool_Dict();
}


#include "TObject.h"
#include "TMemberInspector.h"
#include "include/Combination.h"
#include "include/CombinationHandler.h"
#include "include/IDVarHandle.h"
#include "include/IDVarLLHHelper.h"
#include "include/LLHCalculator.h"
#include "include/LLHCommonUtilities.h"
#include "include/Ranking.h"
#include "include/Variable.h"
#include "include/VariablePool.h"
#include <algorithm>
namespace std { }
using namespace std;

#ifndef G__MEMFUNCBODY
#endif

extern G__linked_taginfo G__LLHTool_DictLN_vectorlEROOTcLcLTSchemaHelpercOallocatorlEROOTcLcLTSchemaHelpergRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlEROOTcLcLTSchemaHelpercOallocatorlEROOTcLcLTSchemaHelpergRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlETVirtualArraymUcOallocatorlETVirtualArraymUgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlETVirtualArraymUcOallocatorlETVirtualArraymUgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_TVectorTlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TVectorTlEdoublegR;
extern G__linked_taginfo G__LLHTool_DictLN_iteratorlEbidirectional_iterator_tagcOTObjectmUcOlongcOconstsPTObjectmUmUcOconstsPTObjectmUaNgR;
extern G__linked_taginfo G__LLHTool_DictLN_maplESMsgTypecOstringcOlesslESMsgTypegRcOallocatorlEpairlEconstsPSMsgTypecOstringgRsPgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlESGeneratorCutcOallocatorlESGeneratorCutgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlESGeneratorCutcOallocatorlESGeneratorCutgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlESFilecOallocatorlESFilegRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlESFilecOallocatorlESFilegRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlESTreecOallocatorlESTreegRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlESTreecOallocatorlESTreegRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_maplEintcOvectorlESTreecOallocatorlESTreegRsPgRcOlesslEintgRcOallocatorlEpairlEconstsPintcOvectorlESTreecOallocatorlESTreegRsPgRsPgRsPgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlESDataSetcOallocatorlESDataSetgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlESDataSetcOallocatorlESDataSetgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_maplEstringcOTObjArraymUcOlesslEstringgRcOallocatorlEpairlEconstsPstringcOTObjArraymUgRsPgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_listlEstringcOallocatorlEstringgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlETTreemUcOallocatorlETTreemUgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlETTreemUcOallocatorlETTreemUgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlETBranchmUcOallocatorlETBranchmUgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlETBranchmUcOallocatorlETBranchmUgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_listlETObjectmUcOallocatorlETObjectmUgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_pairlEstringcOstringgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlEpairlEstringcOstringgRcOallocatorlEpairlEstringcOstringgRsPgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlEpairlEstringcOstringgRcOallocatorlEpairlEstringcOstringgRsPgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlESInputDatacOallocatorlESInputDatagRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlESInputDatacOallocatorlESInputDatagRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlEstringcOallocatorlEstringgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlEstringcOallocatorlEstringgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlEintcOallocatorlEintgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlEintcOallocatorlEintgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_maplEstringcOToolBasemUcOlesslEstringgRcOallocatorlEpairlEconstsPstringcOToolBasemUgRsPgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTBaselEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTBaselEdoublegR;
extern G__linked_taginfo G__LLHTool_DictLN_TElementActionTlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TElementPosActionTlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTRow_constlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTRowlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTDiag_constlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTColumn_constlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTFlat_constlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTSub_constlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTSparseRow_constlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTSparseDiag_constlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTColumnlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTDiaglEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTFlatlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTSublEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTSparseRowlEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_TMatrixTSparseDiaglEfloatgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlEvectorlEfloatcOallocatorlEfloatgRsPgRcOallocatorlEvectorlEfloatcOallocatorlEfloatgRsPgRsPgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlEvectorlEfloatcOallocatorlEfloatgRsPgRcOallocatorlEvectorlEfloatcOallocatorlEfloatgRsPgRsPgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlEvectorlEintcOallocatorlEintgRsPgRcOallocatorlEvectorlEintcOallocatorlEintgRsPgRsPgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlEvectorlEintcOallocatorlEintgRsPgRcOallocatorlEvectorlEintcOallocatorlEintgRsPgRsPgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlEvectorlEunsignedsPintcOallocatorlEunsignedsPintgRsPgRcOallocatorlEvectorlEunsignedsPintcOallocatorlEunsignedsPintgRsPgRsPgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlEvectorlEunsignedsPintcOallocatorlEunsignedsPintgRsPgRcOallocatorlEvectorlEunsignedsPintcOallocatorlEunsignedsPintgRsPgRsPgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlED3PDReadercLcLTruthTaumUcOallocatorlED3PDReadercLcLTruthTaumUgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlED3PDReadercLcLTruthTaumUcOallocatorlED3PDReadercLcLTruthTaumUgRsPgRcLcLiteratorgR;
extern G__linked_taginfo G__LLHTool_DictLN_vectorlED3PDReadercLcLTaumUcOallocatorlED3PDReadercLcLTaumUgRsPgR;
extern G__linked_taginfo G__LLHTool_DictLN_reverse_iteratorlEvectorlED3PDReadercLcLTaumUcOallocatorlED3PDReadercLcLTaumUgRsPgRcLcLiteratorgR;

/* STUB derived class for protected member access */
