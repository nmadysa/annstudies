#ifndef LLHCalculator_CXX
#define LLHCalculator_CXX

//custom includes
#include "LLHCalculator.h"

//STL includes
#include <iostream>
#include <assert.h>
#include <cxxabi.h>
#include <cstdlib>

//ClassImp(LLHCalculator)

LLHCalculator::LLHCalculator(CycleBase* pParent,const char* sName):
  ToolBase(pParent,sName)
{
  DeclareProperty("llhbinborders", m_binborders);
}

LLHCalculator::~LLHCalculator()
{
}

void LLHCalculator::Initialize(std::vector<double>& binborders){
  //  m_binborders = binborders;
  m_binborders.resize(binborders.size());
  copy(binborders.begin(), binborders.end(), m_binborders.begin());
}
#endif
