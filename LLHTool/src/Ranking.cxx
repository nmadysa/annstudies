#ifndef Ranking_CXX
#define Ranking_CXX

#ifndef __CINT__
//boost includes
#include "boost/bind.hpp"
#endif //__CINT

//custom includes
#include "Ranking.h"

Ranking* Ranking::m_instance = 0;

Ranking* Ranking::GetInstance(){
  if(m_instance == 0)
    m_instance = new Ranking();
  return m_instance;
}
/*
void Ranking::PrintBest(){
  m_topten.front()->Print();
}

void Ranking::PrintWorst(){
  m_bottomten.back()->Print();
}
void Ranking::CheckCombination(Combination* comb){
  //get local copy, ownership at Ranking
  Combination* rcomb = comb;
  m_topten.push_back(rcomb);
  m_bottomten.push_front(rcomb);
  //sort by sig eff vs bkg rej
  m_topten.sort(boost::bind(&Combination::best_perf, _1, _2));
  m_bottomten.sort(boost::bind(&Combination::worst_perf, _1, _2));

  //keep only top ten and worst ten
  if(m_topten.size() > 10)
    m_topten.pop_back();
  if(m_bottomten.size() > 10)
    m_topten.pop_front();
}
*/
#endif
