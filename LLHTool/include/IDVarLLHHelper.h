#ifndef IDVarLLHHelper_H
#define IDVarLLHHelper_H

template<class T>
class IDVarLLHHelper{

 public:
  //this code will be generated automatically; do not tauch this
  //cgen::start define NTupleFunctors
	static float EMRadius(T const& particle)
	{
		return particle->EMRadius();
	}
	static float CalRadius(T const& particle)
	{
		return particle->CalRadius();
	}
  //cgen::end
  //variable recalculation has to be put HERE and will not be touched by CodeGenerator
};
#endif
