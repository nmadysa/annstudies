#ifndef LLHCalculator_H
#define LLHCalculator_H

// SFrame include
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// STL
#include <vector>

//Custom includes
#include "Variable.h"
#include "AnalysisTau.h"
#include "LLHTau.h"
#include "Tau.h"
#include "ToolBase.h"

class LLHCalculator : public ToolBase
{
public:

  LLHCalculator(CycleBase* pParent,const char* sName);

  virtual ~LLHCalculator();
  void Initialize(std::vector<double>&);
  template<class T>
  double CalculateLLHRatio(T particle, int nvtx, Variable<T>* iVar);
  template <class T>
  float CalculateLHScore(const T particle, Variable<T>* iVar);

private:
  std::vector<double> m_binborders;
  template <class T>
  double CalculateSingleLLHRatio(const T particle, int nvtx, Variable<T>* iVar=NULL, float nextbin = 0.);
public:
  //  ClassDef(LLHCalculator,0);
};

//template implementation
#include "LLHCalculator.icc"
#endif // LLHCalculator_H


