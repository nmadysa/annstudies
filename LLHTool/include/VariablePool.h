#ifndef VariablePool_H
#define VariablePool_H

//STL includes
#include <map>
#include <string>
#include <assert.h>
#include <iostream>

// SFrame include
#include "core/include/SError.h"
#include "core/include/SInputData.h"

//ROOT includes
#include "TFile.h"
#include "TTree.h"

//custom includes
#include "ToolBase.h"
#include "Variable.h"
#include "LLHCalculator.h"

template <class T> class VariablePool : public ToolBase
{
 public:
  /// constructor specifying the parent and the name of the tool
 VariablePool(CycleBase* pParent,const char* sName);

  virtual ~VariablePool(){
    //m_fSignal->Close();
    //    m_fData->Close();
  }

  void Initialize();
  void Acquire(const std::string& varname, const int& id, bool doOpt=false){
    if(!doOpt)
      m_sVarMap[varname] = new Variable<T>(varname, id, m_fSignal, m_fData, true);
    else
      m_sVarMap[varname] = new Variable<T>(varname, id);
    m_iVarMap[id] = m_sVarMap[varname];
  }
  //TODO should return const
  Variable<T>* Retrieve(const std::string& varname){
    typename std::map<std::string, Variable<T>* >::iterator it = m_sVarMap.find(varname);
    assert(it != m_sVarMap.end());
    return it->second;
  }
  void GetLHRatio(T const vParticle, const int& nVertices=1);
  float GetLHScore(const std::vector<std::string> varset, const T particle);
  std::map<std::string, Variable<T>* > GetMap(){return this->m_sVarMap;}

 private:
  bool                                 c_do_readPDF;
  std::string                          c_pdf_sig_fName;
  std::string                          c_pdf_data_fName;
  TFile* m_fSignal;
  TFile* m_fData;
  std::map<std::string, Variable<T>*> m_sVarMap;
  std::map<int, Variable<T>*> m_iVarMap;
  LLHCalculator* m_calculator;

};
#include "VariablePool.icc"

#endif
