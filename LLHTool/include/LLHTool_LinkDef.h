// Dear emacs, this is -*- c++ -*-
// $Id: LLHTool_LinkDef.h 357597 2014-04-22 13:05:49Z shanisch $
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;

#endif // __CINT__
