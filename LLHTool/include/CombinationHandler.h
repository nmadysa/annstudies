#ifndef CombinationHandler_H
#define CombinationHandler_H

//STL includes
#include <string>
#include <vector>

// SFrame include
#include "core/include/SError.h"
#include "core/include/SInputData.h"

//core includes
#include "ToolBase.h"

//custom includes
#include "Combination.h"
#include "LLHCalculator.h"
#include "VariablePool.h"

template <class T>
class CombinationHandler : public ToolBase{

public:
  CombinationHandler(CycleBase* pParent,const char* sName);

  virtual ~CombinationHandler(){}

  void Initialize(const std::vector<std::string>& vVarSet);
  void ProcessCombination(T const particle);
  void WriteAllCombinations();

  double GetScore(T const particle);

private:

  std::vector<std::string>                    m_vVarSet;
  std::vector<Combination<T>* >               m_vCombinations;

  bool                                        c_do_nx;
};
//implementation
#include "CombinationHandler.icc"
#endif
