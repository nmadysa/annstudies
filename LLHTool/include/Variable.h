#ifndef Variable_H
#define Variable_H

//STL includes
#include <string>

#ifndef __CINT__
//boost includes
#include "boost/function.hpp"
#endif

//ROOT includes
#include "TH1F.h"
#include "TH3F.h"
#include "TFile.h"

//custom includes
#include "IDVarHandle.h"


template <typename P>
class Variable{
  #ifndef __CINT__
  typedef boost::function<float (const P)> TFUNC;
#endif
public:
  Variable(){}
  Variable(const std::string& name,
	   const int& id,
	   TFile* fSignal,
	   TFile* fData,
	   bool readPDF = true);
  Variable(const std::string& name,
	   const int& id);
  Variable(Variable& var){
    *this = var;
  };
  virtual ~Variable();

  Variable& operator=(const Variable& rhs){
    m_name = rhs.m_name;
    m_id = rhs.m_id;
    //    m_readPDF = rhs.m_readPDF;
    pdf_sig_1P = rhs.pdf_sig_1P;
    pdf_sig_3P = rhs.pdf_sig_3P;
    pdf_data_1P = rhs.pdf_data_1P;
    pdf_data_3P = rhs.pdf_data_3P;

    return *this;
  }
  float GetValue(const P particle){
    return m_rFunctor(particle);
  }
  float GetLHRatio(){return this->llhratio;}
  int GetId(){return this->m_id;}
  std::string GetName(){return this->m_name;}
  TH3F* pdf_sig_1P;
  TH3F* pdf_sig_3P;
  TH3F* pdf_data_1P;
  TH3F* pdf_data_3P;
  float llhratio;
private:
  std::string m_name;
  int m_id;
  bool m_readPDF;
  #ifndef __CINT__
  TFUNC m_rFunctor;
#endif
};
#include "Variable.icc"

#endif
