#ifndef Ranking_H
#define Ranking_H

//STL includes
#include <list>

//custom includes
//#include "Combination.h"

class Ranking
{
 public:
  static Ranking* GetInstance();
  /*  virtual  ~Ranking(){
    delete &m_topten;
    delete &m_bottomten;
  };
  */
  //  void CheckCombination(Combination* comb);
  void PrintBest();
  void PrintWorst();
 private:
  Ranking(){}
  static Ranking* m_instance;
  //  std::list<Combination*> m_topten;
  //  std::list<Combination*> m_bottomten;
};

#endif
