#ifndef Combination_H
#define Combination_H

//boost includes
#ifndef __CINT__
#include "boost/bind.hpp"
#include "boost/function.hpp"
#endif //__CINT__

//STL includes
#include <vector>

//ROOT includes
#include "TH3F.h"
#include "TFile.h"

//custom includes
#include "VariablePool.h"

template <class T> class Combination{
 
public:
  //constructor
  Combination(std::vector<std::string>& varset){
    sVarSet = varset;
    m_LHScore = new TH3F("LHScore", "LHScore", 1000, 0., 1000., 100, -40., 40., 40, -0.5, 39.5);
  }
  Combination(std::vector<int>& varset){
    m_ivarset = varset;
  }

  void Print();
  void NormalizeLHScore();
  void WriteToFile();
  void Fill(T const particle, const double& score, bool IsData = false);

  TH3F* GetScore() const {return this->m_LHScore;}
  double GetScore(const T particle);
  bool GetNext();
  static bool best_perf(Combination* comb1, Combination* comb2){
    return (comb1->GetSOverB() > comb1->GetSOverB());
  }
  static bool worst_perf(Combination* comb1, Combination* comb2){
    return (comb1->GetSOverB() < comb1->GetSOverB());
  }
  std::vector<std::string> sVarSet;

private:
  std::vector<int> m_ivarset;
  TH3F* m_LHScore; 
};
#include "Combination.icc"
#endif
