#ifndef LLHCommonUtilities_H
#define LLHCommonUtilities_H

#include "TruthTau.h"
#include "Tau.h"
#include "Vertex.h"
namespace LLHCommonUtilities
{
  inline bool type_comp(const D3PDReader::Vertex* v1, const D3PDReader::Vertex* v2){
    return (v1->type() < v2->type());
  }
  inline bool TrackMatch(const D3PDReader::TruthTau* truetau, D3PDReader::Tau* tau){
    if((unsigned int)truetau->tauAssoc_index() == tau->GetIndex() && truetau->nProng() == tau->numTrack())
      return true;
    else 
      return false;
  }
  inline void truthmatching(const std::vector<D3PDReader::TruthTau*>& m_vTrueTaus, std::vector<D3PDReader::Tau*>& m_vTaus){
    std::vector<D3PDReader::Tau*>::iterator it = m_vTaus.begin();
    while (it!= m_vTaus.end()){
      std::vector<D3PDReader::TruthTau*>::const_iterator sec = std::find_if(m_vTrueTaus.begin(), m_vTrueTaus.end(), boost::bind(&LLHCommonUtilities::TrackMatch, _1, *it));
      if(sec == m_vTrueTaus.end())
	it = m_vTaus.erase(it);
      else
	++it;
    }
  }

}
#endif
