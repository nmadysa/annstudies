//ROOT includes
#include "TFile.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TCanvas.h"

//STL includes
#include <assert.h>
#include <string>
#include <iostream>
#include <vector>
#include <cstdlib>

std::vector<TCanvas*> PlotScores(const std::string& fNameSig, const std::string& fNameData);
void CalculateLMTCuts(TH1D* h_llhscore, std::vector<std::vector<double> >* vCuts);
TCanvas* ApplyStyle(TH1D* sig, TH1D* data);
void Print(const std::vector<double>& vCuts, int type);
void Help();

int main(int argc, char* argv[]){
  if(argc<2){
    std::cout << "No input file given" << std::endl;
    exit(1);
  }
  if(strcmp(argv[1],"Help") == 0){
    Help();
    exit(1);
  }
  if(argc<4){
    std::cout << "No input file given" << std::endl;
    exit(1);
  }
  else if((std::string(argv[1])).find(".root") != std::string::npos && (std::string(argv[2])).find(".root") != std::string::npos){
    std::vector<TCanvas*> v_cScores = PlotScores(std::string(argv[1]), std::string(argv[2]));
    std::vector<TCanvas*>::iterator it = v_cScores.begin();
    for(; it != v_cScores.end(); ++it)
      (*it)->Print((argv[4]+(std::string)((*it)->GetName())+".png").c_str());
  }
}
std::vector<TCanvas*> PlotScores(const std::string& fNameSig, const std::string& fNameData){
  std::vector<TCanvas*> v_cScores;
  TFile* fSig = TFile::Open(fNameSig.c_str());
  TH2F* h_llhscore_pt_sig = dynamic_cast<TH2F*>(fSig->Get("test"));
  TFile* fData = TFile::Open(fNameData.c_str());
  TH2F* h_llhscore_pt_data = dynamic_cast<TH2F*>(fData->Get("test"));
  assert(h_llhscore_pt_sig->GetNbinsX() == h_llhscore_pt_data->GetNbinsX());
  for(Int_t iPt=0; iPt<h_llhscore_pt_sig->GetNbinsX() - 1; iPt++){
    TH1D* h_llhscore_sig = h_llhscore_pt_sig->ProjectionY(Form("llhscore_%i_sig", iPt), iPt, iPt+1);
    TH1D* h_llhscore_data = h_llhscore_pt_data->ProjectionY(Form("llhscore_%i_data", iPt), iPt, iPt+1);
    v_cScores.push_back(ApplyStyle(h_llhscore_sig, h_llhscore_data));
  }
  return v_cScores;
}

TCanvas* ApplyStyle(TH1D* sig, TH1D* data){
  TCanvas* c = new TCanvas(sig->GetName(), sig->GetName(), 800, 600);
  c->cd();
  sig->Draw();
  data->Draw("psames");
  return c;
}


void Help(){
  std::cout << "Help" << std::endl;
}
