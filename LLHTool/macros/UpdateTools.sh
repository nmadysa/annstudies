#!/bin/bash

SRCDIR=../src
INCDIR=../include
if [ $# -eq 0 ]
then
    TOOL=ALL
else
    TOOL="$1"
fi

if [[ "$TOOL" == "PDFTool" || "$TOOL" == "ALL" ]] 
then
    source UpdateCode.sh "$SRCDIR/PDFPlotter.cxx"
fi

if [[ "$TOOL" == "NTupleTool" || "$TOOL" == "ALL" ]] 
then
    source UpdateCode.sh "$INCDIR/IDVarInitializer.h"
    source UpdateCode.sh "$INCDIR/IDVarHelper.h"
    source UpdateCode.sh "$INCDIR/IDVarLLHHelper.h"
fi
