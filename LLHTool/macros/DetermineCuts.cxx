//ROOT includes
#include "TFile.h"
#include "TH1D.h"
#include "TH2F.h"

//STL includes
#include <string>
#include <iostream>
#include <vector>
#include <cstdlib>

void DetermineCuts(std::string fName);
void CalculateLMTCuts(TH1D* h_llhscore, std::vector<std::vector<double> >* vCuts);
void Print(std::vector<std::vector<double> >* vCuts);
void Print(const std::vector<double>& vCuts, int type);
void Help();

int main(int argc, char* argv[]){
  if(argc<2){
    std::cout << "No input file given" << std::endl;
    exit(1);
  }
  if(strcmp(argv[1],"Help") == 0){
    Help();
    exit(1);
  }
  else if((std::string(argv[1])).find(".root") != std::string::npos){
    DetermineCuts(argv[1]);
  }
}
void DetermineCuts(std::string fName){
  std::vector<std::vector<double> >* vLMTCuts = new std::vector<std::vector<double> >(3);
  TFile* f = TFile::Open(fName.c_str());
  TH2F* h_llhscore_pt = dynamic_cast<TH2F*>(f->Get("test"));
  for(Int_t iPt=0; iPt<h_llhscore_pt->GetNbinsX() - 1; iPt++){
    TH1D* h_llhscore = h_llhscore_pt->ProjectionY(Form("llhscore_%i", iPt), iPt, iPt+1);
    CalculateLMTCuts(h_llhscore, vLMTCuts);
  }
  Print(vLMTCuts);
}

void CalculateLMTCuts(TH1D* h_llhscore, std::vector<std::vector<double> >* vLMTCuts){
  std::vector<double> cuts;
  double integral = h_llhscore->Integral();
  double sum = 0.;
  for(Int_t i=0; i<h_llhscore->GetNbinsX() - 1; i++){
    sum += h_llhscore->GetBinContent(i);
    if(sum > 0.3*integral && cuts.size() == 0)
      cuts.push_back(h_llhscore->GetBinLowEdge(i));
    else if(sum > 0.5*integral && cuts.size() == 1)
      cuts.push_back(h_llhscore->GetBinLowEdge(i));
    else if(sum > 0.7*integral && cuts.size() == 2)
      cuts.push_back(h_llhscore->GetBinLowEdge(i));
  }
  vLMTCuts->push_back(cuts);
}

void Print(std::vector<std::vector<double> >* vLMTCuts ){
  std::vector<std::vector<double> >::const_iterator cit = vLMTCuts->begin();
  std::cout << "----------- loose cuts ----------------" << std::endl;
  for(; cit != vLMTCuts->end(); ++cit)
    Print((*cit), 0);
  cit = vLMTCuts->begin();
  std::cout << "----------- medium cuts ----------------" << std::endl;
  for(; cit != vLMTCuts->end(); ++cit)
    Print((*cit), 1);
  cit = vLMTCuts->begin();
  std::cout << "----------- tight cuts ----------------" << std::endl;
  for(; cit != vLMTCuts->end(); ++cit)
    Print((*cit), 2);
}

void Print(const std::vector<double>& vCuts, int type){
  if(vCuts.size())
   std::cout << vCuts.at(type) << std::endl;
}

void Help(){
  std::cout << "Help" << std::endl;
}
