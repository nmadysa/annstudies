#!/bin/bash

if [ $# -eq 0 ]
then
    FILE=ALL
else
    FILE=$1
fi

if [ $# -eq 1 ]
then 
    VARLIST="variables.xml"
else
    VARLIST=$2
fi

EXEC="python CodeGenerator.py"
DIR=$PWD
cd ../python/
$EXEC --file $FILE --varlist $VARLIST
cd $DIR
