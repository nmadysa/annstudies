#!/usr/bin/env python

import XMLParser
import HistCreator
import LLHCreator
import CParser
import shutil
import os,sys
import argparse

def readVariables(defFile="variables.xml"):
    result = XMLParser.load_dict(defFile)
    return result

def main(argv):
    parser = argparse.ArgumentParser(description='Code Generator to generate code for hist, pdf, functors etc. automatically')
    parser.add_argument('--file',  dest='file', default="VariablePlotter_template.cxx", help='file to be updated')
    parser.add_argument('--varlist', dest='varlist', default="variables.xml",help='variable list')

    args = parser.parse_args()
    result = readVariables(args.varlist)
    histcreator = HistCreator.HistCreator()
    histcreator.nptbins = (1,2,3)
    histcreator.prongbins = True
    llhcreator = LLHCreator.LLHCreator()
    book1D = ""
    fill1D = ""
    fill1Dbinned = ""
    book3D = ""
    fill3Dpdf = ""
    initIDVarFunctors = ""
    initLLHIDVarFunctors = ""
    defineFunctors = ""
    defineNTupleFunctors = ""
    aquireVariables = ""
    for hist in result.values():
        book1D += histcreator.define1DHist(hist)
        fill1D += (histcreator.define1DFiller(hist))[0]
        fill1Dbinned += (histcreator.define1DFiller(hist))[1]
        book3D += histcreator.define3DHist(hist)
        fill3Dpdf += histcreator.define3DPDFFiller(hist)
        initIDVarFunctors += (llhcreator.InitFunctors(hist))[0]
        initLLHIDVarFunctors += (llhcreator.InitFunctors(hist))[1]
        defineFunctors += llhcreator.defineFunctor(hist)
        defineNTupleFunctors += llhcreator.defineNTupleFunctor(hist)
        aquireVariables += llhcreator.acquireVariables(hist)

    writer = CParser.CParser(args.file)
    patterns = writer.getPatterns()
    writer.parseAndAdd("book 1D", book1D)
    writer.parseAndAdd("fill 1D inclusive", fill1D)
    writer.parseAndAdd("fill 1D pT binned", fill1Dbinned)
    writer.parseAndAdd("book 3D", book3D)
    writer.parseAndAdd("fill 3D pdf", fill3Dpdf)
    writer.parseAndAdd("build IDVarInitializer", initIDVarFunctors)
    writer.parseAndAdd("build LLHIDVarInitializer", initLLHIDVarFunctors)
    writer.parseAndAdd("define Functors", defineFunctors)
    writer.parseAndAdd("define NTupleFunctors", defineNTupleFunctors)
    writer.parseAndAdd("aquire variables",aquireVariables)
    shutil.copy("tmp.cxx",args.file)
    writer.cleanup()
    try:
        os.remove("../src/test2.cxx")
    except OSError:
        print "requested file does not exists"

if __name__ == '__main__':
    main(sys.argv[1:])
