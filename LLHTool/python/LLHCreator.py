#/usr/bin/env python

class LLHCreator(object):
    def __init__(self):
        self.nptbins = None
        self.prongbins = False
        pass

    def InitFunctors(self, definition):
        if not definition["active"] == "true":
            return ("","")
        string = ""
        string2 = ""
        string += "\tvarhandle->AddVariable(\"%s\", ((boost::function<float (const T)>) IDVarHelper<T>::%s));\n" %(definition["name"],
                                                                                                                   definition["name"])
        string2 += "\tvarhandle->AddVariable(\"%s\", ((boost::function<float (const T)>) IDVarLLHHelper<T>::%s));\n" %(definition["name"],
                                                                                                                      definition["name"])
        return (string, string2)

    def defineFunctor(self, definition):
        if definition["d3pdname"] == "None":
            return ""
        string = ""
        string += "\tstatic float %s(T const& particle)\n" % (definition["name"])
        string += "\t{\n"
        string += "\t\treturn particle->%s();\n" % (definition["d3pdname"])
        string += "\t}\n"
        return string

    def defineNTupleFunctor(self, definition):
        string = ""
        string += "\tstatic float %s(T const& particle)\n" % (definition["name"])
        string += "\t{\n"
        string += "\t\treturn particle->%s();\n" % (definition["name"])
        string += "\t}\n"
        return string

    def acquireVariables(self, definition):
        if not definition["active"] == "true":
            return ""
        string = ""
        string += "\tm_pool->Acquire(\"%s\", %s, doOpt);\n" % (definition["name"],
                                                               definition["id"])
        return string


    def define3DPDFFiller(self, definition):
        string = ""
        string += "\t(dynamic_cast<TH3F*>(Hist((\"hpdf_\"+prong+\"_both_v%s\").c_str())))->Fill((*cit)->%s(),(*cit)->TLV().Pt()/1000., nvtx);\n" % (definition["id"],
                                                                                                                                                  definition["d3pdname"]
                                                                                                                                                  )
        return string

    def equalBinRanges(self, definition):
        nbinsx = int(definition["nbinsx"])
        xlow = float(definition["xlow"])
        xup = float(definition["xup"])
        print "name: " + definition["name"] + "   nbinsx: " + definition["nbinsx"] + "  xlow: " + definition["xlow"] + "   xup: " + definition["xup"]
        i = 0
        string = "{"
        for i in range(nbinsx+1):
            val = xlow + i*(xup-xlow)/nbinsx;
            string = string + str(val) + ","
        string = (string.rstrip(",")) + "}"
        return string

if __name__ == '_main__':
    pass
