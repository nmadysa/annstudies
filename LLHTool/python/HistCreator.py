#/usr/bin/env python

class HistCreator(object):
    def __init__(self):
        self.nptbins = None
        self.prongbins = False
        pass
    def createHistDef(self, definition, dimension, bins):
        string = ""

        return string

    def define1DHist(self, definition):
        string = ""
        
        if self.prongbins:
            for prong in ("1P","3P"):
                string += "\tBook(TH1F(\"%s%s\",\"%s;%s;%s\",%s, %s, %s));\n" % ( definition["name"],
                                                                                 str(prong),
                                                                                 definition["title"],
                                                                                 definition["xtitle"],
                                                                                 definition["ytitle"],
                                                                                 definition["nbinsx"],
                                                                                 definition["xlow"],
                                                                                 definition["xup"]
                                                                                 )
        
        if self.nptbins is not None and self.prongbins:
            for pt in self.nptbins:
                for prong in ("1P","3P"):
                    string += "\tBook(TH1F(\"%s%s%s\",\"%s;%s;%s\",%s, %s, %s));\n" % ( definition["name"],
                                                                                       str(pt),
                                                                                       str(prong),
                                                                                       definition["title"],
                                                                                       definition["xtitle"],
                                                                                       definition["ytitle"],
                                                                                       definition["nbinsx"],
                                                                                       definition["xlow"],
                                                                                       definition["xup"]
                                                                                       )
        elif not self.prongbins:
            string += "\tBook(TH1F(\"%s\",\"%s;%s;%s\",%s, %s, %s));\n" % ( definition["name"],
                                                                               definition["title"],
                                                                               definition["xtitle"],
                                                                               definition["ytitle"],
                                                                               definition["nbinsx"],
                                                                               definition["xlow"],
                                                                               definition["xup"]
                                                                               )

        return string

    def define1DFiller(self, definition):
        string = ""
        string2 = ""
        if self.prongbins:
            string += "\tHist((\"%s\"+prong).c_str())->Fill((*cit)->%s());\n" % (definition["name"],
                                                                                 definition["d3pdname"]
                                                                                 )
        if self.nptbins is not None and self.prongbins:
            string2 += "\tHist((\"%s\"+pTBin+prong).c_str())->Fill((*cit)->%s()); \n" % (definition["name"],
                                                                                   definition["d3pdname"]                                                                                             )
                    
        return (string,string2)

    def define3DHist(self, definition):
        if not definition["active"] == "true":
            return ""
        string = ""
        bins = self.equalBinRanges(definition)
        if self.prongbins:
            string += "\tconst double %sRanges[%s] = %s;\n" % ( definition["name"],
                                                                 str(int(definition["nbinsx"])+1),
                                                                 bins
                                                                 )
            for prong in ("1prong","3prong"):
                string += "\tBook(TH3F(\"hpdf_%s_both_v%s\",\"%s\",%s, %sRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));\n" % ( str(prong),
                                                                                                                                    definition["id"],
                                                                                                                                    definition["title"],
                                                                                                                                    definition["nbinsx"],
                                                                                                                                    definition["name"]
                                                                                                                                    )

                #print string
        #print "####################################################"
        #print string
        return string


    def define3DPDFFiller(self, definition):
        if definition["d3pdname"] == "None" or not definition["active"] == "true":
            return ""
        string = ""
        string += "\t(dynamic_cast<TH3F*>(Hist((\"hpdf_\"+prong+\"_both_v%s\").c_str())))->Fill((*cit)->%s(),(*cit)->TLV().Pt()/1000., nvtx);\n" % (definition["id"],
                                                                                                                                                  definition["d3pdname"]
                                                                                                                                                  )
        return string

    def equalBinRanges(self, definition):
        nbinsx = int(definition["nbinsx"])
        xlow = float(definition["xlow"])
        xup = float(definition["xup"])
        print "name: " + definition["name"] + "   nbinsx: " + definition["nbinsx"] + "  xlow: " + definition["xlow"] + "   xup: " + definition["xup"]
        i = 0
        string = "{"
        for i in range(nbinsx+1):
            val = xlow + i*(xup-xlow)/nbinsx;
            string = string + str(val) + ","
        string = (string.rstrip(",")) + "}"
        return string

if __name__ == '_main__':
    pass
