import xml.sax as sax

class DictHandler(sax.handler.ContentHandler):

    def __init__(self): 
        self.ergebnis = {} 
        self.name = ""
        self.id = ""
        self.nbinsx = ""
        self.xrange = ""
        self.xlow = ""
        self.xup = ""
        self.yrange = ""
        self.ylow = ""
        self.yup = ""
        self.zrange = ""
        self.zlow = ""
        self.zup = ""
        self.xtitle = ""
        self.ytitle = "Normalised"
        self.ztitle = ""
        self.title = ""
        self.d3pdname = ""
        self.defvar = ""
        self.active = ""
        self.aktiv = None 

    def startElement(self, name, attrs): 
        if name == "variable": 
            self.name = ""
            self.nbinsx = ""
            self.title = ""
            self.d3pdname = ""
            self.id = ""
            self.xrange = ""
            self.defvar = ""
            self.active = ""
        elif name == "name" or name == "nbinsx" or name == "title" or name == "xrange" or name == "d3pdname" or name == "id" or name == "defvar" or name == "active": 
            self.aktiv = name 

    def endElement(self, name): 
        if name == "variable": 
            if self.d3pdname == "":
                self.d3pdname += self.name
            self.ergebnis[self.name] = {
                "name":self.name,
                "id":self.id,
                "nbinsx":self.nbinsx,
                "title":self.title,
                "xrange":self.xrange,
                "xlow":self.xlow,
                "xup":self.xup,
                "yrange":self.yrange,
                "ylow":self.ylow,
                "yup":self.yup,
                "zrange":self.zrange,
                "zlow":self.zlow,
                "zup":self.zup,
                "xtitle":self.xtitle,
                "ytitle":self.ytitle,
                "ztitle":self.ztitle,
                "d3pdname":self.d3pdname,
                "defvar":self.defvar,
                "active":self.active
                }
        elif name == "name" or name == "title":  
            self.aktiv = None

    def characters(self, content): 
        if self.aktiv == "name":
            self.name += content.strip()
        elif self.aktiv == "id":
            self.id += content.strip()
        elif self.aktiv == "nbinsx":
            self.nbinsx += content.strip()
        elif self.aktiv == "title":
            self.title += content.strip()
        elif self.aktiv == "d3pdname":
            self.d3pdname += content.strip()
        elif self.aktiv == "defvar":
            self.defvar += content.strip()
        elif self.aktiv == "active":
            self.active += content.strip()
        elif self.aktiv == "xrange":
            self.xrange += content.strip()
            try:
                self.xlow = (self.xrange.split()[0]).strip('"')
                self.xup = (self.xrange.split()[1]).strip('"')
            except IndexError:
                print "No proper xrange definition given; Setting to default and continue"
                self.xlow = "0"
                self.xup = "100"
        elif self.aktiv == "yrange":
            self.yrange += content.strip()
            if self.ylow == "" and self.yup == "":
                try:
                    self.ylow += (self.yrange.split()[0]).strip()
                    self.yup += self.yrange.split()[1]
                except IndexError:
                    print "No proper yrange definition given; Setting to default and continue"
                    self.ylow = "0"
                    self.yup = "100"
        elif self.aktiv == "zrange":
            self.zrange += content.strip()
            if self.zlow == "" and self.zup == "":
                try:
                    self.zlow += (self.zrange.split()[0]).strip()
                    self.zup += self.zrange.split()[1]
                except IndexError:
                    print "No proper zrange definition given; Setting to default and continue"
                    self.zlow = "0"
                    self.zup = "100"


def load_dict(dateiname): 
    handler = DictHandler() 
    parser = sax.make_parser() 
    parser.setContentHandler(handler) 
    parser.parse(dateiname) 
    return handler.ergebnis

if __name__=='__main__':
    ergebnis = load_dict('variables.xml')
