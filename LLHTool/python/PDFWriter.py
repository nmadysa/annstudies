#!/usr/bin/env python

import ROOT

def initDirStructure():
    outFile = ROOT.TFile.Open("test.root", "RECREATE")
    outFile.cd()
    prongdirs=("1prong","3prong")
    authordirs=("both","calo")
    for iPr in prongdirs:
        tdir = outFile.mkdir(iPr,iPr)
        tdir.cd()
        for iAuthor in authordirs:
            tdir.mkdir(iAuthor, iAuthor)
    tdir.cd()
    return outFile

def copy(src,dest):
    tdir = src.GetDirectory("/PDFPlotter/") #src.cd("/TauSelector/")
    tdir.pwd()
    tdir.cd()
    
    for obj in tdir.GetListOfKeys():
        if(obj.GetName().find("1prong") is not -1):
            if(obj.GetName().find("both") is not -1):
                print (obj.ReadObj()).GetEntries()
                print "Will copy hist"
                destdir = dest.GetDirectory("/1prong/both/")
                destdir.cd()
                clone = (obj.ReadObj()).Clone()
                clone.Write()
                del clone
        #pass
    
if __name__ == '__main__':
    pdffile = initDirStructure()
    inFile = ROOT.TFile.Open("../out/Variables/PDFCycle.mc.Ztautau.root")
    copy(inFile, pdffile)
