#!/usr/bin/env python

import sys
import shutil
import os

class CParser(object):
    def __init__(self, template="VariablePlotter_template.cxx", dest="test.cxx"):
        self.f = None
        self.fOut = None
        self.wasOpen = False
        self.template = template
        self.destpath = ""
        self.dest = self.destpath+dest
        self.patternlist = []

    def open(self):
        self.fOut = open("tmp.cxx",'w+')
        if self.wasOpen:
            self.f = open("tmpdest.cxx",'r')
        else:
            shutil.copyfile(self.template,"tmp_template.cxx")
            self.f = open("tmp_template.cxx",'r')
            self.clearFile()
            self.fOut.close()
            shutil.copyfile("tmp.cxx","tmpdest.cxx")
            self.wasOpen = True
        
    def clone(self):
        pass

    def close(self):
        self.f.close()
        self.fOut.close()
        shutil.copyfile("tmp.cxx","tmpdest.cxx")

    def cleanup(self):
        if os.path.exists("tmpdest.cxx"):
            os.remove("tmpdest.cxx")
        if os.path.exists("tmp.cxx"):
            os.remove("tmp.cxx")
        if os.path.exists("tmp_template.cxx"):
            os.remove("tmp_template.cxx")
    def clearFile(self):
        if self.f == None:
            print "No source file opened. Giving up..."
            sys.exit()
        start = False
        for line in self.f:
            if(start and not (line.strip()).startswith("//cgen::end")):
                continue
            elif(start):
                start = False
            self.fOut.write(line)
            if (line.strip()).startswith("//cgen::start"):
                self.patternlist.append(((line.strip()).strip("//cgen::start")).strip())
                start = True                

    def getPatterns(self):
        self.open()
        return self.patternlist
    
    def parseAndAdd(self, pattern="", string=""):
        self.open()
        if self.f == None:
            print "No source file opened. Giving up..."
            sys.exit()
        if pattern == "":
            print "No matching pattern specified"
            sys.exit()
        for line in self.f:
            self.fOut.write(line)
            if (line.strip()).startswith("//cgen::start") and line.find(pattern) is not -1:
                self.fOut.write(string)
        self.close()

if __name__ == "__main__":
    pass
