#!/bin/bash

# Ztautau
sh launch_skim.sh MC group10.perf-tau.mc10_7TeV.106052.PythiaZtautau.e574_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM/ .Skim.v5

#ZPrime
sh launch_skim.sh MC group10.perf-tau.mc10_7TeV.107381.Pythia_Zprime_tautau_SSM250.e632_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh MC group10.perf-tau.mc10_7TeV.107382.Pythia_Zprime_tautau_SSM500.e632_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh MC group10.perf-tau.mc10_7TeV.107383.Pythia_Zprime_tautau_SSM750.e632_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh MC group10.perf-tau.mc10_7TeV.107385.Pythia_Zprime_tautau_SSM1250.e632_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM/  .Skim.v5

#Wtaunu
sh launch_skim.sh MC group10.perf-tau.mc10_7TeV.107054.PythiaWtaunu_incl.e574_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM/  .Skim.v5