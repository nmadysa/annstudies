#!/bin/bash

# JetTauEtMiss stream
sh launch_skim.sh DATA group10.perf-tau.00179710.JetTauEtmiss-AOD.f361_m796.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00179725.JetTauEtmiss-AOD.f361_m796.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00179739.JetTauEtmiss-AOD.f361_m801.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00179771.JetTauEtmiss-AOD.f362_m801.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00179938.JetTauEtmiss-AOD.f365_m806.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00179939.JetTauEtmiss-AOD.f366_m806.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00180124.JetTauEtmiss-AOD.f368_m806.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00180144.JetTauEtmiss-AOD.f368_m806.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00180149.JetTauEtmiss-AOD.f368_m806.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00180153.JetTauEtmiss-AOD.f368_m806.01-01-02.D3PD_TauMEDIUM/ .Skim.v5
sh launch_skim.sh DATA group10.perf-tau.00180164.JetTauEtmiss-AOD.f368_m806.01-01-02.D3PD_TauMEDIUM/ .Skim.v5

