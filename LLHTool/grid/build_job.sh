#!/bin/bash

# unpack all archives
for i in $(ls *.tar)
do
  echo "unpacking $i"
  tar -xf $i
done

# source setup script
source setup.sh

# set boost path
echo "setting BOOST paths"
export BOOST_INC=$SITEROOT/sw/lcg/external/Boost/1.44.0_python2.6/i686-slc5-gcc43-opt/include/boost-1_44/
export BOOST_LIB=$SITEROOT/sw/lcg/external/Boost/1.44.0_python2.6/i686-slc5-gcc43-opt/lib/
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BOOST_LIB
echo "BOOST_INC = \"$BOOST_INC\""
echo "BOOST_LIB = \"$BOOST_LIB\""

# compile all needed packages
echo "compiling..."
echo "...core/..."
cd core/; make; cd -;

echo "...TauCommon/..."
cd TauCommon/; make; cd -;

echo "...Tools/GoodRunsLists/..."
cd Tools/GoodRunsLists/; make; cd -;

echo "...Tools/PileupReweighting/..."
cd Tools/PileupReweighting/; make; cd -;

echo "...SFrame/..."
cd SFrame/; make; cd -;

echo "...Tools/CommonSkim/..."
cd Tools/CommonSkim/; make; cd -;

echo "...LLHTool..."
cd LLHTool; make; cd -;

