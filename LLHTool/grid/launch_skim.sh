#!/bin/bash

TYPE="$1"
INDS="$2"
VER="$3"

if [ "$TYPE" == "MC" ]
then
    sh launch_job.sh SkimMCGrid.xml "input/InputTemp.xml" "$VER" "$INDS" "SkimCycle.mc.Skim.root"
elif [ "$TYPE" == "DATA" ]
then
    sh launch_job.sh SkimDataGRID.xml "input/InputTemp.xml" "$VER" "$INDS" "SkimCycle.data.Skim.root"
fi