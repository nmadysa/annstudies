#!/bin/bash

XML=$1
INPUT=$2
POSTFIX=$3
INDS=$4
OUTROOT=$5
temp=${INDS#group10.*\.}
OUTDS="user.morgenst.${temp%\.D3PD_Tau*/}$POSTFIX"

prun --exec "sh my_job.sh \"$XML\" \"$INPUT\" \"%IN\"" --outDS "$OUTDS" --inDS "$INDS" --athenaTag=17.0.0 --outputs build_log,$OUTROOT --nFilesPerJob=10 --extFile "*.tar"
