#ifndef CommonSkimCycle_CXX
#define CommonSkimCycle_CXX

// system include(s)
#include "assert.h"
#include <set>

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

// ROOT include(s)
#include "TTree.h"
#include "TError.h"
#include "TBranch.h"
#include "TBranchElement.h"

// core include(s)
#include "Region.h"
#include "Utilities.h"
#include "BoundHistogram.h"

// TauCommon include(s)
#include "TauSelector.h"
#include "Helpers.h"
#include "TrigDecisionTool.h"

// LLHTool include(s)
#include "CommonSkimCycle.h"

// GoodRunList
#include "GoodRunsLists/TGoodRunsListReader.h"

ClassImp(CommonSkimCycle);

using namespace TauCommon_Helpers;

CommonSkimCycle::CommonSkimCycle(CycleBase* pParent, const char* sName) :
ToolBase(pParent, sName) {
    // make ROOT ignore level independent from SFrame level 
    gErrorIgnoreLevel = kWarning;
    //SetErrorHandler(DefaultErrorHandler);
   
    DeclareProperty("input_trig_conf_tree_name", c_trig_conf_in_tree_name);
    DeclareProperty("output_trig_conf_tree_name", c_trig_conf_out_tree_name);
}

CommonSkimCycle::~CommonSkimCycle() {
    // clear memory
}

namespace {

    const char* findRootType(const char* root_type) {
        //
        // Try for all the known ROOT types:
        //
        if (!strcmp(root_type, "Char_t")) {
            return "B";
        } else if (!strcmp(root_type, "UChar_t")) {
            return "b";
        } else if (!strcmp(root_type, "Short_t")) {
            return "S";
        } else if (!strcmp(root_type, "UShort_t")) {
            return "s";
        } else if (!strcmp(root_type, "Int_t")) {
            return "I";
        } else if (!strcmp(root_type, "UInt_t")) {
            return "i";
        } else if (!strcmp(root_type, "Float_t")) {
            return "F";
        } else if (!strcmp(root_type, "Double_t")) {
            return "F";
        } else if (!strcmp(root_type, "Long64_t")) {
            return "L";
        } else if (!strcmp(root_type, "ULong64_t")) {
            return "l";
        } else if (!strcmp(root_type, "Bool_t")) {
            return "O";
        }

        // Return something dummy...
        return "I";
    }
}

std::pair<int, std::pair<int, int> >
CommonSkimCycle::MakeTrigDBKeys(Int_t smk, Int_t l1psk, Int_t hltpsk) const {
    return std::make_pair(smk, std::make_pair(l1psk, hltpsk));
}

std::set<std::pair<int, std::pair<int, int> > >
CommonSkimCycle::GetExistingTrigConfigs(TTree* target) const {
    // The result object:
    std::set< std::pair<int, std::pair<int, int> > > result;

    // The branches used to read the configuration keys:
    TBranch *b_smk = 0, *b_l1psk = 0, *b_hltpsk = 0;

    // Check whether the branches even exist:
    if ((!(b_smk = target->GetBranch("SMK"))) ||
            (!(b_l1psk = target->GetBranch("L1PSK"))) ||
            (!(b_hltpsk = target->GetBranch("HLTPSK")))) {
        m_logger << DEBUG << "Target tree still empty" << SLogger::endmsg;
        return result;
    }

    // Activate the branches:
    target->SetBranchStatus("SMK", 1);
    target->SetBranchStatus("L1PSK", 1);
    target->SetBranchStatus("HLTPSK", 1);

    // Connect the branches:
    Int_t smk = 0, l1psk = 0, hltpsk = 0;
    target->SetBranchAddress("SMK", &smk, &b_smk);
    target->SetBranchAddress("L1PSK", &l1psk, &b_l1psk);
    target->SetBranchAddress("HLTPSK", &hltpsk, &b_hltpsk);

    // Now extract the needed information:
    for (Int_t i = 0; i < target->GetEntries(); ++i) {

        // Load the entry:
        b_smk->GetEntry(i);
        b_l1psk->GetEntry(i);
        b_hltpsk->GetEntry(i);
        // Add it to the result:
        result.insert(MakeTrigDBKeys(smk, l1psk, hltpsk));
    }

    // Let go of these branches:
    b_smk->ResetAddress();
    b_l1psk->ResetAddress();
    b_hltpsk->ResetAddress();

    return result;
}

void CommonSkimCycle::CopyTrigConfTree() {
    // Access the input and output metadata trees. The functions will throw
    // an exception in case of problems, so there's no need to check their
    // return value.
    TTree* source = GetInputMetadataTree(c_trig_conf_in_tree_name.c_str());
    TTree* target = GetOutputMetadataTree(c_trig_conf_out_tree_name.c_str());

    REPORT_VERBOSE("Copying trigger configuration metadata tree (name="
            << c_trig_conf_out_tree_name << ")");

    // Check which configurations are already part of the target tree:
    std::set< std::pair<int, std::pair<int, int> > > existingConfigs = GetExistingTrigConfigs(target);

    // Transient object pointers used in the copying. I use an std::list instead
    // of std::vector, as lists are guaranteed not to move their elements around.
    // This is important because we have to pass the location of some of these
    // pointers to the transient TTree.
    std::list< void* > branch_objects;

    // Transient configuration keys:
    Int_t *smk = 0, *l1psk = 0, *hltpsk = 0;

    //
    // Loop over all the branches of the trigger configuration tree:
    //
    TObjArray* branches = source->GetListOfBranches();
    for (Int_t i = 0; i < branches->GetSize(); ++i) {

        // Investigate this branch:
        TObject* obj = branches->At(i);
        if (!obj) continue;
        const char* br_type = obj->IsA()->GetName();

        // If this is a primitive branch:
        if (!strcmp(br_type, "TBranch")) {

            // Access the exact branch object:
            TBranch* sbranch = dynamic_cast<TBranch*> (obj);
            if (!sbranch) {
                REPORT_ERROR("Couldn't perform dynamic cast to TBranch");
            }
            REPORT_VERBOSE("Found primitive branch with name: "
                    << sbranch->GetName());

            // Check if a branch with this name already exists in the output
            // tree:
            TBranch* tbranch = target->GetBranch(sbranch->GetName());
            if (!tbranch) {

                // Get the primitive type from the branch's only leaf:
                TObjArray* leaves = sbranch->GetListOfLeaves();
                for (Int_t j = 0; j < leaves->GetSize(); ++j) {

                    // See if this is the leaf that we're looking for:
                    TLeaf* sleaf = dynamic_cast<TLeaf*> (leaves->At(j));
                    if (!sleaf) continue;
                    REPORT_VERBOSE("   The variable type is: " << sleaf->GetTypeName());

                    // Create the output branch:
                    std::ostringstream leaflist;
                    leaflist << sbranch->GetName() << "/" << findRootType(sleaf->GetTypeName());
                    tbranch = target->Branch(sbranch->GetName(), 0, leaflist.str().c_str());
                    if (!tbranch) {
                        REPORT_ERROR("Couldn't create branch: " << sbranch->GetName()
                                << ", " << leaflist.str());
                    }

                    break;
                }
            }

            // Find the (single) leaf for the branch:
            TLeaf* tleaf = tbranch->GetLeaf(sbranch->GetName());
            if (!tleaf) {
                REPORT_ERROR("Couldn't find leaf in output branch");
            }

            // Get the pointer to the output variable:
            void* ptr = tleaf->GetValuePointer();

            // Pick out the configuration keys:
            if (!strcmp(sbranch->GetName(), "SMK")) {
                smk = reinterpret_cast<Int_t*> (ptr);
            } else if (!strcmp(sbranch->GetName(), "L1PSK")) {
                l1psk = reinterpret_cast<Int_t*> (ptr);
            } else if (!strcmp(sbranch->GetName(), "HLTPSK")) {
                hltpsk = reinterpret_cast<Int_t*> (ptr);
            }

            // Create a "permanent" pointer to this object:
            branch_objects.push_back(ptr);
            // Connect the transient TTree to this newly created object:
            sbranch->SetAddress(branch_objects.back());

        }            // If this is an object branch:
        else if (!strcmp(br_type, "TBranchElement")) {

            // Access the exact branch object:
            TBranchElement* sbranch = dynamic_cast<TBranchElement*> (obj);
            if (!sbranch) {
                REPORT_ERROR("Couldn't perform dynamic cast to TBranchElement");
            }

            REPORT_VERBOSE("Found an object branch with name: " << sbranch->GetName()
                    << " and type: " << sbranch->GetClassName());

            // Check if a branch with this name already exists in the output
            // tree:
            TBranch* tbranch = target->GetBranch(sbranch->GetName());
            if (!tbranch) {

                // Create the branch"
                tbranch = target->Bronch(sbranch->GetName(), sbranch->GetClassName(), 0);
                if (!tbranch) {
                    REPORT_ERROR("Couldn't create branch: " << sbranch->GetName()
                            << ", " << sbranch->GetClassName());
                }
            }

            TBranchElement* bre = dynamic_cast<TBranchElement*> (tbranch);
            if (!bre) {
                REPORT_ERROR("Unexpected branch type created for "
                        << sbranch->GetName());
            }

            // Get the pointer to the output variable:
            void* ptr = bre->GetObject();

            // Create a "permanent" pointer to this object:
            branch_objects.push_back(ptr);
            // Connect the transient TTree to this newly created object:
            sbranch->SetAddress(&branch_objects.back());

        } else {
            m_logger << WARNING << "Unknown branch type encountered: "
                    << br_type << SLogger::endmsg;
        }
    }

    m_logger << DEBUG << "Finished setting up the metadata tree variables"
            << SLogger::endmsg;

    // Check if we've found all the configuration key variables:
    if ((!smk) || (!l1psk) || (!hltpsk)) {
        REPORT_ERROR("Didn't find all the configuration key variables");
    }

    //
    // Copy every entry from the source tree to the target tree:
    //
    for (int i = 0; i < source->GetEntries(); ++i) {

        // Get the entry from the source:
        source->GetEntry(i);

        // Check if this configuration is already part of the target tree:
        if (existingConfigs.find(MakeTrigDBKeys(*smk, *l1psk, *hltpsk)) !=
                existingConfigs.end()) continue;

        // If it's not there yet, copy it over:
        target->Fill();
        m_logger << INFO << "Copied trigger configuration: SMK: " << *smk << ", L1PSK: "
                << *l1psk << ", HLTPSK: " << *hltpsk << SLogger::endmsg;
    }

    //
    // Release the branches of the source tree:
    //
    for (Int_t i = 0; i < branches->GetSize(); ++i) {

        TObject* obj = branches->At(i);
        if (!obj) continue;

        TBranch* branch = dynamic_cast<TBranch*> (obj);
        if (!branch) continue;

        branch->ResetAddress();
    }

    return;
}

#endif // CommonSkimCycle_CXX

