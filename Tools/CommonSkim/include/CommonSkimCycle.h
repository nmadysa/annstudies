#ifndef CommonSkimCycle_H
#define CommonSkimCycle_H

// STL include(s)
#include <string>
#include <set>

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "ToolBase.h"

class CommonSkimCycle : public ToolBase
{
public:
  /// standard constructor
  CommonSkimCycle(CycleBase* pParent,const char* sName);
  /// standard destructor
  virtual ~CommonSkimCycle();
  
  /// hook in SFrame event loop  
  void CopyTrigConfTree();
  std::pair<int,std::pair<int,int> > MakeTrigDBKeys(int,int,int) const;
  std::set<std::pair<int,std::pair<int,int> > > GetExistingTrigConfigs(TTree* target) const;

protected:
  std::string                               c_trig_conf_in_tree_name;
  std::string                               c_trig_conf_out_tree_name;

  ClassDef(CommonSkimCycle,0);
};

#endif // CommonSkimCycle_H
