#!/bin/bash

# expect name of output directory
if [ $# -ne 2 ]
then
  echo "unexpected number of parameters (should be 2)"
  echo "Example: sh grid_helper.sh /home/wahrmund/workarea/Release17/Validation/OfflineLLH/grid/ /home/wahrmund/workarea/Release17/Validation/OfflineLLH/"
  exit 1
fi

# output directory
OUTDIR=`cd $1; pwd`
CUSTOMPACKAGES="$2"

if [ -d "$1" ]
then
  echo "move archives to directory \"$1\""
else
  echo "\"$1\" is not a directory"
  exit 1
fi


# create archives for common packages and move to output directory
PACKAGE_DIR="../../"
PACKAGES=" SFrame/ core/ D3PDObjects/  AnalysisUtilities/ TauCommon/ D3PDReaderObjects/"
OLD_DIR=$(pwd)

#clean up temporary job output
cd $CUSTOMPACKAGES/config/
rm -rf jobTempOutput*

cd $PACKAGE_DIR

TARNAME="ExternalTools.tar"
echo "creating \"$TARNAME\" from \"ExternalTools\""
tar -chf $TARNAME --exclude=".svn" --exclude=".bzr" --exclude="obj" --exclude="lib" --exclude="out" --exclude="root" --exclude="macros" "ExternalTools"
mv $TARNAME $OUTDIR

for i in $PACKAGES
do
  TARNAME="$(basename $i).tar"
  echo "creating \"$TARNAME\" from \"$i\""
  tar -chf $TARNAME --exclude=".svn" --exclude=".bzr" --exclude="obj" --exclude="lib" --exclude="out" --exclude="root" --exclude="macros" $i
  mv $TARNAME $OUTDIR
done

# copy setup script as well
cp setup.sh $OUTDIR

cd $OLD_DIR

# create archives for custom packages
for j in $CUSTOMPACKAGES
do
  if [ -d "$j" ]
  then
    cd $j/../
    TARNAME="$(basename $j).tar"
    echo "creating \"$TARNAME\" from \"$j\""
    tar -chf $TARNAME --exclude=".svn" --exclude=".bzr" --exclude="obj" --exclude="out" --exclude="root" --exclude="macros" --exclude="$(basename ${OUTDIR%/}*)" $(basename $j)
    mv $TARNAME $OUTDIR
    cd $OLD_DIR
  else
    echo "directory \"$j\" not found"
    exit 1
  fi
done

# create grid executable
echo "creating executable for prun"
EXEC="build_job.sh"

if [ -f "$EXEC" ]
then
  rm -v "$EXEC"
fi

echo '#!/bin/bash'                                                                                 >> $EXEC
echo                                                                                               >> $EXEC
echo '# unpack all archives'                                                                       >> $EXEC
echo 'for i in $(ls *.tar)'                                                                        >> $EXEC
echo 'do'                                                                                          >> $EXEC
echo '  echo "unpacking $i"'                                                                       >> $EXEC
echo '  tar -xf $i'                                                                                >> $EXEC
echo 'done'                                                                                        >> $EXEC
echo                                                                                               >> $EXEC
echo '# source setup script'                                                                       >> $EXEC
echo 'source setup.sh'                                                                             >> $EXEC
echo                                                                                               >> $EXEC
echo '# set boost path'                                                                            >> $EXEC
echo 'echo "setting BOOST paths"'                                                                  >> $EXEC
# echo 'export BOOST_INC=$SITEROOT/sw/lcg/external/Boost/1.44.0_python2.6/i686-slc5-gcc43-opt/include/boost-1_44/'>> $EXEC
# echo 'export BOOST_LIB=$SITEROOT/sw/lcg/external/Boost/1.44.0_python2.6/i686-slc5-gcc43-opt/lib/'>> $EXEC
# echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BOOST_LIB'                                          >> $EXEC
echo 'export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase'                   >> $EXEC
echo 'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'                                     >> $EXEC
echo 'localSetupBoost boost-1.55.0-python2.7-x86_64-slc6-gcc48'                                    >> $EXEC
echo 'export BOOST_INC=${boostLibDir/lib/include}   # replcace "lib" to "include"'                 >> $EXEC
echo 'export BOOST_LIB=${boostLibDir}'                                                             >> $EXEC
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BOOST_LIB '                                         >> $EXEC

echo 'echo "BOOST_INC = \"$BOOST_INC\""'                                                           >> $EXEC
echo 'echo "BOOST_LIB = \"$BOOST_LIB\""'                                                           >> $EXEC
echo                                                                                               >> $EXEC
echo '# compile all needed packages'                                                               >> $EXEC
echo 'echo "compiling..."'                                                                         >> $EXEC

echo "cd ExternalTools; make; cd -; # source ExternalTools/RootCore/scripts/setup.sh;
  "               >> $EXEC

for k in $PACKAGES
do
  echo "echo \"...$k...\""                                                                         >> $EXEC
  echo "cd $k; make distclean; make; cd -;"                                                        >> $EXEC
  echo                                                                                             >> $EXEC
done

for l in $CUSTOMPACKAGES
do
  echo "echo \"...$(basename $l)...\""                                                             >> $EXEC
  echo "cd $(basename $l); make distclean; make -f Makefile.SKIM; cd -;"                           >> $EXEC
  echo                                                                                             >> $EXEC
done

echo "move/copy prun scripts to \"$OUTDIR\""
mv $EXEC $OUTDIR
cp run_job.sh $OUTDIR

echo "done"

exit 0
