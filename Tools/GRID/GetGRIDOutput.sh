#!/bin/bash

# check input parameters
if [ $# -lt 2 ]
then
  echo "expect 2 parameter: sh GetGRIDOutput.sh <outputdir> <dq2-dataset>"
  exit 1;
fi

echo
echo "Retrieving GRID Output"
echo "======================"
echo

# check for output directory
if [ ! -d "$1" ]
then
  echo "creating output directory \"$1\""
  mkdir -p "$1"
fi

echo "using output directorty \"$1\""
OUTDIR=$1

echo "look for dataset \"$2\""
DATASET=$2

echo "using dq2-get options \"$3\""
DQ2OPT="$3"

echo
echo start query: dq2-get $DQ2OPT $DATASET
echo

# download dataset
dq2-ls -fH  $DATASET | grep .root > download.log
cat download.log | while read i ; do
    FILE=$(echo $i | awk '{print $3}')
    DQ2SIZE=$(echo $i | awk '{print $6}')
    #check if file already exists
    if [ -f $OUTDIR/$FILE ]; then
	LOCALSIZE=$(du -m  $OUTDIR/$FILE | awk '{print $1}').00
	diff=$(echo $DQ2SIZE $LOCALSIZE | awk '{print 10000*((($1 - $2) /$2)*(($1 - $2) /$2))}')
	#if file exists, check if local file size is consistent with dq2 size within 1%
	if [ $(echo "if ($diff <  1) 1 else 0" | bc) -eq 1 ] ;then
	    continue
	else
	    dq2-get -f $FILE $DATASET
	fi
    else
	dq2-get -f $FILE $DATASET
    fi
done

if [ -f download.log ]; then
    rm download.log
fi

# scan all folders which match the dataset name and collect their content in output dir
# remove empty folders afterwards
for i in $(find . -iname "${DATASET%/}.*")
do
  echo "cleaning directory $i"
  if [ -n "$(ls $i)" ]
  then
    mv -v $(basename $i)/* "$OUTDIR"
  else
    echo "$i is empty"
  fi
  if [ -z "$(ls $i)" ]
  then
    rm -rv $i
  else
    echo "ERROR: $i is not empty -> not removed"
  fi
done

# hadd all root files
if [ -n "$ROOTSYS" ]
then
  cd $OUTDIR

  if [ -n "$(ls *root*)" ]
  then
    hadd ${DATASET%/}.root *root*
    if [ -e ${DATASET%/}.root ]
    then
      root -b -q ${DATASET%/}.root "dump_config.cxx(\"${DATASET%/}.log\")"
    fi
  fi
fi

echo 
echo "DONE"
echo
