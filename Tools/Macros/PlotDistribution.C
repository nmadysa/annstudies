#include <vector>
#include <map>
#include <string>

#include "TH1.h"
#include "TFile.h"
#include "TLegend.h"
#include "THStack.h"

#include "myTypes.h"
#include "Helpers.C"
#include "Samples.C"

using namespace std;

void PlotDistribution(string sDist,string sPath,string sCycleName,const vector<Dataset>& vMCSamples,float fLsoll)
{
  const string sPrefix = sCycleName + ".";
  const string sPostfix = ".root";
  
  // helper variables
  TFile* f = 0;
  TH1* h = 0;
  string sFileName;
  Dataset dCurDS;
  float fLumiWeight;
  long int iMCEvents;
  map<string,TH1*> mMap;
  TLegend l;
  string sXTitle, sYTitle;

  // loop over all samples
  for(unsigned int i = 0; i < vMCSamples.size(); ++i)
  {
    // compose filename
    dCurDS = vMCSamples.at(i);
    sFileName = sPath + "/" + sPrefix + dCurDS.ShortName + sPostfix;

    // open file
    f = TFile::Open(sFileName.c_str());
    if(!f)
    {
      cout << "skip file: " << sFileName << endl;
      continue;
    }

    // retrieve distribution
    h = (TH1*)f->Get(sDist.c_str());
    if(!h)
    {
      cout << "skip distribution \"" << sDist << "\" for file " << sFileName << endl;
      f->Close();
      continue;
    }

    if(sXTitle.empty())
      sXTitle = h->GetXaxis()->GetTitle();
    if(sYTitle.empty())
      sYTitle = h->GetYaxis()->GetTitle();

    // scale to luminosity
    iMCEvents = GetMCEvents(f,"BaselineSelection/h_cut_flow_raw");
    if(iMCEvents)
    {
      if(iMCEvents != dCurDS.GeneratedEvents)
	cout << "Warning: processed events = " << iMCEvents << ", generated events (AMI) = " << dCurDS.GeneratedEvents << endl;
      fLumiWeight = GetLumiWeight(dCurDS,iMCEvents,fLsoll);
    }
    else
    {
      f->Close();
      continue;
    }

    h->Scale(fLumiWeight);

    // add all hsitograms of same type
    if(mMap.find(dCurDS.type) != mMap.end())
      mMap[dCurDS.type]->Add(mMap[dCurDS.type],h);
    else
    {
      mMap[dCurDS.type] = (TH1*)h->Clone();
      mMap[dCurDS.type]->SetDirectory(0);
      mMap[dCurDS.type]->SetFillColor(dCurDS.color);
      l.AddEntry(mMap[dCurDS.type],dCurDS.label.c_str(),"f");
    }

    // close the file;
    f->Close();
    f = 0;
    h = 0;
  }

  // look for data
  TH1* data = 0;
  sFileName = sPath + "/" + sPrefix + "data" + sPostfix;
  // open file
  f = TFile::Open(sFileName.c_str());
  if(!f)
    cout << "skip file: " << sFileName << endl;
  else
  {
    // retrieve distribution
    data = (TH1*)f->Get(sDist.c_str());
    if(!data)
      cout << "skip distribution \"" << sDist << "\" for file " << sFileName << endl;
  }

  // build MC stack
  THStack* hs = new THStack("hs","Stack");
  map<string,TH1*>::iterator it = mMap.begin();
  for(; it != mMap.end(); ++it)
    hs->Add(it->second);

  hs->Draw("hist");
  hs->GetHistogram()->GetXaxis()->SetTitle(sXTitle.c_str());
  hs->GetHistogram()->GetYaxis()->SetTitle(sYTitle.c_str());
  if(data)
  {
    data->Draw("same");
    hs->SetMaximum(1.1 * max(hs->GetMaximum(),data->GetMaximum()));
  }
}
