#ifndef MY_TYPES_H
#define MY_TYPES_H

#include <string>

using namespace std;

struct Dataset
{
  // dataset information
  long int   ID;
  string     ShortName;
  string     DSName;
  double     crossx;
  double     filtereff;
  long int   GeneratedEvents;
  string     tags;

  // style and plotting
  string     label;
  string     type;
  int        color;
  int        style;
};

#endif
