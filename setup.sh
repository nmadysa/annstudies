#!/bin/bash
#-----------------------------------------------------------------------------
# file:     AnalysisModul/setup.sh
# author:   Christian Gumpert <cgumpert@cern.ch>
# date :    2010-11-17
#------------------------------------------------------------------------------
# edited:   Nico Madysa <nico.madsa@tu-dresden.de>
# date :    2015-02-12
#-----------------------------------------------------------------------------

# Get path to this file via $BASH_SOURCE or via $0 if that fails.
# Then resolve symbolic links 
sframe_superdir=`dirname $(echo $(readlink -f ${BASH_SOURCE[0]:-$0}))`

#------------------------------------------------------------------------------
# verify paths and convert to absolute paths
#------------------------------------------------------------------------------

sframe_dir="${sframe_superdir}/SFrame"
if [ ! -d "${sframe_dir}" ]; then
    echo "  SFrame directory not found." 1>&2
    echo "  Please see the README for instructions." 1>&2
    # Exit both when sourced and called in a sub-shell.
    return 2>/dev/null || exit 1
#~ else
    #~ sframe=`cd ${sframe_dir}; pwd`  # Probably unnecessary.
fi

#------------------------------------------------------------------------------
# setup SFrame
#------------------------------------------------------------------------------

#  echo "  Setting up SFrame..." 1>&2

cwd=${PWD}
cd "${sframe_dir}"
source setup.sh >/dev/null 2>&1
cd "${cwd}"
export PYTHONPATH="$PYTHONPATH:${sframe_superdir}/TauCommon/macros"
export PYTHONPATH="$PYTHONPATH:${sframe_superdir}/GridSearch"
#  echo "  AnalysisModul setup done." 1>&2


