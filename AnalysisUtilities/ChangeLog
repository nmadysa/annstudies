2013-02-12  Christian Gumpert  <cgumpert@cern.ch>

	* tag version AnalysisUtilities-12-00-22
	* migrate enumeration type for jet collection to Constants.h

2013-02-04  Christian Gumpert  <cgumpert@cern.ch>

	* tag version AnalysisUtilities-12-00-21 (compatible with core-01-00-14 and higher)
	* tools now have CycleBase as direct parent type

2013-01-31  Christian Gumpert  <cgumpert@cern.ch>

	* (Truth)TauSelector: hide for SMWZ analyses

2013-01-30  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/BaseTauIDCut.h (TauIDCut): fix tau eta bug

2013-01-24  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* src/TrigDecisionTool.cxx (TrigDecisionTool): add tau125_medium1

2013-01-17  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/ElQualityCut.h (>): hide IsMultiLepton for TauD3PDs as variables are missing

2013-01-07  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/BaseTauIDCut.h (TauIDCut): re-implement D3PD style ID cuts

2012-12-12  Christian Gumpert  <cgumpert@cern.ch>

	* tag version AnalysisUtilities-12-00-19
	* muon selector: add flag for using unbiased IP variables (SMWZ D3PD only)

2012-11-27  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/BaseTauIDCut.h (TauIDCut): finally commit it

2012-11-28  Christian Gumpert  <cgumpert@cern.ch>

	* include/Cuts/ElZ0ThetaCut.h
	(class ElZ0ThetaCut): fix problem with uninitialised member variable

2012-11-27  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/BaseTauIDCut.h (TauIDCut): implement parametrised tau BDT electron veto cuts

2012-11-15  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* src/JetCleaningTool.cxx (CheckHotTileProblem): add explicit cast to int

2012-11-08  Christian Gumpert  <cgumpert@cern.ch>

	* tag version AnalysisUtilities-12-00-14

2012-11-07  Christian Gumpert  <cgumpert@cern.ch>

	* src/JetCleaningTool.cxx
	(DoJetCleaning): run number no longer as input needed --> interface change

2012-11-02  Christian Gumpert  <cgumpert@cern.ch>

	* tag version AnalysisUtilities-12-00-13
	* src/Utilities.cxx (GetFullPathName): add another version for getting the full path directly

2012-11-02  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* src/Utilities.cxx (SetFullPathName): check if it is already a absolute path

	* include/Utilities.h (Utilities): pass map as reference

2012-11-01  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Utilities.icc (DeleteMap): add DeleteMap

2012-10-31  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Utilities.h (Utilities): move SetFullPathName to Utilities

2012-10-31  Christian Gumpert  <cgumpert@cern.ch>

	* tag version 12-00-10
	* src/JetCleaningTool.cxx
	(CheckHotTileProblem): separate hot tile problem from MET cleaning, use calibrated jet eta/phi

2012-10-23  Christian Gumpert  <cgumpert@cern.ch>

	* tag version 12-00-09
	* src/JetCleaningTool.cxx
	(CheckFCalProblem):  use normal jet eta/phi
	(HotTile): use normal jet eta/phi

2012-09-17  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	*tag version 12-00-08
	* include/JetCleaningTool.h (ToolBase): implement FCal problem 

2012-09-11  Christian Gumpert  <cgumpert@cern.ch>

	* tag version 12-00-07
	* src/MuonSelector.cxx: bugfix in MuonSelector

	* tag version 12-00-06
	* src/JetSelector.cxx (BeginInputData) 
	* include/Cuts/JVFCut.h
	(class JVFCut): re-introduce eta requirement for JVF cut

	* tag version 12-00-05
	* src/MuonSelector.cxx
	(BeginInputData): include new cut on z0 x sin(theta) for muons
	* include/Cuts/MuZ0ThetaCut.h
	(class MuZ0ThetaCut): add cut on z0 x sin(theta) for muons

	* tag version 12-00-04
	* src/ElectronSelector.cxx
	(BeginInputData): include new cut on z0 x sin(theta)

	* include/Cuts/ElZ0ThetaCut.h: add new cut on z0 x sin(theta) for electrons

2012-09-07  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* tag version 12-00-03
	* include/Cuts/TauIDCuts.h (>): implement custom tau ID cuts

2012-09-04  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/BaseTauIDCut.h (T): implement user defined working-points

2012-08-30  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* src/TrigDecisionTool.cxx: implement trigger matching for EF_2tau38T_medium1

2012-08-23  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* src/TrigDecisionTool.cxx (MatchedTrigger): fix di-tau trigger matching

2012-08-22  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* src/TrigDecisionTool.cxx (MatchedTrigger): implement di-tau
	trigger matching

2012-08-22  Christian Gumpert  <cgumpert@cern.ch>

	* tag version AnalysisUtilities-12-00-02
	* include/Cuts/JVFCut.h
	(class JVFCut): fix unfortunate implementation of JVF cut

2012-08-16  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/AnalysisUtilities_LinkDef.h: add pragma link for std::map<std::string, std::string>

2012-08-14  Christian Gumpert  <cgumpert@cern.ch>

	* tag version AnalysisUtilities-12-00-01

2012-08-10  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/BaseTauIDCut.h: implement virtual destructors

2012-08-09  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/BaseTauIDCut.h: extract class

	* include/Cuts/TauIDCuts.h (T): implement abstract TauIDCut class
	(T): implement tau ID enums

2012-08-08  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/TrigDecisionTool.h (ToolBase): implement resurrected trigger match

2012-08-07  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/TrigDecisionTool.h (ToolBase): implement trigger
	matching function

	* include/Cuts/TauIDCuts.h (T): fix bug in parametrised LLH cut
	(pT in GeV required)

2012-08-07  Christian Gumpert  <cgumpert@cern.ch>

	* include/Cuts/JVFCut.h: apply JVF cut only if the jet has some
	tracks (substitutes the eta former requirement)

	* src/IsEMPlusPlusHelper.cxx: fix bug in nSiOutliers definition

	* src/JetCleaningTool.cxx (DoJetCleaning): using static methods of JetCleaningCut

	* include/Cuts/MuD0SigCut.h: simplify #ifdef statements

	* include/Cuts/JetCleaningCut.h: update jet cleaning cut
	* include/JetSelector.h: 
	* src/JetSelector.cxx: 

	* include/Cuts/ElConeCuts.h: remove old cuts
	* include/Cuts/MuConeCuts.h:
	* include/Cuts/MuZCut.h:
	* include/Cuts/LArVetoCutForTaus.h:
	* include/Cuts/MuBLayerCut.h:
	* include/Cuts/LArVetoCut.h:
	* include/MuonSelector.h: 
	* include/TauSelector.h: 
	* include/TruthTauSelector.h: 

2012-08-07  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* src/MuonSelector.cxx (BeginInputData): add Muon author cut

2012-08-06  Christian Gumpert  <cgumpert@cern.ch>

	* include/Cuts/ElQualityCut.h (class ElQualityCut): use new macro
	for checking the electron quality, new quality levels:
	0 - loosePP  (from D3PD)
	1 - mediumPP (from D3PD)
	2 - tightPP  (from D3PD)
	3 - loosePP  (from egamma Macro)
	4 - mediumPP (from egamma Macro)
	5 - tightPP  (from egamma Macro)

	* include/Cuts/IsEMPlusPlusDefs.h: add macro for 2012 electron ID
	* include/Cuts/IsEMPlusPlusHelper.h: 
	* src/IsEMPlusPlusDefs.cxx: 
	* src/IsEMPlusPlusHelper.cxx:

2012-08-03  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/TauIDCuts.h (T): fix relative path issue if running in proof mode

2012-08-02  Christian Gumpert  <cgumpert@cern.ch>

	* include/Cuts/egammaPIDdefs.h: update to 2012 egamma PIDs

2012-08-01  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/JetSelector.h (>): remove JetCleaning cut

	* include/Cuts/TauIDCuts.h (>): implement parametrized cuts
	(T): refactor parametrized cuts

2012-07-26  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* tag version AnalysisUtilities-12-00-00

	* src/TauSelector.cxx (BeginInputData): remove obsolete cut based tau ID

2012-07-25  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/JetCleaningTool.h: implement hot tile runs

	* include/Cuts/TauIDCuts.h: implement parametrized BDT cuts

2012-07-06  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* include/Cuts/TauCrackVeto.h (>): fix bug concerning absolute eta

2012-06-30  Marcus Morgenstern  <marcus.matthias.morgenstern@cern.ch>

	* tag version AnalysisUtilities-11-00-00
	
	* src/TrigDecisionTool.cxx: check for embedding 

2012-06-28  Christian Gumpert  <cgumpert@cern.ch>

	* tag version AnalysisUtilities-02-02-01 

	* src/ElectronSelector.cxx
	(BeginInputData): use default eta cut for electrons

	* include/Cuts/EtaCut.h: remove specific cluster cut

2012-06-27  Christian Gumpert  <cgumpert@cern.ch>

	* tag version AnalysisUtilities-02-02-00

	* src/JetCleaningTool.cxx: fix bug in Looser JetCleaning

2012-06-12  Marcus Morgenstern  <marcusmorgenstern@Marcus-Morgensterns-MacBook-Pro.local>

	* create new tag AnalysisUtilities-02-01-01

	* TauCrackVeto.h: remove unused variable lead trk phi

	* TauIDCuts.h: use correct ElBDT veto variables
	
2012-11-28  Philipp Anger  <philipp.anger@cern.ch>

	* support for segment tagged muons
	
	* support for unbiased primary vertex cuts (d0sig, z0, z0sintheta)


2012-12-08  Philipp Anger  <philipp.anger@cern.ch>

	* support for unbiased  primary vertex cuts for muons (d0sig, z0, z0sintheta)
