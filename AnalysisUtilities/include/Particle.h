#ifndef AnalysisUtilities_Particle_H
#define AnalysisUtilities_Particle_H

// ROOT include(s)
#include "TLorentzVector.h"

/**
 * @short generic base class describing four vector properties of a particle
 * @ingroup core
 *
 * This class is meant to be used as a common base class for various particle
 * types. It only implements the very basic property of having a four-vector.
 * In addition an object weight can be assigned to address correction factors.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
class Particle
{
public:
  /// standard constructor
  Particle():m_fWeight(1),m_iCharge(0),tlv() {}
  
  /// standard copy constructor
  Particle(const Particle& rCopy):m_fWeight(rCopy.m_fWeight),m_iCharge(rCopy.m_iCharge),tlv(rCopy.tlv) {}
  
  /// initialize particle with given four vector
  Particle(const TLorentzVector& lorentz):m_fWeight(1),m_iCharge(0),tlv(lorentz) {}
  
  /// standard destructor
  virtual ~Particle() {}
  
  /// provide cast of particle to four vector type
  operator TLorentzVector() const
  {
    return tlv;
  }
  
  /// get object weight (const)
  float Weight() const {return m_fWeight;}
  
  /// get/set object weight
  float& Weight() {return m_fWeight;}

  /// sort predicate (high pt comes first)
  static bool pt_comp(const Particle* p1,const Particle* p2)
  {
    return (p1->tlv.Pt() > p2->tlv.Pt());
  }

  /// returns a reference to the TLorentzVector of the particle
  TLorentzVector& TLV() {return tlv;}

  /// returns the TLorentzVector of the particle (const version)
  TLorentzVector TLV() const {return tlv;}

  /// returns the electric charge of the particle
  int& GetCharge() {return m_iCharge;}

  /// returns the electric charge of the particle (const version)
  const int GetCharge() const {return m_iCharge;}
  
protected:
  float            m_fWeight; ///< object weight
  int              m_iCharge; ///< charge particle 
  TLorentzVector   tlv;       ///< four-vector of the particle
};

#endif // AnalysisUtilities_Particle_H
