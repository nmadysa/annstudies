#ifndef AnalysisUtilities_JetSelector_H
#define AnalysisUtilities_JetSelector_H

// STL include(s)
#include <vector>

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "BaseObjectSelector.h"
class CycleBase;

// D3PDObjects include(s)
#include "Jet.h"

/**
 * @short jet object selector
 *
 * Class responsible for selecting D3PDReader::Jet objects supporting the following cuts:
 * - eta cut: cut on the absolute eta value of the jet, parameters: c_eta_min, c_eta_max, c_exclude_etas
 * - pteta cut: cut on the pt within a special interval of eta
 * - pt cut: cut on the pt of the jet, parameter: c_pt_min_i, c_pt_max_i (in MeV)
 * - track criteria: quality cuts on jet track, see twki: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesPLHC2011
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

class JetSelector : public BaseObjectSelector<D3PDReader::Jet>
{
public:
  /// standard constructor
  JetSelector(CycleBase* pParent,const char* sName);
  /// standard destructor
  virtual ~JetSelector() {}

  /// book histograms and initialise cut vector
  virtual void BeginInputData(const SInputData& id) throw(SError);

  /// calculate cut efficiencies
  virtual void EndMasterInputData(const SInputData& id) throw(SError);
  
  /// do the selection and fill before/after histograms
  virtual void operator()(std::vector<D3PDReader::Jet*>& vJets,float fWeight = 1);

  /// use general method for checking one jet object
  using BaseObjectSelector<D3PDReader::Jet>::operator();

 
private:
  // configurable properties
  bool                  c_do_eta_cut;
  double                c_eta_min;
  double                c_eta_max;
  std::vector<double>   c_exclude_etas;

  bool                  c_do_pt_cut;
  double                c_pt_min;
  double                c_pt_max;

  bool                  c_do_cleaning;
  int                   c_cleaning_level;
  
  bool                  c_do_jvtxf_cut;
  double                c_jvtxf_abs_jvf_min;
  double                c_jvtxf_abs_eta_max;

};

#endif // AnalysisUtilities_JetSelector_H
