#ifndef AnalysisUtilities_Constants_H
#define AnalysisUtilities_Constants_H

namespace AnalysisUtilities_Constants
{
  // type for jet collection
  enum eJetType {TopoEM = 0, TopoLC = 1};

  // physical constants
  const float GeV = 1000.0;
  const float m_Z = 91.188 * GeV;
  const float m_W = 80.39 * GeV;
}

#endif // AnalysisUtilities_Constants_H
