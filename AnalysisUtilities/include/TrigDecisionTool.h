#ifndef TrigDecisionTool_H
#define TrigDecisionTool_H

#ifdef TAU_D3PD

//STL
#include <vector>
#include <string>

// SFrame include(s)
#include "core/include/SCycleBase.h"
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "ToolBase.h"

//AnalysisUtilities includes
#include "TriggerInfoCollection.h"
#include "TriggerMetadataCollection.h"
#include "Tau.h"
#include "TrigEFTau.h"

// BOOST include(s)
#ifndef __CINT__
#include "boost/function.hpp"
#endif // __CINT__

/**
 * TriggerInfoCollection need followin methods
 * - DB_SMK, DB_L1PSK, DB_HTLPSK
 * - Nav_chain_ChainId
 * - Nav_chain_RoIIndex
 * - Nav_chain_RoIType
 * - RoI_EF_(e|mu|tau)_..._(Container|ContainerStatus)
 */

class TrigDecisionTool : public ToolBase
{
public:
  /// standard constructor
  TrigDecisionTool(CycleBase* pParent, const char* sName);
  /// standard destructor
  virtual ~TrigDecisionTool() {}

  void BeginInputFile(const SInputData& id) throw(SError);

  /// connect prescale and super master keys
  void Initialise(const D3PDReader::TriggerInfoCollection& rTriggerInfo);
  /// update current configuration
  bool UpdateConfiguration();
  /// was the given HLT physics trigger issued
  bool CheckHLTTrigger(const std::string& sChain,const std::vector<short>& rPassedPhysics);
  
  template<class T>
  std::vector<T*> GetTriggerObjects(const std::string& sChain,const std::vector<T*>& rTriggerObjects);

  /// returns a list of RoIs associated with a given chain
  std::vector<int> GetRoIIndices(int iChainID);
  /// returns a list of RoI types associated with a given chain
  std::vector<int> GetRoITypes(int iChainID);
  /// returns ID of chain with given name
  int GetHLTChainID(const std::string& sChain);
  /// returns the name of the chain with the given ID
  std::string GetHLTChainName(int iChainID);

  /// returns single prescale of given (LVL1/2,HLT) trigger
  float GetPrescales(const std::string& sChain, bool bRerun=false);
  /// returns combined prescale of whole chain of given trigger
  float GetCombinedPrescales(std::string sChain, int iDeepness=2, bool bRerun=false, bool bVerbose=false);
  /// returns next! lower chain name of given trigger
  string GetLowerChainName(const std::string& sChain);
  bool MatchedTrigger(const std::string& sChain, const D3PDReader::Tau* const pTau, const std::vector<D3PDReader::TrigEFTau*>& vTrigTaus, const double& drMax=0.2);
  bool MatchedTrigger(const std::string& sChain, const D3PDReader::Tau* const pTau1, const D3PDReader::Tau* const pTau2, const std::vector<D3PDReader::TrigEFTau*>& vTrigTaus, const double& drMax=0.2);
  bool MatchedResurrected(const std::string& sChain, const D3PDReader::Tau* const pTau, const std::vector<D3PDReader::TrigEFTau*>& vTrigTaus, const double& drMax=0.2);
  std::pair<std::string, std::string> GetSingleTriggerFromDiTau(const std::string& sChain);
private:
  D3PDReader::TriggerMetadataCollection      m_rTrigMetaData;
  const D3PDReader::TriggerInfoCollection*   m_pTriggerInfoCollection;

  long int              m_lMetaEntry;
  long int              m_lMetaEntriesMax;

  std::string           c_conf_tree_name;

  bool c_is_embed;
  #ifndef __CINT__
  typedef const int& (D3PDReader::TrigEFTau::*MFP)() const;
  std::map<std::string, MFP> m_mResurrected;
  #endif //__CINT__
  template<class T>
  std::vector<int> GetTrigObjIndices(const std::string& sChain,const std::vector<T*>&);

  std::vector<int> GetTrigObjIndices(const std::vector<int>* pRoIIndices,
				     const std::vector<int>* pRoITypes,
				     int iRoIType,
				     int nObjRoIs,
				     const std::vector<short>* pRoI_type,
				     const std::vector<std::vector<int> >* pObjects,
				     const std::vector<std::vector<int> >* pObjectStatus);
};

#ifndef __CINT__
template<class T>
std::vector<T*> TrigDecisionTool::GetTriggerObjects(const std::string& sChain,const std::vector<T*>& rEFObjects)
{
  vector<T*> vPassed;

  if(!UpdateConfiguration())
    return vPassed;

  if(sChain.find("L2") != std::string::npos)
  {
    m_logger << ERROR << "retrieval of L2 trigger objects is not yet implemented" << SLogger::endmsg;
    return vPassed;
  }
  
  std::vector<int> vTrigObjIndices = GetTrigObjIndices(sChain,rEFObjects);

  typename std::vector<T*>::const_iterator cit = rEFObjects.begin();
  for(; cit != rEFObjects.end(); ++cit)
  {
    if(std::find(vTrigObjIndices.begin(),vTrigObjIndices.end(),(int)(*cit)->GetIndex()) != vTrigObjIndices.end())
      vPassed.push_back(*cit);
  }
  
  return vPassed;
}
#endif

#endif // TAU_D3PD

#endif // TrigDecisionTool_H
