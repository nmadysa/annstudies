#ifndef AnalysisUtilities_JetCleaningTool_H
#define AnalysisUtilities_JetCleaningTool_H

// STL include(s)
#include <vector>

// SFrame include(s)
#include "core/include/SInputData.h"
#include "core/include/SError.h"

// core include(s)
class CycleBase;
#include "ToolBase.h"

// D3PDObjects include(s)
#include "Jet.h"

class JetCleaningTool : public ToolBase {
public:

  enum LEVEL {LOOSER = 0, LOOSE, MEDIUM, MEDIUMTAU, TIGHT};

  JetCleaningTool(CycleBase* parent, const char* name);
  virtual ~JetCleaningTool();

  void BeginInputData(const SInputData& id) throw (SError);
  bool DoJetCleaning(const std::vector<D3PDReader::Jet*>& vJets, unsigned int lev) const;
  bool CheckHotTileProblem(const std::vector<D3PDReader::Jet*>& vJets,const unsigned int& RunNumber) const;
  bool CheckFCalProblem(const std::vector<D3PDReader::Jet*>& vJets,const unsigned int& RunNumber) const;
private:
  std::vector<int> m_vHotTileRuns;
};

#endif // AnalysisUtilities_JetCleaningTool_H
