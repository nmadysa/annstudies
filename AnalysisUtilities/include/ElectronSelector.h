#ifndef AnalysisUtilities_ElectronSelector_H
#define AnalysisUtilities_ElectronSelector_H

// STL include(s)
#include <vector>

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "BaseObjectSelector.h"
class CycleBase;

// D3PDObjects include(s)
#include "Electron.h"

/**
 * @short electron object selector
 *
 * Class responsible for the seelction of D3PDReader::Electron objects supporting the following cuts
 *
 * - Eta Cut: cut on the absolute eta value of the electron, parameters: c_eta_min, c_eta_max, c_exclude_etas
 * - PT Cut: cut on the pt value of the electron, parameters: c_pt_min, c_pt_max (in MeV)
 * - Quality cut: cut on quality definition of the electron, parameter: c_quality_level (0=loose, 1=medium, 2=tight)
 * - author cut: cut on the electron reco algorithm, parameter: c_allowed_authors
 * - OQ cleaning: cut on calorimeter related quality definitions, parameter: c_oq_bit_mask
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

class ElectronSelector : public BaseObjectSelector<D3PDReader::Electron>
{
public:
  /// standard constructor
  ElectronSelector(CycleBase* pParent,const char* sName);
  /// standard destructor
  virtual ~ElectronSelector() {}

  /// book histograms and initialise cut vector
  virtual void BeginInputData(const SInputData& id) throw(SError);

  /// calculate cut efficiencies
  virtual void EndMasterInputData(const SInputData& id) throw(SError);
  
  /// do the selection and fill before/after histograms
  virtual void operator()(std::vector<D3PDReader::Electron*>& vElectrons,float fWeight = 1);

  /// use general method for checking one electron object
  using BaseObjectSelector<D3PDReader::Electron>::operator();

private:
  // configurable properties
  bool                  c_do_author_cut;
  std::vector<int>      c_allowed_authors;

  bool                  c_do_oq_cleaning;
  int                   c_oq_bit_mask;

  bool                  c_do_pt_cut;
  double                c_pt_min;
  double                c_pt_max;

  bool                  c_do_eta_cut;
  double                c_eta_min;
  double                c_eta_max;
  std::vector<double>   c_exclude_etas;
  
  bool                  c_do_quality_cut;
  int                   c_quality_level;

  bool                  c_do_charge_cut;
  bool                  c_check_only_abs_charge;

  bool                  c_do_z0_cut;
  double                c_z0_pv;

  bool                  c_do_z0theta_cut;
  double                c_z0theta;
  
  bool                  c_do_d0_sig_cut;
  double                c_d0_offset;
  double                c_d0_sig;

  bool                  c_do_blayer_cut;
  bool                  c_use_unbiased_pv;
};

#endif // AnalysisUtilities_ElectronSelector_H
