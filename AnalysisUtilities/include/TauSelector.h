#ifndef AnalysisUtilities_TauSelector_H
#define AnalysisUtilities_TauSelector_H

#ifdef TAU_D3PD

// STL
#include <vector>

// SFrame
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "BaseObjectSelector.h"
class CycleBase;

// D3PDObjects include(s)
#include "Tau.h"

/**
 * @short tau object selector
 *
 * Class responsible for the seelction of D3PDReader::Tau objects supporting the following cuts
 *
 * - Eta Cut: cut on the absolute eta value of the tau, parameters: c_eta_min, c_eta_max, c_exclude_etas
 * - PT Cut: cut on the pt value of the tau, parameters: c_pt_min, c_pt_max (in MeV)
 * - Quality cut: cut on quality definition of the tau, parameter: c_quality_level (0=loose, 1=medium, 2=tight)
 * - author cut: cut on the tau reco algorithm, parameter: c_allowed_authors
 *
 * @author Marcus Morgenstern <marcus.matthias.morgenstern@cern.ch>
 */

class TauSelector : public BaseObjectSelector<D3PDReader::Tau> {
public:
  /// standard constructor
  TauSelector(CycleBase* pParent, const char* sName);
  /// standard destructor

  virtual ~TauSelector() {
  }

  /// book histograms and initialise cut vector
  virtual void BeginInputData(const SInputData& id) throw (SError);

  /// calculate cut efficiencies
  virtual void EndMasterInputData(const SInputData& id) throw (SError);

  /// do the selection and fill before/after histograms
  virtual void operator()(std::vector<D3PDReader::Tau*>& vTaus, float fWeight = 1);

  /// use general method for checking one tau object
  using BaseObjectSelector<D3PDReader::Tau>::operator();

private:
  // configurable properties
  bool c_do_eta_cut;
  double c_eta_min;
  double c_eta_max;
  std::vector<double> c_exclude_etas;

  bool c_do_pt_cut;
  double c_pt_min;
  double c_pt_max;

  bool c_do_quality_cut;
  int c_quality_level; ///< 0...loose, 1...medium, 2...tight

  bool c_do_author_cut;
  std::vector<int> c_allowed_authors;

  bool c_do_muonVeto;

  // TauID
  bool c_do_cutBasedID;
  int c_cutBasedID_level; ///< 0...loose, 1...medium, 2...tight

  bool c_do_llhBasedID;
  int c_llhBasedID_level; ///< 0...loose, 1...medium, 2...tight
  double c_llhBasedID_cut;

  bool c_do_bdtBasedID;
  int c_bdtBasedID_level; ///< 0...loose, 1...medium, 2...tight
  double c_bdtBasedID_cut; 

  bool c_do_bdtBasedEleVeto;
  int c_bdtBasedEleVeto_level; ///< 0...loose, 1...medium, 2...tight

  bool c_do_cutBasedEleVeto;
  int c_cutBasedEleVeto_level; ///< 0...loose, 1...medium, 2...tight

  //tau tracks
  bool c_do_min_number_of_tracks;
  int c_min_number_of_tracks;

  bool c_do_exact_number_of_tracks;
  std::vector<int> c_exact_number_of_tracks;

  //tau charge
  bool c_do_charge_cut;
  int c_abs_charge;

  bool c_do_crackVeto;
  double c_crackVeto_eta_min;
  double c_crackVeto_eta_max;
};

#endif // TAU_D3PD

#endif // AnalysisUtilities_TauSelector_H
