#ifndef AnalysisUtilities_MuonSelector_H
#define AnalysisUtilities_MuonSelector_H

// STL include(s)
#include <vector>

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "BaseObjectSelector.h"
class CycleBase;

// D3PDObjects include(s)
#include "Muon.h"

/**
 * @short electron object selector
 *
 * Class responsible for selecting D3PDReader::Muon objects supporting the following cuts:
 * - eta cut: cut on the absolute eta value of the muon, parameters: c_eta_min, c_eta_max, c_exclude_etas
 * - pt cut: cut on the pt of the muon, parameter: c_pt_min, c_pt_max (in MeV)
 * - track criteria: quality cuts on muon track, see twki: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesPLHC2011
 *
 * @author Philipp Anger <panger@cern.ch>
 */

class MuonSelector : public BaseObjectSelector<D3PDReader::Muon>
{
public:
  /// standard constructor
  MuonSelector(CycleBase* pParent,const char* sName);
  /// standard destructor
  virtual ~MuonSelector() {}

  /// book histograms and initialise cut vector
  virtual void BeginInputData(const SInputData& id) throw(SError);

  /// calculate cut efficiencies
  virtual void EndMasterInputData(const SInputData& id) throw(SError);
  
  /// do the selection and fill before/after histograms
  virtual void operator()(std::vector<D3PDReader::Muon*>& vMuons,float fWeight = 1);

  /// use general method for checking one muon object
  using BaseObjectSelector<D3PDReader::Muon>::operator();

private:
  // configurable properties
  bool                  c_do_eta_cut;
  double                c_eta_min;
  double                c_eta_max;
  std::vector<double>   c_exclude_etas;
  
  bool                  c_do_pt_cut;
  double                c_pt_min;
  double                c_pt_max;

  bool                  c_do_track_cuts;

  bool                  c_do_z0_cut;
  double                c_z0_pv;

  bool                  c_do_z0theta_cut;
  double                c_z0theta;

  bool                  c_require_combined;

  bool                  c_do_charge_cut;
  bool                  c_check_only_abs_charge;

  bool                  c_do_quality_cut;
  int                   c_quality_cut;

  bool                  c_do_d0_sig_cut;
  double                c_d0_offset;
  double                c_d0_sig;

  bool                  c_do_author_cut;
  std::vector<int>      c_author_cut;
  
  bool                  c_use_unbiased_pv;  
};

#endif // AnalysisUtilities_MuonSelector_H
