#ifndef AnalysisModul_MuD0SigCut_H
#define AnalysisModul_MuD0SigCut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisModul include(s)
#include "Muon.h"

class MuD0SigCut : public CutWrapper<const D3PDReader::Muon>
{
public:
  MuD0SigCut(std::string name,float fDmax, bool bUnbiased=false, float fOffset = 0):
    CutWrapper<const D3PDReader::Muon>(name),m_bUnbiased(bUnbiased),m_fOffset(fOffset),m_fDmax(fabs(fDmax)) {}
  virtual ~MuD0SigCut() {}

protected:
  virtual bool PassCut(const D3PDReader::Muon* muon)
  {
    if (m_bUnbiased)
      return (fabs((muon->trackd0pvunbiased() + m_fOffset) / muon->tracksigd0pvunbiased()) < m_fDmax);
    else {
#ifdef TAU_D3PD
      return (fabs(muon->id_d0_exPV() / sqrt(muon->cov_d0_exPV())) < m_fDmax);
#else
      return (fabs(muon->id_d0_exPV() / sqrt(muon->id_cov_d0_exPV())) < m_fDmax);
#endif
    }
  }

private:
  bool  m_bUnbiased;
  float m_fOffset;
  float m_fDmax;
};

#endif // AnalysisModul_MuD0SigCut_H
