#ifndef AnalysisUtilities_TauChargeCut_H
#define AnalysisUtilities_TauChargeCut_H

// system include(s)
#include <string>

// custom include(s)
#include "Tau.h"
#include "CutWrapper.h"

class TauChargeCut : public CutWrapper<const D3PDReader::Tau>
{
public:
  TauChargeCut(std::string sName,int iCharge):
    CutWrapper<const D3PDReader::Tau>(sName),m_iCharge(iCharge)
  {}

  virtual ~TauChargeCut() {}

protected:
  virtual bool PassCut(const D3PDReader::Tau* pTau)
  {
    return fabs(pTau->charge()) == m_iCharge;
  }

private:
  int m_iCharge;
};

#endif // AnalysisUtilities_TauChargeCut_H
