/* 
 * File:   TauMuonVetoCut.h
 * Author: morgenst
 *
 * Created on January 24, 2012, 8:46 PM
 */

#ifndef TAUMUONVETO_H
#define	TAUMUONVETO_H

class TauMuonVetoCut : public CutWrapper<const D3PDReader::Tau>
{
  public:
    TauMuonVetoCut(std::string sName):
      CutWrapper<const D3PDReader::Tau>(sName)
    {}

    virtual ~TauMuonVetoCut() {}

  protected:
    virtual bool PassCut(const D3PDReader::Tau* pTau)
    {
      return pTau->muonVeto() == 0; 
    }
};



#endif	/* TAUMUONVETO_H */

