#ifndef MuQualityCut_H
#define MuQualityCut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisUtilities include(s)
#include "Muon.h"

class MuQualityCut : public CutWrapper<const D3PDReader::Muon>
{
public:
  MuQualityCut(std::string name, int iLevel):
    CutWrapper<const D3PDReader::Muon>(name),m_iLevel(iLevel) {}
  virtual ~MuQualityCut() {}

protected:
  virtual bool PassCut(const D3PDReader::Muon* muon)
  {
    switch(m_iLevel)
    {
      case 0: return (muon->loose()==1); break;
      case 1: return (muon->medium()==1); break;
      case 2: return (muon->tight()==1); break;
      case 3: return (muon->loose()==1 && muon->isCombinedMuon()); break;
      case 4: return (muon->medium()==1 && muon->isCombinedMuon()); break;
      case 5: return (muon->tight()==1 && muon->isCombinedMuon()); break;
      case 6: return (muon->loose()==1 && (muon->isCombinedMuon()||muon->isSegmentTaggedMuon())); break;
      case 7: return (muon->medium()==1 && (muon->isCombinedMuon()||muon->isSegmentTaggedMuon())); break;
      case 8: return (muon->tight()==1 && (muon->isCombinedMuon()||muon->isSegmentTaggedMuon())); break;
      default:
        return false;
    }
  }

private:
  int m_iLevel;
};

#endif // MuQualityCut_H
