#ifndef AnalysisUtilities_MuTrackCuts_H
#define AnalysisUtilities_MuTrackCuts_H

// STL include(s)
#include <string>

// system include(s)
#include <cmath>

// custom include(s)
#include "Muon.h"
#include "CutWrapper.h"

class MuTrackCuts : public CutWrapper<const D3PDReader::Muon>
{
public:
  MuTrackCuts(std::string sName):CutWrapper<const D3PDReader::Muon>(sName)
  {}

  virtual ~MuTrackCuts() {}

protected:
  virtual bool PassCut(const D3PDReader::Muon* pMuon)
  {
    if((pMuon->expectBLayerHit()) && (pMuon->nBLHits() <= 0))
      return false;
    if(pMuon->nPixHits() + pMuon->nPixelDeadSensors() <= 0)
      return false;
    if(pMuon->nSCTHits() + pMuon->nSCTDeadSensors() < 5)
      return false;
    if(pMuon->nPixHoles() + pMuon->nSCTHoles() >= 3)
      return false;
    int iNtot = pMuon->nTRTHits() + pMuon->nTRTOutliers();
    if((fabs(pMuon->TLV().Eta()) < 1.9) && (fabs(pMuon->TLV().Eta()) > 0.1) && ((iNtot <= 5) || (pMuon->nTRTOutliers() >= 0.9 * iNtot)))
      return false;
    if(((fabs(pMuon->TLV().Eta()) >= 1.9) || (fabs(pMuon->TLV().Eta()) <= 0.1)) && (iNtot > 5) && (pMuon->nTRTOutliers() >= 0.9 * iNtot))
      return false;

    return true;
  }
};

#endif // AnalysisUtilities_MuTrackCuts_H
