#ifndef AnalysisModul_PTCut_H
#define AnalysisModul_PTCut_H

// system
#include "assert.h"

// custom include(s)
#include "CutWrapper.h"
#include "Particle.h"

/**
 * @short pt cut
 *
 * applies m_fMinPt < Particle->TLV().Pt() < m_fMaxPt
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

class PTCut : public CutWrapper<const Particle>
{
public:
  /// standard constructor taking the name and the limits on the pt
  PTCut(std::string name,double MinPt = 0,double MaxPt = 999e12):
    CutWrapper<const Particle>(name),m_fMaxPt(MaxPt),m_fMinPt(MinPt)
  {
    assert(m_fMinPt < m_fMaxPt);
  }

  /// standard destructor
  virtual ~PTCut() {}

protected:
  /// applies the actual pt cut
  virtual bool PassCut(const Particle* particle)
  {
    if((m_fMinPt < particle->TLV().Pt()) && (particle->TLV().Pt() < m_fMaxPt))
      return true;
    else
      return false;
  }
    
private:
  double  m_fMaxPt;   ///< upper pt limit
  double  m_fMinPt;   ///< lower pt limit
};

#endif // AnalysisModul_PTCut_H
