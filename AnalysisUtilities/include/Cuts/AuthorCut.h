#ifndef AnalysisUtilities_AuthorCut_H
#define AnalysisUtilities_AuthorCut_H

// STL include(s)
#include <string>
#include <vector>
#include <algorithm>

// custom include(s)
#include "CutWrapper.h"

template<class T>
class AuthorCut : public CutWrapper<const T>
{
public:
  AuthorCut(std::string sName,const std::vector<int>& vAllowedAuthors):
    CutWrapper<const T>(sName),m_vAllowedAuthors(vAllowedAuthors) {}
  virtual ~AuthorCut() {}

protected:    
  virtual bool PassCut(const T* pParticle)
  {
    if(std::find(m_vAllowedAuthors.begin(),m_vAllowedAuthors.end(),pParticle->author()) != m_vAllowedAuthors.end())
      return true;
    else
      return false;
  }
    
private:
  std::vector<int>  m_vAllowedAuthors; ///< vector of allowed authors
};

#endif // AnalysisUtilities_AuthorCut_H
