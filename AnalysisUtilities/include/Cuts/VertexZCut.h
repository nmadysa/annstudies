#ifndef AnalysisUtilities_VertexZCut_H
#define AnalysisUtilities_VertexZCut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisUtilities include(s)
#include "Vertex.h"

class VertexZCut : public CutWrapper<const D3PDReader::Vertex>
{
public:
  VertexZCut(std::string name,double MinZ = -1000,double MaxZ = 1000):
    CutWrapper<const D3PDReader::Vertex>(name),m_dMaxZ(MaxZ),m_dMinZ(MinZ)  {}
  virtual ~VertexZCut() {}
  
protected:    
  virtual bool PassCut(const D3PDReader::Vertex* vertex)
  {
    if((m_dMinZ < vertex->z()) && (vertex->z() < m_dMaxZ))
      return true;
    else
      return false;
  }
    
private:
  double m_dMaxZ;
  double m_dMinZ;
};

#endif // AnalysisUtilities_VertexZCut_H
