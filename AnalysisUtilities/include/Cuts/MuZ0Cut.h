#ifndef AnalysisModul_MuZ0Cut_H
#define AnalysisModul_MuZ0Cut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisModul include(s)
#include "Muon.h"

class MuZ0Cut : public CutWrapper<const D3PDReader::Muon>
{
public:
  MuZ0Cut(std::string name,float fZmax, bool bUnbiased=false):
    CutWrapper<const D3PDReader::Muon>(name),m_bUnbiased(bUnbiased),m_fZmax(fabs(fZmax)) {}
  virtual ~MuZ0Cut() {}

protected:
  virtual bool PassCut(const D3PDReader::Muon* muon)
  {
  if (m_bUnbiased)
    return (fabs(muon->trackz0pvunbiased()) < m_fZmax);
  else
    return (fabs(muon->id_z0_exPV()) < m_fZmax);
   } 

private:
  bool  m_bUnbiased; 
  float m_fZmax;
};

#endif // AnalysisModul_MuZ0Cut_H
