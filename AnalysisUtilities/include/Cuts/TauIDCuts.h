#ifndef AnalysisUtilities_TauIDCuts_H
#define AnalysisUtilities_TauIDCuts_H

// system include(s)
#include <cmath>

// custom include(s)
#include "CutWrapper.h"

#include "BaseTauIDCut.h"

//=====================================================================================
class TauIDLLH : public CutWrapper<const D3PDReader::Tau>
{
  public:
  TauIDLLH(std::string sName, int iLevel, float fCut=0):
    CutWrapper<const D3PDReader::Tau>(sName),m_iLevel(iLevel),m_fCut(fCut)
    {
      m_LLH = new TauLLHCut();
    }

    virtual ~TauIDLLH() {}

  protected:
    virtual inline bool PassCut(const D3PDReader::Tau* pTau)
    {
      return m_LLH->PassCut(pTau, m_iLevel, m_fCut);
    }
private:
  int m_iLevel; ///< quality level; 0...loose, 1...medium, 2...tight, 3...custom
  float m_fCut;
  TauLLHCut* m_LLH;
};

//====================================================================================
class TauIDBDT : public CutWrapper<const D3PDReader::Tau>
{
  public:
  TauIDBDT(std::string sName, int iLevel, float fCut=0):
    CutWrapper<const D3PDReader::Tau>(sName),m_iLevel(iLevel), m_fCut(fCut)
    {
      m_BDT = new TauBDTCut();
    }

    virtual ~TauIDBDT() {}

  protected:
    virtual bool PassCut(const D3PDReader::Tau* pTau)
    {
      return m_BDT->PassCut(pTau, m_iLevel, m_fCut);
    }

  private:
    int m_iLevel; ///< quality level; 0...loose, 1...medium, 2...tight, 3...costum
  float m_fCut;
  TauBDTCut* m_BDT;
};

//====================================================================================


class TauBDTEleVeto : public CutWrapper<const D3PDReader::Tau>
{
  public:
    TauBDTEleVeto(std::string sName, int iLevel):
      CutWrapper<const D3PDReader::Tau>(sName),m_iLevel(iLevel)
    {
      m_ElBDT = new TauBDTElVetoCut();
    }

    virtual ~TauBDTEleVeto() {}

  protected:
    virtual inline bool PassCut(const D3PDReader::Tau* pTau)
    {
      //electronVeto - bdt based
        
      //only for 1-prong taus; pass cut for everything else
      if ( pTau->numTrack()!=1 ) return true;
      
      //realculate decision for high pT taus
      if(pTau->TLV().Pt() > 80000.)
	return m_ElBDT->PassCut(pTau, m_iLevel);
      
      switch(m_iLevel)
      {
        case 0: return pTau->EleBDTLoose() == 0; break;
        case 1: return pTau->EleBDTMedium() == 0; break;
        case 2: return pTau->EleBDTTight() == 0; break;
        default:
          return false;
      } 
    }

private:
  int m_iLevel; ///< quality level; 0...loose, 1...medium, 2...tight
  TauBDTElVetoCut* m_ElBDT;
};

//====================================================================================

#endif // AnalysisUtilities_TauIDCuts_H
