#ifndef AnalysisModul_ElD0SigCut_H
#define AnalysisModul_ElD0SigCut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisModul include(s)
#include "Electron.h"

class ElD0SigCut : public CutWrapper<const D3PDReader::Electron>
{
public:
  ElD0SigCut(std::string name,float fDmax, bool bUnbiased=false, float fOffset=0):
    CutWrapper<const D3PDReader::Electron>(name),m_bUnbiased(bUnbiased),m_fOffset(fOffset),m_fDmax(fabs(fDmax)) {}
  virtual ~ElD0SigCut() {}

protected:
  virtual bool PassCut(const D3PDReader::Electron* electron)
  {    
    if (m_bUnbiased)
      return (fabs((electron->trackd0pvunbiased() + m_fOffset)/electron->tracksigd0pvunbiased()) < m_fDmax);
    else
      return (fabs(electron->trackd0pv()/electron->tracksigd0pv()) < m_fDmax);  
  }

private:
  bool  m_bUnbiased;
  float m_fOffset;
  float m_fDmax;

};

#endif // AnalysisModul_ElD0SigCut_H
