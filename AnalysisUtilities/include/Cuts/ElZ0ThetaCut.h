#ifndef AnalysisModul_ElZ0ThetaCut_H
#define AnalysisModul_ElZ0ThetaCut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisModul include(s)
#include "Electron.h"

class ElZ0ThetaCut : public CutWrapper<const D3PDReader::Electron>
{
public:
  ElZ0ThetaCut(std::string name,float fZmax,bool bUnbiased=false):
    CutWrapper<const D3PDReader::Electron>(name),m_bUnbiased(bUnbiased),m_fZmax(fabs(fZmax)) {}
  virtual ~ElZ0ThetaCut() {}

protected:
  virtual bool PassCut(const D3PDReader::Electron* pElectron)
  {
    if (m_bUnbiased)
      return (fabs(pElectron->trackz0pvunbiased() * sin(pElectron->tracktheta())) < m_fZmax);
    else
      return (fabs(pElectron->trackz0pv() * sin(pElectron->tracktheta())) < m_fZmax);
  }

private:
  bool  m_bUnbiased;
  float m_fZmax;
};

#endif // AnalysisModul_ElZ0ThetaCut_H
