#ifndef AnalysisModul_ElZ0Cut_H
#define AnalysisModul_ElZ0Cut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisModul include(s)
#include "Electron.h"

class ElZ0Cut : public CutWrapper<const D3PDReader::Electron>
{
public:
  ElZ0Cut(std::string name,float fZmax, bool bUnbiased=false):
    CutWrapper<const D3PDReader::Electron>(name),m_bUnbiased(bUnbiased),m_fZmax(fabs(fZmax)) {}
  virtual ~ElZ0Cut() {}

protected:
  virtual bool PassCut(const D3PDReader::Electron* electron)
  {
    if (m_bUnbiased)
      return (fabs(electron->trackz0pvunbiased()) < m_fZmax);
    else
      return (fabs(electron->trackz0pv()) < m_fZmax);
  }

private:
  bool  m_bUnbiased;  
  float m_fZmax;
};

#endif // AnalysisModul_ElZ0Cut_H
