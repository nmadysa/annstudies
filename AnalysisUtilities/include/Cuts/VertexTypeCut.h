#ifndef AnalysisUtilities_VertexTypeCut_H
#define AnalysisUtilities_VertexTypeCut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisUtilities include(s)
#include "Vertex.h"

class VertexTypeCut : public CutWrapper<const D3PDReader::Vertex>
{
public:
 VertexTypeCut(std::string name, const std::vector<int>& vAllowedTypes):
  CutWrapper<const D3PDReader::Vertex>(name),m_vAllowedTypes(vAllowedTypes){}
  virtual ~VertexTypeCut() {}
  
protected:    
  virtual bool PassCut(const D3PDReader::Vertex* vertex)
  {
    if(std::find(m_vAllowedTypes.begin(), m_vAllowedTypes.end(), vertex->type()) != m_vAllowedTypes.end())
      return true;
    else
      return false;
  }
    
private:
  std::vector<int> m_vAllowedTypes;
};

#endif // AnalysisUtilities_VertexZCut_H
