#ifndef AnalysisUtilities_ElOQCut_H
#define AnalysisUtilities_ElOQCut_H

// STL include(s)
#include <string>

// custom include(s)
#include "Electron.h"
#include "CutWrapper.h"

class ElOQCut : public CutWrapper<const D3PDReader::Electron>
{
public:
  ElOQCut(std::string sName,int iBitMask):
    CutWrapper<const D3PDReader::Electron>(sName),m_iBitMask(iBitMask)
  {}

  virtual ~ElOQCut() {}

protected:
  virtual bool PassCut(const D3PDReader::Electron* pElectron)
  {
    return (pElectron->OQ() & m_iBitMask) == 0;
  }

private:
  int m_iBitMask; ///< bit mask representing object quality criteria
};

#endif // AnalysisUtilities_ElOQCut_H
