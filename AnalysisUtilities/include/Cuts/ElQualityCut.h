#ifndef AnalysisUtilities_ElQualityCut_H
#define AnalysisUtilities_ElQualityCut_H

// system include(s)
#include <string>

// custom include(s)
#include "Electron.h"
#include "CutWrapper.h"
#include "IsEMPlusPlusHelper.h"

class ElQualityCut : public CutWrapper<const D3PDReader::Electron>
{
public:

  ElQualityCut(std::string sName, int iLevel) :
    CutWrapper<const D3PDReader::Electron>(sName), m_iLevel(iLevel)
  {}

  virtual ~ElQualityCut() {}

protected:

  virtual bool PassCut(const D3PDReader::Electron* pElectron)
  {
    switch (m_iLevel) {
    case 0: return pElectron->loosePP() == 1;
      break;
    case 1: return pElectron->mediumPP() == 1;
      break;
    case 2: return pElectron->tightPP() == 1;
      break;
    case 3: return isEMPlusPlusHelper::IsLoosePlusPlus(pElectron);
	    break;
    case 4: return isEMPlusPlusHelper::IsMediumPlusPlus(pElectron);
      break;
    case 5: return isEMPlusPlusHelper::IsTightPlusPlus(pElectron);
      break;
#ifndef TAU_D3PD
#ifdef FIXED      
    case 6: return isEMPlusPlusHelper::IsMultiLepton(pElectron);
    break;
#endif    
#endif
    default:
      return false;
    }
  }

private:
  int m_iLevel; ///< quality level; 0...loose, 1...medium, 2...tight
};

#endif // AnalysisUtilities_ElQualityCut_H
