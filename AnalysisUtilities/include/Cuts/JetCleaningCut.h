#ifndef AnalysisUtilities_JetCleaningCut_H
#define AnalysisUtilities_JetCleaningCut_H

// system include(s)
#include <string>
#include <iostream>

// custom include(s)
#include "Jet.h"
#include "CutWrapper.h"

class JetCleaningCut : public CutWrapper<const D3PDReader::Jet>
{
public:

  JetCleaningCut(std::string sName, int iLevel) :
    CutWrapper<const D3PDReader::Jet>(sName), m_iLevel(iLevel)
  {}

  virtual ~JetCleaningCut() {}

  static bool Looser(const D3PDReader::Jet* const& pJet)
  {
    // HEC spikes
    if(pJet->hecf() > 0.5 && fabs(pJet->HECQuality()) > 0.5 && pJet->AverageLArQF() / 65535. > 0.8)
      return false;

    if(fabs(pJet->NegativeE()) > 60000.)
      return false;

    // EM coherent noise
    if(pJet->emfrac() > 0.95 && fabs(pJet->LArQuality()) > 0.8 && pJet->AverageLArQF() / 65535. > 0.8 && fabs(pJet->emscale_eta()) < 2.8)
      return false;

    // cosmics
    if(pJet->emfrac() < 0.05 && (pJet->sumPtTrk() / pJet->TLV().Pt() < 0.05) && fabs(pJet->emscale_eta()) < 2.)
      return false;

    if(pJet->emfrac() < 0.05 && fabs(pJet->emscale_eta()) >= 2.)
      return false;

    if(pJet->fracSamplingMax() > 0.99 && fabs(pJet->emscale_eta()) < 2.)
      return false;

    return true;
  }

  static bool Loose(const D3PDReader::Jet* const& pJet)
  {
    if(!JetCleaningCut::Looser(pJet))
      return false;

    if((fabs(pJet->LArQuality()) > 0.8) && (pJet->emfrac() > 0.95) && (fabs(pJet->emscale_eta()) < 2.8))
      return false;

    if((pJet->hecf() > 0.5) && (fabs(pJet->HECQuality()) > 0.5))
      return false;

    if(fabs(pJet->Timing()) > 25)
      return false;

    return true;
  }
  
  static bool Medium(const D3PDReader::Jet* const& pJet,bool bTauSpecific = false)
  {
    if(!JetCleaningCut::Loose(pJet))
      return false;

    if((fabs(pJet->LArQuality()) > 0.8) && (pJet->emfrac() > 0.9) && (fabs(pJet->emscale_eta()) < 2.8))
      return false;
 
    if(pJet->hecf() > 1 - fabs(pJet->HECQuality()))
      return false;

    if((pJet->emfrac() < 0.05) && (pJet->sumPtTrk() / pJet->TLV().Pt() < 0.1) && (fabs(pJet->emscale_eta()) < 2))
      return false;
    if(fabs(pJet->Timing()) > 10)
      return false;

    if(!bTauSpecific)
      if ((pJet->emfrac() > 0.95) && (pJet->sumPtTrk() / pJet->TLV().Pt() < 0.05) && (fabs(pJet->emscale_eta()) < 2))
	return false;

    return true;
  }
  
protected:

  virtual bool PassCut(const D3PDReader::Jet* pJet)
  {
    switch(m_iLevel)
    {
    case 0: return JetCleaningCut::Looser(pJet);
    case 1: return JetCleaningCut::Loose(pJet);
    case 2: return JetCleaningCut::Medium(pJet);
    case 3: return JetCleaningCut::Medium(pJet,true);
    case 4:
      std::cerr << "tight jet cleaning not implemented yet" << std::endl;
      return false;

    default:
      std::cerr << "unknown jet cleaning level" << std::endl;
      throw SError(SError::StopExecution);
      return false;
    }
  }

private:
  int m_iLevel; ///< 0...looser, 1...loose, 2...medium, 3...medium (tau specific), 4...tight
};

#endif // AnalysisUtilities_JetCleaningCut_H
