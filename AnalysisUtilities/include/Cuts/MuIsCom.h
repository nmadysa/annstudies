#ifndef AnalysisUtilities_MuIsCom_H
#define AnalysisUtilities_MuIsCom_H

// STL include(s)
#include <string>

// system include(s)
#include <cmath>

// custom include(s)
#include "Muon.h"
#include "CutWrapper.h"

class MuIsCom : public CutWrapper<const D3PDReader::Muon>
{
public:
  MuIsCom(std::string sName):CutWrapper<const D3PDReader::Muon>(sName)
  {}

  virtual ~MuIsCom() {}

protected:
  virtual bool PassCut(const D3PDReader::Muon* pMuon)
  {
    // require_combined
    if(!(pMuon->isCombinedMuon())) return false;

    return true;
  }
};

#endif // AnalysisUtilities_MuIsCom_H
