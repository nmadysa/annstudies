#ifndef AnalysisModul_ChargeCut_H
#define AnalysisModul_ChargeCut_H

// STL include(s)
#include "math.h"

// custom include(s)
#include "CutWrapper.h"
#include "Particle.h"

/**
 * @short charge cut
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

template<class T>
class ChargeCut : public CutWrapper<const T>
{
public:
  /// standard constructor taking the name and the limits on the pt
  ChargeCut(std::string name,int iCharge,bool bAbs = false):
    CutWrapper<const T>(name),m_iCharge(iCharge),m_bAbs(bAbs)
  {}

  /// standard destructor
  virtual ~ChargeCut() {}

protected:
  /// applies the actual pt cut
  virtual bool PassCut(const T* particle)
  {
    return ((m_bAbs) ? (fabs(particle->charge()) == m_iCharge) : (particle->charge() == m_iCharge));
  }
    
private:
  int   m_iCharge;   ///< required charge
  bool  m_bAbs;      ///< flag whether only abslute value should be checked
};

#endif // AnalysisModul_ChargeCut_H
