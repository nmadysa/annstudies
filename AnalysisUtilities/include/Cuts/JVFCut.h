#ifndef AnalysisModul_JVFCut_H
#define AnalysisModul_JVFCut_H

// STL include(s)
#include "math.h"

// core include(s)
#include "CutWrapper.h"

// D3PDReader include(s)
#include "Jet.h"

/**
 * @short charge cut
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

class JVFCut : public CutWrapper<const D3PDReader::Jet>
{
public:
  /// standard constructor taking the name and the limits on the pt
  JVFCut(std::string name,float fMinAbsJVF,float fMaxAbsEta):
    CutWrapper<const D3PDReader::Jet>(name),m_fMinAbsJVF(fMinAbsJVF),m_fMaxAbsEta(fMaxAbsEta)
  {}

  /// standard destructor
  virtual ~JVFCut() {}

protected:
  /// applies the actual pt cut
  virtual bool PassCut(const D3PDReader::Jet* pJet)
  {
    return ((fabs(pJet->eta()) > m_fMaxAbsEta) || (fabs(pJet->jvtxf()) > m_fMinAbsJVF));
  }
    
private:
  float   m_fMinAbsJVF;   ///< required minimal absolute value of JVF
  float   m_fMaxAbsEta;   ///< maximum absolute value to which this cut is applied
};

#endif // AnalysisModul_JVFCut_H
