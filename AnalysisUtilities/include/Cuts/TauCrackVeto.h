/* 
 * File:   TauCrackVeto.h
 * Author: morgenst
 *
 * Created on January 24, 2012, 9:07 PM
 */

#ifndef TAUCRACKVETO_H
#define	TAUCRACKVETO_H

class TauCrackVeto : public CutWrapper<const D3PDReader::Tau> {
public:

    TauCrackVeto(std::string sName, double MinEta = 999e12, double MaxEta = 999e12) :
    CutWrapper<const D3PDReader::Tau>(sName), m_fMaxEta(MaxEta), m_fMinEta(MinEta) {
    }

    virtual ~TauCrackVeto() {
    }

protected:

    virtual bool PassCut(const D3PDReader::Tau* pTau) {
        // inside tau candidate loop with index i
        // first get the index of the leading track
        int ldtrkindex = -1;
        float ldtrkpt = 0.;
        if (pTau->track_n() == 0)
            return true;
        for (int j = 0; j < pTau->track_n(); j++) {
            if (pTau->track_pt().at(j) > ldtrkpt) {
                ldtrkpt = pTau->track_pt().at(j);
                ldtrkindex = j;
            }
        }

        // Apply cuts on direction of the lead track
        float ldtrk_eta = TMath::Abs(pTau->track_eta().at(ldtrkindex));

        if (m_fMinEta < ldtrk_eta && ldtrk_eta < m_fMaxEta)
            return false;
        else
            return true;
    }

private:
    double m_fMaxEta;
    double m_fMinEta;
};


#endif	/* TAUCRACKVETO_H */

