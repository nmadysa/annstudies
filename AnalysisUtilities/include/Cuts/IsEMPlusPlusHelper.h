#ifndef ISEMPLUSPLUSHELPER_H
#define ISEMPLUSPLUSHELPER_H
#include <iostream>
#include <fstream>
#include <string>
#include "Cuts/IsEMPlusPlusDefs.h"
#include "Cuts/MultiLeptonDefs.h"
#include "Electron.h"

namespace isEMPlusPlusHelper{

  bool IsLoosePlusPlus(const D3PDReader::Electron* eg, egammaMenu::egMenu menu = egammaMenu::eg2012, 
		       bool debug=false,
		       bool isTrigger=false);
  bool IsLoosePlusPlus0(const D3PDReader::Electron* eg,egammaMenu::egMenu menu =egammaMenu::eg2012,
		       bool debug=false,
		       bool isTrigger=false);
  bool IsLoosePlusPlus1(const D3PDReader::Electron* eg,egammaMenu::egMenu menu=egammaMenu::eg2012,
		       bool debug=false,
		       bool isTrigger=false);
  bool IsMediumPlusPlus(const D3PDReader::Electron* eg,egammaMenu::egMenu menu=egammaMenu::eg2012,
			bool debug=false,
			bool isTrigger=false);

  bool IsTightPlusPlus(const D3PDReader::Electron* eg, egammaMenu::egMenu menu=egammaMenu::eg2012, 
			bool debug=false,
			bool isTrigger=false);
#ifndef TAU_D3PD
#ifdef FIXED  
  bool IsMultiLepton(const D3PDReader::Electron* eg);
#endif  
#endif
}

#endif //
