#ifndef AnalysisModul_MuZ0ThetaCut_H
#define AnalysisModul_MuZ0ThetaCut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisModul include(s)
#include "Muon.h"

class MuZ0ThetaCut : public CutWrapper<const D3PDReader::Muon>
{
public:
  MuZ0ThetaCut(std::string name,float fZmax,bool bUnbiased=false):
    CutWrapper<const D3PDReader::Muon>(name),m_bUnbiased(bUnbiased),m_fZmax(fabs(fZmax)) {}
  virtual ~MuZ0ThetaCut() {}

protected:
  virtual bool PassCut(const D3PDReader::Muon* pMuon)
  {
    if (m_bUnbiased)
      return (fabs(pMuon->trackz0pvunbiased() * sin(pMuon->id_theta_exPV())) < m_fZmax);
    else
      return (fabs(pMuon->id_z0_exPV() * sin(pMuon->id_theta_exPV())) < m_fZmax);
  }

private:
  bool  m_bUnbiased;  
  float m_fZmax;
};

#endif // AnalysisModul_MuZ0Cut_H
