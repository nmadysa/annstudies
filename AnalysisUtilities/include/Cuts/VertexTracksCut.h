#ifndef AnalysisUtilities_VertexTracksCut_H
#define AnalysisUtilities_VertexTracksCut_H

// core include(s)
#include "CutWrapper.h"

// AnalysisUtilities include(s)
#include "Vertex.h"

class VertexTracksCut : public CutWrapper<const D3PDReader::Vertex>
{
public:
  VertexTracksCut(std::string name,unsigned int MinTracks = 0,unsigned int MaxTracks = 10000):
    CutWrapper<const D3PDReader::Vertex>(name),m_iMaxTracks(MaxTracks),m_iMinTracks(MinTracks)  {}
  virtual ~VertexTracksCut() {}
  
protected:    
  virtual bool PassCut(const D3PDReader::Vertex* vertex)
  {
    if((m_iMinTracks <= (unsigned int)vertex->nTracks()) && ((unsigned int)vertex->nTracks() <= m_iMaxTracks))
      return true;
    else
      return false;
  }
    
private:
  unsigned int m_iMaxTracks;
  unsigned int m_iMinTracks;
};

#endif // AnalysisUtilities_VertexTracksCut_H
