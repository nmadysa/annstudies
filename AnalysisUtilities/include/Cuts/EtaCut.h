#ifndef AnalysisModul_EtaCut_H
#define AnalysisModul_EtaCut_H

// system
#include "assert.h"

// STL
#include <vector>
#include <math.h>
#include <algorithm>
#include <iterator>

// custom header
#include "Particle.h"
#include "CutWrapper.h"

#include "Electron.h"
#include "Jet.h"
/**
 * @short \f$\eta\f$ cut
 *
 * applies m_fMinEta < abs(Particle->TLV().Eta()) < m_fMaxEta
 *
 * and exclude regions between [m_vExcludeEtas.at(k),m_vExcludeEtas.at(k+1)], k = 0, 2, 4 ...
 *
 * The input vector for the excluded eta regions is expected to be sort ascendingly and has
 * an even number of elements, e.g. vExcludeEtas = {0,0.2, 1.3,1.4, 4,4.5}.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

class EtaCut : public CutWrapper<const Particle>
{
public:
  /// standard constructor taking the name and the limits of the eta cut
  EtaCut(std::string name,const std::vector<double>& vExcludeEtas,double MinEta = 0,
	 double MaxEta = 100):
    CutWrapper<const Particle>(name),m_fMaxEta(MaxEta),m_fMinEta(MinEta),
    m_vExcludeEtas(vExcludeEtas)
  {
    assert(m_fMinEta < m_fMaxEta);
    assert(m_vExcludeEtas.size() % 2 == 0);
  }

  /// standard destructor
  virtual ~EtaCut() {}

protected:
  /// applies the actual eta cut
  virtual bool PassCut(const Particle* particle)
  {
    float abseta = fabs(particle->TLV().Eta());
    // check for exclusion
    // particle is accepted if first value in m_vExcludeEtas which is greater
    // than abseta, is at an even position
    // eg. abseta = 1.5, m_vExcludeEtas = {0,0.2, 1.3,1.4, 4,4.5}
    std::vector<double>::iterator it = std::lower_bound(m_vExcludeEtas.begin(),
						        m_vExcludeEtas.end(),
						        abseta);
    if((m_fMinEta <= abseta) && (abseta < m_fMaxEta) &&
       ((int)distance(m_vExcludeEtas.begin(),it)%2 == 0))
      return true;
    else
      return false;
  }  
  
private:
  double               m_fMaxEta;           ///< upper limit
  double               m_fMinEta;           ///< lower limit
  std::vector<double>  m_vExcludeEtas;      ///< excluded regions (pairwise)
};

#endif // AnalysisModul_EtaCut_H
