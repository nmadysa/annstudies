#ifndef AnalysisUtilities_TauNumTrack_H
#define AnalysisUtilities_TauNumTrack_H

// STL include(s)
#include <string>

// system include(s)
#include <cmath>

// custom include(s)
#include "Tau.h"
#include "CutWrapper.h"


class TauNumTrack : public CutWrapper<const D3PDReader::Tau>
{
  public:
    TauNumTrack(std::string sName, int iMinTrack=-1):
      CutWrapper<const D3PDReader::Tau>(sName), m_iMinTrack(iMinTrack)
    {
    }
  
    TauNumTrack(std::string sName, const std::vector<int>& vAllowedNumTracks ):
      CutWrapper<const D3PDReader::Tau>(sName), m_vAllowedNumTracks(vAllowedNumTracks)
    {
      m_iMinTrack=-1;
    }

    virtual ~TauNumTrack() {}

  protected:
    virtual bool PassCut(const D3PDReader::Tau* pTau)
    {
    
      // minimal number of tracks 
      if (m_iMinTrack > -1)
        return pTau->numTrack() >= m_iMinTrack;

      // allow only a exact number of tracks
      if (m_vAllowedNumTracks.size()>0)
        if(std::find(m_vAllowedNumTracks.begin(),m_vAllowedNumTracks.end(), pTau->numTrack()) != m_vAllowedNumTracks.end())
          return true;
 
      return false;
    }

  private:
    int m_iMinTrack;
    std::vector<int> m_vAllowedNumTracks;
};

#endif // AnalysisUtilities_TauNumTrack_H
