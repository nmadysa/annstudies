#ifndef AnalysisModul_ElBLayerCut_H
#define AnalysisModul_ElBLayerCut_H

// system include(s)
#include <string>

// custom include(s)
#include "Electron.h"
#include "CutWrapper.h"


class ElBLayerCut : public CutWrapper<const D3PDReader::Electron>
{
public:
  ElBLayerCut(std::string sName):
    CutWrapper<const D3PDReader::Electron>(sName)
  {}

  virtual ~ElBLayerCut() {}

protected:
  virtual bool PassCut(const D3PDReader::Electron* pElectron)
  {
    return ((!pElectron->expectBLayerHit()) || (pElectron->nBLHits()>0));
  }
};

#endif // AnalysisModul_ElBLayerCut_H
