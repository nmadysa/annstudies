#ifndef AnalysisUtilities_BaseTauIDCut_H
#define AnalysisUtilities_BaseTauIDCut_H

// STL include(s)
#include <string>

#include "Constants.h"
#include "Tau.h"

#include "TFile.h"
#include "TGraph.h"
#include "TH2F.h"

class TauIDCut{
public:
  TauIDCut(){};
  virtual ~TauIDCut(){};

  virtual bool PassCut(const D3PDReader::Tau* pTau, const int& level, const float& min = 0.) = 0;
  enum ID{LLHLOOSE, LLHMEDIUM, LLHTIGHT, BDTLOOSE, BDTMEDIUM, BDTTIGHT};
  virtual ID GetID(const unsigned int& level) = 0;
};

class TauLLHCut : public TauIDCut
{
public:
  TauLLHCut():
    TauIDCut()
  {
    TFile* m_fCuts = TFile::Open((std::string(getenv("PWD")) + "/../../AnalysisUtilities/share/ParametrizedLLHSelection.root").c_str(), "READ");
    m_mCuts["loose_1p"] = ((TGraph*)m_fCuts->Get("1prong/loose"));
    m_mCuts["medium_1p"] = (TGraph*)m_fCuts->Get("1prong/medium");
    m_mCuts["tight_1p"] = (TGraph*)m_fCuts->Get("1prong/tight");
    m_mCuts["loose_3p"] = (TGraph*)m_fCuts->Get("3prong/loose");
    m_mCuts["medium_3p"] = (TGraph*)m_fCuts->Get("3prong/medium");
    m_mCuts["tight_3p"] = (TGraph*)m_fCuts->Get("3prong/tight");
  }
  
  virtual ~TauLLHCut() {}
  
  bool PassCut(const D3PDReader::Tau* pTau, const int& level, const float& min =0)
    {
      float cut = 1111.;
      float pT = pTau->TLV().Pt()/1000.;
      switch(level)
	{
	case 0:;
	  pTau->numTrack() <= 1 ? cut = m_mCuts["loose_1p"]->Eval(pT) : cut = m_mCuts["loose_3p"]->Eval(pT);
          #ifdef DEBUG
	  if (pTau->likelihood() > cut) assert(pTau->tauLlhLoose());
          #endif
	  return (pTau->likelihood() > cut);
	  break;
        case 1:
	  pTau->numTrack() <= 1 ? cut = m_mCuts["medium_1p"]->Eval(pT) : cut = m_mCuts["medium_3p"]->Eval(pT);
	  #ifdef DEBUG
	  if (pTau->likelihood() > cut) assert(pTau->tauLlhMedium());
	  #endif
	  return (pTau->likelihood() > cut);
	  break;
        case 2:
	  pTau->numTrack() <= 1 ? cut = m_mCuts["tight_1p"]->Eval(pT) : cut = m_mCuts["tight_3p"]->Eval(pT);
	  #ifdef DEBUG
	  if (pTau->likelihood() > cut) assert(pTau->tauLlhTight());
	  #endif
	  return (pTau->likelihood() > cut);
	  break;
	case 3:
	  return (pTau->tauLlhLoose() == 1);
	  break;
	case 4:
	  return (pTau->tauLlhMedium() == 1);
	  break;
	case 5:
	  return (pTau->tauLlhTight() == 1);
	  break;
	case 6:
	  return (pTau->likelihood() > min);
	  break;

        default:
          return false;
	}
    }

  ID GetID(const unsigned int& level){
    if(level < 3)
      return (ID(level));
    else
      return (ID(level - 3));
  }

private:
  std::map<std::string, TGraph*> m_mCuts;
};

class TauBDTCut : public TauIDCut
{
public:
  TauBDTCut():
    TauIDCut()
  {
    TFile* m_fCuts = TFile::Open((std::string(getenv("PWD")) + "/../../AnalysisUtilities/share/ParametrizedBDTSelection.root").c_str(), "READ");
    m_mCuts["loose_1p"] = ((TGraph*)m_fCuts->Get("loose_1p"));
    m_mCuts["medium_1p"] = (TGraph*)m_fCuts->Get("medium_1p");
    m_mCuts["tight_1p"] = (TGraph*)m_fCuts->Get("tight_1p");
    m_mCuts["loose_3p"] = (TGraph*)m_fCuts->Get("loose_3p");
    m_mCuts["medium_3p"] = (TGraph*)m_fCuts->Get("medium_3p");
    m_mCuts["tight_3p"] = (TGraph*)m_fCuts->Get("tight_3p");
  }
  
  virtual ~TauBDTCut() {}
  
  bool PassCut(const D3PDReader::Tau* pTau, const int& level, const float& min = 0)
    {
      float cut = 1111.;
      switch(level)
	{
      case 0:
	pTau->numTrack() <= 1 ? cut = m_mCuts["loose_1p"]->Eval(pTau->TLV().Pt()) : cut = m_mCuts["loose_3p"]->Eval(pTau->TLV().Pt());
	  return (pTau->BDTJetScore() > cut);
	  break;
        case 1:
	  pTau->numTrack() <= 1 ? cut = m_mCuts["medium_1p"]->Eval(pTau->TLV().Pt()) : cut = m_mCuts["medium_3p"]->Eval(pTau->TLV().Pt());
	  return (pTau->BDTJetScore() > cut);
	  break;
        case 2:
	  pTau->numTrack() <= 1 ? cut = m_mCuts["tight_1p"]->Eval(pTau->TLV().Pt()) : cut = m_mCuts["tight_3p"]->Eval(pTau->TLV().Pt());
	  return (pTau->BDTJetScore() > cut);
	  break;
	case 3:
	  return (pTau->JetBDTSigLoose() == 1);
	  break;
	case 4:
	  return (pTau->JetBDTSigMedium() == 1);
	  break;
	case 5:
	  return (pTau->JetBDTSigTight() == 1);
	  break;
	case 6:
	  return (pTau->BDTJetScore() > min);
	  break;
	default:
          return false;
	}
    }
  ID GetID(const unsigned int& level){
    if(level < 3)
      return (ID(level+3));
    else
      return (ID(level));
  }
  
private:
  std::map<std::string, TGraph*> m_mCuts;
};

class TauBDTElVetoCut : public TauIDCut
{
public:
  TauBDTElVetoCut():
    TauIDCut()
  {
    TFile* m_fCuts = TFile::Open((std::string(getenv("PWD")) + "/../../AnalysisUtilities/share/ParametrizedEleBDTSelection.root").c_str(), "READ");
    m_mCuts["loose"] = ((TH2F*)m_fCuts->Get("h2_BDTEleDecision_pteta_loose"));
    m_mCuts["medium"] = ((TH2F*)m_fCuts->Get("h2_BDTEleDecision_pteta_medium"));
    m_mCuts["tight"] = ((TH2F*)m_fCuts->Get("h2_BDTEleDecision_pteta_tight"));
  }
  
  virtual ~TauBDTElVetoCut() {}
  
  bool PassCut(const D3PDReader::Tau* pTau, const int& level, const float& min = 1111)
    {
      float cut = 1111.;
      int bin = this->GetBin(pTau->TLV().Pt() / AnalysisUtilities_Constants::GeV, TMath::Abs(pTau->leadTrack_eta()));
      switch(level)
	{
	case 0:
	  cut = m_mCuts["loose"]->GetBinContent(bin);
	  return (pTau->BDTEleScore() > cut);
	  break;
        case 1:
	  cut = m_mCuts["medium"]->GetBinContent(bin);
	  return (pTau->BDTEleScore() > cut);
	  break;
        case 2:
	  cut = m_mCuts["tight"]->GetBinContent(bin);
	  return (pTau->BDTJetScore() > cut);
	  break;
	case 3:
	  return (pTau->BDTJetScore() > min);
	  break;	  
        default:
          return false;
	}
    }

  ID GetID(const unsigned int& level){
    return (ID(level+3));
  }

private:
  std::map<std::string, TH2F*> m_mCuts;
  int GetBin(const float& pt, const float& eta)  {
    if (pt <= 800 && eta <= 3.0)
      return m_mCuts["loose"]->FindBin(pt, eta);
    else if (pt <= 800 && eta > 3.0)
      return m_mCuts["loose"]->FindBin(pt, 2.9);
    else if(pt > 800 && eta <= 3.0)
      return m_mCuts["loose"]->FindBin(799, eta);
    else
      return m_mCuts["loose"]->FindBin(799, 2.9);
  }

};
#endif
