#ifndef AnalysisUtilities_VertexSelector_H
#define AnalysisUtilities_VertexSelector_H

// STL include(s)
#include <vector>

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "BaseObjectSelector.h"
class CycleBase;

// D3PDObjects include(s)
#include "Vertex.h"

class VertexSelector : public BaseObjectSelector<D3PDReader::Vertex>
{
public:
  VertexSelector(CycleBase* parent,const char* name);
  virtual ~VertexSelector() {};

  /// declare all histograms here and add all cuts
  virtual void BeginInputData(const SInputData& id) throw(SError);
  /// do the selection and fill before/after histograms
  virtual void operator()(std::vector<D3PDReader::Vertex*>& vVertices,float hist_weight = 1);

protected:
  //----------------------------------------------------------------------------
  // configurable properties
  //----------------------------------------------------------------------------
  bool                c_do_ntracks_cut;
  int                 c_ntracks_max;
  int                 c_ntracks_min;
  bool                c_do_type_cut;
  std::vector<int>    c_allowed_types;
  bool                c_do_z_cut;
  double              c_z_max;
  double              c_z_min;
};

#endif // AnalysisUtilities_VertexSelector_H
