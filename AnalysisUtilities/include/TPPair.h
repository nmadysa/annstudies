#ifndef AnalysisUtilities_TPPair_H
#define AnalysisUtilities_TPPair_H

// system include(s)
#include <utility>

// ROOT include(s)
#include "TLorentzVector.h"

// AnalysisUtilities include(s)
#include "Particle.h"

/**
 * @short small class describing a tag-and-probe pair
 * @ingroup utils
 *
 * This class encapsulates the basic four-vector properties of a tag-and-probe pair.
 * The ownership of the tag and probe objects is not transferred.
 *
 * @param T type of the tag object (has to be derived from Particle)
 * @param U type of the probe object (hase to be derived from Particle)
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<class T,class U = T>
class TPPair : public std::pair<T*,U*>
{
public:
  /// default constructor
  TPPair():std::pair<T*,U*>(0,0) {}

  /// initialize TP pair with objects
  TPPair(T& rTag,U& rProbe):
    std::pair<T*,U*>(&rTag,&rProbe)
  {}

  /// default destructor
  virtual ~TPPair() {}

  /// returns tag object
  T*         Tag() {return this->first;}
  
  /// returns tag object (const version)
  const T*   Tag() const {return this->first;}
  
  /// returns probe object
  U*         Probe() {return this->second;}
  
  /// returns probe object (const version)
  const U*   Probe() const {return this->second;}

  /// assigns new tag object
  void       SetTag(T& rTag)
  {
    this->first = &rTag;
  }
  
  /// assigns new probe object
  void       SetProbe(T& rProbe)
  {
    this->second = &rProbe;
  }

  /// returns invariant mass of TP pair
  float      M() const {return (Tag()->TLV() + Probe()->TLV()).M();}
  
  /// returns combined four-vector of TP pair
  TLorentzVector   TLV() const {return (Tag()->TLV() + Probe()->TLV());}
};

#endif // AnalysisUtilities_TPPair_H
