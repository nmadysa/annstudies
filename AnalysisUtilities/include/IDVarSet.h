#ifndef SKIM
#ifndef IDVARSET_H_
#define IDVARSET_H_

class IDVarSet {
public:
  ~IDVarSet(){
    dRChrgPi.clear();
    dRNeutPi.clear();
    dRNeutral.clear();
    dRCharged.clear();
    dROuterChrg.clear();
    dROuterNeut.clear();
    dRNeutHLV.clear();
    dRChrgHLV.clear();
    dRNeutLowAHLV.clear();
    dRNeutLowBHLV.clear();
  };

  float nChrg0204;
  float nChrg01;
  float nChrg02;
  float nChrg04;
  float nNeut0204;
  float nNeut01;
  float nNeut02;
  float nNeut04;
  float massChrgSys01;
  float massChrgSys02;
  float massChrgSys04;
  float massNeutSys01;
  float massNeutSys02;
  float massNeutSys04;
  float dRmax01;
  float dRmax02;
  float dRmax04;
  float visTauM01;
  float visTauM02;
  float visTauM04;
  float ipSigLeadTrk;
  float trFlightPathSig;
  float ptRatio01;
  float ptRatio02;
  float ptRatio04;
  float ptRatioNeut01;
  float ptRatioNeut02;
  float ptRatioNeut04;
  float ptRatioChrg01;
  float ptRatioChrg02;
  float ptRatioChrg04;
  float fLeadChrg01;
  float fLeadChrg02;
  float fLeadChrg04;
  float eFrac0102;
  float eFrac0204;
  float eFrac0104;
  float eFracChrg0102;
  float eFracChrg0204;
  float eFracChrg0104;
  float eFracNeut0102;
  float eFracNeut0204;
  float eFracNeut0104;
  float eFrac010201;
  float eFrac010202;
  float eFrac010204;
  float eFracChrg010201;
  float eFracChrg010202;
  float eFracChrg010204;
  float eFracNeut010201;
  float eFracNeut010202;
  float eFracNeut010204;
  float eFrac020401;
  float eFrac020402;
  float eFrac020404;
  float eFracChrg020401;
  float eFracChrg020402;
  float eFracChrg020404;
  float eFracNeut020401;
  float eFracNeut020402;
  float eFracNeut020404;
  float rCal01;
  float rCal02;
  float rCal04;
  float rCalChrg01;
  float rCalChrg02;
  float rCalChrg04;
  float rCalNeut01;
  float rCalNeut02;
  float rCalNeut04;
  float dRminmaxPtChrg04;
  std::vector<float> dRChrgPi;
  std::vector<float> dRNeutPi;
  std::vector<float> dRCharged;
  std::vector<float> dRNeutral;
  std::vector<float> dROuterNeut;
  std::vector<float> dROuterChrg;
  std::vector<float> dRNeutHLV;
  std::vector<float> dRChrgHLV;
  std::vector<float> dRNeutLowAHLV;
  std::vector<float> dRNeutLowBHLV;
};

#endif
#endif
