#ifndef AnalysisUtilities_Utilities_H
#define AnalysisUtilities_Utilities_H

// STL include(s)
#include <vector>
#include <utility>
#include <map>

// SFrame include(s)
#include "core/include/SCycleBase.h"

// ROOT include(s)
#include "TLorentzVector.h"

// core include(s)
#include "ToolBase.h"
#include "Particle.h"

// D3PD objects include(s)
#include "Electron.h"
#include "Muon.h"
#include "Jet.h"

/**
 * @short namespace containing useful helper functions
 * @ingroup utils
 *
 * This namespace provides some useful helper functions.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
namespace Utilities
{
  /// apply overlap removal between physics objects of different types
  template<class T,class U>
  int OverlapRemoval(const std::vector<T*>& rFirst,std::vector<U*>& rSecond,float fDeltaR);

  /// apply overlap removal between physics objects of the same type
  template<class T>
  int OverlapRemoval(std::vector<T*>& rFirst,float fDeltaR);

  /// apply a DeltaR matching between physics objects and a TLV
  template<class T>
  int DeltaRMatching(std::vector<T*>& rObjects,const TLorentzVector& rTLV,float fDeltaR,bool bInvert = false);

  /// count physics objects which matches a TLV within a given DeltaR
  template<class T>
  int CountDeltaRMatches(const std::vector<T*>& rObjects,const TLorentzVector& rTLV,float fDeltaR,bool bInvert = false);

  /// apply a DeltaR matching between two sets of physics objects
  template<class T,class U>
  int DeltaRMatching(const std::vector<T*>& rFirst,std::vector<U*>& rSecond,float fDeltaR);

  /// apply specialised overlap removal between electrons
  int OverlapRemovalEl(std::vector<D3PDReader::Electron*>& electron,float fDeltaRCut);

  /// apply specialised overlap removal between electrons and muons
  int OverlapRemovalMuEl(const std::vector<D3PDReader::Muon*>& vMuons,std::vector<D3PDReader::Electron*>& vElectrons, float fDeltaRCut);

  /// apply specialised overlap removal between electrons and jets
  int OverlapRemovalElJet(const std::vector<D3PDReader::Electron*>& vElectrons,std::vector<D3PDReader::Jet*>& vJets, float fDeltaRCut);

  /// apply specialised overlap removal between muons and jets
  int OverlapRemovalMuJet(const std::vector<D3PDReader::Muon*>& vMuons,std::vector<D3PDReader::Jet*>& vJets, float fDeltaRCut);

  /// expand relative path names
  void SetFullPathName(std::string& sFilename);

  /// expand relative path names
  std::string GetFullPathName(const std::string& sFilename);

  /// clear pointer map
  template<class T, class U>
  void DeleteMap(std::map<T, U*>& mMap);
}

#include "Utilities.icc"

#endif // AnalysisUtilities_Utilities_H
