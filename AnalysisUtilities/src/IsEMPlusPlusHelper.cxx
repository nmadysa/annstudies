#include "Cuts/IsEMPlusPlusHelper.h"
#include "Cuts/IsEMPlusPlusDefs.h"
#include "Cuts/egammaPIDdefs.h"
// ====================================================================
bool isEMPlusPlusHelper::IsLoosePlusPlus0(const D3PDReader::Electron* eg, egammaMenu::egMenu menu,
					 bool debug,
					 bool isTrigger){
  
    
    if (!eg) {
      std::cout << "Failed, no egamma object. " << std::endl;
      return false; 
    }

    float eta = fabs(eg->etas2());
    float et = eg->cl_E()/cosh(eta);
    float raphad = eg->Ethad()/(eg->cl_E()/cosh(eta));
    float raphad1 = eg->Ethad1()/(eg->cl_E()/cosh(eta));
    float Reta = eg->reta();
    float weta2c = eg->weta2();
    float f1 = eg->f1();
    float wtot = eg->wstot();
    float demaxs1 = (eg->emaxs1() - eg->Emax2())/(eg->emaxs1() + eg->Emax2());
    float deltaEta = eg->deltaeta1();
    int nSi = eg->nSiHits();
    int nSiOutliers = eg->nSCTOutliers() + eg->nPixelOutliers();
    int nPi = eg->nPixHits();
    int nPiOutliers = eg->nPixelOutliers();
    
    bool result = isLoosePlusPlus0(eta,et,
				   raphad,raphad1,Reta,weta2c,
				   f1,wtot,demaxs1,deltaEta,nSi,nSiOutliers,
				   nPi, nPiOutliers,
				   menu,debug,isTrigger);
    if (debug)
    {

      std::cout << "rhad= "     << raphad  << "  " << raphad1 << std::endl;
      std::cout << "reta= "     << Reta  << std::endl;
      std::cout << "wstot= "    << wtot  << std::endl;
      std::cout << "weta2= "    << weta2c  << std::endl;
      std::cout << "eratio= "   << demaxs1  << std::endl;
      std::cout << "npix= "     << nPi + nPiOutliers  << std::endl;
      std::cout << "nsi= "      << nSi + nSiOutliers  << std::endl; 
      std::cout << "deltaEta= "      << deltaEta  << std::endl;
    }
				  
    return result;
}



// ====================================================================
bool isEMPlusPlusHelper::IsLoosePlusPlus1(const D3PDReader::Electron* eg, egammaMenu::egMenu menu,
					 bool debug,
					 bool isTrigger){
  
    
    if (!eg) {
      std::cout << "Failed, no egamma object. " << std::endl;
      return false; 
    }

    float eta = fabs(eg->etas2());
    float et = eg->cl_E()/cosh(eta);
    float raphad = eg->Ethad()/(eg->cl_E()/cosh(eta));
    float raphad1 = eg->Ethad1()/(eg->cl_E()/cosh(eta));
    float Reta = eg->reta();
    float weta2c = eg->weta2();
    float f1 = eg->f1();
    float wtot = eg->wstot();
    float demaxs1 = (eg->emaxs1() - eg->Emax2())/(eg->emaxs1() + eg->Emax2());
    float deltaEta = eg->deltaeta1();
    int nSi = eg->nSiHits();
    int nSiOutliers = eg->nSCTOutliers() + eg->nPixelOutliers();
    int nPi = eg->nPixHits();
    int nPiOutliers = eg->nPixelOutliers();

    bool result = isLoosePlusPlus1(eta,et,
				   raphad,raphad1,Reta,weta2c,
				   f1,wtot,demaxs1,deltaEta,nSi,nSiOutliers,
				   nPi, nPiOutliers,
				   menu,debug,isTrigger);
				  
    if (debug)
    {

      std::cout << "rhad= "     << raphad  << "  " << raphad1 << std::endl;
      std::cout << "reta= "     << Reta  << std::endl;
      std::cout << "wstot= "    << wtot  << std::endl;
      std::cout << "weta2= "    << weta2c  << std::endl;
      std::cout << "eratio= "   << demaxs1  << std::endl;
      std::cout << "npix= "     << nPi + nPiOutliers  << std::endl;
      std::cout << "nsi= "      << nSi + nSiOutliers  << std::endl; 
      std::cout << "deltaEta= "      << deltaEta  << std::endl;
    }

    return result;
}


// ====================================================================
bool isEMPlusPlusHelper::IsLoosePlusPlus(const D3PDReader::Electron* eg, egammaMenu::egMenu menu, 
					 bool debug,
					 bool isTrigger){
  
    
    if (!eg) {
      std::cout << "Failed, no egamma object. " << std::endl;
      return false; 
    }

    float eta = fabs(eg->etas2());
    float et = eg->cl_E()/cosh(eta);
    float raphad = eg->Ethad()/(eg->cl_E()/cosh(eta));
    float raphad1 = eg->Ethad1()/(eg->cl_E()/cosh(eta));
    float Reta = eg->reta();
    float weta2c = eg->weta2();
    float f1 = eg->f1();
    float wtot = eg->wstot();
    float demaxs1 = (eg->emaxs1() - eg->Emax2())/(eg->emaxs1() + eg->Emax2());
    float deltaEta = eg->deltaeta1();
    int nSi = eg->nSiHits();
    int nSiOutliers = eg->nSCTOutliers() + eg->nPixelOutliers();
    int nPi = eg->nPixHits();
    int nPiOutliers = eg->nPixelOutliers();

    bool result = isLoosePlusPlus(eta,et,
				  raphad,raphad1,Reta,weta2c,
				  f1,wtot,demaxs1,deltaEta,nSi,nSiOutliers,
				  nPi, nPiOutliers,
				  menu, debug,isTrigger);
				  
    if (debug)
    {

      std::cout << "rhad= "     << raphad  << "  " << raphad1 << std::endl;
      std::cout << "reta= "     << Reta  << std::endl;
      std::cout << "wstot= "    << wtot  << std::endl;
      std::cout << "weta2= "    << weta2c  << std::endl;
      std::cout << "eratio= "   << demaxs1  << std::endl;
      std::cout << "npix= "     << nPi + nPiOutliers  << std::endl;
      std::cout << "nsi= "      << nSi + nSiOutliers  << std::endl; 
      std::cout << "deltaEta= "      << deltaEta  << std::endl;
    }

    return result;
}

// ====================================================================
bool isEMPlusPlusHelper::IsMediumPlusPlus(const D3PDReader::Electron* eg, egammaMenu::egMenu menu, 
					  bool debug,
					  bool isTrigger){

    float eta = fabs(eg->etas2());
    float et = eg->cl_E()/cosh(eta);
    float raphad = eg->Ethad()/(eg->cl_E()/cosh(eta));
    float raphad1 = eg->Ethad1()/(eg->cl_E()/cosh(eta));
    float Reta = eg->reta();
    float weta2c = eg->weta2();
    float f1 = eg->f1();
    float f3 = eg->f3();
    float wtot = eg->wstot();
    float demaxs1 = (eg->emaxs1() - eg->Emax2())/(eg->emaxs1() + eg->Emax2());
    float deltaEta = eg->deltaeta1();
    float trackd0 = eg->trackd0_physics();
    int nSi = eg->nSiHits();
    int nSiOutliers = eg->nSCTOutliers() + eg->nPixelOutliers();
    int nPi = eg->nPixHits();
    int nPiOutliers = eg->nPixelOutliers();
    int nBL = eg->nBLHits();
    int nBLOutliers = eg->nBLayerOutliers();
    bool expectHitInBLayer = eg->expectHitInBLayer();
    int nTRT = eg->nTRTHits();
    int nTRTOutliers = eg->nTRTOutliers();
    float rTRT = eg->TRTHighTOutliersRatio();
    
    bool result = isMediumPlusPlus(eta,et,f3,
				   raphad,raphad1,Reta,weta2c,
				   f1,wtot,demaxs1,deltaEta,trackd0,
				   rTRT,nTRT,nTRTOutliers,
				   nSi,nSiOutliers,nPi,nPiOutliers,
				   nBL,nBLOutliers,expectHitInBLayer, 
				   menu, debug,isTrigger);


    if (debug)
    {

      std::cout << "rhad= "     << raphad  << "  " << raphad1 << std::endl;
      std::cout << "reta= "     << Reta  << std::endl;
      std::cout << "wstot= "    << wtot  << std::endl;
      std::cout << "weta2= "    << weta2c  << std::endl;
      std::cout << "eratio= "   << demaxs1  << std::endl;
      std::cout << "deltaeta= " << deltaEta  << std::endl;
      std::cout << "npix= "     << nPi + nPiOutliers  << std::endl;
      std::cout << "nsi= "      << nSi + nSiOutliers  << std::endl;
      std::cout << "nbl= "      << nBL + nBLOutliers  << std::endl;
      std::cout << "A0= "       << trackd0  << std::endl;
      std::cout << "TR = "      << rTRT << std::endl;

    } 

    return result;
}
// ====================================================================
bool isEMPlusPlusHelper::IsTightPlusPlus(const D3PDReader::Electron* eg, egammaMenu::egMenu menu,  
					  bool debug,
					  bool isTrigger){
    
    float eta = fabs(eg->etas2());
    float et = eg->cl_E()/cosh(eta);
    float raphad = eg->Ethad()/(eg->cl_E()/cosh(eta));
    float raphad1 = eg->Ethad1()/(eg->cl_E()/cosh(eta));
    float Reta = eg->reta();
    float weta2c = eg->weta2();
    float f1 = eg->f1();
    float f3 = eg->f3();
    float wtot = eg->wstot();
    float demaxs1 = (eg->emaxs1() - eg->Emax2())/(eg->emaxs1() + eg->Emax2());
    float deltaEta = eg->deltaeta1();
    float trackd0 = eg->trackd0_physics();
    int nSi = eg->nSiHits();
    int nSiOutliers = eg->nSCTOutliers() + eg->nPixelOutliers();
    int nPi = eg->nPixHits();
    int nPiOutliers = eg->nPixelOutliers();
    int nBL = eg->nBLHits();
    int nBLOutliers = eg->nBLayerOutliers();
    bool expectHitInBLayer = eg->expectHitInBLayer();
    int nTRT = eg->nTRTHits();
    int nTRTOutliers = eg->nTRTOutliers();
    float rTRT = eg->TRTHighTOutliersRatio();
    float deltaphi = eg->deltaphi2();
    float ep = eg->cl_E() * fabs(eg->trackqoverp());
    int convBit = eg->isEM() & (1 << egammaPID::ConversionMatch_Electron);

    bool result = isTightPlusPlus(eta,et,f3, 
				   raphad,raphad1,Reta,weta2c,
				   f1,wtot,demaxs1,deltaEta,trackd0,
				   rTRT,nTRT,nTRTOutliers,
				   nSi,nSiOutliers,nPi,nPiOutliers,
				   nBL,nBLOutliers,expectHitInBLayer, ep, deltaphi,
				   convBit, menu, debug, isTrigger);
	
    if (debug)
    {

      std::cout << "rhad= "     << raphad  << "  " << raphad1 << std::endl;
      std::cout << "reta= "     << Reta  << std::endl;
      std::cout << "wstot= "    << wtot  << std::endl;
      std::cout << "weta2= "    << weta2c  << std::endl;
      std::cout << "eratio= "   << demaxs1  << std::endl;
      std::cout << "deltaphi= " << deltaphi  << std::endl;
      std::cout << "deltaeta= " << deltaEta  << std::endl;
      std::cout << "ep= "       << ep  << std::endl;
      std::cout << "npix= "     << nPi + nPiOutliers  << std::endl;
      std::cout << "nsi= "      << nSi + nSiOutliers  << std::endl;
      std::cout << "nbl= "      << nBL + nBLOutliers  << std::endl;
      std::cout << "A0= "       << trackd0  << std::endl;
      std::cout << "Conv = "    << convBit << std::endl;
      std::cout << "TR = "      << rTRT << std::endl;
      std::cout << "nTRT = "      << nTRT + nTRTOutliers << std::endl;
    } 


    return result;
}
#ifndef TAU_D3PD
#ifdef FIXED
// ====================================================================
bool isEMPlusPlusHelper::IsMultiLepton(const D3PDReader::Electron* eg){

  double Et_cl            = eg->cl_E()/cosh(eg->etas2());
  double etas2            = eg->etas2();
  double rHad             = eg->Ethad()/Et_cl;
  double rHad1            = eg->Ethad1()/Et_cl;
  double Reta             = eg->reta();
  double w2               = eg->weta2();
  double f1               = eg->f1();
  double f3               = eg->f3();
  double wstot            = eg->wstot();
  double DEmaxs1          = fabs(eg->emaxs1()+eg->Emax2())>0. ? (eg->emaxs1()- eg->Emax2())/(eg->emaxs1()+eg->Emax2()) : 0.;
  double deltaEta         = eg->deltaeta1();
  double deltaPhiRescaled = eg->deltaphiRescaled();
  int    nSi              = eg->nSiHits();
  int    nSiDeadSensors   = eg->nSCTDeadSensors() + eg->nPixelDeadSensors();
  int    nPix             = eg->nPixHits();
  int    nPixDeadSensors  = eg->nPixelDeadSensors();
  int    nTRThigh         = eg->nTRTHighTHits();
  int    nTRThighOutliers = eg->nTRTHighTOutliers();
  int    nTRT             = eg->nTRTHits();  
  int    nTRTOutliers     = eg->nTRTOutliers(); 
  bool   expectBlayer     = eg->expectBLayerHit(); 
  int    nBlayerHits      = eg->nBLHits();

  double dpOverp =0;
  for (unsigned int i = 0; i < eg->refittedTrack_LMqoverp().size();++i) {
    if((eg->refittedTrack_author()).at(i)==4){
      dpOverp= 1-(eg->trackqoverp()/(eg->refittedTrack_LMqoverp().at(i)));
    }
  }

  double rTRT = 0.;
  rTRT = (nTRT+nTRTOutliers) > 0 ?  ((double) (nTRThigh+nTRThighOutliers)/(nTRT+nTRTOutliers) ) : 0.;
  int nTRTTotal = nTRT+nTRTOutliers;


  return passMultiLepton(etas2, Et_cl,
                         rHad, rHad1, Reta, w2,
                         f1,f3, wstot, DEmaxs1, 
			                   deltaEta, nSi,nSiDeadSensors, nPix, 
			                   nPixDeadSensors,deltaPhiRescaled,dpOverp,
			                   rTRT,nTRTTotal,nBlayerHits,expectBlayer);
}
#endif
#endif //Tau_D3PD
