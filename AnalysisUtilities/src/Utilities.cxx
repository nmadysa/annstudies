#ifndef AnalysisModul_Utilities_CXX
#define AnalysisModul_Utilities_CXX

//STL includes
#include <cstdlib>

//boost includes
#ifndef __CINT__
#include "boost/algorithm/string.hpp"
#endif

// AnalysisModul include(s)
#include "Utilities.h"

int Utilities::OverlapRemovalEl(std::vector<D3PDReader::Electron*>& vElectrons,float fDeltaRCut)
{
  // same template types
  int iRemoved = 0;
  std::sort(vElectrons.begin(),vElectrons.end(),Particle::pt_comp); // to remove the lowest pt electron
  std::vector<D3PDReader::Electron*>::iterator it = vElectrons.begin();
  std::vector<D3PDReader::Electron*>::iterator sec;
  
  TLorentzVector el1_tlv;
  TLorentzVector el2_tlv;

  while(it != vElectrons.end())
  {
    sec = it + 1;
    el1_tlv.SetPtEtaPhiM( (*it)->cl_E()/cosh( (*it)->tracketa()), (*it)->cl_eta(), (*it)->cl_phi(), (*it)->m());
    while(sec != vElectrons.end())
    {
      el2_tlv.SetPtEtaPhiM( (*sec)->cl_E()/cosh( (*sec)->tracketa()), (*sec)->cl_eta(), (*sec)->cl_phi(), (*sec)->m());
      if(el1_tlv.DeltaR(el2_tlv) < fDeltaRCut)
      {
	++iRemoved;
	sec = vElectrons.erase(sec);
      }
      else
	++sec;
    }
    ++it;
  }
  return iRemoved;
}

int Utilities::OverlapRemovalMuEl(const std::vector<D3PDReader::Muon*>& vMuons,std::vector<D3PDReader::Electron*>& vElectrons, float fDeltaRCut)
{
  int iRemoved = 0;
  std::vector<D3PDReader::Muon*>::const_iterator it = vMuons.begin();
  std::vector<D3PDReader::Electron*>::iterator sec;
  TLorentzVector mu_tlv;
  TLorentzVector el_tlv;

  while(it != vMuons.end())
  {
    mu_tlv = (*it)->TLV();
    mu_tlv.SetPhi((*it)->id_phi());
    mu_tlv.SetTheta((*it)->id_theta());

    sec = vElectrons.begin();
    while(sec != vElectrons.end())
    {
      el_tlv = (*sec)->TLV();
      el_tlv.SetPtEtaPhiM((*sec)->pt(),(*sec)->tracketa(),(*sec)->trackphi(),(*sec)->m());

      if(mu_tlv.DeltaR(el_tlv) < fDeltaRCut)
      {
	++iRemoved;
	sec = vElectrons.erase(sec);
      }
      else
	++sec;
    }
    ++it;
  };

  return iRemoved;
}

int Utilities::OverlapRemovalElJet(const std::vector<D3PDReader::Electron*>& vElectrons,std::vector<D3PDReader::Jet*>& vJets, float fDeltaRCut)
{
  int iRemoved = 0;
  std::vector<D3PDReader::Electron*>::const_iterator it = vElectrons.begin();
  std::vector<D3PDReader::Jet*>::iterator sec;
  TLorentzVector el_tlv;
  TLorentzVector jet_tlv;

  while(it != vElectrons.end())
  {
    el_tlv.SetPtEtaPhiM( (*it)->cl_E()/cosh( (*it)->tracketa()), (*it)->cl_eta(), (*it)->cl_phi(), (*it)->m());

    sec = vJets.begin();
    while(sec != vJets.end())
    {
      jet_tlv.SetPtEtaPhiE((*sec)->pt(),(*sec)->emscale_eta(),(*sec)->emscale_phi(),(*sec)->E());

      if(el_tlv.DeltaR(jet_tlv) < fDeltaRCut)
      {
	++iRemoved;
	sec = vJets.erase(sec);
      }
      else
	++sec;
    }
    ++it;
  };

  return iRemoved;
}

int Utilities::OverlapRemovalMuJet(const std::vector<D3PDReader::Muon*>& vMuons,std::vector<D3PDReader::Jet*>& vJets, float fDeltaRCut)
{
  int iRemoved = 0;
  std::vector<D3PDReader::Muon*>::const_iterator it = vMuons.begin();
  std::vector<D3PDReader::Jet*>::iterator sec;
  TLorentzVector mu_tlv;
  TLorentzVector jet_tlv;

  while(it != vMuons.end())
  {
    mu_tlv.SetPtEtaPhiM((*it)->pt(),(*it)->eta(),(*it)->phi(),(*it)->m());

    sec = vJets.begin();
    while(sec != vJets.end())
    {
      jet_tlv.SetPtEtaPhiE((*sec)->pt(),(*sec)->emscale_eta(),(*sec)->emscale_phi(),(*sec)->E());

      if(mu_tlv.DeltaR(jet_tlv) < fDeltaRCut)
      {
	++iRemoved;
	sec = vJets.erase(sec);
      }
      else
	++sec;
    }
    ++it;
  };

  return iRemoved;
}

std::string Utilities::GetFullPathName(const std::string& sFilename)
{
  static std::string cwd = std::string(getenv("PWD")) + "/";
  
  if(boost::starts_with(sFilename, "/"))
    return sFilename;
  else
    return cwd + sFilename;
}

void Utilities::SetFullPathName(std::string& sFilename)
{
  static std::string cwd = std::string(getenv("PWD")) + "/";
  
  if(!boost::starts_with(sFilename, "/"))
    sFilename = cwd + sFilename;
}

#endif // AnalysisModul_Utilities_ICC

