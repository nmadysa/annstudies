#ifndef AnalysisUtilities_ElectronSelector_CXX
#define AnalysisUtilities_ElectronSelector_CXX

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "Cuts/PTCut.h"
#include "Cuts/EtaCut.h"

// custom include(s)
#include "ElectronSelector.h"
#include "Electron.h"
#include "Cuts/ElQualityCut.h"
#include "Cuts/AuthorCut.h"
#include "Cuts/ElOQCut.h"
#include "Cuts/ChargeCut.h"
#include "Cuts/ElZ0Cut.h"
#include "Cuts/ElZ0ThetaCut.h"
#include "Cuts/ElD0SigCut.h"
#include "Cuts/ElBLayerCut.h"

ElectronSelector::ElectronSelector(CycleBase* pParent,const char* sName):
  BaseObjectSelector<D3PDReader::Electron>(pParent,sName)
{
  // declare configurable properties
  DeclareProperty(GetName() + "_do_eta_cut",c_do_eta_cut=false);
  DeclareProperty(GetName() + "_eta_min",c_eta_min=0);
  DeclareProperty(GetName() + "_eta_max",c_eta_max=100);
  DeclareProperty(GetName() + "_exclude_etas",c_exclude_etas);

  DeclareProperty(GetName() + "_do_pt_cut",c_do_pt_cut=false);
  DeclareProperty(GetName() + "_pt_min",c_pt_min=0);
  DeclareProperty(GetName() + "_pt_max",c_pt_max=1e12);

  DeclareProperty(GetName() + "_do_quality_cut",c_do_quality_cut=false);
  DeclareProperty(GetName() + "_quality_level",c_quality_level);

  DeclareProperty(GetName() + "_do_author_cut",c_do_author_cut=false);
  DeclareProperty(GetName() + "_allowed_authors",c_allowed_authors);

  DeclareProperty(GetName() + "_do_oq_cleaning",c_do_oq_cleaning=false);
  DeclareProperty(GetName() + "_oq_bit_mask",c_oq_bit_mask=1446);

  DeclareProperty(GetName() + "_do_charge_cut",c_do_charge_cut=false);
  DeclareProperty(GetName() + "_check_only_abs_charge",c_check_only_abs_charge=true);

  DeclareProperty(GetName() + "_do_z0_cut",c_do_z0_cut=false);
  DeclareProperty(GetName() + "_z0_pv",c_z0_pv);

  DeclareProperty(GetName() + "_do_z0theta_cut",c_do_z0theta_cut=false);
  DeclareProperty(GetName() + "_z0theta",c_z0theta);

  DeclareProperty(GetName() + "_do_d0_sig_cut", c_do_d0_sig_cut=false);
  DeclareProperty(GetName() + "_d0_offset", c_d0_offset=0);  
  DeclareProperty(GetName() + "_d0_sig",c_d0_sig);

  DeclareProperty(GetName() + "_do_blayer", c_do_blayer_cut=false);

  DeclareProperty(GetName() + "_use_unbiased_pv", c_use_unbiased_pv=false);
}

void ElectronSelector::BeginInputData(const SInputData& id) throw(SError)
{
  // which cuts shall be applied (order is important)
  m_vCuts.clear();
  if(c_do_author_cut)
    m_vCuts.push_back(new AuthorCut<const D3PDReader::Electron>("AUTHOR",c_allowed_authors));
  if(c_do_oq_cleaning)
    m_vCuts.push_back(new ElOQCut("OQ Cleaning",c_oq_bit_mask));
  if(c_do_pt_cut)
    m_vCuts.push_back(new PTCut("PT",c_pt_min,c_pt_max));
  if(c_do_eta_cut)
    m_vCuts.push_back(new EtaCut("ETA",c_exclude_etas,c_eta_min,c_eta_max));
  if(c_do_quality_cut)
    m_vCuts.push_back(new ElQualityCut("QUALITY",c_quality_level));
  if(c_do_blayer_cut)
    m_vCuts.push_back(new ElBLayerCut("B LAYER"));
  if(c_do_z0_cut)
    m_vCuts.push_back(new ElZ0Cut("Z0", c_z0_pv, c_use_unbiased_pv));
  if(c_do_z0theta_cut)
    m_vCuts.push_back(new ElZ0ThetaCut("Z0_Theta", c_z0theta,c_use_unbiased_pv));
  if(c_do_d0_sig_cut)
    m_vCuts.push_back(new ElD0SigCut("D0 Sig", c_d0_sig, c_use_unbiased_pv, c_d0_offset));
  if(c_do_charge_cut)
    m_vCuts.push_back(new ChargeCut<const D3PDReader::Electron>("CHARGE",1,c_check_only_abs_charge));

  // book histograms
  Book(TH1F("h_el_pt_pre","electrons before cut;p_{T,el};electrons / 2 GeV",100,0,200));
  Book(TH1F("h_el_pt","electrons after cut;p_{T,el};electrons / 2 GeV",100,0,200));
  Book(TH1F("h_el_pt_eff","electrons selection efficiency;p_{T,el};#varepsilon / 2 GeV",100,0,200));
  Book(TH1F("h_el_eta_pre","electrons before cut;#eta_{el};electrons / 0.1",100,-5,5));
  Book(TH1F("h_el_eta","electrons after cut;#eta_{el};electrons / 0.1",100,-5,5));
  Book(TH1F("h_el_eta_eff","electrons selection efficiency;#eta_{el};#varepsilon / 0.1",100,-5,5));
  Book(TH1F("h_el_phi_pre","electrons before cut;#phi_{el};electrons / #pi/6",12,-3.1416,3.1416));
  Book(TH1F("h_el_phi","electrons after cut;#phi_{el};electrons / #pi/6",12,-3.1416,3.1416));
  Book(TH1F("h_el_phi_eff","electrons selection efficiency;#phi_{el};#varepsilon / #pi/6",12,-3.1416,3.1416));

  BaseObjectSelector<D3PDReader::Electron>::BeginInputData(id);
}

//______________________________________________________________________________
void ElectronSelector::EndMasterInputData(const SInputData& id) throw(SError)
{
  // calculate selection efficiencies
  Hist("h_el_pt_eff")->Divide(Hist("h_el_pt"),Hist("h_el_pt_pre"),1,1,"B");
  Hist("h_el_eta_eff")->Divide(Hist("h_el_eta"),Hist("h_el_eta_pre"),1,1,"B");
  Hist("h_el_phi_eff")->Divide(Hist("h_el_phi"),Hist("h_el_phi_pre"),1,1,"B");

  BaseObjectSelector<D3PDReader::Electron>::EndMasterInputData(id);
}

//______________________________________________________________________________
void ElectronSelector::operator()(std::vector<D3PDReader::Electron*>& vElectrons,float fWeight)
{
  // fill kinematics before cuts
  for(std::vector<D3PDReader::Electron*>::iterator it = vElectrons.begin();
      it != vElectrons.end();++it)
  {
    Hist("h_el_pt_pre")->Fill((*it)->TLV().Pt()/1000,fWeight * (*it)->Weight());
    Hist("h_el_eta_pre")->Fill((*it)->TLV().Eta(),fWeight * (*it)->Weight());
    Hist("h_el_phi_pre")->Fill((*it)->TLV().Phi(),fWeight * (*it)->Weight());
  }

  // do actual cuts and fill cut flow (electrons failing at least one cut
  // are removed from vElectrons)
  BaseObjectSelector<D3PDReader::Electron>::operator()(vElectrons,fWeight);

  // fill kinematics after cuts
  for(std::vector<D3PDReader::Electron*>::iterator it = vElectrons.begin();
      it != vElectrons.end();++it)
  {
    Hist("h_el_pt")->Fill((*it)->TLV().Pt()/1000,fWeight * (*it)->Weight());
    Hist("h_el_eta")->Fill((*it)->TLV().Eta(),fWeight * (*it)->Weight());
    Hist("h_el_phi")->Fill((*it)->TLV().Phi(),fWeight * (*it)->Weight());
  }
}

#endif // AnalysisUtilities_ElectronSelector_CXX
