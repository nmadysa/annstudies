#ifndef AnalysisUtilities_JetCleaningTool_CXX
#define AnalysisUtilities_JetCleaningTool_CXX

// STL include(s)
#include <vector>
#include <math.h>

// SFrame include(s)
#include "core/include/SInputData.h"
#include "core/include/SError.h"

// core include(s)
#include "ToolBase.h"

// AnalysisUtilities include(s)
#include "JetCleaningTool.h"
#include "Cuts/JetCleaningCut.h"

//______________________________________________________________________________
JetCleaningTool::JetCleaningTool(CycleBase* parent,const char* name):
  ToolBase(parent,name)
{
  DeclareProperty(GetName()+"_hot_tile_runs", m_vHotTileRuns);
}

//______________________________________________________________________________
JetCleaningTool::~JetCleaningTool()
{}

//______________________________________________________________________________
void JetCleaningTool::BeginInputData(const SInputData& id) throw (SError)
{
  if(m_vHotTileRuns.size() == 0)
  {
    m_logger << WARNING << "No runs for hot tile check passed. Will switch off check" << SLogger::endmsg;
    //m_vHotTileRuns.push_back(0);
  }
  else
    std::sort(m_vHotTileRuns.begin(), m_vHotTileRuns.end());
}

//______________________________________________________________________________
bool JetCleaningTool::DoJetCleaning(const std::vector<D3PDReader::Jet*>& vJets, unsigned int lev) const
{
  std::vector<D3PDReader::Jet*>::const_iterator it = vJets.begin();
  bool isGood = true;
  
  for(; it != vJets.end(); ++it)
  {
    // pt of jet less than threshold for bad jet definition
    if ((*it)->TLV().Pt() < 20000)
      continue;

    switch(lev)
    {
    case LOOSER:
      isGood = isGood && JetCleaningCut::Looser(*it);
      break;
    case LOOSE:
      isGood = isGood && JetCleaningCut::Loose(*it);
      break;
    case MEDIUM:
      isGood = isGood && JetCleaningCut::Medium(*it,false);
      break;
    case MEDIUMTAU:
      isGood = isGood && JetCleaningCut::Medium(*it,true);
      break;
    case TIGHT:
      m_logger << WARNING << "tight jet cleaning not implemented yet" << SLogger::endmsg;
      return false;
    default:
      m_logger << ERROR << "unknown jet cleaning level" << SLogger::endmsg;
      throw SError(SError::StopExecution);
      break;
    }
    
    if(!isGood)
      return false; // at least one bad jet found
  }

  // no bad jet found
  return true;
}

//______________________________________________________________________________
bool JetCleaningTool::CheckHotTileProblem(const std::vector<D3PDReader::Jet*>& vJets,const unsigned int& RunNumber) const
{
  // check only affected runs
  if(std::find(m_vHotTileRuns.begin(),m_vHotTileRuns.end(),RunNumber) == m_vHotTileRuns.end())
    return true;

  float j_eta = 0;
  float j_phi = 0;
  
  for(std::vector<D3PDReader::Jet*>::const_iterator it = vJets.begin(); it != vJets.end(); ++it)
  {
    j_eta = (*it)->eta();
    j_phi = (*it)->phi();
    
    if(j_eta>-0.2 && j_eta<-0.1 && j_phi>2.65 && j_phi< 2.75 )
    {
      if((*it)->fracSamplingMax()>0.6 && (*it)->SamplingMax()==13)
	return false;
    }
  }
  
  return true;
}

//______________________________________________________________________________
bool JetCleaningTool::CheckFCalProblem(const std::vector<D3PDReader::Jet*>& vJets, const unsigned int& RunNumber) const
{
  if(RunNumber < 206248 || RunNumber > 207332)
    return true;
  
  std::vector<D3PDReader::Jet*>::const_iterator jet = vJets.begin();
  for(;jet != vJets.end(); ++jet){
    if((*jet)->TLV().Pt() < 20000.)
      continue;
    if(TMath::Abs((*jet)->eta()) > 3.2 && 1.6 < (*jet)->phi() && (*jet)->phi() < 3.1)
      return false;
  }
  return true;  
}

#endif // AnalysisUtilities_JetCleaningTool_CXX
