#ifndef AnalysisUtilities_TauSelector_CXX
#define AnalysisUtilities_TauSelector_CXX

#ifdef TAU_D3PD

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "Cuts/PTCut.h"
#include "Cuts/EtaCut.h"
//#include "Cuts/EtCut.h"

// AnalysisUtilities include(s)
#include "TauSelector.h"
#include "Cuts/AuthorCut.h"
#include "Cuts/TauNumTrack.h"
#include "Cuts/TauChargeCut.h"
#include "Cuts/TauIDCuts.h"
#include "Cuts/TauMuonVetoCut.h"
#include "Cuts/TauCrackVeto.h"

// D3PDObjects include(s)
#include "Tau.h"

TauSelector::TauSelector(CycleBase* pParent, const char* sName) :
BaseObjectSelector<D3PDReader::Tau>(pParent, sName) {
    // declare configurable properties
    DeclareProperty(GetName() + "_do_eta_cut", c_do_eta_cut = false);
    DeclareProperty(GetName() + "_eta_min", c_eta_min = 0);
    DeclareProperty(GetName() + "_eta_max", c_eta_max = 100);
    DeclareProperty(GetName() + "_exclude_etas", c_exclude_etas);

    DeclareProperty(GetName() + "_do_pt_cut", c_do_pt_cut = false);
    DeclareProperty(GetName() + "_pt_min", c_pt_min = 0);
    DeclareProperty(GetName() + "_pt_max", c_pt_max = 1e12);

    DeclareProperty(GetName() + "_do_author_cut", c_do_author_cut = false);
    DeclareProperty(GetName() + "_allowed_authors", c_allowed_authors);

    DeclareProperty(GetName() + "_do_minNumber_Tracks", c_do_min_number_of_tracks = false);
    DeclareProperty(GetName() + "_minNumber_Tracks", c_min_number_of_tracks = 0);

    DeclareProperty(GetName() + "_do_exactNumber_Tracks", c_do_exact_number_of_tracks = false);
    DeclareProperty(GetName() + "_exactNumber_Tracks", c_exact_number_of_tracks);

    DeclareProperty(GetName() + "_do_charge_cut", c_do_charge_cut = false);
    DeclareProperty(GetName() + "_abs_charge", c_abs_charge = 1);

    DeclareProperty(GetName() + "_do_cutBasedID", c_do_cutBasedID = false);
    DeclareProperty(GetName() + "_cutBasedID_level", c_cutBasedID_level = 0);

    DeclareProperty(GetName() + "_do_llhBasedID", c_do_llhBasedID = false);
    DeclareProperty(GetName() + "_llhBasedID_level", c_llhBasedID_level = 0);
    DeclareProperty(GetName() + "_llhBasedID_cut", c_llhBasedID_cut = 0);

    DeclareProperty(GetName() + "_do_bdtBasedID", c_do_bdtBasedID = false);
    DeclareProperty(GetName() + "_bdtBasedID_level", c_bdtBasedID_level = 0);
    DeclareProperty(GetName() + "_bdtBasedID_cut", c_bdtBasedID_cut = 0);

    DeclareProperty(GetName() + "_do_bdtBasedEleVeto", c_do_bdtBasedEleVeto = false);
    DeclareProperty(GetName() + "_bdtBasedEleVeto_level", c_bdtBasedEleVeto_level = -1); 

    DeclareProperty(GetName() + "_do_muonVeto", c_do_muonVeto = false);

    DeclareProperty(GetName() + "_do_crackVeto", c_do_crackVeto = false);
    DeclareProperty(GetName() + "_crackVeto_eta_min", c_crackVeto_eta_min = 1e12);
    DeclareProperty(GetName() + "_crackVeto_eta_max", c_crackVeto_eta_max = 1e12);
}

void TauSelector::BeginInputData(const SInputData& id) throw (SError) {
    // which cuts shall be applied (order is important)
    m_vCuts.clear();
    if (c_do_pt_cut)
        m_vCuts.push_back(new PTCut("PT", c_pt_min, c_pt_max));
    if (c_do_eta_cut)
        m_vCuts.push_back(new EtaCut("ETA", c_exclude_etas, c_eta_min, c_eta_max));
    if (c_do_author_cut)
        m_vCuts.push_back(new AuthorCut<const D3PDReader::Tau > ("AUTHOR", c_allowed_authors));

    if (c_do_min_number_of_tracks)
        m_vCuts.push_back(new TauNumTrack("MINTRACKVETO", c_min_number_of_tracks));

    if (c_do_exact_number_of_tracks)
        m_vCuts.push_back(new TauNumTrack("TRACKVETO", c_exact_number_of_tracks));

    if (c_do_charge_cut)
        m_vCuts.push_back(new TauChargeCut("CHARGECUT", c_abs_charge));

    if (c_do_llhBasedID)
      m_vCuts.push_back(new TauIDLLH("LLHBASEDID", c_llhBasedID_level, c_llhBasedID_cut));
    if (c_do_bdtBasedID)
      m_vCuts.push_back(new TauIDBDT("BDTBASEDID", c_bdtBasedID_level, c_bdtBasedID_cut));

    if (c_do_bdtBasedEleVeto)
        m_vCuts.push_back(new TauBDTEleVeto("BDTBASEDEleVeto", c_bdtBasedEleVeto_level));

    if (c_do_muonVeto)
        m_vCuts.push_back(new TauMuonVetoCut("MUONVETO"));

    if (c_do_crackVeto)
        m_vCuts.push_back(new TauCrackVeto("TAUCRACKVETO", c_crackVeto_eta_min, c_crackVeto_eta_max));
    

    // book histograms
    Book(TH1F("h_tau_pt_pre", "taus before cut;p_{T}[GeV];#", 100, 0, 200));
    Book(TH1F("h_tau_pt", "taus after cut;p_{T}[GeV];#", 100, 0, 200));
    Book(TH1F("h_tau_pt_eff", "taus selection efficiency;p_{T}[GeV];#varepsilon", 100, 0, 200));
    Book(TH1F("h_tau_eta_pre", "taus before cut;#eta;#", 100, -5, 5));
    Book(TH1F("h_tau_eta", "taus after cut;#eta;#", 100, -5, 5));
    Book(TH1F("h_tau_eta_eff", "taus selection efficiency;#eta;#varepsilon", 100, -5, 5));
    Book(TH1F("h_tau_phi_pre", "taus before cut;#phi;#", 10, -3.1416, 3.1416));
    Book(TH1F("h_tau_phi", "taus after cut;#phi;#", 10, -3.1416, 3.1416));
    Book(TH1F("h_tau_phi_eff", "taus selection efficiency;#phi;#varepsilon", 10, -3.1416, 3.1416));

    BaseObjectSelector<D3PDReader::Tau>::BeginInputData(id);
}

//______________________________________________________________________________

void TauSelector::EndMasterInputData(const SInputData& id) throw (SError) {
    // calculate selection efficiencies
    Hist("h_tau_pt_eff")->Divide(Hist("h_tau_pt"), Hist("h_tau_pt_pre"), 1, 1, "B");
    Hist("h_tau_eta_eff")->Divide(Hist("h_tau_eta"), Hist("h_tau_eta_pre"), 1, 1, "B");
    Hist("h_tau_phi_eff")->Divide(Hist("h_tau_phi"), Hist("h_tau_phi_pre"), 1, 1, "B");

    BaseObjectSelector<D3PDReader::Tau>::EndMasterInputData(id);
}

//______________________________________________________________________________

void TauSelector::operator()(std::vector<D3PDReader::Tau*>& vTaus, float fWeight) {
    // fill kinematics before cuts
    for (std::vector<D3PDReader::Tau*>::iterator it = vTaus.begin();
            it != vTaus.end(); ++it) {
        Hist("h_tau_pt_pre")->Fill((*it)->TLV().Pt() / 1000, fWeight * (*it)->Weight());
        Hist("h_tau_eta_pre")->Fill((*it)->TLV().Eta(), fWeight * (*it)->Weight());
        Hist("h_tau_phi_pre")->Fill((*it)->TLV().Phi(), fWeight * (*it)->Weight());
    }

    // do actual cuts and fill cut flow (taus failing at least one cut
    // are removed from vTaus)
    BaseObjectSelector<D3PDReader::Tau>::operator()(vTaus, fWeight);

    // fill kinematics after cuts
    for (std::vector<D3PDReader::Tau*>::iterator it = vTaus.begin();
            it != vTaus.end(); ++it) {
        Hist("h_tau_pt")->Fill((*it)->TLV().Pt() / 1000, fWeight * (*it)->Weight());
        Hist("h_tau_eta")->Fill((*it)->TLV().Eta(), fWeight * (*it)->Weight());
        Hist("h_tau_phi")->Fill((*it)->TLV().Phi(), fWeight * (*it)->Weight());
    }
}

#endif // TAU_D3PD

#endif // AnalysisUtilities_TauSelector_CXX
