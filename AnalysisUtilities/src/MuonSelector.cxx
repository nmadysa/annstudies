#ifndef AnalysisUtilities_MuonSelector_CXX
#define AnalysisUtilities_MuonSelector_CXX

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "Cuts/PTCut.h"
#include "Cuts/EtaCut.h"

// custom include(s)
#include "MuonSelector.h"
#include "Muon.h"
#include "Cuts/MuTrackCuts.h"
#include "Cuts/MuIsCom.h"
#include "Cuts/ChargeCut.h"
#include "Cuts/MuQualityCut.h"
#include "Cuts/MuZ0Cut.h"
#include "Cuts/MuZ0ThetaCut.h"
#include "Cuts/MuD0SigCut.h"
#include "Cuts/AuthorCut.h"

MuonSelector::MuonSelector(CycleBase* pParent,const char* sName):
  BaseObjectSelector<D3PDReader::Muon>(pParent,sName)
{
  // declare configurable properties
  DeclareProperty(GetName() + "_do_eta_cut",c_do_eta_cut=false);
  DeclareProperty(GetName() + "_eta_min",c_eta_min=0);
  DeclareProperty(GetName() + "_eta_max",c_eta_max=100);
  DeclareProperty(GetName() + "_exclude_etas",c_exclude_etas);

  DeclareProperty(GetName() + "_do_pt_cut",c_do_pt_cut=false);
  DeclareProperty(GetName() + "_pt_min",c_pt_min=0);
  DeclareProperty(GetName() + "_pt_max",c_pt_max=1e12);

  DeclareProperty(GetName() + "_do_track_cuts",c_do_track_cuts=false);

  DeclareProperty(GetName() + "_do_z0_cut",c_do_z0_cut=false);
  DeclareProperty(GetName() + "_z0_pv",c_z0_pv=1000);
  
  DeclareProperty(GetName() + "_do_z0theta_cut",c_do_z0theta_cut=false);
  DeclareProperty(GetName() + "_z0theta",c_z0theta=1000);
  
  DeclareProperty(GetName() + "_require_combined",c_require_combined=false);

  DeclareProperty(GetName() + "_do_charge_cut",c_do_charge_cut=false);
  DeclareProperty(GetName() + "_check_only_abs_charge",c_check_only_abs_charge=true);

  DeclareProperty(GetName() + "_do_quality_cut",c_do_quality_cut=false);
  DeclareProperty(GetName() + "_quality_cut",c_quality_cut);
  
  DeclareProperty(GetName() + "_do_d0_sig_cut",c_do_d0_sig_cut=false);
  DeclareProperty(GetName() + "_d0_offset",c_d0_offset=0);
  DeclareProperty(GetName() + "_d0_sig",c_d0_sig=1000);

  DeclareProperty(GetName() + "_do_author_cut",c_do_author_cut=false);
  DeclareProperty(GetName() + "_author_cut",c_author_cut);
  
  DeclareProperty(GetName() + "_use_unbiased_pv", c_use_unbiased_pv=false);  
}

void MuonSelector::BeginInputData(const SInputData& id) throw(SError)
{
  // which cuts shall be applied (order is important)
  m_vCuts.clear();
  if(c_do_author_cut)
    m_vCuts.push_back(new AuthorCut<const D3PDReader::Muon>("AUTHOR",c_author_cut));
  if(c_do_quality_cut)
    m_vCuts.push_back(new MuQualityCut("QUALITY", c_quality_cut));
  if(c_require_combined)
     m_vCuts.push_back(new MuIsCom("IS_COMBINED"));
  if(c_do_eta_cut)
    m_vCuts.push_back(new EtaCut("ETA",c_exclude_etas,c_eta_min,c_eta_max));
  if(c_do_pt_cut)
     m_vCuts.push_back(new PTCut("PT",c_pt_min,c_pt_max));
  if(c_do_track_cuts)
    m_vCuts.push_back(new MuTrackCuts("TRACK_CLEANING"));
  if(c_do_z0theta_cut)
    m_vCuts.push_back(new MuZ0ThetaCut("Z0_Theta",c_z0theta, c_use_unbiased_pv));
  if(c_do_z0_cut)
    m_vCuts.push_back(new MuZ0Cut("Z0",c_z0_pv, c_use_unbiased_pv));
  if(c_do_d0_sig_cut)
    m_vCuts.push_back(new MuD0SigCut("D0 Sig",c_d0_sig, c_use_unbiased_pv, c_d0_offset));
  if(c_do_charge_cut)
    m_vCuts.push_back(new ChargeCut<const D3PDReader::Muon>("CHARGE",1,c_check_only_abs_charge));
  

  // book histograms
  Book(TH1F("h_mu_pt_pre","muons before cut;p_{T,#mu};muons / 2 GeV",100,0,200));
  Book(TH1F("h_mu_pt","muons after cut;p_{T,#mu};muons / 2 GeV",100,0,200));
  Book(TH1F("h_mu_pt_eff","muons selection efficiency;p_{T,#mu};#varepsilon / 2 GeV",100,0,200));
  Book(TH1F("h_mu_eta_pre","muons before cut;#eta_{#mu};muons / 0.1",100,-5,5));
  Book(TH1F("h_mu_eta","muons after cut;#eta_{#mu};muons / 0.1",100,-5,5));
  Book(TH1F("h_mu_eta_eff","muons selection efficiency;#eta_{#mu};#varepsilon / 0.1",100,-5,5));
  Book(TH1F("h_mu_phi_pre","muons before cut;#phi_{#mu};muons / #pi/6",12,-3.1416,3.1416));
  Book(TH1F("h_mu_phi","muons after cut;#phi_{#mu};muons / #pi/6",12,-3.1416,3.1416));
  Book(TH1F("h_mu_phi_eff","muons selection efficiency;#phi_{#mu};#varepsilon / #pi/6",12,-3.1416,3.1416));
  Book(TH1F("h_mu_pt_diff_rel_pre","muons before cuts;#frac{p_{T,ID} - p_{T,MS}}{p_{T,ID}};#muons / 0.02",50,-1,1));
  Book(TH1F("h_mu_pt_diff_rel","muons after cuts;#frac{p_{T,ID} - p_{T,MS}}{p_{T,ID}};#muons / 0.02",50,-1,1));

  BaseObjectSelector<D3PDReader::Muon>::BeginInputData(id);
}

//______________________________________________________________________________
void MuonSelector::EndMasterInputData(const SInputData& id) throw(SError)
{
  // calculate selection efficiencies
  Hist("h_mu_pt_eff")->Divide(Hist("h_mu_pt"),Hist("h_mu_pt_pre"),1,1,"B");
  Hist("h_mu_eta_eff")->Divide(Hist("h_mu_eta"),Hist("h_mu_eta_pre"),1,1,"B");
  Hist("h_mu_phi_eff")->Divide(Hist("h_mu_phi"),Hist("h_mu_phi_pre"),1,1,"B");

  BaseObjectSelector<D3PDReader::Muon>::EndMasterInputData(id);
}

//______________________________________________________________________________
void MuonSelector::operator()(std::vector<D3PDReader::Muon*>& vMuons,float fWeight)
{
  // fill kinematics before cuts
  for(std::vector<D3PDReader::Muon*>::iterator it = vMuons.begin();
      it != vMuons.end();++it)
  {
    Hist("h_mu_pt_pre")->Fill((*it)->TLV().Pt()/1000,fWeight * (*it)->Weight());
    Hist("h_mu_eta_pre")->Fill((*it)->TLV().Eta(),fWeight * (*it)->Weight());
    Hist("h_mu_phi_pre")->Fill((*it)->TLV().Phi(),fWeight * (*it)->Weight());
    float pT_ID = fabs(sin((*it)->id_theta_exPV())/(*it)->id_qoverp_exPV());
    float pT_MS = fabs(sin((*it)->me_theta_exPV())/(*it)->me_qoverp_exPV());
    float pt_diff = pT_ID ? (pT_ID - pT_MS)/pT_ID : -1.5;
    Hist("h_mu_pt_diff_rel_pre")->Fill(pt_diff,fWeight * (*it)->Weight());
  }

  // do actual cuts and fill cut flow (muons failing at least one cut
  // are removed from vMuons)
  BaseObjectSelector<D3PDReader::Muon>::operator()(vMuons,fWeight);

  // fill kinematics after cuts
  for(std::vector<D3PDReader::Muon*>::iterator it = vMuons.begin();
      it != vMuons.end();++it)
  {
    Hist("h_mu_pt")->Fill((*it)->TLV().Pt()/1000,fWeight * (*it)->Weight());
    Hist("h_mu_eta")->Fill((*it)->TLV().Eta(),fWeight * (*it)->Weight());
    Hist("h_mu_phi")->Fill((*it)->TLV().Phi(),fWeight * (*it)->Weight());
    float pT_ID = fabs(sin((*it)->id_theta_exPV())/(*it)->id_qoverp_exPV());
    float pT_MS = fabs(sin((*it)->me_theta_exPV())/(*it)->me_qoverp_exPV());
    float pt_diff = pT_ID ? (pT_ID - pT_MS)/pT_ID : -1.5;
    Hist("h_mu_pt_diff_rel")->Fill(pt_diff,fWeight * (*it)->Weight());
  }
}

#endif // AnalysisUtilities_MuonSelector_CXX
