#ifndef AnalysisUtilities_VertexSelector_CXX
#define AnalysisUtilities_VertexSelector_CXX

// STL include(s)
#include <vector>

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "BaseObjectSelector.h"

// AnalysisUtilities include(s)
#include "VertexSelector.h"
#include "Cuts/VertexZCut.h"
#include "Cuts/VertexTracksCut.h"
#include "Cuts/VertexTypeCut.h"

// D3PDObjects include(s)
#include "Vertex.h"

//______________________________________________________________________________
VertexSelector::VertexSelector(CycleBase* parent,const char* name):
  BaseObjectSelector<D3PDReader::Vertex>(parent,name)
{
  // configurable properties
  DeclareProperty(GetName() + "_do_ntracks_cut",c_do_ntracks_cut = false);
  DeclareProperty(GetName() + "_ntracks_max",c_ntracks_max = 1000);
  DeclareProperty(GetName() + "_ntracks_min",c_ntracks_min = 0);  
  DeclareProperty(GetName() + "_do_type_cut",c_do_type_cut = false);
  DeclareProperty(GetName() + "_allowed_types", c_allowed_types);
  DeclareProperty(GetName() + "_do_z_cut",c_do_z_cut = false);
  DeclareProperty(GetName() + "_z_max",c_z_max = 1000);
  DeclareProperty(GetName() + "_z_min",c_z_min = -1000);
}

//______________________________________________________________________________
void VertexSelector::BeginInputData(const SInputData& id) throw(SError)
{  
  // which cuts shall be applied (order is important)
  m_vCuts.clear();
  if(c_do_type_cut)
    m_vCuts.push_back(new VertexTypeCut("Type",c_allowed_types));
  if(c_do_ntracks_cut)
    m_vCuts.push_back(new VertexTracksCut("NTracks",c_ntracks_min,c_ntracks_max));
  if(c_do_z_cut)
    m_vCuts.push_back(new VertexZCut("Z",c_z_min,c_z_max));

  // book histograms
  Book(TH1F("h_vxp_z_pre","vertices before cut;z;#",100,-500,500));
  Book(TH1F("h_vxp_z","vertices after cut;z;#",100,-500,500));
  Book(TH1F("h_vxp_ntracks_pre","vertices before cut;N Tracks;#",100,0,100));
  Book(TH1F("h_vxp_ntracks","vertices after cut;N Tracks;#",100,0,100));

  BaseObjectSelector<D3PDReader::Vertex>::BeginInputData(id);
}

//______________________________________________________________________________
void VertexSelector::operator()(std::vector<D3PDReader::Vertex*>& vVertices,float hist_weight)
{
  // fill control histograms
  std::vector<D3PDReader::Vertex*>::iterator it = vVertices.begin();
  for(;it != vVertices.end();++it)
  {
    Hist("h_vxp_ntracks_pre")->Fill((*it)->nTracks(),hist_weight);
    Hist("h_vxp_z_pre")->Fill((*it)->z(),hist_weight);
  }

  BaseObjectSelector<D3PDReader::Vertex>::operator()(vVertices,hist_weight);

  // fill control histograms
  it = vVertices.begin();
  for(;it != vVertices.end();++it)
  {
    Hist("h_vxp_ntracks")->Fill((*it)->nTracks(),hist_weight);
    Hist("h_vxp_z")->Fill((*it)->z(),hist_weight);
  }
}

#endif // AnalysisUtilities_VertexSelector_CXX
