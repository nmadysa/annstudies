#ifndef AnalysisUtilities_TruthTauSelector_CXX
#define AnalysisUtilities_TruthTauSelector_CXX

#ifdef TAU_D3PD

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "Cuts/PTCut.h"
#include "Cuts/EtaCut.h"
//#include "Cuts/EtCut.h"

// AnalysisUtilities include(s)
#include "TruthTauSelector.h"
#include "Cuts/AuthorCut.h"
#include "Cuts/TauNumTrack.h"
#include "Cuts/TauChargeCut.h"

// D3PDObjects include(s)
#include "TruthTau.h"

TruthTauSelector::TruthTauSelector(CycleBase* pParent,const char* sName):
  BaseObjectSelector<D3PDReader::TruthTau>(pParent,sName)
{
  // declare configurable properties
  DeclareProperty(GetName() + "_do_eta_cut",c_do_eta_cut=false);
  DeclareProperty(GetName() + "_eta_min",c_eta_min=0);
  DeclareProperty(GetName() + "_eta_max",c_eta_max=100);
  DeclareProperty(GetName() + "_exclude_etas",c_exclude_etas);

  DeclareProperty(GetName() + "_do_pt_cut",c_do_pt_cut=false);
  DeclareProperty(GetName() + "_pt_min",c_pt_min=0);
  DeclareProperty(GetName() + "_pt_max",c_pt_max=1e12);

  DeclareProperty(GetName() + "_do_minNumber_Tracks", c_do_min_number_of_tracks=false);
  DeclareProperty(GetName() + "_minNumber_Tracks", c_min_number_of_tracks=0);
  
  DeclareProperty(GetName() + "_do_exactNumber_Tracks", c_do_exact_number_of_tracks=false);
  DeclareProperty(GetName() + "_exactNumber_Tracks", c_exact_number_of_tracks);

  DeclareProperty(GetName() + "_do_charge_cut", c_do_charge_cut=false);
  DeclareProperty(GetName() + "_abs_charge", c_abs_charge=1);
}

void TruthTauSelector::BeginInputData(const SInputData& id) throw(SError)
{
  // which cuts shall be applied (order is important)
  m_vCuts.clear();
  if(c_do_pt_cut)
    m_vCuts.push_back(new PTCut("PT",c_pt_min,c_pt_max));
  if(c_do_eta_cut)
    m_vCuts.push_back(new EtaCut("ETA",c_exclude_etas,c_eta_min,c_eta_max));

  if(c_do_min_number_of_tracks)
    m_vCuts.push_back(new TauNumTrack("MINTRACKVETO",c_min_number_of_tracks));

  if(c_do_exact_number_of_tracks)
    m_vCuts.push_back(new TauNumTrack("TRACKVETO",c_exact_number_of_tracks));

  if(c_do_charge_cut)
    m_vCuts.push_back(new TauChargeCut("CHARGECUT", c_abs_charge));

  // book histograms
  Book(TH1F("h_tau_pt_pre","taus before cut;p_{T}[GeV];#",100,0,200));
  Book(TH1F("h_tau_pt","taus after cut;p_{T}[GeV];#",100,0,200));
  Book(TH1F("h_tau_pt_eff","taus selection efficiency;p_{T}[GeV];#varepsilon",100,0,200));
  Book(TH1F("h_tau_eta_pre","taus before cut;#eta;#",100,-5,5));
  Book(TH1F("h_tau_eta","taus after cut;#eta;#",100,-5,5));
  Book(TH1F("h_tau_eta_eff","taus selection efficiency;#eta;#varepsilon",100,-5,5));
  Book(TH1F("h_tau_phi_pre","taus before cut;#phi;#",10,-3.1416,3.1416));
  Book(TH1F("h_tau_phi","taus after cut;#phi;#",10,-3.1416,3.1416));
  Book(TH1F("h_tau_phi_eff","taus selection efficiency;#phi;#varepsilon",10,-3.1416,3.1416));

  BaseObjectSelector<D3PDReader::TruthTau>::BeginInputData(id);
}

//______________________________________________________________________________
void TruthTauSelector::EndMasterInputData(const SInputData& id) throw(SError)
{
  // calculate selection efficiencies
  Hist("h_tau_pt_eff")->Divide(Hist("h_tau_pt"),Hist("h_tau_pt_pre"),1,1,"B");
  Hist("h_tau_eta_eff")->Divide(Hist("h_tau_eta"),Hist("h_tau_eta_pre"),1,1,"B");
  Hist("h_tau_phi_eff")->Divide(Hist("h_tau_phi"),Hist("h_tau_phi_pre"),1,1,"B");

  BaseObjectSelector<D3PDReader::TruthTau>::EndMasterInputData(id);
}

//______________________________________________________________________________
void TruthTauSelector::operator()(std::vector<D3PDReader::TruthTau*>& vTruthTaus,float fWeight)
{
  // fill kinematics before cuts
  for(std::vector<D3PDReader::TruthTau*>::iterator it = vTruthTaus.begin();
      it != vTruthTaus.end();++it)
  {
    Hist("h_tau_pt_pre")->Fill((*it)->TLV().Pt()/1000,fWeight * (*it)->Weight());
    Hist("h_tau_eta_pre")->Fill((*it)->TLV().Eta(),fWeight * (*it)->Weight());
    Hist("h_tau_phi_pre")->Fill((*it)->TLV().Phi(),fWeight * (*it)->Weight());
  }

  // do actual cuts and fill cut flow (taus failing at least one cut
  // are removed from vTruthTaus)
  BaseObjectSelector<D3PDReader::TruthTau>::operator()(vTruthTaus,fWeight);

  // fill kinematics after cuts
  for(std::vector<D3PDReader::TruthTau*>::iterator it = vTruthTaus.begin();
      it != vTruthTaus.end();++it)
  {
    Hist("h_tau_pt")->Fill((*it)->TLV().Pt()/1000,fWeight * (*it)->Weight());
    Hist("h_tau_eta")->Fill((*it)->TLV().Eta(),fWeight * (*it)->Weight());
    Hist("h_tau_phi")->Fill((*it)->TLV().Phi(),fWeight * (*it)->Weight());
  }
}

#endif // TAU_D3PD

#endif // AnalysisUtilities_TruthTauSelector_CXX
