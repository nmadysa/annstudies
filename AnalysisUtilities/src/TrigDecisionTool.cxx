#ifndef TrigDecisionTool_CXX
#define TrigDecisionTool_CXX

#ifdef TAU_D3PD

// system include(s)
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <iterator>
#include <numeric>

// SFrame include(s)
#include "core/include/SError.h"

#ifndef __CINT__
// Boost include(s)
#include "boost/algorithm/string.hpp"
#endif // __CINT__

// AnalysisUtilities include(s)
#include "TrigEFElectron.h"
#include "TrigEFMuon.h"

#include "Utilities.h"

//custom include(s)
#include "TrigDecisionTool.h"


TrigDecisionTool::TrigDecisionTool(CycleBase* pParent, const char* sName):
  ToolBase(pParent, sName),
  m_rTrigMetaData(m_lMetaEntry),
  m_pTriggerInfoCollection(0),
  m_lMetaEntry(0),
  m_lMetaEntriesMax(-1)
{
  DeclareProperty(GetName() + "_is_embedding",c_is_embed = false);
  DeclareProperty(GetName() + "_TrigConfTreeName",c_conf_tree_name);
  m_mResurrected["EF_tau20_medium1"] = (&D3PDReader::TrigEFTau::EF_tau20_medium1);
  m_mResurrected["EF_tau29_medium1"] = (&D3PDReader::TrigEFTau::EF_tau29_medium1);
  m_mResurrected["EF_tau20T_medium1"] = (&D3PDReader::TrigEFTau::EF_tau20T_medium1);
  m_mResurrected["EF_tau29T_medium1"] = (&D3PDReader::TrigEFTau::EF_tau29T_medium1);
  m_mResurrected["EF_tau20Ti_medium1"] = (&D3PDReader::TrigEFTau::EF_tau20Ti_medium1);
  m_mResurrected["EF_tau29Ti_medium1"] = (&D3PDReader::TrigEFTau::EF_tau29Ti_medium1);
  m_mResurrected["EF_tau38T_medium1"] = (&D3PDReader::TrigEFTau::EF_tau38T_medium1);
  m_mResurrected["EF_tau125_medium1"] = (&D3PDReader::TrigEFTau::EF_tau125_medium1);
}

void TrigDecisionTool::Initialise(const D3PDReader::TriggerInfoCollection& rTriggerInfo)
{
  m_pTriggerInfoCollection = &rTriggerInfo;
}

bool TrigDecisionTool::UpdateConfiguration()
{
  if(m_pTriggerInfoCollection == 0)
  {
    m_logger << FATAL << "TrigDecisionTool not yet initialised" << SLogger::endmsg;
    throw SError(SError::StopExecution);
    return false;
  }

  if(m_lMetaEntriesMax < 0)
  {
    m_logger << FATAL << "TrigDecisionTool not yet connected to TrigConfTree" << SLogger::endmsg;
    throw SError(SError::StopExecution);
    return false;
  }

  // save current loaded entry number
  int iEntry = m_lMetaEntry;

  // do we need to update the configuration
  while((m_rTrigMetaData.SMK() != (int)m_pTriggerInfoCollection->DB_SMK()) ||
	(m_rTrigMetaData.L1PSK() != (int)m_pTriggerInfoCollection->DB_L1PSK()) ||
	(m_rTrigMetaData.HLTPSK() != (int)m_pTriggerInfoCollection->DB_HLTPSK()))
  {
    // check nex entry
    ++m_lMetaEntry;
    // reached end of TrigConfTree -> start from beginning
    if(m_lMetaEntry == m_lMetaEntriesMax)
      m_lMetaEntry = 0;
    // reached entry number which we started with -> configuration not found
    if(iEntry == m_lMetaEntry)
    {
      std::cout << &m_rTrigMetaData << std::endl;
      m_logger << FATAL << "configuration for SMK = " << m_pTriggerInfoCollection->DB_SMK() << ", L1PSK = "
	       << m_pTriggerInfoCollection->DB_L1PSK() << " and HLTPSK = " << m_pTriggerInfoCollection->DB_HLTPSK() << " not found in "
	       << c_conf_tree_name << SLogger::endmsg;
      throw SError(SError::StopExecution);

      return false;
    }
  }

  return true;
}

void TrigDecisionTool::BeginInputFile(const SInputData& id) throw(SError)
{
  if(c_is_embed)
    return;
  m_lMetaEntriesMax = GetParent()->GetInputMetadataTree(c_conf_tree_name.c_str())->GetEntries();
  m_lMetaEntry = 0;
  m_rTrigMetaData.ReadFrom(GetParent()->GetInputMetadataTree(c_conf_tree_name.c_str()));
}
 
float TrigDecisionTool::GetPrescales(const std::string& sChain, bool bRerun) 
{
  if(!UpdateConfiguration())
    return 1.;
    
  float prescale = 0.;  
  std::map<std::string,float>::const_iterator it;
  
  if(sChain.find("L1_") != std::string::npos) {
    it = m_rTrigMetaData.LVL1PrescaleMap()->find(sChain);
    if(it != m_rTrigMetaData.LVL1PrescaleMap()->end()) prescale = it->second;
  }
  
  if( (sChain.find("EF_") != std::string::npos) || (sChain.find("L2_") != std::string::npos) ) {
      if (!bRerun) {
        it = m_rTrigMetaData.HLTPrescaleMap()->find(sChain);
        if(it != m_rTrigMetaData.HLTPrescaleMap()->end()) prescale = it->second;
      }
      else {
        it = m_rTrigMetaData.HLTRerunPrescaleMap()->find(sChain);
        if(it != m_rTrigMetaData.HLTRerunPrescaleMap()->end()) prescale = it->second;
      }          
  }

  if (prescale==0) {
    m_logger << WARNING << "chain name \"" << sChain << "\" not found in current prescale map" << SLogger::endmsg;
    return 1;
  }
  
  return prescale;    
}

float TrigDecisionTool::GetCombinedPrescales(std::string sChain, int iDeepness, bool bRerun, bool bVerbose)
{    
    float prescale = GetPrescales(sChain, bRerun);
    if (bVerbose && bRerun) m_logger << ALWAYS << "rerun ";
    if (bVerbose) m_logger << ALWAYS << "prescale chain: "<< sChain << "=" << prescale;
    while (sChain!="" && iDeepness>0) {
        sChain = GetLowerChainName(sChain);
        float pre = GetPrescales(sChain, bRerun); 
        prescale *=pre;
        --iDeepness;
        if (bVerbose) m_logger << ALWAYS << " -> "<< sChain << "=" << pre;
    }
    if (bVerbose) m_logger << ALWAYS << " ==> " << prescale << SLogger::endmsg;
    return prescale;
}

string TrigDecisionTool::GetLowerChainName(const std::string& sChain) 
{    
    std::map<std::string,string>::const_iterator its;  
    its = m_rTrigMetaData.HLTLowerChainNameMap()->find(sChain);
    if(its != m_rTrigMetaData.HLTLowerChainNameMap()->end()) return its->second;    
    return "";
}

bool TrigDecisionTool::CheckHLTTrigger(const std::string& sChain,const std::vector<short>& rPassedPhysics)
{
  if(!UpdateConfiguration())
    return false;
  
  // event filter chains have an offset of 10000
  int offset = (sChain.find("EF") != std::string::npos) ? 10000 : 0;

  // get chain ID
  int iChainID = GetHLTChainID(sChain) - offset;

  if(iChainID > 0)
  {
    std::vector<short>::const_iterator cit = std::find(rPassedPhysics.begin(),rPassedPhysics.end(),iChainID);
    if(cit != rPassedPhysics.end())
      return true;
    else
      return false;
  }
  else
    return false;
}

std::vector<int> TrigDecisionTool::GetRoIIndices(int iChainID)
{
  if(!UpdateConfiguration())
    return std::vector<int>();
  
  const std::vector<short>* pNavChainID = m_pTriggerInfoCollection->Nav_chain_ChainId();
  std::vector<short>::const_iterator nav_it = std::find(pNavChainID->begin(),pNavChainID->end(),iChainID);
  if(nav_it != pNavChainID->end())
  {
    return m_pTriggerInfoCollection->Nav_chain_RoIIndex()->at((int)distance(pNavChainID->begin(),nav_it));
  }
  else
  {
    m_logger << ERROR << "chain ID " << iChainID << " not found in Nav_chain_ChainId" << SLogger::endmsg;
    return std::vector<int>();
  }
}

std::vector<int> TrigDecisionTool::GetRoITypes(int iChainID)
{
  if(!UpdateConfiguration())
    return std::vector<int>();
  
  const std::vector<short>* pNavChainID = m_pTriggerInfoCollection->Nav_chain_ChainId();
  std::vector<short>::const_iterator nav_it = std::find(pNavChainID->begin(),pNavChainID->end(),iChainID);
  if(nav_it != pNavChainID->end())
  {
    return m_pTriggerInfoCollection->Nav_chain_RoIType()->at((int)distance(pNavChainID->begin(),nav_it));
  }
  else
  {
    m_logger << ERROR << "chain ID " << iChainID << " not found in Nav_chain_ChainId" << SLogger::endmsg;
    return std::vector<int>();
  }
}

int TrigDecisionTool::GetHLTChainID(const std::string& sChain)
{
  if(!UpdateConfiguration())
    return -1;

  // event filter chains have an offset of 10000
  int offset = (sChain.find("EF") != std::string::npos) ? 10000 : 0;
    
  // get chain ID
  std::map<std::string,int>::const_iterator it = m_rTrigMetaData.HLTNameMap()->find(sChain);
  if(it != m_rTrigMetaData.HLTNameMap()->end())
    return it->second + offset;
  // chain id not found
  else
  {
    m_logger << WARNING << "chain name \"" << sChain << "\" not found in current HLTNameMap" << SLogger::endmsg;
    return -1;
  }    
}

std::string TrigDecisionTool::GetHLTChainName(int iChainID)
{
  if(!UpdateConfiguration())
    return "";

  bool bIsEF = (iChainID > 10000);

  if(bIsEF)
    iChainID = iChainID - 10000;
  
  std::map<std::string, int>::const_iterator it = m_rTrigMetaData.HLTNameMap()->begin();
  for(; it != m_rTrigMetaData.HLTNameMap()->end(); it++)
  {
    if(it->second == iChainID)
    {
      if((bIsEF && (it->first.find("EF") != std::string::npos)) || (!bIsEF && (it->first.find("L2") != std::string::npos)))
	return it->first;
    }
  }

  for(it = m_rTrigMetaData.LVL1NameMap()->begin(); it != m_rTrigMetaData.LVL1NameMap()->end(); it++)
  {
    if(it->second == iChainID)
      return it->first;
  }
  
  m_logger << ERROR << "HLTChain with ID " << iChainID << " not found in trigger configuration." << SLogger::endmsg;
  return "";
}

template<>
std::vector<int> TrigDecisionTool::GetTrigObjIndices(const std::string& sChain,const std::vector<D3PDReader::TrigEFElectron*>&)
{
  int iRoIType = 1002;
  bool bIsEF = false;
  if(sChain.find("EF") != std::string::npos)
  {
    bIsEF = true;
    iRoIType = 2002;
  }
  
  int iChainID = GetHLTChainID(sChain);
  std::vector<int> vRoIIndices = GetRoIIndices(iChainID);
  std::vector<int> vRoITypes = GetRoITypes(iChainID);

  for(unsigned int i = 0; i < vRoIIndices.size(); ++i)
    m_logger << DEBUG << "#" <<i << " Index = " << vRoIIndices.at(i) << " Type = " << vRoITypes.at(i) << SLogger::endmsg;

  int nRoIs = m_pTriggerInfoCollection->RoI_EF_e_n();
  const std::vector<short>* pRoI_type = m_pTriggerInfoCollection->RoI_EF_e_type();
  const std::vector<std::vector<int> >* pObjects = m_pTriggerInfoCollection->RoI_EF_e_egammaContainer_egamma_Electrons();
  const std::vector<std::vector<int> >* pObjectStatus = m_pTriggerInfoCollection->RoI_EF_e_egammaContainer_egamma_ElectronsStatus();

  return GetTrigObjIndices(&vRoIIndices,&vRoITypes,iRoIType,nRoIs,pRoI_type,pObjects,pObjectStatus);
}

template<>
std::vector<int> TrigDecisionTool::GetTrigObjIndices(const std::string& sChain,const std::vector<D3PDReader::TrigEFMuon*>&)
{
  int iRoIType = 1001;
  bool bIsEF = false;
  if(sChain.find("EF") != std::string::npos)
  {
    bIsEF = true;
    iRoIType = 2001;
  }
  
  int iChainID = GetHLTChainID(sChain);
  std::vector<int> vRoIIndices = GetRoIIndices(iChainID);
  std::vector<int> vRoITypes = GetRoITypes(iChainID);

  for(unsigned int i = 0; i < vRoIIndices.size(); ++i)
    m_logger << DEBUG << "#" <<i << " Index = " << vRoIIndices.at(i) << " Type = " << vRoITypes.at(i) << SLogger::endmsg;
  
  int nRoIs = m_pTriggerInfoCollection->RoI_EF_mu_n();
  const std::vector<short>* pRoI_type = m_pTriggerInfoCollection->RoI_EF_mu_type();
  const std::vector<std::vector<int> >* pObjects = m_pTriggerInfoCollection->RoI_EF_mu_TrigMuonEFInfoContainer();
  const std::vector<std::vector<int> >* pObjectStatus = m_pTriggerInfoCollection->RoI_EF_mu_TrigMuonEFInfoContainerStatus();

  return GetTrigObjIndices(&vRoIIndices,&vRoITypes,iRoIType,nRoIs,pRoI_type,pObjects,pObjectStatus);
}

#ifdef TAU_D3PD
template<>
std::vector<int> TrigDecisionTool::GetTrigObjIndices(const std::string& sChain,const std::vector<D3PDReader::TrigEFTau*>&)
{
  int iRoIType = 1003;
  bool bIsEF = false;
  if(sChain.find("EF") != std::string::npos)
  {
    bIsEF = true;
    iRoIType = 2003;
  }
  
  int iChainID = GetHLTChainID(sChain);
  std::vector<int> vRoIIndices = GetRoIIndices(iChainID);
  std::vector<int> vRoITypes = GetRoITypes(iChainID);

  int nRoIs = m_pTriggerInfoCollection->RoI_EF_tau_n();
  const std::vector<short>* pRoI_type = m_pTriggerInfoCollection->RoI_EF_tau_type();
  const std::vector<std::vector<int> >* pObjects = m_pTriggerInfoCollection->RoI_EF_tau_Analysis__TauJetContainer();
  const std::vector<std::vector<int> >* pObjectStatus = m_pTriggerInfoCollection->RoI_EF_tau_Analysis__TauJetContainerStatus();

  return GetTrigObjIndices(&vRoIIndices,&vRoITypes,iRoIType,nRoIs,pRoI_type,pObjects,pObjectStatus);
}
#endif // TAU_D3PD

std::vector<int> TrigDecisionTool::GetTrigObjIndices(const std::vector<int>* pRoIIndices,
						     const std::vector<int>* pRoITypes,
						     int iRoIType,
						     int nObjRoIs,
						     const std::vector<short>* pRoI_type,
						     const std::vector<std::vector<int> >* pObjects,
						     const std::vector<std::vector<int> >* pObjectStatus)
{
  std::vector<int> vTrigObjIndices;
  int iRoIIndex = -1;

  m_logger << DEBUG << "looking for RoIs of type " << iRoIType << " in " << nObjRoIs << " RoIs of object chain" << SLogger::endmsg;

  // loop over all given RoIs
  for(unsigned int i = 0; i < pRoIIndices->size(); ++i)
  {
    iRoIIndex = pRoIIndices->at(i);
    m_logger << DEBUG << "looking for RoI index " << iRoIIndex << " with type " << pRoITypes->at(i) << " in object RoIs" << SLogger::endmsg;

    // is this RoI of correct type (= type of trigger objects)
    if(pRoITypes->at(i) != iRoIType)
      continue;
    
    if(iRoIIndex >= nObjRoIs)
    {
      m_logger << ERROR << "RoI index greater than number of RoIs" << SLogger::endmsg;
      continue;
    }
    m_logger << DEBUG << "RoI(" << iRoIIndex << ") has type " << pRoI_type->at(iRoIIndex) << SLogger::endmsg;
    if(pRoI_type->at(iRoIIndex) == iRoIType)
    {
      for(unsigned int j = 0; j < pObjects->at(iRoIIndex).size(); j++)
      {
	if(pObjectStatus->at(iRoIIndex).at(j) != 0)
	  vTrigObjIndices.push_back(pObjects->at(iRoIIndex).at(j));
      }
    }
  }

  return vTrigObjIndices;
}

bool TrigDecisionTool::MatchedTrigger(const std::string& sChain,
				      const D3PDReader::Tau* const pTau,
				      const std::vector<D3PDReader::TrigEFTau*>& vTrigTaus,
				      const double& drMax){
  std::vector<D3PDReader::TrigEFTau*> vMatched = this->GetTriggerObjects(sChain, vTrigTaus);
  return (Utilities::CountDeltaRMatches(vMatched, pTau->TLV(), drMax) > 0);
}

inline std::string make_string(std::string init, std::string add){
  if(init == "")
    return init+add;
  return init +"_" + add;
};

std::pair<std::string, std::string> TrigDecisionTool::GetSingleTriggerFromDiTau(const std::string& sChain){
  std::vector<std::string> singleTriggers;
  boost::split(singleTriggers, sChain, boost::is_any_of("_"));
  //implementation for EF_2tau38_medium1
  if(singleTriggers.size() == 3){
    std::string trigger1 = "EF_tau38T_medium1";
    return make_pair(trigger1, trigger1);
  }
  std::vector<std::string>::iterator it = singleTriggers.begin();
  std::string init = "";
  std::string trigger1 = std::accumulate(singleTriggers.begin(), singleTriggers.begin()+3, init, make_string);
  std::string trigger2 = "EF_" + std::accumulate(singleTriggers.begin()+3, singleTriggers.begin()+5, init, make_string);
  return make_pair(trigger1, trigger2);
}

//di-tau trigger matching
bool TrigDecisionTool::MatchedTrigger(const std::string& sChain,
				      const D3PDReader::Tau* const pTau1,
				      const D3PDReader::Tau* const pTau2,
				      const std::vector<D3PDReader::TrigEFTau*>& vTrigTaus,
				      const double& drMax){
  m_logger << DEBUG << "Apply trigger matching for chain: " << sChain << SLogger::endmsg;
  std::pair<std::string, std::string> singleTriggers = GetSingleTriggerFromDiTau(sChain);
  MFP leadTrigger = m_mResurrected[singleTriggers.first];
  MFP subleadTrigger = m_mResurrected[singleTriggers.second];
  std::vector<D3PDReader::TrigEFTau*> vLeadTriggerRoI;
  std::vector<D3PDReader::TrigEFTau*> vSubLeadTriggerRoI;
  std::vector<D3PDReader::TrigEFTau*>::const_iterator cit = vTrigTaus.begin();
  for(; cit!=vTrigTaus.end(); ++cit){
    if(((*cit)->*leadTrigger)())
      vLeadTriggerRoI.push_back(*cit);
    if(((*cit)->*subleadTrigger)())
      vSubLeadTriggerRoI.push_back(*cit);
  }
  return ((Utilities::CountDeltaRMatches(vLeadTriggerRoI, pTau1->TLV(), drMax)) &&
	  (Utilities::CountDeltaRMatches(vSubLeadTriggerRoI, pTau2->TLV(), drMax)));
}

bool TrigDecisionTool::MatchedResurrected(const std::string& sChain,
					  const D3PDReader::Tau* const pTau,
					  const std::vector<D3PDReader::TrigEFTau*>& vTrigTaus,
					  const double& drMax){
  std::vector<D3PDReader::TrigEFTau*>::const_iterator cit = vTrigTaus.begin();
  MFP trigger = m_mResurrected[sChain];
  std::vector<D3PDReader::TrigEFTau*> vResurrectedRoI;
  for(; cit!=vTrigTaus.end(); ++cit){
    if(((*cit)->*trigger)())
      vResurrectedRoI.push_back(*cit);
  }
  return (Utilities::CountDeltaRMatches(vResurrectedRoI, pTau->TLV(), drMax) > 0);
  
  return true;
}

#endif // TAU_D3PD

#endif // TrigDecisionTool_CXX
