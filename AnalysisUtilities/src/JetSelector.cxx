#ifndef AnalysisUtilities_JetSelector_CXX
#define AnalysisUtilities_JetSelector_CXX

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "Cuts/PTCut.h"
#include "Cuts/EtaCut.h"
#include "Cuts/JetCleaningCut.h"
#include "Cuts/JVFCut.h"

// custom include(s)
#include "JetSelector.h"
#include "Jet.h"

JetSelector::JetSelector(CycleBase* pParent,const char* sName):
  BaseObjectSelector<D3PDReader::Jet>(pParent,sName)
{
  // declare configurable properties
  DeclareProperty(GetName() + "_do_eta_cut",c_do_eta_cut=false);
  DeclareProperty(GetName() + "_eta_min",c_eta_min=0);
  DeclareProperty(GetName() + "_eta_max",c_eta_max=100);
  DeclareProperty(GetName() + "_exclude_etas",c_exclude_etas);

  DeclareProperty(GetName() + "_do_pt_cut",c_do_pt_cut=false);
  DeclareProperty(GetName() + "_pt_min",c_pt_min=0);
  DeclareProperty(GetName() + "_pt_max",c_pt_max=1e12);

  DeclareProperty(GetName() + "_do_cleaning",c_do_cleaning = false);
  DeclareProperty(GetName() + "_cleaning_level",c_cleaning_level = 0);
  
  DeclareProperty(GetName() + "_do_jvtxf_cut",c_do_jvtxf_cut = false);
  DeclareProperty(GetName() + "_jvtxf_abs_jvf_min",c_jvtxf_abs_jvf_min=0.5);
  DeclareProperty(GetName() + "_jvtxf_abs_eta_max",c_jvtxf_abs_eta_max=2.5);
}

void JetSelector::BeginInputData(const SInputData& id) throw(SError)
{
  // which cuts shall be applied (order is important)
  m_vCuts.clear();
  if(c_do_pt_cut)
    m_vCuts.push_back(new PTCut("PT",c_pt_min,c_pt_max));
  if(c_do_eta_cut)
    m_vCuts.push_back(new EtaCut("ETA",c_exclude_etas,c_eta_min,c_eta_max));
  if(c_do_cleaning)
    m_vCuts.push_back(new JetCleaningCut("CLEANING",c_cleaning_level));
  if(c_do_jvtxf_cut)
    m_vCuts.push_back(new JVFCut("JVFCUT",c_jvtxf_abs_jvf_min,c_jvtxf_abs_eta_max));

  // book histograms
  Book(TH1F("h_jet_pt_pre","jets before cut;p_{T,jet};jets / 2 GeV",100,0,200));
  Book(TH1F("h_jet_pt","jets after cut;p_{T,jet};jets / 2 GeV",100,0,200));
  Book(TH1F("h_jet_pt_eff","jets selection efficiency;p_{T,jet};#varepsilon / 2 GeV",100,0,200));
  Book(TH1F("h_jet_eta_pre","jets before cut;#eta_{jet};jets / 0.1",100,-5,5));
  Book(TH1F("h_jet_eta","jets after cut;#eta_{jet};jets / 0.1",100,-5,5));
  Book(TH1F("h_jet_eta_eff","jets selection efficiency;#eta_{jet};#varepsilon / 0.1",100,-5,5));
  Book(TH1F("h_jet_phi_pre","jets before cut;#phi_{jet};jets / #pi/6",12,-3.1416,3.1416));
  Book(TH1F("h_jet_phi","jets after cut;#phi_{jet};jets / #pi/6",12,-3.1416,3.1416));
  Book(TH1F("h_jet_phi_eff","jets selection efficiency;#phi_{jet};#varepsilon / #pi/6",12,-3.1416,3.1416));

  BaseObjectSelector<D3PDReader::Jet>::BeginInputData(id);
}

//______________________________________________________________________________
void JetSelector::EndMasterInputData(const SInputData& id) throw(SError)
{
  // calculate selection efficiencies
  Hist("h_jet_pt_eff")->Divide(Hist("h_jet_pt"),Hist("h_jet_pt_pre"),1,1,"B");
  Hist("h_jet_eta_eff")->Divide(Hist("h_jet_eta"),Hist("h_jet_eta_pre"),1,1,"B");
  Hist("h_jet_phi_eff")->Divide(Hist("h_jet_phi"),Hist("h_jet_phi_pre"),1,1,"B");

  BaseObjectSelector<D3PDReader::Jet>::EndMasterInputData(id);
}

//_____________________________________________________________________________
void JetSelector::operator()(std::vector<D3PDReader::Jet*>& vJets,float fWeight)
{
  // fill kinematics before cuts
  for(std::vector<D3PDReader::Jet*>::iterator it = vJets.begin();
      it != vJets.end();++it)
  {
    Hist("h_jet_pt_pre")->Fill((*it)->TLV().Pt()/1000,fWeight * (*it)->Weight());
    Hist("h_jet_eta_pre")->Fill((*it)->TLV().Eta(),fWeight * (*it)->Weight());
    Hist("h_jet_phi_pre")->Fill((*it)->TLV().Phi(),fWeight * (*it)->Weight());
  }

  // do actual cuts and fill cut flow (jets failing at least one cut
  // are removed from vJets)
  BaseObjectSelector<D3PDReader::Jet>::operator()(vJets,fWeight);

  // fill kinematics after cuts
  for(std::vector<D3PDReader::Jet*>::iterator it = vJets.begin();
      it != vJets.end();++it)
  {
    Hist("h_jet_pt")->Fill((*it)->TLV().Pt()/1000,fWeight * (*it)->Weight());
    Hist("h_jet_eta")->Fill((*it)->TLV().Eta(),fWeight * (*it)->Weight());
    Hist("h_jet_phi")->Fill((*it)->TLV().Phi(),fWeight * (*it)->Weight());
  }
}

#endif // AnalysisUtilities_JetSelector_CXX
