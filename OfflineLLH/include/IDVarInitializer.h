#ifndef REFAC
#ifndef SKIM
#ifndef IDVarInitializer_H
#define IDVarInitializer_H

#ifndef __CINT__
#include "boost/function.hpp"
#endif //__CINT__
#include "IDVarHelper.h"
#include "IDVarLLHHelper.h"
#include "IDVarHandle.h"
#include "VariablePool.h"

namespace IDVarInitializer{

  template<class T>
  void Initialize(IDVarHandle<T>* varhandle, std::vector<T>& dummy){
    //cgen::start build IDVarInitializer
#ifdef OBSOLTE
    varhandle->AddVariable("LLHScore", ((boost::function<float (const T)>) IDVarHelper<T>::LLHScore));
    varhandle->AddVariable("BDTScore", ((boost::function<float (const T)>) IDVarHelper<T>::BDTScore));
#endif
    varhandle->AddVariable("ptRatio", ((boost::function<float (const T)>) IDVarHelper<T>::ptRatio));
    varhandle->AddVariable("pi0_n", ((boost::function<float (const T)>) IDVarHelper<T>::pi0_n));
    varhandle->AddVariable("pi0_vistau_m", ((boost::function<float (const T)>) IDVarHelper<T>::pi0_vistau_m));
    varhandle->AddVariable("DrMax", ((boost::function<float (const T)>) IDVarHelper<T>::DrMax));
    varhandle->AddVariable("MassTrkSys", ((boost::function<float (const T)>) IDVarHelper<T>::MassTrkSys));
    varhandle->AddVariable("TrFligthPathSig", ((boost::function<float (const T)>) IDVarHelper<T>::TrFligthPathSig));
    varhandle->AddVariable("TrkAvgDist", ((boost::function<float (const T)>) IDVarHelper<T>::TrkAvgDist));
    varhandle->AddVariable("IpSigLeadTrk", ((boost::function<float (const T)>) IDVarHelper<T>::IpSigLeadTrk));
    varhandle->AddVariable("NTracksdrdR", ((boost::function<float (const T)>) IDVarHelper<T>::NTracksdrdR));
    varhandle->AddVariable("CentFrac0102", ((boost::function<float (const T)>) IDVarHelper<T>::CentFrac0102));
    varhandle->AddVariable("FTrk02", ((boost::function<float (const T)>) IDVarHelper<T>::FTrk02));
    //cgen::end

    // substructure id
    varhandle->AddVariable("panTauCellBased_nChrg0204", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_nChrg0204));
    varhandle->AddVariable("panTauCellBased_nChrg01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_nChrg01));
    varhandle->AddVariable("panTauCellBased_nChrg02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_nChrg02));
    varhandle->AddVariable("panTauCellBased_nChrg04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_nChrg04));
    varhandle->AddVariable("panTauCellBased_nNeut0204", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_nNeut0204));
    varhandle->AddVariable("panTauCellBased_nNeut01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_nNeut01));
    varhandle->AddVariable("panTauCellBased_nNeut02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_nNeut02));
    varhandle->AddVariable("panTauCellBased_nNeut04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_nNeut04));
    varhandle->AddVariable("panTauCellBased_massChrgSys01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_massChrgSys01));
    varhandle->AddVariable("panTauCellBased_massChrgSys02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_massChrgSys02));
    varhandle->AddVariable("panTauCellBased_massChrgSys04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_massChrgSys04));
    varhandle->AddVariable("panTauCellBased_massNeutSys01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_massNeutSys01));
    varhandle->AddVariable("panTauCellBased_massNeutSys02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_massNeutSys02));
    varhandle->AddVariable("panTauCellBased_massNeutSys04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_massNeutSys04));
    varhandle->AddVariable("panTauCellBased_visTauM01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_visTauM01));
    varhandle->AddVariable("panTauCellBased_visTauM02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_visTauM02));
    varhandle->AddVariable("panTauCellBased_visTauM04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_visTauM04));
    varhandle->AddVariable("panTauCellBased_dRmax01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_dRmax01));
    varhandle->AddVariable("panTauCellBased_dRmax02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_dRmax02));
    varhandle->AddVariable("panTauCellBased_dRmax04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_dRmax04));
    varhandle->AddVariable("panTauCellBased_ipSigLeadTrk", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ipSigLeadTrk));
    varhandle->AddVariable("panTauCellBased_trFlightPathSig", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_trFlightPathSig));
    varhandle->AddVariable("panTauCellBased_ptRatio01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ptRatio01));
    varhandle->AddVariable("panTauCellBased_ptRatio02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ptRatio02));
    varhandle->AddVariable("panTauCellBased_ptRatio04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ptRatio04));
    varhandle->AddVariable("panTauCellBased_ptRatioNeut01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ptRatioNeut01));
    varhandle->AddVariable("panTauCellBased_ptRatioNeut02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ptRatioNeut02));
    varhandle->AddVariable("panTauCellBased_ptRatioNeut04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ptRatioNeut04));
    varhandle->AddVariable("panTauCellBased_ptRatioChrg01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ptRatioChrg01));
    varhandle->AddVariable("panTauCellBased_ptRatioChrg02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ptRatioChrg02));
    varhandle->AddVariable("panTauCellBased_ptRatioChrg04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_ptRatioChrg04));
    varhandle->AddVariable("panTauCellBased_fLeadChrg01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_fLeadChrg01));
    varhandle->AddVariable("panTauCellBased_fLeadChrg02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_fLeadChrg02));
    varhandle->AddVariable("panTauCellBased_fLeadChrg04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_fLeadChrg04));
    varhandle->AddVariable("panTauCellBased_eFrac0102", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFrac0102));
    varhandle->AddVariable("panTauCellBased_eFrac0204", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFrac0204));
    varhandle->AddVariable("panTauCellBased_eFrac0104", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFrac0104));
    varhandle->AddVariable("panTauCellBased_eFracChrg0102", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracChrg0102));
    varhandle->AddVariable("panTauCellBased_eFracChrg0204", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracChrg0204));
    varhandle->AddVariable("panTauCellBased_eFracChrg0104", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracChrg0104));
    varhandle->AddVariable("panTauCellBased_eFracNeut0102", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracNeut0102));
    varhandle->AddVariable("panTauCellBased_eFracNeut0204", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracNeut0204));
    varhandle->AddVariable("panTauCellBased_eFracNeut0104", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracNeut0104));
    varhandle->AddVariable("panTauCellBased_eFrac010201", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFrac010201));
    varhandle->AddVariable("panTauCellBased_eFrac010202", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFrac010202));
    varhandle->AddVariable("panTauCellBased_eFrac010204", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFrac010204));
    varhandle->AddVariable("panTauCellBased_eFracChrg010201", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracChrg010201));
    varhandle->AddVariable("panTauCellBased_eFracChrg010202", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracChrg010202));
    varhandle->AddVariable("panTauCellBased_eFracChrg010204", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracChrg010204));
    varhandle->AddVariable("panTauCellBased_eFracNeut010201", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracNeut010201));
    varhandle->AddVariable("panTauCellBased_eFracNeut010202", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracNeut010202));
    varhandle->AddVariable("panTauCellBased_eFracNeut010204", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracNeut010204));
    varhandle->AddVariable("panTauCellBased_eFrac020401", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFrac020401));
    varhandle->AddVariable("panTauCellBased_eFrac020402", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFrac020402));
    varhandle->AddVariable("panTauCellBased_eFrac020404", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFrac020404));
    varhandle->AddVariable("panTauCellBased_eFracChrg020401", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracChrg020401));
    varhandle->AddVariable("panTauCellBased_eFracChrg020402", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracChrg020402));
    varhandle->AddVariable("panTauCellBased_eFracChrg020404", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracChrg020404));
    varhandle->AddVariable("panTauCellBased_eFracNeut020401", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracNeut020401));
    varhandle->AddVariable("panTauCellBased_eFracNeut020402", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracNeut020402));
    varhandle->AddVariable("panTauCellBased_eFracNeut020404", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_eFracNeut020404));
    varhandle->AddVariable("panTauCellBased_rCal01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_rCal01));
    varhandle->AddVariable("panTauCellBased_rCal02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_rCal02));
    varhandle->AddVariable("panTauCellBased_rCal04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_rCal04));
    varhandle->AddVariable("panTauCellBased_rCalChrg01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_rCalChrg01));
    varhandle->AddVariable("panTauCellBased_rCalChrg02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_rCalChrg02));
    varhandle->AddVariable("panTauCellBased_rCalChrg04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_rCalChrg04));
    varhandle->AddVariable("panTauCellBased_rCalNeut01", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_rCalNeut01));
    varhandle->AddVariable("panTauCellBased_rCalNeut02", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_rCalNeut02));
    varhandle->AddVariable("panTauCellBased_rCalNeut04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_rCalNeut04));
    varhandle->AddVariable("panTauCellBased_dRminmaxPtChrg04", ((boost::function<float (const T)>) IDVarHelper<T>::panTauCellBased_dRminmaxPtChrg04));
  }
  
  template <class T>
  void Initialize(IDVarHandle<T>* varhandle, const T& dummy){
    //cgen::start build LLHIDVarInitializer
// #ifdef OBSOLTE
//     varhandle->AddVariable("LLHScore", ((boost::function<float (const T)>) IDVarLLHHelper<T>::LLHScore));
//     varhandle->AddVariable("BDTScore", ((boost::function<float (const T)>) IDVarLLHHelper<T>::BDTScore));
// #endif
    varhandle->AddVariable("ptRatio", ((boost::function<float (const T)>) IDVarLLHHelper<T>::ptRatio));
    varhandle->AddVariable("pi0_n", ((boost::function<float (const T)>) IDVarLLHHelper<T>::pi0_n));
    varhandle->AddVariable("pi0_vistau_m", ((boost::function<float (const T)>) IDVarLLHHelper<T>::pi0_vistau_m));
    varhandle->AddVariable("DrMax", ((boost::function<float (const T)>) IDVarLLHHelper<T>::DrMax));
    varhandle->AddVariable("MassTrkSys", ((boost::function<float (const T)>) IDVarLLHHelper<T>::MassTrkSys));
    varhandle->AddVariable("TrFligthPathSig", ((boost::function<float (const T)>) IDVarLLHHelper<T>::TrFligthPathSig));
    varhandle->AddVariable("TrkAvgDist", ((boost::function<float (const T)>) IDVarLLHHelper<T>::TrkAvgDist));
    varhandle->AddVariable("IpSigLeadTrk", ((boost::function<float (const T)>) IDVarLLHHelper<T>::IpSigLeadTrk));
    varhandle->AddVariable("NTracksdrdR", ((boost::function<float (const T)>) IDVarLLHHelper<T>::NTracksdrdR));
    varhandle->AddVariable("CentFrac0102", ((boost::function<float (const T)>) IDVarLLHHelper<T>::CentFrac0102));
    varhandle->AddVariable("FTrk02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::FTrk02));

    //cgen::end

    // substructure default id
    varhandle->AddVariable("panTauCellBased_nChrg0204", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_nChrg0204));
  varhandle->AddVariable("panTauCellBased_nChrg01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_nChrg01));
  varhandle->AddVariable("panTauCellBased_nChrg02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_nChrg02));
  // varhandle->AddVariable("panTauCellBased_nChrg04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_nChrg04));
  varhandle->AddVariable("panTauCellBased_nNeut0204", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_nNeut0204));
  varhandle->AddVariable("panTauCellBased_nNeut01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_nNeut01));
    varhandle->AddVariable("panTauCellBased_nNeut02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_nNeut02));
    //varhandle->AddVariable("panTauCellBased_nNeut04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_nNeut04));
  varhandle->AddVariable("panTauCellBased_massChrgSys01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_massChrgSys01));
  varhandle->AddVariable("panTauCellBased_massChrgSys02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_massChrgSys02));
    varhandle->AddVariable("panTauCellBased_massChrgSys04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_massChrgSys04));
  varhandle->AddVariable("panTauCellBased_massNeutSys01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_massNeutSys01));
  varhandle->AddVariable("panTauCellBased_massNeutSys02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_massNeutSys02));
  varhandle->AddVariable("panTauCellBased_massNeutSys04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_massNeutSys04));
  varhandle->AddVariable("panTauCellBased_visTauM01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_visTauM01));
    varhandle->AddVariable("panTauCellBased_visTauM02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_visTauM02));
  varhandle->AddVariable("panTauCellBased_visTauM04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_visTauM04));
  // varhandle->AddVariable("panTauCellBased_dRmax01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_dRmax01));
    varhandle->AddVariable("panTauCellBased_dRmax02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_dRmax02));
 varhandle->AddVariable("panTauCellBased_dRmax04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_dRmax04));
    varhandle->AddVariable("panTauCellBased_ipSigLeadTrk", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ipSigLeadTrk));
    varhandle->AddVariable("panTauCellBased_trFlightPathSig", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_trFlightPathSig));
  varhandle->AddVariable("panTauCellBased_ptRatio01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ptRatio01));
  varhandle->AddVariable("panTauCellBased_ptRatio02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ptRatio02));
    varhandle->AddVariable("panTauCellBased_ptRatio04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ptRatio04));
  varhandle->AddVariable("panTauCellBased_ptRatioNeut01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ptRatioNeut01));
  varhandle->AddVariable("panTauCellBased_ptRatioNeut02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ptRatioNeut02));
  varhandle->AddVariable("panTauCellBased_ptRatioNeut04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ptRatioNeut04));
  varhandle->AddVariable("panTauCellBased_ptRatioChrg01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ptRatioChrg01));
  varhandle->AddVariable("panTauCellBased_ptRatioChrg02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ptRatioChrg02));
  varhandle->AddVariable("panTauCellBased_ptRatioChrg04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_ptRatioChrg04));
  varhandle->AddVariable("panTauCellBased_fLeadChrg01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_fLeadChrg01));
    varhandle->AddVariable("panTauCellBased_fLeadChrg02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_fLeadChrg02));
  varhandle->AddVariable("panTauCellBased_fLeadChrg04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_fLeadChrg04));
  varhandle->AddVariable("panTauCellBased_eFrac0102", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFrac0102));
    varhandle->AddVariable("panTauCellBased_eFrac0204", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFrac0204));
    varhandle->AddVariable("panTauCellBased_eFrac0104", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFrac0104));
    //varhandle->AddVariable("panTauCellBased_eFracChrg0102", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracChrg0102));
  varhandle->AddVariable("panTauCellBased_eFracChrg0204", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracChrg0204));
  varhandle->AddVariable("panTauCellBased_eFracChrg0104", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracChrg0104));
  // varhandle->AddVariable("panTauCellBased_eFracNeut0102", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracNeut0102));
  varhandle->AddVariable("panTauCellBased_eFracNeut0204", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracNeut0204));
  varhandle->AddVariable("panTauCellBased_eFracNeut0104", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracNeut0104));
  // varhandle->AddVariable("panTauCellBased_eFrac010201", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFrac010201));
  // varhandle->AddVariable("panTauCellBased_eFrac010202", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFrac010202));
  // varhandle->AddVariable("panTauCellBased_eFrac010204", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFrac010204));
  // varhandle->AddVariable("panTauCellBased_eFracChrg010201", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracChrg010201));
  // varhandle->AddVariable("panTauCellBased_eFracChrg010202", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracChrg010202));
  // varhandle->AddVariable("panTauCellBased_eFracChrg010204", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracChrg010204));
  // varhandle->AddVariable("panTauCellBased_eFracNeut010201", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracNeut010201));
  // varhandle->AddVariable("panTauCellBased_eFracNeut010202", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracNeut010202));
  // varhandle->AddVariable("panTauCellBased_eFracNeut010204", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracNeut010204));
  // varhandle->AddVariable("panTauCellBased_eFrac020401", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFrac020401));
  // varhandle->AddVariable("panTauCellBased_eFrac020402", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFrac020402));
  // varhandle->AddVariable("panTauCellBased_eFrac020404", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFrac020404));
  // varhandle->AddVariable("panTauCellBased_eFracChrg020401", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracChrg020401));
  // varhandle->AddVariable("panTauCellBased_eFracChrg020402", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracChrg020402));
  // varhandle->AddVariable("panTauCellBased_eFracChrg020404", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracChrg020404));
  // varhandle->AddVariable("panTauCellBased_eFracNeut020401", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracNeut020401));
  // varhandle->AddVariable("panTauCellBased_eFracNeut020402", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracNeut020402));
  // varhandle->AddVariable("panTauCellBased_eFracNeut020404", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_eFracNeut020404));
  // varhandle->AddVariable("panTauCellBased_rCal01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_rCal01));
    varhandle->AddVariable("panTauCellBased_rCal02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_rCal02));
    varhandle->AddVariable("panTauCellBased_rCal04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_rCal04));
  // varhandle->AddVariable("panTauCellBased_rCalChrg01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_rCalChrg01));
 varhandle->AddVariable("panTauCellBased_rCalChrg02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_rCalChrg02));
    varhandle->AddVariable("panTauCellBased_rCalChrg04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_rCalChrg04));
  // varhandle->AddVariable("panTauCellBased_rCalNeut01", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_rCalNeut01));
  varhandle->AddVariable("panTauCellBased_rCalNeut02", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_rCalNeut02));
  varhandle->AddVariable("panTauCellBased_rCalNeut04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_rCalNeut04));
  varhandle->AddVariable("panTauCellBased_dRminmaxPtChrg04", ((boost::function<float (const T)>) IDVarLLHHelper<T>::panTauCellBased_dRminmaxPtChrg04));
  }
  
template<class T>
void AcquireVariables(VariablePool<T>* m_pool, bool doOpt=false){
    //cgen::start aquire variables
#ifdef OBSOLTE
    m_pool->Acquire("LLHScore", 02, doOpt);
    m_pool->Acquire("BDTScore", 03, doOpt);
#endif
    m_pool->Acquire("ptRatio", 04, doOpt);
    m_pool->Acquire("pi0_n", 05, doOpt);
    m_pool->Acquire("pi0_vistau_m", 06, doOpt);
    m_pool->Acquire("DrMax", 10, doOpt);
    m_pool->Acquire("MassTrkSys", 12, doOpt);
    m_pool->Acquire("TrFligthPathSig", 17, doOpt);
    m_pool->Acquire("TrkAvgDist", 20, doOpt);
    m_pool->Acquire("IpSigLeadTrk", 23, doOpt);
    m_pool->Acquire("NTracksdrdR", 50, doOpt);
    m_pool->Acquire("CentFrac0102", 53, doOpt);
    m_pool->Acquire("FTrk02", 56, doOpt);
    // new
    // m_pool->Acquire("ChPiEMEOverCaloEME", 70, doOpt);
    // m_pool->Acquire("EMPOverTrkSysP", 71, doOpt);
    // m_pool->Acquire("pantauFeature_CellBased_Charged_Ratio_EtOverEtNeutLowB", 72, doOpt);
    // m_pool->Acquire("pantauFeature_CellBased_Neutral_Ratio_EtOverEtAllConsts", 73, doOpt);
    // m_pool->Acquire("pantauFeature_CellBased_Charged_DeltaR_MaxToJetAxis_EtSort", 74, doOpt);
    // m_pool->Acquire("pantauFeature_CellBased_Charged_JetMoment_EtDR", 75, doOpt);
    // m_pool->Acquire("pantauFeature_CellBased_Basic_NOuterNeutConsts", 76, doOpt);
    // m_pool->Acquire("pantauFeature_CellBased_Charged_Mean_Et_WrtEtNeutLowB", 77, doOpt);
    // m_pool->Acquire("pantauFeature_CellBased_Charged_DeltaR_1stTo2nd_EtSort", 78, doOpt);
    

    // substructure default id
    m_pool->Acquire("panTauCellBased_nChrg0204", 100, doOpt);
    m_pool->Acquire("panTauCellBased_nChrg01", 101, doOpt);
    m_pool->Acquire("panTauCellBased_nChrg02", 102, doOpt);
    m_pool->Acquire("panTauCellBased_nNeut0204", 103, doOpt);
    m_pool->Acquire("panTauCellBased_nNeut01", 104, doOpt);
    m_pool->Acquire("panTauCellBased_nNeut02", 105, doOpt);
    m_pool->Acquire("panTauCellBased_massChrgSys01", 106, doOpt);
    m_pool->Acquire("panTauCellBased_massChrgSys02", 107, doOpt);
    m_pool->Acquire("panTauCellBased_massChrgSys04", 108, doOpt);
    m_pool->Acquire("panTauCellBased_massNeutSys01", 109, doOpt);
    m_pool->Acquire("panTauCellBased_massNeutSys02", 110, doOpt);
    m_pool->Acquire("panTauCellBased_massNeutSys04", 111, doOpt);
    m_pool->Acquire("panTauCellBased_visTauM01", 112, doOpt);
    m_pool->Acquire("panTauCellBased_visTauM02", 113, doOpt);
    m_pool->Acquire("panTauCellBased_visTauM04", 114, doOpt);
    m_pool->Acquire("panTauCellBased_dRmax02", 115, doOpt);
    m_pool->Acquire("panTauCellBased_dRmax04", 116, doOpt);
    m_pool->Acquire("panTauCellBased_ipSigLeadTrk", 117, doOpt);
    m_pool->Acquire("panTauCellBased_trFlightPathSig", 118, doOpt);
    m_pool->Acquire("panTauCellBased_ptRatio01", 119, doOpt);
    m_pool->Acquire("panTauCellBased_ptRatio02", 120, doOpt);
    m_pool->Acquire("panTauCellBased_ptRatio04", 121, doOpt);
    m_pool->Acquire("panTauCellBased_ptRatioNeut01", 122, doOpt);
    m_pool->Acquire("panTauCellBased_ptRatioNeut02", 123, doOpt);
    m_pool->Acquire("panTauCellBased_ptRatioNeut04", 124, doOpt);
    m_pool->Acquire("panTauCellBased_ptRatioChrg01", 125, doOpt);
    m_pool->Acquire("panTauCellBased_ptRatioChrg02", 126, doOpt);
    m_pool->Acquire("panTauCellBased_ptRatioChrg04", 127, doOpt);
    m_pool->Acquire("panTauCellBased_fLeadChrg01", 128, doOpt);
    m_pool->Acquire("panTauCellBased_fLeadChrg02", 129, doOpt);
    m_pool->Acquire("panTauCellBased_fLeadChrg04", 130, doOpt);
    m_pool->Acquire("panTauCellBased_eFrac0204", 131, doOpt);
    m_pool->Acquire("panTauCellBased_eFrac0104", 132, doOpt);
    m_pool->Acquire("panTauCellBased_eFracChrg0204", 133, doOpt);
    m_pool->Acquire("panTauCellBased_eFracChrg0104", 134, doOpt);
    m_pool->Acquire("panTauCellBased_eFracNeut0204", 135, doOpt);
    m_pool->Acquire("panTauCellBased_eFracNeut0104", 136, doOpt);
    m_pool->Acquire("panTauCellBased_rCal02", 137, doOpt);
    m_pool->Acquire("panTauCellBased_rCal04", 138, doOpt);
    m_pool->Acquire("panTauCellBased_rCalChrg02", 139, doOpt);
    m_pool->Acquire("panTauCellBased_rCalChrg04", 140, doOpt);
    m_pool->Acquire("panTauCellBased_rCalNeut02", 141, doOpt);
    m_pool->Acquire("panTauCellBased_rCalNeut04", 142, doOpt);
    m_pool->Acquire("panTauCellBased_dRminmaxPtChrg04", 143, doOpt);
    m_pool->Acquire("panTauCellBased_eFrac0102", 144, doOpt);
    /*
https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/TauID/TauDiscriminant/trunk/Root/CommonLikelihood.cxx fuer v Nummer
     */
    //cgen::end
  }
}
#endif
#endif // SKIM
#endif











#ifdef REFAC
#ifndef SKIM
#ifndef IDVarInitializer_H
#define IDVarInitializer_H

#ifndef __CINT__
#include "boost/function.hpp"
#endif //__CINT__
#include "IDVarHelper.h"
#include "IDVarLLHHelper.h"
#include "IDVarHandle.h"
#include "VariablePool.h"

namespace IDVarInitializer{

  template<class T>
  void Initialize(IDVarHandle<T>* varhandle, std::vector<T>& dummy){
    varhandle->AddVariable("ptRatio", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::ptRatio));
    varhandle->AddVariable("pi0_n", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::pi0_n));
    varhandle->AddVariable("pi0_vistau_m",
			   ((boost::function<float (const T)>) IDVarHelper<T>::pi0_vistau_m));
    varhandle->AddVariable("DrMax", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::DrMax));
    varhandle->AddVariable("MassTrkSys", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::MassTrkSys));
    varhandle->AddVariable("TrFligthPathSig", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::TrFligthPathSig));
    varhandle->AddVariable("TrkAvgDist", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::TrkAvgDist));
    varhandle->AddVariable("IpSigLeadTrk", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::IpSigLeadTrk));
    varhandle->AddVariable("NTracksdrdR", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::NTracksdrdR));
    varhandle->AddVariable("CentFrac0102", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::CentFrac0102));
    varhandle->AddVariable("FTrk02", 
			   ((boost::function<float (const T)>) IDVarHelper<T>::FTrk02));
  }
  
  template <class T>
  void Initialize(IDVarHandle<T>* varhandle, const T& dummy){
    varhandle->AddVariable("ptRatio", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::ptRatio));
    varhandle->AddVariable("pi0_n", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::pi0_n));
    varhandle->AddVariable("pi0_vistau_m", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::pi0_vistau_m));
    varhandle->AddVariable("DrMax", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::DrMax));
    varhandle->AddVariable("MassTrkSys", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::MassTrkSys));
    varhandle->AddVariable("TrFlightPathSig", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::TrFlightPathSig));
    varhandle->AddVariable("TrkAvgDist", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::TrkAvgDist));
    varhandle->AddVariable("IpSigLeadTrk", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::IpSigLeadTrk));
    varhandle->AddVariable("NTracksdrdR", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::NTracksdrdR));
    varhandle->AddVariable("CentFrac0102", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::CentFrac0102));
    varhandle->AddVariable("FTrk02", 
			   ((boost::function<float (const T)>) IDVarLLHHelper<T>::FTrk02));
  }
  
  template<class T>
  void AcquireVariables(VariablePool<T>* m_pool, bool doOpt=false){
    m_pool->Acquire("ptRatio", 04, doOpt);
    m_pool->Acquire("pi0_n", 05, doOpt);
    m_pool->Acquire("pi0_vistau_m", 06, doOpt);
    m_pool->Acquire("DrMax", 10, doOpt);
    m_pool->Acquire("MassTrkSys", 12, doOpt);
    m_pool->Acquire("TrFligthPathSig", 17, doOpt);
    m_pool->Acquire("TrkAvgDist", 20, doOpt);
    m_pool->Acquire("IpSigLeadTrk", 23, doOpt);
    m_pool->Acquire("NTracksdrdR", 50, doOpt);
    m_pool->Acquire("CentFrac0102", 53, doOpt);
    m_pool->Acquire("FTrk02", 56, doOpt);

    /*
https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/TauID/TauDiscriminant/trunk/Root/CommonLikelihood.cxx fuer v Nummer
     */
  }
}
#endif
#endif // SKIM
#endif
