#ifndef LLHCommon_H
#define LLHCommon_H

// STL include(s)
#include <string>
#include "TH1.h"
// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "CycleBase.h"
#include "BaseObjectCreator.h"

#include "Tau.h"
#include "TruthTau.h"
#include "AnalysisTau.h"
#include "Jet.h"
#include "Vertex.h"
#include "TauD3PDCollection.h"
#include "VertexD3PDCollection.h"
#include "AnalysisTauD3PDCollection.h"
#include "TruthTauD3PDCollection.h"
#include "EventInfo.h"


#include "GoodRunsLists/TGoodRunsList.h"

class LLHCommon : public CycleBase
{
public:
  /// standard constructor
  LLHCommon();
  /// standard destructor
  virtual ~LLHCommon() {};

  /// hook in SFrame event loop
  virtual void   AnalyseEvent(const SInputData& id,double dWeight) throw(SError) = 0;
  virtual void   StartCycle() throw(SError);
  virtual void   StartInputData(const SInputData& id) throw(SError);
  virtual void   StartInputFile(const SInputData &) throw(SError) = 0;

protected:
  /// private methods defining the selection steps
  bool ApplyPileupReweighting(float& fWeight);
  bool CheckGRL(float& fWeight);
  bool CheckJetCleaning(float fWeight, int iLevel);
  bool CheckNTaus(float fWeight, int iMin);
  bool CheckNVertex(float fWeight, int iMin);

  bool SelectPhysicsObjects(float& fWeight);

  /// objects responsible for reading the D3PD tree
  D3PDReader::EventInfo                     m_rEventInfo;
  D3PDReader::TruthTauD3PDCollection        m_rTruthTauCollection;
/* #ifndef SKIM */
/*   D3PDReader::AnalysisTauD3PDCollection     m_rAnalysisTauCollection; */
/* #endif */
  D3PDReader::TauD3PDCollection             m_rTauCollection;
  D3PDReader::JetD3PDCollection             m_rJetCollection;
  D3PDReader::VertexD3PDCollection          m_rVertexCollection;

  /// object creators
  BaseObjectCreator<D3PDReader::TruthTau>   m_oTruthTauCreator;
/* #ifndef SKIM */
/*   BaseObjectCreator<D3PDReader::AnalysisTau> m_oAnalysisTauCreator; */
/* #endif */
  BaseObjectCreator<D3PDReader::Tau>        m_oTauCreator;
  BaseObjectCreator<D3PDReader::Jet>        m_oJetCreator;
  BaseObjectCreator<D3PDReader::Vertex>     m_oVertexCreator;

  /// analysis objects
  std::vector<D3PDReader::TruthTau*>        m_vTruthTaus;
/* #ifndef SKIM */
/*   std::vector<D3PDReader::AnalysisTau*>     m_vAnalysisTaus; */
/* #endif */
  std::vector<D3PDReader::Tau*>             m_vTaus;
  std::vector<D3PDReader::Jet*>             m_vJets;
  std::vector<D3PDReader::Vertex*>          m_vVertices;

  // GoodRunList
  bool                                      c_do_grl;
  std::string                               c_grl_file;
  Root::TGoodRunsList                       m_GRL;
  
  // Jet cleaning
  bool                                      c_do_jet_cleaning;

  ClassDef(LLHCommon,0);
};

#endif // LLHCommon_H
