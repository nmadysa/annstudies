#ifndef SKIM
#ifndef LLHCommonUtilities_H
#define LLHCommonUtilities_H

#ifndef __CINT__
#include "boost/bind.hpp"
#endif //__CINT__

#include "AnalysisTau.h"
#include "TruthTau.h"
#include "Tau.h"
#include "Vertex.h"

namespace LLHCommonUtilities
{
  inline bool type_comp(const D3PDReader::Vertex* v1, const D3PDReader::Vertex* v2){
    return (v1->type() < v2->type());
  }
  inline bool TrackMatch(const D3PDReader::TruthTau* truetau, D3PDReader::Tau* tau){
    if((unsigned int)truetau->tauAssoc_index() == tau->GetIndex() && truetau->nProng() == tau->numTrack())
      return true;
    else 
      return false;
  }
  inline void truthmatching(std::vector<D3PDReader::AnalysisTau*>& vTaus){
    vTaus.erase(std::remove_if(vTaus.begin(), vTaus.end(),
			       [](D3PDReader::AnalysisTau* pTau) -> bool {return !pTau->hasTruthMatch();}),
		vTaus.end());
  }

}
#endif
#endif
