#ifndef SKIM
#ifndef IDVARCALCULATOR_H_
#define IDVARCALCULATOR_H_

#include <vector>
#include <memory>

#include "AnalysisTau.h"
#include "IDVarSet.h"
#include "PileUpCorrectionTool.h"

class TLorentzVector;

class Pion {
public:
  Pion(){}
  Pion(const float& pt,
       const float& eta,
       const float& phi,
       const float& m) {tlv.SetPtEtaPhiM(pt, eta, phi, m);}
  ~Pion(){}
  TLorentzVector tlv;
};

template<class Derived>
class IDVarCalculatorBase {
public:
  IDVarCalculatorBase(){
    m_pileUpCorrectionTool = new PileUpCorrectionTool();
    m_pileUpCorrectionTool->switchOff(true);
  }
  IDVarSet calculateVariables(const D3PDReader::AnalysisTau& tau, const int& nvtx = 1);
  void setupPileupCorrection(const std::string& c_pileup_correction_file){
    m_pileUpCorrectionTool->setInputFile(c_pileup_correction_file);
    m_pileUpCorrectionTool->switchOff(false);
    m_pileUpCorrectionTool->readInput();
  }

protected:
  PileUpCorrectionTool* m_pileUpCorrectionTool;
  void initialise(const D3PDReader::AnalysisTau& tau){
    m_vOuterChargedPions.clear();
    
    for(int iOuterTrk = 0; iOuterTrk < tau.seedCalo_wideTrk_n(); iOuterTrk++) {
      m_vOuterChargedPions.push_back(Pion(tau.seedCalo_wideTrk_pt().at(iOuterTrk),
					  tau.seedCalo_wideTrk_eta().at(iOuterTrk),
					  tau.seedCalo_wideTrk_phi().at(iOuterTrk),
					  139.57));				 
    }
    static_cast<Derived*>(this)->initialiseImpl(tau);
  }
  std::vector<Pion> m_vChargedPions;
  std::vector<Pion> m_vNeutralPions;
  std::vector<Pion> m_vOuterChargedPions;
  std::vector<Pion> m_vNeutrals;
  std::vector<Pion> m_vChargedHLV;
  std::vector<Pion> m_vNeutralHLV;
  std::vector<Pion> m_vNeutLowAHLV;
  std::vector<Pion> m_vNeutLowBHLV;
  std::vector<Pion> m_vOuterChrgHLV;
  std::vector<Pion> m_vOuterNeutHLV;
  float m_ftransSignficance;
  float m_ftrkTransSig1;
  float m_ftrkTransSig2;
  float m_ftrkTransSig3;
};

class CellBasedCalculator: public IDVarCalculatorBase<CellBasedCalculator> {
public:
  void initialiseImpl(const D3PDReader::AnalysisTau& tau);
};

class EFlowRecCalculator: public IDVarCalculatorBase<EFlowRecCalculator> {
public:
  void initialiseImpl(const D3PDReader::AnalysisTau& tau);
};

template<class Derived>
IDVarSet IDVarCalculatorBase<Derived>::calculateVariables(const D3PDReader::AnalysisTau& tau, const int& nvtx) {

  auto piIDVars = IDVarSet();
  initialise(tau);
  float chargedPiEnergy01 = 0.;	// cent only
  float chargedPiEnergy02 = 0.;	// core only
  float chargedPiEnergy04 = 0.;
  float chargedPiEnergy0102 = 0.; // inner ring only
  float chargedPiEnergy0204 = 0.; // outer ring only
  float chargedPiPtDr01 = 0.;	  // cent only
  float chargedPiPtDr02 = 0.;	  // core only
  float chargedPiPtDr04 = 0.;
  float chargedPiN01 = 0;	// cent only
  float chargedPiN02 = 0;	// core only
  float chargedPiN04 = 0;
  float chargedPiN0204 = 0;	// outer ring only
  float chargedPiDrMax01 = 0.;	// cent only
  float chargedPiDrMax02 = 0.;	// core only
  float chargedPiDrMax04 = 0.;
  float chargedPiPtLead01 = 0.;	// cent only
  float chargedPiPtLead02 = 0.;	// core only
  float chargedPiPtLead04 = 0.;
  auto chargedPiTLV01 = TLorentzVector(0., 0., 0., 0.);	// cent only
  auto chargedPiTLV02 = TLorentzVector(0., 0., 0., 0.);	// core only
  auto chargedPiTLV04 = TLorentzVector(0., 0., 0., 0.);
  auto chargedPiPtHigh04 = TLorentzVector(0., 0., 0., 0.); // iso only
  TLorentzVector chargedPiPtLow04;
  chargedPiPtLow04.SetPtEtaPhiM(1000000000., 0., 0., 0.); // iso only
  
  float neutralPiEnergy01 = 0.;	// cent only
  float neutralPiEnergy02 = 0.;	// core only
  float neutralPiEnergy04 = 0.;
  float neutralPiEnergy0102 = 0.; // inner ring only
  float neutralPiEnergy0204 = 0.; // outer ring only
  float neutralPiPtDr01 = 0.;	  // cent only
  float neutralPiPtDr02 = 0.;	  // core only
  float neutralPiPtDr04 = 0.;
  float neutralPiN01 = 0.;	// cent only
  float neutralPiN02 = 0.;	// core only
  float neutralPiN04 = 0.;
  float neutralPiN0204 = 0.;	// outer ring only
  auto neutralPiTLV01 = TLorentzVector(0., 0., 0., 0.); // cent only
  auto neutralPiTLV02 = TLorentzVector(0., 0., 0., 0.); // core only
  auto neutralPiTLV04 = TLorentzVector(0., 0., 0., 0.);
  
  // core only extend with tracks
  for (auto pion : m_vChargedPions) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dRChrgPi.push_back(dr);
    // cent
    if(dr < 0.1){
      chargedPiEnergy01 += tlvPi.Pt();
      chargedPiPtDr01 += tlvPi.Pt()*dr;
      chargedPiN01++;
      chargedPiTLV01 += tlvPi;
      if(dr > chargedPiDrMax01)
	chargedPiDrMax01 = dr;
      if(tlvPi.Pt() > chargedPiPtLead01)
	chargedPiPtLead01 = tlvPi.Pt();
    }
    // core
    chargedPiEnergy02 += tlvPi.Pt();
    chargedPiPtDr02 += tlvPi.Pt()*dr;
    chargedPiN02++;
    chargedPiTLV02 += tlvPi;
    if(dr > chargedPiDrMax02)
      chargedPiDrMax02 = dr;
    if(tlvPi.Pt() > chargedPiPtLead02)
      chargedPiPtLead02 = tlvPi.Pt();
    // iso
    chargedPiEnergy04 += tlvPi.Pt();
    chargedPiPtDr04 += tlvPi.Pt()*dr;
    chargedPiN04++;    
    chargedPiTLV04 += tlvPi;
    if(dr > chargedPiDrMax04)
      chargedPiDrMax04 = dr;
    if(tlvPi.Pt() > chargedPiPtLead04){
      chargedPiPtLead04 = tlvPi.Pt();
      chargedPiPtHigh04 = tlvPi;
    }
    if(tlvPi.Pt() < chargedPiPtLow04.Pt())
      chargedPiPtLow04 = tlvPi;
    // inner ring
    if(dr > 0.1)
      chargedPiEnergy0102 += tlvPi.Pt();
  }

  // core only
  for(auto pion : m_vNeutralPions) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dRNeutPi.push_back(dr);
    // cent
    if(dr < 0.1){
      neutralPiEnergy01 += tlvPi.Pt();
      neutralPiPtDr01 += tlvPi.Pt()*dr;
      neutralPiN01++;
      neutralPiTLV01 += tlvPi;
    }
    // core
    neutralPiEnergy02 += tlvPi.Pt();
    neutralPiPtDr02 += tlvPi.Pt()*dr;
    neutralPiN02++;
    neutralPiTLV02 += tlvPi;
    // iso
    neutralPiEnergy04 += tlvPi.Pt();
    neutralPiPtDr04 += tlvPi.Pt()*dr;
    neutralPiN04++;
    neutralPiTLV04 += tlvPi;
    // inner ring
    if(dr > 0.1)
      neutralPiEnergy0102 += tlvPi.Pt();
  }

  // isolation only
  for (auto pion : m_vOuterChargedPions) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dRCharged.push_back(dr);
    // iso
    chargedPiEnergy04 += tlvPi.Pt();
    chargedPiPtDr04 += tlvPi.Pt()*dr;
    chargedPiN04++;
    chargedPiTLV04 += tlvPi;
    if(dr > chargedPiDrMax04)
      chargedPiDrMax04 = dr;
    if(tlvPi.Pt() > chargedPiPtLead04){
      chargedPiPtLead04 = tlvPi.Pt();
      chargedPiPtHigh04 = tlvPi;
    }
    if(tlvPi.Pt() < chargedPiPtLow04.Pt())
      chargedPiPtLow04 = tlvPi;
    // outer ring
    chargedPiN0204++;
    chargedPiEnergy0204 += tlvPi.Pt();
  }

  // neutrals, more than pi0
  for(auto pion : m_vNeutrals) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dRNeutral.push_back(dr);
  }
  
  for(auto pion : m_vChargedHLV) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dRChrgHLV.push_back(dr);
  }
  
  for(auto pion : m_vNeutralHLV) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dRNeutHLV.push_back(dr);
  }
  
  for(auto pion : m_vNeutLowAHLV) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dRNeutLowAHLV.push_back(dr);
  }
  
  for(auto pion : m_vNeutLowBHLV) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dRNeutLowBHLV.push_back(dr);
  }
  
  for(auto pion : m_vOuterChrgHLV) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dROuterChrg.push_back(dr);
  }
  
  // used as extension
  for(auto pion : m_vOuterNeutHLV) {
    auto tlvPi = pion.tlv;
    float dr = tlvPi.DeltaR(tau.TLV());
    piIDVars.dROuterNeut.push_back(dr);
    // iso
    neutralPiEnergy04 += tlvPi.Pt();
    neutralPiPtDr04 += tlvPi.Pt()*dr;
    neutralPiN04++;
    neutralPiTLV04 += tlvPi;
    // outer ring
    neutralPiEnergy0204 += tlvPi.Pt();
    neutralPiN0204++;
  }
  piIDVars.nChrg0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg0204",
							       &tau,
							       nvtx,
							       chargedPiN0204);// original
  piIDVars.nChrg01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg01",
							     &tau,
							     nvtx,
							     chargedPiN01);
  piIDVars.nChrg02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg02",
							     &tau,
							     nvtx,
							     chargedPiN02);
  piIDVars.nChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg04",
							     &tau,
							     nvtx,
							     chargedPiN04);

  piIDVars.nNeut0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut0204",
							       &tau,
							       nvtx,
							       neutralPiN0204);
  piIDVars.nNeut01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut01",
							     &tau,
							     nvtx,
							     neutralPiN01);
  piIDVars.nNeut02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut02",
							     &tau,
							     nvtx,
							     neutralPiN02);// original
  piIDVars.nNeut04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut04",
							     &tau,
							     nvtx,
							     neutralPiN04);
  piIDVars.massChrgSys01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys01",
								   &tau,
								   nvtx,
								   chargedPiTLV01.M());  
  piIDVars.massChrgSys02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys02",
								   &tau,
								   nvtx,
								   chargedPiTLV02.M());
  piIDVars.massChrgSys04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys04",
								   &tau,
								   nvtx,
								   chargedPiTLV04.M()); // original

  piIDVars.massNeutSys01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys01",
								   &tau,
								   nvtx,
								   neutralPiTLV01.M());  
  piIDVars.massNeutSys02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys02",
								   &tau,
								   nvtx,
								   neutralPiTLV02.M());
  piIDVars.massNeutSys04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys04",
								   &tau,
								   nvtx,
								   neutralPiTLV04.M());

  piIDVars.dRmax01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRmax01",
							     &tau,
							     nvtx,
							     chargedPiDrMax01);
  piIDVars.dRmax02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRmax02",
							     &tau,
							     nvtx,
							     chargedPiDrMax02); // original
  piIDVars.dRmax04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRmax04",
							     &tau,
							     nvtx,
							     chargedPiDrMax04);

  piIDVars.visTauM01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM01",
							       &tau,
							       nvtx,
							       neutralPiTLV01.M() + chargedPiTLV01.M());
  piIDVars.visTauM02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM02",
							       &tau,
							       nvtx,
							       neutralPiTLV02.M() + chargedPiTLV02.M()); // original
  piIDVars.visTauM04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM04",
							       &tau,
							       nvtx,
							       neutralPiTLV04.M() + chargedPiTLV04.M());


  piIDVars.ipSigLeadTrk = m_ftransSignficance; // original
  piIDVars.trFlightPathSig = m_ftrkTransSig1 + m_ftrkTransSig2 + m_ftrkTransSig3; // original

  piIDVars.ptRatio01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio01",
							       &tau,
							       nvtx,
							       (chargedPiEnergy01 + neutralPiEnergy01)/ tau.TLV().Pt());
  piIDVars.ptRatio02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio02",
							       &tau,
							       nvtx,
							       (chargedPiEnergy02 + neutralPiEnergy02)/ tau.TLV().Pt());
  piIDVars.ptRatio04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio04",
							       &tau,
							       nvtx,
							       (chargedPiEnergy04 + neutralPiEnergy04)/ tau.TLV().Pt()); // original

  piIDVars.ptRatioNeut01 =   piIDVars.ptRatio04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut01",
											  &tau,
											  nvtx,
											  neutralPiEnergy01/ tau.TLV().Pt());
  piIDVars.ptRatioNeut02 =   piIDVars.ptRatio04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut02",
											  &tau,
											  nvtx,
											  neutralPiEnergy02/ tau.TLV().Pt());
  piIDVars.ptRatioNeut04 =   piIDVars.ptRatio04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut04",
											  &tau,
											  nvtx,
											  neutralPiEnergy04/ tau.TLV().Pt());

  piIDVars.ptRatioChrg01 =   piIDVars.ptRatio04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg01",
											  &tau,
											  nvtx,
											  chargedPiEnergy01/ tau.TLV().Pt());
  piIDVars.ptRatioChrg02 =   piIDVars.ptRatio04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg02",
											  &tau,
											  nvtx,
											  chargedPiEnergy02/ tau.TLV().Pt());
  piIDVars.ptRatioChrg04 =   piIDVars.ptRatio04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg04",
											  &tau,
											  nvtx,
											  chargedPiEnergy04/ tau.TLV().Pt());

  piIDVars.fLeadChrg01 = (chargedPiEnergy01 + neutralPiEnergy01) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg01",
														&tau,
														nvtx,
														chargedPiPtLead01 / (chargedPiEnergy01 + neutralPiEnergy01)) : -1111.;
  piIDVars.fLeadChrg02 = (chargedPiEnergy02 + neutralPiEnergy02) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg02",
														&tau,
														nvtx,
														chargedPiPtLead02 / (chargedPiEnergy02 + neutralPiEnergy02)) : -1111.; // original
  piIDVars.fLeadChrg04 = (chargedPiEnergy04 + neutralPiEnergy04) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg04",
														&tau,
														nvtx,
														chargedPiPtLead04 / (chargedPiEnergy04 + neutralPiEnergy04)) : -1111.;

  piIDVars.eFrac0102 = (chargedPiEnergy02 + neutralPiEnergy02) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0102",
													      &tau,
													      nvtx,
													      (chargedPiEnergy01 + neutralPiEnergy01) / (chargedPiEnergy02 + neutralPiEnergy02)) : -1111.; // original
  piIDVars.eFrac0204 = (chargedPiEnergy02 + neutralPiEnergy02) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0204",
													      &tau,
													      nvtx,
													      (chargedPiEnergy02 + neutralPiEnergy02) / (chargedPiEnergy04 + neutralPiEnergy04)) : -1111.;
  piIDVars.eFrac0104 = (chargedPiEnergy01 + neutralPiEnergy01) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0104",
													      &tau,
													      nvtx,
													      (chargedPiEnergy01 + neutralPiEnergy01) / (chargedPiEnergy04 + neutralPiEnergy04)) : -1111.;
  piIDVars.eFracChrg0102 = chargedPiEnergy02 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg0102",
											    &tau,
											    nvtx,
											    chargedPiEnergy01 / chargedPiEnergy02) : -1111.; 
  piIDVars.eFracChrg0204 = chargedPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg0204",
											    &tau,
											    nvtx,
											    chargedPiEnergy02 / chargedPiEnergy04) : -1111.; 

  piIDVars.eFracChrg0104 = chargedPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg0104",
											    &tau,
											    nvtx,
											    chargedPiEnergy01 / chargedPiEnergy04) : -1111.; 

  piIDVars.eFracNeut0102 = neutralPiEnergy02 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut0102",
											    &tau,
											    nvtx,
											    neutralPiEnergy01 / neutralPiEnergy02) : -1111.; 
  piIDVars.eFracNeut0204 = neutralPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut0204",
											    &tau,
											    nvtx,
											    neutralPiEnergy02 / neutralPiEnergy04) : -1111.; 

  piIDVars.eFracNeut0104 = neutralPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut0104",
											    &tau,
											    nvtx,
											    neutralPiEnergy01 / neutralPiEnergy04) : -1111.; 

  piIDVars.eFrac010201 = (chargedPiEnergy01 + neutralPiEnergy01) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac010201",
														&tau,
														nvtx,
														(chargedPiEnergy0102 + neutralPiEnergy0102) / (chargedPiEnergy01 + neutralPiEnergy01)) : -1111.;
  
  piIDVars.eFrac010202 = (chargedPiEnergy02 + neutralPiEnergy02) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac010202",
														&tau,
														nvtx,
														(chargedPiEnergy0102 + neutralPiEnergy0102) / (chargedPiEnergy02 + neutralPiEnergy02)) : -1111.; 
  piIDVars.eFrac010204 = (chargedPiEnergy04 + neutralPiEnergy04) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac010204",
														&tau,
														nvtx,
														(chargedPiEnergy0102 + neutralPiEnergy0102) / (chargedPiEnergy04 + neutralPiEnergy04)) : -1111.; 

  piIDVars.eFracChrg010201 = chargedPiEnergy01 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg010201",
											      &tau,
											      nvtx,
											      chargedPiEnergy0102 / chargedPiEnergy01) : -1111.; 
  piIDVars.eFracChrg010202 = chargedPiEnergy02 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg010202",
											      &tau,
											      nvtx,
											      chargedPiEnergy0102 / chargedPiEnergy02) : -1111.; 
  piIDVars.eFracChrg010204 = chargedPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg010204",
											      &tau,
											      nvtx,
											      chargedPiEnergy0102 / chargedPiEnergy04) : -1111.;

  piIDVars.eFracNeut010201 = neutralPiEnergy01 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut010201",
											      &tau,
											      nvtx,
											      neutralPiEnergy0102 / neutralPiEnergy01) : -1111.; 
  piIDVars.eFracNeut010202 = neutralPiEnergy02 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut010202",
											      &tau,
											      nvtx,
											      neutralPiEnergy0102 / neutralPiEnergy02) : -1111.; 
  piIDVars.eFracNeut010204 = neutralPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut010204",
											      &tau,
											      nvtx,
											      neutralPiEnergy0102 / neutralPiEnergy04) : -1111.;

  piIDVars.eFrac020401 = (chargedPiEnergy01 + neutralPiEnergy01) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac020401",
														&tau,
														nvtx,
														(chargedPiEnergy0204 + neutralPiEnergy0204) / (chargedPiEnergy01 + neutralPiEnergy01)) : -1111.; 
  piIDVars.eFrac020402 = (chargedPiEnergy02 + neutralPiEnergy02) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac020402",
														&tau,
														nvtx,
														(chargedPiEnergy0204 + neutralPiEnergy0204) / (chargedPiEnergy02 + neutralPiEnergy02)) : -1111.; 
  piIDVars.eFrac020404 = (chargedPiEnergy04 + neutralPiEnergy04) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac020404",
														&tau,
														nvtx,
														(chargedPiEnergy0204 + neutralPiEnergy0204) / (chargedPiEnergy04 + neutralPiEnergy04)) : -1111.; 

  piIDVars.eFracChrg020401 = chargedPiEnergy01 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg020401",
											      &tau,
											      nvtx,
											      chargedPiEnergy0204 / chargedPiEnergy01) : -1111.; 
  piIDVars.eFracChrg020402 = chargedPiEnergy02 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg020402",
											      &tau,
											      nvtx,
											      chargedPiEnergy0204 / chargedPiEnergy02) : -1111.; 
  piIDVars.eFracChrg020404 = chargedPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg020404",
											      &tau,
											      nvtx,
											      chargedPiEnergy0204 / chargedPiEnergy04) : -1111.;

  piIDVars.eFracNeut020401 = neutralPiEnergy01 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut020401",
											      &tau,
											      nvtx,
											      neutralPiEnergy0204 / neutralPiEnergy01) : -1111.; 
  piIDVars.eFracNeut020402 = neutralPiEnergy02 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut020402",
											      &tau,
											      nvtx,
											      neutralPiEnergy0204 / neutralPiEnergy02) : -1111.; 
  piIDVars.eFracNeut020402 = neutralPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut020402",
											      &tau,
											      nvtx,
											      neutralPiEnergy0204 / neutralPiEnergy04) : -1111.;

  piIDVars.rCal01 = (chargedPiEnergy01 + neutralPiEnergy01) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCal01",
													   &tau,
													   nvtx,
													   (chargedPiPtDr01 + neutralPiPtDr01) / (chargedPiEnergy01 + neutralPiEnergy01)) : -1111.;
  piIDVars.rCal02 = (chargedPiEnergy02 + neutralPiEnergy02) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCal02",
													   &tau,
													   nvtx,
													   (chargedPiPtDr02 + neutralPiPtDr02) / (chargedPiEnergy02 + neutralPiEnergy02)) : -1111.;
  piIDVars.rCal04 = (chargedPiEnergy04 + neutralPiEnergy04) != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCal04",
													   &tau,
													   nvtx,
													   (chargedPiPtDr04 + neutralPiPtDr04) / (chargedPiEnergy04 + neutralPiEnergy04)) : -1111.; // original

  piIDVars.rCalChrg01 = chargedPiEnergy01 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalChrg01",
											 &tau,
											 nvtx,chargedPiPtDr01 / chargedPiEnergy01) : -1111.;
  piIDVars.rCalChrg02 = chargedPiEnergy02 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalChrg02",
											 &tau,
											 nvtx,
											 chargedPiPtDr02 / chargedPiEnergy02) : -1111.;
  piIDVars.rCalChrg04 = chargedPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalChrg04",
											 &tau,
											 nvtx,
											 chargedPiPtDr04 / chargedPiEnergy04) : -1111.; // original rtrack/trkavgdist

  piIDVars.rCalNeut01 = neutralPiEnergy01 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalNeut01",
											 &tau,
											 nvtx,
											 neutralPiPtDr01 / neutralPiEnergy01) : -1111.;
  piIDVars.rCalNeut02 = neutralPiEnergy02 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalNeut02",
											 &tau,
											 nvtx,
											 neutralPiPtDr02 / neutralPiEnergy02) : -1111.;
  piIDVars.rCalNeut04 = neutralPiEnergy04 != 0 ? m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalNeut04",
											 &tau,
											 nvtx,
											 neutralPiPtDr04 / neutralPiEnergy04) : -1111.;
  piIDVars.dRminmaxPtChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRminmaxPtChrg04",
								      &tau,
								      nvtx,
								      chargedPiPtHigh04.DeltaR(chargedPiPtLow04));
  
  return piIDVars;
}

#endif /* IDVARCALCULATOR_H_ */
#endif
