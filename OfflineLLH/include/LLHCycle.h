#ifndef SKIM
#ifndef LLHCycle_H
#define LLHCycle_H

// STL include(s)
#include <string>

//custom includes
#include "AnalysisLLHCommon.h"

//Tools includes
#include "PileupReweighting/TPileupReweighting.h"

class VariablePlotter;

class LLHCycle : public AnalysisLLHCommon
{
public:
  /// standard constructor
  LLHCycle();
  /// standard destructor
  virtual ~LLHCycle();

  /// hook in SFrame event loop
  virtual void AnalyseEvent(const SInputData& id,double dWeight) throw(SError);
  void StartCycle() throw (SError);
  virtual void StartInputFile(const SInputData &) throw(SError);
  virtual void FinishMasterInputData(const SInputData& id) throw(SError);

private:
  //skim properties

  VariablePlotter* m_pPlotter;
  
  ClassDef(LLHCycle,0);
};

#endif // LLHCycle_H
#endif // SKIM
