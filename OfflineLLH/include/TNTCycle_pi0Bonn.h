#ifdef PI0BONN
#ifndef TNTCycle_H
#define TNTCycle_H

// STL include(s)
#include <string>
#include <set>

#include "LLHCommon.h"

class TNTCycle : public LLHCommon {
public:
  /// standard constructor
  TNTCycle();
  /// standard destructor
  virtual ~TNTCycle();

  /// hook in SFrame event loop
  virtual void AnalyseEvent(const SInputData& id, double dWeight) throw (SError);
  virtual void StartInputFile(const SInputData & id) throw (SError);
  virtual void StartInputData(const SInputData & id) throw (SError);

private:
  void DeclareVariables();
  void CalculateVariables();
  void GetTruthInfo(D3PDReader::Tau*);
  void GetTrueTauInfo();
  int HasTruthMatch(const D3PDReader::Tau* const pTau);

  //tau variables
  std::vector<float> m_vtau_Et;
  std::vector<float> m_vtau_eta;
  std::vector<float> m_vtau_m;
  std::vector<float> m_vtau_phi;
  std::vector<float> m_vtau_charge;
  std::vector<int> m_vtau_author;
  std::vector<int> m_vtau_numTrack;
  std::vector<int> m_vtau_hasTruthMatch;

  std::vector<float> m_vtau_BDTJetScore;
  std::vector<float> m_vtau_SafeLikelihood;
  std::vector<int> m_vtau_LLH_loose;
  std::vector<int> m_vtau_LLH_medium;
  std::vector<int> m_vtau_LLH_tight;
  std::vector<int> m_vtau_BDT_loose;
  std::vector<int> m_vtau_BDT_medium;
  std::vector<int> m_vtau_BDT_tight;
  std::vector<float> m_vtau_likelihood;

  std::vector<float> m_vtau_ipSigLeadTrk;
  std::vector<float> m_vtau_massTrkSys;
  std::vector<float> m_vtau_trFlightPathSig;
  std::vector<float> m_vtau_seedCalo_trkAvgDist;
  std::vector<float> m_vtau_seedCalo_dRmax;
  std::vector<int> m_vtau_seedCalo_wideTrk_n;
  std::vector<float> m_vtau_calcVars_corrCentFrac;
  std::vector<float> m_vtau_calcVars_corrFTrk;
  std::vector<float> m_vtau_etOverPtLeadTrk;
  std::vector<float> m_vtau_leadTrack_eta;
  std::vector<float> m_vtau_ptRatio;
  std::vector<float> m_vtau_pi0_vistau_m;
  std::vector<int> m_vtau_pi0_n;

  std::vector<float> m_vtau_matchVisEt;
  std::vector<float> m_vtau_matchVisEta;
  std::vector<float> m_vtau_matchVisPhi;
  std::vector<float> m_vtau_matchVisM;
  std::vector<float> m_vtau_matchPt;
  std::vector<float> m_vtau_matchEta;
  std::vector<float> m_vtau_matchPhi;
  std::vector<float> m_vtau_matchM;
  std::vector<int> m_vtau_matchNProng;

  std::vector<float> m_vtruth_VisEt;
  std::vector<float> m_vtruth_VisEta;
  std::vector<float> m_vtruth_VisPhi;
  std::vector<float> m_vtruth_VisM;
  std::vector<float> m_vtruth_Pt;
  std::vector<float> m_vtruth_Eta;
  std::vector<float> m_vtruth_Phi;
  std::vector<float> m_vtruth_M;
  std::vector<int> m_vtruth_NProng;

  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmean;
  std::vector<int > m_vtau_pi0Bonn_nPi0Cluster;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_expectBLayerHit;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nTRTHighTOutliers;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_noCorr_pt;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_mergedScore;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nGangedFlaggedFakes;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nTRTXenonHits;
  std::vector<int > m_vtau_seedCalo_wideTrk_atTJVA_n;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nSCTOutliers;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_qoverp;
  std::vector<vector<double> > m_vtau_pi0Bonn_pi0_E;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_ws5;
  std::vector<vector<vector<int> > > m_vtau_pi0Bonn_ShotsInPi0Clusters;
  std::vector<int > m_vtau_pi0Bonn_nPi0;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_BDTScore;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_phi;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nSCTDeadSensors;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_CENTER_LAMBDA_helped;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmode;
  std::vector<vector<vector<int> > > m_vtau_pi0Bonn_Pi0ClustersInPi0Candidates;
  std::vector<vector<int> > m_vtau_pi0Bonn_TauShot_Shot_nPhotons;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_MAX;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atTJVA_phi;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nPixelSpoiltHits;
  std::vector<int > m_vtau_pi0Bonn_TauShot_nShot;
  std::vector<double > m_vtau_pi0Bonn_visTau_pt;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atTJVA_pt;
  // std::vector<int > m_vtau_seedCalo_wideTrk_n;
  std::vector<double > m_vtau_pi0Bonn_visTau_E;
  std::vector<vector<int> > m_vtau_pi0Bonn_nShotsInPi0Clusters;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_noCorr_phi;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nPixSharedHits;
  std::vector<double > m_vtau_pi0Bonn_visTau_eta;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nPixelDeadSensors;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nBLayerOutliers;
  std::vector<vector<vector<float> > > m_vtau_pi0Bonn_TauShot_Shot_cellEta;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_log_SECOND_ENG_DENS;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_FIRST_ETA;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_pt;
  std::vector<vector<vector<float> > > m_vtau_pi0Bonn_TauShot_Shot_cellPt;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atPV_phi;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nContribPixelLayers;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_eta;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_noCorr_eta;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM1;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM2;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atTJVA_qoverp;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_At_DELTA_THETA;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_At_LATERAL;
  std::vector<vector<int> > m_vtau_pi0Bonn_Pi0Cluster_NHitsInEM1;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_EcoreOverEEM1;
  std::vector<vector<int> > m_vtau_pi0Bonn_nPi0ClustersInPi0Candidates;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nPixHits;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nBLayerSplitHits;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_EM;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atTJVA_d0;
  std::vector<vector<int> > m_vtau_pi0Bonn_Pi0Cluster_NPosECells_EM2;
  std::vector<vector<int> > m_vtau_pi0Bonn_Pi0Cluster_NPosECells_EM1;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_fracSide_3not1;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_phi;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atPV_z0;
  std::vector<double > m_vtau_pi0Bonn_sumPi0_pt;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atPV_qoverp;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nPixelOutliers;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nBLSharedHits;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atPV_pt;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_At_LONGITUDINAL;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_E;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_signalScore;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_eta;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_eta;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nPixSplitHits;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nHits;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_pt5;
  std::vector<double > m_vtau_pi0Bonn_sumPi0_E;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nSCTSpoiltHits;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_pt1;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_d0;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_pt3;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atTJVA_eta;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_At_DELTA_PHI;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_theta;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_noCorr_E;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_Fside_5not3;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atPV_theta;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_Fside_5not1;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_phi;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_SECOND_R;
  std::vector<vector<double> > m_vtau_pi0Bonn_pi0_phi;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nBLHits;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_z0;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_LONGITUDINAL;
  std::vector<double > m_vtau_pi0Bonn_sumPi0_eta;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nSCTHoles;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atTJVA_theta;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_SECOND_LAMBDA;
  std::vector<vector<double> > m_vtau_pi0Bonn_pi0_pt;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_Abs_DELTA_THETA;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nSCTHits;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_CORE;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nTRTHits;
  std::vector<vector<int> > m_vtau_pi0Bonn_Pi0Cluster_NPosCells_PS;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_Fside_3not1;
  std::vector<double > m_vtau_pi0Bonn_visTau_phi;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_sdevPt5;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nSCTDoubleHoles;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atPV_eta;
  std::vector<vector<vector<float> > > m_vtau_pi0Bonn_TauShot_Shot_cellPhi;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nTRTHoles;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_Abs_DELTA_PHI;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nTRTOutliers;
  std::vector<vector<double> > m_vtau_pi0Bonn_pi0_eta;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_pt1OverPt3;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atPV_d0;
  std::vector<vector<float> > m_vtau_seedCalo_wideTrk_atTJVA_z0;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_deltaPt12_min;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_AsymmetryWRTTrack;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_CORE;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_pt;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nSCTSharedHits;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nPixHoles;
  std::vector<double > m_vtau_pi0Bonn_sumPi0_phi;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_fracSide_5not1;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_fracSide_5not3;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nGangedPixels;
  std::vector<vector<int> > m_vtau_seedCalo_wideTrk_nTRTHighTHits;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_At_SECOND_ENG_DENS;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_MAX;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_LATERAL;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_pt;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_EM;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM2;
  std::vector<vector<float> > m_vtau_pi0Bonn_TauShot_Shot_pt3OverPt5;
  std::vector<vector<double> > m_vtau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM1;
  
  int m_itau_n;
  int m_itrueTau_n;
  int m_inVtx;
  float m_fmu;
  unsigned int m_ilbn;
  unsigned int m_iRunNumber;
  unsigned int m_iEventNumber;

  ClassDef(TNTCycle, 0);
};

#endif // TNTCycle_H
#endif // PI0BONN
