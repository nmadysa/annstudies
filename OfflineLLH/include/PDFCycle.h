#ifndef SKIM
#ifndef PDFCycle_H
#define PDFCycle_H

//custom includes
#include "AnalysisLLHCommon.h"

class PDFCycle : public AnalysisLLHCommon
{
public:
  /// standard constructor
  PDFCycle();
  /// standard destructor
  virtual ~PDFCycle();

  /// hook in SFrame event loop
  virtual void   AnalyseEvent(const SInputData& id,double dWeight) throw(SError);
  virtual void   StartInputFile(const SInputData &) throw(SError);

private:
  bool b_switchOffTrFlightPathSig;
  bool CheckTrFlightPathSig(D3PDReader::AnalysisTau* const pTau);
  void CheckTrFlightPathSig(std::vector<D3PDReader::AnalysisTau*>& vTaus);
  
  ClassDef(PDFCycle,0);
};

#endif // PDFCycle_H
#endif // SKIM
