#ifndef SKIM
#ifndef NtupleCycle_H
#define NtupleCycle_H

// STL include(s)
#include <string>

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "CycleBase.h"
#include "BaseObjectCreator.h"

//TODO: refactor make forward declaration
//custom includes
#include "AnalysisLLHCommon.h"
#include "LLHCalculator.h"
#include "IDVarHandle.h"
#include "IDVarHelper.h"
#include "VariablePool.h"
#include "TreeFillerTool.h"
#include "IDVarCalculator.h"

class NtupleCycle : public AnalysisLLHCommon
{
public:
  /// standard constructor
  NtupleCycle();
  /// standard destructor
  virtual ~NtupleCycle();

  /// hook in SFrame event loop
  virtual void AnalyseEvent(const SInputData& id,double dWeight) throw(SError);
  virtual void StartCycle() throw(SError);
  virtual void StartInputFile(const SInputData &) throw(SError);
  virtual void StartInputData(const SInputData &) throw(SError);
  virtual void FinishMasterInputData(const SInputData& id) throw(SError);

private:

  void FillTruthNormalisationHist();
  void FillVariables();
  void ApplyTruthMatch();
  void HistsForEfficiency();
    
  LLHCalculator* m_pLLHCalculator;
  TreeFillerTool* m_pTreeFiller;
  TH3F h_passed_taus;
  TH3F* m_pTruthPt;
  //CellBasedCalculator* m_pIDVarCalculator;
  EFlowRecCalculator* m_pIDVarCalculator;


  //configurables
  std::vector<double> c_llh_binborders;  
  std::string c_pileup_correction_file;
  bool c_pileup_correction_switchOff;

  ClassDef(NtupleCycle,0);
};

#endif // NtupleCycle_H
#endif // SKIM
