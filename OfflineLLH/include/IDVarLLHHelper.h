#ifndef REFAC
#ifndef SKIM
#ifndef IDVarLLHHelper_H
#define IDVarLLHHelper_H

template<class T>
class IDVarLLHHelper{

public:
  //this code will be generated automatically; do not tauch this
  //cgen::start define NTupleFunctors
  static float TrkAvgDist(T const& particle)
  {
    return particle->TrkAvgDist();
  }
  static float CentFrac(T const& particle)
  {
    return particle->CentFrac();
  }
  static float FTrk02(T const& particle)
  {
    return particle->FTrk02();
  }
  static float IpSigLeadTrk(T const& particle)
  {
    return particle->IpSigLeadTrk();
  }
  static float DrMax(T const& particle)
  {
    return particle->DrMax();
  }
  static float MassTrkSys(T const& particle)
  {
    return particle->MassTrkSys();
  }
  static float TrFligthPathSig(T const& particle)
  {
    return particle->TrFligthPathSig();
  }
  static float NTracksdrdR(T const& particle)
  {
    return particle->NTracksdrdR();
  }
  static float CentFrac0102(T const& particle)
  {
    return particle->CentFrac0102();
  }
  static float pi0_n(T const& particle)
  {
    return particle->pi0_n();
  }
  static float pi0_vistau_m(T const& particle)
  {
    return particle->pi0_vistau_m();
  }
  static float ptRatio(T const& particle)
  {
    return particle->ptRatio();
  }

  // new
  static float ChPiEMEOverCaloEME(T const& particle)
  {
    return particle->ChPiEMEOverCaloEME();
  }
  static float EMPOverTrkSysP(T const& particle)
  {
    return particle->EMPOverTrkSysP();
  }

  static float pantauFeature_CellBased_Charged_Ratio_EtOverEtNeutLowB(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_Ratio_EtOverEtNeutLowB();
  }
  static float pantauFeature_CellBased_Neutral_Ratio_EtOverEtAllConsts(T const& particle)
  {
    return particle->pantauFeature_CellBased_Neutral_Ratio_EtOverEtAllConsts();
  }
  static float pantauFeature_CellBased_Charged_DeltaR_MaxToJetAxis_EtSort(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_DeltaR_MaxToJetAxis_EtSort();
  }
  static float pantauFeature_CellBased_Charged_JetMoment_EtDR(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_JetMoment_EtDR();
  }
  static float pantauFeature_CellBased_Basic_NOuterNeutConsts(T const& particle)
  {
    return particle->pantauFeature_CellBased_Basic_NOuterNeutConsts();
  }
  static float pantauFeature_CellBased_Charged_Mean_Et_WrtEtNeutLowB(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_Mean_Et_WrtEtNeutLowB();
  }
  static float pantauFeature_CellBased_Charged_DeltaR_1stTo2nd_EtSort(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_DeltaR_1stTo2nd_EtSort();
  }
  // static float CentFrac(T const& particle){
  //   return particle->pantauCellBasedCentFrac();
  // }
  // static float DRMax(T const& particle){
  //   return particle->pantauCellBasedDRmax();
  // }
  // static float Ftrack(T const& particle){
  //   return particle->pantauCellBasedFtrack();
  // }
  // static float IPSigLeadTrk(T const& particle){
  //   return particle->pantauCellBasedIPSigLeadTrk();
  // }
  // static float MassTrkSys(T const& particle){
  //   return particle->pantauCellBasedMassTrkSys();
  // }
  // static float NwideTracks(T const& particle){
  //   return particle->pantauCellBasedNwideTracks();
  // }
  // static float Pi0N(T const& particle){
  //   return particle->pantauCellBasedPi0N();
  // }
  // static float Pi0vistauM(T const& particle){
  //   return particle->pantauCellBasedPi0VistauM();
  // }
  // static float PtRatio(T const& particle){
  //   return particle->pantauCellBasedPtRatio();
  // }
  // static float RcalCore(T const& particle){
  //   return particle->pantauCellBasedRCalCore();
  // }
  // static float RcalIso(T const& particle){
  //   return particle->pantauCellBasedRCalIso();
  // }
  // static float TrFligthPathSig(T const& particle){
  //   return particle->pantauCellBasedTrFlightPathSig();
  // }
  // static float TrkAvgDist(T const& particle){
  //   return particle->pantauCellBasedTrkAvgDist();
  // }


  static float panTauCellBased_nChrg01(T const& particle){
    return particle->panTauCellBased_nChrg01();
  }
  static float panTauCellBased_nChrg02(T const& particle){
    return particle->panTauCellBased_nChrg02();
  }
  static float panTauCellBased_nChrg0204(T const& particle){
    return particle->panTauCellBased_nChrg0204();
  }
  static float panTauCellBased_nNeut01(T const& particle){
    return particle->panTauCellBased_nNeut01();
  }
  static float panTauCellBased_nNeut02(T const& particle){
    return particle->panTauCellBased_nNeut02();
  }
  static float panTauCellBased_nNeut0204(T const& particle){
    return particle->panTauCellBased_nNeut0204();
  }
  static float panTauCellBased_massChrgSys01(T const& particle){
    return particle->panTauCellBased_massChrgSys01();
  }
  static float panTauCellBased_massChrgSys02(T const& particle){
    return particle->panTauCellBased_massChrgSys02();
  }
  static float panTauCellBased_massChrgSys04(T const& particle){
    return particle->panTauCellBased_massChrgSys04();
  }
  static float panTauCellBased_massNeutSys01(T const& particle){
    return particle->panTauCellBased_massNeutSys01();
  }
  static float panTauCellBased_massNeutSys02(T const& particle){
    return particle->panTauCellBased_massNeutSys02();
  }
  static float panTauCellBased_massNeutSys04(T const& particle){
    return particle->panTauCellBased_massNeutSys04();
  }
  static float panTauCellBased_visTauM01(T const& particle){
    return particle->panTauCellBased_visTauM01();
  }
  static float panTauCellBased_visTauM02(T const& particle){
    return particle->panTauCellBased_visTauM02();
  }
  static float panTauCellBased_visTauM04(T const& particle){
    return particle->panTauCellBased_visTauM04();
  }
  static float panTauCellBased_dRmax02(T const& particle){
    return particle->panTauCellBased_dRmax02();
  }
  static float panTauCellBased_dRmax04(T const& particle){
    return particle->panTauCellBased_dRmax04();
  }
  static float panTauCellBased_ipSigLeadTrk(T const& particle){
    return particle->panTauCellBased_ipSigLeadTrk();
  }
  static float panTauCellBased_trFlightPathSig(T const& particle){
    return particle->panTauCellBased_trFlightPathSig();
  }
  static float panTauCellBased_ptRatio01(T const& particle){
    return particle->panTauCellBased_ptRatio01();
  }
  static float panTauCellBased_ptRatio02(T const& particle){
    return particle->panTauCellBased_ptRatio02();
  }
  static float panTauCellBased_ptRatio04(T const& particle){
    return particle->panTauCellBased_ptRatio04();
  }
  static float panTauCellBased_ptRatioChrg01(T const& particle){
    return particle->panTauCellBased_ptRatioChrg01();
  }
  static float panTauCellBased_ptRatioChrg02(T const& particle){
    return particle->panTauCellBased_ptRatioChrg02();
  }
  static float panTauCellBased_ptRatioChrg04(T const& particle){
    return particle->panTauCellBased_ptRatioChrg04();
  }
  static float panTauCellBased_ptRatioNeut01(T const& particle){
    return particle->panTauCellBased_ptRatioNeut01();
  }
  static float panTauCellBased_ptRatioNeut02(T const& particle){
    return particle->panTauCellBased_ptRatioNeut02();
  }
  static float panTauCellBased_ptRatioNeut04(T const& particle){
    return particle->panTauCellBased_ptRatioNeut04();
  }
  static float panTauCellBased_fLeadChrg01(T const& particle){
    return particle->panTauCellBased_fLeadChrg01();
  }
  static float panTauCellBased_fLeadChrg02(T const& particle){
    return particle->panTauCellBased_fLeadChrg02();
  }
  static float panTauCellBased_fLeadChrg04(T const& particle){
    return particle->panTauCellBased_fLeadChrg04();
  }
  static float panTauCellBased_eFrac0102(T const& particle){
    return particle->panTauCellBased_eFrac0102();
  }
  static float panTauCellBased_eFrac0204(T const& particle){
    return particle->panTauCellBased_eFrac0204();
  }
  static float panTauCellBased_eFrac0104(T const& particle){
    return particle->panTauCellBased_eFrac0104();
  }
  static float panTauCellBased_eFracChrg0204(T const& particle){
    return particle->panTauCellBased_eFracChrg0204();
  }
  static float panTauCellBased_eFracChrg0104(T const& particle){
    return particle->panTauCellBased_eFracChrg0104();
  }
  static float panTauCellBased_eFracNeut0204(T const& particle){
    return particle->panTauCellBased_eFracNeut0204();
  }
  static float panTauCellBased_eFracNeut0104(T const& particle){
    return particle->panTauCellBased_eFracNeut0104();
  }
  static float panTauCellBased_rCal02(T const& particle){
    return particle->panTauCellBased_rCal02();
  }
  static float panTauCellBased_rCal04(T const& particle){
    return particle->panTauCellBased_rCal04();
  }
  static float panTauCellBased_rCalChrg02(T const& particle){
    return particle->panTauCellBased_rCalChrg02();
  }
  static float panTauCellBased_rCalChrg04(T const& particle){
    return particle->panTauCellBased_rCalChrg04();
  }
  static float panTauCellBased_rCalNeut02(T const& particle){
    return particle->panTauCellBased_rCalNeut02();
  }
  static float panTauCellBased_rCalNeut04(T const& particle){
    return particle->panTauCellBased_rCalNeut04();
  }
  static float panTauCellBased_dRminmaxPtChrg04(T const& particle){
    return particle->panTauCellBased_dRminmaxPtChrg04();
  }
 
  // cgen::end
  // variable recalculation has to be put HERE and will not be touched by CodeGenerator
};
#endif
#endif
#endif //REFAC











#ifdef REFAC
#ifndef SKIM
#ifndef IDVarLLHHelper_H
#define IDVarLLHHelper_H

template<class T>
class IDVarLLHHelper{

public:
  static float TrkAvgDist(T const& particle)
  {
    return particle->TrkAvgDist();
  }
  static float CentFrac(T const& particle)
  {
    return particle->CentFrac();
  }
  static float FTrk02(T const& particle)
  {
    return particle->FTrk02();
  }
  static float IpSigLeadTrk(T const& particle)
  {
    return particle->IpSigLeadTrk();
  }
  static float DrMax(T const& particle)
  {
    return particle->DrMax();
  }
  static float MassTrkSys(T const& particle)
  {
    return particle->MassTrkSys();
  }
  static float TrFligthPathSig(T const& particle)
  {
    return particle->TrFligthPathSig();
  }
  static float NTracksdrdR(T const& particle)
  {
    return particle->NTracksdrdR();
  }
  static float CentFrac0102(T const& particle)
  {
    return particle->CentFrac0102();
  }
  static float pi0_n(T const& particle)
  {
    return particle->pi0_n();
  }
  static float pi0_vistau_m(T const& particle)
  {
    return particle->pi0_vistau_m();
  }
  static float ptRatio(T const& particle)
  {
    return particle->ptRatio();
  }

  
};
#endif
#endif
#endif //REFAC
