#ifndef SKIM
#ifndef EventFilterEfficiencyBaseCycle_H
#define EventFilterEfficiencyBaseCycle_H

#include <string>
#include <map>
#include <utility>
#include <memory>
#include <vector>
#include "core/include/SError.h"
#include "core/include/SInputData.h"
#include "CycleBase.h"
#include "BaseObjectCreator.h"
#include "LLHTau.h"
#include "VarHandle.h"
#include "Combination.h"
#include "BDTCalculator.h"
//#include "WeightTool.h"

class EfficiencyBaseCycle : public CycleBase
{
public:
  EfficiencyBaseCycle();

  virtual void AnalyseEvent(const SInputData& id,double dWeight) throw(SError) = 0;
  virtual void FinishCycle() throw(SError){};
  virtual void FinishInputData(const SInputData& id) throw(SError){};
  virtual void FinishMasterInputData(const SInputData& id) throw(SError);
  virtual void StartCycle() throw(SError);
  virtual void StartInputData(const SInputData& id) throw(SError);
  virtual void StartInputFile(const SInputData &) throw(SError);
  virtual void StartMasterInputData(const SInputData& id) throw(SError){};

public:
  // Translation of NTuple variable names to TMVA's TTNT names
  // plus saving their values.
  struct IDVariable {
    typedef D3PDReader::VarHandle<Float_t> VarHandle;

    IDVariable(std::string ttnt_name_, const VarHandle& getter)
      : ttnt_name(move(ttnt_name_))
      , getter(getter)
      , value(0.f)
      , is_logarithmic(IsNameLogarithmic(ttnt_name)) // Don't use ttnt_name_ here!
    {}
    IDVariable(const IDVariable&) = default;
    IDVariable& operator=(const IDVariable&) = delete;
    IDVariable(IDVariable&& rhs) = default;
    IDVariable& operator=(IDVariable&& rhs) = delete;

    inline void Update() {
      value = getter();
      if (is_logarithmic) { value = std::log(value); }
    }
    inline static bool IsNameLogarithmic(const std::string& name) {
      return name.compare(0, 4, "log(")==0 && name.back()==')';
    }
    inline static std::string GetLogarithmContent(const std::string& log_name) {
      return log_name.substr(4, log_name.size()-5);
    }

    const std::string ttnt_name;
    const VarHandle&  getter;
    float             value;
    const bool        is_logarithmic;
  };

protected:
  bool CheckPt(float fWeight, float fPtMin, float fPtMax);
  bool CheckTruthNumTracks(float fWeight);
  bool CheckRecoNumTracks(float fWeight);
  void InitializeBaseVariables();
  void InitializeMVAVariables();
  void DeclareMVA();
  void CalculateScores();
  float BDTScore() const;
  float MLPScore() const;
  D3PDReader::LLHTau m_rLLHTaus;
  void ReadWeights();
  float GetWeight(const float& pt, int nvtx, int prongness);
  /* WeightTool* m_WeightTool; */
  /* std::string c_inputfile_weights; */
  /* bool b_switchOffWeightTool; */

  std::vector<Combination<D3PDReader::LLHTau*>* > m_vCombinations;

  int c_prongmode;

  std::vector<std::string> c_varset1P;
  std::vector<std::string> c_varset3P;

  double m_pt;
  double m_recoPt;
  double m_numTrack;
  double m_mu;

  double m_truthVisEt;
  
  double m_bdtscore;
  double m_mlpscore;
  double m_default_bdtscore;
    
  std::unique_ptr<BDTCalculator> m_pMVACalculator;
  std::string c_bdt_weight_file;
  std::vector<std::string> c_mlp_weight_files;
  std::vector<std::string> m_mlpnames;
  bool c_use_mlp_ensembles;

  std::vector<IDVariable> m_variables;

private:
  bool CheckNumTrack(float fWeight);
  void CopySkimCutflow(const SInputData& id);
  ClassDef(EfficiencyBaseCycle,0);

  // copy skim cutflow
  bool                                       c_copy_skim_cutflow;
  std::string                                c_skim_cut_flow;
  std::string                                c_skim_cut_flow_raw;

};

#endif // EfficiencyBaseCycle_H
#endif //SKIM
