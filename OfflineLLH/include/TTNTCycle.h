#ifndef SKIM
#ifndef TTNTCycle_H
#define TTNTCycle_H

// STL include(s)
#include <string>
#include <set>

#include "AnalysisLLHCommon.h"
#include "IDVarCalculator.h"
#include "PileUpCorrectionTool.h"

class VariablePlotter;

class TTNTCycle : public AnalysisLLHCommon {
public:
  TTNTCycle();
  virtual ~TTNTCycle();

  /// hook in SFrame event loop
  virtual void AnalyseEvent(const SInputData& id, double dWeight) throw (SError);
  virtual void StartInputFile(const SInputData & id) throw (SError);
  virtual void StartInputData(const SInputData & id) throw (SError);
  // void StartCycle() throw (SError);

private:
  bool SelectTrainingObject(D3PDReader::AnalysisTau * const tau );

  void DeclareVariables();
  void CalculateVariables(D3PDReader::AnalysisTau*);

  float m_ftau_calcVars_corrCentFrac;
  float m_ftau_calcVars_corrFTrk;
  float m_ftau_ipSigLeadTrk;
  float m_ftau_massTrkSys;
  float m_ftau_pi0_visTau_m;
  float m_ftau_ptRatio;
  float m_ftau_seedCalo_trkAvgDist;
  float m_ftau_seedCalo_dRmax;
  float m_ftau_trFlightPathSig;
  float m_ftau_m;
  float m_ftau_pt;
  float m_ftau_eta;
  float m_ftau_phi;
  int m_itau_pi0_n;
  int m_itau_seedCalo_wideTrk_n;
  int m_itau_numTrack;
  int m_itau_n;
  int m_itau_nVtx;
  float m_ftau_mu;
  int m_itau_EventNumber;

  float m_fpanTauCellBased_eFrac0102;
  float m_fpanTauCellBased_eFrac0204;
  float m_fpanTauCellBased_eFrac0104;
  float m_fpanTauCellBased_eFracChrg0104;
  float m_fpanTauCellBased_eFracChrg0204;
  float m_fpanTauCellBased_eFracNeut0104;
  float m_fpanTauCellBased_eFracNeut0204;
  float m_fpanTauCellBased_fLeadChrg01;
  float m_fpanTauCellBased_fLeadChrg02;
  float m_fpanTauCellBased_fLeadChrg04;
  float m_fpanTauCellBased_trFlightPathSig;
  float m_fpanTauCellBased_ipSigLeadTrk;
  float m_fpanTauCellBased_massChrgSys01;
  float m_fpanTauCellBased_massChrgSys02;
  float m_fpanTauCellBased_massChrgSys04;
  float m_fpanTauCellBased_massNeutSys01;
  float m_fpanTauCellBased_massNeutSys02;
  float m_fpanTauCellBased_massNeutSys04;
  float m_fpanTauCellBased_rCal02;
  float m_fpanTauCellBased_rCal04;
  float m_fpanTauCellBased_rCalChrg02;
  float m_fpanTauCellBased_rCalChrg04;
  float m_fpanTauCellBased_rCalNeut02;
  float m_fpanTauCellBased_rCalNeut04;
  float m_fpanTauCellBased_dRmax02;
  float m_fpanTauCellBased_dRmax04;
  float m_fpanTauCellBased_nChrg01;
  float m_fpanTauCellBased_nChrg02;
  float m_fpanTauCellBased_nChrg0204;
  float m_fpanTauCellBased_ptRatio01;
  float m_fpanTauCellBased_ptRatio02;
  float m_fpanTauCellBased_ptRatio04;
  float m_fpanTauCellBased_ptRatioChrg01;
  float m_fpanTauCellBased_ptRatioChrg02;
  float m_fpanTauCellBased_ptRatioChrg04;
  float m_fpanTauCellBased_ptRatioNeut01;
  float m_fpanTauCellBased_ptRatioNeut02;
  float m_fpanTauCellBased_ptRatioNeut04;
  float m_fpanTauCellBased_nNeut01;
  float m_fpanTauCellBased_nNeut02;
  float m_fpanTauCellBased_nNeut0204;
  float m_fpanTauCellBased_visTauM01;
  float m_fpanTauCellBased_visTauM02;
  float m_fpanTauCellBased_visTauM04;
  float m_fpanTauCellBased_dRminmaxPtChrg04;
  VariablePlotter* m_pPlotter;
  CellBasedCalculator* m_IDVarCalculator;
  //  EFlowRecCalculator* m_IDVarCalculator;
  IDVarSet m_piCellBasedIDVars;
  PileUpCorrectionTool* m_pileUpCorrectionTool;
  std::string c_pileup_correction_file;
  bool c_pileup_correction_switchOff;
  ClassDef(TTNTCycle, 0);
};

#endif // TTNTCycle_H
#endif
