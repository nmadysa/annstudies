#ifndef WEIGHTTOOL_H
#define WEIGHTTOOL_H

#include <string>
#include "AnalysisTau.h"
#include "TH1F.h"

class WeightTool{
public:
 WeightTool() : m_bSwitchOff(false){}
  ~WeightTool(){}

  void setInputFile(const std::string sInputFileName){ c_inputfile_weights = sInputFileName; }
  void setIsData(bool isData){ m_bIsData = isData; }
  void switchOff(bool bSwitchOff){m_bSwitchOff = bSwitchOff;}
  void ReadWeights();
  float getWeight(const D3PDReader::AnalysisTau* const pTau, const float& mu);
  float getWeight(const int& numTrack, const float& pt, const float& mu);
  
private:
  float GetPtWeight(const D3PDReader::AnalysisTau* const pTau);
  float GetMuWeight(const D3PDReader::AnalysisTau* const pTau, const float& mu);
  float GetPtWeight(const int& numTrack, const float& pt);
  float GetMuWeight(const int& numTrack, const float& pt, const float& mu);
  bool m_bIsData;
  bool m_bSwitchOff;
  std::string c_inputfile_weights;
  TH1F* m_hPtWeights_1p;
  TH1F* m_hPtWeights_3p;
  TH1F* m_hMuWeights_1p;
  TH1F* m_hMuWeights_3p;

};
#endif //WEIGHTTOOL_H
