#ifndef SKIM
#ifndef EvaluationHelpers_H
#define EvaluationHelpers_H

//STL includes
#include <vector>
#include <string>

//ROOT includes
#include "TFile.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TGraph.h"
#include "TGraph2D.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"

class EvaluationHelpers{

 public:
  EvaluationHelpers(){}
  EvaluationHelpers(const std::string& fNSignal);
  EvaluationHelpers(const std::string& fNSignal, const std::string& fNData);
  ~EvaluationHelpers(){}

  void SetProngMode(const int& prongmode = 1);
  void InitializeDetermineCuts();
  void InitializeEfficiencyCalculation();
  std::vector<TGraphErrors*> GetEffVsRej(std::vector<int>* const vPtBins, std::vector<int>* const vNVtxBins);
  std::vector<TGraphErrors*> GetEffVsRej(std::vector<int>* const vPtBins, std::vector<int>* const vNVtxBins,  const std::string& method);
  std::vector<TH1D*> GetScore(std::vector<int>* const vPtBins, std::vector<int>* const vNVtxBins);
  std::vector<TGraphAsymmErrors*> CalculateEfficiency(std::vector<int>* const vPtBins, std::vector<int>* const vNVtxBins, const std::string& method);
  std::vector<TGraphAsymmErrors*> CalculateEfficiencyVsMu(std::vector<int>* const vPtBins, std::vector<int>* const vNVtxBins, const std::string& method);
  std::vector<TGraph*> DetermineLMTCuts(const std::vector<double>& target_efficiencies, std::vector<double> vFlatPtBins);
 // std::vector<TGraph2D*> DetermineLMTCutsMultiDimension(const std::vector<double>& target_efficiencies, std::vector<double> vFlatPtBins);
  
private:
  void CalculateEffVsRej(TH1D* h_signal, TH1D* h_data, std::vector<TGraphErrors*>& vHist, double norm_signal = -1, double norm_data = -1);
  void CalculateEffVsRejCut(TH1D* h_signal, TH1D* h_data, std::vector<TGraphErrors*>& vHist, double norm_signal= -1, double norm_data = -1);
  TGraphAsymmErrors* CalculateEfficiency(const TH1D*, const TH1D*);
  TGraph* GetScoreVsEfficiency(TH1D* score,double ptlow, double pthigh, double mulow, double muhigh);
  std::vector<TGraph*> InitializeTGraphs();
  TH1D* GetProjectionX(TH3F* const hist, const int& xlow = 0, const int& xhigh = -1, const int& zlow =0, const int& zhigh = -1, const std::string& hName = "");
  TH1D* GetProjectionY(TH3F* const hist, const int& xlow = 0, const int& xhigh = -1, const int& zlow =0, const int& zhigh = -1, const std::string& hName = "");
  TH1D* GetProjectionZ(TH3F* const hist, const int& xlow = 0, const int& xhigh = -1, const int& ylow =0, const int& yhigh = -1);
  TH1D* GetProjectionY(TH2F* const hist, const int& xlow = 0, const int& xhigh = -1);

  TFile* fSignal;
  TFile* fData;
  TH1D* h_LLHScore_signal;
  TH1D* h_LLHScore_data;
  TH3F* h_truth_pT_signal; 
  TH3F* h_truth_pT_data; 
  TH3F* h_LLHScore_pT_signal;
  TH3F* h_LLHScore_pT_data;
  TH3F* h_total_inclusive_signal;
  TH3F* h_passed_inclusive_signal;
  TH3F* h_passed_defaultllh_inclusive_signal;
  TH3F* h_passed_defaultbdt_inclusive_signal;
  TH3F* h_passed_defaultcut_inclusive_signal;
  TH1D* h_LLHScore;
  TH3F* h_truth_pT; 
  TH3F* h_LLHScore_pT;
  TH3F* h_total_inclusive;
  TH3F* h_passed_inclusive;
  TH3F* h_passed_defaultllh_inclusive;
  TH3F* h_passed_defaultbdt_inclusive;
  TH3F* h_passed_defaultcut_inclusive;
  TH3F* h_total_inclusive_data;
  TH3F* h_passed_inclusive_data;
  TH3F* h_passed_defaultllh_inclusive_data;
  TH3F* h_passed_defaultbdt_inclusive_data;
  TH3F* h_passed_defaultcut_inclusive_data;
  TH2F* h_TruthPt_OfflinePt;
  int m_nprong_min;
  int m_nprong_max;
};
#endif
#endif // SKIM
