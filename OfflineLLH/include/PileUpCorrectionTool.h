#ifndef PILEUPCORRECTIONTOOL_H
#define PILEUPCORRECTIONTOOL_H

#include <string>
#include "AnalysisTau.h"
#include "TH1F.h"

class PileUpCorrectionTool{
public:
  PileUpCorrectionTool(){}
  ~PileUpCorrectionTool(){}

  void setInputFile(const std::string sInputFileName){ std::cout << sInputFileName << std::endl; m_sFileName = sInputFileName; }
  void switchOff(bool switchOff){ m_bSwitchOff = switchOff; }
  void readInput();
  float getCorrectedVar(const std::string& sVarname,
			const D3PDReader::AnalysisTau* const pTau,
			const int& mu,
			const float& val);
  
private:
  bool m_bSwitchOff;
  std::string m_sFileName;
  TH1F* m_hCorrections;
  float getCorrection(const std::string& sVarname,
		      const D3PDReader::AnalysisTau* const pTau);

};
#endif //PILEUPCORRECTIONTOOL_H
