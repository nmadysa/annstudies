#ifndef SKIM
#ifndef BDTCalculator_H
#define BDTCalculator_H

// ROOT include(s)
#include "TNamed.h"
#include "TString.h"

// TMVA include(s)
#include "TMVA/Reader.h"

class BDTCalculator : public TMVA::Reader
{
public:
  BDTCalculator(const char* name, const TString& theOption="", Bool_t verbose = 0);

  const char* GetName() const {return fName.Data();}
private:
  TString fName;
  
  ClassDef(BDTCalculator, 0);
};

#endif
#endif
