#ifndef NEWRELEASE
#ifndef SKIM
#ifndef IDVarHelper_H
#define IDVarHelper_H

#include <iostream>

#include "LLHTau.h"
// ROOT include(s)
#include "TMath.h"
#include "Constants.h"

template<class T>
class IDVarHelper{

public:
  //this code will be generated automatically; do not tauch this
  //cgen::start define Functors
  static float TrkAvgDist(T const& particle)
  {
    return particle->seedCalo_trkAvgDist();
  }
  static float CentFrac(T const& particle)
  {
    return particle->seedCalo_centFrac();
  }
  static float FTrk02(T const& particle)
  {
    return particle->calcVars_corrFTrk();
  }
  static float IpSigLeadTrk(T const& particle)
  {
    return particle->ipSigLeadTrk();
  }
  static float DrMax(T const& particle)
  {
    return particle->seedCalo_dRmax();
  }
  static float MassTrkSys(T const& particle)
  {
    return particle->massTrkSys();
  }
  static float TrFligthPathSig(T const& particle)
  {
    return particle->trFlightPathSig();
  }
  static float NTracksdrdR(T const& particle)
  {
    return particle->seedCalo_wideTrk_n();
  }
  static float CentFrac0102(T const& particle)
  {
    return particle->calcVars_corrCentFrac();
  }
#ifdef OBSOLTE
  static float LLHScore(T const& particle)
  {
    return particle->DefaultLLHScore();
  }
  static float BDTScore(T const& particle)
  {
    return particle->BDTScore();
  }
#endif
  static float ptRatio(T const& particle)
  {
    return particle->ptRatio();
  }
  static float pi0_n(T const& particle)
  {
    return particle->pi0_n();
  }
  static float pi0_vistau_m(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->pi0_vistau_m()  / GeV;
  }

  // new
  static float ChPiEMEOverCaloEME(T const& particle)
  {
    return particle->calcVars_ChPiEMEOverCaloEME();
  }
  static float EMPOverTrkSysP(T const& particle)
  {
    return particle->calcVars_EMPOverTrkSysP();
  }
  static float pantauFeature_CellBased_Charged_Ratio_EtOverEtNeutLowB(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_Ratio_EtOverEtNeutLowB();
  }
  static float pantauFeature_CellBased_Neutral_Ratio_EtOverEtAllConsts(T const& particle)
  {
    return particle->pantauFeature_CellBased_Neutral_Ratio_EtOverEtAllConsts();
  }
  static float pantauFeature_CellBased_Charged_DeltaR_MaxToJetAxis_EtSort(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_DeltaR_MaxToJetAxis_EtSort();
  }
  static float pantauFeature_CellBased_Charged_JetMoment_EtDR(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_JetMoment_EtDR();
  }
  static float pantauFeature_CellBased_Basic_NOuterNeutConsts(T const& particle)
  {
    return particle->pantauFeature_CellBased_Basic_NOuterNeutConsts();
  }
  static float pantauFeature_CellBased_Charged_Mean_Et_WrtEtNeutLowB(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_Mean_Et_WrtEtNeutLowB();
  }
  static float pantauFeature_CellBased_Charged_DeltaR_1stTo2nd_EtSort(T const& particle)
  {
    return particle->pantauFeature_CellBased_Charged_DeltaR_1stTo2nd_EtSort();
  }

  // substructure id
  static float panTauCellBased_nChrg0204(T const& particle)
  {
    return particle->getIDVarSet().nChrg0204;
  }
  static float panTauCellBased_nChrg01(T const& particle)
  {
    return particle->getIDVarSet().nChrg01;
  }
  static float panTauCellBased_nChrg02(T const& particle)
  {
    return particle->getIDVarSet().nChrg02;
  }
  static float panTauCellBased_nChrg04(T const& particle)
  {
    return particle->getIDVarSet().nChrg04;
  }
  static float panTauCellBased_nNeut0204(T const& particle)
  {
    return particle->getIDVarSet().nNeut0204;
  }
  static float panTauCellBased_nNeut01(T const& particle)
  {
    return particle->getIDVarSet().nNeut01;
  }
  static float panTauCellBased_nNeut02(T const& particle)
  {
    return particle->getIDVarSet().nNeut02;
  }
  static float panTauCellBased_nNeut04(T const& particle)
  {
    return particle->getIDVarSet().nNeut04;
  }
  static float panTauCellBased_massChrgSys01(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->getIDVarSet().massChrgSys01  / GeV;
  }
  static float panTauCellBased_massChrgSys02(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->getIDVarSet().massChrgSys02 / GeV;
  }
  static float panTauCellBased_massChrgSys04(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->getIDVarSet().massChrgSys04 / GeV;
  }
  static float panTauCellBased_massNeutSys01(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->getIDVarSet().massNeutSys01 / GeV;
  }
  static float panTauCellBased_massNeutSys02(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->getIDVarSet().massNeutSys02 / GeV;
  }
  static float panTauCellBased_massNeutSys04(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->getIDVarSet().massNeutSys04 / GeV;
  }
  static float panTauCellBased_visTauM01(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->getIDVarSet().visTauM01 / GeV;
  }
  static float panTauCellBased_visTauM02(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->getIDVarSet().visTauM02 / GeV;
  }
  static float panTauCellBased_visTauM04(T const& particle)
  {
    using AnalysisUtilities_Constants::GeV;
    return particle->getIDVarSet().visTauM04 / GeV;
  }
  static float panTauCellBased_dRmax01(T const& particle)
  {
    return particle->getIDVarSet().dRmax01;
  }
  static float panTauCellBased_dRmax02(T const& particle)
  {
    return particle->getIDVarSet().dRmax02;
  }
  static float panTauCellBased_dRmax04(T const& particle)
  {
    return particle->getIDVarSet().dRmax04;
  }
  static float panTauCellBased_ipSigLeadTrk(T const& particle)
  {
    return particle->getIDVarSet().ipSigLeadTrk;
  }
  static float panTauCellBased_trFlightPathSig(T const& particle)
  {
    return particle->getIDVarSet().trFlightPathSig;
  }
  static float panTauCellBased_ptRatio01(T const& particle)
  {
    return particle->getIDVarSet().ptRatio01;
  }
  static float panTauCellBased_ptRatio02(T const& particle)
  {
    return particle->getIDVarSet().ptRatio02;
  }
  static float panTauCellBased_ptRatio04(T const& particle)
  {
    return particle->getIDVarSet().ptRatio04;
  }
  static float panTauCellBased_ptRatioNeut01(T const& particle)
  {
    return particle->getIDVarSet().ptRatioNeut01;
  }
  static float panTauCellBased_ptRatioNeut02(T const& particle)
  {
    return particle->getIDVarSet().ptRatioNeut02;
  }
  static float panTauCellBased_ptRatioNeut04(T const& particle)
  {
    return particle->getIDVarSet().ptRatioNeut04;
  }
  static float panTauCellBased_ptRatioChrg01(T const& particle)
  {
    return particle->getIDVarSet().ptRatioChrg01;
  }
  static float panTauCellBased_ptRatioChrg02(T const& particle)
  {
    return particle->getIDVarSet().ptRatioChrg02;
  }
  static float panTauCellBased_ptRatioChrg04(T const& particle)
  {
    return particle->getIDVarSet().ptRatioChrg04;
  }
  static float panTauCellBased_fLeadChrg01(T const& particle)
  {
    return particle->getIDVarSet().fLeadChrg01;
  }
  static float panTauCellBased_fLeadChrg02(T const& particle)
  {
    return particle->getIDVarSet().fLeadChrg02;
  }
  static float panTauCellBased_fLeadChrg04(T const& particle)
  {
    return particle->getIDVarSet().fLeadChrg04;
  }
  static float panTauCellBased_eFrac0102(T const& particle)
  {
    return particle->getIDVarSet().eFrac0102;
  }
  static float panTauCellBased_eFrac0204(T const& particle)
  {
    return particle->getIDVarSet().eFrac0204;
  }
  static float panTauCellBased_eFrac0104(T const& particle)
  {
    return particle->getIDVarSet().eFrac0104;
  }
  static float panTauCellBased_eFracChrg0102(T const& particle)
  {
    return particle->getIDVarSet().eFracChrg0102;
  }
  static float panTauCellBased_eFracChrg0204(T const& particle)
  {
    return particle->getIDVarSet().eFracChrg0204;
  }
  static float panTauCellBased_eFracChrg0104(T const& particle)
  {
    return particle->getIDVarSet().eFracChrg0104;
  }
  static float panTauCellBased_eFracNeut0102(T const& particle)
  {
    return particle->getIDVarSet().eFracNeut0102;
  }
  static float panTauCellBased_eFracNeut0204(T const& particle)
  {
    return particle->getIDVarSet().eFracNeut0204;
  }
  static float panTauCellBased_eFracNeut0104(T const& particle)
  {
    return particle->getIDVarSet().eFracNeut0104;
  }
  static float panTauCellBased_eFrac010201(T const& particle)
  {
    return particle->getIDVarSet().eFrac010201;
  }
  static float panTauCellBased_eFrac010202(T const& particle)
  {
    return particle->getIDVarSet().eFrac010202;
  }
  static float panTauCellBased_eFrac010204(T const& particle)
  {
    return particle->getIDVarSet().eFrac010204;
  }
  static float panTauCellBased_eFracChrg010201(T const& particle)
  {
    return particle->getIDVarSet().eFracChrg010201;
  }
  static float panTauCellBased_eFracChrg010202(T const& particle)
  {
    return particle->getIDVarSet().eFracChrg010202;
  }
  static float panTauCellBased_eFracChrg010204(T const& particle)
  {
    return particle->getIDVarSet().eFracChrg010204;
  }
  static float panTauCellBased_eFracNeut010201(T const& particle)
  {
    return particle->getIDVarSet().eFracNeut010201;
  }
  static float panTauCellBased_eFracNeut010202(T const& particle)
  {
    return particle->getIDVarSet().eFracNeut010202;
  }
  static float panTauCellBased_eFracNeut010204(T const& particle)
  {
    return particle->getIDVarSet().eFracNeut010204;
  }
  static float panTauCellBased_eFrac020401(T const& particle)
  {
    return particle->getIDVarSet().eFrac020401;
  }
  static float panTauCellBased_eFrac020402(T const& particle)
  {
    return particle->getIDVarSet().eFrac020402;
  }
  static float panTauCellBased_eFrac020404(T const& particle)
  {
    return particle->getIDVarSet().eFrac020404;
  }
  static float panTauCellBased_eFracChrg020401(T const& particle)
  {
    return particle->getIDVarSet().eFracChrg020401;
  }
  static float panTauCellBased_eFracChrg020402(T const& particle)
  {
    return particle->getIDVarSet().eFracChrg020402;
  }
  static float panTauCellBased_eFracChrg020404(T const& particle)
  {
    return particle->getIDVarSet().eFracChrg020404;
  }
  static float panTauCellBased_eFracNeut020401(T const& particle)
  {
    return particle->getIDVarSet().eFracNeut020401;
  }
  static float panTauCellBased_eFracNeut020402(T const& particle)
  {
    return particle->getIDVarSet().eFracNeut020402;
  }
  static float panTauCellBased_eFracNeut020404(T const& particle)
  {
    return particle->getIDVarSet().eFracNeut020404;
  }
  static float panTauCellBased_rCal01(T const& particle)
  {
    return particle->getIDVarSet().rCal01;
  }
  static float panTauCellBased_rCal02(T const& particle)
  {
    return particle->getIDVarSet().rCal02;
  }
  static float panTauCellBased_rCal04(T const& particle)
  {
    return particle->getIDVarSet().rCal04;
  }
  static float panTauCellBased_rCalChrg01(T const& particle)
  {
    return particle->getIDVarSet().rCalChrg01;
  }
  static float panTauCellBased_rCalChrg02(T const& particle)
  {
    return particle->getIDVarSet().rCalChrg02;
  }
  static float panTauCellBased_rCalChrg04(T const& particle)
  {
    return particle->getIDVarSet().rCalChrg04;
  }
  static float panTauCellBased_rCalNeut01(T const& particle)
  {
    return particle->getIDVarSet().rCalNeut01;
  }
  static float panTauCellBased_rCalNeut02(T const& particle)
  {
    return particle->getIDVarSet().rCalNeut02;
  }
  static float panTauCellBased_rCalNeut04(T const& particle)
  {
    return particle->getIDVarSet().rCalNeut04;
  }
  static float panTauCellBased_dRminmaxPtChrg04(T const& particle)
  {
    return particle->getIDVarSet().dRminmaxPtChrg04;
  }

//cgen::end
//variable recalculation has to be put HERE and will not be touched by CodeGenerator
};
#endif
#endif // SKIM
#endif
