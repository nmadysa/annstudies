#ifndef SKIM
#ifndef VariablePlotter_H
#define VariablePlotter_H

// STL
#include <vector>
#include <string>
// SFrame include
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// custom header
#include "ToolBase.h"
#include "AnalysisTau.h"
#include "TruthTau.h"
#include "AnalysisEventInfo.h"
#include "TauIDCuts.h"
#include "IDVarCalculator.h"
#include "WeightTool.h"
#include "PileUpCorrectionTool.h"

#ifndef __CINT__
#include "boost/function.hpp"
#endif // __CINT__

class VariablePlotter : public ToolBase
{
public:
/// constructor specifying the parent and the name of the tool
  VariablePlotter(CycleBase* pParent,const char* sName);
  
  virtual ~VariablePlotter(){}

  void BeginInputData(const SInputData& id) throw(SError);
    
  //plot the variable distributions needed
  void PlotDistributions(std::vector<D3PDReader::AnalysisTau*>& vTaus,
			 const unsigned int& iNVtx,
			 float fWeight = 1);
  void PlotDistribution(const D3PDReader::AnalysisTau* const tau,
			const unsigned int& iNVtx);
  void PlotDistributions(const std::vector<D3PDReader::TruthTau*>& vTaus,
  			 const unsigned int& iNVtx);
  void PlotPerformance(const std::vector<D3PDReader::AnalysisTau*>& vTaus,
  		       const unsigned int& iNVtx);
  void PlotEventBasedVariables(const D3PDReader::AnalysisEventInfo& rEvent,
			       float fWeight = 1);
  void PlotsForReweighting(const std::vector<D3PDReader::AnalysisTau*>& vTaus,
			   const unsigned int& iNVtx);
  
private:
#ifndef __CINT__
  typedef boost::function<bool (D3PDReader::Tau * const)> boolFct;
  std::map<std::string, boolFct> m_mid;
#endif
  std::string c_inputfile_weights;
  TauLLHCut* m_pLLHCut;
  TauBDTCut* m_pBDTCut;
  //  CellBasedCalculator* m_IDVarCalculator;
  EFlowRecCalculator* m_IDVarCalculator;
  IDVarSet m_piCellBasedIDVars;
  float m_fpanTauCellBased_eFrac0102;
  float m_fpanTauCellBased_eFrac0204;
  float m_fpanTauCellBased_eFrac0104;
  float m_fpanTauCellBased_eFracChrg0104;
  float m_fpanTauCellBased_eFracChrg0204;
  float m_fpanTauCellBased_eFracNeut0104;
  float m_fpanTauCellBased_eFracNeut0204;
  float m_fpanTauCellBased_fLeadChrg01;
  float m_fpanTauCellBased_fLeadChrg02;
  float m_fpanTauCellBased_fLeadChrg04;
  float m_fpanTauCellBased_trFlightPathSig;
  float m_fpanTauCellBased_ipSigLeadTrk;
  float m_fpanTauCellBased_massChrgSys01;
  float m_fpanTauCellBased_massChrgSys02;
  float m_fpanTauCellBased_massChrgSys04;
  float m_fpanTauCellBased_massNeutSys01;
  float m_fpanTauCellBased_massNeutSys02;
  float m_fpanTauCellBased_massNeutSys04;
  float m_fpanTauCellBased_rCal02;
  float m_fpanTauCellBased_rCal04;
  float m_fpanTauCellBased_rCalChrg02;
  float m_fpanTauCellBased_rCalChrg04;
  float m_fpanTauCellBased_rCalNeut02;
  float m_fpanTauCellBased_rCalNeut04;
  float m_fpanTauCellBased_dRmax02;
  float m_fpanTauCellBased_dRmax04;
  float m_fpanTauCellBased_nChrg01;
  float m_fpanTauCellBased_nChrg02;
  float m_fpanTauCellBased_nChrg0204;
  float m_fpanTauCellBased_ptRatio01;
  float m_fpanTauCellBased_ptRatio02;
  float m_fpanTauCellBased_ptRatio04;
  float m_fpanTauCellBased_ptRatioChrg01;
  float m_fpanTauCellBased_ptRatioChrg02;
  float m_fpanTauCellBased_ptRatioChrg04;
  float m_fpanTauCellBased_ptRatioNeut01;
  float m_fpanTauCellBased_ptRatioNeut02;
  float m_fpanTauCellBased_ptRatioNeut04;
  float m_fpanTauCellBased_nNeut01;
  float m_fpanTauCellBased_nNeut02;
  float m_fpanTauCellBased_nNeut0204;
  float m_fpanTauCellBased_visTauM01;
  float m_fpanTauCellBased_visTauM02;
  float m_fpanTauCellBased_visTauM04;
  float m_fpanTauCellBased_dRminmaxPtChrg04;
  WeightTool* m_WeightTool;  
  bool b_switchOffWeightTool;
  PileUpCorrectionTool* m_pileUpCorrectionTool;
  std::string c_pileup_correction_file;
  bool c_pileup_correction_switchOff;
  ClassDef(VariablePlotter, 0);
};

#endif // VariablePlotter_H
#endif // SKIM
