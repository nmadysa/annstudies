#ifndef SKIM
#ifndef EventFilterEfficiencyEvaluationCycle_H
#define EventFilterEfficiencyEvaluationCycle_H

#include <map>
#include "TFile.h"
#include "TGraph.h"
#include "TH2F.h"
#include "EfficiencyBaseCycle.h"
//#include "WeightTool.h"

class EfficiencyEvaluationCycle : public EfficiencyBaseCycle
{
public:
  EfficiencyEvaluationCycle();
  virtual void AnalyseEvent(const SInputData& id,double dWeight) throw(SError);
  virtual void StartCycle() throw(SError);
  virtual void StartInputData(const SInputData& id) throw(SError);

private:
  void FillScores();
  void FillPassed();
  void FillPassedAllCuts();
  void FillTotal();
  void FillPassed(const std::string& sWorkingPoint);
  void FillPassedAllCuts(const std::string& algo, const float& efficiency);
  double CalculateCut(TGraph* cuts);
  void ReadCuts();
  void ReadAllCuts();
  void ReadWeights();
  float GetPtWeight(float pt);
  float GetMuWeight(float mu);
  void CalculateVariables();
  void DeclareVariables();
  
  float m_fillPt;
  std::string c_inputfile_bdt_cuts;
  std::string c_inputfile_mlp_cuts;
  std::string c_inputfile_bdt_reccuts;
  std::string c_inputfile_bdt_allcuts;
  std::string c_inputfile_mlp_allcuts;
  std::string c_inputfile_bdt_alldefcuts;
  std::string c_inputfile_weights;
  std::map<std::string, float> efficiencies;
  std::map<std::string, TGraph*> m_mCutsBDT;
  std::map<std::string, TGraph*> m_mCutsMLP;
  std::map<std::string, TGraph*> m_mCutsDefaultBDT;

  TFile* f_mlp;
  TFile* f_bdt;
  TFile* f_bdt_all;
  TFile* f_mlp_all;
  TFile* f_defbdt_all;
  TFile* f_bdt_rec;

  TGraph* tg_loose_bdt_cuts;
  TGraph* tg_medium_bdt_cuts;
  TGraph* tg_tight_bdt_cuts;  

  TGraph* tg_loose_mlp_cuts;
  TGraph* tg_medium_mlp_cuts;
  TGraph* tg_tight_mlp_cuts;  

  TGraph* tg_loose_bdt_reccuts;
  TGraph* tg_medium_bdt_reccuts;
  TGraph* tg_tight_bdt_reccuts;

  //  bool b_switchOffWeightTool;    
  //  WeightTool* m_WeightTool;
  
  TH1F* m_hPtWeights_1p;
  TH1F* m_hPtWeights_3p;
  TH1F* m_hMuWeights_1p;
  TH1F* m_hMuWeights_3p;

  ClassDef(EfficiencyEvaluationCycle,0);
};

#endif // EfficiencyEvaluationCycle_H
#endif //SKIM
