#ifndef SKIM
#ifndef IDVarHandle_H
#define IDVarHandle_H

//STL includes
#include <map>
#include <string>
#include <assert.h>

//boost includes
#ifndef __CINT__
#include "boost/function.hpp"
#include "boost/bind.hpp"
#endif

//custom includes
#include "IDVarHelper.h"

#ifndef __CINT__
template<typename P> 
class IDVarHandle
{
  typedef boost::function<float (const P)> TVALUE;
public:
  //template<typename P>
  static IDVarHandle<P>* GetInstance()
  { 
    if(m_instance==0)
      m_instance = new IDVarHandle();
    return m_instance;
  }
  //default destructor
  virtual ~IDVarHandle(){}
  void AddVariable(std::string varname, TVALUE rFunctor){
    m_rContainer[varname] = rFunctor;
  }
  //template <typename P>
  float Retrieve(std::string varname, P* particle){
    TVALUE m_rFunctor = boost::bind(&IDVarHandle::RetrieveFunctor,this,_1)(varname);
    return m_rFunctor(particle);
  }
  
  TVALUE& RetrieveFunctor(const std::string& varname){
    typename std::map<std::string,TVALUE>::iterator it = m_rContainer.find(varname);
    //Very hard cut, maybe need find a way to return KeyError Functor, printing and setting default value
    assert(it != m_rContainer.end());
    return it->second;
  }

protected:
  IDVarHandle(){}

private:
  //standard constructor
  static IDVarHandle* m_instance;
  std::map<std::string, TVALUE> m_rContainer;
};

template<typename P>
IDVarHandle<P>* IDVarHandle<P>::m_instance=0;

#endif //__CINT__

#endif //IDVarHandle
#endif //SKIM
