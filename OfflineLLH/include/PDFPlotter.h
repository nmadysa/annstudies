#ifndef SKIM
#ifndef PDFPlotter_H
#define PDFPlotter_H

// STL
#include <vector>

// SFrame include
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// custom header
#include "ToolBase.h"
#include "AnalysisTau.h"
#include "IDVarCalculator.h"
#include "WeightTool.h"
#include "PileUpCorrectionTool.h"

// IDVarHelper
#include "IDVarHelper.h"

//#include "boost/lexical_cast.hpp"

class PDFPlotter : public ToolBase
{
 public:
  /// constructor specifying the parent and the name of the tool
  PDFPlotter(CycleBase* pParent,const char* sName);
  
  /// default destructor
  //  virtual ~PDFPlotter(){}

  /// book histograms for cutflow
  void BeginInputData(const SInputData& id) throw(SError);
   
  //plot the variable distributions needed
  void PlotDistributions(std::vector<D3PDReader::AnalysisTau*>& vTaus, int nvtx = 1, float fWeight = 1);

private:
  void createArrayFromRange(double* array, const double& start, const double& end, const unsigned int& step);

  void ReadWeights();
  float GetWeight(const float& pt, int nvtx, int prongness);
  
  //  CellBasedCalculator* m_IDVarCalculator;
  EFlowRecCalculator* m_IDVarCalculator;
  IDVarSet m_piCellBasedIDVars;
  WeightTool* m_WeightTool;
  std::string c_inputfile_weights;
  bool b_switchOffWeightTool;
  PileUpCorrectionTool* m_pileUpCorrectionTool;
  std::string c_pileup_correction_file;
  bool c_pileup_correction_switchOff;
};

#endif // PDFPlotter_H
#endif // SKIM
