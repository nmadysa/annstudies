#ifndef SKIM
#ifndef EfficiencyOptimisationCycle_H
#define EfficiencyOptimisationCycle_H

#include "EfficiencyBaseCycle.h"
#include "TH2F.h"

class EfficiencyOptimisationCycle : public EfficiencyBaseCycle
{
public:
  EfficiencyOptimisationCycle();

  /// hook in SFrame event loop
  virtual void AnalyseEvent(const SInputData& id,double dWeight) throw(SError);
  virtual void StartCycle() throw(SError);
  virtual void StartInputData(const SInputData& id) throw(SError);

private:
  void FillScores();
  void HistsForEfficiency();

  TH2F* m_pTruthVsRecoPt;

  // Unneeded, commented out.
  // ClassDef(EfficiencyOptimisationCycle,0);
};

#endif // EfficiencyOptimisationCycle_H
#endif //SKIM
