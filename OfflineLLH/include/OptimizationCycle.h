#ifdef DEV
#ifndef SKIM
#ifndef EventFilterOptimizationCycle_H
#define EventFilterOptimizationCycle_H

// STL include(s)
#include <string>

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

//root include(s)
#include "TFile.h"
#include "TGraph.h"

// core include(s)
#include "CycleBase.h"
#include "BaseObjectCreator.h"

// TauCommon include(s)
#include "LLHTau.h"

// LLHTool includes
#include "Combination.h"

class OptimizationCycle : public CycleBase
{
public:
  OptimizationCycle();
  //  virtual ~OptimizationCycle(){};

  /// hook in SFrame event loop
  virtual void AnalyseEvent(const SInputData& id,double dWeight) throw(SError);
  virtual void FinishCycle() throw(SError){};
  virtual void FinishInputData(const SInputData& id) throw(SError){};
  virtual void FinishMasterInputData(const SInputData& id) throw(SError);
  virtual void StartCycle() throw(SError);
  virtual void StartInputData(const SInputData& id) throw(SError);
  virtual void StartInputFile(const SInputData &) throw(SError);
  virtual void StartMasterInputData(const SInputData& id) throw(SError){};

private:
  /// private methods defining the selection steps
  bool CheckPt(float& fWeight);
  bool CheckTruthNumTracks(float& fWeight);
  bool CheckRecoNumTracks(float& fWeight);
  bool CheckNumTracks(float& fWeight);
  bool IsInBin(double, double);
  /// other private methods
  void InitializeBaseVariables();
  void FillScores(const double& llhscore);
  void FillPassed(const double& llhscore);
  void FillTotal();
  double CalculateCut(TGraph* cuts);
  /// objects responsible for reading the D3PD tree
  D3PDReader::LLHTau m_rLLHTaus;

  std::vector<Combination<D3PDReader::LLHTau*>* > m_vCombinations;

  //cycle properties
  int c_prongmode;
  int c_runmode;
  std::vector<std::string> c_varset1P;
  std::vector<std::string> c_varset3P;

  //selection cuts
  double c_pt_cut;
  double m_pt;
  double m_numTrack;
  
  std::string c_inputfile_cuts;
  TFile* f;
  TGraph* tg_loose_cuts;
  TGraph* tg_medium_cuts;
  TGraph* tg_tight_cuts;
  
  double m_mu_correction_factor;
  double m_mu_correction_factor_1P;
  double m_mu_correction_factor_3P;

  int m_numScore;
  int m_numWrongScore;
  ClassDef(OptimizationCycle,0);
};

#endif // OptimizationCycle_H
#endif //SKIM
#endif






#ifndef DEV
#ifndef SKIM
#ifndef EventFilterOptimizationCycle_H
#define EventFilterOptimizationCycle_H

// STL include(s)
#include <string>

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

//root include(s)
#include "TFile.h"
#include "TGraph.h"

// core include(s)
#include "CycleBase.h"
#include "BaseObjectCreator.h"

// TauCommon include(s)
#include "LLHTau.h"

// LLHTool includes
#include "Combination.h"

class OptimizationCycle : public CycleBase
{
public:
  OptimizationCycle();
  //  virtual ~OptimizationCycle(){};

  /// hook in SFrame event loop
  virtual void   AnalyseEvent(const SInputData& id,double dWeight) throw(SError);
  virtual void   FinishCycle() throw(SError){};
  virtual void   FinishInputData(const SInputData& id) throw(SError){};
  virtual void   FinishMasterInputData(const SInputData& id) throw(SError);
  virtual void   StartCycle() throw(SError);
  virtual void   StartInputData(const SInputData& id) throw(SError);
  virtual void   StartInputFile(const SInputData &) throw(SError);
  virtual void   StartMasterInputData(const SInputData& id) throw(SError){};

private:
  /// private methods defining the selection steps
  bool CheckPt(float& fWeight);
  bool CheckTruthNumTracks(float& fWeight);
  bool CheckRecoNumTracks(float& fWeight);
  bool CheckNumTracks(float& fWeight);
  bool IsInBin(double, double);
  /// other private methods
  void InitializeBaseVariables();
  void FillScores(const double& llhscore);
  void FillPassed(const double& llhscore);
  void FillTotal();
  double CalculateCut(TGraph* cuts);
  /// objects responsible for reading the D3PD tree
  D3PDReader::LLHTau                                      m_rLLHTaus;

  std::vector<Combination<D3PDReader::LLHTau*>* >         m_vCombinations;

  //cycle properties
  int                                     c_prongmode;
  int                                     c_runmode;
  std::vector<std::string>                c_varset1P;
  std::vector<std::string>                c_varset3P;

  //selection cuts
  double                                  c_pt_cut;
  double                                  m_pt;
  double                                  m_numTrack;
  
  std::string                             c_inputfile_cuts;
  TFile*                                  f;
  TGraph*                                 tg_loose_cuts;
  TGraph*                                 tg_medium_cuts;
  TGraph*                                 tg_tight_cuts;
  
  double                                  m_mu_correction_factor;
  double                                  m_mu_correction_factor_1P;
  double                                  m_mu_correction_factor_3P;

  int 									  m_numScore;
  int 									  m_numWrongScore;
  ClassDef(OptimizationCycle,0);
};

#endif // OptimizationCycle_H
#endif //SKIM
#endif
