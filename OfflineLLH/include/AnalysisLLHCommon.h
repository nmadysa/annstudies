#ifndef SKIM
#ifndef AnalysisLLHCommon_H
#define AnalysisLLHCommon_H

// STL include(s)
#include <string>
#include "TH1.h"
#include "TFile.h"

// SFrame include(s)
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// core include(s)
#include "CycleBase.h"
#include "BaseObjectCreator.h"

#include "AnalysisTau.h"
#include "AnalysisTauD3PDCollection.h"
#include "AnalysisEventInfo.h"
#include "TruthTau.h"
#include "TruthTauD3PDCollection.h"

class AnalysisLLHCommon : public CycleBase
{
public:
  /// standard constructor
  AnalysisLLHCommon();
  /// standard destructor
  virtual ~AnalysisLLHCommon() {};

  /// hook in SFrame event loop
  virtual void   AnalyseEvent(const SInputData& id,double dWeight) throw(SError) = 0;
  virtual void   StartInputFile(const SInputData &) throw(SError) = 0;

protected:
  bool CheckNTaus(float fWeight, int iMin);
  bool SelectPhysicsObjects(float& fWeight);
  void TightTruthMatch(std::vector<D3PDReader::AnalysisTau*>& vTaus);
  void LooserTruthMatch(std::vector<D3PDReader::AnalysisTau*>& vTaus);
  void TightRecoNumTrack(std::vector<D3PDReader::AnalysisTau*>& vTaus);
  void LooserRecoNumTrack(std::vector<D3PDReader::AnalysisTau*>& vTaus);
  bool TightTruthMatch(D3PDReader::AnalysisTau* const pTau);
  bool TightRecoNumTrack(D3PDReader::AnalysisTau* const pTau);
  void CopySkimCutflow(const SInputData& id);
  /// objects responsible for reading the D3PD tree
  D3PDReader::AnalysisEventInfo m_rEventInfo;
  D3PDReader::AnalysisTauD3PDCollection m_rTauCollection;
  D3PDReader::TruthTauD3PDCollection m_rTruthTauCollection;

  /// object creators
  BaseObjectCreator<D3PDReader::AnalysisTau> m_oTauCreator;
  BaseObjectCreator<D3PDReader::TruthTau> m_oTruthTauCreator;
  /// analysis objects
  std::vector<D3PDReader::AnalysisTau*> m_vTaus;
  std::vector<D3PDReader::TruthTau*> m_vTruthTaus;

  // copy skim cutflow
  bool                                       c_copy_skim_cutflow;
  std::string                                c_skim_cut_flow;
  std::string                                c_skim_cut_flow_raw;


private:
  bool LooserTruthMatch(D3PDReader::AnalysisTau* const pTau);
  bool LooserRecoNumTrack(D3PDReader::AnalysisTau* const pTau);

  ClassDef(AnalysisLLHCommon,0);
};

#endif // AnalysisLLHCommon_H
#endif // SKIM
