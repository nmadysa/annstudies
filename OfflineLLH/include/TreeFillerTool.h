#ifndef SKIM
#ifndef TreeFillerTool_H
#define TreeFillerTool_H

// STL
#include <vector>
#include <map>

// SFrame include
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// custom header
#include "AnalysisEventInfo.h"
#include "ToolBase.h"
#include "AnalysisTau.h"
#include "TruthTau.h"
#include "Variable.h"
#include "MCTruthParticle.h"
#include "IDVarCalculator.h"
#include "PileUpCorrectionTool.h"

class TreeFillerTool : public ToolBase
{
public:
  /// constructor specifying the parent and the name of the tool
  TreeFillerTool(CycleBase* pParent,const char* sName);
  
  /// default destructor
  ~TreeFillerTool(){}

  //plot the variable distributions needed
  void Initialize(std::map<std::string,
		  Variable<D3PDReader::AnalysisTau*>* > map);
  void Fill(D3PDReader::AnalysisTau* pTau,
	    std::map<std::string, Variable<D3PDReader::AnalysisTau*>* > map,
	    const D3PDReader::AnalysisEventInfo& rEventInfo);

private:
  int m_runNum;
  int m_evtNum;
  double m_pt;
  double m_et;
  double m_eta;
  double m_phi;
  double m_default_llhscore;
  double m_BDTJet_Score;
  int m_numTrack;
  int m_default_llh_loose;
  int m_default_llh_medium;
  int m_default_llh_tight;
  int m_default_bdt_loose;
  int m_default_bdt_medium;
  int m_default_bdt_tight;
  int m_nvertices;
  int m_numGoodVertices;
  int m_mu;
  double m_pi0BDTPrimaryScore;
  double m_pi0BDTSecondaryScore;
  double m_truth_pt;
  double m_truth_eta;
  double m_truth_phi;
  int m_truth_numTrack;
  double m_matchVisEt;
  
  std::map<std::string, float> m_map_llhScore;
  std::map<std::string, float> m_map_varValue;

  CellBasedCalculator* m_pIDVarCalculator;
  float m_fpantauCellBasedCentFrac;
  float m_fpantauCellBasedTrkAvgDist;
  float m_fpantauCellBasedFtrack;
  float m_fpantauCellBasedNwideTrk;
  float m_fpantauCellBasedMassTrkSys;
  float m_fpantauCellBasedDRmax;
  float m_fpantauCellBasedPi0N;
  float m_fpantauCellBasedPi0VistauM;
  float m_fpantauCellBasedPtRatio;
  float m_fpantauCellBasedIpSigLeadTrk;
  float m_fpantauCellBasedTrkFlightPathSig;

  PileUpCorrectionTool* m_pileUpCorrectionTool;
  std::string c_pileup_correction_file;
  bool c_pileup_correction_switchOff;


};

#endif // TreeFillerTool_H
#endif // SKIM
