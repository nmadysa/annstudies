#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;

#pragma link C++ class LLHCommon+;
#pragma link C++ class TNTCycle+;
#ifndef SKIM
#pragma link C++ class AnalysisLLHCommon+;
#pragma link C++ class LLHCycle+;
#pragma link C++ class VariablePlotter+;
#pragma link C++ class PDFCycle+;
#pragma link C++ class NtupleCycle+;
#pragma link C++ class OptimizationCycle+;
#pragma link C++ class EvaluationHelpers+;
#pragma link C++ class EfficiencyBaseCycle+;
#pragma link C++ class EfficiencyOptimisationCycle+;
#pragma link C++ class EfficiencyEvaluationCycle+;
#pragma link C++ class PDFPlotter+;
#pragma link C++ class TTNTCycle+;
#pragma link C++ class BDTCalculator+;
#pragma link C++ class Pion+;
#pragma link C++ class IDVarSet+;
#pragma link C++ class IDVarCalculatorBase<CellBasedCalculator>+;
#pragma link C++ class WeightTool+;
#pragma link C++ class PileUpCorrectionTool+;
// #pragma link C++ class TMVA::Reader+;
#pragma link C++ class TMVA::Configurable+;
#endif //SKIM
#pragma link C++ class std::vector<std::vector<int> >+;
#pragma link C++ class std::vector<std::vector<std::vector<int> > >+;
#pragma link C++ class std::vector<std::vector<unsigned int> >+;
#pragma link C++ class std::vector<std::vector<long> >+;
#pragma link C++ class std::vector<std::vector<unsigned long> >+;
#pragma link C++ class std::vector<std::vector<float> >+;
#pragma link C++ class std::vector<std::vector<std::vector<float> > >+;
#pragma link C++ class std::vector<std::vector<double> >+;
#pragma link C++ class std::vector<std::vector<std::vector<double> > >+;
#pragma link C++ class std::vector<std::string>+;
#pragma link C++ class std::vector<std::vector<std::string> >+;
#pragma link C++ class std::map<std::string,ToolBase*>;
#pragma link C++ class std::map<std::string,std::pair<std::string,float*> > +;

#endif // __CINT__

