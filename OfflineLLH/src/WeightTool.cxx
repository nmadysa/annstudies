#ifndef WEIGHTTOOL_CXX
#define WEIGHTTOOL_CXX

#include "WeightTool.h"
#include "TFile.h"
#include "TauCommonUtilities.h"
#include "Constants.h"

using namespace AnalysisUtilities_Constants;

void WeightTool::ReadWeights() {
  TFile* fw = TFile::Open(c_inputfile_weights.c_str());
  m_hPtWeights_1p = (TH1F*) fw->Get("1prong/pt_weights");
  m_hPtWeights_3p = (TH1F*) fw->Get("3prong/pt_weights");
  m_hMuWeights_1p = (TH1F*) fw->Get("1prong/mu_weights");
  m_hMuWeights_3p = (TH1F*) fw->Get("3prong/mu_weights");
}


float WeightTool::getWeight(const D3PDReader::AnalysisTau* const pTau, const float& mu){
  if(m_bSwitchOff)
    return 1.;
  if(m_bIsData)
    return GetPtWeight(pTau);
  else
    return GetMuWeight(pTau, mu);
}

float WeightTool::GetPtWeight(const D3PDReader::AnalysisTau* const pTau) {
  if(pTau->numTrack() == 1)
    return m_hPtWeights_1p->GetBinContent(m_hPtWeights_1p->FindBin(pTau->TLV().Pt() / GeV));
  else if(pTau->numTrack() == 2 || pTau->numTrack() == 3)
    return m_hPtWeights_3p->GetBinContent(m_hPtWeights_3p->FindBin(pTau->TLV().Pt() / GeV));
  return 0;
}

float WeightTool::GetMuWeight(const D3PDReader::AnalysisTau* const pTau, const float& mu) {
  if (mu > 4. && mu < 35.) {
    if(pTau->numTrack() == 1)
      return m_hMuWeights_1p->GetBinContent(m_hMuWeights_1p->FindBin(mu));
    else if(pTau->numTrack() == 2 || pTau->numTrack() == 3)
      return m_hMuWeights_3p->GetBinContent(m_hMuWeights_3p->FindBin(mu));
  }
  return 0;
}

float WeightTool::getWeight(const int& numTrack, const float& pt, const float& mu){
  if(m_bSwitchOff)
    return 1.;
  if(m_bIsData)
    return GetPtWeight(numTrack, pt);
  else
    return GetMuWeight(numTrack, pt, mu);
}

float WeightTool::GetPtWeight(const int& numTrack, const float& pt) {
  if(numTrack == 1)
    return m_hPtWeights_1p->GetBinContent(m_hPtWeights_1p->FindBin(pt));
  else if(numTrack == 2 || numTrack == 3)
    return m_hPtWeights_3p->GetBinContent(m_hPtWeights_3p->FindBin(pt));
  return 0;
}

float WeightTool::GetMuWeight(const int& numTrack, const float& pt, const float& mu) {
  if (mu > 4. && mu < 35.) {
    if(numTrack == 1)
      return m_hMuWeights_1p->GetBinContent(m_hMuWeights_1p->FindBin(mu));
    else if(numTrack == 2 || numTrack == 3)
      return m_hMuWeights_3p->GetBinContent(m_hMuWeights_3p->FindBin(mu));
  }
  return 0;
}



#endif //WEIGHTTOOL_CXX
