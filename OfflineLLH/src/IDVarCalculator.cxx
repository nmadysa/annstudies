#ifndef SKIM
#ifndef IDVARCALCULATOR_CXX
#define IDVARCALCULATOR_CXX

#include "IDVarCalculator.h"
#include "TLorentzVector.h"


void CellBasedCalculator::initialiseImpl(const D3PDReader::AnalysisTau& tau) {
  m_vChargedPions.clear();
  m_vNeutralPions.clear();
  m_vNeutrals.clear();
  m_vChargedHLV.clear();
  m_vNeutralHLV.clear();
  m_vNeutLowAHLV.clear();
  m_vNeutLowBHLV.clear();
  m_vOuterChrgHLV.clear();
  m_vOuterNeutHLV.clear();

  for(unsigned int iPi = 0; iPi < tau.pantau_CellBased_ChargedEFOs_pt().size(); iPi++)
    m_vChargedPions.push_back(Pion(tau.pantau_CellBased_ChargedEFOs_pt().at(iPi),
				   tau.pantau_CellBased_ChargedEFOs_eta().at(iPi),
				   tau.pantau_CellBased_ChargedEFOs_phi().at(iPi),
				   tau.pantau_CellBased_ChargedEFOs_m().at(iPi)));
  
  for(unsigned int iPi = 0; iPi < tau.pantau_CellBased_Pi0NeutEFOs_pt().size(); iPi++)
    m_vNeutralPions.push_back(Pion(tau.pantau_CellBased_Pi0NeutEFOs_pt().at(iPi),
				  tau.pantau_CellBased_Pi0NeutEFOs_eta().at(iPi),
				  tau.pantau_CellBased_Pi0NeutEFOs_phi().at(iPi),
				  tau.pantau_CellBased_Pi0NeutEFOs_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantau_CellBased_NeutralEFOs_pt().size(); iPi++)
    m_vNeutrals.push_back(Pion(tau.pantau_CellBased_NeutralEFOs_pt().at(iPi),
			       tau.pantau_CellBased_NeutralEFOs_eta().at(iPi),
			       tau.pantau_CellBased_NeutralEFOs_phi().at(iPi),
			       tau.pantau_CellBased_NeutralEFOs_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_CellBased_Charged_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vChargedHLV.push_back(Pion(tau.pantauFeature_CellBased_Charged_HLV_EtSort_Constituents_pt().at(iPi),
				 tau.pantauFeature_CellBased_Charged_HLV_EtSort_Constituents_eta().at(iPi),
				 tau.pantauFeature_CellBased_Charged_HLV_EtSort_Constituents_phi().at(iPi),
				 tau.pantauFeature_CellBased_Charged_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_CellBased_Neutral_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vNeutralHLV.push_back(Pion(tau.pantauFeature_CellBased_Neutral_HLV_EtSort_Constituents_pt().at(iPi),
				 tau.pantauFeature_CellBased_Neutral_HLV_EtSort_Constituents_eta().at(iPi),
				 tau.pantauFeature_CellBased_Neutral_HLV_EtSort_Constituents_phi().at(iPi),
				 tau.pantauFeature_CellBased_Neutral_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_CellBased_NeutLowA_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vNeutLowAHLV.push_back(Pion(tau.pantauFeature_CellBased_NeutLowA_HLV_EtSort_Constituents_pt().at(iPi),
				  tau.pantauFeature_CellBased_NeutLowA_HLV_EtSort_Constituents_eta().at(iPi),
				  tau.pantauFeature_CellBased_NeutLowA_HLV_EtSort_Constituents_phi().at(iPi),
				  tau.pantauFeature_CellBased_NeutLowA_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_CellBased_NeutLowB_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vNeutLowBHLV.push_back(Pion(tau.pantauFeature_CellBased_NeutLowB_HLV_EtSort_Constituents_pt().at(iPi),
				  tau.pantauFeature_CellBased_NeutLowB_HLV_EtSort_Constituents_eta().at(iPi),
				  tau.pantauFeature_CellBased_NeutLowB_HLV_EtSort_Constituents_phi().at(iPi),
				  tau.pantauFeature_CellBased_NeutLowB_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_CellBased_OuterChrg_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vOuterChrgHLV.push_back(Pion(tau.pantauFeature_CellBased_OuterChrg_HLV_EtSort_Constituents_pt().at(iPi),
				   tau.pantauFeature_CellBased_OuterChrg_HLV_EtSort_Constituents_eta().at(iPi),
				   tau.pantauFeature_CellBased_OuterChrg_HLV_EtSort_Constituents_phi().at(iPi),
				   tau.pantauFeature_CellBased_OuterChrg_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_CellBased_OuterNeut_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vOuterNeutHLV.push_back(Pion(tau.pantauFeature_CellBased_OuterNeut_HLV_EtSort_Constituents_pt().at(iPi),
				   tau.pantauFeature_CellBased_OuterNeut_HLV_EtSort_Constituents_eta().at(iPi),
				   tau.pantauFeature_CellBased_OuterNeut_HLV_EtSort_Constituents_phi().at(iPi),
				   tau.pantauFeature_CellBased_OuterNeut_HLV_EtSort_Constituents_m().at(iPi)));

  m_ftransSignficance = tau.pantauFeature_CellBased_ImpactParams_TransSignfIPTrack1_SortByEt();
  m_ftrkTransSig1 = tau.pantauFeature_CellBased_ImpactParams_TransSignfIP1_SortByValue();
  m_ftrkTransSig2 = tau.pantauFeature_CellBased_ImpactParams_TransSignfIP2_SortByValue();
  m_ftrkTransSig3 = tau.pantauFeature_CellBased_ImpactParams_TransSignfIP3_SortByValue();	     
}

void EFlowRecCalculator::initialiseImpl(const D3PDReader::AnalysisTau& tau) {
  m_vChargedPions.clear();
  m_vNeutralPions.clear();
  m_vNeutrals.clear();
  m_vChargedHLV.clear();
  m_vNeutralHLV.clear();
  m_vNeutLowAHLV.clear();
  m_vNeutLowBHLV.clear();
  m_vOuterChrgHLV.clear();
  m_vOuterNeutHLV.clear();

  for(unsigned int iPi = 0; iPi < tau.pantau_eflowRec_ChargedEFOs_pt().size(); iPi++)
    m_vChargedPions.push_back(Pion(tau.pantau_eflowRec_ChargedEFOs_pt().at(iPi),
				   tau.pantau_eflowRec_ChargedEFOs_eta().at(iPi),
				   tau.pantau_eflowRec_ChargedEFOs_phi().at(iPi),
				   tau.pantau_eflowRec_ChargedEFOs_m().at(iPi)));
  
  for(unsigned int iPi = 0; iPi < tau.pantau_eflowRec_Pi0NeutEFOs_pt().size(); iPi++)
    m_vNeutralPions.push_back(Pion(tau.pantau_eflowRec_Pi0NeutEFOs_pt().at(iPi),
				  tau.pantau_eflowRec_Pi0NeutEFOs_eta().at(iPi),
				  tau.pantau_eflowRec_Pi0NeutEFOs_phi().at(iPi),
				  tau.pantau_eflowRec_Pi0NeutEFOs_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantau_eflowRec_NeutralEFOs_pt().size(); iPi++)
    m_vNeutrals.push_back(Pion(tau.pantau_eflowRec_NeutralEFOs_pt().at(iPi),
			       tau.pantau_eflowRec_NeutralEFOs_eta().at(iPi),
			       tau.pantau_eflowRec_NeutralEFOs_phi().at(iPi),
			       tau.pantau_eflowRec_NeutralEFOs_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_eflowRec_Charged_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vChargedHLV.push_back(Pion(tau.pantauFeature_eflowRec_Charged_HLV_EtSort_Constituents_pt().at(iPi),
				 tau.pantauFeature_eflowRec_Charged_HLV_EtSort_Constituents_eta().at(iPi),
				 tau.pantauFeature_eflowRec_Charged_HLV_EtSort_Constituents_phi().at(iPi),
				 tau.pantauFeature_eflowRec_Charged_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_eflowRec_Neutral_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vNeutralHLV.push_back(Pion(tau.pantauFeature_eflowRec_Neutral_HLV_EtSort_Constituents_pt().at(iPi),
				 tau.pantauFeature_eflowRec_Neutral_HLV_EtSort_Constituents_eta().at(iPi),
				 tau.pantauFeature_eflowRec_Neutral_HLV_EtSort_Constituents_phi().at(iPi),
				 tau.pantauFeature_eflowRec_Neutral_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_eflowRec_NeutLowA_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vNeutLowAHLV.push_back(Pion(tau.pantauFeature_eflowRec_NeutLowA_HLV_EtSort_Constituents_pt().at(iPi),
				  tau.pantauFeature_eflowRec_NeutLowA_HLV_EtSort_Constituents_eta().at(iPi),
				  tau.pantauFeature_eflowRec_NeutLowA_HLV_EtSort_Constituents_phi().at(iPi),
				  tau.pantauFeature_eflowRec_NeutLowA_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_eflowRec_NeutLowB_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vNeutLowBHLV.push_back(Pion(tau.pantauFeature_eflowRec_NeutLowB_HLV_EtSort_Constituents_pt().at(iPi),
				  tau.pantauFeature_eflowRec_NeutLowB_HLV_EtSort_Constituents_eta().at(iPi),
				  tau.pantauFeature_eflowRec_NeutLowB_HLV_EtSort_Constituents_phi().at(iPi),
				  tau.pantauFeature_eflowRec_NeutLowB_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_eflowRec_OuterChrg_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vOuterChrgHLV.push_back(Pion(tau.pantauFeature_eflowRec_OuterChrg_HLV_EtSort_Constituents_pt().at(iPi),
				   tau.pantauFeature_eflowRec_OuterChrg_HLV_EtSort_Constituents_eta().at(iPi),
				   tau.pantauFeature_eflowRec_OuterChrg_HLV_EtSort_Constituents_phi().at(iPi),
				   tau.pantauFeature_eflowRec_OuterChrg_HLV_EtSort_Constituents_m().at(iPi)));

  for(unsigned int iPi = 0; iPi < tau.pantauFeature_eflowRec_OuterNeut_HLV_EtSort_Constituents_pt().size(); iPi++)
    m_vOuterNeutHLV.push_back(Pion(tau.pantauFeature_eflowRec_OuterNeut_HLV_EtSort_Constituents_pt().at(iPi),
				   tau.pantauFeature_eflowRec_OuterNeut_HLV_EtSort_Constituents_eta().at(iPi),
				   tau.pantauFeature_eflowRec_OuterNeut_HLV_EtSort_Constituents_phi().at(iPi),
				   tau.pantauFeature_eflowRec_OuterNeut_HLV_EtSort_Constituents_m().at(iPi)));

  m_ftransSignficance = tau.pantauFeature_eflowRec_ImpactParams_TransSignfIPTrack1_SortByEt();
  m_ftrkTransSig1 = tau.pantauFeature_eflowRec_ImpactParams_TransSignfIP1_SortByValue();
  m_ftrkTransSig2 = tau.pantauFeature_eflowRec_ImpactParams_TransSignfIP2_SortByValue();
  m_ftrkTransSig3 = tau.pantauFeature_eflowRec_ImpactParams_TransSignfIP3_SortByValue();	     
}


#endif
#endif
