#ifndef SKIM
#ifndef PDFPlotter_CXX
#define PDFPlotter_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/lexical_cast.hpp"
#endif // __CINT__

// STL
#include <vector>
#include <iostream>

// custom header
#include "PDFPlotter.h"
#include "TauCommonUtilities.h"
#include "IDVarCalculator.h"

//ROOT includes
#include "TFile.h"
#include "TH2.h"
#include "TH3.h"
#include "Constants.h"

using namespace AnalysisUtilities_Constants;

PDFPlotter::PDFPlotter(CycleBase* pParent,const char* sName):ToolBase(pParent,sName)
{
  DeclareProperty("weights_inputfile", c_inputfile_weights = "");
  DeclareProperty("weights_switchOff", b_switchOffWeightTool = false);
  DeclareProperty("pileup_correction_inputfile", c_pileup_correction_file = "");
  DeclareProperty("pileup_correction_switchOff", c_pileup_correction_switchOff = false);
  
  m_WeightTool = new WeightTool();
  m_pileUpCorrectionTool = new PileUpCorrectionTool();
  //m_IDVarCalculator = new CellBasedCalculator();
  m_IDVarCalculator = new EFlowRecCalculator();
};

void PDFPlotter::BeginInputData(const SInputData& id) throw(SError)
{
  m_WeightTool->setIsData(IsData());
  m_WeightTool->setInputFile(c_inputfile_weights);
  m_WeightTool->switchOff(b_switchOffWeightTool);
  m_WeightTool->ReadWeights();
  m_pileUpCorrectionTool->setInputFile(c_pileup_correction_file);
  m_pileUpCorrectionTool->switchOff(c_pileup_correction_switchOff);
  m_pileUpCorrectionTool->readInput();
  
  const int nVtxRanges = 1;
  const int nJVFRanges = 2;
  const int nPtRanges = 3;
  const int nBins = 50;
  double vtxRanges[nVtxRanges+1] = {0.5, 39.5};
  const double jvfRanges[nJVFRanges+1] = {0.,0.9,1.0};
  const double ptRanges[nPtRanges+1] = {0,45,100,1000};

  double LLHScore[71]; createArrayFromRange(LLHScore, -50., 20., 70);
  Book(TH3F("hpdf_1prong_both_v01","LLHScore", 70, LLHScore, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v01","LLHScore", 70, LLHScore, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double BDTScore[101]; createArrayFromRange(BDTScore, 0., 1., 100);
  Book(TH3F("hpdf_1prong_both_v03","BDTScore", 100, BDTScore, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v03","BDTScore", 100, BDTScore, nPtRanges, ptRanges, nVtxRanges, vtxRanges));

  double ptRatioRange[nBins + 1]; createArrayFromRange(ptRatioRange, 0., 2., nBins);
  Book(TH3F("hpdf_1prong_both_v04","ptRatio", nBins, ptRatioRange, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v04","ptRatio", nBins, ptRatioRange, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double pi0NRange[12]; createArrayFromRange(pi0NRange, -0.5, 10.5, 11);
  Book(TH3F("hpdf_1prong_both_v05","Nb of Pi0", 11, pi0NRange, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v05","Nb of Pi0", 11, pi0NRange, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double pi0VisTauMRange[76]; createArrayFromRange(pi0VisTauMRange, 0., 15., 75);
  Book(TH3F("hpdf_1prong_both_v06","pi0_vistau_m", 75, pi0VisTauMRange, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v06","pi0_vistau_m", 75, pi0VisTauMRange, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double DrMaxRanges[nBins + 1]; createArrayFromRange(DrMaxRanges, 0., 0.204, nBins);
  Book(TH3F("hpdf_1prong_both_v10","DrMax",50, DrMaxRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v10","DrMax",50, DrMaxRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double MassTrkSysRanges[76]; createArrayFromRange(MassTrkSysRanges, 0., 15., 75);
  Book(TH3F("hpdf_1prong_both_v12","mass of 3 prong tracks",75, MassTrkSysRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v12","mass of 3 prong tracks",75, MassTrkSysRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double TrFlightPathSigRanges[61]; createArrayFromRange(TrFlightPathSigRanges, -15., 45., 60);
  Book(TH3F("hpdf_1prong_both_v17","TrFlightPathSig",60, TrFlightPathSigRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v17","TrFlightPathSig",60, TrFlightPathSigRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double TrkAvgDistRanges[nBins + 1]; createArrayFromRange(TrkAvgDistRanges, 0, 0.4, nBins);
  Book(TH3F("hpdf_1prong_both_v20","TrkAvgDist", nBins, TrkAvgDistRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v20","TrkAvgDist", nBins, TrkAvgDistRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double IpSigLeadTrkRanges[201]; createArrayFromRange(IpSigLeadTrkRanges, -50., 50., 200);
  Book(TH3F("hpdf_1prong_both_v23","IpSigLeadTrk",200, IpSigLeadTrkRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v23","IpSigLeadTrk",200, IpSigLeadTrkRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double NTracksdrdRRanges[12]; createArrayFromRange(NTracksdrdRRanges, -0.5, 10.5, 11);
  Book(TH3F("hpdf_1prong_both_v50","Nb of wide tracks",11, NTracksdrdRRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v50","Nb of wide tracks",11, NTracksdrdRRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double CentFrac0102Ranges[66]; createArrayFromRange(CentFrac0102Ranges, 0., 1.25, 65);
  Book(TH3F("hpdf_1prong_both_v53","CentFrac0102",65, CentFrac0102Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v53","CentFrac0102",65, CentFrac0102Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double FTrk02Ranges[nBins + 1]; createArrayFromRange(FTrk02Ranges, -0.11, 1.8, nBins+10);
  Book(TH3F("hpdf_1prong_both_v56","LeadTrkPt over SumCellE02",50, FTrk02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v56","LeadTrkPt over SumCellE02",50, FTrk02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));

  // substructure default id
  double panTauCellBased_nChrg0204Ranges[12]; createArrayFromRange(panTauCellBased_nChrg0204Ranges, -0.5, 10.5, 11.);
  Book(TH3F("hpdf_1prong_both_v100","panTauCellBased_nChrg0204Ranges",11, panTauCellBased_nChrg0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v100","panTauCellBased_nChrg0204Ranges",11, panTauCellBased_nChrg0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_nChrg01Ranges[12]; createArrayFromRange(panTauCellBased_nChrg01Ranges, -0.5, 10.5, 11.);
  Book(TH3F("hpdf_1prong_both_v101","panTauCellBased_nChrg01Ranges",11, panTauCellBased_nChrg01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v101","panTauCellBased_nChrg01Ranges",11, panTauCellBased_nChrg01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_nChrg02Ranges[12]; createArrayFromRange(panTauCellBased_nChrg02Ranges, -0.5, 10.5, 11.);
  Book(TH3F("hpdf_1prong_both_v102","panTauCellBased_nChrg02Ranges",11, panTauCellBased_nChrg02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v102","panTauCellBased_nChrg02Ranges",11, panTauCellBased_nChrg02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_nNeut0204Ranges[14]; createArrayFromRange(panTauCellBased_nNeut0204Ranges, -2.5, 10.5, 13.);
  Book(TH3F("hpdf_1prong_both_v103","panTauCellBased_nNeut0204Ranges",13, panTauCellBased_nNeut0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v103","panTauCellBased_nNeut0204Ranges",13, panTauCellBased_nNeut0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_nNeut01Ranges[12]; createArrayFromRange(panTauCellBased_nNeut01Ranges, -0.5, 10.5, 11.);
  Book(TH3F("hpdf_1prong_both_v104","panTauCellBased_nNeut01Ranges",11, panTauCellBased_nNeut01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v104","panTauCellBased_nNeut01Ranges",11, panTauCellBased_nNeut01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_nNeut02Ranges[12]; createArrayFromRange(panTauCellBased_nNeut02Ranges, -0.5, 10.5, 11.);
  Book(TH3F("hpdf_1prong_both_v105","panTauCellBased_nNeut02Ranges",11, panTauCellBased_nNeut02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v105","panTauCellBased_nNeut02Ranges",11, panTauCellBased_nNeut02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_massChrgSys01Ranges[101]; createArrayFromRange(panTauCellBased_massChrgSys01Ranges, -5., 15., 100);
  Book(TH3F("hpdf_1prong_both_v106","panTauCellBased_massChrgSys01Ranges",100, panTauCellBased_massChrgSys01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v106","panTauCellBased_massChrgSys01Ranges",100, panTauCellBased_massChrgSys01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_massChrgSys02Ranges[101]; createArrayFromRange(panTauCellBased_massChrgSys02Ranges, -5., 15., 100);
  Book(TH3F("hpdf_1prong_both_v107","panTauCellBased_massChrgSys02Ranges",100, panTauCellBased_massChrgSys02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v107","panTauCellBased_massChrgSys02Ranges",100, panTauCellBased_massChrgSys02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_massChrgSys04Ranges[101]; createArrayFromRange(panTauCellBased_massChrgSys04Ranges, -5., 15., 100);
  Book(TH3F("hpdf_1prong_both_v108","panTauCellBased_massChrgSys04Ranges",100, panTauCellBased_massChrgSys04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v108","panTauCellBased_massChrgSys04Ranges",100, panTauCellBased_massChrgSys04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_massNeutSys01Ranges[101]; createArrayFromRange(panTauCellBased_massNeutSys01Ranges, -5., 15., 100);
  Book(TH3F("hpdf_1prong_both_v109","panTauCellBased_massNeutSys01Ranges",100, panTauCellBased_massNeutSys01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v109","panTauCellBased_massNeutSys01Ranges",100, panTauCellBased_massNeutSys01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_massNeutSys02Ranges[101]; createArrayFromRange(panTauCellBased_massNeutSys02Ranges, -5., 15., 100);
  Book(TH3F("hpdf_1prong_both_v110","panTauCellBased_massNeutSys02Ranges",100, panTauCellBased_massNeutSys02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v110","panTauCellBased_massNeutSys02Ranges",100, panTauCellBased_massNeutSys02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_massNeutSys04Ranges[101]; createArrayFromRange(panTauCellBased_massNeutSys04Ranges, -5., 15., 100);
  Book(TH3F("hpdf_1prong_both_v111","panTauCellBased_massNeutSys04Ranges",100, panTauCellBased_massNeutSys04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v111","panTauCellBased_massNeutSys04Ranges",100, panTauCellBased_massNeutSys04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_visTauM01Ranges[101]; createArrayFromRange(panTauCellBased_visTauM01Ranges, -5., 15., 100);
  Book(TH3F("hpdf_1prong_both_v112","panTauCellBased_visTauM01Ranges",100, panTauCellBased_visTauM01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v112","panTauCellBased_visTauM01Ranges",100, panTauCellBased_visTauM01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_visTauM02Ranges[101]; createArrayFromRange(panTauCellBased_visTauM02Ranges, -5., 15., 100);
  Book(TH3F("hpdf_1prong_both_v113","panTauCellBased_visTauM02Ranges",100, panTauCellBased_visTauM02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v113","panTauCellBased_visTauM02Ranges",100, panTauCellBased_visTauM02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_visTauM04Ranges[101]; createArrayFromRange(panTauCellBased_visTauM04Ranges, -5., 15., 100);
  Book(TH3F("hpdf_1prong_both_v114","panTauCellBased_visTauM04Ranges",100, panTauCellBased_visTauM04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v114","panTauCellBased_visTauM04Ranges",100, panTauCellBased_visTauM04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_dRmax02Ranges[nBins + 3]; createArrayFromRange(panTauCellBased_dRmax02Ranges, -0.08, 0.204, nBins+2);
  Book(TH3F("hpdf_1prong_both_v115","panTauCellBased_dRmax02Ranges",52, panTauCellBased_dRmax02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v115","panTauCellBased_dRmax02Ranges",52, panTauCellBased_dRmax02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_dRmax04Ranges[nBins + 3]; createArrayFromRange(panTauCellBased_dRmax04Ranges, -0.16, 0.408, nBins+2);
  Book(TH3F("hpdf_1prong_both_v116","panTauCellBased_dRmax04Ranges",52, panTauCellBased_dRmax04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v116","panTauCellBased_dRmax04Ranges",52, panTauCellBased_dRmax04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ipSigLeadTrkRanges[201]; createArrayFromRange(panTauCellBased_ipSigLeadTrkRanges, -50., 50., 200);
  Book(TH3F("hpdf_1prong_both_v117","panTauCellBased_ipSigLeadTrkRanges",200, panTauCellBased_ipSigLeadTrkRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v117","panTauCellBased_ipSigLeadTrkRanges",200, panTauCellBased_ipSigLeadTrkRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_trFlightPathSigRanges[61]; createArrayFromRange(panTauCellBased_trFlightPathSigRanges, -15., 45., 60);
  Book(TH3F("hpdf_1prong_both_v118","panTauCellBased_trFlightPathSigRanges",60, panTauCellBased_trFlightPathSigRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v118","panTauCellBased_trFlightPathSigRanges",60, panTauCellBased_trFlightPathSigRanges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ptRatio01Ranges[nBins + 1]; createArrayFromRange(panTauCellBased_ptRatio01Ranges, 0., 2., nBins);
  Book(TH3F("hpdf_1prong_both_v119","panTauCellBased_ptRatio01Ranges",50, panTauCellBased_ptRatio01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v119","panTauCellBased_ptRatio01Ranges",50, panTauCellBased_ptRatio01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ptRatio02Ranges[nBins + 1]; createArrayFromRange(panTauCellBased_ptRatio02Ranges, 0., 2., nBins);
  Book(TH3F("hpdf_1prong_both_v120","panTauCellBased_ptRatio02Ranges",50, panTauCellBased_ptRatio02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v120","panTauCellBased_ptRatio02Ranges",50, panTauCellBased_ptRatio02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ptRatio04Ranges[nBins + 1]; createArrayFromRange(panTauCellBased_ptRatio04Ranges, 0., 2., nBins);
  Book(TH3F("hpdf_1prong_both_v121","panTauCellBased_ptRatio04Ranges",50, panTauCellBased_ptRatio04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v121","panTauCellBased_ptRatio04Ranges",50, panTauCellBased_ptRatio04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ptRatioNeut01Ranges[nBins + 3]; createArrayFromRange(panTauCellBased_ptRatioNeut01Ranges, -0.08, 2., nBins+2);
  Book(TH3F("hpdf_1prong_both_v122","panTauCellBased_ptRatioNeut01Ranges",52, panTauCellBased_ptRatioNeut01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v122","panTauCellBased_ptRatioNeut01Ranges",52, panTauCellBased_ptRatioNeut01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ptRatioNeut02Ranges[nBins + 3]; createArrayFromRange(panTauCellBased_ptRatioNeut02Ranges, -0.08, 2., nBins+2);
  Book(TH3F("hpdf_1prong_both_v123","panTauCellBased_ptRatioNeut02Ranges",52, panTauCellBased_ptRatioNeut02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v123","panTauCellBased_ptRatioNeut02Ranges",52, panTauCellBased_ptRatioNeut02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ptRatioNeut04Ranges[nBins + 3]; createArrayFromRange(panTauCellBased_ptRatioNeut04Ranges, -0.08, 2., nBins+2);
  Book(TH3F("hpdf_1prong_both_v124","panTauCellBased_ptRatioNeut04Ranges",52, panTauCellBased_ptRatioNeut04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v124","panTauCellBased_ptRatioNeut04Ranges",52, panTauCellBased_ptRatioNeut04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ptRatioChrg01Ranges[nBins + 3]; createArrayFromRange(panTauCellBased_ptRatioChrg01Ranges, -0.08, 2., nBins+2);
  Book(TH3F("hpdf_1prong_both_v125","panTauCellBased_ptRatioChrg01Ranges",52, panTauCellBased_ptRatioChrg01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v125","panTauCellBased_ptRatioChrg01Ranges",52, panTauCellBased_ptRatioChrg01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ptRatioChrg02Ranges[nBins + 3]; createArrayFromRange(panTauCellBased_ptRatioChrg02Ranges, -0.08, 2., nBins+2);
  Book(TH3F("hpdf_1prong_both_v126","panTauCellBased_ptRatioChrg02Ranges",52, panTauCellBased_ptRatioChrg02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v126","panTauCellBased_ptRatioChrg02Ranges",52, panTauCellBased_ptRatioChrg02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_ptRatioChrg04Ranges[nBins + 3]; createArrayFromRange(panTauCellBased_ptRatioChrg04Ranges, -0.08, 2., nBins+2);
  Book(TH3F("hpdf_1prong_both_v127","panTauCellBased_ptRatioChrg04Ranges",52, panTauCellBased_ptRatioChrg04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v127","panTauCellBased_ptRatioChrg04Ranges",52, panTauCellBased_ptRatioChrg04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_fLeadChrg01Ranges[63]; createArrayFromRange(panTauCellBased_fLeadChrg01Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v128","panTauCellBased_fLeadChrg01Ranges",62, panTauCellBased_fLeadChrg01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v128","panTauCellBased_fLeadChrg01Ranges",62, panTauCellBased_fLeadChrg01Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_fLeadChrg02Ranges[63]; createArrayFromRange(panTauCellBased_fLeadChrg02Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v129","panTauCellBased_fLeadChrg02Ranges",62, panTauCellBased_fLeadChrg02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v129","panTauCellBased_fLeadChrg02Ranges",62, panTauCellBased_fLeadChrg02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_fLeadChrg04Ranges[63]; createArrayFromRange(panTauCellBased_fLeadChrg04Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v130","panTauCellBased_fLeadChrg04Ranges",62, panTauCellBased_fLeadChrg04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v130","panTauCellBased_fLeadChrg04Ranges",62, panTauCellBased_fLeadChrg04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_eFrac0204Ranges[63]; createArrayFromRange(panTauCellBased_eFrac0204Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v131","panTauCellBased_eFrac0204Ranges",62, panTauCellBased_eFrac0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v131","panTauCellBased_eFrac0204Ranges",62, panTauCellBased_eFrac0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_eFrac0104Ranges[63]; createArrayFromRange(panTauCellBased_eFrac0104Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v132","panTauCellBased_eFrac0104Ranges",62, panTauCellBased_eFrac0104Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v132","panTauCellBased_eFrac0104Ranges",62, panTauCellBased_eFrac0104Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_eFracChrg0204Ranges[63]; createArrayFromRange(panTauCellBased_eFracChrg0204Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v133","panTauCellBased_eFracChrg0204Ranges",62, panTauCellBased_eFracChrg0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v133","panTauCellBased_eFracChrg0204Ranges",62, panTauCellBased_eFracChrg0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_eFracChrg0104Ranges[63]; createArrayFromRange(panTauCellBased_eFracChrg0104Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v134","panTauCellBased_eFracChrg0104Ranges",62, panTauCellBased_eFracChrg0104Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v134","panTauCellBased_eFracChrg0104Ranges",62, panTauCellBased_eFracChrg0104Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_eFracNeut0204Ranges[63]; createArrayFromRange(panTauCellBased_eFracNeut0204Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v135","panTauCellBased_eFracNeut0204Ranges",62, panTauCellBased_eFracNeut0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v135","panTauCellBased_eFracNeut0204Ranges",62, panTauCellBased_eFracNeut0204Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_eFracNeut0104Ranges[63]; createArrayFromRange(panTauCellBased_eFracNeut0104Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v136","panTauCellBased_eFracNeut0104Ranges",62, panTauCellBased_eFracNeut0104Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v136","panTauCellBased_eFracNeut0104Ranges",62, panTauCellBased_eFracNeut0104Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_rCal02Ranges[nBins + 4]; createArrayFromRange(panTauCellBased_rCal02Ranges, -0.08, 0.204, nBins+3);
  Book(TH3F("hpdf_1prong_both_v137","panTauCellBased_rCal02Ranges",53., panTauCellBased_rCal02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v137","panTauCellBased_rCal02Ranges",53., panTauCellBased_rCal02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_rCal04Ranges[nBins + 4]; createArrayFromRange(panTauCellBased_rCal04Ranges, -0.08, 0.408, nBins+3);
  Book(TH3F("hpdf_1prong_both_v138","panTauCellBased_rCal04Ranges",53., panTauCellBased_rCal04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v138","panTauCellBased_rCal04Ranges",53., panTauCellBased_rCal04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_rCalChrg02Ranges[nBins + 4]; createArrayFromRange(panTauCellBased_rCalChrg02Ranges, -0.08, 0.204, nBins+3);
  Book(TH3F("hpdf_1prong_both_v139","panTauCellBased_rCalChrg02Ranges",53., panTauCellBased_rCalChrg02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v139","panTauCellBased_rCalChrg02Ranges",53., panTauCellBased_rCalChrg02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_rCalChrg04Ranges[nBins + 4]; createArrayFromRange(panTauCellBased_rCalChrg04Ranges, -0.16, 0.408, nBins+3);
  Book(TH3F("hpdf_1prong_both_v140","panTauCellBased_rCalChrg04Ranges",53., panTauCellBased_rCalChrg04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v140","panTauCellBased_rCalChrg04Ranges",53., panTauCellBased_rCalChrg04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_rCalNeut02Ranges[101]; createArrayFromRange(panTauCellBased_rCalNeut02Ranges, -0.04, 0.24, 100);
  Book(TH3F("hpdf_1prong_both_v141","panTauCellBased_rCalNeut02Ranges",100., panTauCellBased_rCalNeut02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v141","panTauCellBased_rCalNeut02Ranges",100., panTauCellBased_rCalNeut02Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_rCalNeut04Ranges[nBins + 6]; createArrayFromRange(panTauCellBased_rCalNeut04Ranges, -0.16, 0.408, nBins+5);
  Book(TH3F("hpdf_1prong_both_v142","panTauCellBased_rCalNeut04Ranges",55., panTauCellBased_rCalNeut04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v142","panTauCellBased_rCalNeut04Ranges",55., panTauCellBased_rCalNeut04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_dRminmaxPtChrg04Ranges[nBins + 3]; createArrayFromRange(panTauCellBased_dRminmaxPtChrg04Ranges, -0.32, 0.8, nBins+2);
  Book(TH3F("hpdf_1prong_both_v143","panTauCellBased_dRminmaxPtChrg04Ranges",52, panTauCellBased_dRminmaxPtChrg04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v143","panTauCellBased_dRminmaxPtChrg04Ranges",52, panTauCellBased_dRminmaxPtChrg04Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  double panTauCellBased_eFrac0102Ranges[63]; createArrayFromRange(panTauCellBased_eFrac0102Ranges, -0.08, 1.2, 62);
  Book(TH3F("hpdf_1prong_both_v144","panTauCellBased_eFrac0102Ranges",62, panTauCellBased_eFrac0102Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));
  Book(TH3F("hpdf_3prong_both_v144","panTauCellBased_eFrac0102Ranges",62, panTauCellBased_eFrac0102Ranges, nPtRanges, ptRanges, nVtxRanges, vtxRanges));

}

void PDFPlotter::PlotDistributions(std::vector<D3PDReader::AnalysisTau*>& vTaus, int nvtx,  float fWeight)
{
  for(auto tau : vTaus){
    const std::string prong = (tau->numTrack()==1) ? "1prong" : "3prong";
    const float pt = tau->TLV().Pt()/1000.;

    m_piCellBasedIDVars = m_IDVarCalculator->calculateVariables(*tau);

    float weight = m_WeightTool->getWeight(tau, nvtx);
    // float weight = 1.;
    
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v01").c_str())))->Fill(tau->etOverPtLeadTrk(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v03").c_str())))->Fill(tau->BDTJetScore(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v04").c_str())))->Fill(tau->ptRatio(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v05").c_str())))->Fill(tau->pi0_n(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v06").c_str())))->Fill(tau->pi0_vistau_m() / 1000., pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v10").c_str())))->Fill(tau->seedCalo_dRmax(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v12").c_str())))->Fill(tau->massTrkSys() / 1000, pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v17").c_str())))->Fill(tau->trFlightPathSig(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v20").c_str())))->Fill(tau->seedCalo_trkAvgDist(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v23").c_str())))->Fill(tau->ipSigLeadTrk(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v50").c_str())))->Fill(tau->seedCalo_wideTrk_n(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v53").c_str())))->Fill(tau->calcVars_corrCentFrac(), pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v56").c_str())))->Fill(tau->calcVars_corrFTrk(), pt, nvtx, weight);

    // substructure default id 
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v100").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg0204",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.nChrg0204),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v101").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg01",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.nChrg01),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v102").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.nChrg02),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v103").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut0204",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.nNeut0204),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v104").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut01",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.nNeut01),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v105").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.nNeut02),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v106").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys01",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.massChrgSys01)  / GeV,
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v107").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.massChrgSys02) / GeV,
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v108").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.massChrgSys04) / GeV,
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v109").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys01",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.massNeutSys01) / GeV,
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v110").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.massNeutSys02) / GeV,
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v111").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.massNeutSys04) / GeV,
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v112").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM01",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.visTauM01) / GeV,
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v113").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.visTauM02) / GeV,
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v114").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.visTauM04)  / GeV,
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v115").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRmax02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.dRmax02),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v116").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRmax04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.dRmax04),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v117").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ipSigLeadTrk",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ipSigLeadTrk),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v118").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_trFlightPathSig",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.trFlightPathSig),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v119").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio01",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ptRatio01),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v120").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ptRatio02),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v121").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ptRatio04),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v122").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut01",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ptRatioNeut01),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v123").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ptRatioNeut02),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v124").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ptRatioNeut04),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v125").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg01",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ptRatioChrg01),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v126").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ptRatioChrg02),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v127").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.ptRatioChrg04),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v128").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg01",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.fLeadChrg01),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v129").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.fLeadChrg02),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v130").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.fLeadChrg04),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v131").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0204",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.eFrac0204),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v132").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0104",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.eFrac0104),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v133").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg0204",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.eFracChrg0204),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v134").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg0104",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.eFracChrg0104),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v135").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut0204",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.eFracNeut0204),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v136").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut0104",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.eFracNeut0104),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v137").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCal02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.rCal02),
									    pt, nvtx, weight);
    if(weight > 10)
      std::cout << "weight > 0" << std::endl;
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v138").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCal04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.rCal04),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v139").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalChrg02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.rCalChrg02),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v140").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalChrg04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.rCalChrg04),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v141").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalNeut02",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.rCalNeut02),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v142").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalNeut04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.rCalNeut04),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v143").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRminmaxPtChrg04",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.dRminmaxPtChrg04),
									    pt, nvtx, weight);
    (dynamic_cast<TH3F*>(Hist(("hpdf_"+prong+"_both_v144").c_str())))->Fill(m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0102",
														    tau,
														    nvtx,
														    m_piCellBasedIDVars.eFrac0102),
									    pt, nvtx, weight);
    
  }
}

void PDFPlotter::createArrayFromRange(double* array,
				      const double& start,
				      const double& end,
				      const unsigned int& step){
  for(unsigned int i = 0; i < step + 1; i++)
    array[i] = start + i* ((end - start) / step);
}

#endif // PDFPlotter_CXX
#endif // SKIM
