#ifndef PILEUPCORRECTIONTOOL_CXX
#define PILEUPCORRECTIONTOOL_CXX

#include "PileUpCorrectionTool.h"
#include "TFile.h"
#include "TauCommonUtilities.h"

using namespace TauCommonUtilities;

void PileUpCorrectionTool::readInput() {
  TFile* fc = TFile::Open(m_sFileName.c_str());
  m_hCorrections = (TH1F*) fc->Get("h_pileupcorr");
}


float PileUpCorrectionTool::getCorrectedVar(const std::string& sVarname,
					    const D3PDReader::AnalysisTau* const pTau,
					    const int& mu,
					    const float& val){
  if(m_bSwitchOff)
    return val;
  float corr = getCorrection(sVarname, pTau);
  if(corr == 1111.)
    return 0;
  // if(sVarname.compare("panTauCellBased_rCal04") == 0)
  //   std::cout << "orig: " << val << " corr: " << val - corr * mu << std::endl;
  return val - corr * mu;
}

float PileUpCorrectionTool::getCorrection(const std::string& sVarname,
					  const D3PDReader::AnalysisTau* const pTau){
  // if(sVarname.compare("panTauCellBased_rCal04") != 0) 
  //    return 0;
  // cout << sVarname << std::endl;
  const std::string sProng = (pTau->numTrack()==1) ? "_1P" : "_3P";    
  int bin = m_hCorrections->GetXaxis()->FindBin((sVarname + sProng).c_str());
  // if(sVarname.compare("panTauCellBased_rCal04") == 0)
  //   std::cout << m_hCorrections->GetBinContent(bin) << std::endl;
  return m_hCorrections->GetBinContent(bin);
}

#endif //PILEUPCORRECTIONTOOL_CXX
