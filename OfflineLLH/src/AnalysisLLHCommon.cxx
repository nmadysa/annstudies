#ifndef SKIM
#ifndef AnalysisLLHCommon_CXX
#define AnalysisLLHCommon_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

// core include(s)
#include "Region.h"
#include "Utilities.h"
#include "CutTemplateManager.h"

#include "TauSelector.h"

// LLH include(s)
#include "AnalysisLLHCommon.h"
#include "Tau.h"

ClassImp(AnalysisLLHCommon);

AnalysisLLHCommon::AnalysisLLHCommon():
  CycleBase(),
  m_rEventInfo(m_lEntryNumber),
  m_rTauCollection(m_lEntryNumber),
  m_rTruthTauCollection(m_lEntryNumber)
{
  DeclareProperty("do_copy_skim_cutflow", c_copy_skim_cutflow=false);
  DeclareProperty("skim_cutflow_name", c_skim_cut_flow);
  DeclareProperty("skim_cutflow_name_raw", c_skim_cut_flow_raw);

  DeclareTool(new TauSelector(this, "TauSelector"));
  DeclareTool(new Region(this,"BaselineSelection"));

  CutTemplateManager* pManager = CutTemplateManager::GetInstance();
  pManager->RegisterCutTemplate(new CutTemplate<TYPELIST_1(int) > ("NTAUS",boost::bind(&AnalysisLLHCommon::CheckNTaus,this,_1,_2)));
}

bool AnalysisLLHCommon::SelectPhysicsObjects(float& fWeight)
{
(*dynamic_cast<TauSelector*>(GetTool("TauSelector")))(*reinterpret_cast<std::vector<D3PDReader::Tau*> * >(&m_vTaus),fWeight);
  return true;
}

bool AnalysisLLHCommon::CheckNTaus(float fWeight, int iMin)
{
  return (m_vTaus.size() > (unsigned int)iMin);
}

bool AnalysisLLHCommon::TightTruthMatch(D3PDReader::AnalysisTau* const pTau){
  if(IsData())
    return false;
  return(pTau->hasTruthMatch() && pTau->matchNProng() == pTau->numTrack());
}

void AnalysisLLHCommon::TightTruthMatch(std::vector<D3PDReader::AnalysisTau*>& vTaus){
  vTaus.erase(remove_if(vTaus.begin(),
			vTaus.end(),
			!boost::bind<bool>(&AnalysisLLHCommon::TightTruthMatch, this, _1)),
  		  vTaus.end());
}

bool AnalysisLLHCommon::LooserTruthMatch(D3PDReader::AnalysisTau* const pTau){
  if(IsData())
    return false;
  if(!pTau->hasTruthMatch())
    return false;
  if(pTau->matchNProng() == 1)
    return  pTau->numTrack() == 1;
  else if (pTau->matchNProng() == 3)
    return  pTau->numTrack() == 2 or pTau->numTrack() == 3;
  else
    return false;
  return false;
}

void AnalysisLLHCommon::LooserTruthMatch(std::vector<D3PDReader::AnalysisTau*>& vTaus){
  vTaus.erase(remove_if(vTaus.begin(),
			vTaus.end(),
			!boost::bind<bool>(&AnalysisLLHCommon::LooserTruthMatch, this, _1)),
  		  vTaus.end());
}

bool AnalysisLLHCommon::TightRecoNumTrack(D3PDReader::AnalysisTau* const pTau){
  if(!IsData())
    return true;
  return pTau->numTrack() == 1 or pTau->numTrack() == 3;
}

void AnalysisLLHCommon::TightRecoNumTrack(std::vector<D3PDReader::AnalysisTau*>& vTaus){
  vTaus.erase(remove_if(vTaus.begin(),
			vTaus.end(),
			!boost::bind<bool>(&AnalysisLLHCommon::TightRecoNumTrack, this, _1)),
	      vTaus.end());
}

bool AnalysisLLHCommon::LooserRecoNumTrack(D3PDReader::AnalysisTau* const pTau){
  if(!IsData())
    return true;
  return( pTau->numTrack() < 4);
}

void AnalysisLLHCommon::LooserRecoNumTrack(std::vector<D3PDReader::AnalysisTau*>& vTaus){
  vTaus.erase(remove_if(vTaus.begin(),
			vTaus.end(),
			!boost::bind<bool>(&AnalysisLLHCommon::LooserRecoNumTrack, this, _1)),
	      vTaus.end());
}

void AnalysisLLHCommon::CopySkimCutflow(const SInputData& id){
  if(c_copy_skim_cutflow){
    TH1* h_skim = 0;
    TH1* h_skim_raw = 0;
    TH1* temp = 0;
    std::vector<SFile> vInputFiles = id.GetSFileIn();
	
    for(std::vector<SFile>::iterator it = vInputFiles.begin(); it != vInputFiles.end(); ++it)
      {
  	// open input file and retrieve cut flow histograms
  	TFile* f = TFile::Open(it->file,"READ");
  	temp = (TH1*)f->Get(c_skim_cut_flow.c_str());
  	if(!temp)
  	  {
  	    m_logger << ERROR << "Couldn't retrieve skim cutflow with name " << c_skim_cut_flow << " from file " << it->file << SLogger::endmsg;
  	    continue;
  	  }
  	if(h_skim == 0)
  	  {
  	    h_skim = (TH1*)temp->Clone();
  	    h_skim->SetDirectory(0);
  	  }
  	else
  	  h_skim->Add(temp);
	
  	temp = (TH1*)f->Get(c_skim_cut_flow_raw.c_str());
  	if(!temp)
  	  {
  	    m_logger << ERROR << "Couldn't retrieve skim cutflow with name " << c_skim_cut_flow << " from file " << it->file << SLogger::endmsg;
  	    continue;
  	  }
  	if(h_skim_raw == 0)
  	  {
  	    h_skim_raw = (TH1*)temp->Clone();
  	    h_skim_raw->SetDirectory(0);
  	  }
  	else
  	  h_skim_raw->Add(temp);
	     
  	f->Close();
  	delete f;
      }
	
    Book(*h_skim,"SkimSelection");
    Book(*h_skim_raw,"SkimSelection");
  }
}

#endif // AnalysisLLHCommon_CXX
#endif // SKIM
