#ifndef SKIM
#ifndef EfficiencyBaseCycle_CXX
#define EfficiencyBaseCycle_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

#include <cmath>
#include <iterator>
#include <vector>
#include <map>
#include <utility>

#include "CutTemplateManager.h"
#include "Utilities.h"
#include "EfficiencyBaseCycle.h"
#include "Region.h"
#include "VariablePool.h"
#include "IDVarHandle.h"
#include "IDVarInitializer.h"
#include "CombinationHandler.h"
#include "BDTCalculator.h"
#include "TauCommonUtilities.h"
#include "Constants.h"
ClassImp(EfficiencyBaseCycle);

using namespace Utilities;
using namespace AnalysisUtilities_Constants;

// Implementation of EfficiencyBaseCycle::IDVariable.

using IDVariable = EfficiencyBaseCycle::IDVariable;

// Function to allocate memory for the ID variables.

namespace {
  using namespace std;

  IDVariable
  LookUpVariable(const string& varname, const D3PDReader::LLHTau& getter_tau) {
    // Return a pair of the found name and the respective VarHandle.
    #define RETURN_RESULT(NTUPLE_NAME, TTNT_NAME) \
      return IDVariable(#TTNT_NAME, getter_tau.Var_##NTUPLE_NAME)

    // This is to be read like this:
    // if `varname` matches `NTUPLE_NAME`, then return `TTNT_NAME` as a
    // string AND the correct member VarHandle of `getter_tau`.
    // However, the "if" is not part of the macro. See below.
    #define NAME_MATCHES_THEN_RETURN_STD(NTUPLE_NAME, TTNT_NAME) \
      (varname == #NTUPLE_NAME) { RETURN_RESULT(NTUPLE_NAME, TTNT_NAME); }

    // Shorthand for PanTau variables, where TTNT and NTuple name are the same.
    #define NAME_MATCHES_THEN_RETURN_PANTAU(NAME) \
      NAME_MATCHES_THEN_RETURN_STD(panTauCellBased_##NAME, panTauCellBased_##NAME)

    if NAME_MATCHES_THEN_RETURN_STD(CentFrac0102, tau_calcVars_corrCentFrac)
    else if NAME_MATCHES_THEN_RETURN_STD(FTrk02, tau_calcVars_corrFTrk)
    else if NAME_MATCHES_THEN_RETURN_STD(pi0_vistau_m, tau_pi0_visTau_m)
    else if NAME_MATCHES_THEN_RETURN_STD(ptRatio, tau_ptRatio)
    else if NAME_MATCHES_THEN_RETURN_STD(TrkAvgDist, tau_seedCalo_trkAvgDist)
    else if NAME_MATCHES_THEN_RETURN_STD(pi0_n, tau_pi0_n)
    else if NAME_MATCHES_THEN_RETURN_STD(IpSigLeadTrk, tau_ipSigLeadTrk)
    else if NAME_MATCHES_THEN_RETURN_STD(NTracksdrdR, tau_seedCalo_wideTrk_n)
    else if NAME_MATCHES_THEN_RETURN_STD(MassTrkSys, tau_massTrkSys)
    else if NAME_MATCHES_THEN_RETURN_STD(DrMax, tau_seedCalo_dRmax)
    else if NAME_MATCHES_THEN_RETURN_STD(TrFligthPathSig, tau_trFlightPathSig)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(nChrg0204)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(nChrg01)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(nChrg02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(nNeut0204)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(nNeut01)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(nNeut02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(massChrgSys01)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(massChrgSys02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(massChrgSys04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(massNeutSys01)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(massNeutSys02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(massNeutSys04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(visTauM01)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(visTauM02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(visTauM04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(dRmax02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(dRmax04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ipSigLeadTrk)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(trFlightPathSig)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ptRatio01)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ptRatio02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ptRatio04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ptRatioNeut01)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ptRatioNeut02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ptRatioNeut04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ptRatioChrg01)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ptRatioChrg02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(ptRatioChrg04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(fLeadChrg01)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(fLeadChrg02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(fLeadChrg04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(eFrac0102)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(eFrac0204)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(eFrac0104)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(eFracChrg0204)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(eFracChrg0104)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(eFracNeut0204)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(eFracNeut0104)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(rCal02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(rCal04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(rCalChrg02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(rCalChrg04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(rCalNeut02)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(rCalNeut04)
    else if NAME_MATCHES_THEN_RETURN_PANTAU(dRminmaxPtChrg04)
    else {
      throw std::runtime_error("Unknown variable name");
    }
    #undef NAME_MATCHES_THEN_ADD_PANTAU
    #undef NAME_MATCHES_THEN_ADD_STD
    #undef ADD_VARIABLE
  }

  vector<IDVariable>
  InitializeVarStorage( const vector<string>& varset
                      , const D3PDReader::LLHTau& getter_tau)
  {
    vector<IDVariable> result;
    for (const string& varname : varset) {
      if (IDVariable::IsNameLogarithmic(varname)) {
        IDVariable raw = LookUpVariable(IDVariable::GetLogarithmContent(varname),
                                        getter_tau);
        const IDVariable::VarHandle& getter = raw.getter;
        std::string ttnt_name = "log("+move(move(raw).ttnt_name)+")";
        result.emplace_back(ttnt_name, getter);
      }
      else {
        result.push_back(LookUpVariable(varname, getter_tau));
      }
    }
    return result;
  }
}

EfficiencyBaseCycle::EfficiencyBaseCycle()
  : CycleBase()
  , m_rLLHTaus(m_lEntryNumber)
  , m_pMVACalculator()
  , m_variables()
{
  DeclareProperty("prongmode", c_prongmode = 1);
  DeclareProperty("varset1P", c_varset1P);
  DeclareProperty("varset3P", c_varset3P);
  DeclareProperty("bdt_weight_file", c_bdt_weight_file = "");
  DeclareProperty("mlp_weight_files", c_mlp_weight_files);
  DeclareProperty("use_mlp_ensembles", c_use_mlp_ensembles = false);

  DeclareProperty("do_copy_skim_cutflow", c_copy_skim_cutflow=false);
  DeclareProperty("skim_cutflow_name", c_skim_cut_flow);
  DeclareProperty("skim_cutflow_name_raw", c_skim_cut_flow_raw);
  // DeclareProperty("weights_inputfile", c_inputfile_weights = "");
  // DeclareProperty("weights_switchOff", b_switchOffWeightTool = false);

  CutTemplateManager* pManager = CutTemplateManager::GetInstance();
  pManager->RegisterCutTemplate(
    new CutTemplate<TYPELIST_2(float, float)>(
      "PT", boost::bind(&EfficiencyBaseCycle::CheckPt, this, _1,_2,_3)
      )
    );
  pManager->RegisterCutTemplate(
    new CutTemplate<TYPELIST_0>(
      "NUMTRACK", boost::bind(&EfficiencyBaseCycle::CheckNumTrack, this, _1))
    );

  DeclareTool(new LLHCalculator(this, "LLHCalculator"));
  DeclareTool(new VariablePool<D3PDReader::LLHTau*>(this, "VariablePool"));
  // check if really needed
  DeclareTool(new CombinationHandler<D3PDReader::LLHTau*>(this, "CombinationTool"));
  DeclareTool(new Region(this,"BaselineSelection"));
  // m_WeightTool = new WeightTool();
}

bool EfficiencyBaseCycle::CheckNumTrack(float fEventWeight){
  if (!CheckRecoNumTracks(fEventWeight))
    return false;
  if (!IsData() && CheckTruthNumTracks(fEventWeight))
    return true;
  return IsData();
}

void EfficiencyBaseCycle::CalculateScores(){
  m_bdtscore = BDTScore();
  m_mlpscore = MLPScore();
}

void EfficiencyBaseCycle::InitializeBaseVariables() {
  if (IsData()) {
    m_recoPt = m_rLLHTaus.pt() / GeV;
    m_pt = m_rLLHTaus.pt() / GeV;
    m_numTrack = m_rLLHTaus.NumTrack();
    m_mu = m_rLLHTaus.mu();
  } else {
    m_pt = m_rLLHTaus.pt() / GeV;
    m_recoPt = m_rLLHTaus.pt() / GeV;
    m_truthVisEt = m_rLLHTaus.matchVisEt() / GeV;
    m_numTrack = m_rLLHTaus.NumTrack();
    m_mu = m_rLLHTaus.mu();
  }
}

void EfficiencyBaseCycle::InitializeMVAVariables() {
  for (auto& id_variable : m_variables) {
    id_variable.Update();
  }
}

void EfficiencyBaseCycle::FinishMasterInputData(const SInputData& id) throw (SError) {
  TH1* h_truthPt = 0;
  TH1* temp = 0;
  std::vector<SFile> vInputFiles = id.GetSFileIn();
  for (std::vector<SFile>::iterator it = vInputFiles.begin(); it != vInputFiles.end(); ++it) {
    // open input file and retrieve truthPt histograms
    TFile* f = TFile::Open(it->file, "READ");
    temp = (TH1*) f->Get("h_truthPt");
    if (!temp) {
      m_logger << ERROR << "Couldn't retrieve normalisation histogramm with name from file " << it->file << SLogger::endmsg;
      continue;
    }
    if (h_truthPt == 0) {
      h_truthPt = (TH1*) temp->Clone();
      h_truthPt->SetDirectory(0);
    } else{
      h_truthPt->Add(temp);
    }
    f->Close();
    delete f;
  }
  if(h_truthPt)
    Book(*h_truthPt, "");
  CopySkimCutflow(id);
}

void EfficiencyBaseCycle::StartCycle() throw (SError) {
  VariablePool<D3PDReader::LLHTau*>* m_pool = dynamic_cast<VariablePool<D3PDReader::LLHTau*>*> (GetTool("VariablePool"));
  m_pool->Initialize();
  IDVarHandle<D3PDReader::LLHTau*>* varhandle = IDVarHandle<D3PDReader::LLHTau*>::GetInstance();
  IDVarInitializer::Initialize(varhandle, &m_rLLHTaus);
  IDVarInitializer::AcquireVariables<D3PDReader::LLHTau*>(m_pool, true);
}

void EfficiencyBaseCycle::StartInputData(const SInputData& id) throw (SError) {
  // for PROOF
  VariablePool<D3PDReader::LLHTau*>* m_pool = dynamic_cast<VariablePool<D3PDReader::LLHTau*>*> (GetTool("VariablePool"));
  m_pool->Initialize();
  IDVarHandle<D3PDReader::LLHTau*>* varhandle = IDVarHandle<D3PDReader::LLHTau*>::GetInstance();
  IDVarInitializer::Initialize(varhandle, &m_rLLHTaus);
  IDVarInitializer::AcquireVariables<D3PDReader::LLHTau*>(m_pool, true);

  if (c_prongmode == 1)
    (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->Initialize(c_varset1P);
  if (c_prongmode == 2 || c_prongmode == 3)
    (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->Initialize(c_varset3P);
  // m_WeightTool->setIsData(IsData());
  // m_WeightTool->setInputFile(c_inputfile_weights);
  // m_WeightTool->switchOff(b_switchOffWeightTool);
  // m_WeightTool->ReadWeights();
  DeclareMVA();
}

void EfficiencyBaseCycle::DeclareMVA() {
  m_pMVACalculator.reset(new BDTCalculator("BDTCalculator", ""));
  std::vector<std::string> varset = c_prongmode == 1 ? c_varset1P : c_varset3P;
  // Add all variables requested in `varset` to `m_variables`.
  m_variables = ::InitializeVarStorage(varset, m_rLLHTaus);

  // Add all variables in `m_variables` to TMVA.
  // We iterate over `auto&` instead of `const auto&` because TMVA requires
  // a non-const `Float_t*`.
  for (auto& id_variable : m_variables){
    m_logger << ALWAYS << "Adding variable " << id_variable.ttnt_name
             << " to TMVA (is_logarithmic: " << id_variable.is_logarithmic
             << ")" << SLogger::endmsg;
    m_pMVACalculator->AddVariable(id_variable.ttnt_name, &id_variable.value);
  }

  SetFullPathName(c_bdt_weight_file);
  m_pMVACalculator->BookMVA("BDT", c_bdt_weight_file);

  if (c_mlp_weight_files.empty()) {
    m_logger << FATAL << "No MLP classifiers specified!" << SLogger::endmsg;
    throw std::runtime_error("No MLP classifiers specified!");
  }
  else if (c_use_mlp_ensembles) {
    size_t i = 0;
    // `file` must not be const because SetFullPathName is modifying.
    for (std::string& file : c_mlp_weight_files) {
      const std::string name = "MLP" + std::to_string(i);
      m_mlpnames.push_back(name);
      SetFullPathName(file);
      m_pMVACalculator->BookMVA(name, file);
      m_logger << DEBUG << "Read classifier " << name
               << " from file " << file << SLogger::endmsg;
      ++i;
    }
  }
  else if (c_mlp_weight_files.size() != 1) {
    m_logger << FATAL << "Expected 1 MLP classifier got "
             << c_mlp_weight_files.size() << " instead." << SLogger::endmsg;
    throw std::runtime_error("Wrong number of MLP classifiers specified!");
  }
  else {
    m_mlpnames.push_back("MLP");
    SetFullPathName(c_mlp_weight_files[0]);
    m_pMVACalculator->BookMVA("MLP", c_mlp_weight_files[0]);
  }
}

float EfficiencyBaseCycle::BDTScore() const {
  return m_pMVACalculator->EvaluateMVA("BDT");
}

float EfficiencyBaseCycle::MLPScore() const {
  double score = 0.0;
  for (const auto& name : m_mlpnames) {
    score += m_pMVACalculator->EvaluateMVA(name);
  }
  return score / m_mlpnames.size();
}

void EfficiencyBaseCycle::StartInputFile(const SInputData&) throw (SError) {
  m_rLLHTaus.ReadFrom(GetInputTree(c_tree_name.c_str()));
}

bool EfficiencyBaseCycle::CheckPt(float fWeight, float fPtMin, float fPtMax) {
  return (m_recoPt > fPtMin && m_recoPt < fPtMax);
}

bool EfficiencyBaseCycle::CheckTruthNumTracks(float fEventWeight) {
  /*
    truth num track is matched truth num track
  */
  if (c_prongmode == 1)
    return (m_rLLHTaus.TruthNumTrack() == 1);
  else if ((c_prongmode == 2 || c_prongmode == 3))
    return (m_rLLHTaus.TruthNumTrack() == 3);
  else
    return false;
}

bool EfficiencyBaseCycle::CheckRecoNumTracks(float fEventWeight) {
  if (c_prongmode == 1)
    return (m_rLLHTaus.NumTrack() == 1);
  else if (c_prongmode == 3)
    return (m_rLLHTaus.NumTrack() == 3);
  else if (c_prongmode == 2)
    return (m_rLLHTaus.NumTrack() == 2 || m_rLLHTaus.NumTrack() == 3);
  else
    return false;
}

void EfficiencyBaseCycle::CopySkimCutflow(const SInputData& id){
  if(c_copy_skim_cutflow){
    TH1* h_skim = 0;
    TH1* h_skim_raw = 0;
    TH1* temp = 0;
    std::vector<SFile> vInputFiles = id.GetSFileIn();

    for(std::vector<SFile>::iterator it = vInputFiles.begin(); it != vInputFiles.end(); ++it)
      {
  	// open input file and retrieve cut flow histograms
  	TFile* f = TFile::Open(it->file,"READ");
  	temp = (TH1*)f->Get(c_skim_cut_flow.c_str());
  	if(!temp)
  	  {
  	    m_logger << ERROR << "Couldn't retrieve skim cutflow with name " << c_skim_cut_flow << " from file " << it->file << SLogger::endmsg;
  	    continue;
  	  }
  	if(h_skim == 0)
  	  {
  	    h_skim = (TH1*)temp->Clone();
  	    h_skim->SetDirectory(0);
  	  }
  	else
  	  h_skim->Add(temp);

  	temp = (TH1*)f->Get(c_skim_cut_flow_raw.c_str());
  	if(!temp)
  	  {
  	    m_logger << ERROR << "Couldn't retrieve skim cutflow with name " << c_skim_cut_flow << " from file " << it->file << SLogger::endmsg;
  	    continue;
  	  }
  	if(h_skim_raw == 0)
  	  {
  	    h_skim_raw = (TH1*)temp->Clone();
  	    h_skim_raw->SetDirectory(0);
  	  }
  	else
  	  h_skim_raw->Add(temp);

  	f->Close();
  	delete f;
      }

    Book(*h_skim,"SkimSelection");
    Book(*h_skim_raw,"SkimSelection");
  }
}

#endif // EfficiencyBaseCycle_CXX
#endif //SKIM
