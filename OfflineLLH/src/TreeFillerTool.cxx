#ifndef SKIM
#ifndef TreeFillerTool_CXX
#define TreeFillerTool_CXX

// STL
#include <vector>
#include <iostream>

// custom header
#include "TreeFillerTool.h"

//ROOT includes
#include "TH2.h"
#include "TH3.h"

#include "TauCommonUtilities.h"
#include "IDVarCalculator.h"

TreeFillerTool::TreeFillerTool(CycleBase* pParent,const char* sName):ToolBase(pParent,sName){
  // DeclareProperty("pileup_correction_inputfile", c_pileup_correction_file = "");
  // DeclareProperty("pileup_correction_switchOff", c_pileup_correction_switchOff = false);

  //  m_pileUpCorrectionTool = new PileUpCorrectionTool();
}
void TreeFillerTool::Initialize(std::map<std::string,
				Variable<D3PDReader::AnalysisTau*>* > map){
  //  m_pIDVarCalculator = new CellBasedCalculator();
  // m_pileUpCorrectionTool->setInputFile(c_pileup_correction_file);
  // m_pileUpCorrectionTool->switchOff(c_pileup_correction_switchOff);
  // m_pileUpCorrectionTool->readInput();
  
  DeclareVariable(m_runNum,"RunNumber","MiniTree");
  DeclareVariable(m_evtNum,"EventNumber","MiniTree");
  DeclareVariable(m_pt,"pt","MiniTree");
  DeclareVariable(m_et,"Et","MiniTree");
  DeclareVariable(m_eta,"eta","MiniTree");
  DeclareVariable(m_phi,"phi","MiniTree");
  DeclareVariable(m_numTrack,"NumTrack","MiniTree");
  DeclareVariable(m_default_llhscore,"DefaultLLHScore","MiniTree");
  DeclareVariable(m_default_llh_loose,"DefaultLLHLoose","MiniTree");
  DeclareVariable(m_default_llh_medium,"DefaultLLHMedium","MiniTree");
  DeclareVariable(m_default_llh_tight,"DefaultLLHTight","MiniTree");
  DeclareVariable(m_truth_pt,"TruthPt","MiniTree");
  DeclareVariable(m_truth_eta,"TruthEta","MiniTree");
  DeclareVariable(m_truth_phi,"TruthPhi","MiniTree");
  DeclareVariable(m_truth_numTrack,"TruthNumTrack","MiniTree");
  DeclareVariable(m_BDTJet_Score,"BDTScore","MiniTree");
  DeclareVariable(m_default_bdt_loose,"DefaultBDTLoose","MiniTree");
  DeclareVariable(m_default_bdt_medium,"DefaultBDTMedium","MiniTree");
  DeclareVariable(m_default_bdt_tight,"DefaultBDTTight","MiniTree");
  DeclareVariable(m_nvertices,"NVertices","MiniTree");
  DeclareVariable(m_numGoodVertices,"NumGoodVertices","MiniTree");
  DeclareVariable(m_mu,"mu","MiniTree");
  DeclareVariable(m_matchVisEt, "matchVisEt", "MiniTree");

  std::map<std::string, Variable<D3PDReader::AnalysisTau*>* >::iterator it = map.begin();
  for(; it!= map.end(); ++it){
    double dummy = 0.;
    m_map_llhScore[it->first] = dummy;
    DeclareVariable(m_map_llhScore[it->first], (it->first).c_str(), "MiniTree");

    m_map_varValue[("Var_" + it->first)] = dummy;
    DeclareVariable(m_map_varValue[("Var_" + it->first)], ("Var_"+it->first).c_str(), "MiniTree");
  }
}

void TreeFillerTool::Fill(D3PDReader::AnalysisTau* pTau,
			  std::map<std::string, Variable<D3PDReader::AnalysisTau*>* > map,
			  const D3PDReader::AnalysisEventInfo& rEventInfo){
  m_runNum = rEventInfo.RunNumber();
  m_evtNum = rEventInfo.EventNumber();
  m_pt = pTau->TLV().Pt();
  m_et = pTau->TLV().Et();
  m_eta = pTau->TLV().Eta();
  m_phi = pTau->TLV().Phi();
  m_numTrack = pTau->numTrack();
  m_default_llhscore = pTau->SafeLikelihood();
  m_BDTJet_Score = pTau->BDTJetScore();
  m_default_llh_loose = pTau->tauLlhLoose();
  m_default_llh_medium = pTau->tauLlhMedium();
  m_default_llh_tight = pTau->tauLlhTight();
  m_default_bdt_loose = pTau->JetBDTSigLoose();
  m_default_bdt_medium = pTau->JetBDTSigMedium();
  m_default_bdt_tight = pTau->JetBDTSigTight();
  m_nvertices = rEventInfo.mu();
  m_mu = rEventInfo.mu();
  m_numGoodVertices = 1.;//pTau->evt_calcVars_numGoodVertices();

  if(IsData()){
    m_truth_pt = -1111.;
    m_truth_eta = -1111.;
    m_truth_phi = -1111.;
    m_truth_numTrack = -1111;
  }
  else{
    m_truth_pt = pTau->matchPt();
    m_truth_eta = pTau->matchEta();
    m_truth_phi = pTau->matchPhi();
    m_truth_numTrack = pTau->matchNProng();
#ifdef TBI
    m_truth_eta = pTau->matchEta();
    m_truth_phi = pTau->matchPhi();
#endif
    m_matchVisEt = pTau->matchVisEt();
  }  
  std::map<std::string, Variable<D3PDReader::AnalysisTau*>* >::iterator it = map.begin();
  
  for(; it!= map.end(); ++it){
    // if(std::string(it->first).compare("panTauCellBased_rCal04") == 0)
    //   std::cout << "ratio: " << it->second->GetLHRatio() << std::endl;
    m_map_llhScore[it->first] = it->second->GetLHRatio();
    // m_map_varValue[("Var_" + it->first)] = m_pileUpCorrectionTool->getCorrectedVar(it->first,
    // 										   pTau,
    // 										   m_mu,
    //
    m_map_varValue[("Var_" + it->first)] = it->second->GetValue(pTau);
  }
  GetOutputMetadataTree("MiniTree")->Fill();
}

#endif // TreeFillerTool_CXX
#endif // SKIM 
