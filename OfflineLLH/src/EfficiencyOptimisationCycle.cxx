#ifndef SKIM
#ifndef EfficiencyOptimisationCycle_CXX
#define EfficiencyOptimisationCycle_CXX

#include "EfficiencyOptimisationCycle.h"
#include "Region.h"
#include "TauCommonUtilities.h"
#include "Constants.h"
ClassImp(EfficiencyOptimisationCycle);

using namespace AnalysisUtilities_Constants;

EfficiencyOptimisationCycle::EfficiencyOptimisationCycle() :
  EfficiencyBaseCycle() { }

void EfficiencyOptimisationCycle::AnalyseEvent(const SInputData& id, double dWeight) throw (SError) {
  if(m_rLLHTaus.EventNumber() % 2 == 0)
    throw SError(SError::SkipEvent);
  InitializeBaseVariables();
  InitializeMVAVariables();
  //  m_fweight = m_WeightTool->getWeight(m_rLLHTaus.NumTrack(), m_recoPt, m_mu);    
  dynamic_cast<Region*> (GetTool("BaselineSelection"))->IsInRegion(m_fEventWeight, true);

  CalculateScores();
  FillScores();
  // HistsForEfficiency();
  if (!IsData()) {
    m_pTruthVsRecoPt->Fill(m_truthVisEt, m_recoPt); 
  }

  dynamic_cast<TH1F*> (Hist("h_numTrack"))->Fill(m_numTrack);    
}

/*
void EfficiencyOptimisationCycle::HistsForEfficiency(){
  const std::string prong = (m_rLLHTaus.NumTrack()==1) ? "1P" : "3P";
  if(m_rLLHTaus.DefaultBDTLoose() == 1) {
    Hist(("h_passed_BDT_loose_"+prong+"_truth_pt").c_str())->Fill(m_rLLHTaus.matchVisEt() / GeV);
  }
  if(m_rLLHTaus.DefaultBDTMedium() == 1) {
    Hist(("h_passed_BDT_medium_"+prong+"_truth_pt").c_str())->Fill(m_rLLHTaus.matchVisEt() / GeV);
  }
  if(m_rLLHTaus.DefaultBDTTight() == 1) {
    Hist(("h_passed_BDT_tight_"+prong+"_truth_pt").c_str())->Fill(m_rLLHTaus.matchVisEt() / GeV);
  }
}
*/

void EfficiencyOptimisationCycle::FillScores() {
  Hist("h_MLPScore")->Fill(m_mlpscore);
  Hist("h_BDTScore")->Fill(m_bdtscore);
  Hist("h_defaultBDTScore")->Fill(m_rLLHTaus.BDTScore());
  double pt = IsData() ? m_recoPt : m_truthVisEt;
  
  dynamic_cast<TH2F*> (Hist("h_pT_MLPScore"))->Fill(pt,
  						    m_mlpscore);
  dynamic_cast<TH2F*> (Hist("h_pT_BDTScore"))->Fill(pt,
						    m_bdtscore);
  dynamic_cast<TH2F*> (Hist("h_pT_defaultBDTScore"))->Fill(pt,
							    m_rLLHTaus.BDTScore());
}

void EfficiencyOptimisationCycle::StartCycle() throw(SError){
  EfficiencyBaseCycle::StartCycle();
  m_pTruthVsRecoPt = 0; //dynamic_cast<TH2F*> (Hist("h_TruthpT_OfflinepT"));
}

void EfficiencyOptimisationCycle::StartInputData(const SInputData& id) throw (SError) {
  EfficiencyBaseCycle::StartInputData(id);

  Book(TH1F("h_numTrack", "number of tracks;number of tracks;a.u", 6, -0.5, 5.5));
  Book(TH1F("h_BDTScore", "BDT Score", 1000, 0., 1.));
  Book(TH1F("h_MLPScore", "MLP Score", 1000, 0., 1.));
  Book(TH1F("h_defaultBDTScore", "default BDT Score", 1000, 0., 1.));
  Book(TH2F("h_pT_BDTScore", "; pt; BDT Score", 1000, 0., 1000., 5000, 0., 1.));
  Book(TH2F("h_pT_MLPScore", "; pt; MLP Score", 1000, 0., 1000., 5000, 0., 1.));
  Book(TH2F("h_pT_defaultBDTScore", "; pt; default BDT Score", 1000, 0., 1000., 1000, 0., 1.));
  Book(TH2F("h_TruthpT_OfflinepT", "; truth pT; reco pT", 1000, 0., 1000., 1000, 0., 1000.));

  /* Commented out, just like HistsForEfficiency
  for(const std::string& sProng : {"1P", "3P"}) {
    Book(TH1F(("truth_tau_vis_pt_"+sProng).c_str(),";trueTau pt;Normalised",500,0.,500.));
    Book(TH1F(("h_passed_BDT_loose_"+sProng+"_truth_pt").c_str(), ";tau pt; a.u.", 500, 0., 500.));
    Book(TH1F(("h_passed_BDT_medium_"+sProng+"_truth_pt").c_str(), ";tau pt; a.u.", 500, 0., 500.));
    Book(TH1F(("h_passed_BDT_tight_"+sProng+"_truth_pt").c_str(), ";tau pt; a.u.", 500, 0., 500.));
  }
  */
  m_pTruthVsRecoPt = dynamic_cast<TH2F*> (Hist("h_TruthpT_OfflinepT"));
}
#endif // EfficiencyOptimisationCycle_CXX
#endif //SKIM
