#ifndef SKIM
#ifndef EfficiencyEvaluationCycle_CXX
#define EfficiencyEvaluationCycle_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

//STL includes
#include <iterator>
#include <vector>
#include <map>

//ROOT includes
#include "TFile.h"
#include "TH2F.h"
#include "CutTemplateManager.h"

#include "Utilities.h"
// TauCommon include(s)

// LLHTool include(s)
#include "EfficiencyEvaluationCycle.h"
#include "Region.h"
#include "VariablePool.h"
#include "IDVarHandle.h"
#include "IDVarInitializer.h"
// #include "CutCalculator.h"
#include "CombinationHandler.h"
#include "BDTCalculator.h"
#include "TauCommonUtilities.h"
#include "Constants.h"
ClassImp(EfficiencyEvaluationCycle);

using namespace Utilities;
using namespace AnalysisUtilities_Constants;

EfficiencyEvaluationCycle::EfficiencyEvaluationCycle() :
  EfficiencyBaseCycle() {
  // recalculated
  DeclareProperty("bdt_cuts_inputfile", c_inputfile_bdt_cuts = "");
  DeclareProperty("mlp_cuts_inputfile", c_inputfile_mlp_cuts = "");
  DeclareProperty("bdt_allcuts_inputfile", c_inputfile_bdt_allcuts = "");
  DeclareProperty("mlp_allcuts_inputfile", c_inputfile_mlp_allcuts = "");
  DeclareProperty("bdt_alldefcuts_inputfile", c_inputfile_bdt_alldefcuts = "");
  // default recalculated cuts
  DeclareProperty("bdt_reccuts_inputfile", c_inputfile_bdt_reccuts = "");

  DeclareProperty("weights_inputfile", c_inputfile_weights = "");
  // DeclareProperty("weights_switch_off", b_switchOffWeightTool = false);

  //  m_WeightTool = new WeightTool();

  efficiencies["0"]=0.;
  // 84 instead of 77 for multi-prong candidates?
  for (unsigned int int_eff=1; int_eff < 77; ++int_eff) {
    char eff_key[4]; // Three digits plus null byte.
    sprintf(eff_key, "%03d", int_eff);
    efficiencies[eff_key] = int_eff/100.;
  }
}
  
void EfficiencyEvaluationCycle::AnalyseEvent(const SInputData& id, double dWeight) throw (SError) {
  if(m_rLLHTaus.EventNumber() % 2 == 0)
    throw SError(SError::SkipEvent);

  InitializeBaseVariables();
  InitializeMVAVariables();
  //  m_fweight = m_WeightTool->getWeight(m_rLLHTaus.NumTrack(), m_recoPt, m_mu);    
  //  m_fweight = 1.;
  dynamic_cast<Region*> (GetTool("BaselineSelection"))->IsInRegion(m_fEventWeight, true);

  m_fillPt = IsData() ? m_recoPt : m_truthVisEt;

  CalculateScores();
  FillPassed();
  FillPassedAllCuts();
  FillTotal();
  FillScores(); // TODO check if needed
  dynamic_cast<TH1F*> (Hist("h_numTrack"))->Fill(m_numTrack);

  // CalculateVariables();
  // TTree* pTree = GetOutputMetadataTree("MiniTree");
  // pTree->Fill();
}
  
void EfficiencyEvaluationCycle::FillScores() {

  const double pt = IsData() ? m_recoPt : m_truthVisEt;
  const float weight = IsData() ? GetPtWeight(m_recoPt) : GetMuWeight(m_mu);
  //~ weight = 1.;
  Hist("h_BDTScore")->Fill(m_bdtscore, weight);
  Hist("h_MLPScore")->Fill(m_mlpscore, weight);
  Hist("h_defaultBDTScore")->Fill(m_rLLHTaus.BDTScore(), weight);
  
  dynamic_cast<TH2F*> (Hist("h_pT_BDTScore"))->Fill(pt, m_bdtscore);
  dynamic_cast<TH2F*> (Hist("h_pT_MLPScore"))->Fill(pt, m_mlpscore);

  /*
  dynamic_cast<TH2F*> (Hist("h_pT_BDTScore"))->Fill(pt, m_bdtscore, weight);
  dynamic_cast<TH2F*> (Hist("h_pT_MLPScore"))->Fill(pt, m_mlpscore, weight);
  dynamic_cast<TH2F*> (Hist("h_pT_default_BDTScore"))->Fill(pt, m_rLLHTaus.BDTScore(), weight);
  dynamic_cast<TH3F*> (Hist("h_pT_BDTScore_mu"))->Fill(pt, m_bdtscore, m_mu, weight);
  dynamic_cast<TH3F*> (Hist("h_pT_MLPScore_mu"))->Fill(pt, m_mlpscore, m_mu, weight);
  dynamic_cast<TH3F*> (Hist("h_pT_default_BDTScore_mu"))->Fill(pt, m_rLLHTaus.BDTScore(), m_mu, weight);
  */
}

void EfficiencyEvaluationCycle::FillPassed() {
  if (CalculateCut(tg_loose_bdt_cuts) < m_bdtscore)
    FillPassed("BDT_loose");
  if (CalculateCut(tg_medium_bdt_cuts) < m_bdtscore)
    FillPassed("BDT_medium");
  if (CalculateCut(tg_tight_bdt_cuts) < m_bdtscore)
    FillPassed("BDT_tight");

  if (CalculateCut(tg_loose_mlp_cuts) < m_mlpscore)
    FillPassed("MLP_loose");
  if (CalculateCut(tg_medium_mlp_cuts) < m_mlpscore)
    FillPassed("MLP_medium");
  if (CalculateCut(tg_tight_mlp_cuts) < m_mlpscore)
    FillPassed("MLP_tight");
  
  // default flags
  if (m_rLLHTaus.DefaultBDTLoose() == 1)
    FillPassed("defaultBDT_loose");    
  if (m_rLLHTaus.DefaultBDTMedium() == 1) 
    FillPassed("defaultBDT_medium");    
  if (m_rLLHTaus.DefaultBDTTight() == 1)
    FillPassed("defaultBDT_tight");    
}

void EfficiencyEvaluationCycle::FillPassed(const std::string& sWorkingPoint){
  //TODO: check
  Hist(("h_passed_" + sWorkingPoint + "_pt").c_str())->Fill(m_fillPt);
  Hist(("h_passed_" + sWorkingPoint + "_mu").c_str())->Fill(m_mu);
}

void EfficiencyEvaluationCycle::FillPassedAllCuts(){
  for (auto it : m_mCutsBDT){
    if (CalculateCut(it.second) < m_bdtscore)
      FillPassedAllCuts("BDT", efficiencies[it.first]);
  }
  FillPassedAllCuts("BDT",0.77);

  for (auto it : m_mCutsMLP){
    if (CalculateCut(it.second) < m_mlpscore)
      FillPassedAllCuts("MLP", efficiencies[it.first]);
  }
  FillPassedAllCuts("MLP",0.77);

 for (auto it : m_mCutsDefaultBDT){
    if (CalculateCut(it.second) < m_rLLHTaus.BDTScore())
      FillPassedAllCuts("defaultBDT", efficiencies[it.first]);
  }
 FillPassedAllCuts("defaultBDT",0.77);
}

void EfficiencyEvaluationCycle::FillPassedAllCuts(const std::string& algo, const float& efficiency){
  dynamic_cast<TH2F*> (Hist(("h_passed_" + algo + "_pt").c_str()))->Fill(efficiency,m_fillPt);
}

void EfficiencyEvaluationCycle::FillTotal() {
  //TODO: check
  Hist("h_total_pt")->Fill(m_fillPt);
  Hist("h_total_mu")->Fill(m_mu);  
}

double EfficiencyEvaluationCycle::CalculateCut(TGraph* cuts) {
  return cuts->Eval(m_rLLHTaus.pt() / GeV);
  // return cuts->Eval(m_rLLHTaus.matchVisEt() / GeV);
}

void EfficiencyEvaluationCycle::ReadCuts(){
  std::string prongdir;
  if (c_prongmode == 1) 
    prongdir = "1prong/";  
  else
    prongdir = "3prong/";

  // recalculated
  f_bdt = TFile::Open(c_inputfile_bdt_cuts.c_str());
  tg_loose_bdt_cuts = (TGraph*) f_bdt->Get((prongdir + "loose").c_str());
  tg_medium_bdt_cuts = (TGraph*) f_bdt->Get((prongdir + "medium").c_str());
  tg_tight_bdt_cuts = (TGraph*) f_bdt->Get((prongdir + "tight").c_str());
  assert(f_bdt != 0);
  assert(tg_loose_bdt_cuts != 0);
  assert(tg_medium_bdt_cuts != 0);
  assert(tg_tight_bdt_cuts != 0);

  f_mlp = TFile::Open(c_inputfile_mlp_cuts.c_str());
  tg_loose_mlp_cuts = (TGraph*) f_mlp->Get((prongdir + "loose").c_str());
  tg_medium_mlp_cuts = (TGraph*) f_mlp->Get((prongdir + "medium").c_str());
  tg_tight_mlp_cuts = (TGraph*) f_mlp->Get((prongdir + "tight").c_str());
  assert(f_mlp != 0);
  assert(tg_loose_mlp_cuts != 0);
  assert(tg_medium_mlp_cuts != 0);
  assert(tg_tight_mlp_cuts != 0);

  // recalculated default
  f_bdt_rec = TFile::Open(c_inputfile_bdt_reccuts.c_str());
  tg_loose_bdt_reccuts = (TGraph*) f_bdt_rec->Get((prongdir + "loose").c_str());
  tg_medium_bdt_reccuts = (TGraph*) f_bdt_rec->Get((prongdir + "medium").c_str());
  tg_tight_bdt_reccuts = (TGraph*) f_bdt_rec->Get((prongdir + "tight").c_str());
  assert(f_bdt_rec != 0);
  assert(tg_loose_bdt_reccuts != 0);
  assert(tg_medium_bdt_reccuts != 0);
  assert(tg_tight_bdt_reccuts != 0);
}

void EfficiencyEvaluationCycle::ReadAllCuts(){
  std::string prongdir;
  if (c_prongmode == 1) 
    prongdir = "1prong/";  
  else
    prongdir = "3prong/";

  // recalculated
  f_bdt_all = TFile::Open(c_inputfile_bdt_allcuts.c_str());
  f_mlp_all = TFile::Open(c_inputfile_mlp_allcuts.c_str());
  f_defbdt_all = TFile::Open(c_inputfile_bdt_alldefcuts.c_str());
  for(auto it : efficiencies){
    m_mCutsBDT[it.first] = (TGraph*) f_bdt_all->Get((prongdir + it.first).c_str());
    m_mCutsMLP[it.first] = (TGraph*) f_mlp_all->Get((prongdir + it.first).c_str());
    m_mCutsDefaultBDT[it.first] = (TGraph*) f_defbdt_all->Get((prongdir + it.first).c_str());
  }
}

void EfficiencyEvaluationCycle::ReadWeights() {
  TFile* fw = TFile::Open(c_inputfile_weights.c_str());
  m_hPtWeights_1p = (TH1F*) fw->Get("1prong/pt_weights");
  m_hPtWeights_3p = (TH1F*) fw->Get("3prong/pt_weights");
  m_hMuWeights_1p = (TH1F*) fw->Get("1prong/mu_weights");
  m_hMuWeights_3p = (TH1F*) fw->Get("3prong/mu_weights");
}

float EfficiencyEvaluationCycle::GetPtWeight(float pt) {
  if(c_prongmode == 1) {
      return m_hPtWeights_1p->GetBinContent(m_hPtWeights_1p->FindBin(pt));
  } else if(c_prongmode == 2 || c_prongmode == 3) {
    return m_hPtWeights_3p->GetBinContent(m_hPtWeights_3p->FindBin(pt));
  } else {
    return 1;
  }
}

float EfficiencyEvaluationCycle::GetMuWeight(float mu) {
  if (0. < mu && mu < 35.) {
    if(c_prongmode == 1) {
      return m_hMuWeights_1p->GetBinContent(m_hMuWeights_1p->FindBin(mu));
    } else if(c_prongmode == 2 || c_prongmode == 3) {
      return m_hMuWeights_3p->GetBinContent(m_hMuWeights_3p->FindBin(mu));
    } else {
      return 1;
    }
  }
  else {
    return 1;
  }
}

void EfficiencyEvaluationCycle::StartCycle() throw (SError) {
  EfficiencyBaseCycle::StartCycle();
  ReadCuts();
  ReadAllCuts();
  ReadWeights();
}

void EfficiencyEvaluationCycle::StartInputData(const SInputData& id) throw (SError) {
  EfficiencyBaseCycle::StartInputData(id);
  //TODO: refactor using loops - one for loose medium tight, and one for LLH and BDT
  //TODO: refactor use members to define binnings -> easier to change if required

  ReadCuts();		       	// for PROOF
  ReadAllCuts();		       	// for PROOF
  // m_WeightTool->switchOff(b_switchOffWeightTool);
  // m_WeightTool->setIsData(IsData());
  // m_WeightTool->setInputFile(c_inputfile_weights);
  // m_WeightTool->ReadWeights();

  ReadWeights();

  // Book histograms.
  std::vector<std::string> methods = { "BDT", "MLP", "defaultBDT"};
  std::vector<std::string> lmt_names = { "loose", "medium", "tight"};
  std::string name, title;
  for (const std::string& method : methods) {
    // Over-all score histogram.
    name = "h_"+method+"Score";
    title = method+" Score";
    Book(TH1F(name.c_str(), title.c_str(), 1000, 0., 1.));
    // pT/Score histogram
    name = "h_pT_"+method+"Score";
    title = "; pt; "+method+" Score";
    Book(TH2F(name.c_str(), title.c_str(), 1000, 0., 1000., 1000, 0., 1.));
    // Working point/pT histogram.
    name = "h_passed_"+method+"_pt";
    Book(TH2F(name.c_str(), "efficiency vs pt;efficiency;pt", 77,0.005,0.775,1000,0.,1000.));
    // pT and mu histograms for three named working points.
    for (const std::string& wp : lmt_names) {
      name = "h_passed_"+method+"_"+wp+"_pt";
      Book(TH1F(name.c_str(), "pt", 1000, 0., 1000.));
      name = "h_passed_"+method+"_"+wp+"_mu";
      Book(TH1F(name.c_str(), "mu", 40, -0.5, 39.5));
    }
  }
  Book(TH1F("h_total_pt", "pt", 1000, 0., 1000.));
  Book(TH1F("h_total_mu", "mu", 40, -0.5, 39.5));
  Book(TH1F("h_numTrack", "number of tracks;number of tracks;a.u", 6, -0.5, 5.5));

}

void EfficiencyEvaluationCycle::CalculateVariables(){
  m_default_bdtscore = m_rLLHTaus.BDTScore();
}
  
void EfficiencyEvaluationCycle::DeclareVariables(){
  DeclareVariable(m_fillPt, "fillPt", "MiniTree");
  DeclareVariable(m_recoPt, "recoPt", "MiniTree");
  DeclareVariable(m_truthVisEt, "truthVisEt", "MiniTree");
  DeclareVariable(m_mu, "mu", "MiniTree");
  DeclareVariable(m_mlpscore, "MLPScore", "MiniTree");
  DeclareVariable(m_bdtscore, "BDTScore", "MiniTree");
  DeclareVariable(m_default_bdtscore, "defaultBDTScore", "MiniTree");
}

#endif // EfficiencyEvaluationCycle_CXX
#endif //SKIM
