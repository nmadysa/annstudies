#ifndef SKIM
#ifndef PDFCycle_CXX
#define PDFCycle_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

// core include(s)
#include "Region.h"
#include "Utilities.h"

// custom include(s)
#include "PDFCycle.h"
#include "PDFPlotter.h"
// #include "LLHCommonUtilities.h"

#include "TFile.h"

ClassImp(PDFCycle);

PDFCycle::PDFCycle():
  AnalysisLLHCommon()
{
  DeclareTool(new PDFPlotter(this,"PDFPlotter"));
  DeclareProperty("trFlightPathSig_switchOff", b_switchOffTrFlightPathSig = false);
}

PDFCycle::~PDFCycle()
{
  m_oTauCreator.ClearAllObjects();
}

void PDFCycle::AnalyseEvent(const SInputData& id,double dWeight) throw(SError)
{
  m_oTauCreator.ClearAllObjects();
  m_oTauCreator.CreateObjects(m_rTauCollection.n(),m_rTauCollection,m_vTaus);
  
  //consider only even events for pdfs
  if(m_rEventInfo.EventNumber() % 2 != 0)
    throw SError(SError::SkipEvent);

  // reset event weight
  m_fEventWeight = 1;
  
  // sort objects according to decreasing pt
  std::sort(m_vTaus.begin(),m_vTaus.end(),Particle::pt_comp);

  SelectPhysicsObjects(m_fEventWeight);  
  dynamic_cast<Region*>(GetTool("BaselineSelection"))->IsInRegion(m_fEventWeight,true);

  //match offline to truth
  // if(!IsData()) LLHCommonUtilities::truthmatching(m_vTaus); 
  if(!IsData()){
    TightTruthMatch(m_vTaus);
  }
  else{
    TightRecoNumTrack(m_vTaus);
  }

  if(!b_switchOffTrFlightPathSig)
    CheckTrFlightPathSig(m_vTaus);
  
  dynamic_cast<PDFPlotter*>(GetTool("PDFPlotter"))->PlotDistributions(m_vTaus, m_rEventInfo.mu(), m_fEventWeight);
}

void PDFCycle::StartInputFile(const SInputData&) throw(SError)
{
  m_rEventInfo.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_rTauCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
}

void PDFCycle::CheckTrFlightPathSig(std::vector<D3PDReader::AnalysisTau*>& vTaus) {
  vTaus.erase(remove_if(vTaus.begin(),
			vTaus.end(),
			!boost::bind<bool>(&PDFCycle::CheckTrFlightPathSig, this, _1)),
	      vTaus.end());
}

bool PDFCycle::CheckTrFlightPathSig(D3PDReader::AnalysisTau* const pTau) {
  if(pTau->numTrack() == 1)
    return true;
  return (-1000 < pTau->trFlightPathSig());
}

#endif // PDFCycle_CXX
#endif // SKIM
