#ifdef TBF
Need to find appropriate name for this class :)
#endif
#ifndef SKIM
#ifndef NtupleCycle_CXX
#define NtupleCycle_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

// core include(s)
#include "Region.h"
#include "Utilities.h"

// TauCommon include(s)
#include "TauSelector.h"
#include "TruthTauSelector.h"
#include "TauCommonUtilities.h"

// LLH include(s)
#include "NtupleCycle.h"
#include "VariablePlotter.h"
#include "LLHCommon.h"
#include "IDVarHandle.h"
#include "IDVarInitializer.h"
#include "TreeFillerTool.h"
#include "LLHCalculator.h"

#include <iostream>
#include "Constants.h"

ClassImp(NtupleCycle);

using namespace AnalysisUtilities_Constants;

NtupleCycle::NtupleCycle():
  AnalysisLLHCommon()
{
  DeclareTool(new Region(this, "NtupleRegion"));
  // DeclareTool(new Region(this, "BaselineSelection"));
  DeclareTool(new LLHCalculator(this, "LLHCalculator"));
  DeclareTool(new TreeFillerTool(this, "TreeFiller"));
  DeclareTool(new VariablePool<D3PDReader::AnalysisTau*>(this, "VariablePool"));

  DeclareProperty("pileup_correction_inputfile", c_pileup_correction_file = "");
  DeclareProperty("pileup_correction_switchOff", c_pileup_correction_switchOff = false);

  //m_pIDVarCalculator = new CellBasedCalculator();
  m_pIDVarCalculator = new EFlowRecCalculator();
}

NtupleCycle::~NtupleCycle()
{
  // clear memory
  m_oTruthTauCreator.ClearAllObjects();
  m_oTauCreator.ClearAllObjects();
  #ifdef TBF
  delete calculator;
  #endif
}

void NtupleCycle::AnalyseEvent(const SInputData& id,double dWeight) throw(SError)
{
  // clear memory
  m_oTruthTauCreator.ClearAllObjects();
  m_oTauCreator.ClearAllObjects();
  // create analysis objects for current event
  if(!IsData()) 
    m_oTruthTauCreator.CreateObjects(m_rTruthTauCollection.n(), 
				     m_rTruthTauCollection,
				     m_vTruthTaus);
  m_oTauCreator.CreateObjects(m_rTauCollection.n(), 
			      m_rTauCollection, 
			      m_vTaus);

  // reset event weight
  m_fEventWeight = 1;

  //consider only odd events for ntuples
  if(m_rEventInfo.EventNumber() % 2 == 0)
    throw SError(SError::SkipEvent);

  // sort objects according to decreasing pt
  if(!IsData()) std::sort(m_vTruthTaus.begin(),
			  m_vTruthTaus.end(),
			  Particle::pt_comp);
  std::sort(m_vTaus.begin(),
	    m_vTaus.end(),
	    Particle::pt_comp);

  SelectPhysicsObjects(m_fEventWeight);

  dynamic_cast<Region*>(GetTool("BaselineSelection"))->IsInRegion(m_fEventWeight,true);

  if(!IsData()){
    FillTruthNormalisationHist();
    ApplyTruthMatch();
  }
  else{
    LooserRecoNumTrack(m_vTaus);
  }
  // HistsForEfficiency();
  FillVariables();
}

void NtupleCycle::HistsForEfficiency(){
  for(auto truthtau : m_vTruthTaus) {
    const std::string prong = (truthtau->nProng()==1) ? "1P" : "3P";
    Hist(("truth_tau_vis_pt_"+prong).c_str())->Fill((truthtau)->TLV().Pt() / GeV);
  }

  for(auto tau : m_vTaus) {
    const std::string prong = (tau->numTrack()==1) ? "1P" : "3P";
    if(tau->JetBDTSigLoose() == 1) {
      Hist(("h_passed_BDT_loose_"+prong+"_truth_pt").c_str())->Fill((tau)->matchVisEt() / GeV);
    }
    if(tau->JetBDTSigMedium() == 1) {
      Hist(("h_passed_BDT_medium_"+prong+"_truth_pt").c_str())->Fill((tau)->matchVisEt() / GeV);
    }
    if(tau->JetBDTSigTight() == 1) {
      Hist(("h_passed_BDT_tight_"+prong+"_truth_pt").c_str())->Fill((tau)->matchVisEt() / GeV);
    }
    else
      continue;
  }      
}

void NtupleCycle::FillVariables(){
  //TODO: refactor make this cast a member to avoid event based casting (slow)
  for(auto tau : m_vTaus) {
    IDVarSet piCellBasedIDVars = m_pIDVarCalculator->calculateVariables(*tau, m_rEventInfo.mu());
    tau->setIDVarSet(piCellBasedIDVars);
    (dynamic_cast<VariablePool<D3PDReader::AnalysisTau*>* >(GetTool("VariablePool")))->GetLHRatio(tau,
												  m_rEventInfo.mu());
    if(tau->numTrack()==0) 
      std::cout << "Wrong selection  "  << tau->numTrack() << "  pT: " << tau->TLV().Pt()/GeV << std::endl;
    //if data, fill dummy values for truth
    if(IsData()) {
      //TODO refactor: make variable pool a member; at least map should be a member
      //TODO: why is VariablePool templatised???
      m_pTreeFiller->Fill(tau,
			  (dynamic_cast<VariablePool<D3PDReader::AnalysisTau*>* >(GetTool("VariablePool")))->GetMap(),
			  m_rEventInfo);
      m_pTruthPt->Fill(tau->numTrack(),
		       tau->TLV().Pt()/GeV ,
		       m_rEventInfo.mu(),1.);
    }
    else {
      m_pTreeFiller->Fill(tau,
			  (dynamic_cast<VariablePool<D3PDReader::AnalysisTau*>* >(GetTool("VariablePool")))->GetMap(),
			  m_rEventInfo);
    }
  }
}

void NtupleCycle::FillTruthNormalisationHist(){
  for(auto truthTau : m_vTruthTaus)
    m_pTruthPt->Fill(truthTau->nProng(),
		     truthTau->TLV().Pt()/GeV ,
		     m_rEventInfo.mu(),
		     1.); 
}

void NtupleCycle::ApplyTruthMatch(){
  LooserTruthMatch(m_vTaus);
}
   

void NtupleCycle::StartCycle() throw(SError){
  (dynamic_cast<VariablePool<D3PDReader::AnalysisTau*>* >(GetTool("VariablePool")))->Initialize();
  m_pLLHCalculator = dynamic_cast<LLHCalculator*>(GetTool("LLHCalculator"));  
  m_pTreeFiller = dynamic_cast<TreeFillerTool*>(GetTool("TreeFiller"));
}

void NtupleCycle::StartInputFile(const SInputData&) throw(SError){
  // read from tree
  m_rEventInfo.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_rTruthTauCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_rTauCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_pIDVarCalculator->setupPileupCorrection(c_pileup_correction_file);
}

void NtupleCycle::StartInputData(const SInputData& id) throw(SError)
{
  AnalysisLLHCommon::StartInputData(id);
#ifndef REFAC
  IDVarHandle<D3PDReader::AnalysisTau*>* varhandle = IDVarHandle<D3PDReader::AnalysisTau*>::GetInstance();
  IDVarInitializer::Initialize(varhandle, m_vTaus);
  (dynamic_cast<VariablePool<D3PDReader::AnalysisTau*>* >(GetTool("VariablePool")))->Initialize();
  IDVarInitializer::AcquireVariables<D3PDReader::AnalysisTau*>((dynamic_cast<VariablePool<D3PDReader::AnalysisTau*> *>(GetTool("VariablePool"))));
  (dynamic_cast<TreeFillerTool*>(GetTool("TreeFiller")))->Initialize((dynamic_cast<VariablePool<D3PDReader::AnalysisTau*>* >(GetTool("VariablePool")))->GetMap());
#endif

  Book(TH3F("h_truthPt","h_truthPt",5,0.,5.,1000,0.,1000.,40,-0.5,39.5));

  std::vector<std::string> vsProng = {"1P", "3P"};
  for(auto sProng : vsProng){
    Book(TH1F(("truth_tau_vis_pt_"+sProng).c_str(),";trueTau pt;Normalised",500,0.,500.));
    Book(TH1F(("h_passed_BDT_loose_"+sProng+"_truth_pt").c_str(), ";tau pt; a.u.", 500, 0., 500.));
    Book(TH1F(("h_passed_BDT_medium_"+sProng+"_truth_pt").c_str(), ";tau pt; a.u.", 500, 0., 500.));
    Book(TH1F(("h_passed_BDT_tight_"+sProng+"_truth_pt").c_str(), ";tau pt; a.u.", 500, 0., 500.));
  }
  
  m_pTruthPt = dynamic_cast<TH3F*>(Hist("h_truthPt"));
  m_pLLHCalculator = dynamic_cast<LLHCalculator*>(GetTool("LLHCalculator"));
  m_pTreeFiller = dynamic_cast<TreeFillerTool*>(GetTool("TreeFiller"));
}

void NtupleCycle::FinishMasterInputData(const SInputData& id) throw(SError)
{
  CopySkimCutflow(id);
}

#endif // NtupleCycle_CXX
#endif //SKIM
