#ifdef DEV
#ifndef SKIM
#ifndef OptimizationCycle_CXX
#define OptimizationCycle_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

//STL includes
#include <iterator>
#include <vector>

//ROOT includes
#include "TFile.h"
#include "TH2F.h"
#include "CutTemplateManager.h"


// TauCommon include(s)

// LLHTool include(s)
#include "OptimizationCycle.h"
#include "VariablePool.h"
#include "IDVarHandle.h"
#include "IDVarInitializer.h"
// #include "CutCalculator.h"
#include "CombinationHandler.h"

ClassImp(OptimizationCycle);

OptimizationCycle::OptimizationCycle() :
  CycleBase(),
  m_rLLHTaus(m_lEntryNumber) {
  DeclareProperty("prongmode", c_prongmode = 1);
  DeclareProperty("pt_cut", c_pt_cut = 1);
  DeclareProperty("varset1P", c_varset1P);
  DeclareProperty("varset3P", c_varset3P);
  DeclareProperty("cuts_inputfile", c_inputfile_cuts = "");
  DeclareProperty("mu_correction_factor_1P", m_mu_correction_factor_1P = 0.);
  DeclareProperty("mu_correction_factor_3P", m_mu_correction_factor_3P = 0.);
  /*
    runmode
    1 - calculate llhscore for all combinations
    2 - calculate performance for given lmt cuts
  */

  DeclareProperty("runmode", c_runmode = 1);

  CutTemplateManager* pManager = CutTemplateManager::GetInstance();
  pManager->RegisterCutTemplate(new CutTemplate<TYPELIST_0 > ("PT", boost::bind(&OptimizationCycle::CheckPt, this, _1)));

  //declare tools
  DeclareTool(new LLHCalculator(this, "LLHCalculator"));
  DeclareTool(new VariablePool<D3PDReader::LLHTau*>(this, "VariablePool"));
  DeclareTool(new CombinationHandler<D3PDReader::LLHTau*>(this, "CombinationTool"));

  m_numScore = 0;
  m_numWrongScore = 0;
  }

void OptimizationCycle::AnalyseEvent(const SInputData& id, double dWeight) throw (SError) {
  m_numTrack = m_rLLHTaus.NumTrack();
#ifdef TBF
  if (!IsData() && !CheckTruthNumTracks(m_fEventWeight))
    throw SError(SError::SkipEvent);
  else if (IsData() && !CheckNumTracks(m_fEventWeight))
    throw SError(SError::SkipEvent);
#endif
  InitializeBaseVariables();

#ifdef TBC
  if (c_runmode == 1) {
    if (!CheckNumTracks(m_fEventWeight))
      throw SError(SError::SkipEvent);
  }
#endif
  (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->ProcessCombination(&m_rLLHTaus);
  double llhscore = (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->GetScore(&m_rLLHTaus);
   
  FillScores(llhscore);
  dynamic_cast<TH2F*> (Hist("h_TruthpT_OfflinepT"))->Fill(m_pt, m_rLLHTaus.pt() / 1000.);
  if (c_runmode == 2) {
    if (!CheckPt(m_fEventWeight)) return;
    FillTotal();
    if (!CheckNumTracks(m_fEventWeight))
      return;
    Hist("h_numTrack")->Fill(m_rLLHTaus.NumTrack());
    FillPassed(llhscore);
  }

}

void OptimizationCycle::InitializeBaseVariables() {
  if (IsData()) {
    m_pt = m_rLLHTaus.pt() / 1000.;
    m_numTrack = m_rLLHTaus.NumTrack();
  } else {
#ifdef TBF
    m_pt = m_rLLHTaus.TruthPt() / 1000.;
#endif
    m_pt = m_rLLHTaus.pt() / 1000.;
    m_numTrack = m_rLLHTaus.NumTrack();
  }

}

void OptimizationCycle::FillScores(const double& llhscore) {

  dynamic_cast<TH3F*> (Hist("h_pT_LLHScore"))->Fill(m_pt, llhscore, m_rLLHTaus.mu());
  dynamic_cast<TH3F*> (Hist("h_default_pT_LLHScore"))->Fill(m_pt, m_rLLHTaus.DefaultLLHScore(), m_rLLHTaus.mu());
  dynamic_cast<TH3F*> (Hist("h_default_pT_BDTScore"))->Fill(m_pt, m_rLLHTaus.BDTScore(), m_rLLHTaus.mu());
}

void OptimizationCycle::FillPassed(const double& llhscore) {
  if (CalculateCut(tg_loose_cuts) < llhscore)
    dynamic_cast<TH3F*> (Hist("h_passed"))->Fill(0., m_pt, m_rLLHTaus.mu(), 1.);

  if (CalculateCut(tg_medium_cuts) < llhscore)
    dynamic_cast<TH3F*> (Hist("h_passed"))->Fill(1., m_pt, m_rLLHTaus.mu(), 1.);

  if (CalculateCut(tg_tight_cuts) < llhscore)
    dynamic_cast<TH3F*> (Hist("h_passed"))->Fill(2., m_pt, m_rLLHTaus.mu(), 1.);

  if (m_rLLHTaus.DefaultLLHLoose() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultllh"))->Fill(0., m_pt, m_rLLHTaus.mu(), 1.);

  if (m_rLLHTaus.DefaultLLHMedium() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultllh"))->Fill(1., m_pt, m_rLLHTaus.mu(), 1.);

  if (m_rLLHTaus.DefaultLLHTight() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultllh"))->Fill(2., m_pt, m_rLLHTaus.mu(), 1.);

  if (m_rLLHTaus.DefaultBDTLoose() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultbdt"))->Fill(0., m_pt, m_rLLHTaus.mu(), 1.);

  if (m_rLLHTaus.DefaultBDTMedium() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultbdt"))->Fill(1., m_pt, m_rLLHTaus.mu(), 1.);

  if (m_rLLHTaus.DefaultBDTTight() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultbdt"))->Fill(2., m_pt, m_rLLHTaus.mu(), 1.);

}

void OptimizationCycle::FillTotal() {
  dynamic_cast<TH3F*> (Hist("h_total"))->Fill(1., m_pt, m_rLLHTaus.mu(), 1.);
}

bool OptimizationCycle::IsInBin(double ptlow, double pT) {
  return (pT / 1000. < ptlow);
}

double OptimizationCycle::CalculateCut(TGraph* cuts) {
  return cuts->Eval(m_rLLHTaus.pt() / 1000.);

  /*
    if (m_rLLHTaus.mu() < 3)
    return (cuts->Eval(m_rLLHTaus.Pt() / 1000.) - (m_rLLHTaus.mu() - 2) * m_mu_correction_factor);
    else if (m_rLLHTaus.mu() >= 3 && m_rLLHTaus.mu() < 8)
    return (cuts->Eval(m_rLLHTaus.Pt() / 1000.) - (m_rLLHTaus.mu() - 4) * m_mu_correction_factor);
    else if (m_rLLHTaus.mu() > 7)
    return (cuts->Eval(m_rLLHTaus.Pt() / 1000.) - (m_rLLHTaus.mu() - 8) * m_mu_correction_factor);
    else
    return cuts->Eval(m_rLLHTaus.Pt() / 1000.);
  */
}

void OptimizationCycle::FinishMasterInputData(const SInputData& id) throw (SError) {

  /*std::cout << "Number Wrong LLHScores / Total Number of Taus : " << m_numWrongScore << " / " << m_numScore << std::endl;*/

  TH1* h_truthPt = 0;
  TH1* temp = 0;
  std::vector<SFile> vInputFiles = id.GetSFileIn();
  for (std::vector<SFile>::iterator it = vInputFiles.begin(); it != vInputFiles.end(); ++it) {
    // open input file and retrieve truthPt histograms
    TFile* f = TFile::Open(it->file, "READ");
    temp = (TH1*) f->Get("h_truthPt");
    if (!temp) {
      m_logger << ERROR << "Couldn't retrieve normalisation histogramm with name from file " << it->file << SLogger::endmsg;
      continue;
    }
    if (h_truthPt == 0) {
      h_truthPt = (TH1*) temp->Clone();
      h_truthPt->SetDirectory(0);
    } else
      h_truthPt->Add(temp);
    f->Close();
    delete f;
  }
  if(h_truthPt)
    Book(*h_truthPt, "");

  // switch (c_runmode) {
  // case 1:
  //   (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->WriteAllCombinations();
  //   break;
  // case 2:
  //   break;
  // }
}

void OptimizationCycle::StartCycle() throw (SError) {
  VariablePool<D3PDReader::LLHTau*>* m_pool = dynamic_cast<VariablePool<D3PDReader::LLHTau*>*> (GetTool("VariablePool"));
  m_pool->Initialize();
  IDVarHandle<D3PDReader::LLHTau*>* varhandle = IDVarHandle<D3PDReader::LLHTau*>::GetInstance();
  IDVarInitializer::Initialize(varhandle, &m_rLLHTaus);
  IDVarInitializer::AcquireVariables<D3PDReader::LLHTau*>(m_pool, true);
  if (c_prongmode == 1)
    m_mu_correction_factor = m_mu_correction_factor_1P;
  else
    m_mu_correction_factor = m_mu_correction_factor_3P;
  if (c_runmode == 2) {
    f = TFile::Open(c_inputfile_cuts.c_str());
    std::string prongdir;
    if (c_prongmode == 1)
      prongdir = "1prong/";
    else
      prongdir = "3prong/";
    std::cout << "Directory" << prongdir + "loose" << std::endl;
    tg_loose_cuts = (TGraph*) f->Get((prongdir + "loose").c_str());
    tg_medium_cuts = (TGraph*) f->Get((prongdir + "medium").c_str());
    tg_tight_cuts = (TGraph*) f->Get((prongdir + "tight").c_str());
    assert(f != 0);
    assert(tg_loose_cuts != 0);
    assert(tg_medium_cuts != 0);
    assert(tg_tight_cuts != 0);
  }
}

void OptimizationCycle::StartInputData(const SInputData& id) throw (SError) {
  if (c_prongmode == 1)
    (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->Initialize(c_varset1P);
  if (c_prongmode == 2 || c_prongmode == 3)
    (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->Initialize(c_varset3P);

  // Book histograms
  /*    if (c_runmode == 2) {
        Book(TH3F("h_passed", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
        Book(TH3F("h_passed_defaultllh", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
        Book(TH3F("h_passed_defaultbdt", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
        Book(TH3F("h_passed_defaultcut", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
        Book(TH3F("h_total", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
	}*/

  if (c_runmode == 2) {
    Book(TH3F("h_passed", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
    Book(TH3F("h_passed_defaultllh", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
    Book(TH3F("h_passed_defaultbdt", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
    Book(TH3F("h_passed_defaultcut", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
    Book(TH3F("h_total", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
  }

  Book(TH2F("h_TruthpT_OfflinepT", "; truth pT, offline pT", 1000, 0., 1000., 1000, 0., 1000.));
  Book(TH3F("h_pT_LLHScore", "; LLH Score;Normalised", 1000, 0., 1000., 700, -40., 30., 40, -0.5, 39.5));
  Book(TH3F("h_default_pT_LLHScore", "DefaultLLHScore; LLH Score;Normalised", 1000, 0., 1000., 700, -40., 30., 40, -0.5, 39.5));
  Book(TH3F("h_default_pT_BDTScore", "DefaultBDTScore; BDT Score;Normalised", 1000, 0., 1000., 100, -0.5, 1.1, 40, -0.5, 39.5));
  //some control plots
  Book(TH1F("h_numTrack", ";nTracks;a.u", 6, -0.5, 5.5));
}

void OptimizationCycle::StartInputFile(const SInputData&) throw (SError) {
  m_rLLHTaus.ReadFrom(GetInputTree(c_tree_name.c_str()));
}

bool OptimizationCycle::CheckPt(float& fEventWeight) {
  return (m_pt > c_pt_cut);
}

bool OptimizationCycle::CheckTruthNumTracks(float& fEventWeight) {
  if (c_prongmode == 1)
    return (m_rLLHTaus.TruthNumTrack() == 1);
  else if ((c_prongmode == 2 || c_prongmode == 3) && !IsData())
    return (m_rLLHTaus.TruthNumTrack() == 3);
  else
    return false;
}

bool OptimizationCycle::CheckRecoNumTracks(float& fEventWeight) {
  if (c_prongmode == 1)
    return (m_rLLHTaus.NumTrack() == 1);
  else if (c_prongmode == 2 || c_prongmode == 3)
    return (m_rLLHTaus.NumTrack() == 3);
  else
    return false;
}

bool OptimizationCycle::CheckNumTracks(float& fEventWeight) {
  if (c_prongmode == 1)
    return (m_numTrack == 1);
  else if (c_prongmode == 2)
    return (m_numTrack == 2 || m_numTrack == 3);
  else if (c_prongmode == 3)
    return (m_numTrack == 3);
  else
    return false;
}
#endif // OptimizationCycle_CXX
#endif //SKIM
#endif




















#ifndef DEV
#ifndef SKIM
#ifndef OptimizationCycle_CXX
#define OptimizationCycle_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

//STL includes
#include <iterator>
#include <vector>

//ROOT includes
#include "TFile.h"
#include "TH2F.h"
#include "CutTemplateManager.h"


// TauCommon include(s)

// LLHTool include(s)
#include "OptimizationCycle.h"
#include "VariablePool.h"
#include "IDVarHandle.h"
#include "IDVarInitializer.h"
#include "CutCalculator.h"
#include "CombinationHandler.h"

ClassImp(OptimizationCycle);

OptimizationCycle::OptimizationCycle() :
  CycleBase(),
  m_rLLHTaus(m_lEntryNumber) {
  DeclareProperty("prongmode", c_prongmode = 1);
  DeclareProperty("pt_cut", c_pt_cut = 1);
  DeclareProperty("varset1P", c_varset1P);
  DeclareProperty("varset3P", c_varset3P);
  DeclareProperty("cuts_inputfile", c_inputfile_cuts = "");
  DeclareProperty("mu_correction_factor_1P", m_mu_correction_factor_1P = 0.);
  DeclareProperty("mu_correction_factor_3P", m_mu_correction_factor_3P = 0.);
  /*
    runmode
    1 - calculate llhscore for all combinations
    2 - calculate performance for given lmt cuts
  */

  DeclareProperty("runmode", c_runmode = 1);

  CutTemplateManager* pManager = CutTemplateManager::GetInstance();
  pManager->RegisterCutTemplate(new CutTemplate<TYPELIST_0 > ("PT", boost::bind(&OptimizationCycle::CheckPt, this, _1)));

  //declare tools
  DeclareTool(new LLHCalculator(this, "LLHCalculator"));
  DeclareTool(new VariablePool<D3PDReader::LLHTau*>(this, "VariablePool"));
  DeclareTool(new CombinationHandler<D3PDReader::LLHTau*>(this, "CombinationTool"));

  m_numScore = 0;
  m_numWrongScore = 0;
  }

void OptimizationCycle::AnalyseEvent(const SInputData& id, double dWeight) throw (SError) {
  m_numTrack = m_rLLHTaus.NumTrack();
  if (!IsData() && !CheckTruthNumTracks(m_fEventWeight))
    throw SError(SError::SkipEvent);
  else if (IsData() && !CheckNumTracks(m_fEventWeight))
    throw SError(SError::SkipEvent);
  InitializeBaseVariables();

  if (c_runmode == 1) {
    if (!CheckNumTracks(m_fEventWeight))
      throw SError(SError::SkipEvent);
  }
  (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->ProcessCombination(&m_rLLHTaus);
  double llhscore = (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->GetScore(&m_rLLHTaus);
   
  // --- Check Score ---
  /*
    if( abs(llhscore - m_rLLHTaus.DefaultLLHScore()) > 1e-5 ) {
    m_numWrongScore++;
		
    std::cout << "N: " << m_numWrongScore;
    std::cout << "\t\tLLHScore: " << llhscore << "\tDefaultScore: " << m_rLLHTaus.DefaultLLHScore();
    std::cout << "\t\tRunNum: " << m_rLLHTaus.RunNumber() << "\t\tEvtNum: " << m_rLLHTaus.EventNumber();
    std::cout << "\t\tPt: " << m_rLLHTaus.Pt() << "\t\tEta: " << m_rLLHTaus.Eta() << std::endl;
    std::cout << "\t\tNumTrk: " << m_rLLHTaus.NumTrack() << "\t\tCentFrac: " << m_rLLHTaus.CentFrac0102() << "\t\tFtrk: " << m_rLLHTaus.FTrk02() << "\t\tTrkAvgDist" << m_rLLHTaus.TrkAvgDist();
    if( m_rLLHTaus.NumTrack() == 1 )
    {
    std::cout << "\t\tNTracksdrdR" << m_rLLHTaus.NTracksdrdR() << "\t\tIpSigLeadTrk" << m_rLLHTaus.IpSigLeadTrk() << std::endl;
    }
    else
    {
    std::cout << "\t\tTrFligthPathSig" << m_rLLHTaus.TrFligthPathSig() << "\t\tMassTrkSys" << m_rLLHTaus.MassTrkSys() << "\t\tDrMax" << m_rLLHTaus.DrMax() << std::endl;
    }


    dynamic_cast<TH1F*> (Hist("val_failedTau_pt"))->Fill(m_pt);
    dynamic_cast<TH1F*> (Hist("val_failedTau_eta"))->Fill(m_rLLHTaus.Eta());
    dynamic_cast<TH1F*> (Hist("val_failedTau_dLLH"))->Fill(abs(llhscore - m_rLLHTaus.DefaultLLHScore()));
    dynamic_cast<TH1F*> (Hist("val_failedTau_nVertices"))->Fill(m_rLLHTaus.NVertices());
    dynamic_cast<TH1F*> (Hist("val_failedTau_numTrack"))->Fill(m_rLLHTaus.NumTrack());
    dynamic_cast<TH1F*> (Hist("val_failedTau_centFrac0102"))->Fill(m_rLLHTaus.CentFrac0102());
    dynamic_cast<TH1F*> (Hist("val_failedTau_drMax"))->Fill(m_rLLHTaus.DrMax());
    dynamic_cast<TH1F*> (Hist("val_failedTau_fTrk02"))->Fill(m_rLLHTaus.FTrk02());
    dynamic_cast<TH1F*> (Hist("val_failedTau_ipSigLeadTrk"))->Fill(m_rLLHTaus.IpSigLeadTrk());
    dynamic_cast<TH1F*> (Hist("val_failedTau_massTrkSys"))->Fill(m_rLLHTaus.MassTrkSys());
    dynamic_cast<TH1F*> (Hist("val_failedTau_nTracksdrdR"))->Fill(m_rLLHTaus.NTracksdrdR());
    dynamic_cast<TH1F*> (Hist("val_failedTau_trFlightPathSig"))->Fill(m_rLLHTaus.TrFligthPathSig());
    dynamic_cast<TH1F*> (Hist("val_failedTau_trkAvgDist"))->Fill(m_rLLHTaus.TrkAvgDist());
    }
    m_numScore++;
    // --- Check Score ---
    */

  FillScores(llhscore);
  dynamic_cast<TH2F*> (Hist("h_TruthpT_OfflinepT"))->Fill(m_pt, m_rLLHTaus.Pt() / 1000.);
  if (c_runmode == 2) {
    if (!CheckPt(m_fEventWeight)) return;
    FillTotal();
    if (!CheckNumTracks(m_fEventWeight))
      return;
    Hist("h_numTrack")->Fill(m_rLLHTaus.NumTrack());
    FillPassed(llhscore);
  }

}

void OptimizationCycle::InitializeBaseVariables() {
  if (IsData()) {
    m_pt = m_rLLHTaus.Pt() / 1000.;
    m_numTrack = m_rLLHTaus.NumTrack();
  } else {
    m_pt = m_rLLHTaus.TruthPt() / 1000.;
    m_numTrack = m_rLLHTaus.NumTrack();
  }

}

void OptimizationCycle::FillScores(const double& llhscore) {

  dynamic_cast<TH3F*> (Hist("h_pT_LLHScore"))->Fill(m_pt, llhscore, m_rLLHTaus.NVertices());
  dynamic_cast<TH3F*> (Hist("h_default_pT_LLHScore"))->Fill(m_pt, m_rLLHTaus.DefaultLLHScore(), m_rLLHTaus.NVertices());
  dynamic_cast<TH3F*> (Hist("h_default_pT_BDTScore"))->Fill(m_pt, m_rLLHTaus.BDTScore(), m_rLLHTaus.NVertices());
  dynamic_cast<TH3F*> (Hist("h_default_pT_CutLoose"))->Fill(m_pt, m_rLLHTaus.DefaultCutLoose(), m_rLLHTaus.NVertices());
  dynamic_cast<TH3F*> (Hist("h_default_pT_CutMedium"))->Fill(m_pt, m_rLLHTaus.DefaultCutMedium(), m_rLLHTaus.NVertices());
  dynamic_cast<TH3F*> (Hist("h_default_pT_CutTight"))->Fill(m_pt, m_rLLHTaus.DefaultCutTight(), m_rLLHTaus.NVertices());

}

void OptimizationCycle::FillPassed(const double& llhscore) {
  if (CalculateCut(tg_loose_cuts) < llhscore)
    dynamic_cast<TH3F*> (Hist("h_passed"))->Fill(0., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (CalculateCut(tg_medium_cuts) < llhscore)
    dynamic_cast<TH3F*> (Hist("h_passed"))->Fill(1., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (CalculateCut(tg_tight_cuts) < llhscore)
    dynamic_cast<TH3F*> (Hist("h_passed"))->Fill(2., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (m_rLLHTaus.DefaultLLHLoose() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultllh"))->Fill(0., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (m_rLLHTaus.DefaultLLHMedium() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultllh"))->Fill(1., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (m_rLLHTaus.DefaultLLHTight() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultllh"))->Fill(2., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (m_rLLHTaus.DefaultBDTLoose() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultbdt"))->Fill(0., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (m_rLLHTaus.DefaultBDTMedium() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultbdt"))->Fill(1., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (m_rLLHTaus.DefaultBDTTight() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultbdt"))->Fill(2., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (m_rLLHTaus.DefaultCutLoose() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultcut"))->Fill(0., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (m_rLLHTaus.DefaultCutMedium() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultcut"))->Fill(1., m_pt, m_rLLHTaus.NVertices(), 1.);

  if (m_rLLHTaus.DefaultCutTight() == 1)
    dynamic_cast<TH3F*> (Hist("h_passed_defaultcut"))->Fill(2., m_pt, m_rLLHTaus.NVertices(), 1.);

}

void OptimizationCycle::FillTotal() {
  dynamic_cast<TH3F*> (Hist("h_total"))->Fill(1., m_pt, m_rLLHTaus.NVertices(), 1.);
}

bool OptimizationCycle::IsInBin(double ptlow, double pT) {
  return (pT / 1000. < ptlow);
}

double OptimizationCycle::CalculateCut(TGraph* cuts) {
  return cuts->Eval(m_rLLHTaus.Pt() / 1000.);

  /*
    if (m_rLLHTaus.NVertices() < 3)
    return (cuts->Eval(m_rLLHTaus.Pt() / 1000.) - (m_rLLHTaus.NVertices() - 2) * m_mu_correction_factor);
    else if (m_rLLHTaus.NVertices() >= 3 && m_rLLHTaus.NVertices() < 8)
    return (cuts->Eval(m_rLLHTaus.Pt() / 1000.) - (m_rLLHTaus.NVertices() - 4) * m_mu_correction_factor);
    else if (m_rLLHTaus.NVertices() > 7)
    return (cuts->Eval(m_rLLHTaus.Pt() / 1000.) - (m_rLLHTaus.NVertices() - 8) * m_mu_correction_factor);
    else
    return cuts->Eval(m_rLLHTaus.Pt() / 1000.);
  */
}

void OptimizationCycle::FinishMasterInputData(const SInputData& id) throw (SError) {

  /*std::cout << "Number Wrong LLHScores / Total Number of Taus : " << m_numWrongScore << " / " << m_numScore << std::endl;*/

  TH1* h_truthPt = 0;
  TH1* temp = 0;
  std::vector<SFile> vInputFiles = id.GetSFileIn();
  for (std::vector<SFile>::iterator it = vInputFiles.begin(); it != vInputFiles.end(); ++it) {
    // open input file and retrieve truthPt histograms
    TFile* f = TFile::Open(it->file, "READ");
    temp = (TH1*) f->Get("h_truthPt");
    if (!temp) {
      m_logger << ERROR << "Couldn't retrieve normalisation histogramm with name from file " << it->file << SLogger::endmsg;
      continue;
    }
    if (h_truthPt == 0) {
      h_truthPt = (TH1*) temp->Clone();
      h_truthPt->SetDirectory(0);
    } else
      h_truthPt->Add(temp);
    f->Close();
    delete f;
  }
  if(h_truthPt)
    Book(*h_truthPt, "");
  /*
  switch (c_runmode) {
  case 1:
    (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->WriteAllCombinations();
    break;
  case 2:
    break;
  }
  */
}

void OptimizationCycle::StartCycle() throw (SError) {
  VariablePool<D3PDReader::LLHTau*>* m_pool = dynamic_cast<VariablePool<D3PDReader::LLHTau*>*> (GetTool("VariablePool"));
  m_pool->Initialize();
  IDVarHandle<D3PDReader::LLHTau*>* varhandle = IDVarHandle<D3PDReader::LLHTau*>::GetInstance();
  IDVarInitializer::Initialize(varhandle, &m_rLLHTaus);
  IDVarInitializer::AcquireVariables<D3PDReader::LLHTau*>(m_pool, true);
  if (c_prongmode == 1)
    m_mu_correction_factor = m_mu_correction_factor_1P;
  else
    m_mu_correction_factor = m_mu_correction_factor_3P;
  if (c_runmode == 2) {
    f = TFile::Open(c_inputfile_cuts.c_str());
    std::string prongdir;
    if (c_prongmode == 1)
      prongdir = "1prong/";
    else
      prongdir = "3prong/";
    std::cout << "Directory" << prongdir + "loose" << std::endl;
    tg_loose_cuts = (TGraph*) f->Get((prongdir + "loose").c_str());
    tg_medium_cuts = (TGraph*) f->Get((prongdir + "medium").c_str());
    tg_tight_cuts = (TGraph*) f->Get((prongdir + "tight").c_str());
    assert(f != 0);
    assert(tg_loose_cuts != 0);
    assert(tg_medium_cuts != 0);
    assert(tg_tight_cuts != 0);
  }
}

void OptimizationCycle::StartInputData(const SInputData& id) throw (SError) {
  if (c_prongmode == 1)
    (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->Initialize(c_varset1P);
  if (c_prongmode == 2 || c_prongmode == 3)
    (dynamic_cast<CombinationHandler<D3PDReader::LLHTau*>*> (GetTool("CombinationTool")))->Initialize(c_varset3P);

  // Book histograms
  /*    if (c_runmode == 2) {
        Book(TH3F("h_passed", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
        Book(TH3F("h_passed_defaultllh", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
        Book(TH3F("h_passed_defaultbdt", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
        Book(TH3F("h_passed_defaultcut", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
        Book(TH3F("h_total", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 20, -0.5, 19.5));
	}*/

  if (c_runmode == 2) {
    Book(TH3F("h_passed", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
    Book(TH3F("h_passed_defaultllh", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
    Book(TH3F("h_passed_defaultbdt", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
    Book(TH3F("h_passed_defaultcut", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
    Book(TH3F("h_total", ";;", 3, -0.5, 2.5, 1000, 0., 1000., 40, -0.5, 39.5));
  }

  Book(TH2F("h_TruthpT_OfflinepT", "; truth pT, offline pT", 1000, 0., 1000., 1000, 0., 1000.));
  Book(TH3F("h_pT_LLHScore", "; LLH Score;Normalised", 1000, 0., 1000., 700, -40., 30., 40, -0.5, 39.5));
  Book(TH3F("h_default_pT_LLHScore", "DefaultLLHScore; LLH Score;Normalised", 1000, 0., 1000., 700, -40., 30., 40, -0.5, 39.5));
  Book(TH3F("h_default_pT_BDTScore", "DefaultBDTScore; BDT Score;Normalised", 1000, 0., 1000., 100, -0.5, 1.1, 40, -0.5, 39.5));
  Book(TH3F("h_default_pT_CutLoose", "DefaultCutLoose; Cut loose;", 1000, 0., 1000., 2, 0., 2., 40, -0.5, 39.5));
  Book(TH3F("h_default_pT_CutMedium", "DefaultCutMedium; Cut medium;", 1000, 0., 1000., 2, 0., 2., 40, -0.5, 39.5));
  Book(TH3F("h_default_pT_CutTight", "DefaultCutTight; Cut tight;", 1000, 0., 1000., 2, 0., 2., 40, -0.5, 39.5));
  //some control plots
  Book(TH1F("h_numTrack", ";nTracks;a.u", 6, -0.5, 5.5));

  /*
  // Book Validation Histograms
  Book(TH1F("val_failedTau_pt", "val_failedTau_pt", 1000, 0.0, 1000.0));
  Book(TH1F("val_failedTau_eta", "val_failedTau_eta", 60, -3.0, 3.0));
  Book(TH1F("val_failedTau_dLLH", "val_failedTau_dLLH", 1000, -40.0, 1200.0));
  Book(TH1F("val_failedTau_nVertices", "val_failedTau_NVertices", 40, 0.0, 40.0));
  Book(TH1F("val_failedTau_numTrack", "val_failedTau_numTrack", 5, 0.0, 5.0));
  Book(TH1F("val_failedTau_centFrac0102", "val_failedTau_centFrac0102", 40, -20.0, 20.0));
  Book(TH1F("val_failedTau_fTrk02", "val_failedTau_fTrk02", 40, -20.0, 20.0));
  Book(TH1F("val_failedTau_drMax", "val_failedTau_drMax", 40, -20.0, 20.0));
  Book(TH1F("val_failedTau_ipSigLeadTrk", "val_failedTau_ipSigLeadTrk", 1000, -1200.0, 20.0));
  Book(TH1F("val_failedTau_massTrkSys", "val_failedTau_massTrkSys", 25, -20.0, 5.0));
  Book(TH1F("val_failedTau_nTracksdrdR", "val_failedTau_nTracksdrdR", 25, -20.0, 5.0));
  Book(TH1F("val_failedTau_trFlightPathSig", "val_failedTau_trFlightPathSig", 1000, -1200.0, 20.0));
  Book(TH1F("val_failedTau_trkAvgDist", "val_failedTau_trkAvgDist", 40, -20.0, 20.0));
  */

}

void OptimizationCycle::StartInputFile(const SInputData&) throw (SError) {
  m_rLLHTaus.ReadFrom(GetInputTree(c_tree_name.c_str()));
}

bool OptimizationCycle::CheckPt(float& fEventWeight) {
  return (m_pt > c_pt_cut);
}

bool OptimizationCycle::CheckTruthNumTracks(float& fEventWeight) {
  if (c_prongmode == 1)
    return (m_rLLHTaus.TruthNumTrack() == 1);
  else if (c_prongmode == 2 || c_prongmode == 3 && !IsData())
    return (m_rLLHTaus.TruthNumTrack() == 3);
  else
    return false;
}

bool OptimizationCycle::CheckRecoNumTracks(float& fEventWeight) {
  if (c_prongmode == 1)
    return (m_rLLHTaus.NumTrack() == 1);
  else if (c_prongmode == 2 || c_prongmode == 3)
    return (m_rLLHTaus.NumTrack() == 3);
  else
    return false;
}

bool OptimizationCycle::CheckNumTracks(float& fEventWeight) {
  if (c_prongmode == 1)
    return (m_numTrack == 1);
  else if (c_prongmode == 2)
    return (m_numTrack == 2 || m_numTrack == 3);
  else if (c_prongmode == 3)
    return (m_numTrack == 3);
  else
    return false;
}
#endif // OptimizationCycle_CXX
#endif //SKIM
#endif
