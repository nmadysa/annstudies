#ifndef SKIM
#ifndef TTNTCycle_CXX
#define TTNTCycle_CXX

// system include(s)
#include "assert.h"
#include <set>

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

// core include(s)
#include "Region.h"
#include "Utilities.h"
#include "BoundHistogram.h"

// TauCommon include(s)
#include "Helpers.h"

// custom include(s)
#include "TTNTCycle.h"
#include "VariablePlotter.h"
#include "Constants.h"
ClassImp(TTNTCycle);
using namespace Utilities;
using namespace AnalysisUtilities_Constants;

TTNTCycle::TTNTCycle() :
  AnalysisLLHCommon(),
  m_itau_n(1),
  m_pPlotter()
{
  DeclareProperty("pileup_correction_inputfile", c_pileup_correction_file = "");
  DeclareProperty("pileup_correction_switchOff", c_pileup_correction_switchOff = false);

  m_IDVarCalculator = new CellBasedCalculator();
  //  m_IDVarCalculator = new EFlowRecCalculator();
  m_pileUpCorrectionTool = new PileUpCorrectionTool();
}

TTNTCycle::~TTNTCycle() {
  m_oTauCreator.ClearAllObjects();
}

void TTNTCycle::AnalyseEvent(const SInputData& id, double dWeight) throw (SError) {
  m_oTauCreator.ClearAllObjects();

  // create analysis objects for current event
  m_oTauCreator.CreateObjects(m_rTauCollection.n(), m_rTauCollection, m_vTaus);
  // if(m_rEventInfo.EventNumber() % 3 != 0)
  //   throw SError(SError::SkipEvent);

  m_fEventWeight = 1;

  AnalysisLLHCommon::SelectPhysicsObjects(m_fEventWeight);

  dynamic_cast<Region*> (GetTool("BaselineSelection"))->IsInRegion(m_fEventWeight, true);

  TTree* pTree = GetOutputMetadataTree("tau");
  for(auto tau : m_vTaus){
    if (not SelectTrainingObject(tau)) 
      continue;
    CalculateVariables(tau);
    pTree->Fill();  
  }
}

bool TTNTCycle::SelectTrainingObject(D3PDReader::AnalysisTau * const tau){
  if(!TightRecoNumTrack(tau))
    return false;
  if(tau->Et() < 15000)
    return false;
  if(abs(tau->eta()) > 2.3)
    return false;
  if(!IsData()){
    if(!TightTruthMatch(tau))
      return false;
    if(tau->matchPt() < 10000)
      return false;
    if(abs(tau->matchEta()) > 2.5)
      return false;
  }
  return true;
}

void TTNTCycle::StartInputFile(const SInputData& id) throw (SError) {
  // read from tree
  m_rEventInfo.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_rTauCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
}

void TTNTCycle::CalculateVariables(D3PDReader::AnalysisTau* tau){  
  m_ftau_calcVars_corrCentFrac = tau->calcVars_corrCentFrac();
  m_ftau_calcVars_corrFTrk = tau->calcVars_corrFTrk();
  m_ftau_ipSigLeadTrk = tau->ipSigLeadTrk();
  m_ftau_massTrkSys = tau->massTrkSys();
  m_ftau_pi0_visTau_m = tau->pi0_vistau_m(); 
  m_ftau_ptRatio = tau->ptRatio();
  m_ftau_seedCalo_trkAvgDist = tau->seedCalo_trkAvgDist();
  m_ftau_seedCalo_dRmax = tau->seedCalo_dRmax();
  m_ftau_trFlightPathSig = tau->trFlightPathSig();
  m_ftau_m = tau->TLV().M();
  m_ftau_pt = tau->TLV().Pt();
  m_ftau_eta = tau->TLV().Eta();
  m_ftau_phi = tau->TLV().Phi();
  m_itau_pi0_n = tau->pi0_n();
  m_itau_seedCalo_wideTrk_n = tau->seedCalo_wideTrk_n();
  m_itau_numTrack = tau->numTrack();
  m_itau_nVtx = m_rEventInfo.nVtx();
  m_ftau_mu = m_rEventInfo.mu();
  m_itau_EventNumber = m_rEventInfo.EventNumber();

  m_piCellBasedIDVars = m_IDVarCalculator->calculateVariables(*tau);
  m_fpanTauCellBased_eFrac0102 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0102",
									 tau,
									 m_ftau_mu,
									 m_piCellBasedIDVars.eFrac0102);
  m_fpanTauCellBased_eFrac0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0204",
									     tau,
									     m_ftau_mu,
									     m_piCellBasedIDVars.eFrac0204);
  m_fpanTauCellBased_eFrac0104 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0104",
									    tau,
									    m_ftau_mu,
									    m_piCellBasedIDVars.eFrac0104);
  m_fpanTauCellBased_eFracChrg0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg0204",
									     tau,
									     m_ftau_mu,
									     m_piCellBasedIDVars.eFracChrg0204);
  m_fpanTauCellBased_eFracChrg0104 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg0104",
									    tau,
									    m_ftau_mu,
									    m_piCellBasedIDVars.eFracChrg0104);
  m_fpanTauCellBased_eFracNeut0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut0204",
									     tau,
									     m_ftau_mu,
									     m_piCellBasedIDVars.eFracNeut0204);
  m_fpanTauCellBased_eFracNeut0104 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut0104",
									    tau,
									    m_ftau_mu,
									    m_piCellBasedIDVars.eFracNeut0104);
  m_fpanTauCellBased_fLeadChrg01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg01",
								       tau,
								       m_ftau_mu,
								       m_piCellBasedIDVars.fLeadChrg01);
  m_fpanTauCellBased_fLeadChrg02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg02",
								       tau,
								       m_ftau_mu,
								       m_piCellBasedIDVars.fLeadChrg02);
  m_fpanTauCellBased_fLeadChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg04",
								       tau,
								       m_ftau_mu,
								       m_piCellBasedIDVars.fLeadChrg04);
  m_fpanTauCellBased_trFlightPathSig = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_trFlightPathSig",
										tau,
										m_ftau_mu,
										m_piCellBasedIDVars.trFlightPathSig);
  m_fpanTauCellBased_ipSigLeadTrk = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ipSigLeadTrk",
									     tau,
									     m_ftau_mu,
									     m_piCellBasedIDVars.ipSigLeadTrk);
  m_fpanTauCellBased_massChrgSys01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys01",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.massChrgSys01 / GeV);
  m_fpanTauCellBased_massChrgSys02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys02",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.massChrgSys02 / GeV);
  m_fpanTauCellBased_massChrgSys04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys04",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.massChrgSys04 / GeV);
  m_fpanTauCellBased_massNeutSys01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys01",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.massNeutSys01 / GeV);
  m_fpanTauCellBased_massNeutSys02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys02",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.massNeutSys02 / GeV);
  m_fpanTauCellBased_massNeutSys04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys04",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.massNeutSys04 / GeV);
  m_fpanTauCellBased_rCal02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCal02",
								       tau,
								       m_ftau_mu,
								       m_piCellBasedIDVars.rCal02);
  m_fpanTauCellBased_rCal04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCal04",
								       tau,
								       m_ftau_mu,
								       m_piCellBasedIDVars.rCal04);
  m_fpanTauCellBased_rCalChrg02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalChrg02",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.rCalChrg02);
  m_fpanTauCellBased_rCalChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalChrg04",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.rCalChrg04);
  m_fpanTauCellBased_rCalNeut02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalNeut02",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.rCalNeut02);
  m_fpanTauCellBased_rCalNeut04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalNeut04",
									   tau,
									   m_ftau_mu,
									   m_piCellBasedIDVars.rCalNeut04);
  m_fpanTauCellBased_dRmax02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRmax02",
								      tau,
								      m_ftau_mu,
								      m_piCellBasedIDVars.dRmax02);
  m_fpanTauCellBased_dRmax04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRmax04",
								      tau,
								      m_ftau_mu,
								      m_piCellBasedIDVars.dRmax04);
  m_fpanTauCellBased_nChrg01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg01",
								       tau,
								       m_ftau_mu,
								       m_piCellBasedIDVars.nChrg01);
  m_fpanTauCellBased_nChrg02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg02",
								       tau,
								       m_ftau_mu,
								       m_piCellBasedIDVars.nChrg02);
  m_fpanTauCellBased_nChrg0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg0204",
									    tau,
									    m_ftau_mu,
									    m_piCellBasedIDVars.nChrg0204);
  m_fpanTauCellBased_ptRatio01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio01",
									tau,
									m_ftau_mu,
									m_piCellBasedIDVars.ptRatio01);
  m_fpanTauCellBased_ptRatio02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio02",
									tau,
									m_ftau_mu,
									m_piCellBasedIDVars.ptRatio02);
  m_fpanTauCellBased_ptRatio04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio04",
									tau,
									m_ftau_mu,
									m_piCellBasedIDVars.ptRatio04);
  m_fpanTauCellBased_ptRatioNeut01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut01",
									tau,
									m_ftau_mu,
									m_piCellBasedIDVars.ptRatioNeut01);
  m_fpanTauCellBased_ptRatioNeut02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut02",
									tau,
									m_ftau_mu,
									m_piCellBasedIDVars.ptRatioNeut02);
  m_fpanTauCellBased_ptRatioNeut04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut04",
									tau,
									m_ftau_mu,
									m_piCellBasedIDVars.ptRatioNeut04);
  m_fpanTauCellBased_ptRatioChrg01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg01",
									tau,
									m_ftau_mu,
									m_piCellBasedIDVars.ptRatioChrg01);
  m_fpanTauCellBased_ptRatioChrg02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg02",
									tau,
									m_ftau_mu,
									     m_piCellBasedIDVars.ptRatioChrg02);
  m_fpanTauCellBased_ptRatioChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg04",
									     tau,
									     m_ftau_mu,
									     m_piCellBasedIDVars.ptRatioChrg04);

  m_fpanTauCellBased_nNeut01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut01",
								       tau,
								       m_ftau_mu,
								       m_piCellBasedIDVars.nNeut01);
  m_fpanTauCellBased_nNeut02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut02",
								       tau,
								       m_ftau_mu,
								       m_piCellBasedIDVars.nNeut02);
  m_fpanTauCellBased_nNeut0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut0204",
									 tau,
									 m_ftau_mu,
									 m_piCellBasedIDVars.nNeut0204);
  m_fpanTauCellBased_visTauM01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM01",
									 tau,
									 m_ftau_mu,
									 m_piCellBasedIDVars.visTauM01 / GeV);
  m_fpanTauCellBased_visTauM02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM02",
									 tau,
									 m_ftau_mu,
									 m_piCellBasedIDVars.visTauM02 / GeV);
  m_fpanTauCellBased_visTauM04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM04",
									 tau,
									 m_ftau_mu,
									 m_piCellBasedIDVars.visTauM04 / GeV);
  m_fpanTauCellBased_dRminmaxPtChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRminmaxPtChrg04",
										tau,
										m_ftau_mu,
										m_piCellBasedIDVars.dRminmaxPtChrg04);

}
  
void TTNTCycle::StartInputData(const SInputData& id) throw(SError){
  CycleBase::StartInputData(id);
  DeclareVariables();
  SetFullPathName(c_pileup_correction_file);
  m_pileUpCorrectionTool->setInputFile(c_pileup_correction_file);
  m_pileUpCorrectionTool->switchOff(c_pileup_correction_switchOff);
  m_pileUpCorrectionTool->readInput();
}

void TTNTCycle::DeclareVariables(){
  DeclareVariable(m_itau_n, "tau_n", "tau");
  DeclareVariable(m_ftau_calcVars_corrCentFrac, "tau_calcVars_corrCentFrac", "tau");
  DeclareVariable(m_ftau_calcVars_corrFTrk, "tau_calcVars_corrFTrk", "tau");
  DeclareVariable(m_ftau_ipSigLeadTrk, "tau_ipSigLeadTrk", "tau");
  DeclareVariable(m_ftau_massTrkSys, "tau_massTrkSys", "tau");
  DeclareVariable(m_ftau_pi0_visTau_m, "tau_pi0_visTau_m", "tau");
  DeclareVariable(m_ftau_ptRatio, "tau_ptRatio", "tau");
  DeclareVariable(m_ftau_seedCalo_trkAvgDist, "tau_seedCalo_trkAvgDist", "tau");
  DeclareVariable(m_ftau_seedCalo_dRmax, "tau_seedCalo_dRmax", "tau"); 
  DeclareVariable(m_ftau_trFlightPathSig, "tau_trFlightPathSig", "tau");
  DeclareVariable(m_itau_pi0_n, "tau_pi0_n", "tau");
  DeclareVariable(m_itau_seedCalo_wideTrk_n, "tau_seedCalo_wideTrk_n", "tau");
  DeclareVariable(m_ftau_m, "tau_m", "tau");
  DeclareVariable(m_ftau_pt, "tau_Et", "tau");
  DeclareVariable(m_ftau_eta, "tau_eta", "tau");
  DeclareVariable(m_ftau_phi, "tau_phi", "tau");
  DeclareVariable(m_itau_numTrack, "tau_numTrack", "tau");
  DeclareVariable(m_itau_nVtx, "nVtx", "tau");
  DeclareVariable(m_ftau_mu, "mu", "tau");
  DeclareVariable(m_itau_EventNumber, "EventNumber", "tau");
  
  DeclareVariable(m_fpanTauCellBased_eFrac0102, "panTauCellBased_eFrac0102", "tau");
  DeclareVariable(m_fpanTauCellBased_eFrac0204, "panTauCellBased_eFrac0204", "tau");
  DeclareVariable(m_fpanTauCellBased_eFrac0104, "panTauCellBased_eFrac0104", "tau");
  DeclareVariable(m_fpanTauCellBased_eFracChrg0204, "panTauCellBased_eFracChrg0204", "tau");
  DeclareVariable(m_fpanTauCellBased_eFracChrg0104, "panTauCellBased_eFracChrg0104", "tau");
  DeclareVariable(m_fpanTauCellBased_eFracNeut0204, "panTauCellBased_eFracNeut0204", "tau");
  DeclareVariable(m_fpanTauCellBased_eFracNeut0104, "panTauCellBased_eFracNeut0104", "tau");
  DeclareVariable(m_fpanTauCellBased_fLeadChrg01, "panTauCellBased_fLeadChrg01", "tau");
  DeclareVariable(m_fpanTauCellBased_fLeadChrg02, "panTauCellBased_fLeadChrg02", "tau");
  DeclareVariable(m_fpanTauCellBased_fLeadChrg04, "panTauCellBased_fLeadChrg04", "tau");
  DeclareVariable(m_fpanTauCellBased_trFlightPathSig, "panTauCellBased_trFlightPathSig", "tau");
  DeclareVariable(m_fpanTauCellBased_ipSigLeadTrk, "panTauCellBased_ipSigLeadTrk", "tau");
  DeclareVariable(m_fpanTauCellBased_massChrgSys01, "panTauCellBased_massChrgSys01", "tau");
  DeclareVariable(m_fpanTauCellBased_massChrgSys02, "panTauCellBased_massChrgSys02", "tau");
  DeclareVariable(m_fpanTauCellBased_massChrgSys04, "panTauCellBased_massChrgSys04", "tau");
  DeclareVariable(m_fpanTauCellBased_massNeutSys01, "panTauCellBased_massNeutSys01", "tau");
  DeclareVariable(m_fpanTauCellBased_massNeutSys02, "panTauCellBased_massNeutSys02", "tau");
  DeclareVariable(m_fpanTauCellBased_massNeutSys04, "panTauCellBased_massNeutSys04", "tau");
  DeclareVariable(m_fpanTauCellBased_rCal02, "panTauCellBased_rCal02", "tau");
  DeclareVariable(m_fpanTauCellBased_rCal04, "panTauCellBased_rCal04", "tau");
  DeclareVariable(m_fpanTauCellBased_rCalChrg02, "panTauCellBased_rCalChrg02", "tau");
  DeclareVariable(m_fpanTauCellBased_rCalChrg04, "panTauCellBased_rCalChrg04", "tau");
  DeclareVariable(m_fpanTauCellBased_rCalNeut02, "panTauCellBased_rCalNeut02", "tau");
  DeclareVariable(m_fpanTauCellBased_rCalNeut04, "panTauCellBased_rCalNeut04", "tau");
  DeclareVariable(m_fpanTauCellBased_dRmax02, "panTauCellBased_dRmax02", "tau");
  DeclareVariable(m_fpanTauCellBased_dRmax04, "panTauCellBased_dRmax04", "tau");
  DeclareVariable(m_fpanTauCellBased_nChrg01, "panTauCellBased_nChrg01", "tau");
  DeclareVariable(m_fpanTauCellBased_nChrg02, "panTauCellBased_nChrg02", "tau");
  DeclareVariable(m_fpanTauCellBased_nChrg0204, "panTauCellBased_nChrg0204", "tau");
  DeclareVariable(m_fpanTauCellBased_ptRatio01, "panTauCellBased_ptRatio01", "tau");
  DeclareVariable(m_fpanTauCellBased_ptRatio02, "panTauCellBased_ptRatio02", "tau");
  DeclareVariable(m_fpanTauCellBased_ptRatio04, "panTauCellBased_ptRatio04", "tau");
  DeclareVariable(m_fpanTauCellBased_ptRatioNeut01, "panTauCellBased_ptRatioNeut01", "tau");
  DeclareVariable(m_fpanTauCellBased_ptRatioNeut02, "panTauCellBased_ptRatioNeut02", "tau");
  DeclareVariable(m_fpanTauCellBased_ptRatioNeut04, "panTauCellBased_ptRatioNeut04", "tau");
  DeclareVariable(m_fpanTauCellBased_ptRatioChrg01, "panTauCellBased_ptRatioChrg01", "tau");
  DeclareVariable(m_fpanTauCellBased_ptRatioChrg02, "panTauCellBased_ptRatioChrg02", "tau");
  DeclareVariable(m_fpanTauCellBased_ptRatioChrg04, "panTauCellBased_ptRatioChrg04", "tau");
  DeclareVariable(m_fpanTauCellBased_nNeut01, "panTauCellBased_nNeut01", "tau");
  DeclareVariable(m_fpanTauCellBased_nNeut02, "panTauCellBased_nNeut02", "tau");
  DeclareVariable(m_fpanTauCellBased_nNeut0204, "panTauCellBased_nNeut0204", "tau");
  DeclareVariable(m_fpanTauCellBased_visTauM01, "panTauCellBased_visTauM01", "tau");
  DeclareVariable(m_fpanTauCellBased_visTauM02, "panTauCellBased_visTauM02", "tau");
  DeclareVariable(m_fpanTauCellBased_visTauM04, "panTauCellBased_visTauM04", "tau");
  DeclareVariable(m_fpanTauCellBased_dRminmaxPtChrg04, "panTauCellBased_dRminmaxPtChrg04", "tau");
}

#endif // TTNTCycle_CXX
#endif

