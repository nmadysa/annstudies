#ifndef SKIM
#ifndef EvaluationHelpers_CXX
#define EvaluationHelpers_CXX

//STL includes
#include <iostream>
#include <assert.h>
#include <math.h>
#include <iterator>
//BOOST includes
#include "boost/assign/std/vector.hpp"
#include "boost/math/tr1.hpp"
//Custom includes
#include "EvaluationHelpers.h"

//ROOT includes
#include "TH3F.h"
#include "TMath.h"

using namespace boost::assign;
using namespace boost::math;

EvaluationHelpers::EvaluationHelpers(const std::string& fNSignal) {
    fSignal = TFile::Open(fNSignal.c_str());
    assert(fSignal->IsOpen());
    SetProngMode();
}

EvaluationHelpers::EvaluationHelpers(const std::string& fNSignal, const std::string& fNData) {
    fSignal = TFile::Open(fNSignal.c_str());
    fData = TFile::Open(fNData.c_str());
    assert(fSignal->IsOpen());
    assert(fData->IsOpen());
    h_LLHScore_pT_signal = (TH3F*) fSignal->Get("CombinationTool/LHScore");
    h_LLHScore_pT_data = (TH3F*) fData->Get("CombinationTool/LHScore");
    h_truth_pT_signal = dynamic_cast<TH3F*> (fSignal->Get("h_truthPt"));
    h_truth_pT_data = dynamic_cast<TH3F*> (fData->Get("h_truthPt"));
    assert(h_LLHScore_pT_signal != 0);
    assert(h_LLHScore_pT_data != 0);
    assert(h_truth_pT_signal != 0);
    assert(h_truth_pT_data != 0);
    h_LLHScore_pT_signal->SetName("LLHScore_pT_signal");
    h_LLHScore_pT_data->SetName("LLHScore_pT_data");
    h_truth_pT_signal->SetName("truth_pT_signal");
    h_truth_pT_data->SetName("truth_pT_data");
}

void EvaluationHelpers::InitializeDetermineCuts() {
    h_LLHScore_pT_signal = (TH3F*) fSignal->Get("CombinationTool/LHScore");
    h_truth_pT_signal = dynamic_cast<TH3F*> (fSignal->Get("h_truthPt"));
    h_TruthPt_OfflinePt = dynamic_cast<TH2F*> (fSignal->Get("h_TruthpT_OfflinepT"));
    assert(h_LLHScore_pT_signal != 0);
    assert(h_truth_pT_signal != 0);
}

void EvaluationHelpers::InitializeEfficiencyCalculation() {
    h_total_inclusive = dynamic_cast<TH3F*> (fSignal->Get("h_total"));
    h_passed_inclusive = dynamic_cast<TH3F*> (fSignal->Get("h_passed"));
    h_passed_defaultllh_inclusive = dynamic_cast<TH3F*> (fSignal->Get("h_passed_defaultllh"));
    h_passed_defaultbdt_inclusive = dynamic_cast<TH3F*> (fSignal->Get("h_passed_defaultbdt"));
    h_passed_defaultcut_inclusive = dynamic_cast<TH3F*> (fSignal->Get("h_passed_defaultcut"));
    h_truth_pT = dynamic_cast<TH3F*> (fSignal->Get("h_truthPt"));
    assert(h_passed_inclusive != 0);
    assert(h_total_inclusive != 0);
    assert(h_truth_pT != 0);
}

std::vector<TGraphAsymmErrors*> EvaluationHelpers::CalculateEfficiency(std::vector<int> * const vPtBins, std::vector<int> * const vNVtxBins, const std::string& id) {
    std::vector<TGraphAsymmErrors*> vGraphs;
    std::vector<int> rebinBins;
    rebinBins += 5, 25;
    //rebinBins += 5, 20;
    //rebinBins += 10, 50;
    TH3F* h_passed;
    TH3F* h_total;
    h_total = dynamic_cast<TH3F*> (h_total_inclusive->Clone());
    if (id.find("defaultllh") != std::string::npos)
        h_passed = dynamic_cast<TH3F*> (h_passed_defaultllh_inclusive->Clone());
    else if (id.find("defaultbdt") != std::string::npos)
        h_passed = dynamic_cast<TH3F*> (h_passed_defaultbdt_inclusive->Clone());
    else if (id.find("defaultcut") != std::string::npos)
        h_passed = dynamic_cast<TH3F*> (h_passed_defaultcut_inclusive->Clone());
    else
        h_passed = h_passed_inclusive;

    assert(h_passed != 0);
    assert(h_total != 0);
    assert(h_truth_pT != 0);

        TFile* myFile = new TFile("Test_norm.root","RECREATE");
        myFile->cd();
        
        h_truth_pT->Write(h_truth_pT->GetName());
    
    std::vector<int>::iterator it = vPtBins->begin();
    for (; it != vPtBins->end(); ++it) {
        TH1D* passed_loose = GetProjectionY(h_passed, 1, 1, 0, -1);
        TH1D* passed_medium = GetProjectionY(h_passed, 2, 2, 0, -1);
        TH1D* passed_tight = GetProjectionY(h_passed, 3, 3, 0, -1);
        TH1D* total_1D = h_truth_pT->ProjectionY(Form("%s_%i_%i_total", id.c_str(), 0, *it), m_nprong_min, m_nprong_max, 0, -1);
        int rebin = rebinBins.at(std::distance(vPtBins->begin(), it));
        passed_loose->Rebin(rebin);
        passed_medium->Rebin(rebin);
        passed_tight->Rebin(rebin);
        total_1D->Rebin(rebin);
        passed_loose->SetName(Form("efficiency_loose_%i", *it));
        passed_medium->SetName(Form("efficiency_medium_%i", *it));
        passed_tight->SetName(Form("efficiency_tight_%i", *it));
        vGraphs.push_back(CalculateEfficiency(passed_loose, total_1D));
        vGraphs.push_back(CalculateEfficiency(passed_medium, total_1D));
        vGraphs.push_back(CalculateEfficiency(passed_tight, total_1D));
        
                total_1D->Write(total_1D->GetName());
                passed_loose->Write(Form("passed_loose_%i", *it));
                passed_medium->Write(Form("passed_medium_%i", *it));
                passed_tight->Write(Form("passed_tight_%i", *it));
                vGraphs.at(0)->Write(Form("vGraph_loose_%i", *it));
                vGraphs.at(1)->Write(Form("vGraph_medium_%i", *it));
                vGraphs.at(2)->Write(Form("vGraph_tight_%i", *it));
        
        delete passed_loose;
        delete passed_medium;
        delete passed_tight;

    }
    
    return vGraphs;
}

std::vector<TGraphAsymmErrors*> EvaluationHelpers::CalculateEfficiencyVsMu(std::vector<int> * const vPtBins, std::vector<int> * const vNVtxBins, const std::string& id) {
    std::vector<TGraphAsymmErrors*> vGraphs;
    std::vector<int> rebinBins;
    rebinBins += 5, 25;
    TH3F* h_passed;
    TH3F* h_total;
    h_total = dynamic_cast<TH3F*> (h_total_inclusive->Clone());
    if (id.find("defaultllh") != std::string::npos)
        h_passed = dynamic_cast<TH3F*> (h_passed_defaultllh_inclusive->Clone());
    else if (id.find("defaultbdt") != std::string::npos)
        h_passed = dynamic_cast<TH3F*> (h_passed_defaultbdt_inclusive->Clone());
    else if (id.find("defaultcut") != std::string::npos)
        h_passed = dynamic_cast<TH3F*> (h_passed_defaultcut_inclusive->Clone());
    else
        h_passed = h_passed_inclusive;

    assert(h_passed != 0);
    assert(h_total != 0);
    assert(h_truth_pT != 0);
    std::vector<int>::iterator it = vPtBins->begin();
    for (; it != vPtBins->end(); ++it) {
		// Set lower bound to 20 GeV to ensure not to pick up bins below the reco pt 20 GeV cut (otherwise the overall eff. would be to low)
        TH1D* passed_loose = GetProjectionZ(h_passed, 1, 1, 20, -1);
        TH1D* passed_medium = GetProjectionZ(h_passed, 2, 2, 20, -1);
        TH1D* passed_tight = GetProjectionZ(h_passed, 3, 3, 20, -1);
        TH1D* total_1D = h_truth_pT->ProjectionZ(Form("%s_%i_%i_total", id.c_str(), 0, *it), m_nprong_min, m_nprong_max, 20, -1);
       
		// Uncommend these line to rebin the Nvtx graphs ----
		std::cout << "Perform Rebin" << std::endl;
		double xBins[27] = {0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5, 14.5, 15.5, 16.5, 17.5, 18.5, 19.5, 20.5, 21.5, 22.5, 23.5, 25.5, 30.5, 39.5};
		passed_loose = (TH1D*)passed_loose->Rebin(26, "loose_rebin", xBins);
		passed_medium = (TH1D*)passed_medium->Rebin(26, "medium_rebin", xBins);
		passed_tight = (TH1D*)passed_tight->Rebin(26, "tight_rebin", xBins);
		total_1D = (TH1D*)total_1D->Rebin(26, "total_1D_rebin", xBins);
		// --------------------------------------------------

		passed_loose->SetName(Form("efficiency_loose_%i", *it));
        passed_medium->SetName(Form("efficiency_medium_%i", *it));
        passed_tight->SetName(Form("efficiency_tight_%i", *it));
        vGraphs.push_back(CalculateEfficiency(passed_loose, total_1D));
        vGraphs.push_back(CalculateEfficiency(passed_medium, total_1D));
        vGraphs.push_back(CalculateEfficiency(passed_tight, total_1D));
        delete passed_loose;
        delete passed_medium;
        delete passed_tight;
    }
    return vGraphs;
}

TGraphAsymmErrors* EvaluationHelpers::CalculateEfficiency(const TH1D* passed, const TH1D* total) {
    TGraphAsymmErrors* graph = new TGraphAsymmErrors(passed, total);
    graph->SetName(passed->GetName());
    return graph;
}

std::vector<TGraphErrors*> EvaluationHelpers::GetEffVsRej(std::vector<int> * const vPtBins, std::vector<int> * const vNVtxBins) {
    std::vector<TGraphErrors*> vHist;
    assert(vPtBins->size() != 0);
    std::vector<int>::const_iterator cit = vPtBins->begin();
    std::vector<int>::const_iterator nvtxit = vNVtxBins->begin();
    for (; cit != vPtBins->end() - 1; ++cit) {
        nvtxit = vNVtxBins->begin();
        for (; nvtxit != vNVtxBins->end() - 1; ++nvtxit) {
            h_LLHScore_signal = GetProjectionY(h_LLHScore_pT_signal, (*cit) + 1, *(cit + 1), (*nvtxit) + 1, *(nvtxit + 1));
            h_LLHScore_data = GetProjectionY(h_LLHScore_pT_data, (*cit) + 1, *(cit + 1), (*nvtxit) + 1, *(nvtxit + 1));
            double norm_signal = GetProjectionX(h_truth_pT_signal, (*cit) + 1, *(cit + 1), 0, -1)->Integral(m_nprong_min, m_nprong_max);
            double norm_data = GetProjectionX(h_truth_pT_data, (*cit) + 1, *(cit + 1), 0, -1)->Integral(m_nprong_min, m_nprong_max);
            CalculateEffVsRej(h_LLHScore_signal, h_LLHScore_data, vHist, norm_signal, norm_data);
        }
    }
    return vHist;
}

//get default performance for comparison

std::vector<TGraphErrors*> EvaluationHelpers::GetEffVsRej(std::vector<int> * const vPtBins, std::vector<int> * const vNVtxBins, const std::string& method) {
    assert(vPtBins->size() != 0);
    h_LLHScore_pT_signal = dynamic_cast<TH3F*> (fSignal->Get(("h_default_pT_" + method).c_str()));
    h_LLHScore_pT_data = dynamic_cast<TH3F*> (fData->Get(("h_default_pT_" + method).c_str()));
    h_LLHScore_pT_signal->SetName(("LLHScore_signal" + method).c_str());
    h_LLHScore_pT_data->SetName(("LLHScore_data" + method).c_str());
    bool doCuts = (method.find("Cut") != std::string::npos);
    assert(h_LLHScore_pT_signal != 0);
    assert(h_LLHScore_pT_data != 0);
    std::vector<TGraphErrors*> vHist;
    std::vector<int>::const_iterator cit = vPtBins->begin();
    std::vector<int>::const_iterator nvtxit = vNVtxBins->begin();
    for (; cit != vPtBins->end() - 1; ++cit) {
        nvtxit = vNVtxBins->begin();
        for (; nvtxit != vNVtxBins->end() - 1; ++nvtxit) {
            h_LLHScore_signal = GetProjectionY(h_LLHScore_pT_signal, (*cit) + 1, *(cit + 1), (*nvtxit) + 1, *(nvtxit + 1));
            h_LLHScore_data = GetProjectionY(h_LLHScore_pT_data, (*cit) + 1, *(cit + 1), (*nvtxit) + 1, *(nvtxit + 1));
            
            double norm_signal = GetProjectionX(h_truth_pT_signal, (*cit) + 1, *(cit + 1), 0, -1)->Integral(m_nprong_min, m_nprong_max);
            double norm_data = GetProjectionX(h_truth_pT_data, (*cit) + 1, *(cit + 1), 0, -1)->Integral(m_nprong_min, m_nprong_max);
            
            //double norm_signal = GetProjectionX(h_truth_pT_signal, (*cit) + 1, *(cit + 1), 1, 2)->Integral(m_nprong_min, m_nprong_max);
            //double norm_data = GetProjectionX(h_truth_pT_data, (*cit) + 1, *(cit + 1), 1, 2)->Integral(m_nprong_min, m_nprong_min);
            //double norm_signal = GetProjectionX(h_truth_pT_signal, (*cit) + 1, *(cit + 1), (*nvtxit) + 1, *(nvtxit + 1))->Integral(m_nprong_min, m_nprong_max);
            //double norm_data = GetProjectionX(h_truth_pT_data, (*cit) + 1, *(cit + 1), (*nvtxit) + 1, *(nvtxit + 1))->Integral(m_nprong_min, m_nprong_min);
            
            std::cout << "signal: " << h_LLHScore_pT_signal->Integral() << "  data: " << h_LLHScore_pT_data->Integral() << std::endl;
            std::cout << "norm signal: " << norm_signal << "  norm data: " << norm_data << std::endl;
            if (!doCuts)
                CalculateEffVsRej(h_LLHScore_signal, h_LLHScore_data, vHist, norm_signal, norm_data);
            else
                CalculateEffVsRejCut(h_LLHScore_signal, h_LLHScore_data, vHist, norm_signal, norm_data);
        }
    }
    return vHist;
}

void EvaluationHelpers::CalculateEffVsRej(TH1D* h_LLHScore_signal, TH1D* h_LLHScore_data, std::vector<TGraphErrors*>& vHist, double norm_signal, double norm_data) {
    assert(h_LLHScore_signal != 0 && h_LLHScore_data != 0);
	std::vector<double> efficiencies;
    //eff = 1.1 required to avoid out of range
    for (double i = 0.; i < 1.1; i += 0.01)
        efficiencies += i;
    //normalisation
    TGraphErrors* h_EffVsRej = new TGraphErrors();
    h_EffVsRej->SetName(h_LLHScore_signal->GetName());
    double sum = 0.;
    double sum_data = 0.;
    double err = 0.;
    double err_data = 0.;
    int point = 0;
    for (int bin = h_LLHScore_signal->GetNbinsX() + 1; bin >= 0; bin--) {
        sum += h_LLHScore_signal->GetBinContent(bin) / norm_signal;
        sum_data += h_LLHScore_data->GetBinContent(bin) / norm_data;
        err += TMath::Power(h_LLHScore_signal->GetBinError(bin) / norm_signal, 2);
        err_data += TMath::Power(h_LLHScore_data->GetBinError(bin) / norm_data, 2);
        if (sum > efficiencies.at(point)) {
            if (sum_data == 0.)
                sum_data = 0.00000001;
            h_EffVsRej->SetPoint(point, sum, 1. / sum_data);
            h_EffVsRej->SetPointError(point, TMath::Sqrt(err), err_data / TMath::Power(sum_data, 2));
            point++;
        }

    }
    vHist.push_back(h_EffVsRej);
}

void EvaluationHelpers::CalculateEffVsRejCut(TH1D* h_signal, TH1D* h_data, std::vector<TGraphErrors*>& vHist, double norm_signal, double norm_data) {
    //eff = 1.1 required to avoid out of range
    TGraphErrors* h_EffVsRej = new TGraphErrors();
    h_EffVsRej->SetPoint(0, h_signal->GetBinContent(2) / norm_signal, norm_data / h_data->GetBinContent(2));
    vHist.push_back(h_EffVsRej);
}

std::vector<TH1D*> EvaluationHelpers::GetScore(std::vector<int> * const vPtBins, std::vector<int> * const vNVtxBins) {
    std::vector<TH1D*> vHist;
    vHist.push_back(GetProjectionY(h_LLHScore_pT_signal, 0, -1, 0, -1));
    vHist.push_back(GetProjectionY(h_LLHScore_pT_data, 0, -1, 0, -1));
    std::vector<int>::const_iterator cit = vPtBins->begin();
    std::vector<int>::const_iterator nvtxit = vNVtxBins->begin();
    for (; cit != vPtBins->end(); ++cit) {
        nvtxit = vNVtxBins->begin();
        for (; nvtxit != vNVtxBins->end(); ++nvtxit) {
            vHist.push_back(GetProjectionY(h_LLHScore_pT_signal, (*cit) + 1, *(cit + 1), (*nvtxit) + 1, *(nvtxit + 1)));
            vHist.push_back(GetProjectionY(h_LLHScore_pT_data, (*cit) + 1, *(cit + 1), (*nvtxit) + 1, *(nvtxit + 1)));
        }
    }
    return vHist;

}

TGraph* EvaluationHelpers::GetScoreVsEfficiency(TH1D* score, double ptlow, double pthigh, double mulow, double muhigh) {
    TGraph* g = new TGraph();
    int point = 0;
    TH1D* h_truth = GetProjectionX(h_truth_pT_signal, (int) ptlow, (int) pthigh, (int) mulow, (int) muhigh);
    double norm = h_truth->Integral(m_nprong_min, m_nprong_max);
    for (int i = score->GetNbinsX() + 1; i >= 0; i--) {
        g->SetPoint(point, score->Integral(i, -1) / norm, score->GetBinLowEdge(i));
        point++;
    }
    return g;
}

std::vector<TGraph*> EvaluationHelpers::InitializeTGraphs() {
    TGraph* tg_loose = new TGraph();
    TGraph* tg_medium = new TGraph();
    TGraph* tg_tight = new TGraph();
    tg_loose->SetName("loose");
    tg_medium->SetName("medium");
    tg_tight->SetName("tight");
    std::vector<TGraph*> v_graphs;
    v_graphs.push_back(tg_loose);
    v_graphs.push_back(tg_medium);
    v_graphs.push_back(tg_tight);
    return v_graphs;
}

/*std::vector<TGraph*> EvaluationHelpers::DetermineLMTCuts(const std::vector<double>& target_efficiencies, std::vector<double> vFlatPtBins) {
    std::vector<TGraph*> v_graphs = InitializeTGraphs();
    std::vector<double>::iterator it = vFlatPtBins.begin();
    int point = 2;
    std::vector<double>::const_iterator target = target_efficiencies.begin();
    for (; it != vFlatPtBins.end() - 1; ++it) {
        TH1D* score = GetProjectionY(h_LLHScore_pT_signal, (int) (*it) + 1, (int) *(it + 1), 0, -1);
        TGraph* efficiency = GetScoreVsEfficiency(score, (*it) + 1, *(it + 1), 0, -1);
        TH1D* recoPt = GetProjectionY(h_TruthPt_OfflinePt, (int) (*it) + 1, (int) *(it + 1));
        double pt_center = recoPt->GetMean();
        target = target_efficiencies.begin();
        for (; target != target_efficiencies.end(); ++target) {
            int bin = std::distance(target_efficiencies.begin(), target);
            v_graphs.at(bin)->SetPoint(point, pt_center, efficiency->Eval(*target));
            if (it == vFlatPtBins.begin()) {
                v_graphs.at(bin)->SetPoint(0, 0, efficiency->Eval(*target));
                v_graphs.at(bin)->SetPoint(1, 10, efficiency->Eval(*target));
            }
            if (it == vFlatPtBins.end() - 2) {
                v_graphs.at(bin)->SetPoint(point + 1, 1000, efficiency->Eval(*target));
            }
        }
        point++;
    }
    return v_graphs;
}*/

std::vector<TGraph*> EvaluationHelpers::DetermineLMTCuts(const std::vector<double>& target_efficiencies, std::vector<double> vFlatPtBins) {
    std::vector<TGraph*> v_graphs = InitializeTGraphs();
    std::vector<double>::iterator it = vFlatPtBins.begin();
    int point = 2;
    std::vector<double>::const_iterator target = target_efficiencies.begin();
    
        TFile* f = new TFile("Check_Graphs.root","RECREATE");
        TGraph* rms_pt_center = new TGraph();
        f->cd();
    
    for (; it != vFlatPtBins.end() - 1; ++it) {
        TH1D* score = GetProjectionY(h_LLHScore_pT_signal, (int) (*it) + 1, (int) *(it + 1), 0, -1);
        TGraph* efficiency = GetScoreVsEfficiency(score, (*it) + 1, *(it + 1), 0, -1);
        TH1D* recoPt = GetProjectionY(h_TruthPt_OfflinePt, (int) (*it) + 1, (int) *(it + 1));
        double pt_center = recoPt->GetMean();
		//double pt_center = ( ((double) (*it) + 1) + ((double) *(it + 1)) ) / 2.0;
        target = target_efficiencies.begin();
        
                std::cout << "\nNextBin: " << std::endl;
                std::cout << "(*it): " << (*it) << "\t(*it)+1: " << (*it) + 1 << "\t*(it+1): " << *(it+1);
                std::cout << "\tpt_center: " << pt_center << "\tpoint: " << point << std::endl;
                efficiency->Write(Form("ScoreVsEff_Ptmin.%i_Ptmax.%i", (int)((*it)+1), (int)(*(it+1)) ) );
                score->Write(Form("Score_Ptmin.%i_Ptmax.%i", (int)((*it)+1), (int)(*(it+1)) ) );
                recoPt->Write(Form("recoPt_Ptmin.%i_Ptmax.%i", (int)((*it)+1), (int)(*(it+1)) ) );
                rms_pt_center->SetPoint(point, pt_center, recoPt->GetRMS());
        
        for (; target != target_efficiencies.end(); ++target) {
            int bin = std::distance(target_efficiencies.begin(), target);
                        std::cout << "target: " << *target << "\tdistance(bin): " << bin << "\tEval: " << efficiency->Eval(*target) << std::endl;
            v_graphs.at(bin)->SetPoint(point, pt_center, efficiency->Eval(*target));
            if (it == vFlatPtBins.begin()) {
                v_graphs.at(bin)->SetPoint(0, 0, efficiency->Eval(*target));
                v_graphs.at(bin)->SetPoint(1, 10, efficiency->Eval(*target));
            }
            if (it == vFlatPtBins.end() - 2) {
                v_graphs.at(bin)->SetPoint(point + 1, 1000, efficiency->Eval(*target));
            }
        }
        point++;
    }
        rms_pt_center->Write("rms_pt_center");
        delete rms_pt_center;
        f->Close();
        
    return v_graphs;
}


TH1D* EvaluationHelpers::GetProjectionX(TH3F * const hist, const int& xlow, const int& xhigh, const int& zlow, const int& zhigh, const std::string& hName) {
    return hist->ProjectionX(Form("%s_%s_%i_%i_%i_%i", hist->GetName(), hName.c_str(), xlow, xhigh, zlow, zhigh), xlow, xhigh, zlow, zhigh);
}

TH1D* EvaluationHelpers::GetProjectionY(TH3F * const hist, const int& xlow, const int& xhigh, const int& zlow, const int& zhigh, const std::string& hName) {
    return hist->ProjectionY(Form("%s_%s_%i_%i_%i_%i", hist->GetName(), hName.c_str(), xlow, xhigh, zlow, zhigh), xlow, xhigh, zlow, zhigh);
}

TH1D* EvaluationHelpers::GetProjectionZ(TH3F * const hist, const int& xlow, const int& xhigh, const int& ylow, const int& yhigh) {
    return hist->ProjectionZ(Form("%s_%i_%i_%i_%i", hist->GetName(), xlow, xhigh, ylow, yhigh), xlow, xhigh, ylow, yhigh);
}

TH1D* EvaluationHelpers::GetProjectionY(TH2F * const hist, const int& xlow, const int& xhigh) {
    return hist->ProjectionY(Form("%s_%i_%i", hist->GetName(), xlow, xhigh), xlow, xhigh);
}


void EvaluationHelpers::SetProngMode(const int& prongmode) {
    if (prongmode == 1) {
        m_nprong_min = 1;
        m_nprong_max = 2;
    } else if (prongmode == 2) {
        m_nprong_min = 2;
        m_nprong_max = 4;
    } else if (prongmode == 3) {
        m_nprong_min = 3;
        m_nprong_max = 4;
    } else
        std::cerr << "Non valid prong initialization" << std::endl;
}
#endif
#endif //SKIM
