#ifdef PI0BONN
#ifndef TNTCycle_CXX
#define TNTCycle_CXX

// system include(s)
#include "assert.h"
#include <set>

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

// core include(s)
#include "Region.h"
#include "Utilities.h"
#include "BoundHistogram.h"

// TauCommon include(s)
#include "Helpers.h"
#include "VertexSelector.h"

// custom include(s)
#include "TNTCycle.h"

ClassImp(TNTCycle);

using namespace TauCommon_Helpers;

TNTCycle::TNTCycle() :
  LLHCommon()
{}

TNTCycle::~TNTCycle() {
  // clear memory
  m_oVertexCreator.ClearAllObjects();
  m_oJetCreator.ClearAllObjects();
  m_oTauCreator.ClearAllObjects();
  m_oTruthTauCreator.ClearAllObjects();
}

void TNTCycle::AnalyseEvent(const SInputData& id, double dWeight) throw (SError) {
  // clear memory
  m_oVertexCreator.ClearAllObjects();
  m_oJetCreator.ClearAllObjects();
  m_oTauCreator.ClearAllObjects();
  m_oTruthTauCreator.ClearAllObjects();

  // create analysis objects for current event
  if(!IsData())
    m_oTruthTauCreator.CreateObjects(m_rTruthTauCollection.n(),m_rTruthTauCollection,m_vTruthTaus);
  m_oVertexCreator.CreateObjects(m_rVertexCollection.n(), m_rVertexCollection, m_vVertices);
  m_oJetCreator.CreateObjects(m_rJetCollection.n(), m_rJetCollection, m_vJets);
  m_oTauCreator.CreateObjects(m_rTauCollection.n(), m_rTauCollection, m_vTaus);

  // reset event weight
  m_fEventWeight = 1;
  if (!IsData()) {
    unsigned int i = 0;
    while (i < m_rEventInfo.mcevt_weight()->size()) {
      if (m_rEventInfo.mcevt_weight()->at(i).size()) {
	m_fEventWeight = m_rEventInfo.mcevt_weight()->at(i).at(0);
	break;
      }
      else
	++i;
    }
  }

  // sort objects according to decreasing pt
  if(!IsData())
    std::sort(m_vTruthTaus.begin(),m_vTruthTaus.end(),Particle::pt_comp);
  std::sort(m_vJets.begin(), m_vJets.end(), Particle::pt_comp);
  std::sort(m_vTaus.begin(), m_vTaus.end(), Particle::pt_comp);

  // Select PhysicsObjects
  SelectPhysicsObjects(m_fEventWeight);

  // do baseline selection
  dynamic_cast<Region*> (GetTool("BaselineSelection"))->IsInRegion(m_fEventWeight, true);
  CalculateVariables();
  TTree* pTree = GetOutputMetadataTree("tau");
  pTree->Fill();   
}

void TNTCycle::StartInputFile(const SInputData& id) throw (SError) {
  // read from tree
  m_rEventInfo.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_rTruthTauCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_rTauCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_rJetCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_rVertexCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
}

void TNTCycle::CalculateVariables(){
  m_itau_n = m_vTaus.size();
  m_itrueTau_n = m_vTruthTaus.size();
  m_inVtx = m_vVertices.size();
  m_fmu = m_rEventInfo.averageIntPerXing();
  m_ilbn = m_rEventInfo.lbn();
  m_iRunNumber = m_rEventInfo.RunNumber();
  m_iEventNumber = m_rEventInfo.EventNumber();

  m_vtau_Et.clear();
  m_vtau_eta.clear();
  m_vtau_m.clear();
  m_vtau_phi.clear();
  m_vtau_charge.clear();
  m_vtau_author.clear();
  m_vtau_numTrack.clear();
  m_vtau_hasTruthMatch.clear();
  m_vtau_likelihood.clear();

  m_vtau_BDTJetScore.clear();
  m_vtau_SafeLikelihood.clear();
  m_vtau_LLH_loose.clear();
  m_vtau_LLH_medium.clear();
  m_vtau_LLH_tight.clear();  
  m_vtau_BDT_loose.clear();
  m_vtau_BDT_medium.clear();
  m_vtau_BDT_tight.clear();

  m_vtau_ipSigLeadTrk.clear();
  m_vtau_massTrkSys.clear();
  m_vtau_trFlightPathSig.clear();
  m_vtau_seedCalo_trkAvgDist.clear();
  m_vtau_seedCalo_dRmax.clear();
  m_vtau_calcVars_corrCentFrac.clear();
  m_vtau_calcVars_corrFTrk.clear();
  m_vtau_leadTrack_eta.clear();
  m_vtau_seedCalo_wideTrk_n.clear();
  m_vtau_etOverPtLeadTrk.clear();
  m_vtau_ptRatio.clear();
  m_vtau_pi0_vistau_m.clear();
  m_vtau_pi0_n.clear();

  m_vtau_matchVisEt.clear();
  m_vtau_matchVisEta.clear();
  m_vtau_matchVisPhi.clear();
  m_vtau_matchVisM.clear();
  m_vtau_matchPt.clear();
  m_vtau_matchEta.clear();
  m_vtau_matchPhi.clear();
  m_vtau_matchM.clear();
  m_vtau_matchNProng.clear();

  m_vtruth_VisEt.clear();
  m_vtruth_VisEta.clear();
  m_vtruth_VisPhi.clear();
  m_vtruth_VisM.clear();
  m_vtruth_Pt.clear();
  m_vtruth_Eta.clear();
  m_vtruth_Phi.clear();
  m_vtruth_M.clear();
  m_vtruth_NProng.clear();

  m_vtau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmean.clear();
  m_vtau_pi0Bonn_nPi0Cluster.clear();
  m_vtau_seedCalo_wideTrk_expectBLayerHit.clear();
  m_vtau_seedCalo_wideTrk_nTRTHighTOutliers.clear();
  m_vtau_pi0Bonn_Pi0Cluster_noCorr_pt.clear();
  m_vtau_pi0Bonn_TauShot_Shot_mergedScore.clear();
  m_vtau_seedCalo_wideTrk_nGangedFlaggedFakes.clear();
  m_vtau_seedCalo_wideTrk_nTRTXenonHits.clear();
  m_vtau_seedCalo_wideTrk_atTJVA_n.clear();
  m_vtau_seedCalo_wideTrk_nSCTOutliers.clear();
  m_vtau_seedCalo_wideTrk_qoverp.clear();
  m_vtau_pi0Bonn_pi0_E.clear();
  m_vtau_pi0Bonn_TauShot_Shot_ws5.clear();
  m_vtau_pi0Bonn_ShotsInPi0Clusters.clear();
  m_vtau_pi0Bonn_nPi0.clear();
  m_vtau_pi0Bonn_Pi0Cluster_BDTScore.clear();
  m_vtau_seedCalo_wideTrk_phi.clear();
  m_vtau_seedCalo_wideTrk_nSCTDeadSensors.clear();
  m_vtau_pi0Bonn_Pi0Cluster_CENTER_LAMBDA_helped.clear();
  m_vtau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmode.clear();
  m_vtau_pi0Bonn_Pi0ClustersInPi0Candidates.clear();
  m_vtau_pi0Bonn_TauShot_Shot_nPhotons.clear();
  m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_MAX.clear();
  m_vtau_seedCalo_wideTrk_atTJVA_phi.clear();
  m_vtau_seedCalo_wideTrk_nPixelSpoiltHits.clear();
  m_vtau_pi0Bonn_TauShot_nShot.clear();
  m_vtau_pi0Bonn_visTau_pt.clear();
  m_vtau_seedCalo_wideTrk_atTJVA_pt.clear();
  // m_vtau_seedCalo_wideTrk_n.clear();
  m_vtau_pi0Bonn_visTau_E.clear();
  m_vtau_pi0Bonn_nShotsInPi0Clusters.clear();
  m_vtau_pi0Bonn_Pi0Cluster_noCorr_phi.clear();
  m_vtau_seedCalo_wideTrk_nPixSharedHits.clear();
  m_vtau_pi0Bonn_visTau_eta.clear();
  m_vtau_seedCalo_wideTrk_nPixelDeadSensors.clear();
  m_vtau_seedCalo_wideTrk_nBLayerOutliers.clear();
  m_vtau_pi0Bonn_TauShot_Shot_cellEta.clear();
  m_vtau_pi0Bonn_Pi0Cluster_log_SECOND_ENG_DENS.clear();
  m_vtau_pi0Bonn_Pi0Cluster_FIRST_ETA.clear();
  m_vtau_seedCalo_wideTrk_pt.clear();
  m_vtau_pi0Bonn_TauShot_Shot_cellPt.clear();
  m_vtau_seedCalo_wideTrk_atPV_phi.clear();
  m_vtau_seedCalo_wideTrk_nContribPixelLayers.clear();
  m_vtau_pi0Bonn_Pi0Cluster_eta.clear();
  m_vtau_pi0Bonn_Pi0Cluster_noCorr_eta.clear();
  m_vtau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM1.clear();
  m_vtau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM2.clear();
  m_vtau_seedCalo_wideTrk_atTJVA_qoverp.clear();
  m_vtau_pi0Bonn_Pi0Cluster_At_DELTA_THETA.clear();
  m_vtau_pi0Bonn_Pi0Cluster_At_LATERAL.clear();
  m_vtau_pi0Bonn_Pi0Cluster_NHitsInEM1.clear();
  m_vtau_pi0Bonn_Pi0Cluster_EcoreOverEEM1.clear();
  m_vtau_pi0Bonn_nPi0ClustersInPi0Candidates.clear();
  m_vtau_seedCalo_wideTrk_nPixHits.clear();
  m_vtau_seedCalo_wideTrk_nBLayerSplitHits.clear();
  m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_EM.clear();
  m_vtau_seedCalo_wideTrk_atTJVA_d0.clear();
  m_vtau_pi0Bonn_Pi0Cluster_NPosECells_EM2.clear();
  m_vtau_pi0Bonn_Pi0Cluster_NPosECells_EM1.clear();
  m_vtau_pi0Bonn_TauShot_Shot_fracSide_3not1.clear();
  m_vtau_pi0Bonn_TauShot_Shot_phi.clear();
  m_vtau_seedCalo_wideTrk_atPV_z0.clear();
  m_vtau_pi0Bonn_sumPi0_pt.clear();
  m_vtau_seedCalo_wideTrk_atPV_qoverp.clear();
  m_vtau_seedCalo_wideTrk_nPixelOutliers.clear();
  m_vtau_seedCalo_wideTrk_nBLSharedHits.clear();
  m_vtau_seedCalo_wideTrk_atPV_pt.clear();
  m_vtau_pi0Bonn_Pi0Cluster_At_LONGITUDINAL.clear();
  m_vtau_pi0Bonn_Pi0Cluster_E.clear();
  m_vtau_pi0Bonn_TauShot_Shot_signalScore.clear();
  m_vtau_pi0Bonn_TauShot_Shot_eta.clear();
  m_vtau_seedCalo_wideTrk_eta.clear();
  m_vtau_seedCalo_wideTrk_nPixSplitHits.clear();
  m_vtau_seedCalo_wideTrk_nHits.clear();
  m_vtau_pi0Bonn_TauShot_Shot_pt5.clear();
  m_vtau_pi0Bonn_sumPi0_E.clear();
  m_vtau_seedCalo_wideTrk_nSCTSpoiltHits.clear();
  m_vtau_pi0Bonn_TauShot_Shot_pt1.clear();
  m_vtau_seedCalo_wideTrk_d0.clear();
  m_vtau_pi0Bonn_TauShot_Shot_pt3.clear();
  m_vtau_seedCalo_wideTrk_atTJVA_eta.clear();
  m_vtau_pi0Bonn_Pi0Cluster_At_DELTA_PHI.clear();
  m_vtau_seedCalo_wideTrk_theta.clear();
  m_vtau_pi0Bonn_Pi0Cluster_noCorr_E.clear();
  m_vtau_pi0Bonn_TauShot_Shot_Fside_5not3.clear();
  m_vtau_seedCalo_wideTrk_atPV_theta.clear();
  m_vtau_pi0Bonn_TauShot_Shot_Fside_5not1.clear();
  m_vtau_pi0Bonn_Pi0Cluster_phi.clear();
  m_vtau_pi0Bonn_Pi0Cluster_SECOND_R.clear();
  m_vtau_pi0Bonn_pi0_phi.clear();
  m_vtau_seedCalo_wideTrk_nBLHits.clear();
  m_vtau_seedCalo_wideTrk_z0.clear();
  m_vtau_pi0Bonn_Pi0Cluster_LONGITUDINAL.clear();
  m_vtau_pi0Bonn_sumPi0_eta.clear();
  m_vtau_seedCalo_wideTrk_nSCTHoles.clear();
  m_vtau_seedCalo_wideTrk_atTJVA_theta.clear();
  m_vtau_pi0Bonn_Pi0Cluster_SECOND_LAMBDA.clear();
  m_vtau_pi0Bonn_pi0_pt.clear();
  m_vtau_pi0Bonn_Pi0Cluster_Abs_DELTA_THETA.clear();
  m_vtau_seedCalo_wideTrk_nSCTHits.clear();
  m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_CORE.clear();
  m_vtau_seedCalo_wideTrk_nTRTHits.clear();
  m_vtau_pi0Bonn_Pi0Cluster_NPosCells_PS.clear();
  m_vtau_pi0Bonn_TauShot_Shot_Fside_3not1.clear();
  m_vtau_pi0Bonn_visTau_phi.clear();
  m_vtau_pi0Bonn_TauShot_Shot_sdevPt5.clear();
  m_vtau_seedCalo_wideTrk_nSCTDoubleHoles.clear();
  m_vtau_seedCalo_wideTrk_atPV_eta.clear();
  m_vtau_pi0Bonn_TauShot_Shot_cellPhi.clear();
  m_vtau_seedCalo_wideTrk_nTRTHoles.clear();
  m_vtau_pi0Bonn_Pi0Cluster_Abs_DELTA_PHI.clear();
  m_vtau_seedCalo_wideTrk_nTRTOutliers.clear();
  m_vtau_pi0Bonn_pi0_eta.clear();
  m_vtau_pi0Bonn_TauShot_Shot_pt1OverPt3.clear();
  m_vtau_seedCalo_wideTrk_atPV_d0.clear();
  m_vtau_seedCalo_wideTrk_atTJVA_z0.clear();
  m_vtau_pi0Bonn_TauShot_Shot_deltaPt12_min.clear();
  m_vtau_pi0Bonn_Pi0Cluster_AsymmetryWRTTrack.clear();
  m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_CORE.clear();
  m_vtau_pi0Bonn_TauShot_Shot_pt.clear();
  m_vtau_seedCalo_wideTrk_nSCTSharedHits.clear();
  m_vtau_seedCalo_wideTrk_nPixHoles.clear();
  m_vtau_pi0Bonn_sumPi0_phi.clear();
  m_vtau_pi0Bonn_TauShot_Shot_fracSide_5not1.clear();
  m_vtau_pi0Bonn_TauShot_Shot_fracSide_5not3.clear();
  m_vtau_seedCalo_wideTrk_nGangedPixels.clear();
  m_vtau_seedCalo_wideTrk_nTRTHighTHits.clear();
  m_vtau_pi0Bonn_Pi0Cluster_At_SECOND_ENG_DENS.clear();
  m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_MAX.clear();
  m_vtau_pi0Bonn_Pi0Cluster_LATERAL.clear();
  m_vtau_pi0Bonn_Pi0Cluster_pt.clear();
  m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_EM.clear();
  m_vtau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM2.clear();
  m_vtau_pi0Bonn_TauShot_Shot_pt3OverPt5.clear();
  m_vtau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM1.clear();
  
  if(!IsData())
    GetTrueTauInfo();
 
  std::vector<D3PDReader::Tau*>::const_iterator cit = m_vTaus.begin();
  for(; cit != m_vTaus.end(); ++cit){
    D3PDReader::Tau* tau = *cit;
    m_vtau_Et.push_back(tau->TLV().Pt());
    m_vtau_eta.push_back(tau->TLV().Eta());
    m_vtau_m.push_back(tau->m());
    m_vtau_phi.push_back(tau->TLV().Phi());
    m_vtau_charge.push_back(tau->charge());
    m_vtau_author.push_back(tau->author());
    m_vtau_numTrack.push_back(tau->numTrack());

    m_vtau_BDTJetScore.push_back(tau->BDTJetScore());
    m_vtau_SafeLikelihood.push_back(tau->SafeLikelihood());
    m_vtau_LLH_loose.push_back(tau->tauLlhLoose());
    m_vtau_LLH_medium.push_back(tau->tauLlhMedium());
    m_vtau_LLH_tight.push_back(tau->tauLlhTight());
    m_vtau_BDT_loose.push_back(tau->JetBDTSigLoose());
    m_vtau_BDT_medium.push_back(tau->JetBDTSigMedium());
    m_vtau_BDT_tight.push_back(tau->JetBDTSigTight());
    m_vtau_likelihood.push_back(tau->likelihood());
    
    m_vtau_ipSigLeadTrk.push_back(tau->ipSigLeadTrk());
    m_vtau_massTrkSys.push_back(tau->massTrkSys());
    m_vtau_trFlightPathSig.push_back(tau->trFlightPathSig());
    m_vtau_seedCalo_trkAvgDist.push_back(tau->seedCalo_trkAvgDist());
    m_vtau_seedCalo_dRmax.push_back(tau->seedCalo_dRmax());
    m_vtau_calcVars_corrCentFrac.push_back(tau->calcVars_corrCentFrac());
    m_vtau_calcVars_corrFTrk.push_back(tau->calcVars_corrFTrk());
    m_vtau_leadTrack_eta.push_back(tau->leadTrack_eta());
    m_vtau_seedCalo_wideTrk_n.push_back(tau->seedCalo_wideTrk_n());
    m_vtau_etOverPtLeadTrk.push_back(tau->etOverPtLeadTrk());
    m_vtau_ptRatio.push_back((tau->pi0_vistau_pt())/(tau->pt()));
    m_vtau_pi0_vistau_m.push_back(tau->pi0_vistau_m());
    m_vtau_pi0_n.push_back(tau->pi0_n());
    
    m_vtau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmean.push_back(tau->pi0Bonn_TauShot_Shot_sdevEta5_WRTmean());
    m_vtau_pi0Bonn_nPi0Cluster.push_back(tau->pi0Bonn_nPi0Cluster());
    m_vtau_seedCalo_wideTrk_expectBLayerHit.push_back(tau->seedCalo_wideTrk_expectBLayerHit());
    m_vtau_seedCalo_wideTrk_nTRTHighTOutliers.push_back(tau->seedCalo_wideTrk_nTRTHighTOutliers());
    m_vtau_pi0Bonn_Pi0Cluster_noCorr_pt.push_back(tau->pi0Bonn_Pi0Cluster_noCorr_pt());
    m_vtau_pi0Bonn_TauShot_Shot_mergedScore.push_back(tau->pi0Bonn_TauShot_Shot_mergedScore());
    m_vtau_seedCalo_wideTrk_nGangedFlaggedFakes.push_back(tau->seedCalo_wideTrk_nGangedFlaggedFakes());
    m_vtau_seedCalo_wideTrk_nTRTXenonHits.push_back(tau->seedCalo_wideTrk_nTRTXenonHits());
    m_vtau_seedCalo_wideTrk_atTJVA_n.push_back(tau->seedCalo_wideTrk_atTJVA_n());
    m_vtau_seedCalo_wideTrk_nSCTOutliers.push_back(tau->seedCalo_wideTrk_nSCTOutliers());
    m_vtau_seedCalo_wideTrk_qoverp.push_back(tau->seedCalo_wideTrk_qoverp());
    m_vtau_pi0Bonn_pi0_E.push_back(tau->pi0Bonn_pi0_E());
    m_vtau_pi0Bonn_TauShot_Shot_ws5.push_back(tau->pi0Bonn_TauShot_Shot_ws5());
    m_vtau_pi0Bonn_ShotsInPi0Clusters.push_back(tau->pi0Bonn_ShotsInPi0Clusters());
    m_vtau_pi0Bonn_nPi0.push_back(tau->pi0Bonn_nPi0());
    m_vtau_pi0Bonn_Pi0Cluster_BDTScore.push_back(tau->pi0Bonn_Pi0Cluster_BDTScore());
    m_vtau_seedCalo_wideTrk_phi.push_back(tau->seedCalo_wideTrk_phi());
    m_vtau_seedCalo_wideTrk_nSCTDeadSensors.push_back(tau->seedCalo_wideTrk_nSCTDeadSensors());
    m_vtau_pi0Bonn_Pi0Cluster_CENTER_LAMBDA_helped.push_back(tau->pi0Bonn_Pi0Cluster_CENTER_LAMBDA_helped());
    m_vtau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmode.push_back(tau->pi0Bonn_TauShot_Shot_sdevEta5_WRTmode());
    m_vtau_pi0Bonn_Pi0ClustersInPi0Candidates.push_back(tau->pi0Bonn_Pi0ClustersInPi0Candidates());
    m_vtau_pi0Bonn_TauShot_Shot_nPhotons.push_back(tau->pi0Bonn_TauShot_Shot_nPhotons());
    m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_MAX.push_back(tau->pi0Bonn_Pi0Cluster_ENG_FRAC_MAX());
    m_vtau_seedCalo_wideTrk_atTJVA_phi.push_back(tau->seedCalo_wideTrk_atTJVA_phi());
    m_vtau_seedCalo_wideTrk_nPixelSpoiltHits.push_back(tau->seedCalo_wideTrk_nPixelSpoiltHits());
    m_vtau_pi0Bonn_TauShot_nShot.push_back(tau->pi0Bonn_TauShot_nShot());
    m_vtau_pi0Bonn_visTau_pt.push_back(tau->pi0Bonn_visTau_pt());
    m_vtau_seedCalo_wideTrk_atTJVA_pt.push_back(tau->seedCalo_wideTrk_atTJVA_pt());
    // m_vtau_seedCalo_wideTrk_n.push_back(tau->seedCalo_wideTrk_n());
    m_vtau_pi0Bonn_visTau_E.push_back(tau->pi0Bonn_visTau_E());
    m_vtau_pi0Bonn_nShotsInPi0Clusters.push_back(tau->pi0Bonn_nShotsInPi0Clusters());
    m_vtau_pi0Bonn_Pi0Cluster_noCorr_phi.push_back(tau->pi0Bonn_Pi0Cluster_noCorr_phi());
    m_vtau_seedCalo_wideTrk_nPixSharedHits.push_back(tau->seedCalo_wideTrk_nPixSharedHits());
    m_vtau_pi0Bonn_visTau_eta.push_back(tau->pi0Bonn_visTau_eta());
    m_vtau_seedCalo_wideTrk_nPixelDeadSensors.push_back(tau->seedCalo_wideTrk_nPixelDeadSensors());
    m_vtau_seedCalo_wideTrk_nBLayerOutliers.push_back(tau->seedCalo_wideTrk_nBLayerOutliers());
    m_vtau_pi0Bonn_TauShot_Shot_cellEta.push_back(tau->pi0Bonn_TauShot_Shot_cellEta());
    m_vtau_pi0Bonn_Pi0Cluster_log_SECOND_ENG_DENS.push_back(tau->pi0Bonn_Pi0Cluster_log_SECOND_ENG_DENS());
    m_vtau_pi0Bonn_Pi0Cluster_FIRST_ETA.push_back(tau->pi0Bonn_Pi0Cluster_FIRST_ETA());
    m_vtau_seedCalo_wideTrk_pt.push_back(tau->seedCalo_wideTrk_pt());
    m_vtau_pi0Bonn_TauShot_Shot_cellPt.push_back(tau->pi0Bonn_TauShot_Shot_cellPt());
    m_vtau_seedCalo_wideTrk_atPV_phi.push_back(tau->seedCalo_wideTrk_atPV_phi());
    m_vtau_seedCalo_wideTrk_nContribPixelLayers.push_back(tau->seedCalo_wideTrk_nContribPixelLayers());
    m_vtau_pi0Bonn_Pi0Cluster_eta.push_back(tau->pi0Bonn_Pi0Cluster_eta());
    m_vtau_pi0Bonn_Pi0Cluster_noCorr_eta.push_back(tau->pi0Bonn_Pi0Cluster_noCorr_eta());
    m_vtau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM1.push_back(tau->pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM1());
    m_vtau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM2.push_back(tau->pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM2());
    m_vtau_seedCalo_wideTrk_atTJVA_qoverp.push_back(tau->seedCalo_wideTrk_atTJVA_qoverp());
    m_vtau_pi0Bonn_Pi0Cluster_At_DELTA_THETA.push_back(tau->pi0Bonn_Pi0Cluster_At_DELTA_THETA());
    m_vtau_pi0Bonn_Pi0Cluster_At_LATERAL.push_back(tau->pi0Bonn_Pi0Cluster_At_LATERAL());
    m_vtau_pi0Bonn_Pi0Cluster_NHitsInEM1.push_back(tau->pi0Bonn_Pi0Cluster_NHitsInEM1());
    m_vtau_pi0Bonn_Pi0Cluster_EcoreOverEEM1.push_back(tau->pi0Bonn_Pi0Cluster_EcoreOverEEM1());
    m_vtau_pi0Bonn_nPi0ClustersInPi0Candidates.push_back(tau->pi0Bonn_nPi0ClustersInPi0Candidates());
    m_vtau_seedCalo_wideTrk_nPixHits.push_back(tau->seedCalo_wideTrk_nPixHits());
    m_vtau_seedCalo_wideTrk_nBLayerSplitHits.push_back(tau->seedCalo_wideTrk_nBLayerSplitHits());
    m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_EM.push_back(tau->pi0Bonn_Pi0Cluster_ENG_FRAC_EM());
    m_vtau_seedCalo_wideTrk_atTJVA_d0.push_back(tau->seedCalo_wideTrk_atTJVA_d0());
    m_vtau_pi0Bonn_Pi0Cluster_NPosECells_EM2.push_back(tau->pi0Bonn_Pi0Cluster_NPosECells_EM2());
    m_vtau_pi0Bonn_Pi0Cluster_NPosECells_EM1.push_back(tau->pi0Bonn_Pi0Cluster_NPosECells_EM1());
    m_vtau_pi0Bonn_TauShot_Shot_fracSide_3not1.push_back(tau->pi0Bonn_TauShot_Shot_fracSide_3not1());
    m_vtau_pi0Bonn_TauShot_Shot_phi.push_back(tau->pi0Bonn_TauShot_Shot_phi());
    m_vtau_seedCalo_wideTrk_atPV_z0.push_back(tau->seedCalo_wideTrk_atPV_z0());
    m_vtau_pi0Bonn_sumPi0_pt.push_back(tau->pi0Bonn_sumPi0_pt());
    m_vtau_seedCalo_wideTrk_atPV_qoverp.push_back(tau->seedCalo_wideTrk_atPV_qoverp());
    m_vtau_seedCalo_wideTrk_nPixelOutliers.push_back(tau->seedCalo_wideTrk_nPixelOutliers());
    m_vtau_seedCalo_wideTrk_nBLSharedHits.push_back(tau->seedCalo_wideTrk_nBLSharedHits());
    m_vtau_seedCalo_wideTrk_atPV_pt.push_back(tau->seedCalo_wideTrk_atPV_pt());
    m_vtau_pi0Bonn_Pi0Cluster_At_LONGITUDINAL.push_back(tau->pi0Bonn_Pi0Cluster_At_LONGITUDINAL());
    m_vtau_pi0Bonn_Pi0Cluster_E.push_back(tau->pi0Bonn_Pi0Cluster_E());
    m_vtau_pi0Bonn_TauShot_Shot_signalScore.push_back(tau->pi0Bonn_TauShot_Shot_signalScore());
    m_vtau_pi0Bonn_TauShot_Shot_eta.push_back(tau->pi0Bonn_TauShot_Shot_eta());
    m_vtau_seedCalo_wideTrk_eta.push_back(tau->seedCalo_wideTrk_eta());
    m_vtau_seedCalo_wideTrk_nPixSplitHits.push_back(tau->seedCalo_wideTrk_nPixSplitHits());
    m_vtau_seedCalo_wideTrk_nHits.push_back(tau->seedCalo_wideTrk_nHits());
    m_vtau_pi0Bonn_TauShot_Shot_pt5.push_back(tau->pi0Bonn_TauShot_Shot_pt5());
    m_vtau_pi0Bonn_sumPi0_E.push_back(tau->pi0Bonn_sumPi0_E());
    m_vtau_seedCalo_wideTrk_nSCTSpoiltHits.push_back(tau->seedCalo_wideTrk_nSCTSpoiltHits());
    m_vtau_pi0Bonn_TauShot_Shot_pt1.push_back(tau->pi0Bonn_TauShot_Shot_pt1());
    m_vtau_seedCalo_wideTrk_d0.push_back(tau->seedCalo_wideTrk_d0());
    m_vtau_pi0Bonn_TauShot_Shot_pt3.push_back(tau->pi0Bonn_TauShot_Shot_pt3());
    m_vtau_seedCalo_wideTrk_atTJVA_eta.push_back(tau->seedCalo_wideTrk_atTJVA_eta());
    m_vtau_pi0Bonn_Pi0Cluster_At_DELTA_PHI.push_back(tau->pi0Bonn_Pi0Cluster_At_DELTA_PHI());
    m_vtau_seedCalo_wideTrk_theta.push_back(tau->seedCalo_wideTrk_theta());
    m_vtau_pi0Bonn_Pi0Cluster_noCorr_E.push_back(tau->pi0Bonn_Pi0Cluster_noCorr_E());
    m_vtau_pi0Bonn_TauShot_Shot_Fside_5not3.push_back(tau->pi0Bonn_TauShot_Shot_Fside_5not3());
    m_vtau_seedCalo_wideTrk_atPV_theta.push_back(tau->seedCalo_wideTrk_atPV_theta());
    m_vtau_pi0Bonn_TauShot_Shot_Fside_5not1.push_back(tau->pi0Bonn_TauShot_Shot_Fside_5not1());
    m_vtau_pi0Bonn_Pi0Cluster_phi.push_back(tau->pi0Bonn_Pi0Cluster_phi());
    m_vtau_pi0Bonn_Pi0Cluster_SECOND_R.push_back(tau->pi0Bonn_Pi0Cluster_SECOND_R());
    m_vtau_pi0Bonn_pi0_phi.push_back(tau->pi0Bonn_pi0_phi());
    m_vtau_seedCalo_wideTrk_nBLHits.push_back(tau->seedCalo_wideTrk_nBLHits());
    m_vtau_seedCalo_wideTrk_z0.push_back(tau->seedCalo_wideTrk_z0());
    m_vtau_pi0Bonn_Pi0Cluster_LONGITUDINAL.push_back(tau->pi0Bonn_Pi0Cluster_LONGITUDINAL());
    m_vtau_pi0Bonn_sumPi0_eta.push_back(tau->pi0Bonn_sumPi0_eta());
    m_vtau_seedCalo_wideTrk_nSCTHoles.push_back(tau->seedCalo_wideTrk_nSCTHoles());
    m_vtau_seedCalo_wideTrk_atTJVA_theta.push_back(tau->seedCalo_wideTrk_atTJVA_theta());
    m_vtau_pi0Bonn_Pi0Cluster_SECOND_LAMBDA.push_back(tau->pi0Bonn_Pi0Cluster_SECOND_LAMBDA());
    m_vtau_pi0Bonn_pi0_pt.push_back(tau->pi0Bonn_pi0_pt());
    m_vtau_pi0Bonn_Pi0Cluster_Abs_DELTA_THETA.push_back(tau->pi0Bonn_Pi0Cluster_Abs_DELTA_THETA());
    m_vtau_seedCalo_wideTrk_nSCTHits.push_back(tau->seedCalo_wideTrk_nSCTHits());
    m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_CORE.push_back(tau->pi0Bonn_Pi0Cluster_ENG_FRAC_CORE());
    m_vtau_seedCalo_wideTrk_nTRTHits.push_back(tau->seedCalo_wideTrk_nTRTHits());
    m_vtau_pi0Bonn_Pi0Cluster_NPosCells_PS.push_back(tau->pi0Bonn_Pi0Cluster_NPosCells_PS());
    m_vtau_pi0Bonn_TauShot_Shot_Fside_3not1.push_back(tau->pi0Bonn_TauShot_Shot_Fside_3not1());
    m_vtau_pi0Bonn_visTau_phi.push_back(tau->pi0Bonn_visTau_phi());
    m_vtau_pi0Bonn_TauShot_Shot_sdevPt5.push_back(tau->pi0Bonn_TauShot_Shot_sdevPt5());
    m_vtau_seedCalo_wideTrk_nSCTDoubleHoles.push_back(tau->seedCalo_wideTrk_nSCTDoubleHoles());
    m_vtau_seedCalo_wideTrk_atPV_eta.push_back(tau->seedCalo_wideTrk_atPV_eta());
    m_vtau_pi0Bonn_TauShot_Shot_cellPhi.push_back(tau->pi0Bonn_TauShot_Shot_cellPhi());
    m_vtau_seedCalo_wideTrk_nTRTHoles.push_back(tau->seedCalo_wideTrk_nTRTHoles());
    m_vtau_pi0Bonn_Pi0Cluster_Abs_DELTA_PHI.push_back(tau->pi0Bonn_Pi0Cluster_Abs_DELTA_PHI());
    m_vtau_seedCalo_wideTrk_nTRTOutliers.push_back(tau->seedCalo_wideTrk_nTRTOutliers());
    m_vtau_pi0Bonn_pi0_eta.push_back(tau->pi0Bonn_pi0_eta());
    m_vtau_pi0Bonn_TauShot_Shot_pt1OverPt3.push_back(tau->pi0Bonn_TauShot_Shot_pt1OverPt3());
    m_vtau_seedCalo_wideTrk_atPV_d0.push_back(tau->seedCalo_wideTrk_atPV_d0());
    m_vtau_seedCalo_wideTrk_atTJVA_z0.push_back(tau->seedCalo_wideTrk_atTJVA_z0());
    m_vtau_pi0Bonn_TauShot_Shot_deltaPt12_min.push_back(tau->pi0Bonn_TauShot_Shot_deltaPt12_min());
    m_vtau_pi0Bonn_Pi0Cluster_AsymmetryWRTTrack.push_back(tau->pi0Bonn_Pi0Cluster_AsymmetryWRTTrack());
    m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_CORE.push_back(tau->pi0Bonn_Pi0Cluster_At_ENG_FRAC_CORE());
    m_vtau_pi0Bonn_TauShot_Shot_pt.push_back(tau->pi0Bonn_TauShot_Shot_pt());
    m_vtau_seedCalo_wideTrk_nSCTSharedHits.push_back(tau->seedCalo_wideTrk_nSCTSharedHits());
    m_vtau_seedCalo_wideTrk_nPixHoles.push_back(tau->seedCalo_wideTrk_nPixHoles());
    m_vtau_pi0Bonn_sumPi0_phi.push_back(tau->pi0Bonn_sumPi0_phi());
    m_vtau_pi0Bonn_TauShot_Shot_fracSide_5not1.push_back(tau->pi0Bonn_TauShot_Shot_fracSide_5not1());
    m_vtau_pi0Bonn_TauShot_Shot_fracSide_5not3.push_back(tau->pi0Bonn_TauShot_Shot_fracSide_5not3());
    m_vtau_seedCalo_wideTrk_nGangedPixels.push_back(tau->seedCalo_wideTrk_nGangedPixels());
    m_vtau_seedCalo_wideTrk_nTRTHighTHits.push_back(tau->seedCalo_wideTrk_nTRTHighTHits());
    m_vtau_pi0Bonn_Pi0Cluster_At_SECOND_ENG_DENS.push_back(tau->pi0Bonn_Pi0Cluster_At_SECOND_ENG_DENS());
    m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_MAX.push_back(tau->pi0Bonn_Pi0Cluster_At_ENG_FRAC_MAX());
    m_vtau_pi0Bonn_Pi0Cluster_LATERAL.push_back(tau->pi0Bonn_Pi0Cluster_LATERAL());
    m_vtau_pi0Bonn_Pi0Cluster_pt.push_back(tau->pi0Bonn_Pi0Cluster_pt());
    m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_EM.push_back(tau->pi0Bonn_Pi0Cluster_At_ENG_FRAC_EM());
    m_vtau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM2.push_back(tau->pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM2());
    m_vtau_pi0Bonn_TauShot_Shot_pt3OverPt5.push_back(tau->pi0Bonn_TauShot_Shot_pt3OverPt5());
    m_vtau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM1.push_back(tau->pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM1());

    if(!IsData())
      GetTruthInfo(tau);
 }
}

void TNTCycle::GetTrueTauInfo(){
  std::vector<D3PDReader::TruthTau*>::const_iterator cit = m_vTruthTaus.begin(); 
  for(; cit != m_vTruthTaus.end(); ++cit){
    D3PDReader::TruthTau* truthTau = *cit;
    m_vtruth_VisEt.push_back(truthTau->TLV().Pt());
    m_vtruth_VisEta.push_back(truthTau->TLV().Eta());
    m_vtruth_VisPhi.push_back(truthTau->TLV().Phi());
    m_vtruth_VisM.push_back(truthTau->TLV().M());
    m_vtruth_Pt.push_back(truthTau->pt());
    m_vtruth_Eta.push_back(truthTau->eta());
    m_vtruth_Phi.push_back(truthTau->phi());
    m_vtruth_M.push_back(truthTau->m());
    m_vtruth_NProng.push_back(truthTau->nProng());
  }
}

void TNTCycle::GetTruthInfo(D3PDReader::Tau* tau){
  int index = this->HasTruthMatch(tau);
  std::vector<D3PDReader::TruthTau*>::const_iterator cit = m_vTruthTaus.begin(); 
  if(this->HasTruthMatch(tau) != -1){
    m_vtau_hasTruthMatch.push_back(1);
    for(; cit != m_vTruthTaus.end(); ++cit){
      D3PDReader::TruthTau* truthTau = *cit;
      if(!(index == truthTau->GetIndex()))
	continue;
      m_vtau_matchVisEt.push_back(truthTau->TLV().Pt());
      m_vtau_matchVisEta.push_back(truthTau->TLV().Eta());
      m_vtau_matchVisPhi.push_back(truthTau->TLV().Phi());
      m_vtau_matchVisM.push_back(truthTau->TLV().M());
      m_vtau_matchPt.push_back(truthTau->pt());
      m_vtau_matchEta.push_back(truthTau->eta());
      m_vtau_matchPhi.push_back(truthTau->phi());
      m_vtau_matchM.push_back(truthTau->m());
      m_vtau_matchNProng.push_back(truthTau->nProng());
      break;
    }
  }
  else{
    m_vtau_hasTruthMatch.push_back(0);
    m_vtau_matchVisEt.push_back(-1111);
    m_vtau_matchVisEta.push_back(-1111);
    m_vtau_matchVisPhi.push_back(-1111);
    m_vtau_matchVisM.push_back(-1111);
    m_vtau_matchPt.push_back(-1111);
    m_vtau_matchEta.push_back(-1111);
    m_vtau_matchPhi.push_back(-1111);
    m_vtau_matchM.push_back(-1111);
    m_vtau_matchNProng.push_back(-1111);
  }
}

int TNTCycle::HasTruthMatch(const D3PDReader::Tau* const pTau){
  std::vector<D3PDReader::TruthTau*>::const_iterator cit = m_vTruthTaus.begin(); 
  for(; cit != m_vTruthTaus.end(); ++cit){
    D3PDReader::TruthTau* truthTau = *cit;
    float deltaR = pTau->TLV().DeltaR(truthTau->TLV());
    if(deltaR <= 0.2){
      std::vector<D3PDReader::Tau*>::const_iterator it = m_vTaus.begin();
      for(; it != m_vTaus.end(); ++it){
	D3PDReader::Tau* otherTau = *it;
	if(otherTau == pTau)
	  continue;
	if(otherTau->TLV().DeltaR(truthTau->TLV()) <= deltaR)
	  return -1;
      }
      return truthTau->GetIndex();
    }
  }
  return -1;
}

void TNTCycle::StartInputData(const SInputData& id) throw(SError){
  LLHCommon::StartInputData(id);
  DeclareVariables();
}

void TNTCycle::DeclareVariables(){
  DeclareVariable(m_inVtx, "nVtx", "tau");
  DeclareVariable(m_fmu, "mu", "tau");
  DeclareVariable(m_ilbn, "lbn", "tau");
  DeclareVariable(m_iRunNumber, "RunNumber", "tau");
  DeclareVariable(m_iEventNumber, "EventNumber", "tau");
  DeclareVariable(m_itau_n, "tau_n", "tau");
  DeclareVariable(m_itrueTau_n, "trueTau_n", "tau");

  DeclareVariable(m_vtau_Et, "tau_Et", "tau");
  DeclareVariable(m_vtau_eta, "tau_eta", "tau");
  DeclareVariable(m_vtau_m, "tau_m", "tau");
  DeclareVariable(m_vtau_phi, "tau_phi", "tau");
  DeclareVariable(m_vtau_charge, "tau_charge", "tau");
  DeclareVariable(m_vtau_author, "tau_author", "tau");
  DeclareVariable(m_vtau_numTrack, "tau_numTrack", "tau");
  DeclareVariable(m_vtau_hasTruthMatch, "tau_hasTruthMatch", "tau");

  DeclareVariable(m_vtau_BDTJetScore, "tau_BDTJetScore", "tau");
  DeclareVariable(m_vtau_SafeLikelihood, "tau_SafeLikelihood", "tau");
  DeclareVariable(m_vtau_LLH_loose, "tau_tauLlhLoose", "tau");
  DeclareVariable(m_vtau_LLH_medium, "tau_tauLlhMedium", "tau");
  DeclareVariable(m_vtau_LLH_tight, "tau_tauLlhTight", "tau");
  DeclareVariable(m_vtau_BDT_loose, "tau_JetBDTSigLoose", "tau");
  DeclareVariable(m_vtau_BDT_medium, "tau_JetBDTSigMedium", "tau");
  DeclareVariable(m_vtau_BDT_tight, "tau_JetBDTSigTight", "tau");
  DeclareVariable(m_vtau_likelihood, "tau_likelihood", "tau");

  DeclareVariable(m_vtau_ipSigLeadTrk, "tau_ipSigLeadTrk", "tau");
  DeclareVariable(m_vtau_massTrkSys, "tau_massTrkSys", "tau");
  DeclareVariable(m_vtau_trFlightPathSig, "tau_trFlightPathSig", "tau");
  DeclareVariable(m_vtau_seedCalo_trkAvgDist, "tau_seedCalo_trkAvgDist", "tau");
  DeclareVariable(m_vtau_seedCalo_dRmax, "tau_seedCalo_dRmax", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_n, "tau_seedCalo_wideTrk_n", "tau");
  DeclareVariable(m_vtau_calcVars_corrCentFrac, "tau_calcVars_corrCentFrac", "tau");
  DeclareVariable(m_vtau_calcVars_corrFTrk, "tau_calcVars_corrFTrk", "tau");
  DeclareVariable(m_vtau_leadTrack_eta, "tau_leadTrack_eta", "tau");
  DeclareVariable(m_vtau_etOverPtLeadTrk, "tau_etOverPtLeadTrk", "tau");
  DeclareVariable(m_vtau_ptRatio, "tau_ptRatio", "tau");
  DeclareVariable(m_vtau_pi0_vistau_m, "tau_pi0_vistau_m", "tau");
  DeclareVariable(m_vtau_pi0_n, "tau_pi0_n", "tau");

  DeclareVariable(m_vtau_matchVisEt, "tau_matchVisEt", "tau");
  DeclareVariable(m_vtau_matchVisEta, "tau_matchVisEta", "tau");
  DeclareVariable(m_vtau_matchVisPhi, "tau_matchVisPhi", "tau");
  DeclareVariable(m_vtau_matchVisM, "tau_matchVisM", "tau");
  DeclareVariable(m_vtau_matchPt, "tau_matchPt", "tau");
  DeclareVariable(m_vtau_matchEta, "tau_matchEta", "tau");
  DeclareVariable(m_vtau_matchPhi, "tau_matchPhi", "tau");
  DeclareVariable(m_vtau_matchM, "tau_matchM", "tau");
  DeclareVariable(m_vtau_matchNProng, "tau_matchNProng", "tau");

  DeclareVariable(m_vtruth_VisEt, "trueTau_vis_Et", "tau");
  DeclareVariable(m_vtruth_VisEta, "trueTau_vis_eta", "tau");
  DeclareVariable(m_vtruth_VisPhi, "trueTau_vis_phi", "tau");
  DeclareVariable(m_vtruth_VisM, "trueTau_vis_m", "tau");
  DeclareVariable(m_vtruth_Pt, "trueTau_pt", "tau");
  DeclareVariable(m_vtruth_Eta, "trueTau_eta", "tau");
  DeclareVariable(m_vtruth_Phi, "trueTau_phi", "tau");
  DeclareVariable(m_vtruth_M, "trueTau_m", "tau");
  DeclareVariable(m_vtruth_NProng, "trueTau_nProng", "tau");
  
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmean, "tau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmean", "tau");
  DeclareVariable(m_vtau_pi0Bonn_nPi0Cluster, "tau_pi0Bonn_nPi0Cluster", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_expectBLayerHit, "tau_seedCalo_wideTrk_expectBLayerHit", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nTRTHighTOutliers, "tau_seedCalo_wideTrk_nTRTHighTOutliers", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_noCorr_pt, "tau_pi0Bonn_Pi0Cluster_noCorr_pt", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_mergedScore, "tau_pi0Bonn_TauShot_Shot_mergedScore", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nGangedFlaggedFakes, "tau_seedCalo_wideTrk_nGangedFlaggedFakes", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nTRTXenonHits, "tau_seedCalo_wideTrk_nTRTXenonHits", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atTJVA_n, "tau_seedCalo_wideTrk_atTJVA_n", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nSCTOutliers, "tau_seedCalo_wideTrk_nSCTOutliers", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_qoverp, "tau_seedCalo_wideTrk_qoverp", "tau");
  DeclareVariable(m_vtau_pi0Bonn_pi0_E, "tau_pi0Bonn_pi0_E", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_ws5, "tau_pi0Bonn_TauShot_Shot_ws5", "tau");
  DeclareVariable(m_vtau_pi0Bonn_ShotsInPi0Clusters, "tau_pi0Bonn_ShotsInPi0Clusters", "tau");
  DeclareVariable(m_vtau_pi0Bonn_nPi0, "tau_pi0Bonn_nPi0", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_BDTScore, "tau_pi0Bonn_Pi0Cluster_BDTScore", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_phi, "tau_seedCalo_wideTrk_phi", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nSCTDeadSensors, "tau_seedCalo_wideTrk_nSCTDeadSensors", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_CENTER_LAMBDA_helped, "tau_pi0Bonn_Pi0Cluster_CENTER_LAMBDA_helped", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmode, "tau_pi0Bonn_TauShot_Shot_sdevEta5_WRTmode", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0ClustersInPi0Candidates, "tau_pi0Bonn_Pi0ClustersInPi0Candidates", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_nPhotons, "tau_pi0Bonn_TauShot_Shot_nPhotons", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_MAX, "tau_pi0Bonn_Pi0Cluster_ENG_FRAC_MAX", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atTJVA_phi, "tau_seedCalo_wideTrk_atTJVA_phi", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nPixelSpoiltHits, "tau_seedCalo_wideTrk_nPixelSpoiltHits", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_nShot, "tau_pi0Bonn_TauShot_nShot", "tau");
  DeclareVariable(m_vtau_pi0Bonn_visTau_pt, "tau_pi0Bonn_visTau_pt", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atTJVA_pt, "tau_seedCalo_wideTrk_atTJVA_pt", "tau");
  // DeclareVariable(m_vtau_seedCalo_wideTrk_n, "tau_seedCalo_wideTrk_n", "tau");
  DeclareVariable(m_vtau_pi0Bonn_visTau_E, "tau_pi0Bonn_visTau_E", "tau");
  DeclareVariable(m_vtau_pi0Bonn_nShotsInPi0Clusters, "tau_pi0Bonn_nShotsInPi0Clusters", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_noCorr_phi, "tau_pi0Bonn_Pi0Cluster_noCorr_phi", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nPixSharedHits, "tau_seedCalo_wideTrk_nPixSharedHits", "tau");
  DeclareVariable(m_vtau_pi0Bonn_visTau_eta, "tau_pi0Bonn_visTau_eta", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nPixelDeadSensors, "tau_seedCalo_wideTrk_nPixelDeadSensors", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nBLayerOutliers, "tau_seedCalo_wideTrk_nBLayerOutliers", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_cellEta, "tau_pi0Bonn_TauShot_Shot_cellEta", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_log_SECOND_ENG_DENS, "tau_pi0Bonn_Pi0Cluster_log_SECOND_ENG_DENS", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_FIRST_ETA, "tau_pi0Bonn_Pi0Cluster_FIRST_ETA", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_pt, "tau_seedCalo_wideTrk_pt", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_cellPt, "tau_pi0Bonn_TauShot_Shot_cellPt", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atPV_phi, "tau_seedCalo_wideTrk_atPV_phi", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nContribPixelLayers, "tau_seedCalo_wideTrk_nContribPixelLayers", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_eta, "tau_pi0Bonn_Pi0Cluster_eta", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_noCorr_eta, "tau_pi0Bonn_Pi0Cluster_noCorr_eta", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM1, "tau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM1", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM2, "tau_pi0Bonn_Pi0Cluster_firstEtaWRTClusterPosition_EM2", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atTJVA_qoverp, "tau_seedCalo_wideTrk_atTJVA_qoverp", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_At_DELTA_THETA, "tau_pi0Bonn_Pi0Cluster_At_DELTA_THETA", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_At_LATERAL, "tau_pi0Bonn_Pi0Cluster_At_LATERAL", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_NHitsInEM1, "tau_pi0Bonn_Pi0Cluster_NHitsInEM1", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_EcoreOverEEM1, "tau_pi0Bonn_Pi0Cluster_EcoreOverEEM1", "tau");
  DeclareVariable(m_vtau_pi0Bonn_nPi0ClustersInPi0Candidates, "tau_pi0Bonn_nPi0ClustersInPi0Candidates", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nPixHits, "tau_seedCalo_wideTrk_nPixHits", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nBLayerSplitHits, "tau_seedCalo_wideTrk_nBLayerSplitHits", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_EM, "tau_pi0Bonn_Pi0Cluster_ENG_FRAC_EM", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atTJVA_d0, "tau_seedCalo_wideTrk_atTJVA_d0", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_NPosECells_EM2, "tau_pi0Bonn_Pi0Cluster_NPosECells_EM2", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_NPosECells_EM1, "tau_pi0Bonn_Pi0Cluster_NPosECells_EM1", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_fracSide_3not1, "tau_pi0Bonn_TauShot_Shot_fracSide_3not1", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_phi, "tau_pi0Bonn_TauShot_Shot_phi", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atPV_z0, "tau_seedCalo_wideTrk_atPV_z0", "tau");
  DeclareVariable(m_vtau_pi0Bonn_sumPi0_pt, "tau_pi0Bonn_sumPi0_pt", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atPV_qoverp, "tau_seedCalo_wideTrk_atPV_qoverp", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nPixelOutliers, "tau_seedCalo_wideTrk_nPixelOutliers", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nBLSharedHits, "tau_seedCalo_wideTrk_nBLSharedHits", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atPV_pt, "tau_seedCalo_wideTrk_atPV_pt", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_At_LONGITUDINAL, "tau_pi0Bonn_Pi0Cluster_At_LONGITUDINAL", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_E, "tau_pi0Bonn_Pi0Cluster_E", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_signalScore, "tau_pi0Bonn_TauShot_Shot_signalScore", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_eta, "tau_pi0Bonn_TauShot_Shot_eta", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_eta, "tau_seedCalo_wideTrk_eta", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nPixSplitHits, "tau_seedCalo_wideTrk_nPixSplitHits", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nHits, "tau_seedCalo_wideTrk_nHits", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_pt5, "tau_pi0Bonn_TauShot_Shot_pt5", "tau");
  DeclareVariable(m_vtau_pi0Bonn_sumPi0_E, "tau_pi0Bonn_sumPi0_E", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nSCTSpoiltHits, "tau_seedCalo_wideTrk_nSCTSpoiltHits", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_pt1, "tau_pi0Bonn_TauShot_Shot_pt1", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_d0, "tau_seedCalo_wideTrk_d0", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_pt3, "tau_pi0Bonn_TauShot_Shot_pt3", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atTJVA_eta, "tau_seedCalo_wideTrk_atTJVA_eta", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_At_DELTA_PHI, "tau_pi0Bonn_Pi0Cluster_At_DELTA_PHI", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_theta, "tau_seedCalo_wideTrk_theta", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_noCorr_E, "tau_pi0Bonn_Pi0Cluster_noCorr_E", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_Fside_5not3, "tau_pi0Bonn_TauShot_Shot_Fside_5not3", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atPV_theta, "tau_seedCalo_wideTrk_atPV_theta", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_Fside_5not1, "tau_pi0Bonn_TauShot_Shot_Fside_5not1", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_phi, "tau_pi0Bonn_Pi0Cluster_phi", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_SECOND_R, "tau_pi0Bonn_Pi0Cluster_SECOND_R", "tau");
  DeclareVariable(m_vtau_pi0Bonn_pi0_phi, "tau_pi0Bonn_pi0_phi", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nBLHits, "tau_seedCalo_wideTrk_nBLHits", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_z0, "tau_seedCalo_wideTrk_z0", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_LONGITUDINAL, "tau_pi0Bonn_Pi0Cluster_LONGITUDINAL", "tau");
  DeclareVariable(m_vtau_pi0Bonn_sumPi0_eta, "tau_pi0Bonn_sumPi0_eta", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nSCTHoles, "tau_seedCalo_wideTrk_nSCTHoles", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atTJVA_theta, "tau_seedCalo_wideTrk_atTJVA_theta", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_SECOND_LAMBDA, "tau_pi0Bonn_Pi0Cluster_SECOND_LAMBDA", "tau");
  DeclareVariable(m_vtau_pi0Bonn_pi0_pt, "tau_pi0Bonn_pi0_pt", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_Abs_DELTA_THETA, "tau_pi0Bonn_Pi0Cluster_Abs_DELTA_THETA", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nSCTHits, "tau_seedCalo_wideTrk_nSCTHits", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_ENG_FRAC_CORE, "tau_pi0Bonn_Pi0Cluster_ENG_FRAC_CORE", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nTRTHits, "tau_seedCalo_wideTrk_nTRTHits", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_NPosCells_PS, "tau_pi0Bonn_Pi0Cluster_NPosCells_PS", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_Fside_3not1, "tau_pi0Bonn_TauShot_Shot_Fside_3not1", "tau");
  DeclareVariable(m_vtau_pi0Bonn_visTau_phi, "tau_pi0Bonn_visTau_phi", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_sdevPt5, "tau_pi0Bonn_TauShot_Shot_sdevPt5", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nSCTDoubleHoles, "tau_seedCalo_wideTrk_nSCTDoubleHoles", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atPV_eta, "tau_seedCalo_wideTrk_atPV_eta", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_cellPhi, "tau_pi0Bonn_TauShot_Shot_cellPhi", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nTRTHoles, "tau_seedCalo_wideTrk_nTRTHoles", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_Abs_DELTA_PHI, "tau_pi0Bonn_Pi0Cluster_Abs_DELTA_PHI", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nTRTOutliers, "tau_seedCalo_wideTrk_nTRTOutliers", "tau");
  DeclareVariable(m_vtau_pi0Bonn_pi0_eta, "tau_pi0Bonn_pi0_eta", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_pt1OverPt3, "tau_pi0Bonn_TauShot_Shot_pt1OverPt3", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atPV_d0, "tau_seedCalo_wideTrk_atPV_d0", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_atTJVA_z0, "tau_seedCalo_wideTrk_atTJVA_z0", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_deltaPt12_min, "tau_pi0Bonn_TauShot_Shot_deltaPt12_min", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_AsymmetryWRTTrack, "tau_pi0Bonn_Pi0Cluster_AsymmetryWRTTrack", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_CORE, "tau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_CORE", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_pt, "tau_pi0Bonn_TauShot_Shot_pt", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nSCTSharedHits, "tau_seedCalo_wideTrk_nSCTSharedHits", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nPixHoles, "tau_seedCalo_wideTrk_nPixHoles", "tau");
  DeclareVariable(m_vtau_pi0Bonn_sumPi0_phi, "tau_pi0Bonn_sumPi0_phi", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_fracSide_5not1, "tau_pi0Bonn_TauShot_Shot_fracSide_5not1", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_fracSide_5not3, "tau_pi0Bonn_TauShot_Shot_fracSide_5not3", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nGangedPixels, "tau_seedCalo_wideTrk_nGangedPixels", "tau");
  DeclareVariable(m_vtau_seedCalo_wideTrk_nTRTHighTHits, "tau_seedCalo_wideTrk_nTRTHighTHits", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_At_SECOND_ENG_DENS, "tau_pi0Bonn_Pi0Cluster_At_SECOND_ENG_DENS", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_MAX, "tau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_MAX", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_LATERAL, "tau_pi0Bonn_Pi0Cluster_LATERAL", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_pt, "tau_pi0Bonn_Pi0Cluster_pt", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_EM, "tau_pi0Bonn_Pi0Cluster_At_ENG_FRAC_EM", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM2, "tau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM2", "tau");
  DeclareVariable(m_vtau_pi0Bonn_TauShot_Shot_pt3OverPt5, "tau_pi0Bonn_TauShot_Shot_pt3OverPt5", "tau");
  DeclareVariable(m_vtau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM1, "tau_pi0Bonn_Pi0Cluster_secondEtaWRTClusterPosition_EM1", "tau");
}

#endif // TNTCycle_CXX
#endif // PI0BONN
