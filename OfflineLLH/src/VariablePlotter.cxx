#ifndef SKIM
#ifndef VariablePlotter_CXX
#define VariablePlotter_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/lexical_cast.hpp"
#include "boost/assign/std/vector.hpp"
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#endif // __CINT__

// STL
#include <vector>
#include <iostream>

// custom header
#include "VariablePlotter.h"
// #include "IDVarHelper.h"
#include "TauCommonUtilities.h"
#include "IDVarCalculator.h"

//ROOT includes
#include "TH2.h"
#include "Constants.h"

using namespace boost::assign;
using namespace AnalysisUtilities_Constants;

ClassImp(VariablePlotter)
VariablePlotter::VariablePlotter(CycleBase* pParent,const char* sName):
  ToolBase(pParent,sName)
{
  m_mid["BDT_loose"] = boost::bind(&TauBDTCut::PassCut, boost::ref(m_pBDTCut), _1, 3, 0);
  m_mid["BDT_medium"] = boost::bind(&TauBDTCut::PassCut, boost::ref(m_pBDTCut), _1, 4, 0);
  m_mid["BDT_tight"] = boost::bind(&TauBDTCut::PassCut, boost::ref(m_pBDTCut), _1, 5, 0);
  m_mid["LLH_loose"] = boost::bind(&TauLLHCut::PassCut, boost::ref(m_pLLHCut), _1, 3, 0);
  m_mid["LLH_medium"] = boost::bind(&TauLLHCut::PassCut, boost::ref(m_pLLHCut), _1, 4, 0);
  m_mid["LLH_tight"] = boost::bind(&TauLLHCut::PassCut, boost::ref(m_pLLHCut), _1, 5, 0);
  DeclareProperty("weights_inputfile", c_inputfile_weights = "");
  DeclareProperty("weights_switch_off", b_switchOffWeightTool = false);
  DeclareProperty("pileup_correction_inputfile", c_pileup_correction_file = "");
  DeclareProperty("pileup_correction_switchOff", c_pileup_correction_switchOff = false);

  //m_IDVarCalculator = new CellBasedCalculator();
  m_IDVarCalculator = new EFlowRecCalculator();
  m_WeightTool = new WeightTool();
  m_pileUpCorrectionTool = new PileUpCorrectionTool();
}

void VariablePlotter::PlotDistributions(std::vector<D3PDReader::AnalysisTau*>& vTaus, const unsigned int& iNVtx, float fWeight)
{
  for(auto tau : vTaus){
    if(!IsData())
      assert(tau->hasTruthMatch());
    this->PlotDistribution(tau,iNVtx);
  }
}

void VariablePlotter::PlotDistribution(const D3PDReader::AnalysisTau * const tau, const unsigned int& iNVtx){
  float weight = m_WeightTool->getWeight(tau, iNVtx);

  for(auto dr : m_piCellBasedIDVars.dRChrgPi)
    Hist("chargedPi_dR")->Fill(dr);
  for(auto dr : m_piCellBasedIDVars.dRNeutPi)
    Hist("neutralPi_dR")->Fill(dr);
  for(auto dr : m_piCellBasedIDVars.dRCharged)
    Hist("wideTrack_dR")->Fill(dr);
  for(auto dr : m_piCellBasedIDVars.dRNeutral)
    Hist("neutrals_dR")->Fill(dr);
  for(auto dr : m_piCellBasedIDVars.dRChrgHLV)
    Hist("chargedHLV_dR")->Fill(dr);
  for(auto dr : m_piCellBasedIDVars.dRNeutHLV)
    Hist("neutralHLV_dR")->Fill(dr);
  for(auto dr : m_piCellBasedIDVars.dRNeutLowAHLV)
    Hist("neutral_lowA_HLV_dR")->Fill(dr);
  for(auto dr : m_piCellBasedIDVars.dRNeutLowBHLV)
    Hist("neutral_lowB_HLV_dR")->Fill(dr);
  for(auto dr : m_piCellBasedIDVars.dROuterChrg)
    Hist("outerCharged_dR")->Fill(dr);
  for(auto dr : m_piCellBasedIDVars.dROuterNeut)
    Hist("outerNeutral_dR")->Fill(dr);

  const std::string prong = (tau->numTrack()==1) ? "1P" : "3P";
  m_piCellBasedIDVars = m_IDVarCalculator->calculateVariables(*tau);
  m_fpanTauCellBased_nChrg0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg0204",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.nChrg0204);
  m_fpanTauCellBased_nChrg01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg01",
								       tau,
								       iNVtx,
								       m_piCellBasedIDVars.nChrg01);
  m_fpanTauCellBased_nChrg02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nChrg02",
								       tau,
								       iNVtx,
								       m_piCellBasedIDVars.nChrg02);
  m_fpanTauCellBased_nNeut0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut0204",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.nNeut0204);
  m_fpanTauCellBased_nNeut01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut01",
								       tau,
								       iNVtx,
								       m_piCellBasedIDVars.nNeut01);
  m_fpanTauCellBased_nNeut02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_nNeut02",
								       tau,
								       iNVtx,
								       m_piCellBasedIDVars.nNeut02);
  m_fpanTauCellBased_massChrgSys01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys01",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.massChrgSys01 / GeV);
  m_fpanTauCellBased_massChrgSys02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys02",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.massChrgSys02 / GeV);
  m_fpanTauCellBased_massChrgSys04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massChrgSys04",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.massChrgSys04 / GeV);
  m_fpanTauCellBased_massNeutSys01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys01",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.massNeutSys01 / GeV);
  m_fpanTauCellBased_massNeutSys02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys02",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.massNeutSys02 / GeV);
  m_fpanTauCellBased_massNeutSys04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_massNeutSys04",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.massNeutSys04 / GeV);
  m_fpanTauCellBased_visTauM01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM01",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.visTauM01 / GeV);
  m_fpanTauCellBased_visTauM02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM02",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.visTauM02 / GeV);
  m_fpanTauCellBased_visTauM04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_visTauM04",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.visTauM04 / GeV);
  m_fpanTauCellBased_dRmax02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRmax02",
								       tau,
								       iNVtx,
								       m_piCellBasedIDVars.dRmax02);
  m_fpanTauCellBased_dRmax04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRmax04",
								       tau,
								       iNVtx,
								       m_piCellBasedIDVars.dRmax04);
  m_fpanTauCellBased_ipSigLeadTrk = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ipSigLeadTrk",
									    tau,
									    iNVtx,
									    m_piCellBasedIDVars.ipSigLeadTrk);
  m_fpanTauCellBased_trFlightPathSig = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_trFlightPathSig",
									       tau,
									       iNVtx,
									       m_piCellBasedIDVars.trFlightPathSig);
  m_fpanTauCellBased_ptRatio01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio01",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.ptRatio01);
  m_fpanTauCellBased_ptRatio02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio02",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.ptRatio02);
  m_fpanTauCellBased_ptRatio04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatio04",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.ptRatio04);
  m_fpanTauCellBased_ptRatioNeut01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut01",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.ptRatioNeut01);
  m_fpanTauCellBased_ptRatioNeut02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut02",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.ptRatioNeut02);
  m_fpanTauCellBased_ptRatioNeut04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioNeut04",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.ptRatioNeut04);
  m_fpanTauCellBased_ptRatioChrg01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg01",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.ptRatioChrg01);
  m_fpanTauCellBased_ptRatioChrg02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg02",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.ptRatioChrg02);
  m_fpanTauCellBased_ptRatioChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_ptRatioChrg04",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.ptRatioChrg04);
  m_fpanTauCellBased_fLeadChrg02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg02",
									   tau,
									   iNVtx,
									   m_piCellBasedIDVars.fLeadChrg02);
  m_fpanTauCellBased_fLeadChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg04",
									   tau,
									   iNVtx,
									   m_piCellBasedIDVars.fLeadChrg04);
  m_fpanTauCellBased_eFrac0102 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0102",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.eFrac0102);
  m_fpanTauCellBased_eFrac0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0204",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.eFrac0204);
  m_fpanTauCellBased_eFrac0104 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFrac0104",
									 tau,
									 iNVtx,
									 m_piCellBasedIDVars.eFrac0104);
  m_fpanTauCellBased_eFracChrg0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg0204",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.eFracChrg0204);
  m_fpanTauCellBased_eFracChrg0104 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracChrg0104",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.eFracChrg0104);
  m_fpanTauCellBased_eFracNeut0204 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut0204",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.eFracNeut0204);
  m_fpanTauCellBased_eFracNeut0104 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_eFracNeut0104",
									     tau,
									     iNVtx,
									     m_piCellBasedIDVars.eFracNeut0104);
  m_fpanTauCellBased_fLeadChrg01 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_fLeadChrg01",
									   tau,
									   iNVtx,
									   m_piCellBasedIDVars.fLeadChrg01);
  m_fpanTauCellBased_rCal02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCal02",
								      tau,
								      iNVtx,
								      m_piCellBasedIDVars.rCal02);
  m_fpanTauCellBased_rCal04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCal04",
								      tau,
								      iNVtx,
								      m_piCellBasedIDVars.rCal04);
  m_fpanTauCellBased_rCalChrg02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalChrg02",
									  tau,
									  iNVtx,
									  m_piCellBasedIDVars.rCalChrg02);
  m_fpanTauCellBased_rCalChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalChrg04",
									  tau,
									  iNVtx,
									  m_piCellBasedIDVars.rCalChrg04);
  m_fpanTauCellBased_rCalNeut02 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalNeut02",
									  tau,
									  iNVtx,
									  m_piCellBasedIDVars.rCalNeut02);
  m_fpanTauCellBased_rCalNeut04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_rCalNeut04",
									  tau,
									  iNVtx,
									  m_piCellBasedIDVars.rCalNeut04);
  m_fpanTauCellBased_dRminmaxPtChrg04 = m_pileUpCorrectionTool->getCorrectedVar("panTauCellBased_dRminmaxPtChrg04",
										tau,
										iNVtx,
										m_piCellBasedIDVars.dRminmaxPtChrg04);

  //inclusive plots
  Hist(("tau_pt_"+prong).c_str())->Fill((tau)->TLV().Pt() / GeV,weight);
  Hist(("tau_CentFrac0102_"+prong).c_str())->Fill((tau)->calcVars_corrCentFrac(),weight);
  Hist(("tau_FTrk02_"+prong).c_str())->Fill((tau)->calcVars_corrFTrk(),weight);
  Hist(("tau_TrFlightPathSig_"+prong).c_str())->Fill((tau)->trFlightPathSig(),weight);
  Hist(("tau_IpSigLeadTrk_"+prong).c_str())->Fill((tau)->ipSigLeadTrk(),weight);
  Hist(("tau_MassTrkSys_"+prong).c_str())->Fill((tau)->massTrkSys() / GeV,weight);
  Hist(("tau_TrkAvgDist_"+prong).c_str())->Fill((tau)->seedCalo_trkAvgDist(),weight);
  Hist(("tau_DrMax_"+prong).c_str())->Fill((tau)->seedCalo_dRmax(),weight);
  Hist(("tau_NTracksdrdR_"+prong).c_str())->Fill((tau)->seedCalo_wideTrk_n(),weight);
  Hist(("tau_LLHScore_"+prong).c_str())->Fill((tau)->SafeLikelihood(),weight);
  Hist(("tau_BDTScore_"+prong).c_str())->Fill((tau)->BDTJetScore(),weight);
  Hist(("tau_ptRatio_"+prong).c_str())->Fill((tau)->ptRatio(),weight);
  Hist(("tau_pi0_n_"+prong).c_str())->Fill((tau)->pi0_n(),weight);
  Hist(("tau_pi0_vistau_m_"+prong).c_str())->Fill((tau)->pi0_vistau_m() / GeV,weight);

  // pantau+cellbased
  Hist(("panTauCellBased_nChrg0204_"+prong).c_str())->Fill(m_fpanTauCellBased_nChrg0204,weight);
  Hist(("panTauCellBased_nChrg01_"+prong).c_str())->Fill(m_fpanTauCellBased_nChrg01,weight);
  Hist(("panTauCellBased_nChrg02_"+prong).c_str())->Fill(m_fpanTauCellBased_nChrg02,weight);
  Hist(("panTauCellBased_nNeut0204_"+prong).c_str())->Fill(m_fpanTauCellBased_nNeut0204,weight);
  Hist(("panTauCellBased_nNeut01_"+prong).c_str())->Fill(m_fpanTauCellBased_nNeut01,weight);
  Hist(("panTauCellBased_nNeut02_"+prong).c_str())->Fill(m_fpanTauCellBased_nNeut02,weight);
  Hist(("panTauCellBased_massChrgSys01_"+prong).c_str())->Fill(m_fpanTauCellBased_massChrgSys01,weight);
  Hist(("panTauCellBased_massChrgSys02_"+prong).c_str())->Fill(m_fpanTauCellBased_massChrgSys02,weight);
  Hist(("panTauCellBased_massChrgSys04_"+prong).c_str())->Fill(m_fpanTauCellBased_massChrgSys04,weight);
  Hist(("panTauCellBased_massNeutSys01_"+prong).c_str())->Fill(m_fpanTauCellBased_massNeutSys01,weight);
  Hist(("panTauCellBased_massNeutSys02_"+prong).c_str())->Fill(m_fpanTauCellBased_massNeutSys02,weight);
  Hist(("panTauCellBased_massNeutSys04_"+prong).c_str())->Fill(m_fpanTauCellBased_massNeutSys04,weight);
  Hist(("panTauCellBased_visTauM01_"+prong).c_str())->Fill(m_fpanTauCellBased_visTauM01,weight);
  Hist(("panTauCellBased_visTauM02_"+prong).c_str())->Fill(m_fpanTauCellBased_visTauM02,weight);
  Hist(("panTauCellBased_visTauM04_"+prong).c_str())->Fill(m_fpanTauCellBased_visTauM04,weight);
  Hist(("panTauCellBased_dRmax02_"+prong).c_str())->Fill(m_fpanTauCellBased_dRmax02,weight);
  Hist(("panTauCellBased_dRmax04_"+prong).c_str())->Fill(m_fpanTauCellBased_dRmax04,weight);
  Hist(("panTauCellBased_ipSigLeadTrk_"+prong).c_str())->Fill(m_fpanTauCellBased_ipSigLeadTrk,weight);
  Hist(("panTauCellBased_trFlightPathSig_"+prong).c_str())->Fill(m_fpanTauCellBased_trFlightPathSig,weight);
  Hist(("panTauCellBased_ptRatio01_"+prong).c_str())->Fill(m_fpanTauCellBased_ptRatio01,weight);
  Hist(("panTauCellBased_ptRatio02_"+prong).c_str())->Fill(m_fpanTauCellBased_ptRatio02,weight);
  Hist(("panTauCellBased_ptRatio04_"+prong).c_str())->Fill(m_fpanTauCellBased_ptRatio04,weight);
  Hist(("panTauCellBased_ptRatioNeut01_"+prong).c_str())->Fill(m_fpanTauCellBased_ptRatioNeut01,weight);
  Hist(("panTauCellBased_ptRatioNeut02_"+prong).c_str())->Fill(m_fpanTauCellBased_ptRatioNeut02,weight);
  Hist(("panTauCellBased_ptRatioNeut04_"+prong).c_str())->Fill(m_fpanTauCellBased_ptRatioNeut04,weight);
  Hist(("panTauCellBased_ptRatioChrg01_"+prong).c_str())->Fill(m_fpanTauCellBased_ptRatioChrg01,weight);
  Hist(("panTauCellBased_ptRatioChrg02_"+prong).c_str())->Fill(m_fpanTauCellBased_ptRatioChrg02,weight);
  Hist(("panTauCellBased_ptRatioChrg04_"+prong).c_str())->Fill(m_fpanTauCellBased_ptRatioChrg04,weight);
  Hist(("panTauCellBased_fLeadChrg01_"+prong).c_str())->Fill(m_fpanTauCellBased_fLeadChrg01,weight);
  Hist(("panTauCellBased_fLeadChrg02_"+prong).c_str())->Fill(m_fpanTauCellBased_fLeadChrg02,weight);
  Hist(("panTauCellBased_fLeadChrg04_"+prong).c_str())->Fill(m_fpanTauCellBased_fLeadChrg04,weight);
  Hist(("panTauCellBased_eFrac0102_"+prong).c_str())->Fill(m_fpanTauCellBased_eFrac0102,weight);
  Hist(("panTauCellBased_eFrac0204_"+prong).c_str())->Fill(m_fpanTauCellBased_eFrac0204,weight);
  Hist(("panTauCellBased_eFrac0104_"+prong).c_str())->Fill(m_fpanTauCellBased_eFrac0104,weight);
  Hist(("panTauCellBased_eFracChrg0204_"+prong).c_str())->Fill(m_fpanTauCellBased_eFracChrg0204,weight);
  Hist(("panTauCellBased_eFracChrg0104_"+prong).c_str())->Fill(m_fpanTauCellBased_eFracChrg0104,weight);
  Hist(("panTauCellBased_eFracNeut0204_"+prong).c_str())->Fill(m_fpanTauCellBased_eFracNeut0204,weight);
  Hist(("panTauCellBased_eFracNeut0104_"+prong).c_str())->Fill(m_fpanTauCellBased_eFracNeut0104,weight);
  Hist(("panTauCellBased_rCal02_"+prong).c_str())->Fill(m_fpanTauCellBased_rCal02,weight);
  Hist(("panTauCellBased_rCal04_"+prong).c_str())->Fill(m_fpanTauCellBased_rCal04,weight);
  Hist(("panTauCellBased_rCalChrg02_"+prong).c_str())->Fill(m_fpanTauCellBased_rCalChrg02,weight);
  Hist(("panTauCellBased_rCalChrg04_"+prong).c_str())->Fill(m_fpanTauCellBased_rCalChrg04,weight);
  Hist(("panTauCellBased_rCalNeut02_"+prong).c_str())->Fill(m_fpanTauCellBased_rCalNeut02,weight);
  Hist(("panTauCellBased_rCalNeut04_"+prong).c_str())->Fill(m_fpanTauCellBased_rCalNeut04,weight);
  Hist(("panTauCellBased_dRminmaxPtChrg04_"+prong).c_str())->Fill(m_fpanTauCellBased_dRminmaxPtChrg04,weight);
  
  dynamic_cast<TH2F*>(Hist(("pt_tau_CentFrac0102_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->calcVars_corrCentFrac(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_FTrk02_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->calcVars_corrFTrk(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_TrFlightPathSig_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->trFlightPathSig(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_IpSigLeadTrk_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->ipSigLeadTrk(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_MassTrkSys_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->massTrkSys() / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_TrkAvgDist_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->seedCalo_trkAvgDist(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_DrMax_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->seedCalo_dRmax(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_NTracksdrdR_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->seedCalo_wideTrk_n(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_LLHScore_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->SafeLikelihood(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_BDTScore_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->BDTJetScore(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_ptRatio_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->ptRatio(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_pi0_n_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->pi0_n(),weight);
  dynamic_cast<TH2F*>(Hist(("pt_tau_pi0_vistau_m_"+prong).c_str()))->Fill((tau)->TLV().Pt() / GeV,(tau)->pi0_vistau_m() / GeV,weight);
  
  // pantau+cellbased
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_nChrg0204_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_nChrg0204,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_nChrg01_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_nChrg01,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_nChrg02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_nChrg02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_nNeut0204_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_nNeut0204,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_nNeut01_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_nNeut01,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_nNeut02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_nNeut02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_massChrgSys01_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_massChrgSys01 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_massChrgSys02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_massChrgSys02 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_massChrgSys04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_massChrgSys04 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_massNeutSys01_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_massNeutSys01 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_massNeutSys02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_massNeutSys02 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_massNeutSys04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_massNeutSys04 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_visTauM01_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_visTauM01 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_visTauM02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_visTauM02 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_visTauM04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_visTauM04 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_dRmax02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_dRmax02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_dRmax04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_dRmax04,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ipSigLeadTrk_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ipSigLeadTrk,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_trFlightPathSig_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_trFlightPathSig,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ptRatio01_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ptRatio01,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ptRatio02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ptRatio02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ptRatio04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ptRatio04,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ptRatioNeut01_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ptRatioNeut01,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ptRatioNeut02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ptRatioNeut02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ptRatioNeut04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ptRatioNeut04,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ptRatioChrg01_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ptRatioChrg01,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ptRatioChrg02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ptRatioChrg02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_ptRatioChrg04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_ptRatioChrg04,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_fLeadChrg01_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_fLeadChrg01,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_fLeadChrg02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_fLeadChrg02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_fLeadChrg04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_fLeadChrg04,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_eFrac0102_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_eFrac0102,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_eFrac0204_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_eFrac0204,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_eFrac0104_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_eFrac0104,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_eFracChrg0204_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_eFracChrg0204,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_eFracChrg0104_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_eFracChrg0104,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_eFracNeut0204_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_eFracNeut0204,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_eFracNeut0104_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_eFracNeut0104,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_rCal02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_rCal02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_rCal04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_rCal04,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_rCalChrg02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_rCalChrg02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_rCalChrg04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_rCalChrg04,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_rCalNeut02_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_rCalNeut02,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_rCalNeut04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_rCalNeut04,weight);
  dynamic_cast<TH2F*>(Hist(("pt_panTauCellBased_dRminmaxPtChrg04_"+prong).c_str()))->Fill((tau)->TLV().Pt()/GeV,m_fpanTauCellBased_dRminmaxPtChrg04,weight);
  
  dynamic_cast<TH2F*>(Hist(("mu_tau_CentFrac0102_"+prong).c_str()))->Fill(iNVtx,(tau)->calcVars_corrCentFrac(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_FTrk02_"+prong).c_str()))->Fill(iNVtx,(tau)->calcVars_corrFTrk(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_TrFlightPathSig_"+prong).c_str()))->Fill(iNVtx,(tau)->trFlightPathSig(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_IpSigLeadTrk_"+prong).c_str()))->Fill(iNVtx,(tau)->ipSigLeadTrk(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_MassTrkSys_"+prong).c_str()))->Fill(iNVtx,(tau)->massTrkSys() / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_TrkAvgDist_"+prong).c_str()))->Fill(iNVtx,(tau)->seedCalo_trkAvgDist(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_DrMax_"+prong).c_str()))->Fill(iNVtx,(tau)->seedCalo_dRmax(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_NTracksdrdR_"+prong).c_str()))->Fill(iNVtx,(tau)->seedCalo_wideTrk_n(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_LLHScore_"+prong).c_str()))->Fill(iNVtx,(tau)->SafeLikelihood(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_BDTScore_"+prong).c_str()))->Fill(iNVtx,(tau)->BDTJetScore(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_ptRatio_"+prong).c_str()))->Fill(iNVtx,(tau)->ptRatio(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_pi0_n_"+prong).c_str()))->Fill(iNVtx,(tau)->pi0_n(),weight);
  dynamic_cast<TH2F*>(Hist(("mu_tau_pi0_vistau_m_"+prong).c_str()))->Fill(iNVtx,(tau)->pi0_vistau_m() / GeV,weight);
  
  // pantau+cellbased
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_nChrg0204_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_nChrg0204,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_nChrg01_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_nChrg01,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_nChrg02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_nChrg02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_nNeut0204_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_nNeut0204,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_nNeut01_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_nNeut01,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_nNeut02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_nNeut02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_massChrgSys01_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_massChrgSys01 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_massChrgSys02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_massChrgSys02 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_massChrgSys04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_massChrgSys04 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_massNeutSys01_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_massNeutSys01 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_massNeutSys02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_massNeutSys02 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_massNeutSys04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_massNeutSys04 / GeV,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_visTauM01_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_visTauM01,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_visTauM02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_visTauM02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_visTauM04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_visTauM04, weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_dRmax02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_dRmax02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_dRmax04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_dRmax04,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ipSigLeadTrk_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ipSigLeadTrk,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_trFlightPathSig_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_trFlightPathSig,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ptRatio01_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ptRatio01,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ptRatio02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ptRatio02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ptRatio04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ptRatio04,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ptRatioNeut01_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ptRatioNeut01,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ptRatioNeut02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ptRatioNeut02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ptRatioNeut04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ptRatioNeut04,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ptRatioChrg01_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ptRatioChrg01,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ptRatioChrg02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ptRatioChrg02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_ptRatioChrg04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_ptRatioChrg04,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_fLeadChrg01_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_fLeadChrg01,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_fLeadChrg02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_fLeadChrg02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_fLeadChrg04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_fLeadChrg04,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_eFrac0102_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_eFrac0102,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_eFrac0204_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_eFrac0204,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_eFrac0104_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_eFrac0104,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_eFracChrg0204_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_eFracChrg0204,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_eFracChrg0104_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_eFracChrg0104,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_eFracNeut0204_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_eFracNeut0204,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_eFracNeut0104_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_eFracNeut0104,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_rCal02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_rCal02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_rCal04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_rCal04,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_rCalChrg02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_rCalChrg02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_rCalChrg04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_rCalChrg04,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_rCalNeut02_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_rCalNeut02,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_rCalNeut04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_rCalNeut04,weight);
  dynamic_cast<TH2F*>(Hist(("mu_panTauCellBased_dRminmaxPtChrg04_"+prong).c_str()))->Fill(iNVtx,m_fpanTauCellBased_dRminmaxPtChrg04,weight);
}

void VariablePlotter::PlotPerformance(const std::vector<D3PDReader::AnalysisTau*>& vTaus,
				      const unsigned int& iNVtx){
  std::map<std::string, boolFct>::const_iterator it;
  for(auto tau : vTaus){
    float weight = m_WeightTool->getWeight(tau, iNVtx);

    const std::string sProng = (tau->numTrack()==1) ? "1P" : "3P";
    Hist(("nvtx_" + sProng).c_str())->Fill(iNVtx, weight);
    it = m_mid.begin();
    for(; it!=m_mid.end(); ++it){
      if((*it).second(tau)){
	Hist(("h_passed_" + (*it).first + "_" + sProng + "_pt").c_str())->Fill(tau->TLV().Pt() / GeV);
	Hist(("h_passed_" + (*it).first + "_" + sProng + "_nvtx").c_str())->Fill(iNVtx);

	if(!IsData()){
	  Hist(("h_passed_" + (*it).first + "_" + sProng + "_truth_pt").c_str())->Fill(tau->matchVisEt() / GeV);
	}
      }
    }
  }
}

void VariablePlotter::PlotDistributions(const std::vector<D3PDReader::TruthTau*>& vTaus,
					const unsigned int& iNVtx)
{
  for(auto cit : vTaus){
    Hist("truth_tau_nprong")->Fill(cit->nProng());
    const std::string prong = (cit->nProng()==1) ? "1P" : "3P";
    Hist(("truth_tau_vis_pt_"+prong).c_str())->Fill((cit)->TLV().Pt() / GeV);
    Hist(("truth_tau_pt_"+prong).c_str())->Fill((cit)->pt() / GeV);
    Hist(("truth_tau_nvtx_" + prong).c_str())->Fill(iNVtx);	
  }
}

void VariablePlotter::PlotEventBasedVariables(const D3PDReader::AnalysisEventInfo& rEvent, float fWeight){
  Hist("mu")->Fill(rEvent.mu());
}

void VariablePlotter::PlotsForReweighting(const std::vector<D3PDReader::AnalysisTau*>& vTaus, const unsigned int& iNVtx) {
  for(auto tau : vTaus){
    const std::string sProng = (tau->numTrack()==1) ? "1P" : "3P";
    Hist(("tau_pt_unweighted_"+sProng).c_str())->Fill((tau)->TLV().Pt() / GeV);
    Hist(("nvtx_unweighted_" + sProng).c_str())->Fill(iNVtx);
    dynamic_cast<TH2F*> (Hist(("pt_mu_unweighted_" + sProng).c_str()))->Fill((tau)->TLV().Pt() / GeV, iNVtx);
  }
}

void VariablePlotter::BeginInputData(const SInputData& id) throw(SError)
{
  m_pBDTCut = new TauBDTCut();
  m_pLLHCut = new TauLLHCut();

  m_WeightTool->switchOff(b_switchOffWeightTool);
  m_WeightTool->setIsData(IsData());
  m_WeightTool->setInputFile(c_inputfile_weights);
  m_WeightTool->ReadWeights();

  m_pileUpCorrectionTool->setInputFile(c_pileup_correction_file);
  m_pileUpCorrectionTool->switchOff(c_pileup_correction_switchOff);
  m_pileUpCorrectionTool->readInput();
    
  Book(TH1F("mu",";#mu;a.u.",40,0.,40.));

  Book(TH1F("truth_tau_nprong",";true tau n-prong;a.u.",5,-0.5,4.5));

  Book(TH1F("chargedPi_dR",";charged pion dR;a.u.",100,0.,1.));
  Book(TH1F("neutralPi_dR",";neutral pion dR;a.u.",100,0.,1.));
  Book(TH1F("wideTrack_dR",";wide tracks dR;a.u.",100,0.,1.));
  Book(TH1F("neutrals_dR",";neutrals dR;a.u.",100,0.,1.));
  Book(TH1F("chargedHLV_dR",";charged HLV dR;a.u.",100,0.,1.));
  Book(TH1F("neutralHLV_dR",";neutral HLV dR;a.u.",100,0.,1.));
  Book(TH1F("neutral_lowA_HLV_dR",";neutral low A HLV dR;a.u.",100,0.,1.));
  Book(TH1F("neutral_lowB_HLV_dR",";neutral low B HLV dR;a.u.",100,0.,1.));
  Book(TH1F("outerCharged_dR",";outer charged dR;a.u.",100,0.,1.));
  Book(TH1F("outerNeutral_dR",";outer neutral dR;a.u.",100,0.,1.));

  std::vector<std::string> tauID;
  boost::copy(m_mid | boost::adaptors::map_keys, std::back_inserter(tauID));
  std::vector<std::string>::iterator ID;
  std::vector<std::string> vsProng = {"1P", "3P"};
  for(auto sProng : vsProng){
    Book(TH1F(("tau_pt_unweighted_" + sProng).c_str(),";Tau pt;Normalised",1000,0.,1000.));
    Book(TH1F(("nvtx_unweighted_" + sProng).c_str(), ";Average number of bunch crossings per interaction; a.u.", 40, 0., 40.));
    Book(TH2F(("pt_mu_unweighted_" + sProng).c_str(), ";pt;mu",1000,0.,1000.,40,0.,40.));

    Book(TH1F(("tau_pt_" + sProng).c_str(),";Tau pt;Normalised",1000,0.,1000.));
    Book(TH1F(("truth_tau_vis_pt_" + sProng).c_str(),";trueTau pt;Normalised",1000,0.,1000.));
    Book(TH1F(("truth_tau_pt_" + sProng).c_str(),";trueTau pt;Normalised",1000,0.,1000.));
    Book(TH1F(("truth_tau_nvtx_" + sProng).c_str(), ";Average number of bunch crossings per interaction; a.u.", 40, 0., 40.));
    Book(TH1F(("nvtx_" + sProng).c_str(), ";Average number of bunch crossings per interaction; a.u.", 40, 0., 40.));

    Book(TH1F(("tau_LLHScore_" + sProng).c_str(),";Likelihood Score;Normalised",70,-50.,20));
    Book(TH1F(("tau_BDTScore_" + sProng).c_str(),";jet-BDT Score;Normalised",100,0.,1.));

    Book(TH1F(("tau_CentFrac0102_" + sProng).c_str(),";tau_CentFrac0102;Normalised",65,-0.05,1.25));
    Book(TH1F(("tau_FTrk02_" + sProng).c_str(),";tau_FTrk02;Normalised",61,-0.05,3.));
    Book(TH1F(("tau_TrkAvgDist_" + sProng).c_str(),";tau_TrkAvgDist;Normalised",50,0.,0.4));
    Book(TH1F(("tau_NTracksdrdR_" + sProng).c_str(),";tau_NTracksdrdR;Normalised",11,-0.5,10.5));
    Book(TH1F(("tau_IpSigLeadTrk_" + sProng).c_str(),";tau_IpSigLeadTrk;Normalised",200,-50.,50.));
    Book(TH1F(("tau_TrFlightPathSig_" + sProng).c_str(),";tau_TrFlightPathSig;Normalised",60,-15.,45.));
    Book(TH1F(("tau_MassTrkSys_" + sProng).c_str(),";tau_MassTrkSys;Normalised",75,0.,15.));
    Book(TH1F(("tau_DrMax_" + sProng).c_str(),";tau_DrMax;Normalised",51,0.,0.204));
    Book(TH1F(("tau_ptRatio_" + sProng).c_str(),";tau_ptRatio;Normalised",50,0.,2.));
    Book(TH1F(("tau_pi0_n_" + sProng).c_str(),";tau_pi0_n;Normalised",11,-0.5,10.5));
    Book(TH1F(("tau_pi0_vistau_m_" + sProng).c_str(),";tau_pi0_vistau_m;Normalised",75,0.,15.));

    Book(TH1F(("panTauCellBased_nChrg0204_"+sProng).c_str(),";panTauCellBased nChrg0204;Normalised",11,-0.5,10.5));
    Book(TH1F(("panTauCellBased_nChrg01_"+sProng).c_str(),";panTauCellBased nChrg01;Normalised",11,-0.5,10.5));
    Book(TH1F(("panTauCellBased_nChrg02_"+sProng).c_str(),";panTauCellBased nChrg02;Normalised",11,-0.5,10.5));
    Book(TH1F(("panTauCellBased_nNeut0204_"+sProng).c_str(),";panTauCellBased nNeut0204;Normalised",11,-0.5,10.5));
    Book(TH1F(("panTauCellBased_nNeut01_"+sProng).c_str(),";panTauCellBased nNeut01;Normalised",11,-0.5,10.5));
    Book(TH1F(("panTauCellBased_nNeut02_"+sProng).c_str(),";panTauCellBased nNeut02;Normalised",11,-0.5,10.5));
    Book(TH1F(("panTauCellBased_massChrgSys01_"+sProng).c_str(),";panTauCellBased massChrgSys01;Normalised",75,0.,15.));
    Book(TH1F(("panTauCellBased_massChrgSys02_"+sProng).c_str(),";panTauCellBased massChrgSys02;Normalised",75,0.,15.));
    Book(TH1F(("panTauCellBased_massChrgSys04_"+sProng).c_str(),";panTauCellBased massChrgSys04;Normalised",75,0.,15.));
    Book(TH1F(("panTauCellBased_massNeutSys01_"+sProng).c_str(),";panTauCellBased massNeutSys01;Normalised",75,0.,15.));
    Book(TH1F(("panTauCellBased_massNeutSys02_"+sProng).c_str(),";panTauCellBased massNeutSys02;Normalised",75,0.,15.));
    Book(TH1F(("panTauCellBased_massNeutSys04_"+sProng).c_str(),";panTauCellBased massNeutSys04;Normalised",75,0.,15.));
    Book(TH1F(("panTauCellBased_visTauM01_"+sProng).c_str(),";panTauCellBased visTauM01;Normalised",75,0.,15.));
    Book(TH1F(("panTauCellBased_visTauM02_"+sProng).c_str(),";panTauCellBased visTauM02;Normalised",75,0.,15.));
    Book(TH1F(("panTauCellBased_visTauM04_"+sProng).c_str(),";panTauCellBased visTauM04;Normalised",75,0.,15.));
    Book(TH1F(("panTauCellBased_dRmax02_"+sProng).c_str(),";panTauCellBased dRmax02;Normalised",51,0.,0.204));
    Book(TH1F(("panTauCellBased_dRmax04_"+sProng).c_str(),";panTauCellBased dRmax04;Normalised",51,0.,0.408));
    Book(TH1F(("panTauCellBased_ipSigLeadTrk_"+sProng).c_str(),";panTauCellBased ipSigLeadTrk;Normalised",200,-50.,50.));
    Book(TH1F(("panTauCellBased_trFlightPathSig_"+sProng).c_str(),";panTauCellBased trFlightPathSig;Normalised",60,-15.,45.));
    Book(TH1F(("panTauCellBased_ptRatio01_"+sProng).c_str(),";panTauCellBased ptRatio01;Normalised",50,0.,2.));
    Book(TH1F(("panTauCellBased_ptRatio02_"+sProng).c_str(),";panTauCellBased ptRatio02;Normalised",50,0.,2.));
    Book(TH1F(("panTauCellBased_ptRatio04_"+sProng).c_str(),";panTauCellBased ptRatio04;Normalised",50,0.,2.));
    Book(TH1F(("panTauCellBased_ptRatioNeut01_"+sProng).c_str(),";panTauCellBased ptRatioNeut01;Normalised",50,0.,2.));
    Book(TH1F(("panTauCellBased_ptRatioNeut02_"+sProng).c_str(),";panTauCellBased ptRatioNeut02;Normalised",50,0.,2.));
    Book(TH1F(("panTauCellBased_ptRatioNeut04_"+sProng).c_str(),";panTauCellBased ptRatioNeut04;Normalised",50,0.,2.));
    Book(TH1F(("panTauCellBased_ptRatioChrg01_"+sProng).c_str(),";panTauCellBased ptRatioChrg01;Normalised",50,0.,2.));
    Book(TH1F(("panTauCellBased_ptRatioChrg02_"+sProng).c_str(),";panTauCellBased ptRatioChrg02;Normalised",50,0.,2.));
    Book(TH1F(("panTauCellBased_ptRatioChrg04_"+sProng).c_str(),";panTauCellBased ptRatioChrg04;Normalised",50,0.,2.));
    Book(TH1F(("panTauCellBased_fLeadChrg01_"+sProng).c_str(),";panTauCellBased fLeadChrg01;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_fLeadChrg02_"+sProng).c_str(),";panTauCellBased fLeadChrg02;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_fLeadChrg04_"+sProng).c_str(),";panTauCellBased fLeadChrg04;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_eFrac0102_"+sProng).c_str(),";panTauCellBased eFrac0102;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_eFrac0204_"+sProng).c_str(),";panTauCellBased eFrac0204;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_eFrac0104_"+sProng).c_str(),";panTauCellBased eFrac0104;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_eFracChrg0204_"+sProng).c_str(),";panTauCellBased eFracChrg0204;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_eFracChrg0104_"+sProng).c_str(),";panTauCellBased eFracChrg0104;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_eFracNeut0204_"+sProng).c_str(),";panTauCellBased eFracNeut0204;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_eFracNeut0104_"+sProng).c_str(),";panTauCellBased eFracNeut0104;Normalised",60,0.,1.2));
    Book(TH1F(("panTauCellBased_rCal02_"+sProng).c_str(),";panTauCellBased rCal02;Normalised",51,0.,0.204));
    Book(TH1F(("panTauCellBased_rCal04_"+sProng).c_str(),";panTauCellBased rCal04;Normalised",51,0.,0.408));
    Book(TH1F(("panTauCellBased_rCalChrg02_"+sProng).c_str(),";panTauCellBased rCalChrg02;Normalised",51,0.,0.204));
    Book(TH1F(("panTauCellBased_rCalChrg04_"+sProng).c_str(),";panTauCellBased rCalChrg04;Normalised",51,0.,0.408));
    Book(TH1F(("panTauCellBased_rCalNeut02_"+sProng).c_str(),";panTauCellBased rCalNeut02;Normalised",51,0.,0.204));
    Book(TH1F(("panTauCellBased_rCalNeut04_"+sProng).c_str(),";panTauCellBased rCalNeut04;Normalised",51,0.,0.408));
    Book(TH1F(("panTauCellBased_dRminmaxPtChrg04_"+sProng).c_str(),";panTauCellBased dRminmaxPtChrg04;Normalised",50,0.,0.8));
      
    Book(TH2F(("pt_tau_LLHScore_" + sProng).c_str(),";pt;Likelihood Score;Normalised",1000,0.,1000.,70,-50.,20));
    Book(TH2F(("pt_tau_BDTScore_" + sProng).c_str(),";pt;jet-BDT Score;Normalised",1000,0.,1000.,100,0.,1.));

    Book(TH2F(("pt_tau_CentFrac0102_" + sProng).c_str(),";pt;tau_CentFrac0102;Normalised",1000,0.,1000.,65,-0.05,1.25));
    Book(TH2F(("pt_tau_FTrk02_" + sProng).c_str(),";pt;tau_FTrk02;Normalised",1000,0.,1000.,61,-0.05,3));
    Book(TH2F(("pt_tau_TrkAvgDist_" + sProng).c_str(),";pt;tau_TrkAvgDist;Normalised",1000,0.,1000.,50,0.,0.4));
    Book(TH2F(("pt_tau_NTracksdrdR_" + sProng).c_str(),";pt;tau_NTracksdrdR;Normalised",1000,0.,1000.,11,-0.5,10.5));
    Book(TH2F(("pt_tau_IpSigLeadTrk_" + sProng).c_str(),";pt;tau_IpSigLeadTrk;Normalised",1000,0.,1000.,100,-50.,50.));
    Book(TH2F(("pt_tau_TrFlightPathSig_" + sProng).c_str(),";pt;tau_TrFlightPathSig;Normalised",1000,0.,1000.,60,-15.,40.));
    Book(TH2F(("pt_tau_MassTrkSys_" + sProng).c_str(),";pt;tau_MassTrkSys;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_tau_DrMax_" + sProng).c_str(),";pt;tau_DrMax;Normalised",1000,0.,1000.,51,0.,0.204));
    Book(TH2F(("pt_tau_ptRatio_" + sProng).c_str(),";pt;tau_ptRatio;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_tau_pi0_n_" + sProng).c_str(),";pt;tau_pi0_n;Normalised",1000,0.,1000.,11,-0.5,10.5));
    Book(TH2F(("pt_tau_pi0_vistau_m_" + sProng).c_str(),";pt;tau_pi0_vistau_m;Normalised",1000,0.,1000.,75,0.,15.));
    
    Book(TH2F(("pt_panTauCellBased_nChrg0204_"+sProng).c_str(),";pt;panTauCellBased nChrg0204;Normalised",1000,0.,1000.,11,-0.5,10.5));
    Book(TH2F(("pt_panTauCellBased_nChrg01_"+sProng).c_str(),";pt;panTauCellBased nChrg01;Normalised",1000,0.,1000.,11,-0.5,10.5));
    Book(TH2F(("pt_panTauCellBased_nChrg02_"+sProng).c_str(),";pt;panTauCellBased nChrg02;Normalised",1000,0.,1000.,11,-0.5,10.5));
    Book(TH2F(("pt_panTauCellBased_nNeut0204_"+sProng).c_str(),";pt;panTauCellBased nNeut0204;Normalised",1000,0.,1000.,11,-0.5,10.5));
    Book(TH2F(("pt_panTauCellBased_nNeut01_"+sProng).c_str(),";pt;panTauCellBased nNeut01;Normalised",1000,0.,1000.,11,-0.5,10.5));
    Book(TH2F(("pt_panTauCellBased_nNeut02_"+sProng).c_str(),";pt;panTauCellBased nNeut02;Normalised",1000,0.,1000.,11,-0.5,10.5));
    Book(TH2F(("pt_panTauCellBased_massChrgSys01_"+sProng).c_str(),";pt;panTauCellBased massChrgSys01 / GeV;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_panTauCellBased_massChrgSys02_"+sProng).c_str(),";pt;panTauCellBased massChrgSys02 / GeV;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_panTauCellBased_massChrgSys04_"+sProng).c_str(),";pt;panTauCellBased massChrgSys04 / GeV;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_panTauCellBased_massNeutSys01_"+sProng).c_str(),";pt;panTauCellBased massNeutSys01 / GeV;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_panTauCellBased_massNeutSys02_"+sProng).c_str(),";pt;panTauCellBased massNeutSys02 / GeV;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_panTauCellBased_massNeutSys04_"+sProng).c_str(),";pt;panTauCellBased massNeutSys04 / GeV;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_panTauCellBased_visTauM01_"+sProng).c_str(),";pt;panTauCellBased visTauM01 / GeV;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_panTauCellBased_visTauM02_"+sProng).c_str(),";pt;panTauCellBased visTauM02 / GeV;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_panTauCellBased_visTauM04_"+sProng).c_str(),";pt;panTauCellBased visTauM04 / GeV;Normalised",1000,0.,1000.,75,0.,15.));
    Book(TH2F(("pt_panTauCellBased_dRmax02_"+sProng).c_str(),";pt;panTauCellBased dRmax02;Normalised",1000,0.,1000.,51,0.,0.204));
    Book(TH2F(("pt_panTauCellBased_dRmax04_"+sProng).c_str(),";pt;panTauCellBased dRmax04;Normalised",1000,0.,1000.,51,0.,0.408));
    Book(TH2F(("pt_panTauCellBased_ipSigLeadTrk_"+sProng).c_str(),";pt;panTauCellBased ipSigLeadTrk;Normalised",1000,0.,1000.,200,-50.,50.));
    Book(TH2F(("pt_panTauCellBased_trFlightPathSig_"+sProng).c_str(),";pt;panTauCellBased trFlightPathSig;Normalised",1000,0.,1000.,60,-15.,45.));
    Book(TH2F(("pt_panTauCellBased_ptRatio01_"+sProng).c_str(),";pt;panTauCellBased ptRatio01;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_panTauCellBased_ptRatio02_"+sProng).c_str(),";pt;panTauCellBased ptRatio02;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_panTauCellBased_ptRatio04_"+sProng).c_str(),";pt;panTauCellBased ptRatio04;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_panTauCellBased_ptRatioNeut01_"+sProng).c_str(),";pt;panTauCellBased ptRatioNeut01;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_panTauCellBased_ptRatioNeut02_"+sProng).c_str(),";pt;panTauCellBased ptRatioNeut02;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_panTauCellBased_ptRatioNeut04_"+sProng).c_str(),";pt;panTauCellBased ptRatioNeut04;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_panTauCellBased_ptRatioChrg01_"+sProng).c_str(),";pt;panTauCellBased ptRatioChrg01;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_panTauCellBased_ptRatioChrg02_"+sProng).c_str(),";pt;panTauCellBased ptRatioChrg02;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_panTauCellBased_ptRatioChrg04_"+sProng).c_str(),";pt;panTauCellBased ptRatioChrg04;Normalised",1000,0.,1000.,50,0.,2.));
    Book(TH2F(("pt_panTauCellBased_fLeadChrg01_"+sProng).c_str(),";pt;panTauCellBased fLeadChrg01;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_fLeadChrg02_"+sProng).c_str(),";pt;panTauCellBased fLeadChrg02;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_fLeadChrg04_"+sProng).c_str(),";pt;panTauCellBased fLeadChrg04;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_eFrac0102_"+sProng).c_str(),";pt;panTauCellBased eFrac0102;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_eFrac0204_"+sProng).c_str(),";pt;panTauCellBased eFrac0204;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_eFrac0104_"+sProng).c_str(),";pt;panTauCellBased eFrac0104;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_eFracChrg0204_"+sProng).c_str(),";pt;panTauCellBased eFracChrg0204;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_eFracChrg0104_"+sProng).c_str(),";pt;panTauCellBased eFracChrg0104;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_eFracNeut0204_"+sProng).c_str(),";pt;panTauCellBased eFracNeut0204;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_eFracNeut0104_"+sProng).c_str(),";pt;panTauCellBased eFracNeut0104;Normalised",1000,0.,1000.,60,0.,1.2));
    Book(TH2F(("pt_panTauCellBased_rCal02_"+sProng).c_str(),";pt;panTauCellBased rCal02;Normalised",1000,0.,1000.,51,0.,0.204));
    Book(TH2F(("pt_panTauCellBased_rCal04_"+sProng).c_str(),";pt;panTauCellBased rCal04;Normalised",1000,0.,1000.,51,0.,0.408));
    Book(TH2F(("pt_panTauCellBased_rCalChrg02_"+sProng).c_str(),";pt;panTauCellBased rCalChrg02;Normalised",1000,0.,1000.,51,0.,0.204));
    Book(TH2F(("pt_panTauCellBased_rCalChrg04_"+sProng).c_str(),";pt;panTauCellBased rCalChrg04;Normalised",1000,0.,1000.,51,0.,0.408));
    Book(TH2F(("pt_panTauCellBased_rCalNeut02_"+sProng).c_str(),";pt;panTauCellBased rCalNeut02;Normalised",1000,0.,1000.,51,0.,0.204));
    Book(TH2F(("pt_panTauCellBased_rCalNeut04_"+sProng).c_str(),";pt;panTauCellBased rCalNeut04;Normalised",1000,0.,1000.,51,0.,0.408));
    Book(TH2F(("pt_panTauCellBased_dRminmaxPtChrg04_"+sProng).c_str(),";pt;panTauCellBased dRminmaxPtChrg04;Normalised",1000,0.,1000.,50,0.,0.8));
   
    Book(TH2F(("mu_tau_LLHScore_" + sProng).c_str(),";mu;Likelihood Score;Normalised",40,0.,40.,70,-50.,20));
    Book(TH2F(("mu_tau_BDTScore_" + sProng).c_str(),";mu;jet-BDT Score;Normalised",40,0.,40.,100,0.,1.));

    Book(TH2F(("mu_tau_CentFrac0102_" + sProng).c_str(),";mu;tau_CentFrac0102;Normalised",40,0.,40.,50,-0.5,1.25));
    Book(TH2F(("mu_tau_FTrk02_" + sProng).c_str(),";mu;tau_FTrk02;Normalised",40,0.,40.,61,-0.05,3.));
    Book(TH2F(("mu_tau_TrkAvgDist_" + sProng).c_str(),";mu;tau_TrkAvgDist;Normalised",40,0.,40.,50,0.,0.4));
    Book(TH2F(("mu_tau_NTracksdrdR_" + sProng).c_str(),";mu;tau_NTracksdrdR;Normalised",40,0.,40.,11,-0.5,10.5));
    Book(TH2F(("mu_tau_IpSigLeadTrk_" + sProng).c_str(),";mu;tau_IpSigLeadTrk;Normalised",40,0.,40.,200,-50.,50.));
    Book(TH2F(("mu_tau_TrFlightPathSig_" + sProng).c_str(),";mu;tau_TrFlightPathSig;Normalised",40,0.,40.,60,-15.,45.));
    Book(TH2F(("mu_tau_MassTrkSys_" + sProng).c_str(),";mu;tau_MassTrkSys;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_tau_DrMax_" + sProng).c_str(),";mu;tau_DrMax;Normalised",40,0.,40.,51,0.,0.204));
    Book(TH2F(("mu_tau_ptRatio_" + sProng).c_str(),";mu;tau_ptRatio;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_tau_pi0_n_" + sProng).c_str(),";mu;tau_pi0_n;Normalised",40,0.,40.,11,-0.5,10.5));
    Book(TH2F(("mu_tau_pi0_vistau_m_" + sProng).c_str(),";mu;tau_pi0_vistau_m;Normalised",40,0.,40.,75,0.,15.));
    
    Book(TH2F(("mu_panTauCellBased_nChrg0204_"+sProng).c_str(),";mu;panTauCellBased nChrg0204;Normalised",40,0.,40.,11,-0.5,10.5));
    Book(TH2F(("mu_panTauCellBased_nChrg01_"+sProng).c_str(),";mu;panTauCellBased nChrg01;Normalised",40,0.,40.,11,-0.5,10.5));
    Book(TH2F(("mu_panTauCellBased_nChrg02_"+sProng).c_str(),";mu;panTauCellBased nChrg02;Normalised",40,0.,40.,11,-0.5,10.5));
    Book(TH2F(("mu_panTauCellBased_nNeut0204_"+sProng).c_str(),";mu;panTauCellBased nNeut0204;Normalised",40,0.,40.,11,-0.5,10.5));
    Book(TH2F(("mu_panTauCellBased_nNeut01_"+sProng).c_str(),";mu;panTauCellBased nNeut01;Normalised",40,0.,40.,11,-0.5,10.5));
    Book(TH2F(("mu_panTauCellBased_nNeut02_"+sProng).c_str(),";mu;panTauCellBased nNeut02;Normalised",40,0.,40.,11,-0.5,10.5));
    Book(TH2F(("mu_panTauCellBased_massChrgSys01_"+sProng).c_str(),";mu;panTauCellBased massChrgSys01 / GeV;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_panTauCellBased_massChrgSys02_"+sProng).c_str(),";mu;panTauCellBased massChrgSys02 / GeV;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_panTauCellBased_massChrgSys04_"+sProng).c_str(),";mu;panTauCellBased massChrgSys04 / GeV;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_panTauCellBased_massNeutSys01_"+sProng).c_str(),";mu;panTauCellBased massNeutSys01 / GeV;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_panTauCellBased_massNeutSys02_"+sProng).c_str(),";mu;panTauCellBased massNeutSys02 / GeV;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_panTauCellBased_massNeutSys04_"+sProng).c_str(),";mu;panTauCellBased massNeutSys04 / GeV;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_panTauCellBased_visTauM01_"+sProng).c_str(),";mu;panTauCellBased visTauM01 / GeV;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_panTauCellBased_visTauM02_"+sProng).c_str(),";mu;panTauCellBased visTauM02 / GeV;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_panTauCellBased_visTauM04_"+sProng).c_str(),";mu;panTauCellBased visTauM04 / GeV;Normalised",40,0.,40.,75,0.,15.));
    Book(TH2F(("mu_panTauCellBased_dRmax02_"+sProng).c_str(),";mu;panTauCellBased dRmax02;Normalised",40,0.,40.,51,0.,0.208));
    Book(TH2F(("mu_panTauCellBased_dRmax04_"+sProng).c_str(),";mu;panTauCellBased dRmax04;Normalised",40,0.,40.,51,0.,0.408));
    Book(TH2F(("mu_panTauCellBased_ipSigLeadTrk_"+sProng).c_str(),";mu;panTauCellBased ipSigLeadTrk;Normalised",40,0.,40.,200,-50.,50.));
    Book(TH2F(("mu_panTauCellBased_trFlightPathSig_"+sProng).c_str(),";mu;panTauCellBased trFlightPathSig;Normalised",40,0.,40.,60,-15.,45.));
    Book(TH2F(("mu_panTauCellBased_ptRatio01_"+sProng).c_str(),";mu;panTauCellBased ptRatio01;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_panTauCellBased_ptRatio02_"+sProng).c_str(),";mu;panTauCellBased ptRatio02;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_panTauCellBased_ptRatio04_"+sProng).c_str(),";mu;panTauCellBased ptRatio04;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_panTauCellBased_ptRatioNeut01_"+sProng).c_str(),";mu;panTauCellBased ptRatioNeut01;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_panTauCellBased_ptRatioNeut02_"+sProng).c_str(),";mu;panTauCellBased ptRatioNeut02;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_panTauCellBased_ptRatioNeut04_"+sProng).c_str(),";mu;panTauCellBased ptRatioNeut04;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_panTauCellBased_ptRatioChrg01_"+sProng).c_str(),";mu;panTauCellBased ptRatioChrg01;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_panTauCellBased_ptRatioChrg02_"+sProng).c_str(),";mu;panTauCellBased ptRatioChrg02;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_panTauCellBased_ptRatioChrg04_"+sProng).c_str(),";mu;panTauCellBased ptRatioChrg04;Normalised",40,0.,40.,50,0.,2.));
    Book(TH2F(("mu_panTauCellBased_fLeadChrg01_"+sProng).c_str(),";mu;panTauCellBased fLeadChrg01;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_fLeadChrg02_"+sProng).c_str(),";mu;panTauCellBased fLeadChrg02;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_fLeadChrg04_"+sProng).c_str(),";mu;panTauCellBased fLeadChrg04;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_eFrac0102_"+sProng).c_str(),";mu;panTauCellBased eFrac0102;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_eFrac0204_"+sProng).c_str(),";mu;panTauCellBased eFrac0204;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_eFrac0104_"+sProng).c_str(),";mu;panTauCellBased eFrac0104;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_eFracChrg0204_"+sProng).c_str(),";mu;panTauCellBased eFracChrg0204;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_eFracChrg0104_"+sProng).c_str(),";mu;panTauCellBased eFracChrg0104;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_eFracNeut0204_"+sProng).c_str(),";mu;panTauCellBased eFracNeut0204;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_eFracNeut0104_"+sProng).c_str(),";mu;panTauCellBased eFracNeut0104;Normalised",40,0.,40.,60,0.,1.2));
    Book(TH2F(("mu_panTauCellBased_rCal02_"+sProng).c_str(),";mu;panTauCellBased rCal02;Normalised",40,0.,40.,51,0.,0.204));
    Book(TH2F(("mu_panTauCellBased_rCal04_"+sProng).c_str(),";mu;panTauCellBased rCal04;Normalised",40,0.,40.,51,0.,0.408));
    Book(TH2F(("mu_panTauCellBased_rCalChrg02_"+sProng).c_str(),";mu;panTauCellBased rCalChrg02;Normalised",40,0.,40.,51,0.,0.208));
    Book(TH2F(("mu_panTauCellBased_rCalChrg04_"+sProng).c_str(),";mu;panTauCellBased rCalChrg04;Normalised",40,0.,40.,51,0.,0.408));
    Book(TH2F(("mu_panTauCellBased_rCalNeut02_"+sProng).c_str(),";mu;panTauCellBased rCalNeut02;Normalised",40,0.,40.,51,0.,0.204));
    Book(TH2F(("mu_panTauCellBased_rCalNeut04_"+sProng).c_str(),";mu;panTauCellBased rCalNeut04;Normalised",40,0.,40.,51,0.,0.408));
    Book(TH2F(("mu_panTauCellBased_dRminmaxPtChrg04_"+sProng).c_str(),";mu;panTauCellBased dRminmaxPtChrg04;Normalised",40,0.,40.,50,0.,0.8));
    
    ID = tauID.begin();
    for(; ID!=tauID.end(); ++ID){
      Book(TH1F(("h_passed_" + *ID + "_" + sProng + "_pt").c_str(), ";tau pt; a.u.", 1000, 0., 1000.));
      Book(TH1F(("h_passed_" + *ID + "_" + sProng + "_nvtx").c_str(), ";Average number of bunch crossings per interaction; a.u.", 40, 0., 40.));
      Book(TH1F(("h_passed_" + *ID + "_" + sProng + "_truth_pt").c_str(), ";tau pt; a.u.", 1000, 0., 1000.));
    }
  }
}

#endif // VariablePlotter_CXX
#endif // SKIM
