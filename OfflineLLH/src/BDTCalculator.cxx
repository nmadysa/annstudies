#ifndef SKIM
#include "BDTCalculator.h"

ClassImp(BDTCalculator);

BDTCalculator::BDTCalculator(const char* name, const TString& option, Bool_t bVerbose):
  TMVA::Reader(option,bVerbose),fName(name)
{}

#endif
