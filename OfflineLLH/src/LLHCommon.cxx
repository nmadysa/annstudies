#ifndef LLHCommon_CXX
#define LLHCommon_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

// core include(s)
#include "Region.h"
#include "Utilities.h"
#include "CutTemplateManager.h"
//ROOT includes
#include "TMath.h"

// TauCommon include(s)
#include "TauSelector.h"
#include "TruthTauSelector.h"
#include "JetSelector.h"
#include "VertexSelector.h"
#include "JetCleaningTool.h"

// LLH include(s)
#include "LLHCommon.h"

// GoodRunList
#include "GoodRunsLists/TGoodRunsListReader.h"

ClassImp(LLHCommon);

//________________________________________________________________________________________________________________
LLHCommon::LLHCommon():
  CycleBase(),
  m_rEventInfo(m_lEntryNumber),
  m_rTruthTauCollection(m_lEntryNumber),
// #ifndef SKIM
//   m_rAnalysisTauCollection(m_lEntryNumber),
// #endif
  m_rTauCollection(m_lEntryNumber),
  m_rJetCollection(m_lEntryNumber),
  m_rVertexCollection(m_lEntryNumber),
  m_GRL("GoodRunsList")
{

  DeclareProperty("apply_grl",c_do_grl = false);  
  DeclareProperty("GRL_file",c_grl_file);
  DeclareProperty("do_jet_cleaning",c_do_jet_cleaning = false);
  
  // declare tools
  DeclareTool(new TauSelector(this, "TauSelector"));
  DeclareTool(new JetSelector(this, "JetSelector"));
  DeclareTool(new VertexSelector(this, "VertexSelector"));
  DeclareTool(new TruthTauSelector(this,"TruthTauSelector"));
  DeclareTool(new Region(this,"BaselineSelection"));
  DeclareTool(new JetCleaningTool(this, "JetCleaning"));

  CutTemplateManager* pManager = CutTemplateManager::GetInstance();

  // event cuts

  pManager->RegisterCutTemplate(new CutTemplate<TYPELIST_0 > ("GRL",boost::bind(&LLHCommon::CheckGRL,this,_1)));
  pManager->RegisterCutTemplate(new CutTemplate<TYPELIST_1(int) > ("JETCLEANING",boost::bind(&LLHCommon::CheckJetCleaning,this,_1, _2)));
  pManager->RegisterCutTemplate(new CutTemplate<TYPELIST_1(int) > ("NTAUS",boost::bind(&LLHCommon::CheckNTaus,this,_1,_2)));
  pManager->RegisterCutTemplate(new CutTemplate<TYPELIST_1(int) > ("NVERTEX",boost::bind(&LLHCommon::CheckNVertex,this,_1,_2)));
}

void LLHCommon::StartInputData(const SInputData& id) throw(SError)
{
  if(c_do_grl && IsData())
    m_GRL = *(dynamic_cast<Root::TGoodRunsList*>(GetConfigObject("GoodRunsList")));
}

bool LLHCommon::CheckGRL(float& fWeight)
{
  if(IsData() && c_do_grl)
    return m_GRL.HasRunLumiBlock( m_rEventInfo.RunNumber(), m_rEventInfo.lbn() );
  return true;
}

bool LLHCommon::SelectPhysicsObjects(float& fWeight)
{
  if(!IsData())
    (*dynamic_cast<TruthTauSelector*>(GetTool("TruthTauSelector")))(m_vTruthTaus,fWeight);
  (*dynamic_cast<TauSelector*>(GetTool("TauSelector")))(m_vTaus,fWeight);
  (*dynamic_cast<JetSelector*>(GetTool("JetSelector")))(m_vJets,fWeight);
  (*dynamic_cast<VertexSelector*>(GetTool("VertexSelector")))(m_vVertices, fWeight);
  return true;
}

bool LLHCommon::CheckJetCleaning(float fWeight, int iLevel)
{
  if( IsData() && c_do_jet_cleaning ) 
    return (*dynamic_cast<JetCleaningTool*>(GetTool("JetCleaning"))).DoJetCleaning( m_vJets, iLevel );
  return true;
}

bool LLHCommon::CheckNTaus(float fWeight, int iMin)
{
  return (m_vTaus.size() > (unsigned int)iMin);
}

bool LLHCommon::CheckNVertex(float fWeight, int iMin){
  return (m_vVertices.size() >= (unsigned int)iMin);
}

void LLHCommon::StartCycle() throw(SError)
{
  // apply GRL?
  if(c_do_grl){
    // get GRL interpreter
    Root::TGoodRunsListReader reader(c_grl_file);
    if(!reader.Interpret()){
      m_logger << FATAL << "Good Runs List requested " << c_grl_file << " but unable to Interpret!" << SLogger::endmsg;
      throw SError(SError::StopExecution);
    }
    else{
      m_logger << INFO << "Using Good Runs List: " << c_grl_file << SLogger::endmsg;
      m_GRL = reader.GetMergedGoodRunsList();
      m_GRL.SetName("GoodRunsList");
      // make GRL object available for all worker nodes
      AddConfigObject(&m_GRL);
    }
  }  
}


#endif // LLHCommon_CXX
