#ifndef SKIM
#ifndef LLHCycle_CXX
#define LLHCycle_CXX

#ifndef __CINT__
// Boost include(s)
#include "boost/bind.hpp"
#endif // __CINT__

// core include(s)
#include "Region.h"
#include "Utilities.h"

// custom include(s)
#include "LLHCycle.h"
#include "VariablePlotter.h"
#include "LLHCommon.h"
#include "LLHCommonUtilities.h"

ClassImp(LLHCycle);

using namespace std;
LLHCycle::LLHCycle():
  AnalysisLLHCommon()
{
  DeclareTool(new VariablePlotter(this, "VariablePlotter"));
}


LLHCycle::~LLHCycle()
{
  // clear memory
  m_oTauCreator.ClearAllObjects();
  m_oTruthTauCreator.ClearAllObjects();
}

void LLHCycle::AnalyseEvent(const SInputData& id,double dWeight) throw(SError)
{
  // clear memory
  m_oTauCreator.ClearAllObjects();
  m_oTauCreator.CreateObjects(m_rTauCollection.n(),
			      m_rTauCollection,
			      m_vTaus);
  // reset event weight
  m_fEventWeight = 1;
  // sort objects according to decreasing pt
  std::sort(m_vTaus.begin(),m_vTaus.end(),Particle::pt_comp);
  SelectPhysicsObjects(m_fEventWeight);
  // false, true->skip event ?
  dynamic_cast<Region*>(GetTool("BaselineSelection"))->IsInRegion(m_fEventWeight,true);
  
  if(!IsData()){
    m_oTruthTauCreator.ClearAllObjects();
    m_oTruthTauCreator.CreateObjects(m_rTruthTauCollection.n(),
  				     m_rTruthTauCollection,
  				     m_vTruthTaus);
    std::sort(m_vTruthTaus.begin(),m_vTruthTaus.end(),Particle::pt_comp);
    m_pPlotter->PlotDistributions(m_vTruthTaus,
  				  m_rEventInfo.mu());
    TightTruthMatch(m_vTaus);
  }
  else{
    TightRecoNumTrack(m_vTaus);
  }
  m_pPlotter->PlotPerformance(m_vTaus,
  			      m_rEventInfo.mu());
  m_pPlotter->PlotDistributions(m_vTaus,
				m_rEventInfo.mu(),
  				m_fEventWeight);
  m_pPlotter->PlotEventBasedVariables(m_rEventInfo,
  				      m_fEventWeight);
  m_pPlotter->PlotsForReweighting(m_vTaus,
				  m_rEventInfo.mu());  
}

void LLHCycle::StartCycle() throw (SError){
  m_pPlotter = dynamic_cast<VariablePlotter*>(GetTool("VariablePlotter"));
}

void LLHCycle::StartInputFile(const SInputData& id) throw(SError)
{
  m_pPlotter = dynamic_cast<VariablePlotter*>(GetTool("VariablePlotter"));
  m_rEventInfo.ReadFrom(GetInputTree(c_tree_name.c_str()));
  m_rTauCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
  if(!IsData())
    m_rTruthTauCollection.ReadFrom(GetInputTree(c_tree_name.c_str()));
}

void LLHCycle::FinishMasterInputData(const SInputData& id) throw(SError)
{
  CopySkimCutflow(id);
}

#endif // LLHCycle_CXX
#endif // SKIM
