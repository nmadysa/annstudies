#!/bin/bash
#-----------------------------------------------------------------------------
# file:     AnalysisModul/setup.sh
# author:   Christian Gumpert <cgumpert@cern.ch>
# date :    2010-11-17
#------------------------------------------------------------------------------

# returns the absolute path of this script
AnalysisModul_DIR=`dirname $(echo $(readlink -f ${BASH_SOURCE[0]:-$0}))`

#------------------------------------------------------------------------------
# verify paths and convert to absolute paths
#------------------------------------------------------------------------------

sframe="SFrame"
if [ -d ${sframe} ]; then
    sframe=`cd ${sframe}; pwd`
    echo "  SFrame directory found: ${sframe}"
else
    echo "  SFrame directory not found."
    echo "  Please see the README for instructions."
    return
fi

#------------------------------------------------------------------------------
# setup SFrame
#------------------------------------------------------------------------------

echo "  Setting up SFrame..."

cwd=${PWD}
cd ${sframe}
source setup.sh
cd ${cwd}
export PYTHONAPTH=${cwd}/TauCommon/macros:$PYTHONPATH
echo "  AnalysisModul setup done."

#-----------------------------------------------------
# setup RootCore
#-----------------------------------------------------
cd ExternalTools/RootCore
./configure
source scripts/setup.sh
cd ${cwd}

#------------------------------------------------------
# switch to Tau D3PDs
#------------------------------------------------------
echo "Configure makefiles for Tau D3PDs"
find . -type f -name Makefile -exec sed -i "s/SMWZ/TAU/g" {} ';'
echo "  AnalysisModul setup done."

export PYTHONPATH=$PYTHONPATH:$PWD/PythonTools/
