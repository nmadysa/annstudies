#!/bin/bash
sh launch_tnt.sh MC mc12_8TeV.147818.Pythia8_AU2CTEQ6L1_Ztautau.merge.NTUP_TAU.e1176_s1479_s1470_r3553_r3549_p1344/ .v1
sh launch_tnt.sh MC mc12_8TeV.147812.Pythia8_AU2CTEQ6L1_Wtaunu.merge.NTUP_TAU.e1176_s1479_s1470_r3553_r3549_p1344/ .v1
sh launch_tnt.sh MC mc12_8TeV.170201.Pythia8_AU2CTEQ6L1_Zprime250tautau.merge.NTUP_TAU.e1176_s1479_s1470_r3553_r3549_p1344/ .v1
sh launch_tnt.sh MC mc12_8TeV.170202.Pythia8_AU2CTEQ6L1_Zprime500tautau.merge.NTUP_TAU.e1176_s1479_s1470_r3553_r3549_p1344/ .v1
sh launch_tnt.sh MC mc12_8TeV.170204.Pythia8_AU2CTEQ6L1_Zprime1000tautau.merge.NTUP_TAU.e1176_s1479_s1470_r3553_r3549_p1344/ .v1
