#!/bin/bash

TYPE="$1"
INDS="$2"
VER="$3"

if [ "$TYPE" == "MC" ]
then
    sh launch_job.sh RunTNTMCGrid.xml "input/InputTemp.xml" "$VER" "${INDS%/}" "TNTCycle.mc.TNT.root"
elif [ "$TYPE" == "DATA" ]
then
    sh launch_job.sh RunTNTDataGrid.xml "input/InputTemp.xml" "$VER" "${INDS%/}" "TNTCycle.data.TNT.root"
fi
