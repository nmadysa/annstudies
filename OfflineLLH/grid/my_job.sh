#!/bin/bash

# custom variables to be set before execution
CONFIGPATH=OfflineLLH/config
XMLFILE="$1"
INPUTFILE=$CONFIGPATH/"$2"
OUTDIR=OfflineLLH/out

# environment variables from WN
CURDIR=$(pwd)

# print config
echo
echo "running with:"
echo "CONFIGPATH = \"$CONFIGPATH\""
echo "XMLFILE = \"$XMLFILE\""
echo "INPUTFILE = \"$INPUTFILE\""
echo "OUTDIR = \"$OUTDIR\""
echo "CURDIR = \"$CURDIR\""
echo

# build and compile everything
sh build_job.sh # > build_log

if [ $? -eq 0 ]
then
  echo "Package successfully built"
else
  echo "Error during building package"
fi
echo

if [ ! -d $OUTDIR ]
then
  mkdir -pv $OUTDIR
fi

# mv build_log $OUTDIR

# parse input list
OLD_IFS=$IFS
IFS=,

# does directory exist in which the input file should be placed?
if [ ! -d ${INPUTFILE%/*} ]
then
    echo "Will call mkdir for " ${INPUTFILE%/*}
    mkdir -pv ${INPUTFILE%/*}
fi

# remove already existing input file and create new one
if [ -f $INPUTFILE ]
then
  rm -v $INPUTFILE
fi

# prepand names of files with current directory to get absolute path
PROTOCOLS="gsidcap://,dcap://,root://"
echo "store input files in \"$INPUTFILE\""
for i in $3
do
  echo $i
  PREFIX=${CURDIR%/}/
  for pre in $PROTOCOLS;
  do
    MATCH=`expr "$i" : "$pre"`
    if [ $MATCH -gt 0 ]
    then
      PREFIX=""
      break;
    fi
  done
  echo "adding...\"$PREFIX$i\""
  echo "<In FileName=\"$PREFIX$i\" Lumi=\"1.0\" />" >> $INPUTFILE
done

IFS=$OLD_IFS

# source SFrame
source setup.sh

# run SFrame from config path
echo "entering config path \"${CURDIR%/}/$CONFIGPATH\"" 
cd ${CURDIR%/}/$CONFIGPATH
echo "run \"sframe_main $XMLFILE\""
sframe_main $XMLFILE

# change to ouput dir
echo "entering output path \"${CURDIR%/}/$OUTDIR\""
cd ${CURDIR%/}/$OUTDIR
ls -lah
mv -v * $CURDIR
cd $CURDIR

