#!/bin/bash

# unpack all archives
for i in $(ls *.tar)
do
  echo "unpacking $i"
  tar -xf $i
done

# source setup script
source setup.sh

# set boost path
echo "setting BOOST paths"
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupBoost boost-1.55.0-python2.7-x86_64-slc6-gcc48
export BOOST_INC=${boostLibDir/lib/include}   # replcace "lib" to "include"
export BOOST_LIB=${boostLibDir}
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BOOST_LIB 
echo "BOOST_INC = \"$BOOST_INC\""
echo "BOOST_LIB = \"$BOOST_LIB\""

# compile all needed packages
echo "compiling..."
cd ExternalTools; make; cd -; # source ExternalTools/RootCore/scripts/setup.sh;
  
echo "...SFrame/..."
cd SFrame/; make distclean; make; cd -;

echo "...core/..."
cd core/; make distclean; make; cd -;

echo "...D3PDObjects/..."
cd D3PDObjects/; make distclean; make; cd -;

echo "...AnalysisUtilities/..."
cd AnalysisUtilities/; make distclean; make; cd -;

echo "...TauCommon/..."
cd TauCommon/; make distclean; make; cd -;

echo "...D3PDReaderObjects/..."
cd D3PDReaderObjects/; make distclean; make; cd -;

echo "...OfflineLLH..."
cd OfflineLLH; make distclean; make -f Makefile.SKIM; cd -;

