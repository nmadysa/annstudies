#!/bin/bash

XML=$1
INPUT=$2
POSTFIX=$3
INDS=$4
OUTROOT=$5
temp=${INDS#group.*\.}
temp=${temp/.DESD_CALJET.t0pro14_v01.v05-01/}
OUTDS="user.shanisch.${temp}$POSTFIX"

prun --exec "sh my_job.sh \"$XML\" \"$INPUT\" \"%IN\" " --outDS "$OUTDS" --inDS "$INDS/" --rootVer=5.34/14 --cmtConfig=x86_64-slc6-gcc48-opt --outputs $OUTROOT --nGBPerJob=MAX --extFile "*.tar"

