import argparse
import sys
import os
import ROOT

sys.path.append('../../TauCommon/macros/')

from InputHandler import Reader
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from Configurator import Configurator
from CommonTools import Writer
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

def makeDistributionPlots(rf, path, varConfig):

    def prepare(variable,config,np):
        processes = [k.GetName() for k in rf.GetListOfKeys()]
    
        plotDict = {}

        def getOption(optionname):
            option = ''
            if config.has_key('histoptions'):
                for optionitem in config['histoptions']:
                    if optionitem.has_key(optionname):
                        option = optionitem[optionname]
                return option

        def getTitle():
            title = ''
            if config.has_key('title'):
                    title = config['title']
                    return title

        def getPosition(ltype):
            position = "{"
            if config.has_key(ltype):
                it = config[ltype].__iter__()
                for x in config[ltype]:
                    for key,value in x.items():
                        position += "\"%s\": %s, " % (key, value)
            position = position.rstrip(" ,") + "}"
            return position
                        
        
        for k in processes:
            t = rf.Get(k)
            eval('t.Draw("%s>>htemp(%s,%s,%s)","%s >= %s && %s <= %s && tau_numTrack == %s","")' % (variable,getOption('bins'),getOption('min'),getOption('max'),variable,getOption('min'),variable,getOption('max'),np))
            h = ROOT.gPad.GetPrimitive("htemp")
            try:
                plotDict[k].append(h.Clone(variable))
            except KeyError:
                plotDict[k] = h.Clone(variable)

        h_data = None
        h_signal = None
        
        for key, hist in plotDict.items():
            if key.count("Period"):
                if h_data is None:
                    h_data = hist.Clone()
                else:
                    h_data.Add(hist)
            else:
                if h_signal is None:
                    h_signal = hist.Clone()
                else:
                    h_signal.Add(hist)

        c.cd()

        h_signal.SetStats(0)
        h_data.SetStats(0)
        h_signal.GetXaxis().SetTitle(getTitle())
        h_signal.Scale(1./h_signal.Integral())
        h_data.Scale(1./h_data.Integral())
        h_signal.Draw("")
        h_signal.GetYaxis().SetRangeUser(0,1.1*max(h_signal.GetMaximum(),h_data.GetMaximum()))
        h_signal.SetFillColor(2)
        h_signal.SetFillStyle(3244)
        h_signal.SetLineColor(2)
        h_signal.SetLineStyle(1)
        h_data.Draw("sameP")
        h_data.SetMarkerColor(1)
        h_data.SetMarkerStyle(20)
        
        formatter.addLumiText(c,
                              lumi = 20281.4,
                              pos = eval(getPosition('lumiposition')),
                              splitLumiText = False)
        
        formatter.addATLASLabel(c,
                                description = 'internal',
                                pos=eval(getPosition('labelposition')),
                                size = 0.045,
                                offset = 0.14)
        
        formatter.addLegendToCanvas(c,
                                    legendOptions = eval(getPosition('legendoptions')),
                                    overwriteLabels = ['signal','data'])

        prongText = str(np)+'-prong, Data 2012'
        prongPos = eval(getPosition('labelposition'))
        prongPos['y'] = prongPos['y'] - 0.04
        formatter.addText(c, prongText, prongPos)
                                                                        
        c.SaveAs(os.path.join(path,variable+'_'+str(np)+'.eps'))
        c.Print(os.path.join(path, 'TMVAInputDistributions.ps'))
        del h_data
        del h_signal
        
    c = ROOT.TCanvas("c","c",1000,700)
    c.Print(os.path.join(path,'TMVAInputDistributions.ps['))

    for np in [1,3]:        
        for variable, config in varConfig.items():
            if config.has_key('nprong') and np != config['nprong']:
                continue
            else:
                prepare(variable,config,np)

    c.Print(os.path.join(path,'TMVAInputDistributions.ps]'))
        
def main(argv):
    parser = argparse.ArgumentParser(description = 'plotter for variable distribution from TMVA')
    parser.add_argument('file', help = 'input file (TMVA input)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    args = parser.parse_args()

    varConfig = Configurator('configs/tmvavariables_config.yml').__dict__

    rf = ROOT.TFile.Open(args.file, 'READ')
    path = args.outdir
    
    makeDistributionPlots(rf,path,varConfig)
    
if __name__ == '__main__':
    main(sys.argv[1:])
