#from pyAMI.client import AMIClient
from pyAMI.client import Client
from pyAMI.objects import DatasetInfo
from pyAMI.auth import AMI_CONFIG, create_auth_config
from pyAMI.query import get_datasets, get_runs

class AMIHandler:
    def __init__(self):
        self.init_ami_client()
        
    def init_ami_client(self):
        client = Client()
        import os
        if not os.path.exists(AMI_CONFIG):
            create_auth_config()
        client.read_config(AMI_CONFIG)
        self.client = client

    def get_runs(self, period, year=12):
        if period:
            return get_runs(self.client, periods=period, year=year)
        else:
            return get_runs(self.client, year=year)
        
    def get_event(self, run, stream, flatten=False):
        return get_datasets(self.client, '%',fields='events', type='NTUP_TAU', project='data12_8TeV', run=run, stream=stream, flatten=flatten)

    def get_generated_events(self, ds, project='mc12_8TeV'):
        return int(get_datasets(self.client, ds, fields='events', type='NTUP_TAU', project=project, flatten=True)[0][0])
