import argparse
import sys
import os
import ROOT

sys.path.append('../../TauCommon/macros/')

from InputHandler import Reader
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from Configurator import Configurator
from CommonTools import Writer
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

def makeNicePlot(signal, data, config, name, switchOffATLASLabel):
    formatter.normalise(signal)
    formatter.normalise(data)
    canvas = plotter.plotHistograms([signal, data],
                                    plotOptions = [PO(fill_style = 3354,
                                                      fill_color = ROOT.kRed),
                                                   PO()],
                                    drawOptions = ['hist', 'p'],
                                    canvasName = config['name'],
                                    canvasTitle = config['name'])
    if name.endswith('1P'):
        prongText = '1-prong'
    else:
        prongText = '3-prong'
    def getPosition(ltype): 
        position = "{"
        if config.has_key(ltype): 
            it = config[ltype].__iter__()
            for x in config[ltype]:
                for key,value in x.items():
                    position += "\"%s\": %s, " % (key, value)
            position = position.rstrip(" ,") + "}"
            return position

    maximum = max(signal.GetMaximum(), data.GetMaximum()) * 1.1
    if config.has_key('xtitle'):
        formatter.setTitle(signal,
                           config['xtitle'],
                           'x')
    else:
        formatter.setTitle(signal,
                           config['name'],
                           'x')
    if config.has_key('ytitle'):
        formatter.setTitle(signal,
                           config['ytitle'],
                           'y')        
    if config.has_key('rebin'):
        formatter.rebin(signal,
                        config['rebin'])
        formatter.rebin(data,
                        config['rebin'])
    if config.has_key('ymax'):
        formatter.setMinMax(signal, 
                            maximum=config['ymax'])
    # if config.has_key('ymax'):
    #     formatter.setMinMax(signal, 
    #                         maximum=maximum)
    if config.has_key('xmin') and config.has_key('xmax'):
        formatter.setMinMax(signal,
                            minimum=config['xmin'],                         
                            maximum=config['xmax'],
                            axis = 'x')
    elif config.has_key('xmax'):
        formatter.setMinMax(signal,
                            maximum=config['xmax'],
                            axis = 'x')
    elif config.has_key('xmin'):
        formatter.setMinMax(signal, 
                            minimum=config['xmin'],
                            axis = 'x')
    prongPos = eval(getPosition('labelposition'))
    prongPos['y'] = prongPos['y'] - 0.035
    formatter.addText(canvas, prongText, prongPos)
    if config.has_key('ndivision'):
        signal.GetXaxis().SetNdivisions(404, False)
    formatter.addLumiText(canvas,
                          lumi = 7653,
                          pos = eval(getPosition('lumiposition')),
                          splitLumiText = True)
    if not switchOffATLASLabel:
        formatter.addATLASLabel(canvas,
                                pos=eval(getPosition('labelposition')),
                                description = 'Internal',
                                offset = 0.125)
    formatter.addLegendToCanvas(canvas,
                                legendOptions = eval(getPosition('legendoptions')),
                                overwriteLabels = ['Z/Z\'#rightarrow#tau#tau', 'Di-jet data (2012)'],
                                textsize = 0.04)
    canvas.Update()
    writer.dumpCanvas(canvas, image = name + '.eps' )
    
def main(argv):
    parser = argparse.ArgumentParser(description = 'Nice LLH variable plotter')
    parser.add_argument('filelist', nargs = '+', help = 'input filelist')
    parser.add_argument('--outdir', '-o', default = None, help = 'output directory')
    parser.add_argument('--switchOffATLAS', '-s', action = 'store_true', default = False, help = 'switch off ATLAS label')
    args = parser.parse_args()

    variablesConfig = Configurator('configs/variables_config_thesis.yml').__dict__
    # variablesConfig = Configurator('configs/variables_config_test.yml').__dict__
    # variablesConfig = Configurator('configs/variables_config.yml').__dict__
    global reader
    reader = Reader(args.filelist)
    global writer
    writer = Writer(args.outdir)
    for variable, config in variablesConfig.items():
        mc_hists, data = reader.readHistogramAndMerge('VariablePlotter/' + config['name'],
                                                      1., mergeOptions = reader.mergeOptions) 
        makeNicePlot(mc_hists['Signal'], data, config, variable, args.switchOffATLAS)
    
if __name__ == '__main__':
    main(sys.argv[1:])
