import os, sys
import Merger
import argparse
import logging
import Utilities
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
_logger = Utilities._logger

"""
module load pyAMI/4.0.3

python merge.py /ZIH.fast/users/morgenst/tmp2/ /ZIH.fast/users/morgenst/tmp/ --idPo
"""

def main(argv):
    parser = argparse.ArgumentParser(description='merge script for skims')
    parser.add_argument('inPath', help='input path')
    parser.add_argument('outPath', help='output path for merged samples')
    parser.add_argument('--idPos', type=int, default=2, help='position of id (for parsing ds name)')
    parser.add_argument('--debug',nargs='?',dest='level',choices=['DEBUG','INFO','WARNING','CRITICAL','ERROR'],default='WARNING',help='logging level')
    parser.add_argument('--skip', action='store_true', default=False, help='skip already skimmed samples')
    parser.add_argument('--dsList', default=None, help='list of official dataset names')
    parser.add_argument('--check', action='store_true', default=False, help='check if skim is complete')
    parser.add_argument('--check_only', action='store_true', default=False, help='only check for consistency, but do not merge')
    parser.add_argument('--mergeDir', default=None, help='directory for temporary merged files')
    parser.add_argument('--grl', default=None, help='good runs list')
    parser.add_argument('--isContainer', action='store_true', default=False, help='skimmed data period containter')
    parser.add_argument('--period_only', default=None, type=str, help='merge only given period')
    parser.add_argument('--ignore_runs_before', default=None, help='ignore runs before given run for check')
    args = parser.parse_args()
    _logger.setLevel(eval('logging.' + args.level))
    merger = Merger.Merger(args.inPath,
                           args.outPath,
                           skip=args.skip,
                           mergeDir=args.mergeDir,
                           maxFileSize=2000,
                           isContainer = args.isContainer,
                           periodOnly = args.period_only)
    helper = Merger.DatasetHelper(args.isContainer,
                                  args.period_only)
    checker = None
    if args.check or args.check_only:
        checker = Merger.DatasetChecker(args.dsList,
                                        args.grl,
                                        args.ignore_runs_before)
    datasetlist = helper.parseDirectory(args.inPath, args.idPos)
    for sample in datasetlist.keys():
        if args.check or args.check_only:
            checker.check(datasetlist[sample], sample)
        if args.check_only:
            continue
        merger.merge(datasetlist[sample], sample)
    if checker:
        checker.printSummary()
    
if __name__=='__main__':
    main(sys.argv[1:])
