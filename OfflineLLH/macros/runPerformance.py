import os
from ShellUtils import mkdir

variableset_1p = ["CentFrac0102",
                  "FTrk02",
                  "pi0_vistau_m",
                  "ptRatio",
                  "TrkAvgDist",
                  "pi0_n",
                  "IpSigLeadTrk",
                  "NTracksdrdR"]
variableset_mp = ["CentFrac0102",
                  "FTrk02",
                  "pi0_vistau_m",
                  "ptRatio",
                  "TrkAvgDist",
                  "pi0_n",
                  "MassTrkSys",
                  "DrMax",
                  "TrFligthPathSig"]

for prong in ['1','m']:
    for variable in eval('variableset_%sp' % prong):
        print prong, variable
        
        mkdir('../out/plots/signalefficiency/fromEffEval/current_result/%s' % variable)
        os.system('python PlotSignalEfficiency.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.mc.Signal.%sp.root -o ../out/plots/signalefficiency/fromEffEval/current_result/%s/' % (variable,prong,variable))

        mkdir('../out/plots/backgroundefficiency/fromEffEval/current_result/%s' % variable)
        os.system('python PlotBackgroundEfficiency.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.data.Data.%sp.root -o ../out/plots/backgroundefficiency/fromEffEval/current_result/%s/' % (variable,prong,variable))

        mkdir('../out/plots/effvsrej/fromEffEval/current_result/%s' % variable)
        for i in range(0,180,20):
            os.system('python PlotEffVsRej.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.mc.Signal.%sp.root ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.data.Data.%sp.root -o ../out/plots/effvsrej/fromEffEval/current_result/%s/ -min %i -max %i' % (variable,prong,variable,prong,variable,i,i+20))

        for i in range(200,800,200):
            os.system('python PlotEffVsRej.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.mc.Signal.%sp.root ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.data.Data.%sp.root -o ../out/plots/effvsrej/fromEffEval/current_result/%s/ -min %i -max %i' % (variable,prong,variable,prong,variable,i,i+200))

        os.system('python PlotEffVsRej.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.mc.Signal.%sp.root ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.data.Data.%sp.root -o ../out/plots/effvsrej/fromEffEval/current_result/%s/' % (variable,prong,variable,prong,variable))
        os.system('python PlotEffVsRej.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.mc.Signal.%sp.root ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.data.Data.%sp.root -o ../out/plots/effvsrej/fromEffEval/current_result/%s/ -min %i' % (variable,prong,variable,prong,variable,25))
        os.system('python PlotEffVsRej.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.mc.Signal.%sp.root ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.data.Data.%sp.root -o ../out/plots/effvsrej/fromEffEval/current_result/%s/ -min %i' % (variable,prong,variable,prong,variable,40))
        os.system('python PlotEffVsRej.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.mc.Signal.%sp.root ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.data.Data.%sp.root -o ../out/plots/effvsrej/fromEffEval/current_result/%s/ -min %i -max %i' % (variable,prong,variable,prong,variable,25,40))
        os.system('python PlotEffVsRej.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.mc.Signal.%sp.root ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.data.Data.%sp.root -o ../out/plots/effvsrej/fromEffEval/current_result/%s/ -min %i -max %i' % (variable,prong,variable,prong,variable,40,160))

        mkdir('../out/plots/scores/fromEffEval/current_result/%s' % variable)
        os.system('python PlotScores.py ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.mc.Signal.%sp.root ../out/EfficiencyEvaluationCycle/current_result/%s/EfficiencyEvaluationCycle.data.Data.%sp.root -o ../out/plots/scores/fromEffEval/current_result/%s/' % (variable,prong,variable,prong,variable))
