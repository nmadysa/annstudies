import argparse
import sys
import os
from array import array
sys.path.append('../../TauCommon/macros')
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from InputHandler import Reader
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from HistFormatter import CanvasOptions as CO
from HistFormatter import plot_graph
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)
    
class EfficiencyCalculator:
    def __init__(self, filelist, isData = False):
        self.initialise(filelist)
        self.isData = isData

    def update(self, filelist):
        #self.rf.Close()
        self.initialise(filelist)
        
    def initialise(self, filelist):
        self.filelist = filelist
        self.reader = Reader(filelist)
        #assert 'mc' in self.rf.GetName() 
        self.parseProngness()
        self.algos = ['LLH','BDT','defaultLLH','defaultBDT']
        self.workpoints = ['loose', 'medium', 'tight']
        self.rebin = 4
        self.plotdict = {}
    
    def parseProngness(self):
        if '1p' in self.filelist[0]:
            self.prong = 1
            self.prongtext = "1-prong"
            self.outputdirectory = "1prong"
        elif '3p' in self.filelist[0]:
            self.prong = 3
            self.prongtext = "3-prong"
            self.outputdirectory = "3prong"
        else:
            self.prong = 3
            self.prongtext = "multi-prong"
            self.outputdirectory = "mprong"

    def getEfficiency(self,
                      hist,
                      norm):
        efficiency = hist.Clone()
        efficiency.Divide(norm)
        return efficiency

    def calculateEfficiencyVsPt(self):
        hNorm = self.getNormHist()
        norm_hist = hNorm.ProjectionY("norm", self.prong+1, self.prong+1, 0, -1)
        norm_hist = norm_hist.Rebin(self.rebin)
        result = {}
        for algo in self.algos:
            signal_loose, signal_medium, signal_tight = 0, 0, 0
            for wp in self.workpoints:
                exec("hist_%s = self.getHistPt('%s', '%s')" % (wp, wp, algo))
                exec("hist_%s=hist_%s.Rebin(self.rebin)" % (wp,wp))
                exec("signal_%s = self.getEfficiency(hist_%s,norm_hist)" % (wp, wp))

            signal_loose.SetName("loose")
            signal_medium.SetName("medium")
            signal_tight.SetName("tight")
            result.update({algo : [signal_loose,
                                   signal_medium,
                                   signal_tight]})
        return result
    
    def calculateEfficiencyVsMu(self):
        hNorm = self.getNormHist()
        norm_hist = hNorm.ProjectionZ("norm",
                                      self.prong+1,
                                      self.prong+1,
                                      0, -1)
        result = {}
        for algo in self.algos:
            signal_loose, signal_medium, signal_tight = 0, 0, 0
            for wp in self.workpoints:        
                exec("hist_%s = self.getHistMu('%s', '%s')" % (wp, wp, algo))
                exec("signal_%s = self.getEfficiency(hist_%s,norm_hist)" % (wp, wp))
            result.update({algo : [signal_loose,
                                   signal_medium,
                                   signal_tight]})
        return result
    
    def getHistPt(self,
                  wp,
                  algo):
        
        return self.reader.readHistogramAndMerge('h_passed_%s_%s_pt' % (algo, wp),
                                                 1.,
                                                 mergeOptions = self.reader.mergeOptions)[0]['Signal']
                                    
    def getHistMu(self,
                  wp,
                  algo):
        return self.reader.readHistogramAndMerge('h_passed_%s_%s_mu' % (algo, wp),
                                                 1.,
                                                 mergeOptions = self.reader.mergeOptions)[0]['Signal']

    def getTotalHistPt(self):
        return self.reader.readHistogramAndMerge('h_total_pt',
                                                 1.,
                                                 mergeOptions = self.reader.mergeOptions)[0]['Signal']

    def getTotalHistMu(self):
        return self.reader.readHistogramAndMerge('h_total_mu',
                                                 1.,
                                                 mergeOptions = self.reader.mergeOptions)[0]['Signal']

    def getNormHist(self):
        return self.reader.readHistogramAndMerge('h_truthPt',
                                                 1.,
                                                 mergeOptions = self.reader.mergeOptions)[0]['Signal']
    
