import argparse
import sys
import os
sys.path.append('../../TauCommon/macros')
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from HistFormatter import plot_graph
import ROOT
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

class PerformancePlotter:
    def __init__(self):
        self.parseProngness()
        self.methods = ['BDTScore','default_BDTScore','LLHScore','default_LLHScore']
        
    def parseProngness(self):
        if '1p' in sig_rf.GetName():
            self.prong = 1
            self.prongtext = "1 prong"
            self.outputdirectory = "1prong"
        elif '3p' in sig_rf.GetName():
            self.prong = 3
            self.prongtext = "3 prong"
            self.outputdirectory = "3prong"
        else:
            self.prong = 3
            self.prongtext = "multi-prong"
            self.outputdirectory = "mprong"

    def getEfficiency(self,
                      hist,
                      norm = None):
        efficiency = []
        if norm == None:
            denominator = hist.Integral()
        else:
            denominator = norm.Integral()
        
        for i in range(hist.GetNbinsX() + 1, 0, -1): 
            eff = hist.Integral(i, -1) / denominator
            efficiency.append(eff)

        return efficiency

    def getRejection(self,
                     hist): 
        tmp = self.getEfficiency(hist)
        return [1. / i  if i > 0  else 0. for i in tmp] 

    def getRocCurve(self):
        hNorm = sig_rf.Get("h_truthPt")
        norm_hist = hNorm.ProjectionY("norm", self.prong + 1, self.prong + 1, 0, -1)

        for method1 in self.methods:
            for method2 in self.methods:
                if method1 == method2:
                    continue
                mc1 = sig_rf.Get('h_%s' % method1)
                data1 = bg_rf.Get('h_%s' % method1)
                signal1 = self.getEfficiency(mc1,norm_hist)
                bkg1 = self.getRejection(data1)
    
                mc2 = sig_rf.Get('h_%s' % method2)
                data2 = bg_rf.Get('h_%s' % method2)
                signal2 = self.getEfficiency(mc2,norm_hist)
                bkg2 = self.getRejection(data2)
                
                graph_1 = plot_graph('RejVsEff',
                                    signal1,
                                    bkg1,
                                    draw_options = 'ac')
                graph_2 = plot_graph('RejVsEff',
                                    signal2,
                                    bkg2,
                                    draw_options = 'csame')
                
                canvas = graph_1['canvas']
                g1 = graph_1['graph']
                g2 = graph_2['graph']
                canvas.cd()

                formatter.setTitle(g1,
                                   'signal efficiency',
                                   'x')
                formatter.setTitle(g1,
                                   'background rejection',
                                   'y')
                formatter.setMinMax(g1,
                                    0.,
                                    2500.,
                                    'y')
                formatter.setMinMax(g1,
                                    0.1,
                                    1.,
                                    'x')
                formatter.addLegendToCanvas(canvas,
                                            legendOptions = {'x1': 0.6, 'x2': 0.9, 'y1': 0.7, 'y2': 0.9},
                                            overwriteLabels = [method1, method2],
                                            overwriteDrawOptions = ['L', 'L'])
                formatter.addLumiText(canvas,
                                      lumi = 20281.4,
                                      pos = {'x':0.2,'y':0.3},
                                      splitLumiText = False)
                formatter.addText(canvas,
                                  self.prongtext,
                                  pos = {'x': 0.2, 'y': 0.25})
                formatter.addATLASLabel(canvas,
                                        description = 'internal',
                                        pos= {'x':0.2,'y':0.2},
                                        size = 0.045,
                                        offset = 0.11)

                g1.SetLineWidth(3)
                g2.SetLineWidth(3)
                g2.SetLineColor(ROOT.kRed)

                canvas.SetLogy()
                canvas.Update()
                canvas.SaveAs(os.path.join(path,'BkgRejVsEff_%s_%s_%s.eps' % (method1, method2, self.outputdirectory)))

    def getScoreDist(self):
        for method in self.methods:
            mc = sig_rf.Get('h_%s' % method)
            data = bg_rf.Get('h_%s' % method)
            mc.Scale(1/mc.Integral())
            data.Scale(1/data.Integral())
            mc.Rebin(2)
            data.Rebin(2)
            
            canvas = plotter.plotHistograms([mc, data],
                                            plotOptions = [PO(marker_color = ROOT.kRed,
                                                              marker_style = 20),
                                                           PO(marker_color = ROOT.kBlack,
                                                              marker_style = 20)],
                                            drawOptions = ['P','P'],
                                            canvasName = 'score distributions')
            
            formatter.setMinMax(mc,
                                0.,
                                1.5*mc.GetMaximum(),
                                'y')
            formatter.setTitle(mc,
                               method,
                               'x')
            formatter.addLegendToCanvas(canvas,
                                legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.8, 'y2': 0.9},
                                overwriteLabels = ['signal', 'data'])
            formatter.addLumiText(canvas,
                                  lumi = 20281.4,
                                  pos = {'x':0.6,'y':0.85},
                                  splitLumiText = False)
            formatter.addText(canvas,
                              self.prongtext,
                              pos = {'x': 0.6, 'y': 0.8})
            formatter.addATLASLabel(canvas,
                                    description = 'internal',
                                    pos= {'x':0.6,'y':0.75},
                                    size = 0.045,
                                    offset = 0.11)

            canvas.Update()
            canvas.SaveAs(os.path.join(path,'Score_%s_%s.eps' % (method,self.outputdirectory)))

                
def main(argv):
    parser = argparse.ArgumentParser(description = 'plotter for background rejection vs. signal efficiency')
    parser.add_argument('filelist', nargs = 2, help = 'input filelist (efficiency evaluation output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    args = parser.parse_args()
    
    global sig_rf
    global bg_rf
    for f in args.filelist:
        if 'mc' in f:
            sig_rf = ROOT.TFile.Open(f,'READ')
        if 'data' in f:
            bg_rf = ROOT.TFile.Open(f,'READ')
        else:
            assert 'mc' or 'data' in f
            
    global path
    path = args.outdir

    performancePlotter = PerformancePlotter()
    performancePlotter.getRocCurve()
    performancePlotter.getScoreDist()
    
if __name__ == '__main__':
    main(sys.argv[1:])
