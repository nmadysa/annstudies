import os
import os.path as op
import shutil
import sys
sys.path.append('../../TauCommon/macros')
import Samples
import Utilities
#import AMIHandler as AH

from math import pow

logger = Utilities._logger
class Merger:
    def __init__(self, inPath, outPath, maxFileSize=100, skip = False, mergeDir=None, isContainer = False, periodOnly = None):
        self.inPath = inPath
        self.outPath = outPath
        self.isContainer = isContainer
        self.periodOnly = periodOnly
        if not op.exists(op.abspath(self.outPath)):
            logger.debug("Requested output path %s does not exists. Going to create it." % (self.outPath))
            os.makedirs(self.outPath)

        self.fCounter = 0
        self.maxFileSize = maxFileSize
        self.mergeDir = None
        self.skip = skip
        if mergeDir and op.exists(mergeDir):
            s = os.statvfs(mergeDir)
            if s.f_bavail * s.f_bsize / pow(1024.,3) > 1:
                logger.debug('%s exists and has enough space. Use it as tmp merge directory' %(mergeDir))
                self.mergeDir = mergeDir
            
        if op.exists('/scratch/users/morgenst/') and not self.mergeDir:
            s = os.statvfs('/scratch')
            if s.f_bavail * s.f_bsize / pow(1024.,3) > 1:
                logger.debug('/scratch/users/morgenst/ exists and has enough space. Use it as tmp merge directory')
                self.mergeDir = '/scratch/users/morgenst/'

        if op.exists('/tmp/') and not self.mergeDir:
            s = os.statvfs('/tmp')
            if s.f_bavail * s.f_bsize / pow(1024.,3) > 1:
                logger.debug('/tmp/ exists and has enough space. Use it as tmp merge directory')
                self.mergeDir = '/tmp/'
                
        if not self.mergeDir:
            logger.debug('Use it as current directory merge directory')
            self.mergeDir = os.abspath(os.curdir)
            
                
    def merge(self, dirlist, outDir):
        print outDir
        if self.isContainer and self.periodOnly:
            if not outDir == 'period'+self.periodOnly:
                return
        self.fCounter = 0
        if not op.exists(op.join(self.outPath, outDir)):
            logger.debug("Output directory %s does not exists. Going to create it." % (op.join(self.outPath, outDir)))
            os.mkdir(op.join(self.outPath, outDir))
        filelist = self.getFiles(dirlist)
        currentSampleSize = self.getSampleSize(filelist)
        mergedSize = self.getMergedSize(op.join(self.outPath, outDir))
        try:
            if self.skip and (mergedSize/currentSampleSize) > 0.9:
                logger.debug("Sample already merged. Merged size %f is in tolerance of full sample size of %f" %(mergedSize, currentSampleSize))
                return
        except ZeroDivisionError:
            pass
        if self.isContainer and not len(os.listdir(op.join(self.outPath, outDir))) == 0:
            for f in os.listdir(op.join(self.outPath, outDir)):
                os.remove(op.join(self.outPath, outDir, f))
        for fileSlice in filelist:
            if len(fileSlice) == 1:
                shutil.copy2(fileSlice[0], op.join(self.outPath, outDir))
                self.fCounter+=1
            else:
                print len(fileSlice)
                self.hadd(fileSlice)
                logger.debug('Will move %s%i.root to %s'%(op.join(self.mergeDir, 'file'), self.fCounter, op.join(self.outPath, outDir)))
                if not op.exists('%s%i.root' % (op.join(self.mergeDir, 'file'), self.fCounter)):
                    logger.fatal('Merged file %s%i.root does not exist' % (op.join(self.mergeDir, 'file'), self.fCounter))
                    self.fCounter+=1
                    continue
                if op.exists('%s%i.root' % (op.join(self.outPath, outDir, 'file'), self.fCounter)):
                    logger.debug("File already exists in destination. Going to delete it")
                    os.remove('%s%i.root' % (op.join(self.outPath, outDir, 'file'), self.fCounter))
                shutil.move('%s%i.root' %(op.join(self.mergeDir, 'file'), self.fCounter), op.join(self.outPath, outDir))
                self.fCounter+=1

    def hadd(self, filelist):
        cmd = 'hadd -f %s%i.root' %(op.join(self.mergeDir, 'file'), self.fCounter)
        for f in filelist:
            cmd += ' %s' % (f)
        os.system(cmd)
        
    def getSampleSize(self, filelist):
        size = 0
        for fileSlice in filelist:
            for f in fileSlice:
                size+=op.getsize(f)
        return float(size)

    def getMergedSize(self, directory):
        size = 0
        for f in os.listdir(directory):
            size += op.getsize(op.join(directory,f))
        return float(size)
    
    def getFiles(self, dirlist):
        files = []
        filesSlice = []
        fileSize = 0
        for d in dirlist:
            for f in os.listdir(op.join(self.inPath,d)):
                if f.count('log'):
                    continue
                fileSize += op.getsize(op.join(self.inPath,d,f))/1024./1024.
                filesSlice.append(op.join(self.inPath,d,f))
                if fileSize > self.maxFileSize or len(filesSlice) > 400:
                    fileSize = 0
                    files.append(filesSlice)
                    filesSlice = []
        if len(filesSlice):
            files.append(filesSlice)
        return files
                    
        
class DatasetHelper:
    def __init__(self, isContainer = False, periodOnly = None):
        self.isContainer = isContainer
        self.periodOnly = periodOnly
        self.Samples = {}
        samplelist = Samples.Samples
        for sample in samplelist:
            if not samplelist[sample].isData():
                self.Samples[samplelist[sample].ID]=sample

    def parseDirectory(self, inPath, pos):
        datasetlist = {}
        isData = False
        for sample in os.listdir(inPath):
            if sample.count('data12_8TeV') or inPath.count('Data') or inPath.count('period') or self.isContainer:
                isData =True
            if not isData:
                logger.debug("Searching for sample name with ID: %i" % (int(sample.split('.')[pos])))
            try:
                if not isData:
                    dsname = self.Samples[int(sample.split('.')[pos])]
                else:
                    dsname = str(sample.split('.')[pos].lstrip('0'))
                    if not self.isContainer and self.periodOnly:
                        if dsname == 'period' + self.periodOnly:
                            continue
            except KeyError:
                logger.fatal("Could not find sample with ID: %i" % (int(sample.split('.')[pos])))
                dsname = "None"
            try:
                datasetlist[dsname].append(op.join(inPath, sample))
            except KeyError:
                datasetlist[dsname] = [op.join(inPath, sample)]
        return datasetlist

class DatasetChecker:
    def __init__(self, dsList, grl, ignoredRuns = None):
        self.dsList = dsList
        logger.debug('Create AMIHandler instance')
        self.AH = AH.AMIHandler()
        self.isData = False
        self.runs = []
        self.extraRuns = []
        self.incompleteDS = []
        self.grl = grl
        self.grlRuns = None
        self.ignoredRuns = ignoredRuns
        if self.grl:
            self.readGRL()
        self.readInput()
            
    def readInput(self):
        self.datasets = {}
        self.dsList = open(self.dsList, 'r')
        for dataset in self.dsList:
            dataset = dataset.replace('\n','')
            if dataset.count('data12_8TeV'):
                self.isData = True
            dsid = int(dataset.split('.')[1].lstrip('0'))
            logger.debug("Add dataset %s" % (dataset))
            self.datasets[dsid] = dataset
        if self.isData:
            self.getRunsFromAMI()
        if not self.grlRuns is None:
            self.runs = list(set(self.runs).intersection(set(self.grlRuns)))
            
    def readGRL(self):
        from xml.etree import ElementTree as ET
        etree = ET.parse(self.grl)
        for node in etree.getroot().iter('Metadata'):
            if node.attrib['Name'] == 'RunList':
                self.grlRuns = [int(x) for x in node.text.split(',')]

    def getRunsFromAMI(self):
        self.runs = self.AH.get_runs(None,12)
        
    def check(self, directories, sample):
        nProcessedEvents = 0
        for d in directories:
            for f in os.listdir(d):
                nProcessedEvents += self.readEventsFromROOTFile(op.join(d, f))
        try:
            nGeneratedEvents = self.AH.get_generated_events(self.datasets[int(sample)], self.datasets[int(sample)].split('.')[0])
        except IndexError:
            nGeneratedEvents = 0
        if not nProcessedEvents == nGeneratedEvents:
            logger.info('Mismatch in dataset with ID/run %s' % (sample))
            logger.info('%i events processed while %i events generated' %(nProcessedEvents, nGeneratedEvents))
            self.incompleteDS.append([sample, nProcessedEvents, nGeneratedEvents])
        if self.isData:
            try:
                self.runs.remove(int(sample))
                logger.debug('Remove run %s from all runs' %(sample))
            except:
                logger.debug('Could not find run %s in list of all runs' %(sample))
                self.extraRuns.append(sample)

    def readEventsFromROOTFile(self, fN):
        from ROOT import TFile, TH1
        f = TFile.Open(fN,'READ')
        h = f.Get('BaselineSelection/h_cut_flow_raw')
        if h == None:
            logger.error("Cannot find cutflow in file %s. Will skip this file." %(fN))
            return 0
        n_events = h.GetBinContent(1)
        f.Close()
        return int(n_events)

    def printSummary(self):
        if self.isData:
            print '-----------------------   Missing runs   -----------------------'
            for run in self.runs:
                if self.isData and self.ignoredRuns and int(run) < int(self.ignoredRuns):
                    continue
                print run

            print '-----------------------   Extra runs   -----------------------'
            for run in self.extraRuns:
                print run

        print '-----------------------   Incomplete runs   -----------------------'
        for ds in self.incompleteDS:
            print '{0:25s}'.format(ds[0]),
            print '{0:10.0f}'.format(ds[1]),
            print '{0:10.0f}'.format(ds[2])
