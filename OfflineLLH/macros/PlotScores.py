import argparse
import os
import sys
sys.path.append('../../TauCommon/macros')
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from HistFormatter import plot_graph
from ShellUtils import mkdir
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

class ScorePlotter:
    def __init__(self):
        self.parseProngness()
        self.algos = ['BDTScore','default_BDTScore','LLHScore','default_LLHScore']
        self.plotdict = {}
        
    def parseProngness(self):
        if '1p' in sig_rf.GetName():
            self.prong = 1
            self.prongtext = "1 prong"
            self.outputdirectory = "1prong"
        elif '3p' in sig_rf.GetName():
            self.prong = 3
            self.prongtext = "3 prong"
            self.outputdirectory = "3prong"
        else:
            self.prong = 3
            self.prongtext = "multi-prong"
            self.outputdirectory = "mprong"

    def getScorePlot(self):
        for algo in self.algos:        
            mc = sig_rf.Get('h_%s' % algo)
            data = bg_rf.Get('h_%s' % algo)
            mc.Rebin(5)
            data.Rebin(5)
            mc.Scale(1/mc.Integral())
            data.Scale(1/data.Integral())

            canvas = ROOT.TCanvas("c", "c", 800, 700)
            canvas.cd()
            mc.SetFillColor(ROOT.kRed)
            data.SetFillColor(ROOT.kBlack)
            mc.SetLineColor(ROOT.kRed)
            data.SetLineColor(ROOT.kBlack)
            mc.SetFillStyle(3003)
            data.SetFillStyle(3003)
            mc.Draw("hist")
            mc.SetMaximum(1.5*mc.GetMaximum())
            mc.GetXaxis().SetTitle(algo)
            data.Draw("samehist")
            mc.SetMarkerColor(2)
            mc.SetMarkerStyle(20)

            mc.SetName("signal")
            data.SetName("background")
            self.plotdict.update({algo : (mc,data)})
            
            formatter.setTitle(mc,
                               algo,
                               'x')        
            formatter.addLegendToCanvas(canvas,
                                        legendOptions = {'x1': 0.6, 'x2': 0.9, 'y1': 0.7, 'y2': 0.9},
                                        overwriteLabels = ['signal', 'data'])#,
                                        #overwriteDrawOptions = ['F', 'F'])
            # formatter.addText(canvas,
            #                   self.prongtext,
            #                   pos = {'x': 0.2, 'y': 0.9})

            formatter.addATLASLabel(canvas,
                                    pos={'x': 0.2, 'y': 0.8},
                                    description = 'Internal',
                                    offset = 0.145)

            canvas.Update()
            canvas.SaveAs(os.path.join(path,'Score_%s_%s.eps' % (algo,self.outputdirectory)))

    def dumpPlots(self,filename,variable):        
        for algo,plots in self.plotdict.items():
            fOut = ROOT.TFile.Open(filename,'update')
            if(not fOut.FindKey(self.outputdirectory)):
                pdir = ROOT.gDirectory.mkdir(self.outputdirectory)
            pdir = fOut.GetDirectory(self.outputdirectory)
            fOut.cd(self.outputdirectory)
            if(not pdir.FindKey(algo)):
                adir = ROOT.gDirectory.mkdir(algo)
            pdir.cd(algo)
            adir = pdir.GetDirectory(algo)
            for plot in plots:
                adir.Delete("%s_%s;1" % (plot.GetName(),variable))
                plot.Write(plot.GetName()+'_'+variable)
            fOut.Close()

def main(argv):
    parser = argparse.ArgumentParser(description = 'plotter for score distributions')
    parser.add_argument('filelist', nargs = 2, help = 'input filelist (efficiency evaluation output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')    
    parser.add_argument('--variable', '-v', help = 'variable to be replaced')
    args = parser.parse_args()

    global sig_rf
    global bg_rf
    for f in args.filelist:
        if 'mc' in f:
            sig_rf = ROOT.TFile.Open(f,'READ')
        if 'data' in f:
            bg_rf = ROOT.TFile.Open(f,'READ')
        else:
            assert 'mc' or 'data' in f

    global path
    path = args.outdir
    mkdir(path)
    scorePlotter = ScorePlotter()
    scorePlotter.getScorePlot()
    if (args.variable!=None):
        temppath = path.replace('/'+args.variable+'/','/')
        filename = temppath+'VariableExclusionScores.root'
        scorePlotter.dumpPlots(filename,args.variable)
        
if __name__ == '__main__':
    main(sys.argv[1:])
