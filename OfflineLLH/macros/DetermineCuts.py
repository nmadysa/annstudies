import argparse
import sys
sys.path.append('../../TauCommon/macros/')
import os
from array import array
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
import HistFormatter as hf
from InputHandler import Reader
from ShellUtils import mkdir

ROOT.gROOT.SetBatch(True)
                            
class CutCalculator:
    def __init__(self, filelist):
        self.filelist = filelist
        self.reader = Reader(filelist)
        #self.rf = ROOT.TFile.Open(rfName, 'READ')
        self.parseProngness()
        self.initialiseEfficiencyTargets()
        self.initialisePtBins()
        self.getNormalisation()
        self.methods = ['LLH','BDT','default_LLH','default_BDT']
        self.recoVsTruthPtHist = self.reader.readHistogramAndMerge('h_TruthpT_OfflinepT', 1., mergeOptions = self.reader.mergeOptions)[0]['Signal']
        #self.rf.Get("h_TruthpT_OfflinepT")

    def initialisePtBins(self):
        # self.ptBins =[i for i in range(25,101)]
        # self.ptBins.extend([151, 201, 1001])
        self.ptBins = [i for i in range(15,25,10)]
        self.ptBins.extend([i for i in range(25,100)])
        self.ptBins.extend([i for i in range(100,200,10)])
        self.ptBins.extend([i for i in range(200,1000,200)])
        
    def parseProngness(self):
        if '1p' in self.filelist[0]:
            self.prong = 1
            self.outputdirectory = "1prong"
        else:
            self.prong = 3
            self.outputdirectory = "3prong"    
            
    def initialiseEfficiencyTargets(self):
        if self.prong == 1:
            self.efficiencyTargets = {'tight': 0.4,
                                      'medium': 0.6,
                                      'loose': 0.7}
        else:
            self.efficiencyTargets = {'tight': 0.35,
                                      'medium': 0.55,
                                      'loose': 0.65}
            
    def getProjection(self,
                      inputHist,
                      bin,
                      offset):
        if bin+offset < len(self.ptBins):
            histProjection = inputHist.ProjectionY("_py",
                                                   self.ptBins[bin],
                                                   self.ptBins[bin+offset])
            if histProjection.Integral() !=0:
                pt = (inputHist.GetXaxis().GetBinLowEdge(self.ptBins[bin])+1+inputHist.GetXaxis().GetBinUpEdge(self.ptBins[bin+offset]))/2
                lowBin = int(inputHist.GetXaxis().GetBinLowEdge(self.ptBins[bin])+1)
                upBin = int(inputHist.GetXaxis().GetBinUpEdge(self.ptBins[bin+offset]))
                return histProjection, pt, bin+offset, lowBin, upBin
            else:
                return self.getProjection(inputHist,
                                          bin,
                                          offset + 1)
        else:
            return None
        
    def getEfficiency(self,
                      hist,
                      norm):
        #hist.Draw()
        #raw_input()
        for j in range(hist.GetNbinsX() + 1, 0, -1):
            eff = hist.Integral(j, -1) / norm
            for key, targetEff in self.efficiencyTargets.items():
                if eff < targetEff:
                    continue
                if len(self.values[key]) == len(self.values['pt']):
                    continue
                self.values[key].append(hist.GetXaxis().GetBinCenter(j))
                if key == 'loose':
                    return

    def getRecoPt(self,
                  lowBin,
                  upBin):
        
        recoVsTruthPtHistProjection = self.recoVsTruthPtHist.ProjectionY("_px" + str(lowBin),
                                                                         lowBin,
                                                                         upBin)
        return recoVsTruthPtHistProjection.GetMean(),recoVsTruthPtHistProjection.GetRMS()
            
    def getCutValues(self,
                     method):
        score = self.reader.readHistogramAndMerge("h_pT_"+method+"Score",
                                                  1.,
                                                  mergeOptions = self.reader.mergeOptions)[0]['Signal']
        self.values = {'pt': [],
                       'meanrecopt' : [],
                       'rmsrecopt' : []}
        for key in self.efficiencyTargets.keys():
            self.values[key] = []
        ptBin = 0
        while ptBin in range(len(self.ptBins)):
            if self.getProjection(score,
                                  ptBin,
                                  offset = 1):
                h_score,pt,ptBin,lowBin,upBin = self.getProjection(score,
                                                                   ptBin,
                                                                   offset = 1)
                self.values['pt'].append(pt)
                meanrecopt,rmsrecopt = self.getRecoPt(lowBin, upBin)
                self.values['meanrecopt'].append(meanrecopt)
                self.values['rmsrecopt'].append(rmsrecopt)
                norm = sum(self.hNorm.GetBinContent(bin) for bin in range(lowBin, upBin+1))
                self.getEfficiency(h_score,
                                   norm)
            else:
                return
            
    def getPlots(self,
                 method):
        self.getCutValues(method)
        self.values['pt'].insert(0,0)
        self.values['meanrecopt'].insert(0,0)
        self.values['rmsrecopt'].insert(0,0)
        print self.values
        self.values['loose'].insert(0,self.values['loose'][0])
        self.values['medium'].insert(0,self.values['medium'][0])
        self.values['tight'].insert(0,self.values['tight'][0])
        for key, value in self.values.items():
            exec("%s_array = array('f', " % key + str(value) + ")")
        nEntries = len(value)
        for key in self.efficiencyTargets.keys():
            exec("%s_graph = ROOT.TGraph(%i ,meanrecopt_array,%s_array)" % (key, nEntries, key))
            # exec("%s_graph = ROOT.TGraph(%i ,pt_array,%s_array)" % (key, nEntries, key))
            exec('%s_graph.Draw("")' % key)
            exec('%s_graph.SetName("%s")' % (key, key))
            exec("%s_graph.SetTitle('%s %s cut for %i prong')" % (key, key, method, self.prong))
            exec("%s_graph.GetXaxis().SetTitle('reco pt')" % key)
            # exec("%s_graph.GetXaxis().SetTitle('pt')" % key)
        cutPlots = loose_graph,medium_graph,tight_graph
        for mean in ['mean','rms']:
            exec("g_truth_%sRecoPt = ROOT.TGraph(%i, pt_array, %srecopt_array)" % (mean, nEntries, mean))            
            exec("g_truth_%sRecoPt.Draw('*')" % mean)
            exec("g_truth_%sRecoPt.SetName('truth_vs_%sReco_pt')" % (mean,mean))
            exec("g_truth_%sRecoPt.SetTitle('truth vs %s reco pt for %i prong')" % (mean,mean,self.prong))
            exec("g_truth_%sRecoPt.GetXaxis().SetTitle('truth pt')" % mean)
            exec("g_truth_%sRecoPt.GetYaxis().SetTitle('%s reco pt')" % (mean,mean))
        controlPlots = g_truth_meanRecoPt,g_truth_rmsRecoPt

        return cutPlots, controlPlots
        
    def getNormalisation(self):
        hNorm = self.reader.readHistogramAndMerge("h_truthPt",
                                                  1., mergeOptions = self.reader.mergeOptions)[0]['Signal']
        self.hNorm = hNorm.ProjectionY("norm",
                                       self.prong + 1,
                                       self.prong + 1,
                                       0, -1)

    def writeCutFile(self, method, plots):
        print "write file"
        filename = path+"LMT_cuts_"+method+".root"
        fOut = ROOT.TFile.Open(filename,'update')
        if(not fOut.FindKey(self.outputdirectory)):
            adir = ROOT.gDirectory.mkdir(self.outputdirectory)
        fOut.cd(self.outputdirectory)
        adir = fOut.GetDirectory(self.outputdirectory)
        adir.Delete("loose;1")
        adir.Delete("medium;1")
        adir.Delete("tight;1")
        for plot in plots:
            plot.Write()
        fOut.Close()

    def writeControlFile(self, plots):
        filename = path+"controlPlots.root"
        fOut = ROOT.TFile.Open(filename,'update')
        if(not fOut.FindKey(self.outputdirectory)):
            adir = ROOT.gDirectory.mkdir(self.outputdirectory)
        fOut.cd(self.outputdirectory)
        adir = fOut.GetDirectory(self.outputdirectory)
        adir.Delete("truth_vs_meanReco_pt;1")
        adir.Delete("truth_vs_rmsReco_pt;1")
        for plot in plots:
            plot.Write()
        fOut.Close()
        
    def getAllCuts(self):
        for method in self.methods:
            cutPlots, controlPlots = self.getPlots(method)
            self.writeCutFile(method,
                              cutPlots)
            self.writeControlFile(controlPlots)
            

def main(argv):

    parser = argparse.ArgumentParser(description = 'script to determine cuts')
    parser.add_argument('filelist', nargs='+', help = 'input files (efficiency optimisation output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    args = parser.parse_args()

    global path
    path = args.outdir
    mkdir(path)
    cutCalculator = CutCalculator(args.filelist)
    cutCalculator.getAllCuts()
    
if __name__ == '__main__':
    main(sys.argv[1:])
