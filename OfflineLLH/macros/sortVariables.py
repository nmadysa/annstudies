import argparse
import sys
import re

def saveVariables(dictionary):
    for algo in dictionary.keys():
        dictionary[algo].sort()
        f = open('subinput/'+algo+'_variables.txt','w')
        for variable in dictionary[algo]:
            print >> f, variable
        f.close()

def sortVariables(dictionary):
    tau = open('subinput/subvars.txt')
    for line in tau.readlines():        
        s = eval(line)[0]
        line = line.rstrip('\n')
        if re.search('([pP]i[0(_0)])',s):
            dictionary['pi0'].append(line)

        if re.search('([pP]an[tT]au)',s):
            dictionary['pantau'].append(line)
            if re.search('([cC][eE][lL][lL])',s):
                dictionary['pantau_cell'].append(line)
            if re.search('([eE][fF][lL][oO][wW])',s):
                dictionary['pantau_eflow'].append(line)

        if re.search('([bB][oO][nN][nN])',s):
            dictionary['bonn'].append(line)

        if re.search('([cC][lL][uU][sS][tT][eE][rR])',s):
            dictionary['cluster'].append(line)

        if re.search('([cC][eE][lL][lL])',s):
            dictionary['cell'].append(line)

        if re.search('([eE][fF][lL][oO][wW])',s):
            dictionary['eflow'].append(line)
            
    tau.close()

    return dictionary

def main(argv):
    parser = argparse.ArgumentParser(description = 'script to sort substructure variables')
    args = parser.parse_args()

    sub_dict = {'pi0':[],
                'pantau':[],
                'bonn':[],
                'cluster':[],
                'cell':[],
                'eflow':[],
                'pantau_cell':[],
                'pantau_eflow':[]} 
    sub_dict = sortVariables(sub_dict)
    saveVariables(sub_dict)
    
if __name__ == '__main__':
    main(sys.argv[1:])
