import sys,os
import argparse
import ROOT

def prepareTree(inputtree, prong, kind):
    cut = "tau_numTrack == "+str(prong)
    if kind == "train":
        cut += " && EventNumber%2 == 0"
    else:
        cut += " && EventNumber%2 == 1"
    inputtree.Draw(">>temp",cut)
    temp = ROOT.gDirectory.Get("temp")
    inputtree.SetEventList(temp)
    #tmp = inputtree.CopyTree(cut)
    inputtree.SetName(kind+str(prong)+"p")
    return inputtree

def writeTree(inputtree,g,treename):
    name = g.split('/')[-1].split('.')[-4]
    name += "_"+inputtree.GetName()
    inputtree.SetName(name)
    print "saving tree '%s' from file '%s' with new name '%s'" % (treename,g,inputtree.GetName())
    inputtree.Write()

def main(argv):
    parser = argparse.ArgumentParser(description='collect TTrees from different files')
    parser.add_argument('tree',nargs=1,type=str,help="treename")
    parser.add_argument('output',nargs=1,type=str,help="name of the output file")
    parser.add_argument('filelist',nargs='+',type=str,help="filelist")
    args = parser.parse_args()

    # create output file
    outfilename = args.output[0]
    treename = args.tree[0]
    print 'using outputfile %s' % outfilename 
    outputfile = ROOT.TFile.Open(outfilename,'RECREATE')
    #outputfile.cd()
    #curdir = outputfile.GetDirectory("")
    if not outputfile:
        return

    for fName in args.filelist:
        # open root file
        f = ROOT.TFile.Open(fName)
        f.cd()
        if f:
            t = f.Get(treename)
            # check tree
            if t:
                copytrain1p = t.Clone("copytrain1P")
                #copytrain1p.SetDirectory(0)
                copytrain3p = t.Clone("copytest1P")
                copytest1p = t.Clone("copytrain3P")
                copytest3p = t.Clone("copytest3P")
                outputfile.cd()
                train1p = prepareTree(copytrain1p,1,"train").CopyTree("")
                #train1p.SetDirectory(0)
                train3p = prepareTree(copytrain3p,3,"train").CopyTree("")
                test1p = prepareTree(copytest1p,1,"test").CopyTree("")
                test3p = prepareTree(copytest3p,3,"test").CopyTree("")

                #print train1p.GetName()
                #print train1p.GetEntries()
                #print test1p.GetEntries()
                #print train3p.GetEntries()
                #print test3p.GetEntries()
                

                writeTree(train1p,fName,treename)
                writeTree(train3p,fName,treename)
                writeTree(test1p,fName,treename)
                writeTree(test3p,fName,treename)
                #copytrain1p.Delete()
        else:
            print "Couldn't open file %s" % fName
        print "closing f"
        #print f.IsOpen()
        #f.cd()
        #f.Close()
        print "closed f"
    print 'finish'
    # close output
    #outputfile.Write()
    outputfile.Close()
    print 'closed'
    
if __name__ == "__main__":
    main(sys.argv[1:])
