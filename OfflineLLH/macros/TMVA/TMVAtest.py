import ROOT
import argparse
from ShellUtils import *
import sys
sys.path.append('../../out/weights/current_result/inverse/')
from weights_1prong import *
from weights_3prong import *
ROOT.PyConfig.IgnoreCommandLineOptions = True

ReadDataFromAsciiIFormat = ROOT.kFALSE

samples = ["Ztautau", "ZPrime250", "ZPrime500", "ZPrime750", "ZPrime1000", "ZPrime1250", 
           "PeriodA", "PeriodB", "PeriodI"]

variables = [("pantau_CellBased_RCal04", "F"),
             ("pantau_CellBased_FTrk02", "F"),
             ("pantau_CellBased_pi0_vistau_m", "F"),
             ("pantau_CellBased_ptRatio", "F"),
             ("pantau_CellBased_TrkAvgDist", "F"),
             ("pantau_CellBased_pi0_n", "I")]

variables1P = [("pantau_CellBased_IpSigLeadTrk", "F"),
               ("pantau_CellBased_NTracksdrdR", "I")]

variables3P = [("pantau_CellBased_MassTrkSys", "F"),
               ("pantau_CellBased_DrMax", "F"),
               ("pantau_CellBased_TrFlightPathSig", "F")]
# ("pantau_CellBased_CentFrac0102", "F"),
  

def help():
  print "ERROR: too few arguments"
  print "default usage: TMVAAnalysis(input file, output file, number of prongs [, etaRegion] [, ptRegion])"
  print "possible values:"
  print "\t for input file: root file"
  print "\t for output file: root file" 
  print "\t for number of prongs: 1 or 3"
  print "\t for eta region: \"barrel\" (abs(eta) < 1.5) or \"endcap\" (abs(eta) > 1.5), default: total eta region" 
  print "\t for pt range: \"min\" (pt < 40 GeV) or \"med\" (40 GeV < pt < 70 GeV) or \"max\" (pt > 70 GeV), default: total pt range" 


def TMVAAnalysis(inFName,
                 outFName,
                 nProng,
                 variable = "",
                 etaRegion = "",
                 ptRange = ""):
    ROOT.TMVA.Tools.Instance()

    Use = {}
    
    Use["BDT"] = 1
    Use["BDTp"] = 0
    Use["Likelihood"] = 0
    Use["LikelihoodD"] = 0
    print
    print "==> Start TMVAClassification"

    if not ROOT.gSystem.AccessPathName(inFName):
        input = ROOT.TFile.Open(inFName)
    assert(input != 0);
    
    print "--- TMVAClassification       : Using input file: " + input.GetName()

    for sample in samples:
        exec('%s_train1p = input.Get("%s_train1p")' % (sample, sample))
        exec('%s_train3p = input.Get("%s_train3p")' % (sample, sample))
        exec('%s_test1p = input.Get("%s_test1p")' % (sample, sample))
        exec('%s_test3p = input.Get("%s_test3p")' % (sample, sample))

    # Create a new root output file.
    outputFile = ROOT.TFile.Open(outFName, "RECREATE")
    factory = ROOT.TMVA.Factory("TMVAClassification", outputFile,
                                "!V:!Silent:Color:DrawProgressBar")

    for variable in variables:
      eval('factory.AddVariable("%s", "%s")' % (variable[0], variable[1]))
    if ( nProng == 1 ):
      for variable in variables1P:
        eval('factory.AddVariable("%s", "%s")' % (variable[0], variable[1]))
    else:
      for variable in variables3P:
        eval('factory.AddVariable("%s", "%s")' % (variable[0], variable[1]))

    #global event weights per tree (see below for setting event-wise weights)
  
    dataWeight = 1.0
    signalWeight = 1.0

    factory.AddSignalTree(Ztautau_train1p, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime250_train1p, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime500_train1p, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime750_train1p, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime1000_train1p, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime1250_train1p, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddBackgroundTree(PeriodA_train1p, dataWeight, ROOT.TMVA.Types.kTraining)
    factory.AddBackgroundTree(PeriodB_train1p, dataWeight, ROOT.TMVA.Types.kTraining)
    factory.AddBackgroundTree(PeriodI_train1p, dataWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(Ztautau_test1p, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime250_test1p, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime500_test1p, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime750_test1p, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime1000_test1p, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime1250_test1p, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddBackgroundTree(PeriodA_test1p, dataWeight, ROOT.TMVA.Types.kTesting)
    factory.AddBackgroundTree(PeriodB_test1p, dataWeight, ROOT.TMVA.Types.kTesting)
    factory.AddBackgroundTree(PeriodI_test1p, dataWeight, ROOT.TMVA.Types.kTesting)
  
    # inverse weights
    # eval('factory.SetSignalWeightExpression( GetWeights_BackgroundMu_%ip() )' % nProng)
    # eval('factory.SetBackgroundWeightExpression( GetWeights_SignalPt_%ip() )' % nProng)
    # correct weights
    # eval('factory.SetSignalWeightExpression( GetWeights_SignalPt_%ip() )' % nProng)
    # eval('factory.SetBackgroundWeightExpression( GetWeights_BackgroundMu_%ip() )' % nProng)

    # nSignal = (Ztautau.GetEntries(preselectionCut) + ZPrime250.GetEntries(preselectionCut) + ZPrime500.GetEntries(preselectionCut) + ZPrime750.GetEntries(preselectionCut) + ZPrime1000.GetEntries(preselectionCut) + ZPrime1250.GetEntries(preselectionCut)) / 2
    # nBackground = (PeriodA.GetEntries(preselectionCut) + PeriodB.GetEntries(preselectionCut) + PeriodI.GetEntries(preselectionCut)) / 2
    # nEvents = min(nSignal,nBackground)
    # print nEvents

    # tell the factory to use all remaining events in the trees after training for testing:
    # factory.PrepareTrainingAndTestTree(ROOT.TCut(preselectionCut),"")
    factory.PrepareTrainingAndTestTree(ROOT.TCut(""),"NormMode=EqualNumEvents:SplitMode=Block")
    # ROOT.TCut(preselectionCut),
                                        #choose NormMode=EqualNumEvents for same bkr and sig number!
    
    # factory.PrepareTrainingAndTestTree(ROOT.TCut(preselectionCut),
    #                                    "nTrain_Signal="+str(nEvents)+":nTrain_Background="+str(nEvents)+":nTest_Signal="+str(nEvents)+":nTest_Background="+str(nEvents)+":NormMode=NumEvents"); 
  
    if (Use["BDT"]): # Adaptive Boost
        # factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDT001",
        #             "!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=NoPruning:DoBoostMonitor:MinNodeSize=0.01")
        factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDT01",
                    "!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=NoPruning:DoBoostMonitor:MinNodeSize=0.1")

    # factory.BookMethod(ROOT.TMVA.Types.kBDT, "alexeys_BDT",
    #"!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=NoPruning:DoBoostMonitor")

    #factory.BookMethod(ROOT.TMVA.Types.kBDT, "pedros_BDT",
    #"!H:V:NTrees=70:MaxDepth=100000:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=500:PruneMethod=CostComplexity:PruneStrength=60:PruningValFraction=0.5:DoBoostMonitor")

    """
    if (Use["BDT"]): // Adaptive Boost
        factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDT",
   			"!H:!V:NTrees=70:MaxDepth=100000:nEventsMin=DEFAULT:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=500:PruneMethod=NoPruning")
    """
    
    #BDT with pruning
    if (Use["BDTp"]): # Adaptive Boost
        factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDTp001",
                "!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=CostComplexity:PruneStrength=60:PruningValFraction=0.5:DoBoostMonitor:MinNodeSize=0.01")
        factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDTp01",
                "!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=CostComplexity:PruneStrength=60:PruningValFraction=0.5:DoBoostMonitor:MinNodeSize=0.1")

    #Likelihood
    if (Use["Likelihood"]):
        factory.BookMethod(ROOT.TMVA.Types.kLikelihood, "Likelihood",
                "!H:!V:PDFInterpol=Spline2:NSmooth=1")
    # Likelihood with decorrelation
    if (Use["LikelihoodD"]):
        factory.BookMethod(ROOT.TMVA.Types.kLikelihood, "LikelihoodD",
                "!H:!V:PDFInterpol=Spline2:NSmooth=1:VarTransform=Decorrelate")

    # Train MVAs using the set of training events
    factory.TrainAllMethods()

    #---- Evaluate all MVAs using the set of test events
    factory.TestAllMethods()

    #----- Evaluate and compare performance of all configured MVAs
    factory.EvaluateAllMethods()

    #--------------------------------------------------------------
    #Save the output
    outputFile.Close()

    print "==> Wrote root file: " + outputFile.GetName()
    print "==> TMVAClassification is done!" 

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'TMVA training script')
    parser.add_argument('--input', '-i', required = True, help = 'input file')
    parser.add_argument('--output', '-o', required = True, help = 'output directory')
    parser.add_argument('--nprong', '-n', required = True, type = int, help = 'prongness')
    parser.add_argument('--postfix', '-pf', default = "", help = 'postfix')
    args = parser.parse_args()

    outfile = os.path.join(args.output, 'TMVAOutput_%ip%s.root' % (args.nprong,args.postfix))
      
    TMVAAnalysis(args.input, outfile, args.nprong)

    move('weights',
         os.path.join(args.output,'weights_%ip%s' % (args.nprong,args.postfix)))

