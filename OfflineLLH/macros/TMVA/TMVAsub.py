import ROOT
import argparse
from ShellUtils import *
import sys
sys.path.append('../../out/weights/current_result/')
from weights_1prong import *
from weights_3prong import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
from varset_all import *
from samples_all import *
ReadDataFromAsciiIFormat = ROOT.kFALSE

def help():
  print "ERROR: too few arguments"
  print "default usage: TMVAAnalysis(input file, output file, number of prongs [, etaRegion] [, ptRegion])"
  print "possible values:"
  print "\t for input file: root file"
  print "\t for output file: root file" 
  print "\t for number of prongs: 1 or 3"
  print "\t for eta region: \"barrel\" (abs(eta) < 1.5) or \"endcap\" (abs(eta) > 1.5), default: total eta region" 
  print "\t for pt range: \"min\" (pt < 40 GeV) or \"med\" (40 GeV < pt < 70 GeV) or \"max\" (pt > 70 GeV), default: total pt range" 


def TMVAAnalysis(inFName,
                 outFName,
                 nProng,
                 variable = "",
                 etaRegion = "",
                 ptRange = ""):
    ROOT.TMVA.Tools.Instance()

    Use = {}
    
    Use["BDT"] = 1
    Use["BDTp"] = 0
    print
    print "==> Start TMVAClassification"

    if not ROOT.gSystem.AccessPathName(inFName):
        input = ROOT.TFile.Open(inFName)
    assert(input != 0);
    
    print "--- TMVAClassification       : Using input file: " + input.GetName()

    for sample in samples:
        exec('%s_train = input.Get("%s")' % (sample, sample + "_train" + str(nProng) + "p"))
        exec('%s_test = input.Get("%s")' % (sample, sample + "_test" + str(nProng) + "p"))

    # Create a new root output file.
    outputFile = ROOT.TFile.Open(outFName, "RECREATE")
    factory = ROOT.TMVA.Factory("TMVAClassification", outputFile,
                                "!V:!Silent:Color:DrawProgressBar")

    for variable in variables:
      eval('factory.AddVariable("%s", "%s")' % (variable[0], variable[1]))
    if ( nProng == 1 ):
      for variable in variables1P:
        eval('factory.AddVariable("%s", "%s")' % (variable[0], variable[1]))
    else:
      for variable in variables3P:
        eval('factory.AddVariable("%s", "%s")' % (variable[0], variable[1]))

    #global event weights per tree (see below for setting event-wise weights)
    dataWeight = 1.0
    signalWeight = 1.0

    factory.AddSignalTree(Ztautau_train, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime250_train, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime500_train, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime750_train, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime1000_train, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddSignalTree(ZPrime1250_train, signalWeight, ROOT.TMVA.Types.kTraining)
    factory.AddBackgroundTree(PeriodA_train, dataWeight, ROOT.TMVA.Types.kTraining)
    factory.AddBackgroundTree(PeriodB_train, dataWeight, ROOT.TMVA.Types.kTraining)
    factory.AddBackgroundTree(PeriodI_train, dataWeight, ROOT.TMVA.Types.kTraining)

    factory.AddSignalTree(Ztautau_test, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime250_test, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime500_test, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime750_test, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime1000_test, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddSignalTree(ZPrime1250_test, signalWeight, ROOT.TMVA.Types.kTesting)
    factory.AddBackgroundTree(PeriodA_test, dataWeight, ROOT.TMVA.Types.kTesting)
    factory.AddBackgroundTree(PeriodB_test, dataWeight, ROOT.TMVA.Types.kTesting)
    factory.AddBackgroundTree(PeriodI_test, dataWeight, ROOT.TMVA.Types.kTesting)

    #Apply additional cuts on the signal and background samples (can be different)
    cut_eta = ""
    cut_pt = ""
    
    if ( etaRegion == "barrel" ):
        cut_eta = "abs(tau_eta) < 1.5"
    elif ( etaRegion == "endcap" ):
        cut_eta = "abs(tau_eta) > 1.5"
    else:
        cut_eta = "abs(tau_eta) < 2.5" 

    if ( ptRange == "min" ):
        cut_pt = "tau_Et < 40000" 
    elif ( ptRange  == "med" ):
        cut_pt = "tau_Et > 40000 && tau_Et < 70000" 
    elif ( ptRange == "max" ):
        cut_pt = "tau_Et > 70000"
    else:
        cut_pt = "tau_Et > 0"
    

    preselectionCut = ""
    # if ( nProng == 1 ):
    #     preselectionCut = "tau_numTrack == 1" + " && " + cut_eta + " && " + cut_pt
    # else:
    #     preselectionCut = "tau_numTrack == 3" + " && " + cut_eta + " && " + cut_pt

    eval('factory.SetSignalWeightExpression( GetWeights_SignalMu_%ip() )' % nProng)
    eval('factory.SetBackgroundWeightExpression( GetWeights_BackgroundPt_%ip() )' % nProng)
    
    factory.PrepareTrainingAndTestTree(ROOT.TCut(preselectionCut),
                                       "NormMode=EqualNumEvents:SplitMode=Block") #choose NormMode=EqualNumEvents for same bkr and sig number!
      
    if (Use["BDT"]): # Adaptive Boost
        factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDT",
                    "!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=NoPruning:DoBoostMonitor:MinNodeSize=0.1")
    # factory.BookMethod(ROOT.TMVA.Types.kBDT, "alexeys_BDT",
    #"!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=NoPruning:DoBoostMonitor")

    #factory.BookMethod(ROOT.TMVA.Types.kBDT, "pedros_BDT",
    #"!H:V:NTrees=70:MaxDepth=100000:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=500:PruneMethod=CostComplexity:PruneStrength=60:PruningValFraction=0.5:DoBoostMonitor")
    
    #BDT with pruning
    if (Use["BDTp"]): # Adaptive Boost
        factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDTp01",
                "!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=CostComplexity:PruneStrength=60:PruningValFraction=0.5:DoBoostMonitor:MinNodeSize=0.1")

    # Train MVAs using the set of training events
    factory.TrainAllMethods()

    #---- Evaluate all MVAs using the set of test events
    factory.TestAllMethods()

    #----- Evaluate and compare performance of all configured MVAs
    factory.EvaluateAllMethods()

    #--------------------------------------------------------------
    #Save the output
    outputFile.Close()

    print "==> Wrote root file: " + outputFile.GetName()
    print "==> TMVAClassification is done!" 

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'TMVA training script')
    parser.add_argument('--input', '-i', required = True, help = 'input file')
    parser.add_argument('--output', '-o', required = True, help = 'output directory')
    parser.add_argument('--nprong', '-n', required = True, type = int, help = 'prongness')
    parser.add_argument('--postfix', '-pf', default = "", help = 'postfix')
    args = parser.parse_args()

    outfile = os.path.join(args.output, 'TMVAOutput_%ip%s.root' % (args.nprong,args.postfix))
      
    TMVAAnalysis(args.input, outfile, args.nprong)

    move('weights',
         os.path.join(args.output,'weights_%ip%s' % (args.nprong,args.postfix)))

