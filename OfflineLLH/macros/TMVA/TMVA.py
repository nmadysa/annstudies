import ROOT
import argparse
from ShellUtils import *
import sys
sys.path.append('../../out/weights/current_result/')
from weights_1prong import *
from weights_3prong import *
ROOT.PyConfig.IgnoreCommandLineOptions = True

ReadDataFromAsciiIFormat = ROOT.kFALSE

samples = ["Ztautau", "ZPrime250", "ZPrime500", "ZPrime750", "ZPrime1000", "ZPrime1250", "PeriodA", "PeriodB", "PeriodI"]

variables = [("tau_calcVars_corrCentFrac", "F"),
             # ("tau_calcVars_corrFTrk", "F"),
             ("tau_pi0_vistau_m", "F"),
             ("tau_ptRatio", "F"),
             ("pantauFeature_CellBased_Neutral_Ratio_EtOverEtAllConsts", "F"),
             ("pantauFeature_CellBased_Charged_DeltaR_MaxToJetAxis_EtSort", "F"),
             # ("pantauFeature_CellBased_Charged_JetMoment_EtDR", "F"),
             ("pantauFeature_CellBased_Basic_NOuterNeutConsts", "F")
             # ("tau_seedCalo_trkAvgDist", "F"),
             # ("tau_pi0_n", "I"),
             # ("tau_calcVars_ChPiEMEOverCaloEME", "F"),
             # ("tau_calcVars_EMPOverTrkSysP", "F")
             ]

variables1P = [("tau_ipSigLeadTrk", "F"),
               ("tau_calcVars_corrFTrk", "F"),
               ("tau_seedCalo_wideTrk_n", "I"),
               ("pantauFeature_CellBased_Charged_Ratio_EtOverEtNeutLowB", "F")
               ]

variables3P = [("tau_massTrkSys", "F"),
               # ("tau_seedCalo_dRmax", "F"),
               ("tau_trFlightPathSig", "F"),
               ("pantauFeature_CellBased_Charged_Mean_Et_WrtEtNeutLowB", "F"),
               ("pantauFeature_CellBased_Charged_DeltaR_1stTo2nd_EtSort", "F")]

def help():
  print "ERROR: too few arguments"
  print "default usage: TMVAAnalysis(input file, output file, number of prongs [, etaRegion] [, ptRegion])"
  print "possible values:"
  print "\t for input file: root file"
  print "\t for output file: root file" 
  print "\t for number of prongs: 1 or 3"
  print "\t for eta region: \"barrel\" (abs(eta) < 1.5) or \"endcap\" (abs(eta) > 1.5), default: total eta region" 
  print "\t for pt range: \"min\" (pt < 40 GeV) or \"med\" (40 GeV < pt < 70 GeV) or \"max\" (pt > 70 GeV), default: total pt range" 


def TMVAAnalysis(inFName,
                 outFName,
                 nProng,
                 variable = "",
                 etaRegion = "",
                 ptRange = ""):
    ROOT.TMVA.Tools.Instance()

    Use = {}
    
    Use["BDT"] = 0
    Use["BDTp"] = 1
    Use["Likelihood"] = 0
    Use["LikelihoodD"] = 0
    print
    print "==> Start TMVAClassification"

    if not ROOT.gSystem.AccessPathName(inFName):
        input = ROOT.TFile.Open(inFName)
    assert(input != 0);
    
    print "--- TMVAClassification       : Using input file: " + input.GetName()

    for sample in samples:
        exec('%s = input.Get("%s")' % (sample, sample))

    # Create a new root output file.
    outputFile = ROOT.TFile.Open(outFName, "RECREATE")
    factory = ROOT.TMVA.Factory("TMVAClassification", outputFile,
                                "!V:!Silent:Color:DrawProgressBar")

    for variable in variables:
      eval('factory.AddVariable("%s", "%s")' % (variable[0], variable[1]))
    if ( nProng == 1 ):
      for variable in variables1P:
        eval('factory.AddVariable("%s", "%s")' % (variable[0], variable[1]))
    else:
      for variable in variables3P:
        eval('factory.AddVariable("%s", "%s")' % (variable[0], variable[1]))

    #global event weights per tree (see below for setting event-wise weights)
  
    dataWeight = 1.0
    signalWeight = 1.0

    factory.AddSignalTree(Ztautau, signalWeight)
    factory.AddSignalTree(ZPrime250, signalWeight)
    factory.AddSignalTree(ZPrime500, signalWeight)
    factory.AddSignalTree(ZPrime750, signalWeight)
    factory.AddSignalTree(ZPrime1000, signalWeight)
    factory.AddSignalTree(ZPrime1250, signalWeight)
    factory.AddBackgroundTree(PeriodA, dataWeight)
    factory.AddBackgroundTree(PeriodB, dataWeight)
    factory.AddBackgroundTree(PeriodI, dataWeight)
  
    #Apply additional cuts on the signal and background samples (can be different)
    cut_trFlightPathSig = "tau_trFlightPathSig > -1000"
    cut_eta = ""
    cut_pt = ""
    cut_ChPiEMEOverCaloEME = "tau_calcVars_ChPiEMEOverCaloEME >= -10 && tau_calcVars_ChPiEMEOverCaloEME <= 10"
    cut_EMPOverTrkSysP = "tau_calcVars_EMPOverTrkSysP >= 0 && tau_calcVars_EMPOverTrkSysP <= 50"
    cut_Neutral_Ratio_EtOverEtAllConsts = "pantauFeature_CellBased_Neutral_Ratio_EtOverEtAllConsts > 0"
    
    if ( etaRegion == "barrel" ):
        cut_eta = "abs(tau_eta) < 1.5"
    elif ( etaRegion == "endcap" ):
        cut_eta = "abs(tau_eta) > 1.5"
    else:
        cut_eta = "abs(tau_eta) < 2.5" 

    if ( ptRange == "min" ):
        cut_pt = "tau_Et < 40000" 
    elif ( ptRange  == "med" ):
        cut_pt = "tau_Et > 40000 && tau_Et < 70000" 
    elif ( ptRange == "max" ):
        cut_pt = "tau_Et > 70000"
    else:
        cut_pt = "tau_Et > 0"
    
    if ( nProng == 1 ):
        preselectionCut = "tau_numTrack == 1" + " && " + cut_eta + " && " + cut_pt
        factory.SetSignalWeightExpression( GetWeights_SignalPt_1p() )
        factory.SetBackgroundWeightExpression( GetWeights_BackgroundMu_1p() ) 
    else:
        preselectionCut = "tau_numTrack == 3" + " && " + cut_eta + " && " + cut_pt + " && " + cut_trFlightPathSig 
        factory.SetSignalWeightExpression( GetWeights_SignalPt_3p() ) 
        factory.SetBackgroundWeightExpression( GetWeights_BackgroundMu_3p() ) 

    nSignal = (Ztautau.GetEntries(preselectionCut) + ZPrime250.GetEntries(preselectionCut) + ZPrime500.GetEntries(preselectionCut) + ZPrime750.GetEntries(preselectionCut) + ZPrime1000.GetEntries(preselectionCut) + ZPrime1250.GetEntries(preselectionCut)) / 2
    nBackground = (PeriodA.GetEntries(preselectionCut) + PeriodB.GetEntries(preselectionCut) + PeriodI.GetEntries(preselectionCut)) / 2
    nEvents = min(nSignal,nBackground)
    print nEvents
    
    # tell the factory to use all remaining events in the trees after training for testing:
    # factory.PrepareTrainingAndTestTree(ROOT.TCut(preselectionCut),"")
    # factory.PrepareTrainingAndTestTree(ROOT.TCut(preselectionCut),
    #                                    "NormMode=EqualNumEvents:SplitMode=Block") #choose NormMode=EqualNumEvents for same bkr and sig number!
    
    if ( nProng == 1 ):
      factory.PrepareTrainingAndTestTree(ROOT.TCut(preselectionCut),
                                         "nTrain_Signal="+str(nEvents)+":nTrain_Background="+str(nEvents)+":nTest_Signal="+str(nEvents)+":nTest_Background="+str(nEvents)+":NormMode=NumEvents"); 
    else:
      factory.PrepareTrainingAndTestTree(ROOT.TCut(preselectionCut),
                                         "nTrain_Signal="+str(nEvents)+":nTrain_Background="+str(nEvents)+":nTest_Signal="+str(nEvents)+":nTest_Background="+str(nEvents)+":NormMode=NumEvents"); 
  
    if (Use["BDT"]): # Adaptive Boost
        factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDT",
                    "!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=NoPruning:DoBoostMonitor:MinNodeSize=0.01")

    # factory.BookMethod(ROOT.TMVA.Types.kBDT, "alexeys_BDT",
    #"!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=NoPruning:DoBoostMonitor")

    #factory.BookMethod(ROOT.TMVA.Types.kBDT, "pedros_BDT",
    #"!H:V:NTrees=70:MaxDepth=100000:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=500:PruneMethod=CostComplexity:PruneStrength=60:PruningValFraction=0.5:DoBoostMonitor")

    """
    if (Use["BDT"]): // Adaptive Boost
        factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDT",
   			"!H:!V:NTrees=70:MaxDepth=100000:nEventsMin=DEFAULT:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=500:PruneMethod=NoPruning")
    """
    
    #BDT with pruning
    if (Use["BDTp"]): # Adaptive Boost
        factory.BookMethod(ROOT.TMVA.Types.kBDT, "BDTp",
                "!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=CostComplexity:PruneStrength=60:PruningValFraction=0.5:DoBoostMonitor:MinNodeSize=0.01")

    #Likelihood
    if (Use["Likelihood"]):
        factory.BookMethod(ROOT.TMVA.Types.kLikelihood, "Likelihood",
                "!H:!V:PDFInterpol=Spline2:NSmooth=1")
    # Likelihood with decorrelation
    if (Use["LikelihoodD"]):
        factory.BookMethod(ROOT.TMVA.Types.kLikelihood, "LikelihoodD",
                "!H:!V:PDFInterpol=Spline2:NSmooth=1:VarTransform=Decorrelate")

    # Train MVAs using the set of training events
    factory.TrainAllMethods()

    #---- Evaluate all MVAs using the set of test events
    factory.TestAllMethods()

    #----- Evaluate and compare performance of all configured MVAs
    factory.EvaluateAllMethods()

    #--------------------------------------------------------------
    #Save the output
    outputFile.Close()

    print "==> Wrote root file: " + outputFile.GetName()
    print "==> TMVAClassification is done!" 

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'TMVA training script')
    parser.add_argument('--input', '-i', required = True, help = 'input file')
    parser.add_argument('--output', '-o', required = True, help = 'output directory')
    parser.add_argument('--nprong', '-n', required = True, type = int, help = 'prongness')
    parser.add_argument('--postfix', '-pf', default = "", help = 'postfix')
    args = parser.parse_args()

    outfile = os.path.join(args.output, 'TMVAOutput_%ip%s.root' % (args.nprong,args.postfix))
      
    TMVAAnalysis(args.input, outfile, args.nprong)

    move('weights',
         os.path.join(args.output,'weights_%ip' % args.nprong))

