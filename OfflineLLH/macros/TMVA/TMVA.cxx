#include <cstdlib>
#include <iostream> 
#include <map>
#include <string>
#include <assert.h>
#include <vector>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TPluginManager.h"
#include "TH1I.h"
//#include "tmva/TMVAGui.C"

#if not defined(__CINT__) || defined(__MAKECINT__)
// needs to be included when makecint runs (ACLIC)
#include "TMVA/Factory.h"
#include "TMVA/Config.h"
#include "TMVA/Tools.h"
#endif

Bool_t ReadDataFromAsciiIFormat = kFALSE;
TString GetWeights_SignalPt_1p() 
 {return "0.000000 * (tau_Et >= 0.000000 && tau_Et< 5000.000000) + 0.000000 * (tau_Et >= 5000.000000 && tau_Et< 10000.000000) + 0.000000 * (tau_Et >= 10000.000000 && tau_Et< 15000.000000) + 3.025328 * (tau_Et >= 15000.000000 && tau_Et< 20000.000000) + 1.086177 * (tau_Et >= 20000.000000 && tau_Et< 25000.000000) + 0.574502 * (tau_Et >= 25000.000000 && tau_Et< 30000.000000) + 0.389418 * (tau_Et >= 30000.000000 && tau_Et< 35000.000000) + 0.320420 * (tau_Et >= 35000.000000 && tau_Et< 40000.000000) + 0.318680 * (tau_Et >= 40000.000000 && tau_Et< 45000.000000) + 0.367897 * (tau_Et >= 45000.000000 && tau_Et< 50000.000000) + 0.447950 * (tau_Et >= 50000.000000 && tau_Et< 55000.000000) + 0.523634 * (tau_Et >= 55000.000000 && tau_Et< 60000.000000) + 0.584776 * (tau_Et >= 60000.000000 && tau_Et< 65000.000000) + 0.611812 * (tau_Et >= 65000.000000 && tau_Et< 70000.000000) + 0.615437 * (tau_Et >= 70000.000000 && tau_Et< 75000.000000) + 0.593680 * (tau_Et >= 75000.000000 && tau_Et< 80000.000000) + 0.568572 * (tau_Et >= 80000.000000 && tau_Et< 85000.000000) + 0.549646 * (tau_Et >= 85000.000000 && tau_Et< 90000.000000) + 0.531668 * (tau_Et >= 90000.000000 && tau_Et< 95000.000000) + 0.473971 * (tau_Et >= 95000.000000 && tau_Et< 150000.000000) + 0.824014 * (tau_Et >= 150000.000000 && tau_Et< 1000000.000000)";}

TString GetWeights_BackgroundMu_1p() 
 {return "158.112579 * (mu >= 0.000000 && mu< 6.000000) + 13.735970 * (mu >= 6.000000 && mu< 7.000000) + 3.267476 * (mu >= 7.000000 && mu< 8.000000) + 1.596780 * (mu >= 8.000000 && mu< 9.000000) + 1.137690 * (mu >= 9.000000 && mu< 10.000000) + 0.990311 * (mu >= 10.000000 && mu< 11.000000) + 0.731653 * (mu >= 11.000000 && mu< 12.000000) + 0.574593 * (mu >= 12.000000 && mu< 13.000000) + 0.613100 * (mu >= 13.000000 && mu< 14.000000) + 0.640584 * (mu >= 14.000000 && mu< 15.000000) + 0.686992 * (mu >= 15.000000 && mu< 16.000000) + 0.717690 * (mu >= 16.000000 && mu< 17.000000) + 0.750206 * (mu >= 17.000000 && mu< 18.000000) + 0.745180 * (mu >= 18.000000 && mu< 19.000000) + 0.761055 * (mu >= 19.000000 && mu< 20.000000) + 0.781124 * (mu >= 20.000000 && mu< 21.000000) + 0.762033 * (mu >= 21.000000 && mu< 22.000000) + 0.803536 * (mu >= 22.000000 && mu< 23.000000) + 0.778408 * (mu >= 23.000000 && mu< 24.000000) + 0.842967 * (mu >= 24.000000 && mu< 25.000000) + 0.852185 * (mu >= 25.000000 && mu< 26.000000) + 0.932903 * (mu >= 26.000000 && mu< 27.000000) + 1.031577 * (mu >= 27.000000 && mu< 28.000000) + 1.204079 * (mu >= 28.000000 && mu< 29.000000) + 1.447859 * (mu >= 29.000000 && mu< 30.000000) + 1.816317 * (mu >= 30.000000 && mu< 31.000000) + 2.100260 * (mu >= 31.000000 && mu< 32.000000) + 2.440586 * (mu >= 32.000000 && mu< 33.000000) + 4.048466 * (mu >= 33.000000 && mu< 34.000000) + 9.878844 * (mu >= 34.000000 && mu< 35.000000) + 46.427437 * (mu >= 35.000000 && mu< 36.000000) + 0.000000 * (mu >= 36.000000 && mu< 37.000000) + 0.000000 * (mu >= 37.000000 && mu< 38.000000) + 0.000000 * (mu >= 38.000000 && mu< 39.000000)";}

TString GetWeights_SignalPt_3p() 
 {return "0.000000 * (tau_Et >= 0.000000 && tau_Et< 5000.000000) + 0.000000 * (tau_Et >= 5000.000000 && tau_Et< 10000.000000) + 0.000000 * (tau_Et >= 10000.000000 && tau_Et< 15000.000000) + 2.125913 * (tau_Et >= 15000.000000 && tau_Et< 20000.000000) + 1.121902 * (tau_Et >= 20000.000000 && tau_Et< 25000.000000) + 0.715321 * (tau_Et >= 25000.000000 && tau_Et< 30000.000000) + 0.538467 * (tau_Et >= 30000.000000 && tau_Et< 35000.000000) + 0.477640 * (tau_Et >= 35000.000000 && tau_Et< 40000.000000) + 0.507856 * (tau_Et >= 40000.000000 && tau_Et< 45000.000000) + 0.632984 * (tau_Et >= 45000.000000 && tau_Et< 50000.000000) + 0.846075 * (tau_Et >= 50000.000000 && tau_Et< 55000.000000) + 1.079901 * (tau_Et >= 55000.000000 && tau_Et< 60000.000000) + 1.275758 * (tau_Et >= 60000.000000 && tau_Et< 65000.000000) + 1.426193 * (tau_Et >= 65000.000000 && tau_Et< 70000.000000) + 1.538706 * (tau_Et >= 70000.000000 && tau_Et< 75000.000000) + 1.567804 * (tau_Et >= 75000.000000 && tau_Et< 80000.000000) + 1.551290 * (tau_Et >= 80000.000000 && tau_Et< 85000.000000) + 1.489582 * (tau_Et >= 85000.000000 && tau_Et< 90000.000000) + 1.433639 * (tau_Et >= 90000.000000 && tau_Et< 95000.000000) + 1.326687 * (tau_Et >= 95000.000000 && tau_Et< 150000.000000) + 2.419488 * (tau_Et >= 150000.000000 && tau_Et< 1000000.000000)";}

TString GetWeights_BackgroundMu_3p() 
 {return "178.057190 * (mu >= 0.000000 && mu< 6.000000) + 12.098226 * (mu >= 6.000000 && mu< 7.000000) + 2.637849 * (mu >= 7.000000 && mu< 8.000000) + 1.320030 * (mu >= 8.000000 && mu< 9.000000) + 1.026758 * (mu >= 9.000000 && mu< 10.000000) + 0.820602 * (mu >= 10.000000 && mu< 11.000000) + 0.634205 * (mu >= 11.000000 && mu< 12.000000) + 0.506100 * (mu >= 12.000000 && mu< 13.000000) + 0.542140 * (mu >= 13.000000 && mu< 14.000000) + 0.577135 * (mu >= 14.000000 && mu< 15.000000) + 0.622040 * (mu >= 15.000000 && mu< 16.000000) + 0.666937 * (mu >= 16.000000 && mu< 17.000000) + 0.705561 * (mu >= 17.000000 && mu< 18.000000) + 0.741201 * (mu >= 18.000000 && mu< 19.000000) + 0.741741 * (mu >= 19.000000 && mu< 20.000000) + 0.775407 * (mu >= 20.000000 && mu< 21.000000) + 0.774989 * (mu >= 21.000000 && mu< 22.000000) + 0.822087 * (mu >= 22.000000 && mu< 23.000000) + 0.834199 * (mu >= 23.000000 && mu< 24.000000) + 0.894558 * (mu >= 24.000000 && mu< 25.000000) + 0.911393 * (mu >= 25.000000 && mu< 26.000000) + 1.033918 * (mu >= 26.000000 && mu< 27.000000) + 1.175445 * (mu >= 27.000000 && mu< 28.000000) + 1.371918 * (mu >= 28.000000 && mu< 29.000000) + 1.672682 * (mu >= 29.000000 && mu< 30.000000) + 2.147088 * (mu >= 30.000000 && mu< 31.000000) + 2.527223 * (mu >= 31.000000 && mu< 32.000000) + 3.001457 * (mu >= 32.000000 && mu< 33.000000) + 5.115017 * (mu >= 33.000000 && mu< 34.000000) + 12.688008 * (mu >= 34.000000 && mu< 35.000000) + 60.576149 * (mu >= 35.000000 && mu< 36.000000) + 0.000000 * (mu >= 36.000000 && mu< 37.000000) + 0.000000 * (mu >= 37.000000 && mu< 38.000000) + 0.000000 * (mu >= 38.000000 && mu< 39.000000)";}

void help(){
  std::cout << "ERROR: too few arguments" << std::endl;
  std::cout << "default usage: TMVAAnalysis(input file, output file, number of prongs [, etaRegion] [, ptRegion])" << std::endl;
  std::cout << "possible values:" << std::endl;
  std::cout << "\t for input file: root file" << std::endl;
  std::cout << "\t for output file: root file" << std::endl; 
  std::cout << "\t for number of prongs: 1 or 3" << std::endl;
  std::cout << "\t for eta region: \"barrel\" (abs(eta) < 1.5) or \"endcap\" (abs(eta) > 1.5), default: total eta region" << std::endl; 
  std::cout << "\t for pt range: \"min\" (pt < 40 GeV) or \"med\" (40 GeV < pt < 70 GeV) or \"max\" (pt > 70 GeV), default: total pt range" << std::endl; 
}

void TMVAAnalysis(TString inFName, TString outFName, int nProng, string variable, string etaRegion = "", string ptRange = "") {

  // if(!inFName or !outFName or !nProng){
  //     help();
  //   }

  // this loads the library
  TMVA::Tools::Instance();

  // default MVA methods to be trained + tested
  std::map<std::string, int> Use;
    
  Use["BDT"] = 1;
  Use["BDTp"] = 0;
  Use["Likelihood"] = 0;
  Use["LikelihoodD"] = 0;
  std::cout << std::endl;
  std::cout << "==> Start TMVAClassification" << std::endl;

  TFile * input(0);
  if (!gSystem->AccessPathName(inFName)) {
    input = TFile::Open(inFName);
  }
  assert(input != 0);
  std::cout << "--- TMVAClassification       : Using input file: " << input->GetName() << std::endl;

  // TTree *Wtaunu = (TTree*) input->Get("Wtaunu");
  // TTree *Ztautau = (TTree*) input->Get("Ztautau");
  // TTree *Zprime250 = (TTree*) input->Get("ZPrime250");
  // TTree *Zprime500 = (TTree*) input->Get("ZPrime500");
  // TTree *Zprime1000 = (TTree*) input->Get("ZPrime1000");
  // TTree *periodA = (TTree*) input->Get("PeriodA");
  // TTree *periodB = (TTree*) input->Get("PeriodB");
  // TTree *periodC = (TTree*) input->Get("PeriodC");
  // TTree *periodD = (TTree*) input->Get("PeriodD");
  // TTree *periodE = (TTree*) input->Get("PeriodE");
  // TTree *periodG = (TTree*) input->Get("PeriodG");
  // TTree *periodH = (TTree*) input->Get("PeriodH");
  // TTree *periodI = (TTree*) input->Get("PeriodI");
  // TTree *periodJ = (TTree*) input->Get("PeriodJ");
  // TTree *periodL = (TTree*) input->Get("PeriodL");
  TTree *mctest = (TTree*) input->Get("mctest");
  TTree *datatest = (TTree*) input->Get("datatest");

  // Create a new root output file.
  TFile* outputFile = TFile::Open(outFName, "RECREATE");
  TMVA::Factory *factory = new TMVA::Factory("TMVAClassification", outputFile,
					     "!V:!Silent:Color:DrawProgressBar");
  
  if (variable != "tau_calcVars_corrCentFrac")
    factory->AddVariable("tau_calcVars_corrCentFrac", 'F');
  if (variable != "tau_calcVars_corrFTrk")
    factory->AddVariable("tau_calcVars_corrFTrk", 'F');
  if (variable != "tau_pi0_vistau_m")
    factory->AddVariable("tau_pi0_vistau_m", 'F');
  if (variable != "tau_ptRatio")
    factory->AddVariable("tau_ptRatio", 'F');
  if (variable != "tau_seedCalo_trkAvgDist")
    factory->AddVariable("tau_seedCalo_trkAvgDist", 'F');
  if (variable != "tau_pi0_n")
    factory->AddVariable("tau_pi0_n", 'I');
  
  if (variable != "tau_calcVars_ChPiEMEOverCaloEME")
    factory->AddVariable("tau_calcVars_ChPiEMEOverCaloEME", 'F');
  if (variable != "tau_calcVars_EMPOverTrkSysP")
    factory->AddVariable("tau_calcVars_EMPOverTrkSysP", 'F');

  if ( nProng == 1 ) {
    if (variable != "tau_ipSigLeadTrk")
      factory->AddVariable("tau_ipSigLeadTrk", 'F'); // 1p
    if (variable != "tau_seedCalo_wideTrk_n")
      factory->AddVariable("tau_seedCalo_wideTrk_n", 'I'); // 1p
  }
  else {
    if (variable != "tau_massTrkSys")
      factory->AddVariable("tau_massTrkSys", 'F'); // 3p
    if (variable != "tau_seedCalo_dRmax")
      factory->AddVariable("tau_seedCalo_dRmax", 'F'); // 3p
    if (variable != "tau_trFlightPathSig")
      factory->AddVariable("tau_trFlightPathSig", 'F'); // 3p
  }
  //cgen::end::add_variable

  // global event weights per tree (see below for setting event-wise weights)
  Double_t signalWeight = 1.0;
  Double_t dataWeight = 1.0;

  // float weightWtaunu = 0.523001;
  // float weightZPrime1000 = 0.000037;
  // float weightZPrime250 = 0.000785;
  // float weightZPrime500 = 0.000734;
  // float weightZtautau = 0.005866;

  // float weightWtaunu = 106.169159;
  // float weightZPrime1000 = 0.007534;
  // float weightZPrime250 = 0.159332;
  // float weightZPrime500 = 0.148988;
  // float weightZtautau = 1.190776;
  
  // factory->AddSignalTree(Wtaunu, weightWtaunu);
  // factory->AddSignalTree(Ztautau, weightZtautau);
  // factory->AddSignalTree(Zprime250, weightZPrime250);
  // factory->AddSignalTree(Zprime500, weightZPrime500);
  // factory->AddSignalTree(Zprime1000, weightZPrime1000);
  // // factory->AddSignalTree(Wtaunu, signalWeight);
  // // factory->AddSignalTree(Ztautau, signalWeight);
  // // factory->AddSignalTree(Zprime250, signalWeight);
  // // factory->AddSignalTree(Zprime500, signalWeight);
  // // factory->AddSignalTree(Zprime1000, signalWeight);
  // factory->AddBackgroundTree(periodA, dataWeight);
  // factory->AddBackgroundTree(periodB, dataWeight);
  // factory->AddBackgroundTree(periodC, dataWeight);
  // factory->AddBackgroundTree(periodD, dataWeight);
  // factory->AddBackgroundTree(periodE, dataWeight);
  // factory->AddBackgroundTree(periodG, dataWeight);
  // factory->AddBackgroundTree(periodH, dataWeight);
  // factory->AddBackgroundTree(periodI, dataWeight);
  // factory->AddBackgroundTree(periodJ, dataWeight);
  // factory->AddBackgroundTree(periodL, dataWeight);
  factory->AddSignalTree(mctest, signalWeight);
  factory->AddBackgroundTree(datatest, dataWeight);

  // Apply additional cuts on the signal and background samples (can be different)
  TCut preselectionCut;
  TCut cut_eta;
  TCut cut_pt;
  TCut cut_trFlightPathSig = "tau_trFlightPathSig > -1000";

  if ( etaRegion == "barrel" ) {
    cut_eta = "abs(tau_eta) < 1.5"; }
  else if ( etaRegion == "endcap" ) {
    cut_eta = "abs(tau_eta) > 1.5"; }
  else {
    cut_eta = "abs(tau_eta) < 2.5"; }

  if ( ptRange == "min" ) {
    cut_pt = "tau_Et < 40000"; }
  else if ( ptRange  == "med" ) {
    cut_pt = "tau_Et > 40000 && tau_Et < 70000"; }
  else if ( ptRange == "max" ) {
    cut_pt = "tau_Et > 70000"; }
  else {
    cut_pt = "tau_Et > 0"; }
    
  if ( nProng == 1 ) {
    preselectionCut = "tau_numTrack == 1" && cut_eta && cut_pt;
    factory->SetSignalWeightExpression( GetWeights_SignalPt_1p() );
    factory->SetBackgroundWeightExpression( GetWeights_BackgroundMu_1p() ); 
  }
  else {      
    preselectionCut = "tau_numTrack == 3" && cut_trFlightPathSig && cut_eta && cut_pt;
    factory->SetSignalWeightExpression( GetWeights_SignalPt_3p() ); 
    factory->SetBackgroundWeightExpression( GetWeights_BackgroundMu_3p() ); 
  }
  // weights

  // tell the factory to use all remaining events in the trees after training for testing:
  factory->PrepareTrainingAndTestTree(preselectionCut,"");
  // factory->PrepareTrainingAndTestTree(preselectionCut,"NormMode=EqualNumEvents:SplitMode=Block"); //choose NormMode=EqualNumEvents for same bkr and sig number!
  if ( nProng == 1 )
    factory->PrepareTrainingAndTestTree(preselectionCut,"nTrain_Signal=686752:nTrain_Background=686752:nTest_Signal=686752:nTest_Background=686752:NormMode=NumEvents"); 
  else
    factory->PrepareTrainingAndTestTree(preselectionCut,"nTrain_Signal=195640:nTrain_Background=195640:nTest_Signal=195640:nTest_Background=195640:NormMode=NumEvents"); 

  // ---- Book MVA methods
  // BDT
  if (Use["BDT"]) // Adaptive Boost
    factory->BookMethod(TMVA::Types::kBDT, "BDT",
    			// "!H:!V:NTrees=10:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=20:PruneMethod=NoPruning:DoBoostMonitor:MinNodeSize=0.01");
    			"!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=NoPruning:DoBoostMonitor:MinNodeSize=0.01");

  // factory->BookMethod(TMVA::Types::kBDT, "alexeys_BDT",
  // 		      "!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=NoPruning:DoBoostMonitor");

  // factory->BookMethod(TMVA::Types::kBDT, "pedros_BDT",
  // 		      "!H:V:NTrees=70:MaxDepth=100000:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=500:PruneMethod=CostComplexity:PruneStrength=60:PruningValFraction=0.5:DoBoostMonitor");
		
  // if (Use["BDT"]) // Adaptive Boost
  //   factory->BookMethod(TMVA::Types::kBDT, "BDT",
  // 			"!H:!V:NTrees=70:MaxDepth=100000:nEventsMin=DEFAULT:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=500:PruneMethod=NoPruning");

  // BDT with pruning
  if (Use["BDTp"]) // Adaptive Boost
    factory->BookMethod(TMVA::Types::kBDT, "BDTp",
			"!H:!V:NTrees=100:MaxDepth=8:BoostType=AdaBoost:AdaBoostBeta=0.2:UseYesNoLeaf=FALSE:SeparationType=GiniIndex:nCuts=200:PruneMethod=CostComplexity:PruneStrength=60:PruningValFraction=0.5:DoBoostMonitor:MinNodeSize=0.01");

  // Likelihood
  if (Use["Likelihood"])
    factory->BookMethod(TMVA::Types::kLikelihood, "Likelihood",
			"!H:!V:PDFInterpol=Spline2:NSmooth=1");
  // Likelihood with decorrelation
  if (Use["LikelihoodD"])
    factory->BookMethod(TMVA::Types::kLikelihood, "LikelihoodD",
			"!H:!V:PDFInterpol=Spline2:NSmooth=1:VarTransform=Decorrelate");

  // Train MVAs using the set of training events
  factory->TrainAllMethods();

  // ---- Evaluate all MVAs using the set of test events
  factory->TestAllMethods();

  // ----- Evaluate and compare performance of all configured MVAs
  factory->EvaluateAllMethods();

  // --------------------------------------------------------------
  // Save the output
  outputFile->Close();

  std::cout << "==> Wrote root file: " << outputFile->GetName() << std::endl;
  std::cout << "==> TMVAClassification is done!" << std::endl;

  delete factory;

  // Launch the GUI for the root macros
  //if (!gROOT->IsBatch()) TMVAGui(outFName);
}
