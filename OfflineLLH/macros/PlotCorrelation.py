import sys
import os
import argparse
import ROOT
from ShellUtils import mkdir
sys.path.append('../../TauCommon/macros/')

# from Configurator import Configurator
# from HistTools import Formatter
# formatter = Formatter()

ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gStyle.SetPaintTextFormat("3.0f")

niceVarname = {"panTauCellBased_rCalChrg04" : "R_{cal,chrg}^{iso}",
               "panTauCellBased_rCalChrg02" : "R_{cal,chrg}^{core}",
               "panTauCellBased_eFracChrg0204" : "f_{E_{T},chrg}^{core-iso}",
               "tau_trFlightPathSig" : "S^{flight}_{T}",
               "panTauCellBased_eFracChrg0104" : "f_{E_{T},chrg}^{cent-iso}",
               "panTauCellBased_eFrac0204" : "f_{E_{T}}^{core-iso}",
               "panTauCellBased_eFrac0104" : "f_{E_{T}}^{cent-iso}",
               "panTauCellBased_rCal04" : "R_{cal}^{iso}",
               "panTauCellBased_nChrg01" : "N_{chrg}^{cent}",
               "panTauCellBased_nChrg0204" : "N_{chrg}^{wide}",
               "panTauCellBased_massChrgSys02" : "m_{chrg}^{core}",
               "panTauCellBased_ptRatioChrg02" : "f_{p_{T},chrg}^{core}",
               "panTauCellBased_dRmax04" : "#DeltaR_{max}^{iso}",
               "panTauCellBased_ptRatioChrg01" : "f_{p_{T},chrg}^{cent}",
               "panTauCellBased_ptRatioChrg02" : "f_{p_{T},chrg}^{core}",
               "panTauCellBased_ptRatioChrg04" : "f_{p_{T},chrg}^{iso}",
               "panTauCellBased_ptRatio01" : "f_{p_{T}}^{cent}",
               "panTauCellBased_ptRatio02" : "f_{p_{T}}^{core}",
               "panTauCellBased_ptRatio04" : "f_{p_{T}}^{iso}",
               "panTauCellBased_visTauM02" : "m_{vis}^{core}",
               "panTauCellBased_rCal02" : "R_{cal}^{core}",
               "tau_ipSigLeadTrk" : "S^{IP}_{lead track}",
               "panTauCellBased_fLeadChrg04" : "f_{lead,chrg}^{iso}",
               "panTauCellBased_massChrgSys01" : "m_{chrg}^{cent}",
               "panTauCellBased_massChrgSys04" : "m_{chrg}^{iso}",
               "panTauCellBased_dRmax02" : "#DeltaR_{max}^{core}",
               "panTauCellBased_ptRatioChrg02" : "f_{p_{T},chrg}^{core}",
               "panTauCellBased_fLeadChrg02" : "f_{lead,chrg}^{core}",
               "panTauCellBased_massNeutSys02" : "m_{neut}^{core}",
               "panTauCellBased_massNeutSys01" : "m_{neut}^{cent}",
               "panTauCellBased_ptRatioNeut04" : "f_{p_{T},neut}^{iso}",
               "panTauCellBased_nNeut0204" : "N_{neut}^{wide}",
               "panTauCellBased_visTauM04" : 'm_{vis}^{iso}',
               'panTauCellBased_dRminmaxPtChrg04' : '#DeltaR_{p_{T}^{minmax}}^{iso}'}

class CorrelationPlotter:
    def __init__(self):
        self.plotsDict = {}
        self.plotDict = {}
        self.corDict = {}

    def getVariables(self,
                     t):
        ROOT.SetOwnership(t,False)
        #EF BDT 3P
        # variables = ["panTauCellBased_rCalChrg04",
        #              "panTauCellBased_eFracChrg0204",
        #              "tau_trFlightPathSig",
        #              "panTauCellBased_eFracChrg0104",
        #              "panTauCellBased_eFrac0204",
        #              "panTauCellBased_eFrac0104",
        #              "panTauCellBased_rCal04",
        #              "panTauCellBased_nChrg0204",
        #              "panTauCellBased_massChrgSys02",
        #              "panTauCellBased_ptRatioChrg02",
        #              "panTauCellBased_dRmax04",
        #              "panTauCellBased_ptRatioChrg01",
        #              "panTauCellBased_visTauM02",
        #              "panTauCellBased_rCal02",
        #              "tau_ipSigLeadTrk",
        #              "panTauCellBased_fLeadChrg04",
        #              "panTauCellBased_massChrgSys01",
        #              "panTauCellBased_massChrgSys04",
        #              "panTauCellBased_dRmax02",
        #              "panTauCellBased_visTauM04"]
        #EF Generic 3P
        variables = ["panTauCellBased_rCalChrg04",
                     "panTauCellBased_eFracChrg0104",
                     "panTauCellBased_eFrac0104",
                     "panTauCellBased_dRmax04",
                     "panTauCellBased_rCal04",
                     "panTauCellBased_eFracChrg0204",
                     "panTauCellBased_rCalChrg02",
                     "panTauCellBased_dRmax02",
                     "panTauCellBased_rCal02",
                     "panTauCellBased_eFrac0204",
                     "panTauCellBased_massChrgSys04",
                     "panTauCellBased_dRminmaxPtChrg04",
                     "panTauCellBased_nChrg01",
                     "panTauCellBased_massChrgSys01",
                     "panTauCellBased_visTauM04",
                     "panTauCellBased_fLeadChrg04",
                     "tau_trFlightPathSig",
                     "panTauCellBased_ptRatio01",
                     "panTauCellBased_visTauM02",
                     "panTauCellBased_ptRatioChrg01"]
        # EFlowRec 1P BDT
        # variables = ["panTauCellBased_eFracChrg0104",
        #              "panTauCellBased_eFracChrg0204",
        #              "panTauCellBased_eFrac0104",
        #              "panTauCellBased_dRmax04",
        #              "panTauCellBased_rCal04",
        #              "panTauCellBased_rCal02",
        #              "panTauCellBased_rCalChrg04",
        #              "panTauCellBased_nChrg0204",
        #              "tau_ipSigLeadTrk",
        #              "panTauCellBased_ptRatio02",
        #              "panTauCellBased_ptRatio01",
        #              "panTauCellBased_visTauM04",
        #              "panTauCellBased_ptRatio04",
        #              "panTauCellBased_visTauM02",
        #              "panTauCellBased_fLeadChrg04",
        #              "panTauCellBased_massNeutSys01",
        #              "panTauCellBased_massChrgSys04",
        #              "panTauCellBased_ptRatioChrg02",
        #              "panTauCellBased_dRminmaxPtChrg04",
        #              "panTauCellBased_eFrac0204"]


        # EFlowRec 1P generic
        # variables = ["panTauCellBased_rCalChrg04",
        #              "panTauCellBased_eFrac0104",
        #              "panTauCellBased_rCal04",
        #              "panTauCellBased_eFracChrg0104",
        #              "panTauCellBased_dRmax04",
        #              "panTauCellBased_rCal02",
        #              "panTauCellBased_eFrac0204",
        #              "panTauCellBased_eFracChrg0204",
        #              "panTauCellBased_rCalChrg02",
        #              "panTauCellBased_dRmax02",
        #              "panTauCellBased_nChrg0204",
        #              "panTauCellBased_dRminmaxPtChrg04",
        #              "panTauCellBased_massChrgSys04",
        #              "panTauCellBased_fLeadChrg04",
        #              "panTauCellBased_ptRatio01",
        #              "panTauCellBased_visTauM04",
        #              "panTauCellBased_nChrg01",
        #              "panTauCellBased_ptRatioChrg01",
        #              "panTauCellBased_ptRatioChrg02",
        #              "panTauCellBased_nNeut02"]

        #CB 3p Generic 
        # variables = ["panTauCellBased_rCalChrg04",
        #              "panTauCellBased_eFracChrg0104",
        #              "panTauCellBased_eFrac0104",
        #              "panTauCellBased_rCal04",
        #              "panTauCellBased_dRmax04",
        #              "panTauCellBased_eFrac0204",
        #              "panTauCellBased_eFracChrg0204",
        #              "panTauCellBased_rCalChrg02",
        #              "panTauCellBased_dRmax02",
        #              "panTauCellBased_rCal02",
        #              "panTauCellBased_massChrgSys04",
        #              "panTauCellBased_dRminmaxPtChrg04",
        #              "panTauCellBased_nChrg01",
        #              "panTauCellBased_massChrgSys01",
        #              "panTauCellBased_visTauM01",
        #              "panTauCellBased_ptRatio01",
        #              "panTauCellBased_visTauM04",
        #              "tau_trFlightPathSig",
        #              "panTauCellBased_ptRatioChrg01",
        #              "panTauCellBased_fLeadChrg04"]
        # CB BDT 3p
        variables = ["panTauCellBased_rCalChrg04",
                     "panTauCellBased_eFracChrg0204",
                     "panTauCellBased_eFrac0204",
                     "panTauCellBased_rCal04",
                     "tau_trFlightPathSig",
                     "panTauCellBased_eFrac0104",
                     "panTauCellBased_nChrg0204",
                     "panTauCellBased_ptRatioChrg02",
                     "panTauCellBased_ptRatioNeut04",
                     "panTauCellBased_massChrgSys02",
                     "panTauCellBased_visTauM02",
                     "panTauCellBased_dRmax04",
                     "panTauCellBased_dRmax02",
                     "panTauCellBased_ptRatio02",
                     "tau_ipSigLeadTrk",
                     "panTauCellBased_ptRatioChrg01",
                     "panTauCellBased_massChrgSys01",
                     "panTauCellBased_massChrgSys04",
                     "panTauCellBased_ptRatioChrg04",
                     "panTauCellBased_fLeadChrg02"]
        # CB Generic 1p
        # variables = ["panTauCellBased_rCal04",
        #              "panTauCellBased_eFrac0104",
        #              "panTauCellBased_rCalChrg04",
        #              "panTauCellBased_eFracChrg0104",
        #              "panTauCellBased_eFrac0204",
        #              "panTauCellBased_dRmax04",
        #              "panTauCellBased_rCal02",
        #              "panTauCellBased_eFracChrg0204",
        #              "panTauCellBased_dRmax02",
        #              "panTauCellBased_rCalChrg02",
        #              "panTauCellBased_dRminmaxPtChrg04",
        #              "panTauCellBased_ptRatio01",
        #              "panTauCellBased_massChrgSys04",
        #              "panTauCellBased_nChrg0204",
        #              "panTauCellBased_nChrg01",
        #              "panTauCellBased_ptRatio02",
        #              "panTauCellBased_visTauM04",
        #              "panTauCellBased_nNeut0204",
        #              "panTauCellBased_fLeadChrg04",
        #              "panTauCellBased_ptRatioChrg01"]

        #CB 1p BDT specific
        # variables = ["panTauCellBased_rCal04",
        #              "panTauCellBased_eFrac0104",
        #              "panTauCellBased_eFracChrg0204",
        #              "panTauCellBased_nChrg0204",
        #              "panTauCellBased_rCal02",
        #              "panTauCellBased_dRmax04",
        #              "panTauCellBased_eFrac0204",
        #              "panTauCellBased_eFracChrg0104",
        #              "tau_ipSigLeadTrk",
        #              "panTauCellBased_ptRatio02",
        #              "panTauCellBased_ptRatio01",
        #              "panTauCellBased_ptRatio04",
        #              "panTauCellBased_visTauM04",
        #              "panTauCellBased_massNeutSys02",
        #              "panTauCellBased_massNeutSys01",
        #              "panTauCellBased_ptRatioNeut04",
        #              "panTauCellBased_nNeut0204",
        #              "panTauCellBased_fLeadChrg02",
        #              "panTauCellBased_massChrgSys04",
        #              "panTauCellBased_ptRatioChrg02"]
        #
        # variables = ["panTauCellBased_rCal04",
        #              "panTauCellBased_eFrac0104",
        #              "panTauCellBased_rCalChrg04",
        #              "panTauCellBased_eFracChrg0104",
        #              "panTauCellBased_eFrac0204",
        #              "panTauCellBased_dRmax04",
        #              "panTauCellBased_rCal02",
        #              "panTauCellBased_eFracChrg0204",
        #              "panTauCellBased_dRmax02",
        #              "panTauCellBased_rCalChrg02",
        #              "panTauCellBased_dRminmaxPtChrg04",
        #              "panTauCellBased_ptRatio01"] 
        #variables = [k.GetName() for k in t.GetListOfBranches() if ((k.GetName().startswith("panTauCellBased_")) and not (k.GetName().startswith("panTauCellBased_nChrg02")))]
        #variables = variables[0:2]
        return variables

    def getCorrelationPlots(self,
                            f,
                            p,
                            v):
        for i in v:
            for j in v[v.index(i) + 1:]:        
                for process in p:
                    t = f.Get(process)
                    eval('t.Draw("%s:%s>>+htemp","","")' % (i,j))
                    h = ROOT.gPad.GetPrimitive("htemp")
                    #if abs(h.GetXaxis().GetXmin()) < 0.01:
                    #    h.GetXaxis().SetRangeUser(0, h.GetXaxis().GetXmax())
                        
                    h.SetTitle(i + "_" + j)
                self.plotsDict[i+"_"+j] = [h.Clone(process+"_"+i+"_"+j)]
                ROOT.gPad.GetPrimitive("htemp").Clear()
        return self.plotsDict

    def mergeCorrelationPlots(self,
                              plots,
                              typ,
                              np,
                              o):
        for v,p in plots.items():
            c = ROOT.TCanvas("c", "c", 600, 600)
            c.cd()
            for i in range(len(p)):
                h = p[i]
            self.plotDict[v] = h.Clone(v)
            h.Draw("COLZ")
            h.SetStats(0)
            c.SaveAs(o+typ+'_'+v+'_Corr_'+np+'p.eps')
        return self.plotDict

    def getCorrelationFactor(self,
                             pd):
        for v,h in pd.items():
            self.corDict[v.replace('tau_','panTauCellBased_')] = h.GetCorrelationFactor()
        return self.corDict

    def getCorrelationMatrix(self,
                             cd,
                             v,
                             t,
                             p,
                             o):
        rf = ROOT.TFile.Open(o+"CorrMatrices.root", "UPDATE")
        rf.cd()
        c =  ROOT.TCanvas(t+p, t+p, 1200, 800)
        c.cd()
        h = ROOT.TH2F("h","",len(v),0.,len(v),len(v),0.,len(v))
        for i in range(len(v)):
            
            if v[i].startswith('panTauCellBased'):
                h.GetXaxis().SetBinLabel(i+1,v[i].replace("panTauCellBased_",""))
                h.GetYaxis().SetBinLabel(i+1,v[i].replace("panTauCellBased_",""))
            else:
                h.GetXaxis().SetBinLabel(i+1,v[i].replace("tau_",""))
                h.GetYaxis().SetBinLabel(i+1,v[i].replace("tau_",""))
        for x in range(len(v)):
            for y in range(len(v)):
                try:
                    h.SetBinContent(x+1,y+1,100*cd["panTauCellBased_"+h.GetXaxis().GetBinLabel(x+1)+"_panTauCellBased_"+h.GetYaxis().GetBinLabel(y+1)])
                except KeyError:
                    if h.GetXaxis().GetBinLabel(x+1) == h.GetYaxis().GetBinLabel(y+1):
                        h.SetBinContent(x+1,y+1,100)
                    else:
                        h.SetBinContent(x+1,y+1,100*cd["panTauCellBased_"+h.GetYaxis().GetBinLabel(y+1)+"_panTauCellBased_"+h.GetXaxis().GetBinLabel(x+1)])
        for i in range(h.GetXaxis().GetNbins() + 1):
            try:
                h.GetXaxis().SetBinLabel(i,niceVarname["panTauCellBased_" + h.GetXaxis().GetBinLabel(i)])
            except KeyError:
                try:
                    h.GetXaxis().SetBinLabel(i,niceVarname["tau_" + h.GetXaxis().GetBinLabel(i)])
                except KeyError:
                    pass
        for i in range(h.GetYaxis().GetNbins() + 1):
            try:
                h.GetYaxis().SetBinLabel(i,niceVarname["panTauCellBased_" + h.GetYaxis().GetBinLabel(i)])
            except KeyError:
                try:
                    h.GetYaxis().SetBinLabel(i,niceVarname["tau_" + h.GetYaxis().GetBinLabel(i)])
                except KeyError:
                    pass
        h.GetXaxis().LabelsOption("v")
        h.GetZaxis().SetRangeUser(-100,100)
        h.GetZaxis().SetTitle('linear correlation [%]')
        h.Draw("COLZTEXT")
        h.SetStats(0)
        c.SetBottomMargin(0.15)
        c.SetLeftMargin(0.1)
        c.SetRightMargin(0.15)
        c.Update()
        c.SaveAs(o+t+'Corr_'+p+'p.eps')
        c.Write(t+p)
        rf.Close()
        
def main(argv):
    parser = argparse.ArgumentParser(description = 'Correlation Plotter')
    parser.add_argument('file', help = 'input file (TMVA input)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    args = parser.parse_args()

    rf = ROOT.TFile.Open(args.file, 'READ')
    outpath = args.outdir
    # histConfig = Configurator('configs/correlation_config.yml').__dict__

    mkdir(outpath)
    #for prongfix in [1,3]:
    for prongfix in [3]:
        processes = [k.GetName() for k in rf.GetListOfKeys() if k.GetName().endswith(str(prongfix)+'p')]
        data = [p for p in processes if p.startswith('Period')]
        mc = list(set(processes) - set(data))
        mc.remove('Ztautau_train%ip'  % prongfix)
        mc.insert(0, 'Ztautau_train%ip' % prongfix)
        mcCorrelationPlotter = CorrelationPlotter()
        varList = mcCorrelationPlotter.getVariables(rf.Get(mc[0]))
        mcCorPlots = mcCorrelationPlotter.getCorrelationPlots(rf, mc, varList)
        mcCorPlot = mcCorrelationPlotter.mergeCorrelationPlots(mcCorPlots, 'mc', str(prongfix),outpath)
        mcCorFac = mcCorrelationPlotter.getCorrelationFactor(mcCorPlot)
        mcCorrelationPlotter.getCorrelationMatrix(mcCorFac, varList, 'mc', str(prongfix),outpath)
        dataCorrelationPlotter = CorrelationPlotter()
        dataCorPlots = dataCorrelationPlotter.getCorrelationPlots(rf, data, varList)
        dataCorPlot = dataCorrelationPlotter.mergeCorrelationPlots(dataCorPlots, 'data', str(prongfix),outpath)
        dataCorFac = dataCorrelationPlotter.getCorrelationFactor(dataCorPlot)
        dataCorrelationPlotter.getCorrelationMatrix(dataCorFac, varList, 'data', str(prongfix),outpath)
        
if __name__ == '__main__':
    main(sys.argv[1:])
