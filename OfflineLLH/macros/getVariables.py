import argparse
import sys
import re
    
def getSubVar(dictionary):
    
    def prepareLine(line):
        line = line.replace('> >','>>')
        line = line.replace('{return m_pParent->','')
        line = line.replace('->at(m_iIndex);}','')
        line = line.replace('const','')
        line = line.replace('\n','')
        line = line.replace('()','')
        line = line.replace('&','')
        line = line.split()
        line[0] = line[0].replace('>>','> >')
        return {line[1]:line[0]}
    
    tau = open('../../D3PDObjects/include/Tau.h')
    sub = open('subinput/subvars_ptpb.txt','w')

    for line in tau.readlines():
        # if re.search('([pP]an[tT]au)',line):
        #     # if re.search('([eE][fF][lL][oO][wW])',line):
        #     if re.search('([cC][eE][lL][lL])',line):
        #         dictionary.update(prepareLine(line))
            
        if re.search('pi0Bonn',line):
            dictionary.update(prepareLine(line))

        # elif re.search('[pP]i[0(_0)]',line):
        #     dictionary.update(prepareLine(line))

        # elif re.search('([bB][oO][nN][nN])',line):
        #     dictionary.update(prepareLine(line))

        # elif re.search('([cC][lL][uU][sS][tT][eE][rR])',line):
        #     dictionary.update(prepareLine(line))

        # elif re.search('([cC][eE][lL][lL])',line):
        #     dictionary.update(prepareLine(line))


        elif re.search('seedCalo_wideTrk',line):
            dictionary.update(prepareLine(line))
            
        else:
            continue

    for item in dictionary.items():
        print >> sub, item
    
    tau.close()
    sub.close()

    return dictionary

def writeHeader(dictionary):
    header = open('subinput/subheader_ptpb.txt','w')
    
    for name,typ in dictionary.items():
        string = 'std::vector<'
        string += typ
        string += ' > m_vtau_'
        string += name
        string += ';'
        
        print >> header, string
        
    header.close()
    
def writeClear(dictionary):
    clear = open('subinput/subclear_ptpb.txt','w')

    for name,typ in dictionary.items():
        string = 'm_vtau_'
        string += name
        string += '.clear();'

        print >> clear, string
            
    clear.close()

def writeInitialise(dictionary):
    initialise = open('subinput/subinitialise_ptpb.txt','w')

    for name,typ in dictionary.items():
        string = 'm_vtau_'
        string += name
        string += '.push_back(tau->'
        string += name
        string += '());'

        print >> initialise, string
    
    initialise.close()

def writeDeclare(dictionary):
    declare = open('subinput/subdeclare_ptpb.txt','w')

    for name,typ in dictionary.items():
        string = 'DeclareVariable('
        string += 'm_vtau_'
        string += name
        string += ', "tau_'
        string += name
        string += '", "tau");'
        
        print >> declare, string
    
    declare.close()

def main(argv):
    parser = argparse.ArgumentParser(description = 'script to generate substructure variable input for tnt')
    args = parser.parse_args()

    sub_dict = {} 
    sub_dict = getSubVar(sub_dict)
    writeHeader(sub_dict)
    writeClear(sub_dict)
    writeInitialise(sub_dict)
    writeDeclare(sub_dict)
    
if __name__ == '__main__':
    main(sys.argv[1:])
