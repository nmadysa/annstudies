#!/usr/bin/python
import sys, argparse
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from ROOT import gDirectory, TFile, TH3F


def createDirStructure(f):
    if not f.IsOpen():
        print "FATAL: Output file not open."
        sys.exit()
    
    author = ('both', 'calo')
    prongness = ('1prong','3prong')
    mdir = TDirectoryFile()
    f.cd()
    for p in prongness:
        gDirectory.cd('/')
        gDirectory.mkdir(p)
        gDirectory.cd(p)
        for a in author:   
            gDirectory.mkdir(a)
            

def copy(inFile, outFile):
    inFile.cd()
    gDirectory.cd('PDFPlotter')
    pdfs1P = []
    pdfs3P = []
    for key in gDirectory.GetListOfKeys():
        obj = gDirectory.Get(key.GetName())
        if (obj.GetName()).count('1prong') !=0:
            pdfs1P.append(obj)
        elif (obj.GetName()).count('3prong') !=0:
            pdfs3P.append(obj)

    outFile.cd("1prong/both")
    for h in pdfs1P:
        if h.GetName().count('25') != 0:
            h.SetName(h.GetName().replace('25','27'))
        h.Write()

    outFile.cd("3prong/both")
    for h in pdfs3P:
        if h.GetName().count('25') != 0:
            h.SetName(h.GetName().replace('25','27'))
        h.Write()

def convert(options):
    inFile = TFile.Open(options['inFile'])
    outFile = TFile.Open(options['outFile'],'RECREATE')
    createDirStructure(outFile)
    copy(inFile,outFile)
    inFile.Close()
    outFile.Close()

def main(argv):
    parser = argparse.ArgumentParser(description='LLH pdf converter - converts PDFCycle outputs to format requried by TauDiscriminant')
    parser.add_argument('input',help='Input file of PDFCycle which needs to be converted')
    parser.add_argument('output',help='Output file')

    args = parser.parse_args()
    options = {}
    options['inFile'] = args.input
    options['outFile'] = args.output

    convert(options)
if __name__ == "__main__":
    main(sys.argv[1:])
