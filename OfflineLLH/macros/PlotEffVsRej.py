import argparse
import sys
import os
sys.path.append('../../TauCommon/macros')
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from HistFormatter import plot_graph
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

class RocPlotter:
    def __init__(self, filelist, ptmin = 0, ptmax = -1):
        self.readFiles(filelist)
        self.parseProngness()
        self.algos = ['BDT', 'LLH', 'defaultBDT', 'defaultLLH']
        self.ptmin = ptmin
        self.ptmax = ptmax
        self.config = 'reference'
        self.plotdict = {self.config : {}}
        self.bkgdict = {self.config : {}}
        
    def readFiles(self, filelist, config = None):
        if not config is None:
            self.config = config
            self.plotdict[self.config] = {}
            self.bkgdict[self.config] = {}
            
        for f in filelist:
            if 'mc' in f:
                self.sig_rf = ROOT.TFile.Open(f,'READ')
            if 'data' in f:
                self.bg_rf = ROOT.TFile.Open(f,'READ')
            else:
                assert 'mc' or 'data' in f
            
        
    def parseProngness(self):
        if '1p' in self.sig_rf.GetName():
            self.prong = 1
            self.prongtext = "1 prong"
            self.outputdirectory = "1prong"
        elif '3p' in self.sig_rf.GetName():
            self.prong = 3
            self.prongtext = "3 prong"
            self.outputdirectory = "3prong"
        else:
            self.prong = 3
            self.prongtext = "multi-prong"
            self.outputdirectory = "mprong"
            
    def getEfficiency(self,
                      hist,
                      norm):
        efficiency = []
        hist.Draw()
        for i in range(0, hist.GetNbinsX() + 1, 1):
            efficiency.append(hist.GetBinContent(i) / norm)
    
        return efficiency

    def getRejection(self,
                     hist, norm): 
        tmp = self.getEfficiency(hist, norm)        
        return [1. / i  if i > 0  else 1000000. for i in tmp] 

    def getRocCurve(self):
        hNorm = self.sig_rf.Get("h_truthPt")
        norm_hist = hNorm.ProjectionY("norm", self.prong + 1, self.prong + 1, 0, -1)
        norm = norm_hist.Integral(self.ptmin+1, self.ptmax)
        hNormBkg = self.bg_rf.Get("h_total_pt")
        normBkg = hNormBkg.Integral(self.ptmin+1, self.ptmax)
        for algo in self.algos:
            mc = self.sig_rf.Get('h_passed_%s_pt' % algo)
            mc_temp = mc.ProjectionX("mc",self.ptmin+1, self.ptmax)
            data = self.bg_rf.Get('h_passed_%s_pt' % algo)
            data_temp = data.ProjectionX("data",self.ptmin+1, self.ptmax)
            signal = self.getEfficiency(mc_temp,norm)
            self.signalForRatio = signal
            bkg = self.getRejection(data_temp, normBkg)
                
            graph = plot_graph('RejVsEff',
                               signal,
                               bkg,
                               draw_options = 'ac*')
            self.plotdict[self.config].update({algo : graph})
            self.bkgdict[self.config].update({algo : bkg})

    def foo(self, compareAlgos, compareConfigs, config = 'reference'):
        if compareAlgos:
            algos_copy = self.plotdict[config].keys()[:]
            for algo1 in self.plotdict[config].keys():
                algos_copy.remove(algo1)
                for algo2 in algos_copy:
                    if algo1 == algo2:
                        continue
                    self.plotRocCurve(algo1, algo2, config, config)
        if compareConfigs:
            print self.plotdict
            for algo in self.plotdict['reference'].keys():
                self.plotRocCurve(algo, algo, 'reference', config)
        else:
            print "wrong"
            
    def plotRocCurve(self, algo1 = "BDT", algo2 = "BDT", config1 = 'reference', config2 = 'reference'):
        ratio = []
        for i in range(len(self.bkgdict[config1][algo1])):
            ratio.append(self.bkgdict[config1][algo1][i]/self.bkgdict[config2][algo2][i])
        
        canvas = ROOT.TCanvas("c", "c", 800, 600)
        g1 = self.plotdict[config1][algo1]['graph']
        g2 = self.plotdict[config2][algo2]['graph']
        g1.SetName(str(self.ptmin)+str(self.ptmax))
        g2.SetName(str(self.ptmin)+str(self.ptmax))
        canvas.cd()
        g1.Draw('ac*')
        g2.Draw('c*same')
        formatter.setTitle(g1,
                           'signal efficiency',
                           'x')
        formatter.setTitle(g1,
                           'background rejection',
                           'y')
        formatter.setMinMax(g1,
                            1.,
                            100000.,
                            'y')
        formatter.setMinMax(g1,
                            0.001,
                            3.,
                            'x')
        labels = ['BDT w/ pile-up corrections', 'BDT w/o pile-up corrections']
        labels = [algo1, algo2]
        formatter.addLegendToCanvas(canvas,
                                    legendOptions = {'x1': 0.4, 'x2': 0.9, 'y1': 0.7, 'y2': 0.9},
                                    overwriteLabels = labels,
                                    overwriteDrawOptions = ['L', 'L'])
        formatter.addLumiText(canvas,
                              lumi = 20281.4,
                              pos = {'x':0.2,'y':0.3},
                              splitLumiText = False)
        if self.ptmax == -1:
            formatter.addText(canvas,
                              self.prongtext + ', ' + str(self.ptmin) + ' GeV < pt',
                              pos = {'x': 0.2, 'y': 0.25})
        else:
            formatter.addText(canvas,
                              self.prongtext + ', ' + str(self.ptmin) + ' < pt < ' + str(self.ptmax) + ' GeV',
                              pos = {'x': 0.2, 'y': 0.25})
            formatter.addATLASLabel(canvas,
                                    description = 'internal',
                                    pos= {'x':0.2,'y':0.2},
                                    size = 0.045,
                                    offset = 0.11)
                    
        g1.SetLineWidth(3)
        g2.SetLineWidth(3)
        g1.SetLineColor(ROOT.kBlue)
        g2.SetLineColor(ROOT.kRed)
        g1.SetMarkerStyle(21)
        g2.SetMarkerStyle(22)
        g1.SetMarkerColor(4)
        g2.SetMarkerColor(2)
        
        canvas.SetLogy()
        canvas.Update()
        canvas.SaveAs(os.path.join(path,'BkgRejVsEff_%s_%s_%s_%i%i_%s_%s.eps' % (algo1,
                                                                                 algo2,
                                                                                 self.outputdirectory,
                                                                                 self.ptmin,
                                                                                 self.ptmax,
                                                                                 config1,
                                                                                 config2)))

        gRatio = plot_graph('ratio',
                            self.signalForRatio,
                            ratio)
        cRatio = gRatio['canvas']
        gr = gRatio['graph']
        formatter.setMinMax(gr,
                            0.001,
                            3.,
                            'x')
        formatter.setMinMax(gr,
                            0.8,
                            1.2,
                            'y')
        
        control = plotter.addRatioToCanvas(canvas,cRatio)
        control.SaveAs(os.path.join(path,'BkgRejVsEff_%s_%s_%s_%i%i_%s_%s_ratio.eps' % (algo1,
                                                                                        algo2,
                                                                                        self.outputdirectory,
                                                                                        self.ptmin,
                                                                                        self.ptmax,
                                                                                        config1,
                                                                                        config2)))
                
def main(argv):
    parser = argparse.ArgumentParser(description = 'plotter for background rejection vs. signal efficiency')
    parser.add_argument('filelist', nargs = 2, help = 'input filelist (efficiency evaluation output)')
    parser.add_argument('--filelist_compare', '-cf', default = None, nargs = 2, help = 'input filelist for comparison (efficiency evaluation output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    parser.add_argument('--ptmin', '-min', type = int, default = 0, help = 'minimum for pt cut')
    parser.add_argument('--ptmax', '-max', type = int, default = -1, help = 'maximum for pt cut')
    parser.add_argument('--variable', '-v', help = 'variable to be replaced')
    args = parser.parse_args()
    
    global path
    path = args.outdir
    
    rocPlotter = RocPlotter(args.filelist,
                            args.ptmin,
                            args.ptmax)
    rocPlotter.getRocCurve()
    rocPlotter.foo(True, False)
    if args.filelist_compare:
        rocPlotter.readFiles(args.filelist_compare, 'compare')
        rocPlotter.getRocCurve()
        
    rocPlotter.foo(False, True, 'compare')        
if __name__ == '__main__':
    main(sys.argv[1:])
