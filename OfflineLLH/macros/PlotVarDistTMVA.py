import argparse
import sys
import os
import ROOT

sys.path.append('../../TauCommon/macros/')

from InputHandler import Reader
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from Configurator import Configurator
from CommonTools import Writer
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

def plotVarDist(rf, path, varConfig):

    td = rf.Get('Method_BDT/BDT')
    print td, type(td)
    for variable, config in varConfig.items():
        if config.has_key('nprong') and config['nprong'] == 3:
            continue
        print variable, ': proccessing'
        signal = td.Get(variable + '__Signal')
        data = td.Get(variable + '__Background')
        signal.Scale(1/signal.Integral())
        data.Scale(1/data.Integral())

        canvas = plotter.plotHistograms([signal, data],
                                        plotOptions = [PO(fill_style = 3003,
                                                          fill_color = ROOT.kRed),
                                                       PO()],
                                        drawOptions = ['hist', 'p'])
        canvas.Update()
        canvas.SaveAs(os.path.join(path, '%s.eps' % variable))
        print variable, ': done'
        
def main(argv):
    parser = argparse.ArgumentParser(description = 'Score Plotter')
    parser.add_argument('file', help = 'input file (TMVA output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    args = parser.parse_args()

    varConfig = Configurator('configs/tmvavariables_config.yml').__dict__
    
    rf = ROOT.TFile.Open(args.file, 'READ')
    
    path = args.outdir

    plotVarDist(rf, path, varConfig)
        
if __name__ == '__main__':
    main(sys.argv[1:])
