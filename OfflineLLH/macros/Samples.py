import Utilities
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from HistFormatter import PlotOptions as PO
import re
_logger = Utilities.GetLogger("samples","ERROR")

class Sample(object):
    """
    class describing a Monte Carlo Sample
    """
    def __init__(self,name=""):
        self.name = name
        self.ID = 0
        self.evgenTag = ""
        self.xsec = -1.0
        self.k_fac = -1.0
        self.filtereff = -1.0
        self.generatedEvents = -1
        self.process = ""
        self.merged_process = self.process
        self.label = ""
        self.tex_name = ""
        self.generator = ""
        self.hasMCWeight = False
        self.plot_options = PO()
        #self.xslimitxsec
        
    def lumiWeight(self,mc_events,lumi,useNormalisedSignalXS=False):
        """
        returns the luminosity weight for a given integrated luminosity
        """
        if self.xsec < 0 or self.k_fac < 0 or self.filtereff < 0:
            _logger.error("invalid cross-section %.2f, k-factor %.2f filter efficiency %.2f",self.xsec,self.k_fac,self.filtereff)
            print self.name
            return 0
        if mc_events <= 0:
            _logger.error("received invalid number of MC events %f",mc_events)
            return 0
        if mc_events != self.generatedEvents:
            _logger.warning("different number of generated events: given %f != generated %f",mc_events,self.generatedEvents)
        if useNormalisedSignalXS:
            return float(0.6476 * 0.6476 * self.k_fac * lumi * self.filtereff) / mc_events
        else:
            return float(self.xsec * self.k_fac * lumi * self.filtereff) / mc_events

    def getWeight(self):
        if not self.xsec:
            return None
        return (self.xsec * self.k_fac * self.filtereff)
    
    def isData(self):
        """
        MC samples are never data
        """
        return False

class SignalSample(Sample):
    def __init__(self, name = ""):
        Sample.__init__(self, name)
        self.mass = re.findall(r'\d+', self.name)[0]
        self.process = self.name.replace(self.mass, '')
        self.signal_plot_options = self.plot_options
        
class DataSample(object):
    """
    class describing a data sample
    """
    def __init__(self,name=''):
        self.name = name
        self.label = ''
        self.lumi = -1.0
        self.generatedEvents = -1
        self.plot_options = PO(line_color=ROOT.kBlack,marker_style=20)
        self.process = "Data"
        self.merged_process = "Data"
    def isData(self):
        """
        data samples are always data
        """
        return True

k_Ztautau = 1.23

Wtaunu = Sample("Wtaunu")
Wtaunu.ID=147812
Wtaunu.xsec=10460
Wtaunu.k_fac = 1.0
Wtaunu.filtereff=1.0
Wtaunu.generatedEvents=1999997
Wtaunu.label="W#rightarrow #tau#nu"
Wtaunu.process="Wtaunu"
Wtaunu.plot_options=PO(fill_color=ROOT.kAzure+6)

Ztautau = Sample("Ztautau")
Ztautau.ID=147818
Ztautau.xsec=878.04
Ztautau.k_fac = 1.0
Ztautau.filtereff=1.0
Ztautau.generatedEvents=14993566
Ztautau.label="Z/#gamma^{*}#rightarrow #tau#tau"
Ztautau.process="Z_DY_tautau"
Ztautau.plot_options=PO(fill_color=ROOT.kOrange)
Ztautau.tex_name="$Z/\\gamma^{\\ast} \\to \\tau\\tau$ (inclusive)"
Ztautau.generator=""

ZPrime250 = SignalSample("ZPrime250")
ZPrime250.ID=170201
ZPrime250.xsec=2.8256
ZPrime250.k_fac=1.0
ZPrime250.filtereff=1.0
ZPrime250.generatedEvents=360000
ZPrime250.label="ZPrime250"
ZPrime250.process="ZPrime250"
ZPrime250.plot_options=PO(fill_color=ROOT.kRed-5,fill_style=1001)

ZPrime500 = SignalSample("ZPrime500")
ZPrime500.ID=170202
ZPrime500.xsec=2.8256
ZPrime500.k_fac=1.0
ZPrime500.filtereff=1.0
ZPrime500.generatedEvents=384995
ZPrime500.label="ZPrime500"
ZPrime500.process="ZPrime500"
ZPrime500.plot_options=PO(fill_color=ROOT.kRed-5,fill_style=1001)

ZPrime750 = SignalSample("ZPrime750")
ZPrime750.ID=170203
ZPrime750.xsec=0.53747
ZPrime750.k_fac=1.0
ZPrime750.filtereff=1.0
ZPrime750.generatedEvents=399999
ZPrime750.label="ZPrime750"
ZPrime750.process="ZPrime750"
ZPrime750.plot_options=PO(fill_color=ROOT.kRed-5,fill_style=1001)

ZPrime1000 = SignalSample("ZPrime1000")
ZPrime1000.ID=170204
ZPrime1000.xsec=0.14845
ZPrime1000.k_fac=1.0
ZPrime1000.filtereff=1.0
ZPrime1000.generatedEvents=399997
ZPrime1000.label="ZPrime1000"
ZPrime1000.process="ZPrime1000"
ZPrime1000.plot_options=PO(fill_color=ROOT.kRed-5,fill_style=1001)

ZPrime1250 = SignalSample("ZPrime1250")
ZPrime1250.ID=170205
ZPrime1250.xsec=0.049671
ZPrime1250.k_fac=1.0
ZPrime1250.filtereff=1.0
ZPrime1250.generatedEvents=399798
ZPrime1250.label="ZPrime1250"
ZPrime1250.process="ZPrime1250"
ZPrime1250.plot_options=PO(fill_color=ROOT.kRed-5,fill_style=1001)
ZPrime1250.signalplot_options=PO(fill_color=ROOT.kRed,marker_color=ROOT.kRed,line_color=ROOT.kRed,line_width=3,fill_style=0)

DataPeriod_A = DataSample("DataPeriod_A")
DataPeriod_A.label = "Data 2012"
DataPeriod_A.lumi = float(11.736)
DataPeriod_A.generatedEvents = 2166986

DataPeriod_B = DataSample("DataPeriod_B")
DataPeriod_B.label = "Data 2012"
DataPeriod_B.lumi = float(11.736)
DataPeriod_B.generatedEvents = 5345174

DataPeriod_C = DataSample("DataPeriod_C")
DataPeriod_C.label = "Data 2012"
DataPeriod_C.lumi = float(11.736)
DataPeriod_C.generatedEvents = 1580896

DataPeriod_D = DataSample("DataPeriod_D")
DataPeriod_D.label = "Data 2012"
DataPeriod_D.lumi = float(11.736)
DataPeriod_D.generatedEvents = 3336303

DataPeriod_E = DataSample("DataPeriod_E")
DataPeriod_E.label = "Data 2012"
DataPeriod_E.lumi = float(11.736)
DataPeriod_E.generatedEvents = 2700581

DataPeriod_G = DataSample("DataPeriod_G")
DataPeriod_G.label = "Data 2012"
DataPeriod_G.lumi = float(11.736)
DataPeriod_G.generatedEvents = 1329050

DataPeriod_H = DataSample("DataPeriod_H")
DataPeriod_H.label = "Data 2012"
DataPeriod_H.lumi = float(11.736)
DataPeriod_H.generatedEvents = 1529361

DataPeriod_I = DataSample("DataPeriod_I")
DataPeriod_I.label = "Data 2012"
DataPeriod_I.lumi = float(11.736)
DataPeriod_I.generatedEvents = 1122801

DataPeriod_J = DataSample("DataPeriod_J")
DataPeriod_J.label = "Data 2012"
DataPeriod_J.lumi = float(11.736)
DataPeriod_J.generatedEvents = 2748271

DataPeriod_L = DataSample("DataPeriod_L")
DataPeriod_L.label = "Data 2012"
DataPeriod_L.lumi = float(11.736)
DataPeriod_L.generatedEvents = 903624

Samples = {}

Samples['Wtaunu'] = Wtaunu
Samples['Ztautau'] = Ztautau

Samples['Signal'] = Ztautau
Samples['Data'] = DataPeriod_B


Samples['ZPrime250'] = ZPrime250
Samples['ZPrime500'] = ZPrime500
Samples['ZPrime750'] = ZPrime750
Samples['ZPrime1000'] = ZPrime1000
Samples['ZPrime1250'] = ZPrime1250

Samples["PeriodA"] = DataPeriod_A
Samples["PeriodB"] = DataPeriod_B
Samples["PeriodC"] = DataPeriod_C
Samples["PeriodD"] = DataPeriod_D
Samples["PeriodE"] = DataPeriod_E
Samples["PeriodG"] = DataPeriod_G
Samples["PeriodH"] = DataPeriod_H
Samples["PeriodI"] = DataPeriod_I
Samples["PeriodJ"] = DataPeriod_J
Samples["PeriodL"] = DataPeriod_L

def find_sample(ds_no):
    for sample in Samples.values():
        if sample.isData():
            continue
        if int(ds_no) == int(sample.ID):
            return sample
    return None

def find_sample_by_short_name(sname):
    for sample in Samples.values():
        if sample.isData():
            continue
        if sname == str(sample.process):
            return sample
    return None
