import sys
import os
import argparse
from ShellUtils import mkdir

variableTransform = {'CentFrac0102' : 'tau_calcVars_corrCentFrac',
                     'FTrk02' : 'tau_calcVars_corrFTrk',
                     'pi0_vistau_m' : 'tau_pi0_vistau_m',
                     'ptRatio' : 'tau_ptRatio',
                     'TrkAvgDist' : 'tau_seedCalo_trkAvgDist',
                     'pi0_n' : 'tau_pi0_n',
                     'IpSigLeadTrk' : 'tau_ipSigLeadTrk',
                     'NTracksdrdR' : 'tau_seedCalo_wideTrk_n',
                     'MassTrkSys' : 'tau_massTrkSys',
                     'DrMax' : 'tau_seedCalo_dRmax',
                     'TrFligthPathSig' : 'tau_trFlightPathSig'}

def writeHeader(script):
    print >> script, 'import os'
    print >> script, 'from ShellUtils import cd'
    print >> script, 'cd(\'../config/\')'

def rewriteConfig(configFileName,
                  outFileName,
                  prong,
                  variableToBeReplaced):
    iconfig = open(configFileName, 'r')
    oconfig = open(outFileName, 'w')

    for line in iconfig.readlines():
        if line.startswith('<!--'):
            continue
        elif line.count('varset%i' % prong):
            # print >> oconfig, line.replace(variableToBeReplaced, '')
            templine = line.replace(variableToBeReplaced, '')
            print >> oconfig, templine.replace(' "','"')
        elif line.count('llh_cuts_inputfile'):
            try:
                print >> oconfig, '<Item Name="llh_cuts_inputfile" Value="/raid2/users/hanisch/TauAnalysis/OfflineLLH/out/cuts/current_result/%s/LMT_cuts_LLH.root" />' % variableToBeReplaced
            except KeyError:
                print >> oconfig, line
        elif line.count('bdt_cuts_inputfile'):
            try:
                print >> oconfig, '<Item Name="bdt_cuts_inputfile" Value="/raid2/users/hanisch/TauAnalysis/OfflineLLH/out/cuts/current_result/%s/LMT_cuts_BDT.root" />' % variableToBeReplaced
            except KeyError:
                print >> oconfig, line
        elif line.count('bdt_weight_file'):
            try:
                print >> oconfig, '<Item Name="bdt_weight_file" Value="/raid2/users/hanisch/TauAnalysis/OfflineLLH/out/TMVA/output/current_result/%s/weights_%ip/TMVAClassification_BDT.weights.xml" />' % (variableTransform[variableToBeReplaced], prong)
            except KeyError:
                print >> oconfig, line
        elif line.count('prongmode'):
            if prong == 1:
                print >> oconfig, '<Item Name="prongmode" Value="1" />'
            else:
                print >> oconfig, '<Item Name="prongmode" Value="2" />'
        else:
            print >> oconfig, line.replace('\n','')

    iconfig.close()
    oconfig.close()

def rewriteCycle(fileName,
                 outFileName,
                 prong,
                 variableToBeReplaced):
    iconfig = open(fileName, 'r')
    oconfig = open(outFileName, 'w')

    for line in iconfig.readlines():
        if line.count('<!--'):
            continue
        elif line.count('Common_config') and line.count('ENTITY'):
            print >> oconfig, '<!ENTITY Common_config  SYSTEM "EfficiencyBase_config_%s_%ip.xml">' % (variableToBeReplaced, prong)
            continue
        elif line.count('<Cycle'):
            info = dict(item.split("=\"") for item in line.split("\" ")[:-2])
            info['OutputDirectory'] += variableToBeReplaced + '/'
            mkdir(info['OutputDirectory'])
            info['PostFix'] = '.1p' if prong == 1 else '.mp'
            for key, item in info.items():
                print >> oconfig, '%s=\"%s\"' %(key,item),
            print >> oconfig, '%s \n' % line.split("\" ")[-1]
            continue
        else:
            print >> oconfig, line.replace('\n','')

    iconfig.close()
    oconfig.close()
    
def main(argv):
    parser = argparse.ArgumentParser(description = 'script to generate xml files for efficiency cycle')
    args = parser.parse_args()

    variableset_1p = ["CentFrac0102",
                      "FTrk02",
                      "pi0_vistau_m",
                      "ptRatio",
                      "TrkAvgDist",
                      "pi0_n",
                      "IpSigLeadTrk",
                      "NTracksdrdR"]
    variableset_3p = ["CentFrac0102",
                      "FTrk02",
                      "pi0_vistau_m",
                      "ptRatio",
                      "TrkAvgDist",
                      "pi0_n",
                      "MassTrkSys",
                      "DrMax",
                      "TrFligthPathSig"]

    runScriptOpt = open('runScriptOptimisation.py', 'w')
    runScriptEval = open('runScriptEvaluation.py', 'w')

    writeHeader(runScriptOpt)
    writeHeader(runScriptEval)
    
    for prong in [1,3]:
        for variable in eval('variableset_%ip' % prong):
            rewriteConfig(os.path.abspath('../config/EfficiencyBase_config.xml'),
                          os.path.abspath('../config/EfficiencyBase_config_%s_%ip.xml') % (variable,prong),
                          prong,
                          variable)
            
            rewriteCycle(os.path.abspath('../config/RunEfficiencyOptimisationCycle.xml'),
                         os.path.abspath('../config/RunEfficiencyOptimisationCycle_%s_%ip.xml') % (variable,prong),
                         prong,
                         variable)
            print >> runScriptOpt, 'os.system(\'sframe_main RunEfficiencyOptimisationCycle_%s_%ip.xml\')' % (variable,prong)

            rewriteCycle(os.path.abspath('../config/RunEfficiencyEvaluationCycle.xml'),
                         os.path.abspath('../config/RunEfficiencyEvaluationCycle_%s_%ip.xml') % (variable,prong),
                         prong,
                         variable)
            print >> runScriptEval, 'os.system(\'sframe_main RunEfficiencyEvaluationCycle_%s_%ip.xml\')' % (variable,prong)
        
    runScriptOpt.close()
    runScriptEval.close()
    
if __name__ == '__main__':
    main(sys.argv[1:])
