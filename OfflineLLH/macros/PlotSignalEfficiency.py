import argparse
import sys
import os
from array import array
sys.path.append('../../TauCommon/macros')
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from EfficiencyCalculator import EfficiencyCalculator
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from HistFormatter import CanvasOptions as CO
from HistFormatter import plot_graph
from ShellUtils import mkdir

plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)
class EfficiencyPlotter:
    def __init__(self, filelist):
        self.calculator = EfficiencyCalculator(filelist)
        self.prongtext = self.calculator.prongtext
        self.outputdirectory = self.calculator.outputdirectory
        self.config = 'reference'
        self.plots = {'pt' : {self.config : {}}, 'mu' : {self.config : {}}}        
        self.pattern = ''
        
    def plotEfficiencies(self):
        self.effPt = self.calculator.calculateEfficiencyVsPt()
        self.effMu = self.calculator.calculateEfficiencyVsMu()
        #for algo in ['LLH','BDT','defaultLLH','defaultBDT']:
        #for algo in ['defaultBDT', 'defaultLLH']:
        for algo in ['defaultLLH', 'LLH']:
        #for algo in ['LLH', 'BDT']:
        #for algo in ['BDT', 'LLH']:
            self.plotEfficiencyVsPt(algo)
            self.plotEfficiencyVsMu(algo)

    def updateFile(self, fName, config):
        self.calculator.update(fName)
        self.config = config
        self.pattern = "_" + config
        self.plots['pt'][self.config] = {}
        self.plots['mu'][self.config] = {}
        
    def plotEfficiencyVsPt(self, algo = 'BDT'):
        plots = self.effPt
        [signal_loose, signal_medium, signal_tight] = plots[algo]
        self.plots['pt'][self.config][algo] = plots[algo]
        canvas = plotter.plotHistograms([signal_loose, signal_medium, signal_tight],
                                        plotOptions = [PO(marker_color = ROOT.kGreen,
                                                          marker_style = 22),
                                                       PO(marker_color = ROOT.kBlue,
                                                          marker_style = 21),
                                                       PO(marker_color = ROOT.kRed,
                                                          marker_style = 23)],
                                        drawOptions = ['P','P','P'],
                                        canvasName = 'signal efficiency vs pt')
            
        formatter.setMinMax(signal_loose,
                            0.,
                            1.3,
                            'y')
        formatter.setMinMax(signal_loose,
                            10.,
                            160.,
                            'x')
        formatter.setTitle(signal_loose,
                           'p^{gen}_{T,vis} [GeV]',
                           'x')
        formatter.setTitle(signal_loose,
                           'signal efficiency',
                           'y')
        formatter.addLegendToCanvas(canvas,
                                    legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9},
                                    overwriteLabels = ['loose', 'medium', 'tight'],
                                    overwriteDrawOptions = ['P', 'P', 'P'])
        # formatter.addLumiText(canvas,
        #                       lumi = 20281.4,
        #                       pos = {'x':0.6,'y':0.8},
        #                       splitLumiText = False)
        formatter.addText(canvas,
                          self.prongtext + ', ' + algo,
                          pos = {'x': 0.6, 'y': 0.75})
        # formatter.addATLASLabel(canvas,
        #                         description = 'internal',
        #                         pos= {'x': 0.6, 'y': 0.7},
        #                         size = 0.045,
        #                         offset = 0.11)
        
        canvas.Update()
        canvas.SaveAs(os.path.join(path, 'SignalEfficiency_%s_%s_pt%s.eps' % (algo, self.outputdirectory, self.pattern)))

    def plotEfficiencyVsMu(self, algo = 'BDT'):
        plots = self.calculator.calculateEfficiencyVsMu()
        [signal_loose, signal_medium, signal_tight] = plots[algo]
        self.plots['mu'][self.config][algo] = plots[algo]
        canvas = plotter.plotHistograms([signal_loose, signal_medium, signal_tight],
                                        plotOptions = [PO(marker_color = ROOT.kGreen,
                                                          marker_style = 22),
                                                       PO(marker_color = ROOT.kBlue,
                                                          marker_style = 21),
                                                       PO(marker_color = ROOT.kRed,
                                                          marker_style = 23)],
                                        drawOptions = ['P','P','P'],
                                        canvasName = 'signal efficiency vs mu')
        
        formatter.setMinMax(signal_loose,
                            0.,
                            1.3,
                            'y')
        formatter.setMinMax(signal_loose,
                            0.,
                            40.,
                            'x')
        formatter.setTitle(signal_loose,
                           'signal efficiency',
                           'y')
        formatter.setTitle(signal_loose,
                           '#mu',
                           'x')
        formatter.addLegendToCanvas(canvas,
                                    legendOptions = {'x1': 0.2, 'x2': 0.5, 'y1': 0.7, 'y2': 0.9} ,
                                    overwriteLabels = ['substructure loose', 'substructure medium', 'substructure tight'],
                                    overwriteDrawOptions = ['P', 'P', 'P'])
        # formatter.addLumiText(canvas,
        #                       lumi = 20281.4,
        #                       pos = {'x':0.6,'y':0.8},
        #                       splitLumiText = False)
        formatter.addText(canvas,
                          self.prongtext + ', ' + algo,
                          pos = {'x': 0.6, 'y': 0.75})
        formatter.addATLASLabel(canvas,
                                description = 'internal',
                                pos= {'x':0.6,'y':0.8},
                                size = 0.045,
                                offset = 0.12)

        canvas.Update()
        canvas.SaveAs(os.path.join(path, 'SignalEfficiency_%s_%s_mu.eps' % (algo, self.outputdirectory)))

    def plotCombinedEfficiencies(self, label):
        for dep in self.plots.keys():
            for algo in ['LLH','BDT','defaultLLH','defaultBDT']:
            #for algo in ['defaultLLH','defaultBDT']:
                self.plotCombinedEfficiency(dep, algo, label)

    def foo(self, compareAlgos, compareConfigs, config = 'reference'):
        for dep in self.plots.keys():
            if compareAlgos:
                algos_copy = self.plots[dep][config].keys()[:]
                print algos_copy
                for algo1 in self.plots[dep][config].keys():
                    algos_copy.remove(algo1)
                    for algo2 in algos_copy:
                        if algo1 == algo2:
                            continue
                        self.plotCombinedEfficiency(dep, algo1, algo2, config, config)
            if compareConfigs:
                for algo in self.plots[dep]['reference'].keys():
                    self.plotCombinedEfficiency(dep, algo, algo, 'reference', config)
            else:
                print "wrong"
                
    def plotCombinedEfficiency(self, dep, algo1 = "BDT", algo2 = "BDT", config1 = 'reference', config2 = 'reference'):
        if algo2.startswith('default'):
            algo1, algo2 = algo2, algo1
        # algo1Name = algo1.replace('default', 'default ')
        # algo2Name = algo2 + " incl. #pi^{0} vars"
        # algo1Name = 'default '+ algo1
        # algo2Name = 'corrected pdf'
        # algo1Name = 'CellBased '+ algo1
        # algo2Name = 'CellBased incl. R_{cal}^{iso} ' + algo2
        # algo1Name = 'EflowRec '+ algo1
        # algo2Name = 'EflowRec incl. R_{cal}^{core} ' + algo2
        algo1Suffix = ' (previous)'
        algo2Suffix = ' (incl. #pi^{0} variables) '
        # algo1Suffix = ' (equivalent)'
        # algo2Suffix = ' (incl. R_{cal}^{iso})'
        # algo1Suffix = ' (w/o pu correction)'
        # algo2Suffix = ' (w/ pu correction) '
        #algo1Suffix = ''
        #algo2Suffix = ''
        
        canvas = plotter.plotHistograms(self.plots[dep][config1][algo1] + self.plots[dep][config2][algo2],
                                        plotOptions = [PO(marker_color = ROOT.kGreen,
                                                          marker_style = 22),
                                                       PO(marker_color = ROOT.kBlue,
                                                          marker_style = 21),
                                                       PO(marker_color = ROOT.kRed,
                                                          marker_style = 23),
                                                       PO(marker_color = ROOT.kGreen,
                                                          marker_style = 26),
                                                       PO(marker_color = ROOT.kBlue,
                                                          marker_style = 25),
                                                       PO(marker_color = ROOT.kRed,
                                                          marker_style = 32)],
                                        drawOptions = ['P','P','P','P','P','P'],
                                        canvasName = 'signal efficiency vs pt')
        algo1 = algo1.replace('default','')
        algo2 = algo2.replace('default','')
        # formatter.addLegendToCanvas(canvas,
        #                             legendOptions = {'x1': 0.2, 'x2': 0.5, 'y1': 0.64, 'y2': 0.94},
        #                             #overwriteLabels = ['w/ pile-up correction loose', 'w/ pile-up correction medium', 'w/ pile-up correction tight', 'w/o pile-up correction loose', 'w/o pile-up correction medium', 'w/o pile-up correction tight'],
        #                             overwriteLabels = ['BDT' + ' loose', 'BDT' + ' medium', 'BDT' + ' tight', 'LLH' + ' loose', 'LLH' +  ' medium', 'LLH' + ' tight'],
        #                             #overwriteLabels = [algo1 + ' loose', algo1 + ' medium', algo1 + ' tight', algo2 + ' loose', algo2 +  ' medium', algo2 + ' tight'],
        #                             overwriteDrawOptions = ['P', 'P', 'P','P', 'P', 'P'],
        #                             ncol=2)
        formatter.addLegendToCanvas(canvas,
                                    legendOptions = {'x1': 0.2, 'x2': 0.8, 'y1': 0.64, 'y2': 0.94},
                                    #overwriteLabels = ['w/ pile-up correction loose', 'w/ pile-up correction medium', 'w/ pile-up correction tight', 'w/o pile-up correction loose', 'w/o pile-up correction medium', 'w/o pile-up correction tight'],
                                    #overwriteLabels = ['BDT' + ' loose', 'BDT' + ' medium', 'BDT' + ' tight', 'LLH' + ' loose', 'LLH' +  ' medium', 'LLH' + ' tight'],
                                    #overwriteLabels = [algo1 + ' loose', algo1 + ' medium', algo1 + ' tight', algo2 + ' loose', algo2 +  ' medium', algo2 + ' tight'],
                                    #overwriteLabels = [algo1Name + ' loose', algo1Name + ' medium', algo1Name + ' tight', algo2Name + ' loose', algo2Name +  ' medium', algo2Name + ' tight'],
                                    overwriteLabels = [algo1 + ' loose' + algo1Suffix, algo1 + ' medium' + algo1Suffix, algo1 + ' tight' + algo1Suffix, algo2 + ' loose' + algo2Suffix, algo2 +  ' medium' + algo2Suffix, algo2 + ' tight' + algo2Suffix],
                                    overwriteDrawOptions = ['P', 'P', 'P','P', 'P', 'P'],
                                    ncol=2,
                                    textsize = 0.025,
                                    ordering = [0,3,1,4,2,5])
        formatter.addLumiText(canvas,
                              lumi = 7653,
                              pos = {'x':0.6,'y':0.25},
                              splitLumiText = False)
        formatter.addText(canvas,
                          self.prongtext,# + ', ' + algo1,
                          pos = {'x': 0.6, 'y': 0.3})
        # formatter.addText(canvas,
        #                   #'EflowRec',#
        #                   'CellBased',
        #                   pos = {'x': 0.2, 'y': 0.25})
        
        # formatter.addATLASLabel(canvas,
        #                         description = 'internal',
        #                         pos= {'x': 0.7, 'y': 0.8},
        #                         size = 0.045,
        #                         offset = 0.11)
        algo1 = 'default' + algo1
        formatter.setTitle(self.plots[dep]['reference'][algo1][0],
                           '#mu' if dep == "mu" else "p^{gen}_{T,vis} [GeV]",
                           'x')
        
        canvas.Update()
        canvas.SaveAs(os.path.join(path, 'CombinedSignalEfficiency_%s_%s_%s_%s.eps' % (algo1, algo2, self.outputdirectory, dep)))
        
            
    def plotOverlap(self):
        canvas = plotter.plotHistograms(self.plotdict['BDT'] + self.plotdict['LLH_BDT'],
                                        plotOptions = [PO(marker_color = ROOT.kGreen,
                                                          marker_style = 22),
                                                       PO(marker_color = ROOT.kBlue,
                                                          marker_style = 21),
                                                       PO(marker_color = ROOT.kRed,
                                                          marker_style = 23),
                                                       PO(marker_color = ROOT.kGreen,
                                                          marker_style = 26),
                                                       PO(marker_color = ROOT.kBlue,
                                                          marker_style = 25),
                                                       PO(marker_color = ROOT.kRed,
                                                          marker_style = 32)],
                                        drawOptions = ['P','P','P','P','P','P'],
                                        canvasName = 'signal efficiency vs pt')

        formatter.setMinMax(self.signal_loose,
                            0.,
                            1.3,
                            'y')
        formatter.setMinMax(self.signal_loose,
                            10.,
                            160.,
                            'x')
        formatter.setTitle(self.signal_loose,
                           'p^{gen}_{T,vis} [GeV]',
                           'x')
        formatter.setTitle(self.signal_loose,
                           'signal efficiency',
                           'y')
        formatter.addLegendToCanvas(canvas,
                                    legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9},
                                    overwriteLabels = ['loose BDT', 'medium BDT', 'tight BDT','loose overlap', 'medium overlap', 'tight overlap'],
                                    overwriteDrawOptions = ['P', 'P', 'P','P', 'P', 'P'])
        formatter.addLumiText(canvas,
                              lumi = 20281.4,
                              pos = {'x':0.6,'y':0.8},
                              splitLumiText = False)
        formatter.addText(canvas,
                          self.prongtext + ', overlap BDT LLH',
                          pos = {'x': 0.6, 'y': 0.75})
        formatter.addATLASLabel(canvas,
                                description = 'internal',
                                pos= {'x': 0.6, 'y': 0.7},
                                size = 0.045,
                                offset = 0.11)

        canvas.Update()
        canvas.SaveAs(os.path.join(path, 'OverlapSignalEfficiency_%s_pt.eps' % (self.outputdirectory)))

    def dumpPlots(self,filename,variable):        
        fOut = ROOT.TFile.Open(filename,'update')
        for algo,plots in self.plotdict.items():
            try:
                fOut.cd('/')
            except ReferenceError:
                pass
            if(not fOut.FindKey(self.outputdirectory)):
                pdir = ROOT.gDirectory.mkdir(self.outputdirectory)
                pdir = fOut.GetDirectory(self.outputdirectory)
                fOut.cd(self.outputdirectory)
                if(not pdir.FindKey(algo)):
                    adir = ROOT.gDirectory.mkdir(algo)
                pdir.cd(algo)
                adir = pdir.GetDirectory(algo)
                for plot in plots:
                    adir.Delete("%s_%s;1" % (plot.GetName(),variable))
            plot.Write(plot.GetName()+'_'+variable)
            fOut.Close()
        
def main(argv):
    parser = argparse.ArgumentParser(description = 'plotter for signal efficiency')
    parser.add_argument('filelist', nargs = '+', help = 'input file (efficiency evaluation output)')
    parser.add_argument('--cfile', '-cf', nargs = '+', help = 'input file for comparison (efficiency evaluation output)')
    parser.add_argument('--cfile_label', '-cfl', default = 'compare', help = 'label for comparison (efficiency evaluation output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    parser.add_argument('--variable', '-v', help = 'variable to be replaced')
    parser.add_argument('--overlap', help = 'test overlap of llh and bdt')
    args = parser.parse_args()
    
    global path
    path = args.outdir
    mkdir(path)
    global overlap
    overlap = args.overlap

    efficiencyPlotter = EfficiencyPlotter(args.filelist)
    efficiencyPlotter.plotEfficiencies()
    if args.cfile:
        efficiencyPlotter.updateFile(args.cfile, 'compare')
        efficiencyPlotter.plotEfficiencies()
        #efficiencyPlotter.plotCombinedEfficiencies(args.cfile_label)
        efficiencyPlotter.foo(False, True, 'compare')
    else:
        #pass
        efficiencyPlotter.foo(True, False)
if __name__ == '__main__':
    main(sys.argv[1:])
