import argparse
import sys
import os
sys.path.append('../../TauCommon/macros')
import ROOT
from array import array
ROOT.PyConfig.IgnoreCommandLineOptions = True
from InputHandler import Reader
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from HistFormatter import plot_graph
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

class EfficiencyPlotter:
    def __init__(self, filelist):
        self.initialise(filelist)

        self.parseProngness()
        if (overlap!=None):
          self.algos = ['LLH','BDT','defaultLLH','defaultBDT','LLH_BDT']
        else:  
          # self.algos = ['LLH','BDT','defaultLLH','defaultBDT','defdefLLH','defrecLLH','defrecBDT']
          self.algos = ['LLH','BDT','defaultLLH','defaultBDT']
        self.workpoints = ['loose','medium','tight']
        self.rebin = 4
        # self.bins = array('d',[0,40,160,1000])

        self.plotdict = {}
        
    def initialise(self, filelist):
        self.filelist = filelist
        self.reader = Reader(filelist)
        
    def parseProngness(self):
        if '1p' in self.filelist[0]:
            self.prong = 1
            self.prongtext = "1 prong"
            self.outputdirectory = "1prong"
        elif '3p' in self.filelist[0]:
            self.prong = 3
            self.prongtext = "3 prong"
            self.outputdirectory = "3prong"
        else:
            self.prong = 3
            self.prongtext = "multi-prong"
            self.outputdirectory = "mprong"

    def getEfficiency(self,
                      hist,
                      norm = None):
        efficiency = hist.Clone()
        efficiency.Divide(norm)
        return efficiency

    def getHistPt(self,
                  wp,
                  algo):
        return self.reader.readHistogramAndMerge('h_passed_%s_%s_pt' % (algo, wp),
                                                 1.,
                                                 mergeOptions = self.reader.mergeOptions)[1]
                                    
    def getHistMu(self,
                  wp,
                  algo):
        return self.reader.readHistogramAndMerge('h_passed_%s_%s_mu' % (algo, wp),
                                                 1.,
                                                 mergeOptions = self.reader.mergeOptions)[1]
        
    def plotEfficiencyVsPt(self):
        hNorm = self.reader.readHistogramAndMerge('h_total_pt',
                                                  1.,
                                                  mergeOptions = self.reader.mergeOptions)[1]
        
        norm_hist = hNorm.Clone()
        norm_hist = norm_hist.Rebin(self.rebin)
        # norm_hist = norm_hist.Rebin(self.rebin,"new",self.bins)
        norm_hist.Draw()

        for algo in self.algos:
            self.background_loose, self.background_medium, self.background_tight = 0, 0, 0
            for wp in self.workpoints:
                exec("hist_%s = self.getHistPt('%s', '%s')" % (wp, wp, algo))
                exec("hist_%s=hist_%s.Rebin(self.rebin)" % (wp,wp))
                # exec("hist_%s=hist_%s.Rebin(self.rebin,'new_%s',self.bins)" % (wp,wp,wp))
                exec("self.background_%s = self.getEfficiency(hist_%s,norm_hist)" % (wp, wp))

            self.background_loose.SetName("loose")
            self.background_medium.SetName("medium")
            self.background_tight.SetName("tight")
            self.plotdict.update({algo : [self.background_loose, self.background_medium, self.background_tight]})

            canvas = plotter.plotHistograms([self.background_loose, self.background_medium, self.background_tight],
                                            plotOptions = [PO(marker_color = ROOT.kGreen,
                                                              marker_style = 22),
                                                           PO(marker_color = ROOT.kBlue,
                                                              marker_style = 21),
                                                           PO(marker_color = ROOT.kRed,
                                                              marker_style = 23)],
                                            drawOptions = ['P','P','P'],
                                            canvasName = 'background efficiency vs pt')
            formatter.setMinMax(self.background_loose,
                                0.001,
                                5,
                                'y')
            formatter.setMinMax(self.background_loose,
                                10.,
                                160.,
                                'x')
            formatter.setTitle(self.background_loose,
                               'p_{T} [GeV]',
                               'x')
            formatter.setTitle(self.background_loose,
                               'background efficiency',
                               'y')
            formatter.addLegendToCanvas(canvas,
                                        legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9},
                                        overwriteLabels = ['loose', 'medium', 'tight'],
                                        overwriteDrawOptions = ['P', 'P', 'P'])
            formatter.addLumiText(canvas,
                                  lumi = 20281.4,
                                  pos = {'x':0.6,'y':0.8},
                                  splitLumiText = False)
            formatter.addText(canvas,
                              self.prongtext + ', ' + algo,
                              pos = {'x': 0.6, 'y': 0.75})
            formatter.addATLASLabel(canvas,
                                    description = 'internal',
                                    pos= {'x':0.6,'y':0.7},
                                    size = 0.045,
                                    offset = 0.11)

            canvas.SetLogy()
            canvas.Update()
            canvas.SaveAs(os.path.join(path, 'BackgroundEfficiency_%s_%s_pt.eps' % (algo, self.outputdirectory)))

    def plotEfficiencyVsMu(self):
        norm_hist =  self.reader.readHistogramAndMerge('h_total_mu',
                                                       1.,
                                                       mergeOptions = self.reader.mergeOptions)[1]


        for algo in self.algos:
            self.background_loose, self.background_medium, self.background_tight = 0, 0, 0
            for wp in self.workpoints:        
                exec("hist_%s = self.getHistMu('%s', '%s')" % (wp, wp, algo))
                exec("self.background_%s = self.getEfficiency(hist_%s,norm_hist)" % (wp, wp))

            canvas = plotter.plotHistograms([self.background_loose, self.background_medium, self.background_tight],
                                            plotOptions = [PO(marker_color = ROOT.kGreen,
                                                                  marker_style = 22),
                                                           PO(marker_color = ROOT.kBlue,
                                                              marker_style = 21),
                                                           PO(marker_color = ROOT.kRed,
                                                              marker_style = 23)],
                                            drawOptions = ['P','P','P'],
                                            canvasName = 'background efficiency vs mu')
    
            formatter.setMinMax(self.background_loose,
                                0.001,
                                5,
                                'y')
            formatter.setMinMax(self.background_loose,
                                0.,
                                40.,
                                'x')
            formatter.setTitle(self.background_loose,
                               'background efficiency',
                               'y')
            formatter.setTitle(self.background_loose,
                               'mu',
                               'x')
            formatter.addLegendToCanvas(canvas,
                                        legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.7, 'y2': 0.9} ,
                                        overwriteLabels = ['loose', 'medium', 'tight'],
                                        overwriteDrawOptions = ['P', 'P', 'P'])
            formatter.addLumiText(canvas,
                                  lumi = 20281.4,
                                  pos = {'x':0.6,'y':0.8},
                                  splitLumiText = False)
            formatter.addText(canvas,
                              self.prongtext + ', ' + algo,
                              pos = {'x': 0.6, 'y': 0.75})
            formatter.addATLASLabel(canvas,
                                    description = 'internal',
                                    pos= {'x':0.6,'y':0.7},
                                    size = 0.045,
                                    offset = 0.11)

            canvas.SetLogy()
            canvas.Update()
            canvas.SaveAs(os.path.join(path, 'BackgroundEfficiency_%s_%s_mu.eps' % (algo, self.outputdirectory)))

    def plotCombinedEfficiencies(self):
        for algo in ['LLH','BDT']:
            canvas = plotter.plotHistograms(self.plotdict['default'+algo] + self.plotdict[algo],
                                            plotOptions = [PO(marker_color = ROOT.kGreen,
                                                              marker_style = 22),
                                                           PO(marker_color = ROOT.kBlue,
                                                              marker_style = 21),
                                                           PO(marker_color = ROOT.kRed,
                                                              marker_style = 23),
                                                           PO(marker_color = ROOT.kGreen,
                                                              marker_style = 26),
                                                           PO(marker_color = ROOT.kBlue,
                                                              marker_style = 25),
                                                           PO(marker_color = ROOT.kRed,
                                                              marker_style = 32)],
                                            drawOptions = ['P','P','P','P','P','P'],
                                            canvasName = 'background efficiency vs pt')

            formatter.setMinMax(self.background_loose,
                                0.001,
                                5,
                                'y')
            formatter.setMinMax(self.background_loose,
                                10.,
                                160.,
                                'x')
            formatter.setTitle(self.background_loose,
                               'p_{T} [GeV]',
                               'x')
            formatter.setTitle(self.background_loose,
                               'background efficiency',
                               'y')
            formatter.addLegendToCanvas(canvas,
                                        legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9},
                                        overwriteLabels = ['default loose', 'default medium', 'default tight','loose', 'medium', 'tight'],
                                        overwriteDrawOptions = ['P', 'P', 'P','P', 'P', 'P'])
            formatter.addLumiText(canvas,
                                  lumi = 20281.4,
                                  pos = {'x':0.6,'y':0.8},
                                  splitLumiText = False)
            formatter.addText(canvas,
                              self.prongtext + ', ' + algo,
                              pos = {'x': 0.6, 'y': 0.75})
            formatter.addATLASLabel(canvas,
                                    description = 'internal',
                                    pos= {'x': 0.6, 'y': 0.7},
                                    size = 0.045,
                                    offset = 0.11)

            canvas.SetLogy()
            canvas.Update()
            canvas.SaveAs(os.path.join(path, 'CombinedBackgroundEfficiency_%s_%s_pt.eps' % (algo, self.outputdirectory)))

    def plotOverlap(self):
        canvas = plotter.plotHistograms(self.plotdict['BDT'] + self.plotdict['LLH_BDT'],
                                        plotOptions = [PO(marker_color = ROOT.kGreen,
                                                          marker_style = 22),
                                                       PO(marker_color = ROOT.kBlue,
                                                          marker_style = 21),
                                                       PO(marker_color = ROOT.kRed,
                                                          marker_style = 23),
                                                       PO(marker_color = ROOT.kGreen,
                                                          marker_style = 26),
                                                       PO(marker_color = ROOT.kBlue,
                                                          marker_style = 25),
                                                       PO(marker_color = ROOT.kRed,
                                                          marker_style = 32)],
                                        drawOptions = ['P','P','P','P','P','P'],
                                        canvasName = 'background efficiency vs pt')

        formatter.setMinMax(self.background_loose,
                            0.001,
                            5,
                            'y')
        formatter.setMinMax(self.background_loose,
                            10.,
                            160.,
                            'x')
        formatter.setTitle(self.background_loose,
                           'p_{T} [GeV]',
                           'x')
        formatter.setTitle(self.background_loose,
                           'background efficiency',
                           'y')
        formatter.addLegendToCanvas(canvas,
                                    legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9},
                                    overwriteLabels = ['loose BDT', 'medium BDT', 'tight BDT','loose overlap', 'medium overlap', 'tight overlap'],
                                    overwriteDrawOptions = ['P', 'P', 'P','P', 'P', 'P'])
        formatter.addLumiText(canvas,
                              lumi = 20281.4,
                              pos = {'x':0.6,'y':0.8},
                              splitLumiText = False)
        formatter.addText(canvas,
                          self.prongtext + ', overlap BDT LLH',
                          pos = {'x': 0.6, 'y': 0.75})
        formatter.addATLASLabel(canvas,
                                description = 'internal',
                                pos= {'x': 0.6, 'y': 0.7},
                                size = 0.045,
                                offset = 0.11)

        canvas.SetLogy()
        canvas.Update()
        canvas.SaveAs(os.path.join(path, 'OverlapBackgroundEfficiency_%s_pt.eps' % (self.outputdirectory)))

    def dumpPlots(self,filename,variable):        
        for algo,plots in self.plotdict.items():
            fOut = ROOT.TFile.Open(filename,'update')
            if(not fOut.FindKey(self.outputdirectory)):
                pdir = ROOT.gDirectory.mkdir(self.outputdirectory)
            pdir = fOut.GetDirectory(self.outputdirectory)
            fOut.cd(self.outputdirectory)
            if(not pdir.FindKey(algo)):
                adir = ROOT.gDirectory.mkdir(algo)
            pdir.cd(algo)
            adir = pdir.GetDirectory(algo)
            for plot in plots:
                adir.Delete("%s_%s;1" % (plot.GetName(),variable))
                plot.Write(plot.GetName()+'_'+variable)
            fOut.Close()
                
def main(argv):
    parser = argparse.ArgumentParser(description = 'plotter for background efficiency')
    parser.add_argument('filelist', nargs = '+', help = 'input file (efficiency evaluation output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    parser.add_argument('--variable', '-v', help = 'variable to be replaced')
    parser.add_argument('--overlap', help = 'test overlap of llh and bdt')
    args = parser.parse_args()


    global path
    path = args.outdir
    
    global overlap
    overlap = args.overlap

    efficiencyPlotter = EfficiencyPlotter(args.filelist)
    efficiencyPlotter.plotEfficiencyVsPt()
    efficiencyPlotter.plotEfficiencyVsMu()
    efficiencyPlotter.plotCombinedEfficiencies()
    if (overlap!=None):
        efficiencyPlotter.plotOverlap()
    if (args.variable!=None):
        temppath = path.replace('/'+args.variable+'/','/')
        filename = temppath+'VariableExclusionBackground.root'
        efficiencyPlotter.dumpPlots(filename,args.variable)
    
if __name__ == '__main__':
    main(sys.argv[1:])
