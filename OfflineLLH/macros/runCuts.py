import os
from ShellUtils import mkdir

variableset_1p = ["CentFrac0102",
                  "FTrk02",
                  "pi0_vistau_m",
                  "ptRatio",
                  "TrkAvgDist",
                  "pi0_n",
                  "IpSigLeadTrk",
                  "NTracksdrdR"]
variableset_mp = ["CentFrac0102",
                  "FTrk02",
                  "pi0_vistau_m",
                  "ptRatio",
                  "TrkAvgDist",
                  "pi0_n",
                  "MassTrkSys",
                  "DrMax",
                  "TrFligthPathSig"]

for prong in ['1','m']:
    for variable in eval('variableset_%sp' % prong):
        mkdir('../out/cuts/current_result/'+variable)
        os.system('python DetermineCuts.py ../out/EfficiencyOptimisationCycle/current_result/'+variable+'/EfficiencyOptimisationCycle.mc.Signal.'+prong+'p.root -o ../out/cuts/current_result/'+variable+'/')
