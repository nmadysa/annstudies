import argparse
import sys
import os
import ROOT

sys.path.append('../../TauCommon/macros/')

ROOT.gROOT.SetBatch(True)

def makePlots(rf,path):

    np = ""
    if '1p' in rf.GetName():
        np = '1p'
    else:
        np = '3p'
    
    for process in ['S','B']:
        c = ROOT.TCanvas("c","c",5200,1700)
        h = rf.Get("CorrelationMatrix"+process)
        c.cd()
        h.Draw("COLZTEXT")
        c.SetBottomMargin(0.3)
        c.SetLeftMargin(0.3)
        c.SetRightMargin(0.3)
        c.SaveAs(os.path.join(path,'CorrelationMatrices_'+process+'_'+np+'.eps'))
        c.Print(os.path.join(path,'CorrelationMatrices_'+process+'_'+np+'.ps'))
        del h

def main(argv):

    parser = argparse.ArgumentParser(description = 'Plotter for Correlation matrices')
    parser.add_argument('file', help = 'input file (TMVA output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    args = parser.parse_args()

    rf = ROOT.TFile.Open(args.file, 'READ')
    path = args.outdir

    makePlots(rf,path)

if __name__ == '__main__':
    main(sys.argv[1:])
