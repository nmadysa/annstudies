import argparse
import sys
sys.path.append('../../TauCommon/macros/')
import os
from array import array
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
import HistFormatter as hf
from InputHandler import Reader

ROOT.gROOT.SetBatch(True)
                            
class CutCalculator:
    def __init__(self, filelist):
        self.filelist = filelist
        self.reader = Reader(filelist)
        #self.rf = ROOT.TFile.Open(rfName, 'READ')
        self.parseProngness()
        self.initialiseEfficiencyTargets()
        self.initialisePtBins()
        self.getNormalisation()
        self.methods = ['LLH','BDT','default_LLH','default_BDT']
        self.recoVsTruthPtHist = self.reader.readHistogramAndMerge('h_TruthpT_OfflinepT', 1., mergeOptions = self.reader.mergeOptions)[0]['Signal']

    def initialisePtBins(self):
        # self.ptBins =[i for i in range(25,101)]
        # self.ptBins.extend([151, 201, 1001])
        self.ptBins = [i for i in range(15,25,10)]
        self.ptBins.extend([i for i in range(25,100)])
        self.ptBins.extend([i for i in range(100,200,10)])
        self.ptBins.extend([i for i in range(200,1000,200)])
        
    def parseProngness(self):
        if '1p' in self.filelist[0]:
            self.prong = 1
            self.outputdirectory = "1prong"
        else:
            self.prong = 3
            self.outputdirectory = "3prong"    
            
    def initialiseEfficiencyTargets(self):
        self.efficiencyTargets = {'0' : 0,
        
                                  '001' : 0.01,
                                  '002' : 0.02,
                                  '003' : 0.03,
                                  '004' : 0.04,
                                  '005' : 0.05,
                                  '006' : 0.06,
                                  '007' : 0.07,
                                  '008' : 0.08,
                                  '009' : 0.09,
                                
                                  '010' : 0.1,
                                  '011' : 0.11,
                                  '012' : 0.12,
                                  '013' : 0.13,
                                  '014' : 0.14,
                                  '015' : 0.15,
                                  '016' : 0.16,
                                  '017' : 0.17,
                                  '018' : 0.18,
                                  '019' : 0.19,
                                  
                                  '020' : 0.2,
                                  '021' : 0.21,
                                  '022' : 0.22,
                                  '023' : 0.23,
                                  '024' : 0.24,
                                  '025' : 0.25,
                                  '026' : 0.26,
                                  '027' : 0.27,
                                  '028' : 0.28,
                                  '029' : 0.29,
                                  
                                  '030' : 0.3,
                                  '031' : 0.31,
                                  '032' : 0.32,
                                  '033' : 0.33,
                                  '034' : 0.34,
                                  '035' : 0.35,
                                  '036' : 0.36,
                                  '037' : 0.37,
                                  '038' : 0.38,
                                  '039' : 0.39,
                                  
                                  '040' : 0.4,
                                  '041' : 0.41,
                                  '042' : 0.42,
                                  '043' : 0.43,
                                  '044' : 0.44,
                                  '045' : 0.45,
                                  '046' : 0.46,
                                  '047' : 0.47,
                                  '048' : 0.48,
                                  '049' : 0.49,
                                  
                                  '050' : 0.5,
                                  '051' : 0.51,
                                  '052' : 0.52,
                                  '053' : 0.53,
                                  '054' : 0.54,
                                  '055' : 0.55,
                                  '056' : 0.56,
                                  '057' : 0.57,
                                  '058' : 0.58,
                                  '059' : 0.59,
                                                                    
                                  '060' : 0.6,
                                  '061' : 0.61,
                                  '062' : 0.62,
                                  '063' : 0.63,
                                  '064' : 0.64,
                                  '065' : 0.65,
                                  '066' : 0.66,
                                  '067' : 0.67,
                                  '068' : 0.68,
                                  '069' : 0.69,

                                  '070' : 0.7,
                                  '071' : 0.71,
                                  '072' : 0.72,
                                  '073' : 0.73,
                                  '074' : 0.74,
                                  '075' : 0.75,
                                  '076' : 0.76,
                                  '077' : 0.77,
                                  '078' : 0.78,
                                  '079' : 0.79,

                                  '080' : 0.8}
        

    def getProjection(self,
                      inputHist,
                      bin,
                      offset):
        if bin+offset < len(self.ptBins):
            histProjection = inputHist.ProjectionY("_py",
                                                   self.ptBins[bin],
                                                   self.ptBins[bin+offset])
            if histProjection.Integral() !=0:
                pt = (inputHist.GetXaxis().GetBinLowEdge(self.ptBins[bin])+1+inputHist.GetXaxis().GetBinUpEdge(self.ptBins[bin+offset]))/2
                lowBin = int(inputHist.GetXaxis().GetBinLowEdge(self.ptBins[bin])+1)
                upBin = int(inputHist.GetXaxis().GetBinUpEdge(self.ptBins[bin+offset]))
                return histProjection, pt, bin+offset, lowBin, upBin
            else:
                return self.getProjection(inputHist,
                                          bin,
                                          offset + 1)
        else:
            return None
        
    def getEfficiency(self,
                      hist,
                      norm):
        for j in range(hist.GetNbinsX() + 1, 0, -1):
            eff = hist.Integral(j, -1) / norm
            for key, targetEff in self.efficiencyTargets.items():
                if eff < targetEff:
                    continue
                if len(self.values[key]) == len(self.values['pt']):
                    continue
                self.values[key].append(hist.GetXaxis().GetBinCenter(j))
                if key == '80':
                    return

    def getRecoPt(self,
                  lowBin,
                  upBin):
        
        recoVsTruthPtHistProjection = self.recoVsTruthPtHist.ProjectionY("_px" + str(lowBin),
                                                                         lowBin,
                                                                         upBin)
        return recoVsTruthPtHistProjection.GetMean(),recoVsTruthPtHistProjection.GetRMS()
            
    def getCutValues(self,
                     method):
        score = self.reader.readHistogramAndMerge("h_pT_"+method+"Score",
                                                  1.,
                                                  mergeOptions = self.reader.mergeOptions)[0]['Signal']
        self.values = {'pt': [],
                       'meanrecopt' : [],
                       'rmsrecopt' : []}
        for key in self.efficiencyTargets.keys():
            self.values[key] = []
        ptBin = 0
        while ptBin in range(len(self.ptBins)):
            if self.getProjection(score,
                                  ptBin,
                                  offset = 1):
                h_score,pt,ptBin,lowBin,upBin = self.getProjection(score,
                                                                   ptBin,
                                                                   offset = 1)
                self.values['pt'].append(pt)
                meanrecopt,rmsrecopt = self.getRecoPt(lowBin, upBin)
                self.values['meanrecopt'].append(meanrecopt)
                self.values['rmsrecopt'].append(rmsrecopt)
                norm = sum(self.hNorm.GetBinContent(bin) for bin in range(lowBin, upBin+1))
                self.getEfficiency(h_score,
                                   norm)
            else:
                return
            
    def getPlots(self,
                 method):
        self.getCutValues(method)
        self.values['pt'].insert(0,0)
        self.values['meanrecopt'].insert(0,0)
        self.values['rmsrecopt'].insert(0,0)

        for key in self.efficiencyTargets.keys():
            try:
                self.values[key].insert(0,self.values[key][0])
            except IndexError:
                self.values[key].insert(0,100)                

        for key, value in self.values.items():
            exec("array_%s = array('f', " % key + str(value) + ")")
        nEntries = len(value)
        cutPlots = []
        for key in self.efficiencyTargets.keys():
            exec("graph_%s = ROOT.TGraph(%i ,array_meanrecopt,array_%s)" % (key, nEntries, key))
            # exec("graph_%s = ROOT.TGraph(%i ,array_pt,array_%s)" % (key, nEntries, key))
            exec('graph_%s.Draw("")' % key)
            exec('graph_%s.SetName("%s")' % (key, key))
            exec("graph_%s.SetTitle('%s %s cut for %i prong')" % (key, key, method, self.prong))
            exec("graph_%s.GetXaxis().SetTitle('reco pt')" % key)
            # exec("graph_%s.GetXaxis().SetTitle('pt')" % key)
            cutPlots.append(eval("graph_%s" % key))

        for mean in ['mean','rms']:
            exec("g_truth_%sRecoPt = ROOT.TGraph(%i, array_pt, array_%srecopt)" % (mean, nEntries, mean))            
            exec("g_truth_%sRecoPt.Draw('*')" % mean)
            exec("g_truth_%sRecoPt.SetName('truth_vs_%sReco_pt')" % (mean,mean))
            exec("g_truth_%sRecoPt.SetTitle('truth vs %s reco pt for %i prong')" % (mean,mean,self.prong))
            exec("g_truth_%sRecoPt.GetXaxis().SetTitle('truth pt')" % mean)
            exec("g_truth_%sRecoPt.GetYaxis().SetTitle('%s reco pt')" % (mean,mean))
        controlPlots = g_truth_meanRecoPt,g_truth_rmsRecoPt

        return cutPlots, controlPlots
        
    def getNormalisation(self):
        hNorm = self.reader.readHistogramAndMerge("h_truthPt",
                                                  1., mergeOptions = self.reader.mergeOptions)[0]['Signal']
        self.hNorm = hNorm.ProjectionY("norm",
                                       self.prong + 1,
                                       self.prong + 1,
                                       0, -1)

    def writeCutFile(self, method, plots):
        filename = path+"cuts_"+method+".root"
        fOut = ROOT.TFile.Open(filename,'update')
        if(not fOut.FindKey(self.outputdirectory)):
            adir = ROOT.gDirectory.mkdir(self.outputdirectory)
        fOut.cd(self.outputdirectory)
        adir = fOut.GetDirectory(self.outputdirectory)

        for key in self.efficiencyTargets.keys():
            adir.Delete("%s;1" % key)
            
        for plot in plots:
            plot.Write()
        fOut.Close()

    def writeControlFile(self, plots):
        filename = path+"controlPlots.root"
        fOut = ROOT.TFile.Open(filename,'update')
        if(not fOut.FindKey(self.outputdirectory)):
            adir = ROOT.gDirectory.mkdir(self.outputdirectory)
        fOut.cd(self.outputdirectory)
        adir = fOut.GetDirectory(self.outputdirectory)
        adir.Delete("truth_vs_meanReco_pt;1")
        adir.Delete("truth_vs_rmsReco_pt;1")
        for plot in plots:
            plot.Write()
        fOut.Close()
        
    def getAllCuts(self):
        for method in self.methods:
            cutPlots, controlPlots = self.getPlots(method)
            self.writeCutFile(method,
                              cutPlots)
            self.writeControlFile(controlPlots)
            

def main(argv):

    parser = argparse.ArgumentParser(description = 'script to determine cuts')
    parser.add_argument('filelist', nargs='+', help = 'input files (efficiency optimisation output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    args = parser.parse_args()

    global path
    path = args.outdir

    cutCalculator = CutCalculator(args.filelist)
    cutCalculator.getAllCuts()
    
if __name__ == '__main__':
    main(sys.argv[1:])
