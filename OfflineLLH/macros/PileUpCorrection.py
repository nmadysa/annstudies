import argparse
import sys
sys.path.append('../../TauCommon/macros/')
import os

from InputHandler import Reader
from HistTools import Plotter,Formatter
from HistFormatter import PlotOptions as PO
from CommonTools import Writer
from RootFileHelper import GetObjectsOfType
plotter=Plotter()
formatter=Formatter()

import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.PyConfig.IgnoreCommandLineOptions = True

class CorrectionCalculator:
    def __init__(self):
        self.corrections = {}

    def getProfile(self,hist):
        self.profile=hist.ProfileX(hist.GetName()+"_x")
        
    def getMuCorrection(self):
        if self.profile.GetEntries() == 0:
            self.corrections[self.profile.GetName().replace('_x', '')] = 0
            self.fit = None
            return 
        self.fit=self.profile.Fit("pol1","QSFEMIN")
        # if self.profile.GetName().replace('_x', '') == "panTauCellBased_massNeutSys02_3P":
        #     print self.fit.Status()
        #     print self.fit.Parameter(1)
        #     raw_input()
        if self.fit.Status() == 0:
            self.corrections[self.profile.GetName().replace('_x', '')] = 0
            self.fit = None
            return          
        self.corrections[self.profile.GetName().replace('_x', '')] = self.fit.Parameter(1)
        
    def applyCorrection(self):
        self.corrProfile = ROOT.TProfile(self.profile.GetName() + "_corr", "",
                                         self.profile.GetNbinsX(), 0, self.profile.GetNbinsX())
        if self.fit is None:
            corrTerm = 0
        else:
            corrTerm = self.fit.Parameter(1)
        for bin in range(0,self.profile.GetNbinsX()-1):
            corrVar = self.profile.GetBinContent(bin) - corrTerm * self.profile.GetBinCenter(bin)
            self.corrProfile.Fill(self.profile.GetBinCenter(bin),corrVar)
    
    def plotProfiles(self):
        comProfile=plotter.plotHistograms([self.profile,self.corrProfile],
                                          plotOptions=[PO(marker_color=ROOT.kBlack,
                                                          marker_style=20),
                                                       PO(marker_color=ROOT.kRed,
                                                          marker_style=20)],
                                          drawOptions=['P','P'],
                                          canvasName=self.profile.GetName())
        formatter.setMinMax(self.profile,
                            5.,
                            35.,
                            'x')
        # formatter.setMinMax(self.profile,
        #                     self.profile.GetBinContent(20)-0.7*abs(self.profile.GetBinContent(5)-self.profile.GetBinContent(35)),
        #                     self.profile.GetBinContent(20)+0.7*abs(self.profile.GetBinContent(5)-self.profile.GetBinContent(35)),
        #                     'y')
        formatter.setMinMax(self.profile,
                            0.,
                            0.095,
                            'y')
        formatter.setTitle(self.profile,
                           '#mu',
                           'x')
        # formatter.setTitle(self.profile,
        #                    self.profile.GetName()[16:-5],
        #                    'y')
        formatter.setTitle(self.profile,
                           "<R_{cal}^{iso}>",
                           'y')

        formatter.addLegendToCanvas(comProfile,
                                    legendOptions = {'x1': 0.2, 'x2': 0.5, 'y1': 0.7, 'y2': 0.9},
                                    overwriteLabels = ['w/o correction', 'w/ correction'],
                                    overwriteDrawOptions = ['P', 'P'],
                                    textsize = 0.04)
        formatter.addText(comProfile, "1-prong", {'x':0.6,'y':0.87})
        formatter.addLumiText(comProfile,
                              lumi = 7653,
                              pos = {'x':0.6,'y':0.8},
                              splitLumiText = False)
        # formatter.addATLASLabel(comProfile,
        #                         description = 'Internal',
        #                         pos= {'x': 0.6, 'y': 0.85},
        #                         size = 0.045,
        #                         offset = 0.11)        
        comProfile.Update()
        writer.dumpCanvas(comProfile,image=self.profile.GetName().replace('_x', '') + '.eps')

    def dump(self,out):
        f = ROOT.TFile(out+'pileupcorrections.root', 'RECREATE')
        h = ROOT.TH1F("h_pileupcorr", "", len(self.corrections.keys()), 0., len(self.corrections.keys()))
        bin = 0
        for key, val in self.corrections.items():
            h.SetBinContent(bin, val)
            h.GetXaxis().SetBinLabel(bin, key)
            bin += 1
        f.cd()
        h.Write()
        f.Close()
        
def main(argv):
    parser = argparse.ArgumentParser(description = 'calculator for pileup corrections')
    parser.add_argument('filelist',nargs='+',help='input filelist (llh output)')
    parser.add_argument('--outdir','-o',default=os.curdir,help='output directory')
    args = parser.parse_args()

    reader = Reader(args.filelist)
    global writer
    writer = Writer(args.outdir)
    allHistNames = GetObjectsOfType(ROOT.TFile.Open(args.filelist[0]), "TH2F")
    correctionCalculator = CorrectionCalculator()
    for histName in allHistNames.keys():
        if not histName.startswith("VariablePlotter/mu_panTau"):
            continue
        mc_hists, data = reader.readHistogramAndMerge(histName,
                                                      1,
                                                      mergeOptions = reader.mergeOptions)
        signal = mc_hists['Signal']
        signal.SetName(histName.replace("VariablePlotter/mu_", ""))
        correctionCalculator.getProfile(signal)
        correctionCalculator.getMuCorrection()
        correctionCalculator.applyCorrection()
        correctionCalculator.plotProfiles()
        correctionCalculator.dump(args.outdir)
    
if __name__ == '__main__':
    main(sys.argv[1:])
