
"""
FileHandler

Module for retrieving histograms and graphs saved to ROOT files.
Also has methods from inspecting ROOT files like getting a list
of existing histograms or TTrees.
"""

#------------------------------------------------------------------------------

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
import Utilities
from Samples import Sample

_logger = Utilities._logger

#------------------------------------------------------------------------------
# FileHandler Class
#------------------------------------------------------------------------------
class FileHandler(object):
    _unique_index = 0 # static class data 
#______________________________________________________________________________
    def __init__(self,filename=None,state='READ',ignoreName=False):
        """
        Initializes an instance of HistGetter with some input files given
        by files.  files can be a single file or a list.
        """
        self.file = None
        self.sample = None
        self._hasFile = False
        try:
            if filename:
                self.file = Utilities.openROOTFile(filename,state)
                self._hasFile = True
                if not ignoreName:
                    self.sample = Utilities.getSampleFromFileName(filename)
        except Utilities.DefaultError as e:
            print str(e)

#______________________________________________________________________________
    def make_copy(self,obj): 
        """
        A helper function to create copy of an object
        """
        try:
            newobj = obj.__class__(obj) # bug?: doesn't have unique name
        except:
            newobj = obj.Clone()
        FileHandler._unique_index += 1
        return newobj

#______________________________________________________________________________
    def get(self,name,scale=0,ignore=False,useNormalisedXS=False):
        """
        Returns a copy of the ROOT object with name, scaled to the given luminosity in pb^{-1}
        scale = -1 -> normalise
        scale = 0  -> unmodified
        scale > 0  -> scaled to luminosity
        """
        obj = None
        if not self._hasFile:
            _logger.error("no file associated to HistGetter")
            return None
        _logger.info("looking for object '%s' in file '%s'",name,self.file.GetName())
        self.file.cd()
        temp = self.file.Get(name)
        if not temp:
            _logger.error("did not find object with name '%s' in file '%s'",name,self.file.GetName())
            return None
        obj = self.make_copy(temp)
        if obj.Class().InheritsFrom("TH1"):
            if ignore:
                return obj
            if scale == -1:
                Utilities.normaliseHistogram(obj)
            if scale > 0 and not self.sample.isData():
                Utilities.scaleHistogram(obj,self.sample,scale,self.getProcessedEvents(),useSignalXS=useNormalisedXS)
            #if self.sample.isData():
            if (self.getProcessedEvents())[0] != self.sample.generatedEvents:
                _logger.info("'%s': number of processed events %.0f does not match expected number of generated events %.0f",self.sample.name,(self.getProcessedEvents())[0],self.sample.generatedEvents)
            obj.sample = self.sample
            obj.SetDirectory(0)
            
        return obj

#______________________________________________________________________________
    def getProcessedEvents(self):
        """
        return the number of processed events in the format: (total, total weightd)
        """
        if not self._hasFile:
            return (0,0)
        cutflow = 'SkimSelection/h_cut_flow'
        cutflow_raw = 'SkimSelection/h_cut_flow_raw'
        self.file.cd()
        h = self.file.Get(cutflow)
        if not h:
            _logger.info("did not find cutflow histogram '%s' in file '%s'",cutflow,self.file.GetName())
            _logger.info("try cutflow histogram 'BaselineSelection/h_cut_flow'")
            h = self.file.Get("BaselineSelection/h_cut_flow")
            if not h:
                _logger.error("did not find cutflow histogram 'BaselineSelection/h_cut_flow' in file '%s'",self.file.GetName())
                return (0,0)
        h_raw = self.file.Get(cutflow_raw)
        if not h_raw:
            _logger.info("did not find cutflow histogram '%s' in file '%s'",cutflow_raw,self.file.GetName())
            _logger.info("try cutflow histogram 'BaselineSelection/h_cut_flow_raw'")
            h_raw = self.file.Get("BaselineSelection/h_cut_flow_raw")
            if not h_raw:
                _logger.error("did not find cutflow histogram 'BaselineSelection/h_cu_flow_raw' in file '%s'",self.file.GetName())
                return (0,0)
        # return total, total weighted
        return (h_raw.GetBinContent(1),h.GetBinContent(1))
    
#______________________________________________________________________________
    def get_obj_names(self,type,strict=False):
        obj_names = {}
        if not self._hasFile:
            _logger.error("no file associated")
            return obj_names
        #----------------------------------------------------------------------
        def traverseDir(directory,objects):
            # check objects corresponding to the keys for their type
            for key in directory.GetListOfKeys():
                _logger.debug("looking for key '%s'",key.GetName())
                obj = directory.Get(key.GetName())
                if (strict and obj.Class().GetName() == type) or (not strict and obj.Class().InheritsFrom(type)):
                    _logger.debug("found object of type '%s' at '%s'",obj.Class().GetName(),obj.GetName())
                    objects[directory.GetPath().rstrip('/') + '/' + obj.GetName()] = obj
                # if object is a directory -> scan this as well
                elif isinstance(obj,ROOT.TDirectory):
                    traverseDir(obj,objects)
            return objects
        #----------------------------------------------------------------------
        basedir = self.file.GetPath().rstrip('/') + '/'
        objects = traverseDir(self.file,obj_names)

        return {k.replace(basedir,""):v for k,v in objects.items()}
        return {}
#______________________________________________________________________________
    def get_tree_entries(self,trees = []):
        if not self._hasFile:
            _logger.error("no file associated")
            return None
        if not trees:
            trees = get_obj_names("TTree").keys()
        if not isinstance(trees,list):
            trees = [trees]
        t_entries = {}
        if trees:
            for treename in trees:
                t = self.get(treename)
                if t:
                    _logger.info("found tree '%s' with %.0f entries",treename,t.GetEntries())
                    t_entries[t.GetName()] = t.GetEntries()
                else:
                    _logger.error("tree '%s' not found in file '%s'",treename,self.file.GetName())
        return t_entries

#______________________________________________________________________________
    def close(self):
        self.file.Close()
        self.file = None


#----------------------------------------
def __GetObjectsOfTypeWrapper(argv):
    getter = FileHandler(argv.filename)
    if getter._hasFile:
        obj = getter.get_obj_names(argv.type,argv.strict)
        for t,o in obj.items():
            print "found object of type (%s) at: %s" %(o.Class().GetName(),t)

#---------------------------------------
def __PrintTreeEntriesWrapper(argv):
    pass
        
def main(argv):
    import argparse,logging
    parser = argparse.ArgumentParser(description='FileHandler - usefull methods for examining ROOT files')
    parser.add_argument('--debug',nargs='?',dest='level',choices=['DEBUG','INFO','WARNING','CRITICAL','ERROR'],default='ERROR',help='logging level')
    subparser = parser.add_subparsers(title='commands',description="These commands specify possible running modes. For details type: 'python RootFileHelper.py <command> -h'",
                                        help='possible execution modes')
    # parameters for looking for objects
    parser_objects = subparser.add_parser('objects',help='scan ROOT file for objects of given type')
    parser_objects.add_argument('filename',help='ROOT file')
    parser_objects.add_argument('type',help='object type')
    parser_objects.add_argument('--strict',default=False,action='store_true',help='use only objects of exactly the given type')
    parser_objects.set_defaults(func=__GetObjectsOfTypeWrapper)
    # parameters for getting number of tree entries
    parser_entries = subparser.add_parser('entries',help='scan list of root files for number of entries in specified tree(s)')
    parser_entries.add_argument('-t',nargs='+',dest='trees',metavar='tree',help="treenames")
    parser_entries.add_argument('filelist',nargs='+',help="filelist")
    parser_entries.set_defaults(func=__PrintTreeEntriesWrapper)
    args = parser.parse_args()
    _logger.setLevel(eval('logging.' + args.level))
    args.func(args)

if __name__ == "__main__":
    import sys
    _logger = Utilities._logger
    main(sys.argv[1:])
