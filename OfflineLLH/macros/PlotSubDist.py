import argparse
import sys
import os
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

sys.path.append('../../TauCommon/macros/')
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from Configurator import Configurator
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

class VariablePlotter:
    def __init__(self):
        self.variables = ["CentFrac0102",
                          "FTrk02",
                          "TrFlightPathSig",
                          "IpSigLeadTrk",
                          "MassTrkSys",
                          "TrkAvgDist",
                          "DrMax",
                          "NTracksdrdR",
                          "ptRatio",
                          "pi0_n",
                          "pi0_vistau_m",
                          "RCal02",
                          "RCal04",
                          "CoreFrac0204",
                          "IsoFrac0104",
                          "ptRatio02"]
        self.config = Configurator('configs/def_sub_variables.yml').__dict__
        
    def getPosition(self,
                    ltype,
                    config):
        position = "{"
        if config.has_key(ltype):
            it = config[ltype].__iter__()
            for x in config[ltype]:
                for key,value in x.items():
                    position += "\"%s\": %s, " % (key, value)
        position = position.rstrip(" ,") + "}"
        return position

    def plotAlgoComp(self,
                         tau,
                     pantau,
                     variable,
                     prong):
        canvas = ROOT.TCanvas("c","c",800,700)
        canvas.cd()

        if tau.GetMaximum()>pantau.GetMaximum():
            tau.Draw("P")
            pantau.Draw("sameP")
            formatter.addLegendToCanvas(canvas,
                                        legendOptions = {'x1': 0.6, 'x2': 0.9, 'y1': 0.7, 'y2': 0.9},
                                        overwriteLabels = ['tau', 'pantau'])
            formatter.setTitle(tau,
                               variable,
                               'x')        
        else:
            pantau.Draw("P")
            tau.Draw("sameP")
            formatter.addLegendToCanvas(canvas,
                                        legendOptions = {'x1': 0.6, 'x2': 0.9, 'y1': 0.7, 'y2': 0.9},
                                        overwriteLabels = ['pantau', 'tau'])                         
            formatter.setTitle(pantau,
                               variable,
                               'x')        
        tau.SetMarkerColor(1)
        tau.SetMarkerStyle(20)
        pantau.SetMarkerColor(2)
        pantau.SetMarkerStyle(20)

        formatter.addText(canvas,
                          prong+'prong',
                          pos = {'x': 0.2, 'y': 0.9})

        canvas.Update()
        return canvas

    def plotProcessComp(self,
                        sig,
                        bg,
                        variable,
                        prong):
        canvas = ROOT.TCanvas("c","c",800,700)
        canvas.cd()

        if sig.GetMaximum()>bg.GetMaximum():
            sig.Draw("P")
            bg.Draw("sameP")
            formatter.addLegendToCanvas(canvas,
                                        legendOptions = {'x1': 0.6, 'x2': 0.9, 'y1': 0.7, 'y2': 0.9},
                                        overwriteLabels = ['signal', 'background'])
            formatter.setTitle(sig,
                               variable,
                               'x')        
        else:
            bg.Draw("P")
            sig.Draw("sameP")
            formatter.addLegendToCanvas(canvas,
                                        legendOptions = {'x1': 0.6, 'x2': 0.9, 'y1': 0.7, 'y2': 0.9},
                                        overwriteLabels = ['bg', 'sig'])                         
            formatter.setTitle(bg,
                               variable,
                               'x')        
        sig.SetMarkerColor(1)
        sig.SetMarkerStyle(20)
        bg.SetMarkerColor(2)
        bg.SetMarkerStyle(20)

        formatter.addText(canvas,
                          prong+'prong',
                          pos = {'x': 0.2, 'y': 0.9})

        canvas.Update()
        return canvas

    def plotComp(self,
                 tau_sig,
                 tau_bg,
                 pantau_sig,
                 pantau_bg,
                 variable,
                 prong,
                 config):
        canvas = plotter.plotHistograms([tau_sig,tau_bg,pantau_sig,pantau_bg],
                                        plotOptions = [PO(line_color = ROOT.kRed,
                                                          fill_color = ROOT.kRed,
                                                          fill_style = 3354),
                                                       PO(line_color = ROOT.kBlack,
                                                          fill_color = ROOT.kBlack,
                                                          fill_style = 3345),
                                                       PO(marker_color = ROOT.kRed,
                                                          marker_style = 20),
                                                       PO(marker_color = ROOT.kBlack,
                                                          marker_style = 20)],
                                        drawOptions = ['hist','hist','P','P'],
                                        canvasName = variable+'_'+prong+'p')
        # canvas = plotter.plotHistograms([pantau_sig,pantau_bg],
        #                                 plotOptions = [PO(marker_color = ROOT.kRed,
        #                                                   marker_style = 20),
        #                                                PO(marker_color = ROOT.kBlack,
        #                                                   marker_style = 20)],
        #                                 drawOptions = ['P','P'],
        #                                 canvasName = variable+'_'+prong+'p')
        pantau_sig.GetXaxis().SetTitleOffset(1)
        if config.has_key('ymax'):
            print config['ymax']
            formatter.setMinMax(tau_sig, # pantau_sig
                                maximum=config['ymax'],
                                axis = 'y')
        # if config.has_key('xmax'):
        #     formatter.setMinMax(pantau_sig, tau_sig, 
        #                         maximum=config['xmax'],
        #                         axis = 'x')
        formatter.setTitle(tau_sig, # pantau_sig,
                           variable,
                           'x')

        atlasPos = eval(self.getPosition('textposition',config))
        # lumiPos = eval(getPosition('textposition'))
        textPos = eval(self.getPosition('textposition',config))
        textPos['y'] = textPos['y'] - 0.05
        formatter.addATLASLabel(canvas,
                                description = 'internal',
                                pos= atlasPos,
                                size = 0.045,
                                offset = 0.12)
        # formatter.addLumiText(canvas,
        #                       lumi = 20281.4,
        #                       pos = eval(getPosition('lumiposition')),
        #                       splitLumiText = False)
        formatter.addText(canvas,
                          variable+', '+prong+' prong',
                          pos = textPos)
        formatter.addLegendToCanvas(canvas,
                                    legendOptions = eval(self.getPosition('legendoptions',config)),
                                    overwriteLabels = ['default signal', 'default background', 
                                                       'substructure signal', 'substructure background'],
                                    overwriteDrawOptions = ['hist', 'hist', 
                                                            'P', 'P'])

        canvas.Update()
        return canvas

    def makeNicePlots(self,
                      sig_rf,
                      bg_rf,
                      path):
        for prong in ['1','3']:
            for variable,config in self.config.items():
                print variable,prong
                sig1 = sig_rf.Get('VariablePlotter/tau_'+variable+'_'+prong+'P')
                sig2 = sig_rf.Get('VariablePlotter/pantau_CellBased_'+variable+'_'+prong+'P')
                bg1 = bg_rf.Get('VariablePlotter/tau_'+variable+'_'+prong+'P')
                bg2 = bg_rf.Get('VariablePlotter/pantau_CellBased_'+variable+'_'+prong+'P')

                if(sig1 != None):
                    if(sig1.Integral()!=0):
                        sig1.Scale(1/sig1.Integral())
                else:
                    if(sig2.Integral()!=0):
                        sig1 = sig2.Scale(1/sig2.Integral())
                if(sig2.Integral()!=0):
                    sig2.Scale(1/sig2.Integral())
                if(bg1 != None):
                    if(bg1.Integral()!=0):
                        bg1.Scale(1/bg1.Integral())
                else:
                    if(bg2.Integral()!=0):
                        bg1 = bg2.Scale(1/bg2.Integral())
                if(bg2.Integral()!=0):
                    bg2.Scale(1/bg2.Integral())

                # c_sig = self.plotAlgoComp(sig1,sig2,variable,prong)
                # c_sig.SaveAs(os.path.join(path,'signal_%s_%s.eps' % (variable,prong)))
                # c_bg = self.plotAlgoComp(bg1,bg2,variable,prong)
                # c_bg.SaveAs(os.path.join(path,'background_%s_%s.eps' % (variable,prong)))
                # c_tau = self.plotProcessComp(sig1,bg1,variable,prong)
                # c_tau.SaveAs(os.path.join(path,'tau_%s_%s.eps' % (variable,prong)))
                # c_pantau = self.plotProcessComp(sig2,bg2,variable,prong)
                # c_pantau.SaveAs(os.path.join(path,'pantau_%s_%s.eps' % (variable,prong)))

                c_comp = self.plotComp(sig1,bg1,sig2,bg2,variable,prong,config)
                # c_comp = self.plotComp(sig2,bg2,variable,prong,config)
                c_comp.SaveAs(os.path.join(path,'comparison_%s_%sprong.eps' % (variable,prong)))

                
def main(argv):
    parser = argparse.ArgumentParser(description = 'Nice LLH variable plotter')
    parser.add_argument('filelist', nargs = '+', help = 'input files')
    parser.add_argument('--outdir', '-o', default = None, help = 'output directory')
    args = parser.parse_args()

    for f in args.filelist:
        if 'mc' in f:
            sig_rf = ROOT.TFile.Open(f,'READ')
        if 'data' in f:
            bg_rf = ROOT.TFile.Open(f,'READ')
        else:
            assert 'mc' or 'data' in f

    variablePlotter = VariablePlotter()
    variablePlotter.makeNicePlots(sig_rf,bg_rf,args.outdir)
    
if __name__ == '__main__':
    main(sys.argv[1:])
