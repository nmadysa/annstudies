import argparse
import sys
sys.path.append('../../TauCommon/macros/')
import os
import string
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from Configurator import Configurator
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

class SeparationTester:
    def __init__(self,sig_rf,bg_rf):
        self.variableset_1p = ['CentFrac0102',
                               'FTrk02',
                               'pi0_vistau_m',
                               'ptRatio',
                               'TrkAvgDist',
                               'pi0_n',
                               'IpSigLeadTrk',
                               'NTracksdrdR']
        self.variableset_3p = ['CentFrac0102',
                               'FTrk02',
                               'pi0_vistau_m',
                               'ptRatio',
                               'TrkAvgDist',
                               'pi0_n',
                               'MassTrkSys',
                               'DrMax',
                               'TrFligthPathSig']        
        self.sig = sig_rf
        self.bg = bg_rf
        self.config = Configurator('configs/def_sub_llh_values_thesis.yml').__dict__
    
    def getVariable(self,
                    rf,
                    vn,
                    np):
        t = rf.Get('MiniTree')
        eval('t.Draw("%s>>htemp(%i,%i,%i)","NumTrack == %i","")' % (vn,60,-25.,5.,np))
        h = ROOT.gPad.GetPrimitive('htemp')
        return h.Clone(vn)

    def getPosition(self,
                    ltype,
                    config):
        position = "{"
        if config.has_key(ltype):
            it = config[ltype].__iter__()
            for x in config[ltype]:
                for key,value in x.items():
                    position += "\"%s\": %s, " % (key, value)
        position = position.rstrip(" ,") + "}"
        return position
    
    def getPlots(self,
                 vn,
                 np,
                 sig,
                 bg,
                 config):        

        if(sig.Integral()!=0):
            sig.Scale(1/sig.Integral())
        if(bg.Integral()!=0):
            bg.Scale(1/bg.Integral())

        canvas = plotter.plotHistograms([sig,bg],
                                        plotOptions = [PO(line_color = ROOT.kRed,
                                                          fill_color = ROOT.kRed,
                                                          fill_style = 3354),
                                                       PO(line_color = ROOT.kBlack,
                                                          fill_color = ROOT.kBlack,
                                                          fill_style = 3345)],
                                        drawOptions = ['hist','hist'],
                                        canvasName = vn+'_'+str(np)+'prong')
        sig.GetXaxis().SetTitleOffset(1)
        if config.has_key('ymax'):
            formatter.setMinMax(sig, 
                                maximum=config['ymax'],
                                axis = 'y')
        if config.has_key('xmin') and config.has_key('xmax'):
            formatter.setMinMax(sig,
                                minimum=config['xmin'],
                                maximum=config['xmax'],
                                axis = 'x')
        if config.has_key('xtitle'):
            formatter.setTitle(sig,
                               config['xtitle'],
                               'x')
        formatter.setTitle(sig,
                           'fraction of candidates',
                           'y')
        formatter.addLumiText(canvas,
                              lumi = 7653,
                              pos = eval(self.getPosition('lumiposition',config)),
                              splitLumiText = False)

        formatter.addText(canvas,
                          str(np)+'-prong',
                          pos = eval(self.getPosition('textposition',config)))
        formatter.addLegendToCanvas(canvas,
                                    legendOptions = eval(self.getPosition('legendoptions',config)),
                                    overwriteLabels = ['Z/Z\'#rightarrow#tau#tau', 'Di-jet data (2012)'],
                                    overwriteDrawOptions = ['hist', 'hist'])

        canvas.Update()
        return canvas
        # c = ROOT.TCanvas('c','c',800,700)
        # c.SetName(vn)
        # c.cd()

        # sig.Scale(1/sig.Integral())
        # bg.Scale(1/bg.Integral())
        
        # if bg.GetMaximum() <= sig.GetMaximum():
        #     sig.Draw("")
        #     bg.Draw("same")
        # else:
        #     bg.Draw("")
        #     sig.Draw("same")

        # sig.GetXaxis().SetTitle('LLH value')
        # bg.SetLineColor(1)
        # bg.SetFillStyle(3005)
        # bg.SetFillColor(1)
        # bg.SetStats(0)
        # sig.SetLineColor(2)
        # sig.SetFillStyle(3004)
        # sig.SetFillColor(2)
        # sig.SetStats(0)
        
        # return c
        
    def getMean(self,
                h):
        return h.GetMean()

    def getVar(self,
                 h):
        return pow(h.GetRMS(),2)
                
    def getString(self,
                  vn,
                  sig,
                  bg):
        s = vn + '\t' + str(self.getMean(sig)) + '\t' + str(self.getMean(bg)) + '\t' + str(self.getMean(sig)-self.getMean(bg)) + '\t' + str(self.getVar(sig)) + '\t' + str(self.getVar(bg)) + '\t' + str(self.getVar(sig)+self.getVar(bg)) + '\t'

        J = pow((self.getMean(sig)-self.getMean(bg)),2)/(self.getVar(sig)+self.getVar(bg))

        s += str(J) + '\n \n' 

        return string.rjust(s,20)
        
    def writeROOTFile(self,
                      path,
                      np,
                      vp):
        filename = path+'separation.root'
        f = ROOT.TFile.Open(filename,'update')
        if(not f.FindKey(str(np)+'prong')):
            adir = ROOT.gDirectory.mkdir(str(np)+'prong')
        f.cd(str(np)+'prong')
        adir = f.GetDirectory(str(np)+'prong')
        adir.Delete('%s;1' % vp.GetName())
        vp.Write()
        f.Close()
        
    def writeTextFile(self,
                      path,
                      np,
                      s):
        f = open('%sseparation_%ip.txt' % (path,np),'w')
        
        print >> f, string.rjust('variable \t mean(sig) \t mean(bg) \t dMean \t var(sig) \t var(bg) \t dVar \t separation power \n \n',10)
        print >> f, s
        f.close()

    def writeEPS(self,
                 path,
                 vp):
        vp.SaveAs(os.path.join(path,'separation_%s.eps' % vp.GetName()))
        
    def getSeparation(self,
                      path):
        for prong in [1,3]:
            string = ''
            for variable,config in self.config.items():
                if variable in eval('self.variableset_%sp' % prong):
                    background = self.getVariable(self.bg,variable,prong)
                    signal = self.getVariable(self.sig,variable,prong)
                    plot = self.getPlots(variable,prong,signal,background,config)
                    string += self.getString(variable,signal,background)
                # self.writeROOTFile(path,prong,plot)
                    self.writeEPS(path,plot)
            self.writeTextFile(path,prong,string)
                
def main(argv):
    parser = argparse.ArgumentParser(description = 'test separation power of variables for llh')
    parser.add_argument('filelist', nargs = 2, help = 'input filelist (NtupleCycle output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    args = parser.parse_args()

    for f in args.filelist:
        if 'mc' in f:
            sig_rf = ROOT.TFile.Open(f,'READ')
        if 'data' in f:
            bg_rf = ROOT.TFile.Open(f,'READ')
        else:
            assert 'mc' or 'data' in f

    separationTester = SeparationTester(sig_rf,bg_rf)
    separationTester.getSeparation(args.outdir)

if __name__ == '__main__':
    main(sys.argv[1:])
