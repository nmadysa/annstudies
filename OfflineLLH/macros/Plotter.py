import sys,argparse,os,__builtin__
import ROOT
from FileHandler import FileHandler as FH
import HistFormatter as HF
import Utilities

_logger = Utilities._logger

def plot_stacked(hists,data,name,canvas_options,legend_options,min,max):
    """
    plots the given list of histogram as a stack and returns the canvas
    """
    po = [k.sample.plot_options for k in hists]
    ret = HF.stack_hists(hists,name,Utilities.default,min,max,po,canvas_options,'hist')
    # create an draw legend
    leg = HF.make_legend(ret['hists'],[h.sample.label for h in hists],'f')
    # handle data
    if data:
        leg.AddEntry(data,data.sample.label,'lp')
        data.sample.plot_options.configure(data)
        ret['stack'].SetMaximum(1.1 * __builtin__.max(data.GetMaximum(),ret['stack'].GetMaximum()))
        ret['canvas'].Update()
        data.Draw("same")
    legend_options.configure(leg)
    leg.Draw('same')
    ret['canvas'].Update()
    ret['legend'] = leg
    return ret

def plot_normalised(hists,data,name,canvas_options,legend_options,min,max):
    """
    plots the given list of histogram and returns the canvas
    """
    # use fill color as line color
    po = [HF.PlotOptions(line_color=k.sample.plot_options.fill_color) for k in hists]
    draw_options = ['hist'] * len(hists)
    if data:
        hists.append(data)
        po.append(data.sample.plot_options)
        draw_options.append('p')
    ret = HF.pile_hists(hists,name,None,min,max,po,canvas_options,draw_options)
    # create an draw legend
    leg = HF.make_legend(ret['hists'],[h.sample.label for h in hists],'L')
    leg.Draw('same')
    ret['canvas'].Update()
    ret['legend'] = leg
    return ret
     
def get_hists(dist,filelist,lumi):
    """
    returns dictionary with keys 'mc_hists' -> list of mc histos,'data' -> data histo (maybe None)
    """
    histos = {}
    mc_hists = {}
    data = None
    normalise = False
    if lumi == -1:
        # fake lumi
        lumi = 100
        normalise = True
    # get histograms from all given files (scaled to given lumi/normalised if lumi == -1)
    for filename in filelist:
        _logger.info("creating HistGetter for file '%s'",filename)
        getter = FH(filename)
        hist = getter.get(dist,lumi)
        if getter._hasFile:
            getter.close()
        if not hist:
            _logger.warning("histogram '%s' not found in file '%s'",dist,filename)
            continue
        if getter.sample.isData():
            if not data:
                hist.SetName("data")
                data = hist
            else:
                data.Add(hist)
            continue
        else:
            process = getter.sample.process
        if process in mc_hists:
            mc_hists[process].Add(hist)
        else:
            hist.SetName(process)
            mc_hists[process] = hist

    # should be normalised
    if normalise:
        for h in mc_hists.values():
            Utilities.normaliseHistogram(h)
        if data:
            Utilities.normaliseHistogram(data)
    histos['mc_hists'] = mc_hists
    histos['data'] = data
    return histos
                
def plot(distributions,filelist,lumi,options,outfile=None,ordering=None,canvas_opt=None,legend_opt=None):
    if not isinstance(distributions,list):
        distributions = [distributions]
    for dist in distributions:
        histos = get_hists(dist,filelist,lumi)
        mc_hists = histos['mc_hists']
        data = histos['data']
        if not mc_hists: # or not data:
            _logger.error("no histograms for distribution '%s' found",dist)
            continue
        # adjust draw order
        hists = []
        if ordering:
            # follow ordering
            for key in ordering:
                if key in mc_hists:
                    hists.append(mc_hists[key])
                    del mc_hists[key]
            # add any left over histograms at the end
            for h in mc_hists.values():
                hists.append(h)
        else:
            hists = mc_hists.values()
        # make plot nice
        HF.set_max(hists)
        HF.set_min(hists)
        if not canvas_opt:
            canvas_options = HF.CanvasOptions()
        else:
            canvas_options = HF.CanvasOptions(**canvas_opt)
        if not legend_opt:
            legend_options = HF.LegendOptions()
        else:
            legend_options = HF.LegendOptions(**legend_opt)

        # what should be plotted
        # usual stacked plot
        if lumi > 0:
            ret = plot_stacked(hists,data,dist.replace('/','_'),canvas_options,legend_options,options['min'],options['max'])
            c = ret['canvas']
            l_text = HF.make_lumi_text(0.35,0.88,lumi)
            l_text.Draw("same")
            c.Update()
        elif lumi == -1:
            ret = plot_normalised(hists,data,dist.replace('/','_'),canvas_options,legend_options,options['min'],options['max'])
            c = ret['canvas']
            c.Update()
        #atlas_text = HF.make_atlas_watermark(0.18,0.85)
        #atlas_text.Draw("same")
        # save canvas as picture?
        batch = options['batch']
        if not batch:
            image = raw_input("save canvas as (<RET> for skipping): ")
        else:
            image = ''
        if image:
            c.SaveAs(image)
        # store canvas in root file
        if outfile:
            if not batch:
                root_out = raw_input("In which directory the TCanvas should be stored in the output ROOT file '%s' (<s> for skipping): " % outfile)
            else:
                root_out = os.path.dirname(dist)
            if root_out != 's':
                Utilities.write_to_root_file(c,outfile,root_out)
    
def main(argv):
    import logging
    ROOT.gROOT.Macro( os.path.expanduser('/home/morgenst/AtlasStyle/AtlasStyle.C'))
    ROOT.SetAtlasStyle()    
    parser = argparse.ArgumentParser(description='Distribution Plotter - plotting stacked histograms')
    parser.add_argument('--debug',nargs='?',dest='level',choices=['DEBUG','INFO','WARNING','CRITICAL','ERROR'],default='WARNING',help='logging level')
    parser.add_argument('filelist',nargs='+',help='filelist')
    parser.add_argument('--dist',nargs='+',help='distribution which is plotted (name under which it is stored inside the ROOT files)')
    parser.add_argument('--lumi',type=float,help='integrated luminosity in pb^{-1}, -1 for normalised')
    parser.add_argument('--canvas',type=str,help='canvas option as dictionary string, possible keys are: width, height, log_[xyz], grid_[xy], tick_[xy], (left|right|top|bottom)_margin')
    parser.add_argument('--legend',type=str,help='legend option as dictionary string, possible keys are: [xy][12], n_col, (col|entry)_sep, margin, header')
    parser.add_argument('--output',default=None,type=str,help='output ROOT file')
    parser.add_argument('--min',default=None,type=float,help='set minimum')
    parser.add_argument('--max',default=None,type=float,help='set maximum')
    parser.add_argument('--batch',default=False,action='store_true',help='start batch mode')
    parser.add_argument('--interactive',default=False,action='store_true',help='start interactive mode')
    default_ordering = ["ggH150","bbA150","ttbar","Z_DY_tautau","Z_DY_mumu","Z_DY_ee","Diboson","Wtaunu","Wmunu","Wenu","s_top","data"]
    parser.add_argument('--ordering',nargs='*',default=default_ordering,help='drawing order')
    args = parser.parse_args()
    # set logging level
    _logger.setLevel(eval('logging.' + args.level))
    # set batch mode
    if args.batch:
        ROOT.gROOT.SetBatch(True)
    # standalone mode
    if not args.interactive:
        if not args.dist:
            _logger.error("no distribution specified -> aborting")
            return
        options = {}
        options['min'] = args.min
        options['max'] = args.max
        options['batch'] = args.batch
        co = {}
        lo = {}
        if args.canvas:
            co = eval(args.canvas)
        if args.legend:
            lo = eval(args.legend)
        plot(args.dist,args.filelist,args.lumi,options,args.output,args.ordering,co,lo)
    # interactive mode
    elif not args.batch:
        # remember file list
        filelist = args.filelist
        outfile = args.output
        print 'interactive mode'
        print '================\n'
        print 'using the following files:'
        for f in filelist:
            _logger.info("using file: '%s'",f)
        print
        print 'For plotting type: --dist <distribution> --lumi <lumi in ipb>'
        print 'type --help for more options and <RET> for quit'
        while 1:
            print
            cmd = raw_input('input (<RET> for quit): ')
            if not cmd:
                print 'Good Bye!'
                return
            else:
                l = cmd.split()
                l.extend(filelist)
                if outfile:
                    l.extend(['--output',outfile])
                args = parser.parse_args(l)
                options = {}
                options['min'] = args.min
                options['max'] = args.max
                co = {}
                lo = {}
                if args.canvas:
                    co = eval(args.canvas)
                if args.legend:
                    lo = eval(args.legend)
                plot(args.dist,args.filelist,args.lumi,options,args.output,args.ordering,co,lo)
    else:
        _logger.critical("you can't specify interactive and batch mode at once")
        
        
if __name__ == "__main__":
    main(sys.argv[1:])
