import argparse
import sys
import os
import ROOT

sys.path.append('../../TauCommon/macros/')

from InputHandler import Reader
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from Configurator import Configurator
from CommonTools import Writer
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

def makeScorePlots(rf, path, histConfig):

    np = ""
    if '1p' in rf.GetName():
        np = '1p'
    else:
        np = '3p'
    
    c = ROOT.TCanvas("c", "c", 800, 700)

    for method in ['BDT','BDTp']:

        method_typ = ""
        
        if method == "BDT" or method == "BDTp":
            method_typ = "BDT"
        else:
            method_typ = "Likelihood"
            
        if rf.Get('Method_'+method_typ+'/'+method+'/'+'MVA_'+method+'_S') == None:
            continue
        else:
            g = rf.Get('Method_'+method_typ+'/'+method+'/'+'MVA_'+method+'_S')
            f = rf.Get('Method_'+method_typ+'/'+method+'/'+'MVA_'+method+'_B')
            g.Scale(1/g.Integral())
            f.Scale(1/f.Integral())
    
            c.cd()
            # c.SetLogy()      

            g.Draw("P")
            g.SetMaximum(1.5*g.GetMaximum())
            g.GetXaxis().SetTitle(method+' score')
            g.SetMarkerColor(2)
            g.SetMarkerStyle(20)
            f.Draw("sameP")

            def getPosition(ltype): 
                position = "{"
                for variable, config in histConfig.items():
                    if variable == 'MVA_'+method+'_S' and config.has_key(ltype):
                        it = config[ltype].__iter__()
                        for x in config[ltype]:
                            for key,value in x.items():
                                position += "\"%s\": %s, " % (key, value)
                        position = position.rstrip(" ,") + "}"
                        return position
                    
            formatter.addLumiText(c,
                                  lumi = 20281.4,
                                  pos = eval(getPosition('lumiposition')),
                                  splitLumiText = False)

            formatter.addATLASLabel(c,
                                    description = 'internal',
                                    pos= eval(getPosition('labelposition')),
                                    size = 0.045,
                                    offset = 0.14)
        
            formatter.addLegendToCanvas(c,
                                        legendOptions = eval(getPosition('legendposition')),
                                        overwriteLabels = ['signal','background'])

            prongText = str(np)+'-prong, Data 2012'
            prongPos = eval(getPosition('labelposition'))
            prongPos['y'] = prongPos['y'] - 0.04
            formatter.addText(c, prongText, prongPos)
        
            c.SaveAs(os.path.join(path, method+'_TMVAScorePlots_'+str(np)+'.eps'))
            del g,f
        
def main(argv):
    parser = argparse.ArgumentParser(description = 'Score Plotter')
    parser.add_argument('file', help = 'input file (TMVA output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    args = parser.parse_args()

    histConfig = Configurator('configs/score_config.yml').__dict__

    rf = ROOT.TFile.Open(args.file, 'READ')
    
    path = args.outdir

    makeScorePlots(rf, path, histConfig)
        
if __name__ == '__main__':
    main(sys.argv[1:])
