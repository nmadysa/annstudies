import argparse
import sys
sys.path.append('../../TauCommon/macros/')
import os
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from InputHandler import Reader
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from HistFormatter import CanvasOptions as CO
plotter = Plotter()
formatter = Formatter()

ROOT.gROOT.SetBatch(True)

class WeightCalculator:
    def __init__(self,fl):
        self.reader = Reader(fl)
        self.initialiseBins()

    def initialiseBins(self):
        self.pt_binning = [i for i in range(15,104,5)]
        self.mu_binning = [i for i in range(1,40,1)]

    def getPtWeightPlot(self,
                        prong,
                        lumi):
        mc_hists, data = self.reader.readHistogramAndMerge('VariablePlotter/tau_pt_unweighted_%iP' % prong,
                                                           lumi,
                                                           mergeOptions = self.reader.mergeOptions)
        
        signal = mc_hists['Signal']
        signal = formatter.rebin(signal,
                                 self.pt_binning)
        signal.Scale(1/signal.Integral())
        data = formatter.rebin(data,
                               self.pt_binning)
        data.Scale(1/data.Integral())
        
        c_pt_weights = ROOT.TCanvas('c_pt_weights','c_pt_weights',700, 700)
        c_pt_weights.cd()

        pt_weights = signal.Clone('pt_weights')
        pt_weights.Divide(data)
        pt_weights.Draw("")

        return signal,data,pt_weights

    def getMuWeightPlot(self,
                        prong,
                        lumi):
        mc_hists, data = self.reader.readHistogramAndMerge('VariablePlotter/nvtx_unweighted_%iP' % prong,
                                                           lumi,
                                                           mergeOptions = self.reader.mergeOptions)
        
        signal = mc_hists['Signal']
        signal = formatter.rebin(signal,
                                     self.mu_binning)
        signal.Scale(1/signal.Integral())
        data = formatter.rebin(data,
                               self.mu_binning)
        data.Scale(1/data.Integral())
            
        c_mu_weights = ROOT.TCanvas('c_mu_weights','c_mu_weights',700, 700)
        c_mu_weights.cd()

        mu_weights = data.Clone('mu_weights')
        mu_weights.Divide(signal)
        mu_weights.Draw("")

        return signal,data,mu_weights

    def getPtWeightString(self,
                          prong,
                          hist):
        ptString = 'TString GetWeights_BackgroundPt_%ip() \n {return \"' % prong
        for bin in range(1, hist.GetNbinsX() + 1):
            ptString += '%f * (tau_Et >= %f && tau_Et< %f) + ' %(hist.GetBinContent(bin),
                                                                 hist.GetBinLowEdge(bin) * 1000.,
                                                                 (hist.GetBinLowEdge(bin) + hist.GetBinWidth(bin)) * 1000.)
        ptString = ptString.rstrip('+ ')
        ptString += '\";}'

        return ptString

    def getPtWeightPyString(self,
                            prong,
                            hist):
        ptString = 'def GetWeights_BackgroundPt_%ip(): \n return \"' % prong
        for bin in range(1, hist.GetNbinsX() + 1):
            ptString += '%f * (tau_Et >= %f && tau_Et< %f) + ' %(hist.GetBinContent(bin),
                                                                 hist.GetBinLowEdge(bin) * 1000.,
                                                                 (hist.GetBinLowEdge(bin) + hist.GetBinWidth(bin)) * 1000. if hist.GetBinLowEdge(bin) < 95. else 10000000000000.)
        ptString = ptString.rstrip('+ ')
        ptString += '\"'

        return ptString

    def getMuWeightString(self,
                          prong,
                          hist):
        muString = 'TString GetWeights_SignalMu_%ip() \n {return \"' % prong
        for bin in range(1, hist.GetNbinsX() + 1):
            muString += '%f * (mu >= %f && mu< %f) + ' %(hist.GetBinContent(bin),
                                                         hist.GetBinLowEdge(bin),
                                                         (hist.GetBinLowEdge(bin) + hist.GetBinWidth(bin)))
        muString = muString.rstrip('+ ')
        muString += '\";}'

        return muString

    def getMuWeightPyString(self,
                            prong,
                            hist):
        muString = 'def GetWeights_SignalMu_%ip(): \n return \"' % prong
        for bin in range(1, hist.GetNbinsX() + 1):
            muString += '%f * (mu >= %f && mu< %f) + ' %(hist.GetBinContent(bin),
                                                         hist.GetBinLowEdge(bin),
                                                         (hist.GetBinLowEdge(bin) + hist.GetBinWidth(bin)))
        muString = muString.rstrip('+ ')
        muString += '\"'

        return muString

    def writeROOTFile(self,
                      prong,
                      pt,
                      mu,
                      path):
        filename = path+"weights.root"
        fOut = ROOT.TFile.Open(filename,'update')
        if(not fOut.FindKey(str(prong)+'prong')):
            adir = ROOT.gDirectory.mkdir(str(prong)+'prong')
        fOut.cd(str(prong)+'prong')
        adir = fOut.GetDirectory(str(prong)+'prong')
        adir.Delete("pt_weights;1")
        adir.Delete("mu_weights;1")
        pt.Write()
        mu.Write()
        fOut.Close()

    def writeTextFile(self,
                      prong,
                      pt,
                      mu,
                      path):
        f = open('%sweights_%iprong.txt' % (path,prong),'w')
        print f
        print >> f, pt
        print >> f, '\n'
        print >> f, mu
        f.close()

    def writePythonFile(self,
                        prong,
                        pt,
                        mu,
                        path):
        f = open('%sweights_%iprong.py' % (path,prong),'w')
        print f
        print >> f, pt
        print >> f, '\n'
        print >> f, mu
        f.close()
        
    def writeControlFile(self,
                         prong,
                         pt_ratio,
                         pt_mc,
                         pt_data,
                         mu_ratio,
                         mu_mc,
                         mu_data,
                         path):
        pt_control = plotter.plotHistograms([pt_data] + [pt_mc],
                                             plotOptions = [PO(marker_color = ROOT.kBlack,
                                                               marker_style = 22),
                                                            PO(marker_color = ROOT.kRed,
                                                               marker_style = 23)],
                                             drawOptions = ['P','P'],
                                             canvasName = 'pt_weight_control_plots_' + str(prong))
        formatter.setMinMax(pt_data,
                            0.,
                            100.,
                            'x')
        formatter.setTitle(pt_data,
                           'Normalised',
                           'y')
        formatter.addLegendToCanvas(pt_control,
                                    legendOptions = {'x1': 0.75, 'x2': 0.9, 'y1': 0.75, 'y2': 0.9} ,
                                    overwriteLabels = ['data', 'signal'],
                                    overwriteDrawOptions = ['P', 'P'])
        formatter.addATLASLabel(pt_control,
                                pos={'x':0.3 , 'y': 0.8},
                                description = 'Internal',
                                offset=0.125)
        # formatter.addATLASLabel(mu_control,
        #                         pos={'x':0.3 , 'y': 0.8},
        #                         description = 'Internal',
        #                         offset=0.075)

        c_pt_ratio = ROOT.TCanvas('c_pt_ratio_'+str(prong),'c_pt_ratio_'+str(prong),700, 700)
        c_pt_ratio.cd()
        pt_ratio.Draw()
        formatter.setMinMax(pt_ratio,
                            0.,
                            100.,
                            'x')
        formatter.setTitle(pt_ratio,
                           'p_{T} [GeV]',
                           'x')
        formatter.setTitle(pt_ratio,
                           'Ratio',
                           'y')
        pt_control = plotter.addRatioToCanvas(pt_control,c_pt_ratio)
        pt_control.SetName('pt_control_'+str(prong))
        
        mu_control = plotter.plotHistograms([mu_data] + [mu_mc],
                                             plotOptions = [PO(marker_color = ROOT.kBlack,
                                                               marker_style = 22),
                                                            PO(marker_color = ROOT.kRed,
                                                               marker_style = 23)],
                                             drawOptions = ['P','P'],
                                             canvasName = 'mu weight control plots' + str(prong))
        formatter.setMinMax(mu_data,
                            0.,
                            40.,
                            'x')
        formatter.setTitle(mu_data,
                           'Normalised',
                           'y')
        formatter.addLegendToCanvas(mu_control,
                                    legendOptions = {'x1': 0.2, 'x2': 0.35, 'y1': 0.75, 'y2': 0.9} ,
                                    overwriteLabels = ['data', 'signal'],
                                    overwriteDrawOptions = ['P', 'P'])

        c_mu_ratio = ROOT.TCanvas('c_mu_ratio_'+str(prong),'c_mu_ratio_'+str(prong),700, 700)
        c_mu_ratio.cd()
        mu_ratio.Draw()
        formatter.setMinMax(mu_ratio,
                            0.,
                            40.,
                            'x')
        formatter.setTitle(mu_ratio,
                           '#mu',
                           'x')
        formatter.setTitle(mu_ratio,
                           'Ratio',
                           'y')
        mu_control = plotter.addRatioToCanvas(mu_control,c_mu_ratio)
        mu_control.SetName('mu_control_'+str(prong))
        
        
        filename = path+"controls.root"
        fOut = ROOT.TFile.Open(filename,'update')
        if(not fOut.FindKey(str(prong)+'prong')):
            adir = ROOT.gDirectory.mkdir(str(prong)+'prong')
        fOut.cd(str(prong)+'prong')
        adir = fOut.GetDirectory(str(prong)+'prong')
        adir.Delete("pt_control_%i;1" % prong)
        adir.Delete("mu_control_%i;1" % prong)
        print 'pt',pt_control
        print 'mu',mu_control
        pt_control.Update()
        mu_control.Update()
        pt_control.Modified()
        mu_control.Modified()
        pt_control.Write()
        mu_control.Write()
        pt_control.SaveAs(path + 'ptweights_' + str(prong) + '.eps')
        mu_control.SaveAs(path + 'muweights_' + str(prong) + '.eps')
        
        fOut.Close()
    
    def getAllWeights(self,
                      lumi,
                      path):
        for prong in [1,3]:
            ptHistMC,ptHistDATA,ptWeightPlot = self.getPtWeightPlot(prong,
                                                                    lumi)
            muHistMC,muHistDATA,muWeightPlot = self.getMuWeightPlot(prong,
                                                                    lumi)
            ptWeightString = self.getPtWeightString(prong,
                                                    ptWeightPlot)
            muWeightString = self.getMuWeightString(prong,
                                                    muWeightPlot)
            ptWeightPyString = self.getPtWeightPyString(prong,
                                                        ptWeightPlot)
            muWeightPyString = self.getMuWeightPyString(prong,
                                                        muWeightPlot)
            
            self.writeROOTFile(prong,
                               ptWeightPlot,
                               muWeightPlot,
                               path)
            self.writeTextFile(prong,
                               ptWeightString,
                               muWeightString,
                               path)
            self.writePythonFile(prong,
                                 ptWeightPyString,
                                 muWeightPyString,
                                 path)
            self.writeControlFile(prong,
                                  ptWeightPlot,
                                  ptHistMC,
                                  ptHistDATA,
                                  muWeightPlot,
                                  muHistMC,
                                  muHistDATA,
                                  path)
            
def main(argv):
    parser = argparse.ArgumentParser(description = 'Pt weight calculator')
    parser.add_argument('filelist', nargs = '+', help = 'input filelist (LLHCycle output)')
    parser.add_argument('--outdir', '-o', default = os.curdir, help = 'output directory')
    parser.add_argument('--lumi', '-l', type = float, default = 1, help = 'luminosity in pb^-1')
    args = parser.parse_args()
    
    reader = Reader(args.filelist)

    weightCalculator = WeightCalculator(args.filelist)
    weightCalculator.getAllWeights(args.lumi,
                                   args.outdir)
    
if __name__ == '__main__':
    main(sys.argv[1:])
    

