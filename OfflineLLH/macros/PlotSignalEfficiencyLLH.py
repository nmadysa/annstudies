import argparse
import sys
sys.path.append('../../TauCommon/macros')
from InputHandler import Reader
from HistTools import Plotter, Formatter
from HistFormatter import PlotOptions as PO
from HistFormatter import plot_graph
from CommonTools import Writer
import ROOT
plotter = Plotter()
formatter = Formatter()

rebin = [i * 5 for i in range(0,10)]
rebin += [60, 80, 100]

ROOT.gROOT.SetBatch(True)
def getEfficiency(hist, norm = None, doRebin = True):
    if doRebin:
        hist = formatter.rebin(hist,
                               rebin)
    efficiency = hist.Clone()
    efficiency.Divide(norm)
    return efficiency

def getHist(wp):
    return reader.readHistogramAndMerge('VariablePlotter/h_passed_%s_%s_%iP_truth_pt' % (algo,
                                                                                         wp,
                                                                                         prong),
                                        1., mergeOptions = reader.mergeOptions)[0]['Signal']

def getHistNVtx(wp):
    return reader.readHistogramAndMerge('VariablePlotter/h_passed_%s_%s_%iP_nvtx' % (algo,
                                                                                     wp,
                                                                                     prong),
                                        1., mergeOptions = reader.mergeOptions)[0]['Signal']

def main(argv):
    parser = argparse.ArgumentParser(description = 'Plotter for background rejection vs. signal efficiency')
    parser.add_argument('filelist', nargs = '+', help = 'filelist')
    parser.add_argument('--outdir', default = None, help = 'output directory')
    parser.add_argument('--prong', default = 1, type = int,help = 'prongness')
    parser.add_argument('--algo', default = 'BDT', type = str, help = 'BDT or LLH')
    global args
    args = parser.parse_args()

    global reader
    reader = Reader(args.filelist)
    global writer
    writer = Writer(args.outdir)
    global algo, prong
    algo = args.algo
    prong = args.prong
    plotEfficiencyVsPt()
    plotEfficiencyVsNVtx()
    
def plotEfficiencyVsPt():
    norm_hist = reader.readHistogramAndMerge('VariablePlotter/truth_tau_vis_pt_%iP' % args.prong, 1., mergeOptions = reader.mergeOptions)[0]['Signal']
    norm_hist = formatter.rebin(norm_hist,
                    rebin)

    mc_hists_loose = getHist("loose")
    signal_loose = getEfficiency(mc_hists_loose,
                                 norm_hist)
    mc_hists_medium = getHist("medium")
    signal_medium = getEfficiency(mc_hists_medium,
                                  norm_hist)
    mc_hists_tight = getHist("tight")
    signal_tight = getEfficiency(mc_hists_tight,
                                 norm_hist)

    canvas = plotter.plotHistograms([signal_loose, signal_medium, signal_tight],
                                    plotOptions = [PO(marker_color = ROOT.kBlue,
                                                      marker_style = 22),
                                                   PO(marker_color = ROOT.kRed,
                                                      marker_style = 21),
                                                   PO(marker_color = ROOT.kGreen,
                                                      marker_style = 23)],
                                    drawOptions = ['P','P','P'],
                                    canvasName = 'efficiency')
    
    formatter.setMinMax(signal_loose,
                        0.,
                        1.2,
                        'y')
    formatter.setMinMax(signal_loose,
                        10.,
                        100.,
                        'x')
    formatter.setTitle(signal_loose,
                       'generated p^{vis}_{T} [GeV]',
                       'x')
    formatter.setTitle(signal_loose,
                       'Signal Efficiency',
                       'y')
    formatter.addLegendToCanvas(canvas,
                                legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9},
                                overwriteLabels = ['loose', 'medium', 'tight'],
                                overwriteDrawOptions = ['P', 'P', 'P'])
    text = '1-prong' if args.prong == 1 else 'multi-prong'
    formatter.addText(canvas,
                      text,
                      pos = {'x': 0.6, 'y': 0.85})
    canvas.Update()
    writer.dumpCanvas(canvas, image = 'SignalEfficiency_%s_%iP.eps' % (args.algo, args.prong))

def plotEfficiencyVsNVtx():
    norm_hist = reader.readHistogramAndMerge('VariablePlotter/truth_tau_nvtx_%iP' % args.prong, 1., mergeOptions = reader.mergeOptions)[0]['Signal']

    mc_hists_loose = getHistNVtx("loose")
    signal_loose = getEfficiency(mc_hists_loose,
                                 norm_hist,
                                 doRebin = False)
    mc_hists_medium = getHistNVtx("medium")
    signal_medium = getEfficiency(mc_hists_medium,
                                  norm_hist,
                                  doRebin = False)
    mc_hists_tight = getHistNVtx("tight")
    signal_tight = getEfficiency(mc_hists_tight,
                                 norm_hist,
                                 doRebin = False)

    canvas = plotter.plotHistograms([signal_loose, signal_medium, signal_tight],
                                    plotOptions = [PO(marker_color = ROOT.kBlue,
                                                      marker_style = 22),
                                                   PO(marker_color = ROOT.kRed,
                                                      marker_style = 21),
                                                   PO(marker_color = ROOT.kGreen,
                                                      marker_style = 23)],
                                    drawOptions = ['P','P','P'],
                                    canvasName = 'efficiency')
    
    formatter.setMinMax(signal_loose,
                        0.,
                        1.2,
                        'y')
    formatter.setMinMax(signal_loose,
                        0.,
                        40.,
                        'x')
    formatter.setTitle(signal_loose,
                       'Signal Efficiency',
                       'y')
    formatter.setTitle(signal_loose,
                       'No. of average interactions per bunch crossing',
                       'x')
    formatter.addLegendToCanvas(canvas,
                                #legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.15, 'y2': 0.4} if args.prong == 1 else {'x1': 0.2, 'x2': 0.4, 'y1': 0.65, 'y2': 0.9} ,
                                legendOptions = {'x1': 0.2, 'x2': 0.4, 'y1': 0.7, 'y2': 0.9} ,
                                overwriteLabels = ['loose', 'medium', 'tight'],
                                overwriteDrawOptions = ['P', 'P', 'P'])
    text = '1-prong' if args.prong == 1 else 'multi-prong'
    formatter.addText(canvas,
                      text,
                      pos = {'x': 0.6, 'y': 0.85})
    canvas.Update()
    writer.dumpCanvas(canvas, image = 'SignalEfficiency_%s_%iP_nvtx.eps' % (args.algo, args.prong))
    
if __name__ == '__main__':
    main(sys.argv[1:])
