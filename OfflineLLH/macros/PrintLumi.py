import argparse
import sys
sys.path.append('../../TauCommon/macros/')
from Samples import Samples
from FileHandler import FileHandler as FH
import Utilities

lumiTable = {}

def calcWeights(filelist,
                lumi):
    for fName in filelist:
        fh = FH(fName)
        processedEvents = fh.getProcessedEvents()[1]
        sample = Utilities.getSampleFromFileName(fName)
        xs = sample.getWeight()
        lumiWeight = float(xs) / processedEvents * lumi
        lumiTable[sample.name] = lumiWeight
        
def printWeights():
    totlumi = sum(lumiTable.values())
    for name, weight in lumiTable.items():
        print 'weight%s = %f' %(name, weight / totlumi)
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Lumi weight printer for TMVA')
    parser.add_argument('filelist', nargs = '+', help = 'filelist')
    parser.add_argument('--lumi', type = float, default=1, help = 'luminosity in pb^-1')
    args = parser.parse_args()
    
    calcWeights(args.filelist, args.lumi)
    printWeights()
