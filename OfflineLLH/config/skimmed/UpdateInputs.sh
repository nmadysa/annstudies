#!/bin/bash

SKIMDIR=/raid2/users/morgenst/TNTs/LLH

sh GenerateInput.sh input_Ztautau.xml $SKIMDIR/MC/ZtautauPythia
sh GenerateInput.sh input_Wtaunu.xml $SKIMDIR/MC/WtaunuPythia
sh GenerateInput.sh input_ZPrime250.xml $SKIMDIR/MC/ZPrime250
sh GenerateInput.sh input_ZPrime500.xml $SKIMDIR/MC/ZPrime500
sh GenerateInput.sh input_ZPrime750.xml $SKIMDIR/MC/ZPrime750
sh GenerateInput.sh input_ZPrime1000.xml $SKIMDIR/MC/ZPrime1000
sh GenerateInput.sh input_ZPrime1250.xml $SKIMDIR/MC/ZPrime1250

sh GenerateInput.sh input_PeriodA.xml $SKIMDIR/Data/periodA
sh GenerateInput.sh input_PeriodB.xml $SKIMDIR/Data/periodB
sh GenerateInput.sh input_PeriodC.xml $SKIMDIR/Data/periodC
sh GenerateInput.sh input_PeriodD.xml $SKIMDIR/Data/periodD
sh GenerateInput.sh input_PeriodE.xml $SKIMDIR/Data/periodE
sh GenerateInput.sh input_PeriodE.xml $SKIMDIR/Data/periodE
sh GenerateInput.sh input_PeriodH.xml $SKIMDIR/Data/periodH
sh GenerateInput.sh input_PeriodI.xml $SKIMDIR/Data/periodI
sh GenerateInput.sh input_PeriodJ.xml $SKIMDIR/Data/periodJ
sh GenerateInput.sh input_PeriodL.xml $SKIMDIR/Data/periodL
