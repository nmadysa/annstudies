#ifndef TauCommon_Helpers_H
#define TauCommon_Helpers_H

// core include(s)
#include "Particle.h"

// TauCommon include(s)
#include "Tau.h"
#include "TrigEFTau.h"
#include "EventInfo.h"

namespace TauCommon_Helpers
{
  const float GeV = 1000.0;
  
  float ParticlePt(Particle* const& pParticle);
  float ParticleEta(Particle* const& pParticle);
  float ParticlePhi(Particle* const& pParticle);
  float LLHScore(D3PDReader::Tau* const& pParticle);
  float NVtx(D3PDReader::EventInfo const& event);
  template<class T>
  float NProng(T* const& pTau)
  {
    return (float)pTau->numTrack();
  };
}

#endif // TauCommon_Helpers_H
