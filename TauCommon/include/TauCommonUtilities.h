#ifndef TauCommonUtilities_H
#define TauCommonUtilities_H

#ifndef __CINT__
#include "boost/bind.hpp"
#endif //__CINT__
//#include "Vertex.h"

#include "Vertex.h"
#include "TruthTau.h"
#include "Tau.h"
#include "TrigL1Tau.h"
#include "TrigL2Tau.h"
#include "TrigEFTau.h"
#include "MCTruthParticle.h"

namespace TauCommonUtilities
{
  const float GeV = 1000.0;

  inline bool type_comp(const D3PDReader::Vertex* v1, const D3PDReader::Vertex* v2){
    return (v1->type() < v2->type());
  }
  inline bool TrackMatch(const D3PDReader::TruthTau* truetau, D3PDReader::Tau* tau){
    if((unsigned int)truetau->tauAssoc_index() == tau->GetIndex() && truetau->nProng() == tau->seedCalo_numTrack())
      return true;
    else 
      return false;
  }
  inline void truthmatching(const std::vector<D3PDReader::TruthTau*>& m_vTrueTaus, std::vector<D3PDReader::Tau*>& m_vTaus){
    std::vector<D3PDReader::Tau*>::iterator it = m_vTaus.begin();
    while (it!= m_vTaus.end()){
      std::vector<D3PDReader::TruthTau*>::const_iterator sec = std::find_if(m_vTrueTaus.begin(), m_vTrueTaus.end(), boost::bind(&TauCommonUtilities::TrackMatch, _1, *it));
      if(sec == m_vTrueTaus.end())
	it = m_vTaus.erase(it);
      else
	++it;
    }
  }

  inline bool TruthMatchByIndex(D3PDReader::TruthTau* const pTruth, D3PDReader::Tau* const pTau){
    return ((unsigned int)pTruth->tauAssoc_index()==pTau->GetIndex());
  }

  inline bool IndexMatch(const D3PDReader::TruthTau* truetau, D3PDReader::Tau* tau){
    if((unsigned int)truetau->tauAssoc_index() == tau->GetIndex())
      return true;
    else 
      return false;
  }

  inline void TruthMatchByIndex2(const std::vector<D3PDReader::TruthTau*>& m_vTrueTaus, std::vector<D3PDReader::Tau*>& m_vTaus){
    std::vector<D3PDReader::Tau*>::iterator it = m_vTaus.begin();
    while (it!= m_vTaus.end()){
      std::vector<D3PDReader::TruthTau*>::const_iterator sec = std::find_if(m_vTrueTaus.begin(), m_vTrueTaus.end(), boost::bind(&TauCommonUtilities::IndexMatch, _1, *it));
      if(sec == m_vTrueTaus.end())
	it = m_vTaus.erase(it);
      else
	++it;
    }
  }

  template<class T, class U>
    bool RoIWordMatch(const T* first, U* second){
    if((unsigned int)first->RoIWord() == (unsigned int)second->RoIWord())
      return true;
    else 
      return false;
  }

  template<class T, class U>
    void RoIWordMatching(const std::vector<T*>& vFirst, std::vector<U*>& vSecond){
    typename std::vector<U*>::iterator itSecond = vSecond.begin();
    while (itSecond != vSecond.end()){
      typename std::vector<T*>::const_iterator itFirst = std::find_if(vFirst.begin(), vFirst.end(), boost::bind(&TauCommonUtilities::RoIWordMatch<T, U>, _1, *itSecond));
      if(itFirst == vFirst.end())
	itSecond = vSecond.erase(itSecond);
      else
	++itSecond;
    }
  }

  template<class T, class U>
    bool DeltaRMatch(const T* first, const U* second, double deltaR){
    return (first->TLV().DeltaR(second->TLV()) < deltaR);
  }



  inline bool IsGoodDaughter(int status, int barcode, int vx_barcode) {
    if( status==2 ) { return true; }
    if( ( status < 200 ) && ( status > 190 ) ) { return true; }
    if( (( status%1000 == 1) || (status%1000 == 2 && status > 1000) || (status==2 && vx_barcode < -200000)) && (barcode<200000) ) { return true; }

    return false;
  }

  /*  inline double PtMCTruthTau(D3PDReader::TruthTau* const pTruth, std::vector<D3PDReader::MCTruthParticle*>* m_vMCTruthParticles) {
    // Get Index of relevant MCTruthParticle
    int index = -1;
    int numCandidates = 0;
    for(unsigned int i = 0; i < m_vMCTruthParticles->size(); i++) {
      if( m_vMCTruthParticles->at(i)->status() == 3 ) { continue; }

      if( fabs(m_vMCTruthParticles->at(i)->pdgId()) == 15 && m_vMCTruthParticles->at(i)->status() == 2 && sqrt( pow(pTruth->eta()-m_vMCTruthParticles->at(i)->eta(), 2) + pow(pTruth->phi()-m_vMCTruthParticles->at(i)->phi(), 2) ) < 0.01 ) {
	//std::cout << "i: " << i << "\tPdgId: " << m_vMCTruthParticles->at(i)->pdgId() << "\teta: " << m_vMCTruthParticles->at(i)->eta() << "\tphi: " << m_vMCTruthParticles->at(i)->phi() << "\tstatus: " << m_vMCTruthParticles->at(i)->status() << std::endl;
	index = i;
	numCandidates++;
      }
    }

    assert(numCandidates == 1);

    // Loop over the children
    TLorentzVector visSum1;

    std::vector<int> vchild = m_vMCTruthParticles->at(index)->child_index();
    for(unsigned int cc = 0; cc < vchild.size(); cc++) {
      int cIdx = vchild.at(cc);

      int c_pdgId = m_vMCTruthParticles->at(cIdx)->pdgId();
      int c_stat = m_vMCTruthParticles->at(cIdx)->status();
      int c_barcode = m_vMCTruthParticles->at(cIdx)->barcode();
      int c_vxcode = m_vMCTruthParticles->at(cIdx)->vx_barcode();

      // skip neutrinos
      if( fabs(c_pdgId) == 12 || fabs(c_pdgId) == 14 || fabs(c_pdgId) == 16 ) { continue; }

      //if ( (c_stat%1000 == 1 || (c_stat==2 and c_vxcode < -200000) || (c_stat%1000 == 2 and c_stat > 1000) ) && c_barcode<200000 ) 
      if ( IsGoodDaughter(c_stat, c_barcode, c_vxcode) ) {
	TLorentzVector c_visSum;
	c_visSum.SetPtEtaPhiM( m_vMCTruthParticles->at(cIdx)->pt(), m_vMCTruthParticles->at(cIdx)->eta(), m_vMCTruthParticles->at(cIdx)->phi(), m_vMCTruthParticles->at(cIdx)->m() );
	visSum1+=c_visSum;
      }
    }

    return visSum1.Et();
  }
  */


	
  /**
   * A DeltaR matching between two sets of physics objects is applied. If the \f$ \Delta R \f$ between an object A of
   * the second container and all objects of the first container is greater than the threshold DeltaRCut, the object A
   * is removed from the second container.
   *
   * @attention The classes T and U must provide a member function "TLV()" returning a TLorentzVector
   *
   * @param rFirst first container of physics objects
   * @param rSecond second container of physics objects which are subject to the DeltaR matching
   * @param fDeltaRCut \f$ \Delta R \f$ cut threshold
   *
   * @return the number of removed objects from rSecond
   */
  template<class T,class U>
    static int DeltaRMatching(const std::vector<T*>& rFirst, std::vector<U*>& rSecond, float fDeltaRCut) {
    // different template types
    int iRemoved = 0;

    typename std::vector<U*>::iterator sec = rSecond.begin();
    typename std::vector<T*>::const_iterator it;
    bool bFound = false;
	  
    // loop over second container
    while(sec != rSecond.end())
      {
	// does current element of second container match any in the first container
	it = rFirst.begin();
	while(it != rFirst.end())
	  {
	    bFound = false;
	    // match found
	    if((*it)->TLV().DeltaR((*sec)->TLV()) <= fDeltaRCut)
	      {
		bFound = true;
		break;
	      }
	    else
	      ++it;
	  }
	// no match found -> remove
	if(!bFound)
	  {
	    ++iRemoved;
	    sec = rSecond.erase(sec);
	  }
	else
	  ++sec;
      }

    return iRemoved;
  }


}
#endif
