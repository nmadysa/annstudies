// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_LLHTrigEFTau_CXX
#define D3PDREADER_LLHTrigEFTau_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "LLHTrigEFTau.h"

ClassImp(D3PDReader::LLHTrigEFTau)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  LLHTrigEFTau::LLHTrigEFTau(const long int& master,const std::string& prefix):
    TObject(),
    Pt(prefix + "Pt",&master),
    Eta(prefix + "Eta",&master),
    Phi(prefix + "Phi",&master),
    NumTrack(prefix + "NumTrack",&master),
    OfflinePt(prefix + "OfflinePt",&master),
    OfflineEta(prefix + "OfflineEta",&master),
    OfflinePhi(prefix + "OfflinePhi",&master),
    OfflineNumTrack(prefix + "OfflineNumTrack",&master),
    OfflineLLHScore(prefix + "OfflineLLHScore",&master),
    OfflineLLHLoose(prefix + "OfflineLLHLoose",&master),
    OfflineLLHMedium(prefix + "OfflineLLHMedium",&master),
    OfflineLLHTight(prefix + "OfflineLLHTight",&master),
    OfflineBDTLoose(prefix + "OfflineBDTLoose",&master),
    OfflineBDTMedium(prefix + "OfflineBDTMedium",&master),
    OfflineBDTTight(prefix + "OfflineBDTTight",&master),
    TruthPt(prefix + "TruthPt",&master),
    TruthEta(prefix + "TruthEta",&master),
    TruthPhi(prefix + "TruthPhi",&master),
    TruthNumTrack(prefix + "TruthNumTrack",&master),
    NVertices(prefix + "NVertices",&master),
    CalRadius(prefix + "CalRadius",&master),
    CentFrac(prefix + "CentFrac",&master),
    DrMax(prefix + "DrMax",&master),
    EtOverPtLeadTrk(prefix + "EtOverPtLeadTrk",&master),
    IpSigLeadTrk(prefix + "IpSigLeadTrk",&master),
    Lead2ClusterEOverAllClusterE(prefix + "Lead2ClusterEOverAllClusterE",&master),
    MassTrkSys(prefix + "MassTrkSys",&master),
    NTracksdrdR(prefix + "NTracksdrdR",&master),
    TrFligthPathSig(prefix + "TrFligthPathSig",&master),
    TrkAvgDist(prefix + "TrkAvgDist",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  LLHTrigEFTau::LLHTrigEFTau(const std::string& prefix):
    TObject(),
    Pt(prefix + "Pt",0),
    Eta(prefix + "Eta",0),
    Phi(prefix + "Phi",0),
    NumTrack(prefix + "NumTrack",0),
    OfflinePt(prefix + "OfflinePt",0),
    OfflineEta(prefix + "OfflineEta",0),
    OfflinePhi(prefix + "OfflinePhi",0),
    OfflineNumTrack(prefix + "OfflineNumTrack",0),
    OfflineLLHScore(prefix + "OfflineLLHScore",0),
    OfflineLLHLoose(prefix + "OfflineLLHLoose",0),
    OfflineLLHMedium(prefix + "OfflineLLHMedium",0),
    OfflineLLHTight(prefix + "OfflineLLHTight",0),
    OfflineBDTLoose(prefix + "OfflineBDTLoose",0),
    OfflineBDTMedium(prefix + "OfflineBDTMedium",0),
    OfflineBDTTight(prefix + "OfflineBDTTight",0),
    TruthPt(prefix + "TruthPt",0),
    TruthEta(prefix + "TruthEta",0),
    TruthPhi(prefix + "TruthPhi",0),
    TruthNumTrack(prefix + "TruthNumTrack",0),
    NVertices(prefix + "NVertices",0),
    CalRadius(prefix + "CalRadius",0),
    CentFrac(prefix + "CentFrac",0),
    DrMax(prefix + "DrMax",0),
    EtOverPtLeadTrk(prefix + "EtOverPtLeadTrk",0),
    IpSigLeadTrk(prefix + "IpSigLeadTrk",0),
    Lead2ClusterEOverAllClusterE(prefix + "Lead2ClusterEOverAllClusterE",0),
    MassTrkSys(prefix + "MassTrkSys",0),
    NTracksdrdR(prefix + "NTracksdrdR",0),
    TrFligthPathSig(prefix + "TrFligthPathSig",0),
    TrkAvgDist(prefix + "TrkAvgDist",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void LLHTrigEFTau::ReadFrom(TTree* tree)
  {
    Pt.ReadFrom(tree);
    Eta.ReadFrom(tree);
    Phi.ReadFrom(tree);
    NumTrack.ReadFrom(tree);
    OfflinePt.ReadFrom(tree);
    OfflineEta.ReadFrom(tree);
    OfflinePhi.ReadFrom(tree);
    OfflineNumTrack.ReadFrom(tree);
    OfflineLLHScore.ReadFrom(tree);
    OfflineLLHLoose.ReadFrom(tree);
    OfflineLLHMedium.ReadFrom(tree);
    OfflineLLHTight.ReadFrom(tree);
    OfflineBDTLoose.ReadFrom(tree);
    OfflineBDTMedium.ReadFrom(tree);
    OfflineBDTTight.ReadFrom(tree);
    TruthPt.ReadFrom(tree);
    TruthEta.ReadFrom(tree);
    TruthPhi.ReadFrom(tree);
    TruthNumTrack.ReadFrom(tree);
    NVertices.ReadFrom(tree);
    CalRadius.ReadFrom(tree);
    CentFrac.ReadFrom(tree);
    DrMax.ReadFrom(tree);
    EtOverPtLeadTrk.ReadFrom(tree);
    IpSigLeadTrk.ReadFrom(tree);
    Lead2ClusterEOverAllClusterE.ReadFrom(tree);
    MassTrkSys.ReadFrom(tree);
    NTracksdrdR.ReadFrom(tree);
    TrFligthPathSig.ReadFrom(tree);
    TrkAvgDist.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void LLHTrigEFTau::WriteTo(TTree* tree)
  {
    Pt.WriteTo(tree);
    Eta.WriteTo(tree);
    Phi.WriteTo(tree);
    NumTrack.WriteTo(tree);
    OfflinePt.WriteTo(tree);
    OfflineEta.WriteTo(tree);
    OfflinePhi.WriteTo(tree);
    OfflineNumTrack.WriteTo(tree);
    OfflineLLHScore.WriteTo(tree);
    OfflineLLHLoose.WriteTo(tree);
    OfflineLLHMedium.WriteTo(tree);
    OfflineLLHTight.WriteTo(tree);
    OfflineBDTLoose.WriteTo(tree);
    OfflineBDTMedium.WriteTo(tree);
    OfflineBDTTight.WriteTo(tree);
    TruthPt.WriteTo(tree);
    TruthEta.WriteTo(tree);
    TruthPhi.WriteTo(tree);
    TruthNumTrack.WriteTo(tree);
    NVertices.WriteTo(tree);
    CalRadius.WriteTo(tree);
    CentFrac.WriteTo(tree);
    DrMax.WriteTo(tree);
    EtOverPtLeadTrk.WriteTo(tree);
    IpSigLeadTrk.WriteTo(tree);
    Lead2ClusterEOverAllClusterE.WriteTo(tree);
    MassTrkSys.WriteTo(tree);
    NTracksdrdR.WriteTo(tree);
    TrFligthPathSig.WriteTo(tree);
    TrkAvgDist.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void LLHTrigEFTau::SetActive(bool active)
  {
    if(active)
    {
     if(Pt.IsAvailable()) Pt.SetActive(active);
     if(Eta.IsAvailable()) Eta.SetActive(active);
     if(Phi.IsAvailable()) Phi.SetActive(active);
     if(NumTrack.IsAvailable()) NumTrack.SetActive(active);
     if(OfflinePt.IsAvailable()) OfflinePt.SetActive(active);
     if(OfflineEta.IsAvailable()) OfflineEta.SetActive(active);
     if(OfflinePhi.IsAvailable()) OfflinePhi.SetActive(active);
     if(OfflineNumTrack.IsAvailable()) OfflineNumTrack.SetActive(active);
     if(OfflineLLHScore.IsAvailable()) OfflineLLHScore.SetActive(active);
     if(OfflineLLHLoose.IsAvailable()) OfflineLLHLoose.SetActive(active);
     if(OfflineLLHMedium.IsAvailable()) OfflineLLHMedium.SetActive(active);
     if(OfflineLLHTight.IsAvailable()) OfflineLLHTight.SetActive(active);
     if(OfflineBDTLoose.IsAvailable()) OfflineBDTLoose.SetActive(active);
     if(OfflineBDTMedium.IsAvailable()) OfflineBDTMedium.SetActive(active);
     if(OfflineBDTTight.IsAvailable()) OfflineBDTTight.SetActive(active);
     if(TruthPt.IsAvailable()) TruthPt.SetActive(active);
     if(TruthEta.IsAvailable()) TruthEta.SetActive(active);
     if(TruthPhi.IsAvailable()) TruthPhi.SetActive(active);
     if(TruthNumTrack.IsAvailable()) TruthNumTrack.SetActive(active);
     if(NVertices.IsAvailable()) NVertices.SetActive(active);
     if(CalRadius.IsAvailable()) CalRadius.SetActive(active);
     if(CentFrac.IsAvailable()) CentFrac.SetActive(active);
     if(DrMax.IsAvailable()) DrMax.SetActive(active);
     if(EtOverPtLeadTrk.IsAvailable()) EtOverPtLeadTrk.SetActive(active);
     if(IpSigLeadTrk.IsAvailable()) IpSigLeadTrk.SetActive(active);
     if(Lead2ClusterEOverAllClusterE.IsAvailable()) Lead2ClusterEOverAllClusterE.SetActive(active);
     if(MassTrkSys.IsAvailable()) MassTrkSys.SetActive(active);
     if(NTracksdrdR.IsAvailable()) NTracksdrdR.SetActive(active);
     if(TrFligthPathSig.IsAvailable()) TrFligthPathSig.SetActive(active);
     if(TrkAvgDist.IsAvailable()) TrkAvgDist.SetActive(active);
    }
    else
    {
      Pt.SetActive(active);
      Eta.SetActive(active);
      Phi.SetActive(active);
      NumTrack.SetActive(active);
      OfflinePt.SetActive(active);
      OfflineEta.SetActive(active);
      OfflinePhi.SetActive(active);
      OfflineNumTrack.SetActive(active);
      OfflineLLHScore.SetActive(active);
      OfflineLLHLoose.SetActive(active);
      OfflineLLHMedium.SetActive(active);
      OfflineLLHTight.SetActive(active);
      OfflineBDTLoose.SetActive(active);
      OfflineBDTMedium.SetActive(active);
      OfflineBDTTight.SetActive(active);
      TruthPt.SetActive(active);
      TruthEta.SetActive(active);
      TruthPhi.SetActive(active);
      TruthNumTrack.SetActive(active);
      NVertices.SetActive(active);
      CalRadius.SetActive(active);
      CentFrac.SetActive(active);
      DrMax.SetActive(active);
      EtOverPtLeadTrk.SetActive(active);
      IpSigLeadTrk.SetActive(active);
      Lead2ClusterEOverAllClusterE.SetActive(active);
      MassTrkSys.SetActive(active);
      NTracksdrdR.SetActive(active);
      TrFligthPathSig.SetActive(active);
      TrkAvgDist.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void LLHTrigEFTau::ReadAllActive()
  {
    if(Pt.IsActive()) Pt();
    if(Eta.IsActive()) Eta();
    if(Phi.IsActive()) Phi();
    if(NumTrack.IsActive()) NumTrack();
    if(OfflinePt.IsActive()) OfflinePt();
    if(OfflineEta.IsActive()) OfflineEta();
    if(OfflinePhi.IsActive()) OfflinePhi();
    if(OfflineNumTrack.IsActive()) OfflineNumTrack();
    if(OfflineLLHScore.IsActive()) OfflineLLHScore();
    if(OfflineLLHLoose.IsActive()) OfflineLLHLoose();
    if(OfflineLLHMedium.IsActive()) OfflineLLHMedium();
    if(OfflineLLHTight.IsActive()) OfflineLLHTight();
    if(OfflineBDTLoose.IsActive()) OfflineBDTLoose();
    if(OfflineBDTMedium.IsActive()) OfflineBDTMedium();
    if(OfflineBDTTight.IsActive()) OfflineBDTTight();
    if(TruthPt.IsActive()) TruthPt();
    if(TruthEta.IsActive()) TruthEta();
    if(TruthPhi.IsActive()) TruthPhi();
    if(TruthNumTrack.IsActive()) TruthNumTrack();
    if(NVertices.IsActive()) NVertices();
    if(CalRadius.IsActive()) CalRadius();
    if(CentFrac.IsActive()) CentFrac();
    if(DrMax.IsActive()) DrMax();
    if(EtOverPtLeadTrk.IsActive()) EtOverPtLeadTrk();
    if(IpSigLeadTrk.IsActive()) IpSigLeadTrk();
    if(Lead2ClusterEOverAllClusterE.IsActive()) Lead2ClusterEOverAllClusterE();
    if(MassTrkSys.IsActive()) MassTrkSys();
    if(NTracksdrdR.IsActive()) NTracksdrdR();
    if(TrFligthPathSig.IsActive()) TrFligthPathSig();
    if(TrkAvgDist.IsActive()) TrkAvgDist();
  }

} // namespace D3PDReader
#endif // D3PDREADER_LLHTrigEFTau_CXX
