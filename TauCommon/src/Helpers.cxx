#ifndef TauCommon_Helpers_CXX
#define TauCommon_Helpers_CXX

// TauCommon include(s)
#include "Helpers.h"

namespace TauCommon_Helpers
{
  float ParticlePt(Particle* const& pParticle)
  {
    return pParticle->TLV().Pt()/1000;
  }

  float ParticleEta(Particle* const& pParticle)
  {
    return pParticle->TLV().Eta();
  }
  
  float ParticlePhi(Particle* const& pParticle)
  {
    return pParticle->TLV().Phi();
  }
  float NVtx(D3PDReader::EventInfo const& event)
  {
    return (float)event.lbn();
  }
}

#endif // TauCommon_Helpers_CXX
