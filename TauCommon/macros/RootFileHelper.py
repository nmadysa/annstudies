#!/usr/bin/env python
import sys
import argparse
import ROOT
import Utilities

ROOT.gErrorIgnoreLevel = 4001;
_logger = Utilities.GetLogger('RootFileHelperLog','ERROR')

def GetObjectsOfType(directory,Type):
    """
    retrieves a list of objects with given type by searching recursively a ROOT file
    input: rootfile - a ROOT file object
           type     - the type of objects requested (e.g 'TH1')
                      note: objects of derived types will be returned as well
           dir      - name of the current directory
    output: dictionary{full name of object(including the path) : object of given type}
    """
    # dictionary: name of the object (including full path) = object
    objects = {}
    _logger.debug("directory = '%s'(%s)",directory.GetName(),directory.Class().GetName())
    _logger.debug("type = '%s'",Type)
    # scan current directory for all keys
    if not directory.IsZombie():
        def traverseDir(directory,objects):
            """
            scan directory recursively
            """
            keylist = directory.GetListOfKeys()
            # check objects corresponding to the keys for their type
            for key in keylist:
                _logger.debug("looking for key '%s'",key.GetName())
                obj = directory.Get(key.GetName())
                if obj.Class().InheritsFrom(Type):
                    objects[directory.GetPath().rstrip('/') + '/' + obj.GetName()] = obj
                # if object is a directory -> scan this as well
                elif isinstance(obj,ROOT.TDirectory):
                    traverseDir(obj,objects)
            return objects

        basedir = directory.GetPath().rstrip('/') + '/'
        traverseDir(directory,objects)
        return {k.replace(basedir,""):v for k,v in objects.items()}
    # invalid ROOT file given
    else:
        _logger.critical("invalid ROOT directory pointer received")

def PrintTreeEntries(filelist,treenames):
    """
    prints the numbers of entries in the given trees
    input: filelist  - names of ROOT files to investigate
           treenames - list of treenames to look for
                       If empty, the ROOT files will be scanned for all trees contained.
    output: None
    """
    # dictionary: name of tree = number of entries
    map = {}
    for filename in filelist:
        _logger.info('examining file \'%s\'',filename)
        f = ROOT.TFile.Open(filename)
        if f:
            # if treenames given -> use them
            if treenames:
                for treename in treenames:
                    t = f.Get(treename)
                    if t:
                        _logger.info("tree \'%s\' with %d entries found",treename,t.GetEntries())
                        # treename already present in map -> add number of entries
                        if treename in map:
                            map[treename] = map[treename] + t.GetEntries()
                        # new treename -> insert number of entries
                        else:
                            map[treename] = t.GetEntries()
            # no treenames given -> scan file for TTree objects
            else:
                trees = GetObjectsOfType(f,'TTree')
                for t in trees.itervalues():
                    treename = t.GetName()
                    _logger.info("tree \'%s\' with %d entries",treename,t.GetEntries())
                    # treename already present -> add number of entries
                    if treename in map:
                        map[treename] = map[treename] + t.GetEntries()
                    # new treename -> insert number of entries
                    else:
                        map[treename] = t.GetEntries()
        else:
            _logger.error("couldn't open file \'%s\'",filename)
    print "\nSummary"
    print "======="
    for tree,entries in map.items():
        print "{0:20s}: {1:10d} entries".format(tree,entries)

def __GetObjectsOfTypeWrapper(args):
    """
    wrapper for calling the GetObjectsOfType function when used as standalone executable
    """
    objects = GetObjectsOfType(ROOT.TFile.Open(args.filename[0]),args.type[0])
    if objects:
        for key,value in objects.iteritems():
            print "found: ({0}) at {1}".format(value.Class().GetName(),key)
    
def __PrintTreeEntriesWrapper(args):
    """
    wrapper for calling the PrintTreeEntries function when used as standalone executable
    """
    PrintTreeEntries(args.filelist,args.trees)

def main(argv):
    """
    function which is executed when this module is used as standalone executable
    """
    import logging
    # setup and parse command line parameters
    parser = argparse.ArgumentParser(description='RootFileHelper - usefull methods for examining ROOT files')
    parser.add_argument('--debug',nargs='?',dest='level',choices=['DEBUG','INFO','WARNING','CRITICAL','ERROR'],default='ERROR',help='logging level')
    subparser = parser.add_subparsers(title='commands',
                                      description="These commands specify possible running modes. For details type: 'python RootFileHelper.py <command> -h'",
                                      help='possible execution modes')
    # parameters for looking for objects
    parser_objects = subparser.add_parser('objects',help='scan ROOT file for objects of given type')
    parser_objects.add_argument('filename',nargs=1,help='ROOT file')
    parser_objects.add_argument('type',nargs=1,help='object type')
    parser_objects.set_defaults(func=__GetObjectsOfTypeWrapper)
    # parameters for getting number of tree entries
    parser_entries = subparser.add_parser('entries',help='scan list of root files for number of entries in specified tree(s)')
    parser_entries.add_argument('-t',nargs='+',dest='trees',metavar='tree',help="treenames")
    parser_entries.add_argument('filelist',nargs='+',help="filelist")
    parser_entries.set_defaults(func=__PrintTreeEntriesWrapper)
    args = parser.parse_args()
    _logger.setLevel(eval('logging.' + args.level))
    args.func(args)
    

if __name__ == "__main__":
    main(sys.argv[1:])
