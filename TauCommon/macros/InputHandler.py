import sys
import re
import ROOT
import Samples
import Utilities
import os
from FileHandler import FileHandler as FH
from Samples import Samples, SignalSample
from HistTools import Formatter as HF
from Configurator import Configurator

_logger = Utilities._logger

class CommonReader(object):
    def __init__(self, filelist, ignoreSample = False):
        _logger.debug("Initialize CommonReader")
        self.filelist = filelist
        self.mc_hists = {}
        self.data_hist = None
        self.ignoreSample = ignoreSample

    def updateFilelist(self, filelist):
        self.filelist = filelist

    def getFilelist(self):
        return self.filelist
    
    def readHistogramFromFilelist(self, dist):
        self.data_hist = None
        self.mc_hists = {}
        #get histograms from all given files
        for filename in self.filelist:
            _logger.info("creating HistGetter for file '%s'",filename)
            getter = FH(filename)
            hist = getter.get(dist, 0)
            if self.ignoreSample:
                self.data_hist = hist
                self.data_hist.SetName('Data')
                return
            samplename = self.getSampleNameFromFileName(filename)
            if getter._hasFile:
                getter.close()
            if not hist:
                _logger.warning("histogram '%s' not found in file '%s'",dist,filename)
                continue
            if getter.sample.isData():
                if self.data_hist:
                    self.data_hist.Add(hist)
                else:
                    self.data_hist = hist
                    self.data_hist.SetName('Data')
            else:
                self.mc_hists[samplename] = (hist)
            
    def getMCHists(self):
        return self.mc_hists

    def getDataHists(self):
        return self.data_hist
    
    def getSampleNameFromFileName(self, filename):
        """
        parse the filename and returns the corresponding sample object
        """
        from Samples import Samples
        #get sample name and retrieve sample object
        samplename = self.ParseFileName(filename)["Name"]
        if not samplename in Samples:
            _logger.error("unknow sample with name '%s' parsed from filename '%s'" % (samplename,filename))
        else:
            return samplename

    def ParseFileName(self,filename):
        """
        parse the name of ROOT file and extract some information
        assumed format: <CycleName>.<type>.<sample_name>.<postfix>.root
        input: filename - name of the ROOT file
        output: list of filename components
        """
        filename = os.path.basename(filename)
        _logger.debug("parsing file name '%s'",filename)
        l = filename.split('.')
        if not len(l) in range(4,6):
            _logger.warning("unexpected number of tokens: %i",len(l))
        if l[-1] != "root":
            _logger.warning("file name does not end with '.root'")
        keys = ["CycleName","Type","Name","PostFix"]
        tokens = {'CycleName':None,'Type':None,'Name':None,'PostFix':None}
        for i in range(min(len(keys),len(l))):
            tokens[keys[i]] = l[i]
        return tokens
       
class WeightHandler(Configurator):
    def __init__(self, reader, configFile = 'configs/reader_config.yml', mergeOptions = None, switchOffXsecProvider = False):
        self.reader = reader
        self.mcCutflows = {}
        self.dataCutflows = None
        self.mergeOptions = mergeOptions
        Configurator.__init__(self,
                              configFile)
        
    def setReader(self, reader):
        self.reader = reader
        
    def readSkimCutflow(self):
        _logger.debug("Read cutflows")
        if self.mcCutflows or self.dataCutflows:
            return
        self.reader.readHistogramFromFilelist('SkimSelection/h_cut_flow')
        self.mcCutflows = self.reader.getMCHists()
        self.dataCutflows = self.reader.getDataHists()

    def weightByXSec(self, hists, lumi, ignore = None, tanb = None):
        formatter = HF()
        if tanb:
            ignore = True
        for sample in hists:
            if ignore and type(Samples[sample]) == SignalSample:
                continue
            weight = self.getWeightFromSampleName(sample)
            if not weight:
                continue
            processedEvents = self.getProcessedEventsFromSampleName(sample)
            weight = weight * lumi / processedEvents
            formatter.scale(hists[sample],
                            weight)
        if tanb:
            for sample in hists:
                if not type(Samples[sample]) == SignalSample:
                    continue
                mass = int(re.findall('[0-9]+', sample)[0])
                process = sample.replace(str(mass),'')
                weight = eval('self.xsecProvider.get%sXsecBR(%s, tanb)' % (process, mass))
                hists[sample] = self.reader.weightSingleHistByXSec(hists[sample],
                                                                   sample,
                                                                   lumi,
                                                                   weight)
        return hists
    
    def weightSingleHistByXSec(self, hist, samplename, lumi, xsec):
        formatter = HF()
        processedEvents = self.getProcessedEventsFromSampleName(samplename)
        weight = xsec * lumi / processedEvents
        formatter.scale(hist,
                        weight)
        return hist
                        
    def getWeightFromSampleName(self,samplename):
        sample = Samples[samplename]
        return sample.getWeight()

    def getProcessedEventsFromSampleName(self, samplename):
        if len(self.mcCutflows.keys()) == 0:
            self.readSkimCutflow()
        h = self.mcCutflows[samplename]
        return h.GetBinContent(1)

    def merge(self, hists, mergeOptions):
        tmp = self.mergeByProcess(hists)
        return self.mergeProcesses(tmp, mergeOptions)
        
    def mergeByProcess(self, hists):
        tmp = {}
        for samplename in hists:
            process = self.getProcessFromSampleName(samplename)
            if process == "None":
                print "Cannot assign process to %s" % samplename
            if process in tmp.keys():
                tmp[process].Add(hists[samplename])
            else:
                hists[samplename].SetName(process)
                tmp[process] = hists[samplename]
        return tmp

    def mergeMultiProcesses(self, hists):
        tmp = {}
        for sName in hists.keys():
            if sName.count('ZPrime'):
                tmp[sName] = hists[sName]
                continue
            elif sName.count('bbA'):
                tmp[sName] = hists[sName]
                continue
            elif sName.count('ggH'):
                tmp[sName] = hists[sName]
                continue            
            mp = getMultiProcessBySampleName(sName)
            mpName = mp.name
            if mpName in tmp.keys():
                tmp[mpName].Add(hists[sName])
            else:
                hists[sName].SetName(mpName)
                tmp[mpName] = hists[sName]
        return tmp
    
    def getProcessFromSampleName(self, samplename):
        return Samples[samplename].process

    def mergeProcesses(self, hist, mergeOptions):
        tmp = {}
        try:
            for process, subprocesses in mergeOptions.items():
                for subprocess in subprocesses:
                    if process == 'signal':
                        for key in hist.keys():
                            try:
                                if type(Samples[key]) == SignalSample:
                                    tmp[key] = hist[key]
                            except KeyError:
                                continue
                        continue
                    if not hist.has_key(subprocess):
                        _logger.error("Could not find %s in hist keys" % subprocess)
                        continue
                    try:
                        tmp[process].Add(hist[subprocess])
                    except KeyError:
                        tmp[process] = hist[subprocess].Clone()
            for key, item in tmp.items():
                item.SetName(key)
            return tmp
        
        except AttributeError:
            _logger.warning('No merge options specified')
            return hist

class Reader(CommonReader, WeightHandler):
    def __init__(self, filelist, configFile = 'configs/reader_config.yml', ignoreSample = False, mergeOptions = None):
        _logger.debug("Initialize Reader")
        CommonReader.__init__(self,
                              filelist,
                              ignoreSample)
        WeightHandler.__init__(self,
                               self,
                               configFile,
                               mergeOptions)
        
    def updateFilelist(self, filelist):
        super(Reader, self).updateFilelist(filelist)
        self.mcCutflows = {}
        self.dataCutflows = None
        
    def readHistogramAndMerge(self, dist, lumi, noMerging=False, mergeOptions=None, cut = -1111., ignore = None, tanb = None):
        self.readSkimCutflow()
        self.readHistogramFromFilelist(dist)
        mc_hists = self.getMCHists()
        data = self.getDataHists()
        if type(data) == ROOT.TH2F or (not len(mc_hists) == 0 and type(mc_hists.values()[0]) == ROOT.TH2F):
            mc_hists = self.getProjection(mc_hists, float(cut))
            data = self.getProjection(data, float(cut))
        if not noMerging and mc_hists:
            mc_hists = self.weightByXSec(mc_hists,
                                         lumi,
                                         ignore,
                                         tanb)
            mc_hists = self.merge(mc_hists, mergeOptions)
        return mc_hists, data
    
    
    def getProjection(self, hists, cut):
        bin = None
        if type(hists) == dict:
            tmp = {}
            for key, hist in hists.items():
                if not bin:
                    bin = hist.FindFixBin(cut, hist.GetYaxis().GetXmin() - 1)
                tmp[key] = hist.ProjectionY(hist.GetName() + key+"proj", bin, -1)
            return tmp
        else:
            return hists.ProjectionY(hists.GetName(), hists.FindFixBin(cut, hists.GetYaxis().GetXmin() - 1), -1)
