import argparse
import ROOT
import os
import Utilities

_logger = Utilities._logger

class BaseParser:
    def __init__(self, description = ""):
        self.__parser = argparse.ArgumentParser(description=description)
        self.configure()
        
    def configure(self):
        self.__parser.add_argument('filelist',nargs='+',help='filelist')
        self.__parser.add_argument('--debug',nargs='?',dest='level',choices=['DEBUG','INFO','WARNING','CRITICAL','ERROR'],default='WARNING',help='logging level')
        self.__parser.add_argument('--lumi',type=float,help='integrated luminosity in pb^{-1}, -1 for normalised')
        self.__parser.add_argument('--batch',default=False,action='store_true',help='start batch mode')
        self.__parser.add_argument('--withQCD',default=False,action='store_true',help='plot QCD estimation')
        self.__parser.add_argument('--noData',default=False,action='store_true',help='do not plot data')
        self.__parser.add_argument('--blind',default=None,type=float,help='blind data after cut')

    def getParser(self):
        return self.__parser

class ROOTHandler():
    def __init__(self):
        pass

    def setupEnv(self, batch = False):
        ROOT.gROOT.Macro( os.path.expanduser('~/AtlasStyle/AtlasStyle.C'))
        ROOT.SetAtlasStyle()
        ROOT.gROOT.SetBatch(batch)


class Writer:
    def __init__(self, directory = None):
        if directory is  None:
            directory = os.path.abspath(os.curdir)
        self.dir = directory
        self.__checkAndCreateDirectory(self.dir)
        
    def __checkAndCreateDirectory(self, directory):
        _logger.debug("Check if directory: %s exists" % (directory))
        if not os.path.exists(directory):
            _logger.debug("Create directory: %s exists" % (directory))
            os.makedirs(directory)

    def dumpCanvas(self, canvas, message=None, image = None):
        if image:
            self.writeCanvasToFile(canvas, image)
        else:
            if message is None:
                image = raw_input("save canvas as (<RET> for skipping): ")
            else:
                image = raw_input(message)
            
            if image:
                self.writeCanvasToFile(canvas, image)

    def writeCanvasToFile(self, canvas, name, extension='pdf'):
        ext = self.parseExtensionFromFileName(name)
        if ext is not None:
            extension = ext
            name = ''.join(name.split('.')[0:-1])
        if not extension.startswith('.'):
            extension = '.' + extension
        if extension == '.root':
            self.writeObjectToROOTFile(canvas,
                                       name+extension)
        else:
            canvas.SaveAs(os.path.join(os.path.join(self.dir,
                                                    name+extension)))

    def writeObjectToROOTFile(self, obj, filename, dir=''):
        f = ROOT.gROOT.GetListOfFiles().FindObject(filename)
        if not f:
            f = ROOT.TFile.Open(filename, 'UPDATE')
        d = f.GetDirectory(dir)
        if not d:
            d = make_root_dir(f, dir)
        d.cd()
        obj.Write()
            
    def parseExtensionFromFileName(self, name):
        ext = name.split('.')[-1]
        if ext is name:
            return None
        return ext
    
    def setDirectory(self, directory):
        self.__checkAndCreateDirectory(directory)
        self.dir = directory
        
def makeGraph(x, y):
    from array import array
    """
    Basic function for plotting a graph of the data in the lists x and y.
    Returns a dictionary with 'graph' and 'canvas' keys.
    """
    assert len(x) == len(y)
    if not isinstance(x, array):
        x = array('f', x)
    if not isinstance(y, array):
        y = array('f', y)
    graph = ROOT.TGraph(len(x), x, y)
    ROOT.SetOwnership(graph, False)
    return graph
