import ROOT
import os
from HistFormatter import CanvasOptions as CO
from HistFormatter import PlotOptions as PO
from HistFormatter import LegendOptions as LO
from Samples import Samples
from Samples import find_sample_by_short_name
import Utilities
from array import array
import re
from math import sqrt
_logger = Utilities._logger

class Formatter:
    def __init__(self):
        pass
    
    def rebin(self, hist, factor):
        if factor is None:
            return hist
        if type(hist) == dict:
            if type(factor) is int:
                for key, value in hist.items():
                    if type(value) == list:
                        for h in value:
                            try:
                                hist[key].append(self.__rebinTH1(h, factor))
                            except KeyError:
                                hist[key] = [self.__rebinTH1(h, factor)]
                    else:
                        hist[key] = self.__rebinTH1(value, factor)
            elif type(factor) == list:
                binning = array('d', factor)
                nbins = len(factor)
                _logger.debug('rebin histogram asymmetrically')
                for key, value in hist.items():
                    hist[key] = self.__rebinAsymmetricTH1(value, nbins, binning)
            else:
                _logger.error('Invalid binning: ' + str(factor))
            return hist
        
        else:
            if type(factor) is int:
                hist = self.__rebinTH1(hist, factor)
            elif type(factor) == list:
                binning = array('d', factor)
                _logger.debug('rebin histogram %s asymmetrically' % (hist.GetName()))
                hist = self.__rebinAsymmetricTH1(hist, len(factor), binning)
            else:
                _logger.error('Invalid binning: ' + str(factor))
            return hist
                
    def __rebinTH1(self, hist, factor):
        ytitle = hist.GetYaxis().GetTitle()
        if factor:
            try:
                try:
                    binning = float(re.findall('[0-9].+', ytitle)[0])
                except ValueError:
                    binning = int(re.findall('[0-9]+', ytitle)[0])
                ytitle = ytitle.replace(str(binning), str(binning*factor))
            except IndexError, KeyError:
                pass
        hist.GetYaxis().SetTitle(ytitle)
        return hist.Rebin(factor)

    def __rebinAsymmetricTH1(self, hist, nbins, bins):
        hist.GetYaxis().SetTitle(hist.GetYaxis().GetTitle() + ' x %i' % nbins)
        return hist.Rebin(nbins - 1, hist.GetName(), bins)

    def mergeOverflowBins(self, hists, xmax = None):
        if type(hists) == dict:
            for item in hists.values():
                self.__mergeOverflowBinsTH1F(item, xmax)
        else:
            self.__mergeOverflowBinsTH1F(hists, xmax)

    def __mergeOverflowBinsTH1F(self, h, xmax):
        if xmax:
            lastVisibleBin = h.FindBin(xmax)
        else:
            lastVisibleBin = h.GetNbinsX()
        h.SetBinContent(lastVisibleBin, h.GetBinContent(lastVisibleBin) + h.Integral(lastVisibleBin, -1))                        
        
    def scale(self, hist, weight):
        return hist.Scale(weight)

    def normalise(self, hist):
        if type(hist) == dict:
            for h in hist.keys():
                hist[h] = self.__normaliseTH1(hist[h])
        elif type(hist) == list:
            for h in hist:
                h = self.__normaliseTH1(h)
        else:
            hist = self.__normaliseTH1(hist)

    def __normaliseTH1(self, hist):
        integral = hist.Integral()
        if integral == 0:
            return hist
        hist.Scale(1./integral)
        return hist

    def getPlotOptionsFromMultiProcessName(self, name):
        return MultiProcesses[name].plot_options

    def getPlotOptionsFromSampleName(self, samplename):
        return Samples[samplename].plot_options

    def getSignalPlotOptionsFromSampleName(self, samplename):
        return Samples[samplename].signal_plot_options
    def getPlotOptionsFromProcessName(self, process):
        sample = find_sample_by_short_name(process)
        if sample is None:
            _logger.debug('Cannot find process %s. Return default plot options' % (process))
            raise KeyError
        return sample.plot_options


    def getLegendLabelFromSampleName(self, samplename):
        return Samples[samplename].label

    def getLegendLabelFromMultiProcessName(self, samplename):
        return MultiProcesses[samplename].label

    def getLegendLabelFromProcessName(self, process):
        sample = find_sample_by_short_name(process)
        if sample is None:
            return None
        return sample.label

    def addText(self, canvas, text, pos = {'x': 0.6, 'y': 0.79}, size = 0.04):
        t = self.make_text(x = pos['x'],
                           y = pos['y'],
                           text = text,
                           size = size)
        ROOT.SetOwnership(t, False)
        t.Draw('sames')
        canvas.Update()

    def addLumiText(self, canvas, lumi, pos ={'x': 0.6, 'y': 0.79}, size = 0.04, splitLumiText = False):
        canvas.cd()
        if splitLumiText:
            text = self.make_text(x=pos['x'],
                                  y=pos['y'],
                                  text='#scale[0.7]{#int}dt L = %.1f fb^{-1}' % (float(lumi) / 1000.),
                                  size=size)
            textenergy = self.make_text(x=pos['x'],
                                        y=pos['y'] - 0.05,
                                        text='#sqrt{s} = 8 TeV',
                                        size=size)
            ROOT.SetOwnership(textenergy, False)
            textenergy.Draw('sames')
        else:
            text = self.make_text(x=pos['x'],
                                  y=pos['y'],
                                  text='#scale[0.7]{#int}dt L = %.1f fb^{-1}, #sqrt{s} = 8 TeV' % (float(lumi) / 1000.),
                                  size=size)
        text.Draw('sames')
        canvas.Update()
        ROOT.SetOwnership(text, False)
        return canvas

    def addATLASLabel(self, canvas, description = '', pos ={'x': 0.6, 'y': 0.87}, size = 0.05, offset = 0.155):
        atlas = self.make_text(x=pos['x'],
                               y=pos['y'],
                               text='ATLAS',
                               size=size,
                               font = 72)
        descr = self.make_text(x=pos['x'] + offset,
                               y=pos['y'],
                               text=description,
                               size=size,
                               font = 42)
        canvas.cd()
        atlas.Draw('sames')
        descr.Draw('sames')
        canvas.Update()
        ROOT.SetOwnership(atlas, False)
        ROOT.SetOwnership(descr, False)
        return canvas

    def addLegendToCanvas(self, canvas, legendOptions = None, signal = [], signalStrength = 1., overwriteLabels = None, overwriteDrawOptions = None, ncol = 1, textsize = None):
        try:
            stack = Utilities.getObjectsFromCanvasByType(canvas, 'THStack')[0]
            stack.GetHists()[0]
        except IndexError:
            stack = None
        hists = []
        drawOptions = []
        labels = []
        if stack is not None:
            for hist in reversed(stack.GetHists()):
                hists.append(hist)
                drawOptions.append(hist.GetDrawOption())
                try:
                    labels.append(self.getLegendLabelFromMultiProcessName(hist.GetName()))
                except KeyError:
                    labels.append(self.getLegendLabelFromProcessName(hist.GetName()))
        if not signal:
            signal = []
            
        def collectInfo():
            hists.append(hist)
            drawOptions.append(hist.GetDrawOption().rstrip('sames'))
            if hist.GetName() == 'Data':
                labels.append("Data 2012")
            elif hist.GetName() in signal:
               label = hist.GetName()
               if not signalStrength == 1.:
                    label += ' #times {0:3.0f}'.format(signalStrength)
               labels.append(label)
            else:
                labels.append(hist.GetName())
            
        for hist in Utilities.getObjectsFromCanvasByType(canvas, 'TH1'):
            collectInfo()
            if hist.GetName() in signal:
                drawOptions.pop()
                drawOptions.append('L')
                
        for hist in Utilities.getObjectsFromCanvasByType(canvas, 'TGraph'):
            collectInfo()

        for hist in Utilities.getObjectsFromCanvasByType(canvas, 'TF1'):
            collectInfo()
            
        for hist in Utilities.getObjectsFromCanvasByType(canvas, 'TEfficiency'):
            collectInfo()
            
        if not overwriteLabels is None:
            labels = overwriteLabels
        if not overwriteDrawOptions is None:
            drawOptions = overwriteDrawOptions
        legend = self.make_legend(hists,
                                  labels,
                                  drawOptions,
                                  legendOptions)
        legend.SetNColumns(ncol)
        if textsize is not None:
            legend.SetTextSize(textsize)
        canvas.cd()
        legend.Draw('sames')

    def addLineToCanvas(self, canvas, value):
        ymin = canvas.GetUymin() if not canvas.GetLogy() else pow(10,canvas.GetUymin())
        ymax = canvas.GetUymax() if not canvas.GetLogy() else pow(10,canvas.GetUymax())
        line = ROOT.TLine(value,ymin,value,ymax)
        ROOT.SetOwnership(line,False)
        line.SetLineColor(ROOT.kBlack)
        line.SetLineStyle(2)
        line.SetLineWidth(2)
        canvas.cd()
        line.Draw('lsames')
                
    def make_legend(self, hists, labels, draw_options,legendOptions):
        
        #Creates a legend from a list of hists (or graphs).
        if not isinstance(draw_options, list):
            draw_options = [draw_options]*len(hists)
        assert len(hists) == len(labels) == len(draw_options)
        leg = ROOT.TLegend(0.25,0.3,0.7,0.7)
        leg.SetFillStyle(0)
        leg.SetBorderSize(0)
        ROOT.SetOwnership(leg, False)
        #leg.SetFillColor(ROOT.kWhite)
        if legendOptions is not None:
            legendOptions = LO(**legendOptions)
        else:
            legendOptions = LO()

        legendOptions.configure(leg)
        for h, lab, opt in zip(hists, labels, draw_options):
            if not opt in ('P', 'F', 'L'):
            ## assume opt is of the same format as draw_options used with Draw
                if opt.count('P'):
                    if opt.count('E'):
                        opt = 'PL'
                    else:
                        opt = 'P'
                else: # '', 'HIST', etc.
                    opt = 'F'
                if hasattr(h,'sample'):
                    try:
                        if h.sample.isData():
                            opt = 'lp'
                    except AttributeError:
                        pass
            leg.AddEntry(h,lab,opt)
        return leg


    def make_text(self, x, y, text, size=0.05, angle=0, font=42, color=None, NDC=True):
        t = ROOT.TLatex(x, y, text)
        if size:  t.SetTextSize(size)
        if angle: t.SetTextAngle(angle)
        if font:  t.SetTextFont(font)
        if color: t.SetTextColor(color)
        if NDC: t.SetNDC()
        return t
    
    def setTitle(self, hist, title, axis = 'x'):
        if title is None:
            return
        if type(hist) == dict:
            for h in hist.keys():
                hist[h] = self.__setTitle(hist[h], title, axis)        
        else:
            if isinstance(hist, ROOT.TEfficiency):
                #hist.Draw('ap')
                ROOT.gPad.Update()
                graph = hist.GetPaintedGraph()
                self.__setTitle(graph, title, axis)
            else:
                hist = self.__setTitle(hist, title, axis)

    def __setTitle(self, hist, title, axis):
        if axis == 'x':
            hist.GetXaxis().SetTitle(title)
        elif axis == 'y':
            hist.GetYaxis().SetTitle(title)

    def setMaximum(self, graph, maximum, axis = 'y'):
        _logger.debug("Set maximum to %f" %(maximum))
        ROOT.SetOwnership(graph, False)
        if axis == 'y':
            if(isinstance(graph, ROOT.THStack)):
               graph.SetMaximum(maximum)
            elif(isinstance(graph, ROOT.TH1)):
                graph.GetYaxis().SetRangeUser(graph.GetMinimum(),
                                              maximum)
            elif(isinstance(graph, ROOT.TEfficiency)):
                graph.GetPaintedGraph().GetYaxis().SetRangeUser(graph.GetPaintedGraph().GetMinimum(),
                                                                maximum)
                 
        elif axis == 'x':
            graph.GetXaxis().SetRangeUser(0, maximum)

    def setMinimum(self, graph, minimum, axis = 'y'):
        _logger.debug("Set minimum to %f" %(minimum))
        ROOT.SetOwnership(graph, False)
        if axis == 'y':
            if(isinstance(graph, ROOT.THStack)):
               graph.SetMinimum(minimum)
            elif(isinstance(graph, ROOT.TH1)):
                graph.GetYaxis().SetRangeUser(minimum,
                                              graph.GetMaximum())
            elif(isinstance(graph, ROOT.TEfficiency)):
                graph.GetPaintedGraph().GetYaxis().SetRangeUser(minimum,
                                                                graph.GetPaintedGraph().GetMaximum())
        elif axis == 'x':
            graph.GetXaxis().SetRangeUser(minimum, graph.GetXaxis().GetXmax())
            
    def setMinMax(self, graph, minimum = None, maximum = None, axis = 'y'):
        ROOT.SetOwnership(graph, False)
        if minimum is None:
            self.setMaximum(graph,
                            maximum,
                            axis)
            return
        if maximum is None:
            self.setMinimum(graph,
                            minimum,
                            axis)
            return
        if(isinstance(graph, ROOT.THStack)):
            if axis == 'y':
                graph.SetMinimum(minimum)
                graph.SetMaximum(maximum)
            else:
                graph.GetXaxis().SetRangeUser(minimum,maximum)
        elif(isinstance(graph, ROOT.TH1) or isinstance(graph, ROOT.TGraph)):
            if axis == 'y':
                graph.GetYaxis().SetRangeUser(minimum,
                                              maximum)
            else:
                graph.GetXaxis().SetRangeUser(minimum,maximum)
        elif(isinstance(graph, ROOT.TEfficiency)):
            if axis == 'y':
                graph.GetPaintedGraph().GetYaxis().SetRangeUser(minimum,
                                                                maximum)
                graph.GetPaintedGraph().SetMinimum(minimum)
                graph.GetPaintedGraph().SetMinimum(maximum)
            else:
                graph.GetPaintedGraph().GetXaxis().SetRangeUser(minimum,
                                                                maximum)
    def configureRatio(self,
                       ratio,
                       unc = None,
                       hSystUncUp = None,
                       hSystUncDown=None,
                       title = 'Data / Model',
                       xtitle = ''):
        
        canvas = ROOT.TCanvas('ratio', 'ratio', 700, 700)
        canvas.cd()
        if unc is not None:
            systematic = None
            if hSystUncUp is not None and hSystUncDown is not None:
                for bin in range(0, unc.GetNbinsX() + 1):
                    hSystUncUp.SetFillColor(ROOT.kRed)
                    hSystUncUp.SetFillStyle(1001)
                    hSystUncDown.SetFillColor(ROOT.kRed)
                    hSystUncDown.SetFillStyle(1001)
                    hSystUncDown.SetMarkerSize(0)
            unc.SetFillColor(ROOT.kYellow)
            unc.SetFillStyle(1001)
            unc.Draw('E2')
            if hSystUncUp:
                hSystUncUp.Draw("E2sames")
                hSystUncDown.Draw("E2sames")
            unc.Draw("E2sames")
            ratio.Draw('psames')
            unc.GetYaxis().SetTitle(title)
            if hSystUncUp:
                unc.GetYaxis().SetRangeUser(0., 2.)
            else:
                unc.GetYaxis().SetRangeUser(0.5, 1.5)
                
        else:
            if isinstance(ratio, ROOT.TGraph):
                ratio.Draw('ap')
                ratio.GetXaxis().SetTitle(xtitle)
            else:
                ratio.Draw('p')
            ratio.GetYaxis().SetTitle(title)
        canvas.SetGridy()
        return canvas

    def convertSystematicsUncToTGraphAsymErrors(self, systUncUp, systUncDown):
        x, y, xe, yel, yeh = [], [], [], [], []
        for bin in range(0, systUncUp.GetNbinsX() +1):
            x.append(systUncUp.GetBinCenter(bin))
            y.append(1)
            xe.append(systUncUp.GetBinWidth(bin) / 2.)
            yel.append(systUncDown.GetBinError(bin))
            yeh.append(systUncUp.GetBinError(bin))
        return self.makeGraphAsymErrors("systematics",
                                        x, y,
                                        xe, xe,
                                        yel, yeh)
    
    def configureSignificance(self, significances, signal):
        canvas = ROOT.TCanvas('ratio', 'ratio', 800, 600)
        canvas.cd()
        significances[0].Draw('al')
        significances[0].GetXaxis().SetTitle(signal.GetXaxis().GetTitle())
        significances[0].GetYaxis().SetTitle('S / #sqrt{B}')
        significances[0].SetLineColor(ROOT.kBlue)
        significances[0].SetFillColor(ROOT.kWhite)
        significances[1].Draw('lsames')
        significances[1].SetLineColor(ROOT.kRed)
        significances[1].SetFillColor(ROOT.kWhite)
        self.addLegendToCanvas(canvas,
                               overwriteLabels = ['>', '<'],
                               overwriteDrawOptions = ['LF', 'LF'])
        return canvas

    def configureTEfficiency(self, graph, plotOptions):
        ROOT.SetOwnership(graph, False)
        if plotOptions is not None:
            plotOptions.configure(graph)

    def makeGraphAsymErrors(self,
                            name,
                            x, y,
                            xel, xeh,
                            yel, yeh,
                            title='',
                            xmin=None, xmax=None,
                            ):
        """
        Basic function for converting a arrays to a graph of the data in the lists x and y.
        Returns a dictionary with 'graph' and 'canvas' keys.
        """
        assert len(x) == len(y)
        if not isinstance(x, array):
            x = array('f', x)
        if not isinstance(y, array):
            y = array('f', y)
        if not isinstance(yel, array):
            yel = array('f', yel)
        if not isinstance(yeh, array):
            yeh = array('f', yeh)
        if not isinstance(xel, array):
            xel = array('f', xel)
        if not isinstance(xeh, array):
            xeh = array('f', xeh)
        
        g = ROOT.TGraphAsymmErrors(len(x), x, y, xel, xeh, yel, yeh)
        ROOT.SetOwnership(g, False)
        g.SetName(name)
        g.SetTitle(title)
        return g

class Plotter:
    def __init__(self, ordering = None):
        self.ordering = ordering
        self.formatter = Formatter()
        self.signalColors = [ROOT.kRed, ROOT.kBlue]        

    def plotHistograms(self, hists,
                       plotOptions = None,
                       drawOptions = None,
                       canvasName = '',
                       canvasTitle = '',
                       canvasOptions=None):
        if canvasOptions is not None:
            canvasOptions.configure(canvas)
        else:
            canvasOptions = CO()
        if plotOptions is not None:
            assert(len(hists) == len(plotOptions))
            for i in range(len(hists)):
                plotOptions[i].configure(hists[i])
                       
        canvas = canvasOptions.create(canvasName, canvasTitle)
        canvas.cd()
        if drawOptions is None:
            drawOptions = ['hist' for i in range(len(hists))]
        hists[0].Draw(drawOptions[0])
        for hist in range(1,len(hists)):
            hists[hist].Draw(drawOptions[hist] + 'sames')

        return canvas

    def plotHist(self, hist,
                 plotOptions = None,
                 drawOptions = None,
                 canvasName = 'c',
                 canvasTitle = '',
                 canvasOptions = None,
                 ymaximum = 0.):

        ROOT.SetOwnership(hist, False)
        
        if canvasOptions is not None:
            canvasOptions = CO(**canvasOptions)
        else:
            canvasOptions = CO()
        if plotOptions is not None:
            plotOptions.configure(hist)

        canvas = canvasOptions.create(canvasName, canvasTitle)
        canvasOptions.configure(canvas)
        canvas.cd()
        if drawOptions is None:
            drawOptions = 'hist'
        hist.SetFillColor(ROOT.kRed)
        hist.Draw(drawOptions)
        return canvas, hist

    def plotGraph(self, graph,
                 plotOptions = None,
                 drawOptions = None,
                 canvasName = '',
                 canvasTitle = '',
                 canvasOptions = None,
                 ymaximum = 0.):

        ROOT.SetOwnership(graph, False)
        if canvasOptions is not None:
            canvasOptions = CO(**canvasOptions)
        else:
            canvasOptions = CO()
        if plotOptions is not None:
                plotOptions.configure(graph)

        canvas = canvasOptions.create(canvasName, canvasTitle)
        canvasOptions.configure(canvas)
        canvas.cd()
        if drawOptions is None:
            drawOptions = 'ap'
        graph.Draw(drawOptions)
        return canvas, graph

    def addGraphToCanvas(self, canvas, graph, plotOptions = None, drawOptions = None):
        canvas.cd()
        if plotOptions is not None:
                plotOptions.configure(graph)
        if drawOptions is None:
            drawOptions = 'psame'
        if not drawOptions.endswith('same'):
            drawOptions += 'same'
        graph.Draw(drawOptions)
        
    def plotStack(self, hists,
                  plotOptions = None,
                  drawOptions = None,
                  canvasName = '',
                  canvasTitle = '',
                  canvasOptions=None,
                  yminimum = 0.01,
                  ymaximum = 0.):
        if canvasOptions is not None:
            canvasOptions = CO(**canvasOptions)
        else:
            canvasOptions = CO()
        
        if plotOptions is not None:
            assert(len(hists) == len(plotOptions))
            for key in hists.keys():
                plotOptions[key].configure(hists[key])

        canvas = canvasOptions.create(canvasName, canvasTitle)
        canvasOptions.configure(canvas)
        canvas.cd()
        if drawOptions is None:
            drawOptions = {}
            for key in hists.keys():
                drawOptions[key] = 'hist'
        stack = ROOT.THStack('', '')
        ROOT.SetOwnership(stack, False)
        #stack.SetMinimum(0.05);
        if ymaximum:
            stack.SetMaximum(ymaximum)
        #if yminimum:
        #    stack.SetMinimum(yminimum)
        xtitle = None
        ytitle = None
        if self.ordering is None:
            self.ordering = hists.keys()
        for key in reversed(self.ordering):
            try:
                stack.Add(hists[key], drawOptions[key])
            except KeyError:
                _logger.debug('Could not add %s to stack' % (key))
            try:
                if xtitle is None:
                    xtitle = hists[key].GetXaxis().GetTitle()
                    ytitle = hists[key].GetYaxis().GetTitle()
            except KeyError:
                continue
            
        stack.Draw()
        self.formatter.setTitle(stack,
                                title = xtitle,
                                axis = 'x')
        self.formatter.setTitle(stack,
                                title = ytitle,
                                axis = 'y')
        return canvas, stack    

    def addDataToStack(self, canvas, data, blind = None):
        if not blind is None:
            for bin in range(data.GetNbinsX() + 1):
                if data.GetBinCenter(bin) < blind:
                    continue
                else:
                    data.SetBinContent(bin,
                                       0.)
                    data.SetBinError(bin,
                                     0.)

        canvas.cd()
        data.Draw("psames")

    def addSignalToStack(self, canvas, signal, signalStrength = 1.):
        canvas.cd()
        for process in signal:
            process.SetLineColor(self.signalColors[signal.index(process)])
            process.Scale(signalStrength)
            process.Draw("histsames")

    def addHistToCanvas(self, canvas, hist, plotOptions = None, drawOptions = None):
        canvas.cd()
        if plotOptions is not None:
            plotOptions.configure(hist)
        if drawOptions is None:
            drawOptions = 'hist'
        hist.Draw(drawOptions + 'sames')
        
    def addStatisticalUncToCanvas(self, canvas, unc):
        canvas.cd()
        unc.SetFillColor(ROOT.kBlack)
        unc.SetFillStyle(3005)
        unc.SetMarkerColor(ROOT.kBlack)
        unc.SetMarkerSize(0.2)
        unc.Draw("E2sames")

    def reset_frame_text( self, fr ):
        xaxis = fr.GetXaxis()
        yaxis = fr.GetYaxis()
        gs = ROOT.gStyle
        yaxis.SetTitleSize( gs.GetTitleSize('Y') )
        yaxis.SetLabelSize( gs.GetLabelSize('Y') )
        yaxis.SetTitleOffset( gs.GetTitleOffset('Y') )
        yaxis.SetLabelOffset( gs.GetLabelOffset('Y') )
        xaxis.SetTitleSize( gs.GetTitleSize('X') )
        xaxis.SetLabelSize( gs.GetLabelSize('X') )
        xaxis.SetTickLength( gs.GetTickLength('X') )

    def scale_frame_text( self, fr, scale ):
        xaxis = fr.GetXaxis()
        yaxis = fr.GetYaxis()
        yaxis.SetTitleSize( yaxis.GetTitleSize() * scale )
        yaxis.SetLabelSize( yaxis.GetLabelSize() * scale )
        yaxis.SetTitleOffset( 1.1* yaxis.GetTitleOffset() / scale  )
        yaxis.SetLabelOffset( yaxis.GetLabelOffset() * scale )
        xaxis.SetTitleSize( xaxis.GetTitleSize() * scale )
        xaxis.SetLabelSize( xaxis.GetLabelSize() * scale )
        xaxis.SetTickLength( xaxis.GetTickLength() * scale )
        xaxis.SetTitleOffset( 2.5* xaxis.GetTitleOffset() / scale  )
        xaxis.SetLabelOffset( 2.5* xaxis.GetLabelOffset() / scale )
        
    def addRatioToCanvas(self, canvas, ratio, ymin = None, ymax = None, ytitle = None):
        y_frac = 0.25
        try:
            hratio = Utilities.getObjectsFromCanvasByType(ratio, "TH1F")[0]
        except IndexError:
            hratio =  Utilities.getObjectsFromCanvasByType(ratio, "TGraph")[0]
        if not canvas or not ratio:
            _logger.debug('No canvas and/or ratio passed')
            return None
        canvas_options = CO()
        c = canvas_options.create('test')
        ROOT.SetOwnership(c, False)
        c.Draw()
        pad1 = ROOT.TPad("pad1","top pad",0.0,y_frac,1.,1.)
        pad1.SetBottomMargin(0.05)
        pad1.Draw()
        pad2 = ROOT.TPad("pad2","bottom pad",0,0.,1,((1 - y_frac) * canvas.GetBottomMargin() / y_frac + 1) * y_frac)
        pad2.SetTopMargin(0.5)
        pad2.SetBottomMargin(0.1)
        pad2.Draw()
        pad1.cd()
        try:
            stack = Utilities.getObjectsFromCanvasByType(canvas, "THStack")[0]
            stack.GetXaxis().SetTitleSize(0)
            stack.GetXaxis().SetLabelSize(0)
            scale = 1./(1.-y_frac) 
            self.scale_frame_text( stack, scale ) 	
        except IndexError:
            pass
        canvas.DrawClonePad()
        pad2.cd()
        hratio.GetYaxis().SetNdivisions(505)
        hratio.GetXaxis().SetNdivisions(505)
        scale = 1./y_frac - 1.5
        self.reset_frame_text( hratio )
        self.scale_frame_text( hratio, scale )
        ratio.Update()
        ratio.SetBottomMargin(0.4)
        ratio.DrawClonePad()
        xlow = pad2.GetUxmin()
        xup = pad2.GetUxmax()
        line = ROOT.TLine(xlow,1,xup,1)
        line.Draw('same')
        pad2.Update()
        c.line = line
        pad = c.cd(1)
        if ymin is not None or ymax is not None:
            isFirst = True
            for obj in Utilities.getObjectsFromCanvasByType(pad1, "TEfficiency"):
                print obj
                if ytitle and isFirst:
                    Formatter().setTitle(obj,
                                         ytitle,
                                         axis = 'y')
                    isFirst = False
                Formatter().setMinMax(obj,
                                      ymin,
                                      ymax)
                pad1.Update()
        pad.Update()
        pad.Modified()
        return c
        

    
