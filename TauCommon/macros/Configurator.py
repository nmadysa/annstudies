import yaml

class Configurator(object):
    """
    """
    
    def __init__(self, config):
        """
        """
        self.readConfig(config)

    def readConfig(self, fConfigName):
        fConfig = open(fConfigName)
        config = yaml.load(fConfig)
        try:
            for key,option in config.items():
                exec("self.%s = %s" %(key, option))
        except AttributeError:
            pass
        fConfig.close()
        
