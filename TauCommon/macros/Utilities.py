#!/usr/bin/env python
import os
import logging
import ROOT
from math import sqrt

default = object()

class DefaultError:
    """
    base class for displaying custom error messages
    """
    def __init__(self,msg):
        """
        init with message
        """
        self.msg = msg

    def __str__(self):
        """
        print the message
        """
        return repr(self.msg)


def GetLogger(name,level):
    """
    returns a logging object
    input: name  - name of the logging instance
           level - severity level for displayed messages
    output: logging  instance of type logging.Logger
    """
    logger = logging.getLogger(name)
    hdl = logging.StreamHandler()
    form = logging.Formatter('[%(asctime)s in %(funcName)s at %(lineno)s] %(levelname)s: %(message)s')
    hdl.setFormatter(form)
    logger.addHandler(hdl)
    logger.setLevel(eval('logging.' + level))
    logger.propagate = 0
    return logger

_logger = GetLogger('UtilitiesLogger','WARNING')

def ParseFileName(filename):
    """
    parse the name of ROOT file and extract some information
    assumed format: <CycleName>.<type>.<sample_name>.<postfix>.root
    input: filename - name of the ROOT file
    output: list of filename components
    """
    filename = os.path.basename(filename)
    _logger.info("parsing file name '%s'",filename)
    l = filename.split('.')
    if not len(l) in range(4,10):
        _logger.warning("unexpected number of tokens: %i",len(l))
    if l[-1] != "root":
        _logger.warning("file name does not end with '.root'")
    keys = ["CycleName","Type","Name","PostFix"]
    tokens = {'CycleName':None,'Type':None,'Name':None,'PostFix':None}
    for i in range(min(len(keys),len(l))):
        tokens[keys[i]] = l[i]
    return tokens

def openROOTFile(filename,state="READ"):
    """
    open a ROOT file
    """
    _logger.info("open ROOT file '%s' in '%s' state",filename,state)
    f= ROOT.TFile.Open(filename,state)
    if not f:
        raise DefaultError("couldn't open ROOT file with name '%s'" % filename)
    else:
        return f

def normaliseHistogram(hist):
    if not hist:
        _logger.error('received invalid histogram for normalising')
        return hist
    if not hist.Class().InheritsFrom("TH1"):
        _logger.error('given object is not an histogram')
        return hist
    # calculate integral including under/overflow
    integral = 0
    if isinstance(hist,ROOT.TH1):
        integral = hist.Integral(0,hist.GetNbinsX() + 1)
    if isinstance(hist,ROOT.TH2):
        integral = hist.Integral(0,hist.GetNbinsX() + 1,0,hist.GetNbinsY() + 1)
    if isinstance(hist,ROOT.TH3):
        integral = hist.Integral(0,hist.GetNbinsX() + 1,0,hist.GetNbinsY() + 1,0,hist.GetNbinsZ() + 1)
    if not integral:
        _logger.error('given histogram is empty, integral = %.3f', integral)
        return hist
    # normalise histogram
    hist.Scale(1/integral)
    return hist

def scaleHistogram(hist,sample,lumi,mc_events):
    """
    scale a histogram from sample to given lumi in pb^{-1}
    mc_events = (total, total weighted)
    """
    if not hist:
        _logger.error('received invalid histogram for normalising')
        return hist
    if not hist.Class().InheritsFrom("TH1"):
        _logger.error('given object is not an histogram')
        return hist
    if not sample:
        _logger.error('received invalid sample for scaling histogram')
        return hist
    if lumi <= 0:
        _logger.error('invalid luminosity of %.2f received for scaling',lumi)
        return hist
    if mc_events[1] <= 0:
        _logger.error('invalid number of MC events %i received for scaling',mc_events[1])
        return hist
    # check number of events
    if mc_events[0] != sample.generatedEvents:
        _logger.error("'%s': number of processed events %.0f does not match expected number of generated events %.0f",sample.name,mc_events[0],sample.generatedEvents)
    # scale histogram
    hist.Scale(sample.lumiWeight(mc_events[1],lumi))
    return hist
    
def getSampleFromFileName(filename):
    """
    parse the filename and returns the corresponding sample object
    """
    from Samples import Samples
    # get sample name and retrieve sample object
    samplename = ParseFileName(filename)["Name"]
    if not samplename in Samples:
        raise DefaultError("unknow sample with name '%s' parsed from filename '%s'" % (samplename,filename))
    else:
        return Samples[samplename]

def getObjectsFromCanvasByType(canvas,type):
    """
    looks for an object of the given type in the canvas
    """
    if not canvas:
        return None
    objects = []
    for p in canvas.GetListOfPrimitives():
        if p.Class().InheritsFrom(type):
            objects.append(p)
    return objects
    
def getObjectsFromCanvasByName(canvas,name):
    """
    looks for an object of the given type in the canvas
    """
    if not canvas:
        return None
    objects = []
    for p in canvas.GetListOfPrimitives():
        if p.GetName() == name:
            objects.append(p)
    return objects

def write_to_root_file(obj, filename, dir=''):
    """
    stores the given object in a root file
    """
    f = ROOT.gROOT.GetListOfFiles().FindObject(filename)
    if not f:
        f = ROOT.TFile.Open(filename, 'UPDATE')
    d = f.GetDirectory(dir)
    if not d:
        d = make_root_dir(f, dir)
    d.cd()
    obj.Write()

def make_root_dir(f, dir):
    """
    (recursively) creates (sub)directories in a rootfile
    """
    dir.rstrip('/')
    dir_split = dir.split('/')
    lead_dir = dir_split[0]
    sub_dirs = dir_split[1:]

    d = f.GetDirectory(lead_dir)
    if not d:
        d = f.mkdir(lead_dir)
    
    if sub_dirs:
        return make_root_dir(d, '/'.join(sub_dirs))
    else:
        return d
    
def Significance(signal,background):
    """
    significance used for measurments
    """
    return float(signal)/sqrt(signal + background) if (signal + background)>0 else 0

def observationSig(signal,background):
    """
    significance used for observations
    """
    return float(signal)/sqrt(background) if background>0 else 0

def purity(signal,background):
    """
    returns the purity
    """
    return float(signal)/(signal + background) if (signal + background)>0 else 0
