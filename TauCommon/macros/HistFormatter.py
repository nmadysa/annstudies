"""
HistFormatter

Module for formatting histograms, drawing multiple histograms on the same
canvas, and drawing stacks of histograms.
"""
#------------------------------------------------------------------------------

from array import array
import ROOT
import Utilities
from Utilities import default

black = ROOT.kBlack
white = ROOT.kWhite
red = ROOT.kRed+1
blue = ROOT.kBlue+2
green = ROOT.kGreen+2
gray = ROOT.kGray+2
light_gray = ROOT.kGray+1
brown = ROOT.kOrange+3
orange = ROOT.kOrange+7
cyan = ROOT.kCyan+1
bright_red = ROOT.kRed
violet = ROOT.kViolet-5
yellow = ROOT.kOrange-2
light_green = ROOT.kGreen-7
light_blue = ROOT.kAzure-2
my_colors = [blue, red, gray, green, orange, brown, black, light_gray, cyan,
          bright_red, violet, yellow, light_green, light_blue]

fill_style_hollow = 1001
fill_style_solid = 1001
fill_style_diag1 = 3354
fill_style_diag2 = 3345
          
my_fill_styles_lines = [3004, 3005, 3006, 3007, 3345, 3354, 3315, 3351, 1001, 1001]

#------------------------------------------------------------------------------
# PlotOptions Class
#------------------------------------------------------------------------------
class PlotOptions(object):
    """Class for configuring histogram/graph properties.""" 
    def __init__(self, **kw):
        kw.setdefault('line_color', default)
        kw.setdefault('line_width', default)
        kw.setdefault('line_style', default)
        kw.setdefault('fill_color', default)
        kw.setdefault('fill_style', default)
        kw.setdefault('marker_style', default)
        kw.setdefault('marker_color', default)
        kw.setdefault('marker_size', default)
        for k,v in kw.iteritems():
            setattr(self, k, v)
#______________________________________________________________________________
    def configure(self, h):
        if not self.line_color is default:
            h.SetLineColor(self.line_color)
        if not self.line_width is default:
            h.SetLineWidth(self.line_width)
        if not self.line_style is default:
            h.SetLineStyle(self.line_style)
        if not self.fill_color is default:
            h.SetFillColor(self.fill_color)
        if not self.fill_style is default:
            h.SetFillStyle(self.fill_style)
        if not self.marker_style is default:
            h.SetMarkerStyle(self.marker_style)
        if not self.marker_color is default:
            h.SetMarkerColor(self.marker_color)
        if not self.marker_size is default:
            h.SetMarkerSize(self.marker_size)

#------------------------------------------------------------------------------
# LegendOption Class
#------------------------------------------------------------------------------
class LegendOptions(object):
    """Class for configuring canvas properties.""" 
#______________________________________________________________________________
    def __init__(self,**kw):
        kw.setdefault('x1', 0.65)
        kw.setdefault('y1', 0.6)
        kw.setdefault('x2', 0.85)
        kw.setdefault('y2', 0.9)
        kw.setdefault('n_col', 1)
        kw.setdefault('col_sep', default)
        kw.setdefault('margin', default)
        kw.setdefault('header', default)
        kw.setdefault('entry_sep', default)
        for k,v in kw.iteritems():
            setattr(self, k, v)
#______________________________________________________________________________
    def configure(self,l):
        l.SetNColumns(self.n_col)
        l.SetX1(self.x1)
        l.SetY1(self.y1)
        l.SetX2(self.x2)
        l.SetY2(self.y2)
        if not self.col_sep is default:
            l.SetColumnSeparation(self.col_sep)
        if not self.margin is default:
            l.SetMargin(self.margin)
        if not self.header is default:
            l.SetHeader(self.header)
        if not self.entry_sep is default:
            l.SetEntrySeparation(self.entry_sep)
    
    
#------------------------------------------------------------------------------
# CanvasOptions Class
#------------------------------------------------------------------------------
class CanvasOptions(object):
    """Class for configuring canvas properties.""" 
#______________________________________________________________________________
    def __init__(self, **kw):
        kw.setdefault('width', 900)
        kw.setdefault('height', 700)
        kw.setdefault('log_x', 0)
        kw.setdefault('log_y', 0)
        kw.setdefault('log_z', 0)
        kw.setdefault('grid_x', 0)
        kw.setdefault('grid_y', 0)
        kw.setdefault('tick_x', 1)
        kw.setdefault('tick_y', 1)
        kw.setdefault('left_margin', default)
        kw.setdefault('right_margin', default)
        kw.setdefault('top_margin', default)
        kw.setdefault('bottom_margin', default)
        for k,v in kw.iteritems():
            setattr(self, k, v)
#______________________________________________________________________________
    def configure(self, c):
        c.SetLogx(self.log_x)
        c.SetLogy(self.log_y)
        c.SetLogz(self.log_z)
        if not self.grid_x is default:
            c.SetGridx(self.grid_x)
        if not self.grid_y is default:
            c.SetGridy(self.grid_y)
        if not self.tick_x is default:
            c.SetTickx(self.tick_x)
        if not self.tick_y is default:
            c.SetTicky(self.tick_y)
        if not self.left_margin is default:
            c.SetLeftMargin(self.left_margin)
        if not self.right_margin is default:
            c.SetRightMargin(self.right_margin)
        if not self.top_margin is default:
            c.SetTopMargin(self.top_margin)
        if not self.bottom_margin is default:
            c.SetBottomMargin(self.bottom_margin)
        c.Update()
#______________________________________________________________________________
    def create(self, name, title=default):
        if title is default:
            title = name
        c = ROOT.TCanvas(name, title,
                200, 10,
                self.width, self.height)
        self.configure(c)
        return c

            

#------------------------------------------------------------------------------
# Free Functions
#------------------------------------------------------------------------------

#______________________________________________________________________________
def set_max(hists, factor=1.1, limit=None):
    """
    Sets the maximum of all histograms or graphs in the list hists to be the
    factor times the maximum among hists.  If limit is specified, then the
    maxiumum cannot be set above that.
    """
    import __builtin__
    if not hists:
        return
    if not isinstance(hists,list):
        hists = [hists]
    maxs = []
    for h in hists:
        if isinstance(h, ROOT.TGraph) or isinstance(h, ROOT.TGraphErrors) or isinstance(h, ROOT.TGraphAsymmErrors): 
            maxs.append(h.GetHistogram().GetMaximum())
        elif h.Class().InheritsFrom("TH1"):
            maxs.append(h.GetMaximum())
    m = factor * __builtin__.max(maxs)
    if not limit is None and m > limit:
        m = limit
    for h in hists:
        h.SetMaximum(m)
    return m

#______________________________________________________________________________
def set_min(hists, factor=0.9, limit=None):
    """
    Sets the minimum of all histograms or graphs in the list hists to be the
    factor times the minimum among hists.  If limit is specified, then the
    miniumum cannot be set below that.
    """
    import __builtin__
    mins = []
    for h in hists:
        if isinstance(h, ROOT.TGraph) or isinstance(h, ROOT.TGraphErrors) or isinstance(h, ROOT.TGraphAsymmErrors): 
            mins.append(h.GetHistogram().GetMinimum())
        else:
            mins.append(h.GetMinimum())
    m = factor * __builtin__.min(mins)
    if not limit is None and m < limit:
        m = limit
    for h in hists:
        h.SetMinimum(m)
    return m

#______________________________________________________________________________
def make_legend(hists, labels, draw_options=default,legend_opt=default):
    """
    Creates a legend from a list of hists (or graphs).
    """
    if draw_options is default:
        draw_options = ['P']*len(hists)
    if not isinstance(draw_options, list):
        draw_options = [draw_options]*len(hists)
    assert len(hists) == len(labels) == len(draw_options)
    leg = ROOT.TLegend(0.65,0.6,0.85,0.9)
    leg.SetFillColor(ROOT.kWhite)
    if not legend_opt is default:
        legend_opt.configure(leg)
    for h, lab, opt in zip(hists, labels, draw_options):
        if not opt in ('P', 'F', 'L'):
            ## assume opt is of the same format as draw_options used with Draw
            if opt.count('P'):
                if opt.count('E'):
                    opt = 'PL'
                else:
                    opt = 'P'
            else: # '', 'HIST', etc.
                opt = 'F'
        if hasattr(h,'sample'):
            if h.sample.isData():
                opt = 'lp'
        leg.AddEntry(h,lab,opt)
    return leg

#______________________________________________________________________________
def make_text(x, y, text, size=0.05, angle=0, font=42, color=None, NDC=True):
    t = ROOT.TLatex(x, y, text)
    if size:  t.SetTextSize(size)
    if angle: t.SetTextAngle(angle)
    if font:  t.SetTextFont(font)
    if color: t.SetTextColor(color)
    if NDC: t.SetNDC()
    return t

#______________________________________________________________________________
def make_lumi_text(x=0.55, y=0.82, lumi='XX pb^{-1}', size=0.04):
    t = make_text(x=x, y=y, text='#scale[0.7]{#int}dt L = %.1f pb^{-1}' % lumi, size=size)
    return t

#______________________________________________________________________________
def make_atlas_watermark(x, y, prelim=True):
    if prelim:
        t = make_text(x, y, 'ATLAS', size=0.05, font=72)
        #t += make_text(x+0.05, y, 'Preliminary', size=0.05)
    else:
        t = make_text(x, y, 'ATLAS', size=0.05, font=72)
    return t

#______________________________________________________________________________
def pile_hists(hists,name,title=None,
        min=None,max=default,
        plot_options=None,
        canvas_options=default,
        draw_options=''):
    """
    Function for formatting a list of histograms and plotting them on the same
    canvas.  Returns a dictionary with the following keys:
    'canvas', 'hists'.
    """
    if canvas_options is default:
        canvas_options = CanvasOptions()
    canvas_options = CanvasOptions()
    c = canvas_options.create(name)
    if not isinstance(draw_options, list):
        draw_options = [draw_options]*len(hists)
    if not plot_options is None:
        for i, h in enumerate(hists):
            plot_options[i].configure(h)
    # set title
    if not title is None:
        for h in hists:
            h.SetTitle(title)
    # set min/max
    if not min is None:
        if min is default:
            set_min(hists)
        else:
            for h in hists:
                h.SetMinimum(min)
    if not max is None:
        if max is default:
            if canvas_options.log_y:
                set_max(hists, factor=2.0)
            else:
                set_max(hists, factor=1.1)
        else:
            for h in hists:
                h.SetMaximum(max)
    # draw hists in reverse order such that the first is drawn on top
    num_hists = len(hists)
    has_drawn_first = False
    for j in range(num_hists-1, -1, -1):
        h = hists[j]
        draw_opt = draw_options[j]
        if not has_drawn_first:
            if isinstance(h, ROOT.TGraph) or isinstance(h, ROOT.TGraphErrors) or isinstance(h, ROOT.TGraphAsymmErrors):
                first_hist = None
                for x in hists:
                    if isinstance(h, ROOT.TH1) or isinstance(h, ROOT.TH2):
                        first_hist = x
                        break
                if not first_hist is None:
                    x_min = first_hist.GetXaxis().GetXmin()
                    x_max = first_hist.GetXaxis().GetXmax()
                    y_min = first_hist.GetMinimum()
                    y_min = first_hist.GetMaximum()
                    title = ';%s;%s' % (h.GetXaxis().GetTitle(), h.GetYaxis().GetTitle())
                    frame = ROOT.TH1F('dummy_frame', title, 1, x_min, x_max, 1, y_min, y_max)
                    frame.Draw()
                elif draw_opt.count('A') == 0:
                    draw_opt = 'A' + draw_opt
        elif isinstance(h, ROOT.TH1) or isinstance(h, ROOT.TH2):
            draw_opt += ' same'
        h.Draw(draw_opt)
        has_drawn_first = True
    c.Update()
    return {'canvas':c, 'hists':hists}


#______________________________________________________________________________
def stack_hists(hists,name,title=default,
        min=None, max=None,
        xmin=None, xmax=None,
        plot_options=None,
        canvas_options=default,
        draw_options=default):
    """
    Function for formatting a list of histograms and plotting them on the same
    canvas, stacked. Returns a dictionary with the following keys:
    'canvas', 'stack', 'hists'.
    """
    if canvas_options is default:
        canvas_options = CanvasOptions()
    c = canvas_options.create(name)
    if plot_options:
        assert len(hists) == len(plot_options)
        for i, h in enumerate(hists):
            plot_options[i].configure(h)
    # set title
    if title is default:
        title = '%s;%s;%s' % (hists[0].GetTitle(), hists[0].GetXaxis().GetTitle(), hists[0].GetYaxis().GetTitle())
    hs = ROOT.THStack(name,title)
    # add hists to stack in reverse order such that first hist is on top
    num_hists = len(hists)
    for j in range(num_hists-1, -1, -1):
        if not draw_options is default:
            if not isinstance(draw_options,list):
                draw_options = [draw_options] * len(hists)
            else:
                assert len(draw_options) == len(hists)
            opt = draw_options[j]
        else:
            opt = ''
        hs.Add(hists[j],opt)
    # draw
    hs.Draw()
    # set min/max
    if not min is None:
        if hists[0].GetEntries():
            hs.SetMinimum(min) 
    if not max is None:
        hs.SetMaximum(max)
    c.Update()
    return {'canvas':c, 'stack':hs, 'hists':hists}

#______________________________________________________________________________
def plot_graph( name,
        x, y,
        title='',
        min=default, max=default,
        options=default,
        draw_options='AP',
        canvas_options=default):
    """
    Basic function for plotting a graph of the data in the lists x and y.
    Returns a dictionary with 'graph' and 'canvas' keys.
    """
    assert len(x) == len(y)
    if not isinstance(x, array):
        x = array('f', x)
    if not isinstance(y, array):
        y = array('f', y)
    g = ROOT.TGraph(len(x), x, y)
    g.SetName(name)
    g.SetTitle(title)
    if not min is default:
        g.SetMinimum(min)
    if not max is default:
        g.SetMaximum(max)
    if options is default:
        options = PlotOptions()
    options.configure(g)
    if not draw_options.count('same'):
        if canvas_options is default:
            canvas_options = CanvasOptions()
        c = canvas_options.create(name)
    else:
        c = None
    g.Draw(draw_options)
    return {'graph':g, 'canvas':c}

def plot_graph_errors( name,
        x, y,
        xel, xeh,
        yel, yeh,
        title='',
        min=default, max=default,
        xmin=None, xmax=None,
        options=default,
        draw_options='AP',
        canvas_options=default):
    """
    Basic function for plotting a graph of the data in the lists x and y.
    Returns a dictionary with 'graph' and 'canvas' keys.
    """
    assert len(x) == len(y)
    if not isinstance(x, array):
        x = array('f', x)
    if not isinstance(y, array):
        y = array('f', y)
    g = ROOT.TGraphAsymmErrors(len(x), x, y, xel, xeh, yel, yeh)
    g.SetName(name)
    g.SetTitle(title)
    if not min is default:
        g.SetMinimum(min)
    if not max is default:
        g.SetMaximum(max)
    if not xmin is None and not xmax is None:
        g.GetXaxis().SetRangeUser(xmin,xmax)
    #if not xmax is default:
    #    g.GetXaxis().SetMaximum(xmax)

    if options is default:
        options = PlotOptions()
    options.configure(g)
    if not draw_options.count('same'):
        if canvas_options is default:
            canvas_options = CanvasOptions()
        c = canvas_options.create(name)
    else:
        c = None
    g.Draw(draw_options)
    return {'graph':g, 'canvas':c}

def make_qq_plot(canvas):
    stack = Utilities.getObjectsFromCanvasByType(canvas, "THStack")[0]
    ROOT.SetOwnership(stack, False)
    hists = stack.GetHists()
    ROOT.SetOwnership(hists, False)
    data = Utilities.getObjectsFromCanvasByName(canvas,"data")[0]
    mc_sum = None
    for h in hists:
        if not mc_sum:
            mc_sum = h.Clone('mc_sum')
        else:
            mc_sum.Add(h)
    from array import array
    nquant = 10
    x = array('d',[0 for i in range(nquant)])
    y = array('d',[0 for i in range(nquant)])
    probSum = array('d', [1./(i+1) for i in range(nquant)])
    mc_sum.GetQuantiles(4,x,probSum)
    data.GetQuantiles(nquant,y,probSum);
    g_qq = ROOT.TGraph(nquant, x, y);
    line = array('d',[i for i in range(100)])
    g_line = ROOT.TGraph(100, line, line);
    mg = ROOT.TMultiGraph()
    c = ROOT.TCanvas('c_qq',canvas.GetName()+'_qq')
    c.cd()
    g_qq.Draw('ap')
    g_qq.GetXaxis().SetTitle("model")
    g_qq.GetYaxis().SetTitle("data")
    leg = ROOT.TLegend(0.2,0.6,0.4,0.8,'','brNDC')
    leg.AddEntry(g_qq,'q-q','p')
    mg.Add(g_line,'l')
    mg.Draw("")
    leg.Draw('sames')
    return {'canvas': c, 'legend':leg, 'graphs': [g_qq, g_line, mg]}
    
def make_ratio_plot(canvas_top,canvas_bottom,name,y_frac=0.35,canvas_options=None,with_line=False):
    if not canvas_top or not canvas_bottom:
        return None
    if not canvas_options:
        canvas_options = CanvasOptions()
    c = canvas_options.create(name)
    c.Divide(1,2,0.01,-1)
    c.Draw()
    pad = c.cd(1)
    pad.SetPad(0,y_frac,1,1)
    canvas_top.DrawClonePad()
    pad = c.cd(2)
    fac = (1 - y_frac) * canvas_top.GetBottomMargin() / y_frac + 1
    pad.SetPad(0,0,1,y_frac*fac)
    pad.SetTopMargin(0.01)
    pad.SetBottomMargin(0.3)
    # adjust text size and marker size in bottom canvas
    h = Utilities.getObjectsFromCanvasByType(canvas_bottom,'TH1')
    for hist in h:
        corr = (1 - y_frac)/(y_frac*fac)
        hist.GetXaxis().SetLabelSize(hist.GetXaxis().GetLabelSize()*corr)
        hist.GetXaxis().SetTitleSize(hist.GetXaxis().GetTitleSize()*corr)
        hist.GetXaxis().SetTitleOffset(hist.GetXaxis().GetTitleOffset()/corr)
        hist.GetYaxis().SetLabelSize(hist.GetYaxis().GetLabelSize()*corr)
        hist.GetYaxis().SetTitleSize(hist.GetYaxis().GetTitleSize()*corr)
        hist.GetYaxis().SetTitleOffset(hist.GetYaxis().GetTitleOffset()/corr)
    g = Utilities.getObjectsFromCanvasByType(canvas_bottom,'TGraph')
    for gr in g:
        hist = gr.GetHistogram()
        corr = (1 - y_frac)/(y_frac*fac)
        hist.GetXaxis().SetLabelSize(hist.GetXaxis().GetLabelSize()*corr)
        hist.GetXaxis().SetTitleSize(hist.GetXaxis().GetTitleSize()*corr)
        hist.GetXaxis().SetTitleOffset(hist.GetXaxis().GetTitleOffset()/corr)
        hist.GetYaxis().SetLabelSize(hist.GetYaxis().GetLabelSize()*corr)
        hist.GetYaxis().SetTitleSize(hist.GetYaxis().GetTitleSize()*corr)
        hist.GetYaxis().SetTitleOffset(hist.GetYaxis().GetTitleOffset()/corr)
    canvas_bottom.Update()
    canvas_bottom.DrawClonePad()
    # draw line at 1
    if with_line:
        xlow = pad.GetUxmin()
        xup = pad.GetUxmax()
        line = ROOT.TLine(xlow,1,xup,1)
        line.Draw('same')
        pad.Update()
        c.line = line
    return c
    
