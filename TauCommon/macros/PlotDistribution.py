import Utilities
import ROOT
from Samples import Samples

ROOT.gErrorIgnoreLevel = 4001;
_logger = Utilities.GetLogger("Plotter","ERROR")

def PlotDistribution(distribution,filelist,lumi,output,inPath,ordering):
    histos = {}
    xtitle = ''
    ytitle = ''
    ztitle = ''
    for filename in filelist:
        _logger.debug("processing '%s'",filename)
        # get sample name and retrieve sample object
        samplename = Utilities.ParseFileName(filename)["Name"]
        if not samplename in Samples:
            _logger.error("skipping unknown sample '%s",samplename)
            continue
        else:
            sample = Samples[samplename]
        # open ROOT file
        print str(inPath)+str(filename)
        f = ROOT.TFile.Open(str(inPath)+str(filename))
        if not f:
            _logger.error("couldn't open file '%s'",filename)
            continue
        # retrieve histogram
        hist = f.Get(distribution)
        if not hist:
            _logger.error("couldn't retrieve histogram '%s' from file '%s'",distribution,filename)
            continue
        # get MC events
        mc_events = Utilities.getProcessedEvents(f)[1]
        # scale histogram
        if lumi is not -1:
            hist.Scale(sample.lumiWeight(mc_events,lumi))
        if sample.process in histos:
            histos[sample.process].Add(hist)
        else:
            # make histogram nice
            hist.SetName(sample.process)
            hist.SetFillColor(sample.color)
            # store axis titles
            if not xtitle:
                xtitle = hist.GetXaxis().GetTitle()
            if not ytitle:
                ytitle = hist.GetYaxis().GetTitle()
            if not ztitle:
                ztitle = hist.GetZaxis().GetTitle()
            histos[sample.process] = hist
    leg = ROOT.TLegend(.7,.7,.9,.9)
    hs = ROOT.TH1F("hs_"+distribution.replace('/','_'),distribution.replace('/','_'),histos[sample.process].GetNbinsX(), histos[sample.process].GetXaxis().GetXmin(),histos[sample.process].GetXaxis().GetXmax())
    if ordering:
        for o in ordering:
            if o in histos:
                hs.Add(histos[o],"hist")
            else:
                _logger.warning("no input ROOT file for process '%s' found",o)
    else:
        for h in histos.values():
            leg.AddEntry(h,h.GetName())
            h.Scale(1./h.GetEntries())
            hs.Add(h)

    c = ROOT.TCanvas()
    hs.Draw()
    leg.Draw("sames")
    c.Update()
    c.Print(output)

def main(argv):
    import logging
    import argparse
    import os
    ROOT.gROOT.Macro( os.path.expanduser('~/Work/RootUtils/rootlogon.C'))
    parser = argparse.ArgumentParser(description='Distribution Plotter - plotting stacked histograms')
    parser.add_argument('--debug',nargs='?',dest='level',choices=['DEBUG','INFO','WARNING','CRITICAL','ERROR'],default='ERROR',help='logging level')
    parser.add_argument('distribution',nargs=1,help='distribution which is plotted (name under which it is stored inside the ROOT files)')
    parser.add_argument('filelist',nargs='+',help='filelist')
    parser.add_argument('-lumi',nargs=1,type=float,default=-1,help='integrated luminosity in pb^{-1}')
    parser.add_argument('-output',nargs=1,default='',help='output file')
    parser.add_argument('-outputPath', nargs=1, default='', help='output path to write files')
    parser.add_argument('-path',dest='inPath',default='',help='path which holds inputfiles')
    args = parser.parse_args()
    _logger.setLevel(eval('logging.' + args.level))
    if args.output:
        output = args.output[0]
    else:
        output = args.distribution[0].replace('/','_') + '.eps'
    PlotDistribution(args.distribution[0],args.filelist,args.lumi[0],output, args.inPath, False)

if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
