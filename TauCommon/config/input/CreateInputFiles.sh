ZEE_DIR=/ZIH.fast/users/TauGroup/D3PDs/mc/mc10b/Zee/
sh GenerateInput.sh input_ZeeNp0.xml ${ZEE_DIR%/}/mc10_7TeV.107650.AlpgenZeeNp0_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1/
sh GenerateInput.sh input_ZeeNp1.xml ${ZEE_DIR%/}/mc10_7TeV.107651.AlpgenZeeNp1_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1/
sh GenerateInput.sh input_ZeeNp2.xml ${ZEE_DIR%/}/mc10_7TeV.107652.AlpgenZeeNp2_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1/
sh GenerateInput.sh input_ZeeNp3.xml ${ZEE_DIR%/}/mc10_7TeV.107653.AlpgenZeeNp3_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3/
sh GenerateInput.sh input_ZeeNp4.xml ${ZEE_DIR%/}/mc10_7TeV.107654.AlpgenZeeNp4_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1/
sh GenerateInput.sh input_ZeeNp5.xml ${ZEE_DIR%/}/mc10_7TeV.107655.AlpgenZeeNp5_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3/

sh GenerateInput.sh input_DYeeNp0.xml ${ZEE_DIR%/}/mc10_7TeV.116250.AlpgenZeeNp0_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium/
sh GenerateInput.sh input_DYeeNp1.xml ${ZEE_DIR%/}/mc10_7TeV.116251.AlpgenZeeNp1_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1/
sh GenerateInput.sh input_DYeeNp2.xml ${ZEE_DIR%/}/mc10_7TeV.116252.AlpgenZeeNp2_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_DYeeNp3.xml ${ZEE_DIR%/}/mc10_7TeV.116253.AlpgenZeeNp3_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_DYeeNp4.xml ${ZEE_DIR%/}/mc10_7TeV.116254.AlpgenZeeNp4_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_DYeeNp5.xml ${ZEE_DIR%/}/mc10_7TeV.116255.AlpgenZeeNp5_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3

ZMUMU_DIR=/ZIH.fast/users/TauGroup/D3PDs/mc/mc10b/Zmumu/
sh GenerateInput.sh input_ZmumuNp0.xml ${ZMUMU_DIR%/}/mc10_7TeV.107660.AlpgenZmumuNp0_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_ZmumuNp1.xml ${ZMUMU_DIR%/}/mc10_7TeV.107661.AlpgenZmumuNp1_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_ZmumuNp2.xml ${ZMUMU_DIR%/}/mc10_7TeV.107662.AlpgenZmumuNp2_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_ZmumuNp3.xml ${ZMUMU_DIR%/}/mc10_7TeV.107663.AlpgenZmumuNp3_pt20.e737_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
sh GenerateInput.sh input_ZmumuNp4.xml ${ZMUMU_DIR%/}/mc10_7TeV.107664.AlpgenZmumuNp4_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_ZmumuNp5.xml ${ZMUMU_DIR%/}/mc10_7TeV.107665.AlpgenZmumuNp5_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3

sh GenerateInput.sh input_DYmumuNp0.xml ${ZMUMU_DIR%/}/mc10_7TeV.116260.AlpgenZmumuNp0_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_DYmumuNp1.xml ${ZMUMU_DIR%/}/mc10_7TeV.116261.AlpgenZmumuNp1_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_DYmumuNp2.xml ${ZMUMU_DIR%/}/mc10_7TeV.116262.AlpgenZmumuNp2_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
sh GenerateInput.sh input_DYmumuNp3.xml ${ZMUMU_DIR%/}/mc10_7TeV.116263.AlpgenZmumuNp3_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_DYmumuNp4.xml ${ZMUMU_DIR%/}/mc10_7TeV.116264.AlpgenZmumuNp4_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_DYmumuNp5.xml ${ZMUMU_DIR%/}/mc10_7TeV.116265.AlpgenZmumuNp5_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3

ZTAUTAU_DIR=/ZIH.fast/users/TauGroup/D3PDs/mc/mc10b/Ztautau/
sh GenerateInput.sh input_ZtautauNp0.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107670.AlpgenZtautauNp0_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_ZtautauNp1.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107671.AlpgenZtautauNp1_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_ZtautauNp2.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107672.AlpgenZtautauNp2_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_ZtautauNp3.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107673.AlpgenZtautauNp3_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_ZtautauNp4.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107674.AlpgenZtautauNp4_pt20.e737_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_ZtautauNp5.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107675.AlpgenZtautauNp5_pt20.e737_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1

sh GenerateInput.sh input_DYtautauNp0.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.116270.AlpgenZtautauNp0_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
sh GenerateInput.sh input_DYtautauNp1.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.116271.AlpgenZtautauNp1_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium
sh GenerateInput.sh input_DYtautauNp2.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.116272.AlpgenZtautauNp2_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium
sh GenerateInput.sh input_DYtautauNp3.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.116273.AlpgenZtautauNp3_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_DYtautauNp4.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.116274.AlpgenZtautauNp4_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium
sh GenerateInput.sh input_DYtautauNp5.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.116275.AlpgenZtautauNp5_Mll10to40_pt20.e660_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium

sh GenerateInput.sh input_Ztautau.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.106052.PythiaZtautau.e574_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM.v1

sh GenerateInput.sh input_ZPrime250.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107381.Pythia_Zprime_tautau_SSM250.e632_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM
sh GenerateInput.sh input_ZPrime500.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107382.Pythia_Zprime_tautau_SSM500.e632_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM
sh GenerateInput.sh input_ZPrime750.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107383.Pythia_Zprime_tautau_SSM750.e632_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM
sh GenerateInput.sh input_ZPrime1250.xml ${ZTAUTAU_DIR%/}/mc10_7TeV.107385.Pythia_Zprime_tautau_SSM1250.e632_s934_s946_r2310_r2300.01-01-02.D3PD_TauMEDIUM

WENU_DIR=/ZIH.fast/users/TauGroup/D3PDs/mc/mc10b/Wenu/
sh GenerateInput.sh input_WenuNp0.xml ${WENU_DIR%/}/mc10_7TeV.107680.AlpgenWenuNp0_pt20.e600_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_WenuNp1.xml ${WENU_DIR%/}/mc10_7TeV.107681.AlpgenWenuNp1_pt20.e600_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_WenuNp2.xml ${WENU_DIR%/}/mc10_7TeV.107682.AlpgenWenuNp2_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_WenuNp3.xml ${WENU_DIR%/}/mc10_7TeV.107683.AlpgenWenuNp3_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_WenuNp4.xml ${WENU_DIR%/}/mc10_7TeV.107684.AlpgenWenuNp4_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_WenuNp5.xml ${WENU_DIR%/}/mc10_7TeV.107685.AlpgenWenuNp5_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1

WMUNU_DIR=/ZIH.fast/users/TauGroup/D3PDs/mc/mc10b/Wmunu/
sh GenerateInput.sh input_WmunuNp0.xml ${WMUNU_DIR%/}/mc10_7TeV.107690.AlpgenWmunuNp0_pt20.e600_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
sh GenerateInput.sh input_WmunuNp1.xml ${WMUNU_DIR%/}/mc10_7TeV.107691.AlpgenWmunuNp1_pt20.e600_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
#sh GenerateInput.sh input_WmunuNp2.xml ${WMUNU_DIR%/}/
sh GenerateInput.sh input_WmunuNp3.xml ${WMUNU_DIR%/}/mc10_7TeV.107693.AlpgenWmunuNp3_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium
sh GenerateInput.sh input_WmunuNp4.xml ${WMUNU_DIR%/}/mc10_7TeV.107694.AlpgenWmunuNp4_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_WmunuNp5.xml ${WMUNU_DIR%/}/mc10_7TeV.107695.AlpgenWmunuNp5_pt20.e760_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1

WTAUNU_DIR=/ZIH.fast/users/TauGroup/D3PDs/mc/mc10b/Wtaunu/
sh GenerateInput.sh input_WtaunuNp0.xml ${WTAUNU_DIR%/}/mc10_7TeV.107700.AlpgenWtaunuNp0_pt20.e600_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_WtaunuNp1.xml ${WTAUNU_DIR%/}/mc10_7TeV.107701.AlpgenWtaunuNp1_pt20.e600_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
sh GenerateInput.sh input_WtaunuNp2.xml ${WTAUNU_DIR%/}/mc10_7TeV.107702.AlpgenWtaunuNp2_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_WtaunuNp3.xml ${WTAUNU_DIR%/}/mc10_7TeV.107703.AlpgenWtaunuNp3_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_WtaunuNp4.xml ${WTAUNU_DIR%/}/mc10_7TeV.107704.AlpgenWtaunuNp4_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v1
sh GenerateInput.sh input_WtaunuNp5.xml ${WTAUNU_DIR%/}/mc10_7TeV.107705.AlpgenWtaunuNp5_pt20.e760_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3

TTBAR_DIR=/ZIH.fast/users/TauGroup/D3PDs/mc/mc10b/ttbar/
sh GenerateInput.sh input_ttbar_fullhad.xml ${TTBAR_DIR%/}/mc10_7TeV.105204.TTbar_FullHad_McAtNlo_.e598_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_ttbar_nofullhad.xml ${TTBAR_DIR%/}/mc10_7TeV.105200.T1_McAtNlo_.e598_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_stop_schan_enu.xml ${TTBAR_DIR%/}/mc10_7TeV.108343.st_schan_enu_McAtNlo_.e598_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
#sh GenerateInput.sh input_stop_schan_munu.xml ${TTBAR_DIR%/}/
sh GenerateInput.sh input_stop_schan_taunu.xml ${TTBAR_DIR%/}/mc10_7TeV.108345.st_schan_taunu_McAtNlo_.e598_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
sh GenerateInput.sh input_stop_tchan_enu.xml ${TTBAR_DIR%/}/mc10_7TeV.108340.st_tchan_enu_McAtNlo_.e598_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_stop_tchan_munu.xml ${TTBAR_DIR%/}/mc10_7TeV.108341.st_tchan_munu_McAtNlo_.e598_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_stop_tchan_taunu.xml ${TTBAR_DIR%/}/mc10_7TeV.108342.st_tchan_taunu_McAtNlo_.e598_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
sh GenerateInput.sh input_stop_Wtchan.xml ${TTBAR_DIR%/}/mc10_7TeV.108346.st_Wt_McAtNlo_.e598_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1

HIGGS_LL_DIR=/ZIH.fast/users/TauGroup/D3PDs/mc/mc10b/HTauTaull/
sh GenerateInput.sh input_ggHtautaull100.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116876.Pythia_ggH100_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull105.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116877.Pythia_ggH105_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull110.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116878.Pythia_ggH110_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull115.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116879.Pythia_ggH115_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull120.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116615.Pythia_ggH120_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull125.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116880.Pythia_ggH125_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull130.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116881.Pythia_ggH130_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull135.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116882.Pythia_ggH135_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull140.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116883.Pythia_ggH140_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull145.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116884.Pythia_ggH145_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1
sh GenerateInput.sh input_ggHtautaull150.xml ${HIGGS_LL_DIR%/}/mc10_7TeV.116885.Pythia_ggH150_tautaull.e773_s933_s946_r2302_r2300.01-01-01.D3PD_TauMEDIUM.v1

HIGGS_HH_DIR=/ZIH.fast/users/TauGroup/D3PDs/mc/mc10b/HTauTauhh/
sh GenerateInput.sh input_bbAtautauhh100.xml ${HIGGS_HH_DIR%/}/mc10_7TeV.109921.SherpabbAtautauhhMA100TB20.e769_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium
sh GenerateInput.sh input_bbAtautauhh200.xml ${HIGGS_HH_DIR%/}/mc10_7TeV.109922.SherpabbAtautauhhMA200TB20.e769_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_bbAtautauhh250.xml ${HIGGS_HH_DIR%/}/mc10_7TeV.125566.SherpabbAtautauhhMA250TB20.e769_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_bbAtautauhh300.xml ${HIGGS_HH_DIR%/}/mc10_7TeV.109920.SherpabbAtautauhhMA300TB20.e769_s933_s946_r2302_r2300.01-01-01.D3PD_TauMedium.v3
sh GenerateInput.sh input_ggHtautauhh120.xml ${HIGGS_HH_DIR%/}/mc10_7TeV.116480.McAtNlo_ggHtautauhh_MA120TB20.e666_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1
sh GenerateInput.sh input_ggHtautauhh200.xml ${HIGGS_HH_DIR%/}/mc10_7TeV.116481.McAtNlo_ggHtautauhh_MA200TB20.e666_s933_s946_r2302_r2300.01-01-02.D3PD_TauMedium.v1

