#!/bin/bash

DIR="/amnt/remote/ZIH.tmp/users/TauGroup/D3PDs/ZTauTauLepLep/local_skims/EF_e15_medium/";
skim=1;
OUTPUT="";

if [ $# -eq 2 ];
then
  skim=0;
  DIR=$2;
  OUTPUT=$1;
  if [ -e $OUTPUT ];
  then
    rm $OUTPUT;
  fi
fi

for i in $(find $DIR -iname '*.root*');
do
  echo "found file $i";
  if [ $skim -eq 1 ];
  then
    name=${i##$DIR};
    name=${name/SkimCycle./};
    name=${name/mc./};
    name=${name/data./};
    name=${name/root/xml};
    name=${name/\//_};
    name="input_Skim$name";
    OUTPUT=$name;
    if [ -e $OUTPUT ];
    then
      rm $OUTPUT;
    fi
    echo "generate input file $OUTPUT";
  fi

  echo "<In FileName=\"$i\" Lumi=\"1.0\" />" >> $OUTPUT;
done
