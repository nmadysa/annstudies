#ifndef D3PDREADER_VARHANDLE_ICC
#define D3PDREADER_VARHANDLE_ICC

// System include(s):
#include <iostream>
#include <string.h>
#include <assert.h>
#include <cxxabi.h>
#include <cstdlib>

// ROOT include(s):
#include <TTree.h>
#include <TBranch.h>

namespace D3PDReader
{
  template<typename Type>
  VarHandle<Type>::VarHandle(const std::string& sName,const long int* const pMaster):
    VarHandleBase(sName,pMaster),
    m_fVariable()
  {}

  template<typename Type>
  TBranch* VarHandle<Type>::WriteTo(TTree* pTree)
  {
    assert(pTree);
    if(!m_bIsActive)
      return 0;

    TBranch* branch = pTree->GetBranch(m_sName.c_str());
    if(branch)
      return branch;

    branch = pTree->Branch(m_sName.c_str(),&m_fVariable,TString::Format("%s/%s",m_sName.c_str(),
                                          RootType(typeid(Type).name())));
    if(!branch)
      std::cerr << "Couldn't add variable " << m_sName << " to tree " << pTree->GetName() << std::endl;
    return branch;
  }

  template<typename Type>
  Type& VarHandle<Type>::operator()()
  {
#ifdef WRITE_BRANCHES_USED
    if(m_bWriteInfo)
    {
       m_bWriteInfo = false;
       std::cout << m_sName << std::endl;
    }
#endif // WRITE_BRANCHES_USED

    if(!m_bFromInput)
      return m_fVariable;

    if(!m_pInBranch)
    {
      if(!m_pInTree)
      {
        std::cerr << m_sName << "(): Object not connected yet!" << std::endl;
        return m_fVariable;
      }
      if(!m_pInTree->GetBranch(m_sName.c_str()))
      {
        std::cerr << "The following branch doesn't exist: " << m_sName << std::endl;
        return m_fVariable;
      }
      m_pInTree->SetBranchStatus(m_sName.c_str(),1);
      m_pInTree->SetBranchAddress(m_sName.c_str(),&m_fVariable,&m_pInBranch);
    }
    if(*m_pMaster != m_pInBranch->GetReadEntry())
      m_pInBranch->GetEntry(*m_pMaster);

    return m_fVariable;
  }

  template<typename Type>
  const Type& VarHandle<Type>::operator()() const
  {
#ifdef WRITE_BRANCHES_USED
    if(m_bWriteInfo)
    {
       m_bWriteInfo = false;
       std::cout << m_sName << std::endl;
    }
#endif // WRITE_BRANCHES_USED

    if(!m_bFromInput)
      return m_fVariable;

    if(!m_pInBranch)
    {
      if(!m_pInTree)
      {
        std::cerr << m_sName << "(): Object not connected yet!" << std::endl;
        return m_fVariable;
      }
      if(!m_pInTree->GetBranch(m_sName.c_str()))
      {
        std::cerr << "The following branch doesn't exist: " << m_sName << std::endl;
        return m_fVariable;
      }
      m_pInTree->SetBranchStatus(m_sName.c_str(),1);
      m_pInTree->SetBranchAddress(m_sName.c_str(),&m_fVariable,&m_pInBranch);
    }
    if(*m_pMaster != m_pInBranch->GetReadEntry())
      m_pInBranch->GetEntry(*m_pMaster);

    return m_fVariable;
  }

  template<typename Type>
  const char* VarHandle<Type>::RootType(const char* typeid_type) const
  {
    if(strlen(typeid_type) != 1)
    {
      std::cerr << "Received complex object description" << std::endl;
      return "";
    }

    switch(typeid_type[0])
    {
    case 'c':
      return "B";
      break;
    case 'h':
      return "b";
      break;
    case 's':
      return "S";
      break;
    case 't':
      return "s";
      break;
    case 'i':
      return "I";
      break;
    case 'j':
      return "i";
      break;
    case 'f':
      return "F";
      break;
    case 'd':
      return "D";
      break;
    case 'x':
      return "L";
      break;
    case 'y':
      return "l";
      break;
    case 'b':
      return "O";
      break;

    }

    std::cerr << "Unknown primitive type encountered: " << typeid_type << std::endl;
    return "";
  }

  template<typename Type>
  VarHandle< Type* >::VarHandle(const std::string& sName,const long int* const pMaster):
    VarHandleBase(sName,pMaster),
    m_fVariable(0)
  {}

  template<typename Type>
  VarHandle<Type*>::~VarHandle()
  {
     delete m_fVariable;
  }

  template<typename Type>
  TBranch* VarHandle<Type*>::WriteTo(TTree* pTree)
  {
    assert(m_pMaster);
    assert(pTree);
    if(!m_bIsActive)
      return 0;

    TBranch* branch = pTree->GetBranch(m_sName.c_str());
    if(branch)
      return branch;

    int status;
    char* type_name = abi::__cxa_demangle(typeid(Type).name(),0,0,&status);
    if(status)
    {
      std::cerr << "Couldn't demangle type name: " << typeid(Type).name() << std::endl;
      return 0;    }
    if(!m_fVariable)
      m_fVariable = new Type();

    branch = pTree->Bronch(m_sName.c_str(),type_name,&m_fVariable);
    if(!branch)
      std::cerr << "Couldn't add variable " << m_sName << " to tree " << pTree->GetName() << std::endl;
    free(type_name);
    return branch;
  }

  template<typename Type>
  Type* VarHandle<Type*>::operator()()
  {
#ifdef WRITE_BRANCHES_USED
    if(m_bWriteInfo)
    {
       m_bWriteInfo = false;
       std::cout << m_sName << std::endl;
    }
#endif // WRITE_BRANCHES_USED

    if(!m_bFromInput)
    {
      if(!m_fVariable)
        m_fVariable = new Type();
      return m_fVariable;
    }

    if(!m_pInBranch)
    {
      if(!m_pInTree)
      {
        std::cerr << m_sName << "(): Object not connected yet!" << std::endl;
        return m_fVariable;
      }
      if(!m_pInTree->GetBranch(m_sName.c_str()))
      {
        std::cerr << "The following variable doesn't exist: " << m_sName << std::endl;
        return m_fVariable;
      }
      m_pInTree->SetBranchStatus(m_sName.c_str(),1);
      m_pInTree->SetBranchAddress(m_sName.c_str(),&m_fVariable,&m_pInBranch);
    }
    if(*m_pMaster != m_pInBranch->GetReadEntry())
      m_pInBranch->GetEntry(*m_pMaster);

    return m_fVariable;
  }

  template<typename Type>
  const Type* VarHandle<Type*>::operator()() const
  {
#ifdef WRITE_BRANCHES_USED
    if(m_bWriteInfo)
    {
       m_bWriteInfo = false;
       std::cout << m_sName << std::endl;
    }
#endif // WRITE_BRANCHES_USED

    if(!m_bFromInput)
    {
      if(!m_fVariable)
        m_fVariable = new Type();
      return m_fVariable;
    }

    if(!m_pInBranch)
    {
      if(!m_pInTree)
      {
        std::cerr << m_sName << "(): Object not connected yet!" << std::endl;
        return m_fVariable;
      }
      if(!m_pInTree->GetBranch(m_sName.c_str()))
      {
        std::cerr << "The following variable doesn't exist: " << m_sName << std::endl;
        return m_fVariable;
      }
      m_pInTree->SetBranchStatus(m_sName.c_str(),1);
      m_pInTree->SetBranchAddress(m_sName.c_str(),&m_fVariable,&m_pInBranch);
    }
    if(*m_pMaster != m_pInBranch->GetReadEntry())
      m_pInBranch->GetEntry(*m_pMaster);

    return m_fVariable;
  }

} // namespace D3PDReader

#endif // D3PDREADER_VARHANDLE_ICC

