// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------
#ifndef D3PDREADER_TrigEFJetD3PDCollection_H
#define D3PDREADER_TrigEFJetD3PDCollection_H

// STL include(s)
#include <string>
#include <vector>
using std::vector;

// ROOT include(s)
#include <TObject.h>
#include <TString.h>

// custom include(s)
#include "VarHandle.h"

class TTree;

namespace D3PDReader
{
  /**
   * Code generated by CodeGenerator on:
   * time = Fri Feb 24 13:05:52 2012
   */
  class TrigEFJetD3PDCollection : public TObject
  {
  public:
    /// Constructor used when reading from a TTree
    TrigEFJetD3PDCollection(const long int& master,const std::string& prefix = "trig_EF_jet_");
    /// Constructor when the object is only used for writing data out
    TrigEFJetD3PDCollection(const std::string& prefix = "trig_EF_jet_");
    /// standard destructor
    ~TrigEFJetD3PDCollection() {}
    /// Get the currently configured prefix value
    const std::string& GetPrefix() const {return m_sPrefix;}
    /// Connect the object to an input TTree
    void ReadFrom(TTree* tree);
    /// Connect the object to an output TTree
    void WriteTo(TTree* tree);
    /// Turn all (available) branches either on or off
    void SetActive(bool active = true);
    /// Read in all the variables that we need to write out as well
    void ReadAllActive();

    /// Did this object pass the chain EF_2fj100_a4tc_EFFS_deta50_FB
    VarHandle<vector<int>* > EF_2fj100_a4tc_EFFS_deta50_FB;
    /// Did this object pass the chain EF_2fj30_a4tc_EFFS_deta50_FB
    VarHandle<vector<int>* > EF_2fj30_a4tc_EFFS_deta50_FB;
    /// Did this object pass the chain EF_2fj30_a4tc_EFFS_deta50_FC
    VarHandle<vector<int>* > EF_2fj30_a4tc_EFFS_deta50_FC;
    /// Did this object pass the chain EF_2fj55_a4tc_EFFS_deta50_FB
    VarHandle<vector<int>* > EF_2fj55_a4tc_EFFS_deta50_FB;
    /// Did this object pass the chain EF_2fj55_a4tc_EFFS_deta50_FC
    VarHandle<vector<int>* > EF_2fj55_a4tc_EFFS_deta50_FC;
    /// Did this object pass the chain EF_2fj75_a4tc_EFFS_deta50_FB
    VarHandle<vector<int>* > EF_2fj75_a4tc_EFFS_deta50_FB;
    /// Did this object pass the chain EF_2fj75_a4tc_EFFS_deta50_FC
    VarHandle<vector<int>* > EF_2fj75_a4tc_EFFS_deta50_FC;
    /// Did this object pass the chain EF_2j100_a4tc_EFFS_deta35_FC
    VarHandle<vector<int>* > EF_2j100_a4tc_EFFS_deta35_FC;
    /// Did this object pass the chain EF_2j135_a4tc_EFFS_deta35_FC
    VarHandle<vector<int>* > EF_2j135_a4tc_EFFS_deta35_FC;
    /// Did this object pass the chain EF_2j180_a4tc_EFFS_deta35_FC
    VarHandle<vector<int>* > EF_2j180_a4tc_EFFS_deta35_FC;
    /// Did this object pass the chain EF_2j240_a4tc_EFFS_deta35_FC
    VarHandle<vector<int>* > EF_2j240_a4tc_EFFS_deta35_FC;
    /// Did this object pass the chain EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu
    VarHandle<vector<int>* > EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu;
    /// Did this object pass the chain EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu
    VarHandle<vector<int>* > EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu;
    /// Did this object pass the chain EF_3j100_a4tc_EFFS
    VarHandle<vector<int>* > EF_3j100_a4tc_EFFS;
    /// Did this object pass the chain EF_3j100_a4tc_EFFS_L1J75
    VarHandle<vector<int>* > EF_3j100_a4tc_EFFS_L1J75;
    /// Did this object pass the chain EF_3j30_a4tc_EFFS
    VarHandle<vector<int>* > EF_3j30_a4tc_EFFS;
    /// Did this object pass the chain EF_3j40_a4tc_EFFS
    VarHandle<vector<int>* > EF_3j40_a4tc_EFFS;
    /// Did this object pass the chain EF_3j45_a4tc_EFFS
    VarHandle<vector<int>* > EF_3j45_a4tc_EFFS;
    /// Did this object pass the chain EF_3j75_a4tc_EFFS
    VarHandle<vector<int>* > EF_3j75_a4tc_EFFS;
    /// Did this object pass the chain EF_4j30_a4tc_EFFS
    VarHandle<vector<int>* > EF_4j30_a4tc_EFFS;
    /// Did this object pass the chain EF_4j40_a4tc_EFFS
    VarHandle<vector<int>* > EF_4j40_a4tc_EFFS;
    /// Did this object pass the chain EF_4j40_a4tc_EFFS_ht350
    VarHandle<vector<int>* > EF_4j40_a4tc_EFFS_ht350;
    /// Did this object pass the chain EF_4j40_a4tc_EFFS_ht400
    VarHandle<vector<int>* > EF_4j40_a4tc_EFFS_ht400;
    /// Did this object pass the chain EF_4j45_a4tc_EFFS
    VarHandle<vector<int>* > EF_4j45_a4tc_EFFS;
    /// Did this object pass the chain EF_4j55_a4tc_EFFS
    VarHandle<vector<int>* > EF_4j55_a4tc_EFFS;
    /// Did this object pass the chain EF_4j60_a4tc_EFFS
    VarHandle<vector<int>* > EF_4j60_a4tc_EFFS;
    /// Did this object pass the chain EF_5j30_a4tc_EFFS
    VarHandle<vector<int>* > EF_5j30_a4tc_EFFS;
    /// Did this object pass the chain EF_5j40_a4tc_EFFS
    VarHandle<vector<int>* > EF_5j40_a4tc_EFFS;
    /// Did this object pass the chain EF_5j45_a4tc_EFFS
    VarHandle<vector<int>* > EF_5j45_a4tc_EFFS;
    /// Did this object pass the chain EF_6j30_a4tc_EFFS
    VarHandle<vector<int>* > EF_6j30_a4tc_EFFS;
    /// Did this object pass the chain EF_6j30_a4tc_EFFS_L15J10
    VarHandle<vector<int>* > EF_6j30_a4tc_EFFS_L15J10;
    /// Did this object pass the chain EF_6j40_a4tc_EFFS
    VarHandle<vector<int>* > EF_6j40_a4tc_EFFS;
    /// Did this object pass the chain EF_6j45_a4tc_EFFS
    VarHandle<vector<int>* > EF_6j45_a4tc_EFFS;
    /// Did this object pass the chain EF_7j30_a4tc_EFFS_L15J10
    VarHandle<vector<int>* > EF_7j30_a4tc_EFFS_L15J10;
    /// Did this object pass the chain EF_7j30_a4tc_EFFS_L16J10
    VarHandle<vector<int>* > EF_7j30_a4tc_EFFS_L16J10;
    /// Did this object pass the chain EF_fj100_a4tc_EFFS
    VarHandle<vector<int>* > EF_fj100_a4tc_EFFS;
    /// Did this object pass the chain EF_fj10_a4tc_EFFS
    VarHandle<vector<int>* > EF_fj10_a4tc_EFFS;
    /// Did this object pass the chain EF_fj10_a4tc_EFFS_1vx
    VarHandle<vector<int>* > EF_fj10_a4tc_EFFS_1vx;
    /// Did this object pass the chain EF_fj135_a4tc_EFFS
    VarHandle<vector<int>* > EF_fj135_a4tc_EFFS;
    /// Did this object pass the chain EF_fj15_a4tc_EFFS
    VarHandle<vector<int>* > EF_fj15_a4tc_EFFS;
    /// Did this object pass the chain EF_fj20_a4tc_EFFS
    VarHandle<vector<int>* > EF_fj20_a4tc_EFFS;
    /// Did this object pass the chain EF_fj30_a4tc_EFFS
    VarHandle<vector<int>* > EF_fj30_a4tc_EFFS;
    /// Did this object pass the chain EF_fj30_a4tc_EFFS_l2cleanph
    VarHandle<vector<int>* > EF_fj30_a4tc_EFFS_l2cleanph;
    /// Did this object pass the chain EF_fj55_a4tc_EFFS
    VarHandle<vector<int>* > EF_fj55_a4tc_EFFS;
    /// Did this object pass the chain EF_fj75_a4tc_EFFS
    VarHandle<vector<int>* > EF_fj75_a4tc_EFFS;
    /// Did this object pass the chain EF_j100_a4tc_EFFS
    VarHandle<vector<int>* > EF_j100_a4tc_EFFS;
    /// Did this object pass the chain EF_j100_a4tc_EFFS_ht350
    VarHandle<vector<int>* > EF_j100_a4tc_EFFS_ht350;
    /// Did this object pass the chain EF_j100_a4tc_EFFS_ht400
    VarHandle<vector<int>* > EF_j100_a4tc_EFFS_ht400;
    /// Did this object pass the chain EF_j100_a4tc_EFFS_ht500
    VarHandle<vector<int>* > EF_j100_a4tc_EFFS_ht500;
    /// Did this object pass the chain EF_j100_j30_a4tc_EFFS_L2dphi04
    VarHandle<vector<int>* > EF_j100_j30_a4tc_EFFS_L2dphi04;
    /// Did this object pass the chain EF_j10_a4tc_EFFS
    VarHandle<vector<int>* > EF_j10_a4tc_EFFS;
    /// Did this object pass the chain EF_j10_a4tc_EFFS_1vx
    VarHandle<vector<int>* > EF_j10_a4tc_EFFS_1vx;
    /// Did this object pass the chain EF_j135_a4tc_EFFS
    VarHandle<vector<int>* > EF_j135_a4tc_EFFS;
    /// Did this object pass the chain EF_j135_a4tc_EFFS_ht500
    VarHandle<vector<int>* > EF_j135_a4tc_EFFS_ht500;
    /// Did this object pass the chain EF_j135_j30_a4tc_EFFS_L2dphi04
    VarHandle<vector<int>* > EF_j135_j30_a4tc_EFFS_L2dphi04;
    /// Did this object pass the chain EF_j135_j30_a4tc_EFFS_dphi04
    VarHandle<vector<int>* > EF_j135_j30_a4tc_EFFS_dphi04;
    /// Did this object pass the chain EF_j15_a4tc_EFFS
    VarHandle<vector<int>* > EF_j15_a4tc_EFFS;
    /// Did this object pass the chain EF_j180_a4tc_EFFS
    VarHandle<vector<int>* > EF_j180_a4tc_EFFS;
    /// Did this object pass the chain EF_j180_j30_a4tc_EFFS_dphi04
    VarHandle<vector<int>* > EF_j180_j30_a4tc_EFFS_dphi04;
    /// Did this object pass the chain EF_j20_a4tc_EFFS
    VarHandle<vector<int>* > EF_j20_a4tc_EFFS;
    /// Did this object pass the chain EF_j240_a10tc_EFFS
    VarHandle<vector<int>* > EF_j240_a10tc_EFFS;
    /// Did this object pass the chain EF_j240_a4tc_EFFS
    VarHandle<vector<int>* > EF_j240_a4tc_EFFS;
    /// Did this object pass the chain EF_j240_a4tc_EFFS_l2cleanph
    VarHandle<vector<int>* > EF_j240_a4tc_EFFS_l2cleanph;
    /// Did this object pass the chain EF_j30_a4tc_EFFS
    VarHandle<vector<int>* > EF_j30_a4tc_EFFS;
    /// Did this object pass the chain EF_j30_a4tc_EFFS_l2cleanph
    VarHandle<vector<int>* > EF_j30_a4tc_EFFS_l2cleanph;
    /// Did this object pass the chain EF_j30_cosmic
    VarHandle<vector<int>* > EF_j30_cosmic;
    /// Did this object pass the chain EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty
    VarHandle<vector<int>* > EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty;
    /// Did this object pass the chain EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty
    VarHandle<vector<int>* > EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty;
    /// Did this object pass the chain EF_j30_firstempty
    VarHandle<vector<int>* > EF_j30_firstempty;
    /// Did this object pass the chain EF_j30_fj30_a4tc_EFFS
    VarHandle<vector<int>* > EF_j30_fj30_a4tc_EFFS;
    /// Did this object pass the chain EF_j320_a10tc_EFFS
    VarHandle<vector<int>* > EF_j320_a10tc_EFFS;
    /// Did this object pass the chain EF_j320_a4tc_EFFS
    VarHandle<vector<int>* > EF_j320_a4tc_EFFS;
    /// Did this object pass the chain EF_j35_a4tc_EFFS
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HV
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HV;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HV_cosmic
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HV_cosmic;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HV_firstempty
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HV_firstempty;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HVtrk
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HVtrk;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso;
    /// Did this object pass the chain EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso
    VarHandle<vector<int>* > EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso;
    /// Did this object pass the chain EF_j40_a4tc_EFFS
    VarHandle<vector<int>* > EF_j40_a4tc_EFFS;
    /// Did this object pass the chain EF_j40_fj40_a4tc_EFFS
    VarHandle<vector<int>* > EF_j40_fj40_a4tc_EFFS;
    /// Did this object pass the chain EF_j425_a10tc_EFFS
    VarHandle<vector<int>* > EF_j425_a10tc_EFFS;
    /// Did this object pass the chain EF_j425_a4tc_EFFS
    VarHandle<vector<int>* > EF_j425_a4tc_EFFS;
    /// Did this object pass the chain EF_j45_a4tc_EFFS
    VarHandle<vector<int>* > EF_j45_a4tc_EFFS;
    /// Did this object pass the chain EF_j50_a4tc_EFFS
    VarHandle<vector<int>* > EF_j50_a4tc_EFFS;
    /// Did this object pass the chain EF_j50_cosmic
    VarHandle<vector<int>* > EF_j50_cosmic;
    /// Did this object pass the chain EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty
    VarHandle<vector<int>* > EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty;
    /// Did this object pass the chain EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty
    VarHandle<vector<int>* > EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty;
    /// Did this object pass the chain EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty
    VarHandle<vector<int>* > EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty;
    /// Did this object pass the chain EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty
    VarHandle<vector<int>* > EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty;
    /// Did this object pass the chain EF_j50_firstempty
    VarHandle<vector<int>* > EF_j50_firstempty;
    /// Did this object pass the chain EF_j55_a4tc_EFFS
    VarHandle<vector<int>* > EF_j55_a4tc_EFFS;
    /// Did this object pass the chain EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10
    VarHandle<vector<int>* > EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10;
    /// Did this object pass the chain EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons
    VarHandle<vector<int>* > EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons;
    /// Did this object pass the chain EF_j55_fj55_a4tc_EFFS
    VarHandle<vector<int>* > EF_j55_fj55_a4tc_EFFS;
    /// Did this object pass the chain EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10
    VarHandle<vector<int>* > EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10;
    /// Did this object pass the chain EF_j70_j25_dphi03_NoEF
    VarHandle<vector<int>* > EF_j70_j25_dphi03_NoEF;
    /// Did this object pass the chain EF_j75_2j30_a4tc_EFFS_ht350
    VarHandle<vector<int>* > EF_j75_2j30_a4tc_EFFS_ht350;
    /// Did this object pass the chain EF_j75_a4tc_EFFS
    VarHandle<vector<int>* > EF_j75_a4tc_EFFS;
    /// Did this object pass the chain EF_j75_a4tc_EFFS_xe40_loose_noMu
    VarHandle<vector<int>* > EF_j75_a4tc_EFFS_xe40_loose_noMu;
    /// Did this object pass the chain EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03
    VarHandle<vector<int>* > EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03;
    /// Did this object pass the chain EF_j75_a4tc_EFFS_xe45_loose_noMu
    VarHandle<vector<int>* > EF_j75_a4tc_EFFS_xe45_loose_noMu;
    /// Did this object pass the chain EF_j75_a4tc_EFFS_xe55_loose_noMu
    VarHandle<vector<int>* > EF_j75_a4tc_EFFS_xe55_loose_noMu;
    /// Did this object pass the chain EF_j75_a4tc_EFFS_xe55_noMu
    VarHandle<vector<int>* > EF_j75_a4tc_EFFS_xe55_noMu;
    /// Did this object pass the chain EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons
    VarHandle<vector<int>* > EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons;
    /// Did this object pass the chain EF_j75_a4tc_EFFS_xs35_noMu
    VarHandle<vector<int>* > EF_j75_a4tc_EFFS_xs35_noMu;
    /// Did this object pass the chain EF_j75_fj75_a4tc_EFFS
    VarHandle<vector<int>* > EF_j75_fj75_a4tc_EFFS;
    /// Did this object pass the chain EF_j75_j30_a4tc_EFFS
    VarHandle<vector<int>* > EF_j75_j30_a4tc_EFFS;
    /// Did this object pass the chain EF_j75_j30_a4tc_EFFS_L2anymct100
    VarHandle<vector<int>* > EF_j75_j30_a4tc_EFFS_L2anymct100;
    /// Did this object pass the chain EF_j75_j30_a4tc_EFFS_L2anymct150
    VarHandle<vector<int>* > EF_j75_j30_a4tc_EFFS_L2anymct150;
    /// Did this object pass the chain EF_j75_j30_a4tc_EFFS_L2anymct175
    VarHandle<vector<int>* > EF_j75_j30_a4tc_EFFS_L2anymct175;
    /// Did this object pass the chain EF_j75_j30_a4tc_EFFS_L2dphi04
    VarHandle<vector<int>* > EF_j75_j30_a4tc_EFFS_L2dphi04;
    /// Did this object pass the chain EF_j75_j30_a4tc_EFFS_anymct150
    VarHandle<vector<int>* > EF_j75_j30_a4tc_EFFS_anymct150;
    /// Did this object pass the chain EF_j75_j30_a4tc_EFFS_anymct175
    VarHandle<vector<int>* > EF_j75_j30_a4tc_EFFS_anymct175;
    /// Did this object pass the chain EF_j75_j30_a4tc_EFFS_leadingmct150
    VarHandle<vector<int>* > EF_j75_j30_a4tc_EFFS_leadingmct150;
    /// Did this object pass the chain EF_j80_a4tc_EFFS_xe60_noMu
    VarHandle<vector<int>* > EF_j80_a4tc_EFFS_xe60_noMu;
    /// Did this object pass the chain EF_je195_NoEF
    VarHandle<vector<int>* > EF_je195_NoEF;
    /// Did this object pass the chain EF_je255_NoEF
    VarHandle<vector<int>* > EF_je255_NoEF;
    /// Did this object pass the chain EF_je300_NoEF
    VarHandle<vector<int>* > EF_je300_NoEF;
    /// Did this object pass the chain EF_je350_NoEF
    VarHandle<vector<int>* > EF_je350_NoEF;
    /// Did this object pass the chain EF_je420_NoEF
    VarHandle<vector<int>* > EF_je420_NoEF;
    /// Did this object pass the chain EF_je500_NoEF
    VarHandle<vector<int>* > EF_je500_NoEF;
    /// Number of ntuple rows.
    VarHandle<Int_t > n;
    VarHandle<vector<float>* > emscale_E;
    VarHandle<vector<float>* > emscale_pt;
    VarHandle<vector<float>* > emscale_m;
    VarHandle<vector<float>* > emscale_eta;
    VarHandle<vector<float>* > emscale_phi;
    /// Was the object created in chain 'EF_.*_a4_.*'
    VarHandle<vector<int>* > a4;
    /// Was the object created in chain 'EF_.*_a4tc_.*'
    VarHandle<vector<int>* > a4tc;
    /// Was the object created in chain 'EF_.*_a10tc_.*'
    VarHandle<vector<int>* > a10tc;
    /// Was the object created in chain 'EF_.*_a6_.*'
    VarHandle<vector<int>* > a6;
    /// Was the object created in chain 'EF_.*_a6tc_.*'
    VarHandle<vector<int>* > a6tc;
    VarHandle<vector<unsigned int>* > RoIword;
    /// Did this object pass the chain EF_3j10_a4tc_EFFS
    VarHandle<vector<int>* > EF_3j10_a4tc_EFFS;
    /// Did this object pass the chain EF_4j10_a4tc_EFFS
    VarHandle<vector<int>* > EF_4j10_a4tc_EFFS;
    /// Did this object pass the chain EF_5j10_a4tc_EFFS
    VarHandle<vector<int>* > EF_5j10_a4tc_EFFS;
    /// Did this object pass the chain EF_j15_a2hi_EFFS
    VarHandle<vector<int>* > EF_j15_a2hi_EFFS;
    /// Did this object pass the chain EF_j15_a2tc_EFFS
    VarHandle<vector<int>* > EF_j15_a2tc_EFFS;
    /// Did this object pass the chain EF_j20_a2hi_EFFS
    VarHandle<vector<int>* > EF_j20_a2hi_EFFS;
    /// Did this object pass the chain EF_j20_a2tc_EFFS
    VarHandle<vector<int>* > EF_j20_a2tc_EFFS;
    /// Did this object pass the chain EF_j20_a4hi_EFFS
    VarHandle<vector<int>* > EF_j20_a4hi_EFFS;
    /// Did this object pass the chain EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10
    VarHandle<vector<int>* > EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10;
    /// Did this object pass the chain EF_j75_j45_a4tc_EFFS_xe55_noMu
    VarHandle<vector<int>* > EF_j75_j45_a4tc_EFFS_xe55_noMu;

  private:
    const std::string m_sPrefix; ///< common prefix to the branch names

    ClassDef(TrigEFJetD3PDCollection,0)
  }; // class TrigEFJetD3PDCollection

} // namespace D3PDReader

#endif // D3PDREADER_TrigEFJetD3PDCollection_H
