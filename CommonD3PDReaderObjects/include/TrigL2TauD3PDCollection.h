// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------
#ifndef D3PDREADER_TrigL2TauD3PDCollection_H
#define D3PDREADER_TrigL2TauD3PDCollection_H

// STL include(s)
#include <vector>
using std::vector;

// ROOT include(s)
#include <TObject.h>
#include <TString.h>

// custom include(s)
#include "VarHandle.h"

class TTree;

namespace D3PDReader
{
  /**
   * Code generated by CodeGenerator on:
   * time = Wed Jul 11 19:00:15 2012
   */
  class TrigL2TauD3PDCollection : public TObject
  {
  public:
    /// Constructor used when reading from a TTree
    TrigL2TauD3PDCollection(const long int& master,const std::string& prefix = "trig_L2_tau_");
    /// Constructor when the object is only used for writing data out
    TrigL2TauD3PDCollection(const std::string& prefix = "trig_L2_tau_");
    /// standard destructor
    ~TrigL2TauD3PDCollection() {}
    /// Get the currently configured prefix value
    const std::string& GetPrefix() const {return m_sPrefix;}
    /// Connect the object to an input TTree
    void ReadFrom(TTree* tree);
    /// Connect the object to an output TTree
    void WriteTo(TTree* tree);
    /// Turn all (available) branches either on or off
    void SetActive(bool active = true);
    /// Read in all the variables that we need to write out as well
    void ReadAllActive();

    /// Number of ntuple rows.
    VarHandle<Int_t > n;
    VarHandle<vector<float>* > pt;
    VarHandle<vector<float>* > eta;
    VarHandle<vector<float>* > phi;
    VarHandle<vector<float>* > px;
    VarHandle<vector<float>* > py;
    VarHandle<vector<float>* > pz;
    VarHandle<vector<unsigned int>* > RoIWord;
    VarHandle<vector<float>* > simpleEtFlow;
    VarHandle<vector<int>* > nMatchedTracks;
    VarHandle<vector<float>* > trkAvgDist;
    VarHandle<vector<float>* > etOverPtLeadTrk;
    VarHandle<vector<int>* > cluster_quality;
    VarHandle<vector<float>* > cluster_EMenergy;
    VarHandle<vector<float>* > cluster_HADenergy;
    VarHandle<vector<float>* > cluster_eta;
    VarHandle<vector<float>* > cluster_phi;
    VarHandle<vector<float>* > cluster_CaloRadius;
    VarHandle<vector<float>* > cluster_EMRadius3S;
    VarHandle<vector<float>* > cluster_CoreFrac;
    VarHandle<vector<float>* > cluster_EMFrac;
    VarHandle<vector<float>* > cluster_HADRadius;
    VarHandle<vector<float>* > cluster_IsoFrac;
    VarHandle<vector<int>* > cluster_numTotCells;
    VarHandle<vector<float>* > cluster_stripWidth;
    VarHandle<vector<float>* > cluster_stripWidthOffline;
    VarHandle<vector<vector<float> >* > cluster_EMenergyWide;
    VarHandle<vector<vector<float> >* > cluster_EMenergyNarrow;
    VarHandle<vector<vector<float> >* > cluster_EMenergyMedium;
    VarHandle<vector<vector<float> >* > cluster_HADenergyWide;
    VarHandle<vector<vector<float> >* > cluster_HADenergyNarrow;
    VarHandle<vector<vector<float> >* > cluster_HADenergyMedium;
    VarHandle<vector<vector<float> >* > cluster_HADRadiusSamp;
    VarHandle<vector<vector<float> >* > cluster_EMRadiusSamp;
    VarHandle<vector<float>* > cluster_etNarrow;
    VarHandle<vector<float>* > cluster_etWide;
    VarHandle<vector<float>* > cluster_etMedium;
    VarHandle<vector<int>* > tracksinfo_nCoreTracks;
    VarHandle<vector<int>* > tracksinfo_nSlowTracks;
    VarHandle<vector<int>* > tracksinfo_nIsoTracks;
    VarHandle<vector<float>* > tracksinfo_charge;
    VarHandle<vector<float>* > tracksinfo_leadingTrackPt;
    VarHandle<vector<float>* > tracksinfo_scalarPtSumCore;
    VarHandle<vector<float>* > tracksinfo_scalarPtSumIso;
    VarHandle<vector<double>* > tracksinfo_3fastest_pt;
    VarHandle<vector<double>* > tracksinfo_3fastest_eta;
    VarHandle<vector<double>* > tracksinfo_3fastest_phi;
    VarHandle<vector<double>* > tracksinfo_3fastest_m;
    VarHandle<vector<int>* > tracks_algorithmId;
    /// Number of ntuple rows.
    VarHandle<vector<int>* > idscan_trk_n;
    /// Index in trig_L2_trk_idscan_tau_
    VarHandle<vector<vector<int> >* > idscan_trk_index;
    /// Number of ntuple rows.
    VarHandle<vector<int>* > sitrack_trk_n;
    /// Index in trig_L2_trk_sitrack_tau_
    VarHandle<vector<vector<int> >* > sitrack_trk_index;

  private:
    const std::string m_sPrefix; ///< common prefix to the branch names

    ClassDef(TrigL2TauD3PDCollection,0)
  }; // class TrigL2TauD3PDCollection

} // namespace D3PDReader

#endif // D3PDREADER_TrigL2TauD3PDCollection_H
