#ifndef D3PDREADER_VARHANDLE_H
#define D3PDREADER_VARHANDLE_H

// STL include(s):
#include <string>

// Forward declaration(s):
class TTree;
class TBranch;

namespace D3PDReader
{
  /**
   * @short base class for D3PDReader classes
   *
   * This class provides some common properties of D3PDReader classes
   * which are independent of the underlying data type.
   *
   * @author Christian Gumpert <cgumpert@cern.ch>
   */
  class VarHandleBase
  {
  protected:
    /// Custom enumeration describing the availability of the branch
    enum BranchAvailability
    {
       UNKNOWN = 0, ///< The input TTree has not yet been checked
       AVAILABLE = 1, ///< The input branch is available
       UNAVAILABLE = 2 ///< The input branch is not available
    };

  public:
    /// Constructor specifying all the needed parameters
    VarHandleBase(const std::string& sName,const long int* const master = 0);
    /// standard destructor 
    virtual ~VarHandleBase() {}

    /// Get the name of the branch handled by this class
    std::string GetName() const {return m_sName;}
    /// Set the name of the branch handled by this class
    void SetName(const std::string& sName) {m_sName = sName;}
    /// Connect the object to an input tree
    void ReadFrom(TTree* pTree);
    /// Check if this variable is "active" at the moment
    bool IsActive() const {return m_bIsActive;}
    /// Set the "activity level" of the variable
    void SetActive(bool active = true);
    /// Check if the variable is available in the input
    bool IsAvailable() const;

  protected:
    const long int* const m_pMaster; ///< Pointer to the current entry number
    std::string m_sName; ///< Name of the branch to handle
    bool m_bFromInput; ///< Flag showing if the variable is read from an input TTree
    TTree* m_pInTree; ///< The input TTree
    mutable TBranch* m_pInBranch; /// The input branch belonging to this variable
#ifdef WRITE_BRANCHES_USED
    bool m_bWriteInfo; ///< print name of branch on first usage
#endif // WRITE_BRANCHES_USED
    bool m_bIsActive; ///< Flag telling if the variable can be written to the output
    mutable BranchAvailability m_eAvailable; ///< Availability of the branch
  }; // class VarHandleBase

  /**
   *  @short Class responsible for reading primitive types from the D3PD
   *
   *  This class is used by all the D3PDReader classes to physically
   *  handle the branches of the input TTree.
   *
   * @author Christian Gumpert <cgumpert@cern.ch>
   *
   */
  template< typename Type >
  class VarHandle : public VarHandleBase
  {
  public:
    /// Constructor specifying all the needed parameters
    VarHandle(const std::string& sName,const long int* const master = 0);
    /// standard destructor 
    virtual ~VarHandle() {}

    /// Connect the object to an output tree
    TBranch* WriteTo(TTree* tree);

    /// access the current value of the branch itself
    Type& operator()();
    /// access the current value of the branch itself (constant version)
    const Type& operator()() const;

  private:
    /// Translate the typeid() type name to something ROOT understands
    const char* RootType(const char* typeid_type) const;

    mutable Type m_fVariable; ///< The variable in memory
  }; // class VarHandle

  /**
   *  @short Class responsible for reading STL objects from the D3PD
   *
   *  This specialization of the template class makes it possible to
   *  handle branches describing primitive types and branches describing
   *  pointer to STL collections.
   *
   * @author Christian Gumpert <cgumpert@cern.ch>
   *
   */
  template<typename Type>
  class VarHandle<Type*> : public VarHandleBase
  {
  public:
    /// Constructor specifying all the needed parameters
    VarHandle(const std::string& sName,const long int* const master = 0);
    /// standard destructor deleting the underlying variable
    ~VarHandle();

    /// Connect the object to an output tree
    TBranch* WriteTo(TTree* tree);
    /// access the current value of the branch itself
    Type* operator()();
    /// access the current value of the branch itself (constant version)
    const Type* operator()() const;

  private:
    mutable Type* m_fVariable; ///< The variable in memory
  }; // class VarHandle

} // namespace D3PDReader

// Include the implementation:
#ifndef __CINT__
#include "VarHandle.icc"
#endif // __CINT__

#endif // D3PDREADER_VARHANDLE_H

