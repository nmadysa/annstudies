// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------
#ifndef D3PDREADER_Vertex_H
#define D3PDREADER_Vertex_H

// custom include(s)
#include "VertexD3PDCollection.h"

namespace D3PDReader
{
  /**
   * Code generated by CodeGenerator on:
   * time = Wed Jul 11 19:00:17 2012
   */
  class Vertex : public TObject
  {
  public:
    Vertex(unsigned int index,VertexD3PDCollection& parent):
      TObject(),
      m_iIndex(index),
      m_pParent(&parent)
    {}

    Vertex(const Vertex& copy):
      TObject(copy),
      m_iIndex(copy.m_iIndex),
      m_pParent(copy.m_pParent)
    {}

    Vertex& operator=(const Vertex& rhs)
    {
      m_iIndex = rhs.m_iIndex;
      m_pParent = rhs.m_pParent;
      return *this;
    }

    bool operator==(unsigned int& rhs) {return (m_iIndex == rhs);}
    bool operator!=(unsigned int& rhs) {return !operator==(rhs);}

    unsigned int GetIndex() const {return m_iIndex;}
    float& x() {return m_pParent->x()->at(m_iIndex);}
    const float& x() const {return m_pParent->x()->at(m_iIndex);}
    float& y() {return m_pParent->y()->at(m_iIndex);}
    const float& y() const {return m_pParent->y()->at(m_iIndex);}
    float& z() {return m_pParent->z()->at(m_iIndex);}
    const float& z() const {return m_pParent->z()->at(m_iIndex);}
    int& type() {return m_pParent->type()->at(m_iIndex);}
    const int& type() const {return m_pParent->type()->at(m_iIndex);}
    float& chi2() {return m_pParent->chi2()->at(m_iIndex);}
    const float& chi2() const {return m_pParent->chi2()->at(m_iIndex);}
    int& ndof() {return m_pParent->ndof()->at(m_iIndex);}
    const int& ndof() const {return m_pParent->ndof()->at(m_iIndex);}
    float& px() {return m_pParent->px()->at(m_iIndex);}
    const float& px() const {return m_pParent->px()->at(m_iIndex);}
    float& py() {return m_pParent->py()->at(m_iIndex);}
    const float& py() const {return m_pParent->py()->at(m_iIndex);}
    float& pz() {return m_pParent->pz()->at(m_iIndex);}
    const float& pz() const {return m_pParent->pz()->at(m_iIndex);}
    float& E() {return m_pParent->E()->at(m_iIndex);}
    const float& E() const {return m_pParent->E()->at(m_iIndex);}
    float& m() {return m_pParent->m()->at(m_iIndex);}
    const float& m() const {return m_pParent->m()->at(m_iIndex);}
    int& nTracks() {return m_pParent->nTracks()->at(m_iIndex);}
    const int& nTracks() const {return m_pParent->nTracks()->at(m_iIndex);}
    float& sumPt() {return m_pParent->sumPt()->at(m_iIndex);}
    const float& sumPt() const {return m_pParent->sumPt()->at(m_iIndex);}
    vector<float>& trk_weight() {return m_pParent->trk_weight()->at(m_iIndex);}
    const vector<float>& trk_weight() const {return m_pParent->trk_weight()->at(m_iIndex);}
    int& trk_n() {return m_pParent->trk_n()->at(m_iIndex);}
    const int& trk_n() const {return m_pParent->trk_n()->at(m_iIndex);}
    vector<int>& trk_index() {return m_pParent->trk_index()->at(m_iIndex);}
    const vector<int>& trk_index() const {return m_pParent->trk_index()->at(m_iIndex);}

  private:
    unsigned int m_iIndex;
    VertexD3PDCollection* m_pParent;

    ClassDef(Vertex,0)
  }; // class Vertex

} // namespace D3PDReader

#endif // D3PDREADER_Vertex_H
