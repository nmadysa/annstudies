// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------
#ifndef D3PDREADER_TrigL1TauD3PDCollection_H
#define D3PDREADER_TrigL1TauD3PDCollection_H

// STL include(s)
#include <vector>
using std::vector;
#include <string>
using std::string;

// ROOT include(s)
#include <TObject.h>
#include <TString.h>

// custom include(s)
#include "VarHandle.h"

class TTree;

namespace D3PDReader
{
  /**
   * Code generated by CodeGenerator on:
   * time = Wed Jul 11 19:00:14 2012
   */
  class TrigL1TauD3PDCollection : public TObject
  {
  public:
    /// Constructor used when reading from a TTree
    TrigL1TauD3PDCollection(const long int& master,const std::string& prefix = "trig_L1_emtau_");
    /// Constructor when the object is only used for writing data out
    TrigL1TauD3PDCollection(const std::string& prefix = "trig_L1_emtau_");
    /// standard destructor
    ~TrigL1TauD3PDCollection() {}
    /// Get the currently configured prefix value
    const std::string& GetPrefix() const {return m_sPrefix;}
    /// Connect the object to an input TTree
    void ReadFrom(TTree* tree);
    /// Connect the object to an output TTree
    void WriteTo(TTree* tree);
    /// Turn all (available) branches either on or off
    void SetActive(bool active = true);
    /// Read in all the variables that we need to write out as well
    void ReadAllActive();

    /// Number of ntuple rows.
    VarHandle<Int_t > n;
    VarHandle<vector<float>* > eta;
    VarHandle<vector<float>* > phi;
    /// Names of the passed thresholds
    VarHandle<vector<vector<string> >* > thrNames;
    /// Values of the passed thresholds
    VarHandle<vector<vector<float> >* > thrValues;
    /// ET of the RoI Core cluster (2x2 towers, EM+Had)
    VarHandle<vector<float>* > core;
    /// Deposited ET of the "EM cluster"
    VarHandle<vector<float>* > EMClus;
    /// Deposited ET of the "tau cluster"
    VarHandle<vector<float>* > tauClus;
    /// EM calorimeter isolation (outer ring of EM towers)
    VarHandle<vector<float>* > EMIsol;
    /// Hadron calorimeter isolation (outer ring of had towers)
    VarHandle<vector<float>* > hadIsol;
    /// ET deposited in the inner hadronic isolation region (2x2 core)
    VarHandle<vector<float>* > hadCore;
    /// Bit-pattern describing the passed thresholds
    VarHandle<vector<unsigned int>* > thrPattern;
    /// 32-bit RoI word produced by the L1Calo hardware
    VarHandle<vector<unsigned int>* > RoIWord;

  private:
    const std::string m_sPrefix; ///< common prefix to the branch names

    ClassDef(TrigL1TauD3PDCollection,0)
  }; // class TrigL1TauD3PDCollection

} // namespace D3PDReader

#endif // D3PDREADER_TrigL1TauD3PDCollection_H
