#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;

#pragma link C++ class D3PDReader::EventInfo+;
#pragma link C++ class D3PDReader::TriggerD3PDCollection+;
#pragma link C++ class D3PDReader::MET+;
#pragma link C++ class D3PDReader::PhotonD3PDCollection+;
#pragma link C++ class D3PDReader::ElectronD3PDCollection+;
#pragma link C++ class D3PDReader::MuonD3PDCollection+;
#pragma link C++ class D3PDReader::TauD3PDCollection+;
#pragma link C++ class D3PDReader::TruthTauD3PDCollection+;
#pragma link C++ class D3PDReader::MCTruthD3PDCollection+;
#pragma link C++ class D3PDReader::JetD3PDCollection+;
#pragma link C++ class D3PDReader::VertexD3PDCollection+;
#pragma link C++ class D3PDReader::TrackD3PDCollection+;
#pragma link C++ class D3PDReader::ClusterD3PDCollection+;
#pragma link C++ class D3PDReader::MCTruthD3PDCollection+;
#pragma link C++ class D3PDReader::Photon+;
#pragma link C++ class D3PDReader::Electron+;
#pragma link C++ class D3PDReader::Muon+;
#pragma link C++ class D3PDReader::Tau+;
#pragma link C++ class D3PDReader::TruthTau+;
#pragma link C++ class D3PDReader::Jet+;
#pragma link C++ class D3PDReader::Vertex+;
#pragma link C++ class D3PDReader::Track+;
#pragma link C++ class D3PDReader::Cluster+;
#pragma link C++ class D3PDReader::MCTruthParticle+;
#pragma link C++ class D3PDReader::TrigEFElectronD3PDCollection+;
#pragma link C++ class D3PDReader::TrigEFElectron+;
//#pragma link C++ class D3PDReader::TrigMuonD3PDCollection+;
//#pragma link C++ class D3PDReader::TrigMuon+;
#pragma link C++ class D3PDReader::TrigEFMuonD3PDCollection+;
#pragma link C++ class D3PDReader::TrigEFMuon+;
#pragma link C++ class D3PDReader::TrigL1TauD3PDCollection+;
#pragma link C++ class D3PDReader::TrigL1Tau+;
#pragma link C++ class D3PDReader::TrigL2TauD3PDCollection+;
#pragma link C++ class D3PDReader::TrigL2Tau+;
#pragma link C++ class D3PDReader::TrigEFTauD3PDCollection+;
#pragma link C++ class D3PDReader::TrigEFTau+;
#pragma link C++ class D3PDReader::TrigEFJetD3PDCollection+;
#pragma link C++ class D3PDReader::TrigEFJet+;
#pragma link C++ class D3PDReader::TriggerInfoCollection+;
#pragma link C++ class D3PDReader::TriggerMetadataCollection+;

#pragma link C++ class std::vector<std::vector<int> >+;
#pragma link C++ class std::vector<std::vector<unsigned int> >+;
#pragma link C++ class std::vector<std::vector<long> >+;
#pragma link C++ class std::vector<std::vector<unsigned long> >+;
#pragma link C++ class std::vector<std::vector<float> >+;
#pragma link C++ class std::vector<std::vector<double> >+;
#pragma link C++ class std::vector<std::string>+;
#pragma link C++ class std::vector<std::vector<std::string> >+;

#pragma link C++ class D3PDReader::VarHandle<float>+;
#pragma link C++ class D3PDReader::VarHandle<int>+;
#pragma link C++ class D3PDReader::VarHandle<vector<float>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<int>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<vector<float> >*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<vector<int> >*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<vector<double> >*>+;
#pragma link C++ class D3PDReader::VarHandle<unsigned int>+;
#pragma link C++ class D3PDReader::VarHandle<vector<short>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<unsigned int>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<vector<unsigned int> >*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<unsigned short>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<double>*>+;

#endif // __CINT__
