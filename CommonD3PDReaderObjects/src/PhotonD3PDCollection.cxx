// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_PhotonD3PDCollection_CXX
#define D3PDREADER_PhotonD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "PhotonD3PDCollection.h"

ClassImp(D3PDReader::PhotonD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  PhotonD3PDCollection::PhotonD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    n(prefix + "n",&master),
    E(prefix + "E",&master),
    Et(prefix + "Et",&master),
    pt(prefix + "pt",&master),
    m(prefix + "m",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    px(prefix + "px",&master),
    py(prefix + "py",&master),
    pz(prefix + "pz",&master),
    author(prefix + "author",&master),
    isRecovered(prefix + "isRecovered",&master),
    isEM(prefix + "isEM",&master),
    isEMLoose(prefix + "isEMLoose",&master),
    isEMMedium(prefix + "isEMMedium",&master),
    isEMTight(prefix + "isEMTight",&master),
    OQ(prefix + "OQ",&master),
    OQRecalc(prefix + "OQRecalc",&master),
    convFlag(prefix + "convFlag",&master),
    isConv(prefix + "isConv",&master),
    nConv(prefix + "nConv",&master),
    nSingleTrackConv(prefix + "nSingleTrackConv",&master),
    nDoubleTrackConv(prefix + "nDoubleTrackConv",&master),
    truth_deltaRRecPhoton(prefix + "truth_deltaRRecPhoton",&master),
    truth_E(prefix + "truth_E",&master),
    truth_pt(prefix + "truth_pt",&master),
    truth_eta(prefix + "truth_eta",&master),
    truth_phi(prefix + "truth_phi",&master),
    truth_type(prefix + "truth_type",&master),
    truth_status(prefix + "truth_status",&master),
    truth_barcode(prefix + "truth_barcode",&master),
    truth_mothertype(prefix + "truth_mothertype",&master),
    truth_motherbarcode(prefix + "truth_motherbarcode",&master),
    truth_index(prefix + "truth_index",&master),
    truth_matched(prefix + "truth_matched",&master),
    loose(prefix + "loose",&master),
    looseIso(prefix + "looseIso",&master),
    tight(prefix + "tight",&master),
    tightIso(prefix + "tightIso",&master),
    looseAR(prefix + "looseAR",&master),
    looseARIso(prefix + "looseARIso",&master),
    tightAR(prefix + "tightAR",&master),
    tightARIso(prefix + "tightARIso",&master),
    goodOQ(prefix + "goodOQ",&master),
    Ethad(prefix + "Ethad",&master),
    Ethad1(prefix + "Ethad1",&master),
    f1(prefix + "f1",&master),
    f1core(prefix + "f1core",&master),
    Emins1(prefix + "Emins1",&master),
    fside(prefix + "fside",&master),
    Emax2(prefix + "Emax2",&master),
    ws3(prefix + "ws3",&master),
    wstot(prefix + "wstot",&master),
    E132(prefix + "E132",&master),
    E1152(prefix + "E1152",&master),
    emaxs1(prefix + "emaxs1",&master),
    E233(prefix + "E233",&master),
    E237(prefix + "E237",&master),
    E277(prefix + "E277",&master),
    weta2(prefix + "weta2",&master),
    rphiallcalo(prefix + "rphiallcalo",&master),
    Etcone45(prefix + "Etcone45",&master),
    Etcone15(prefix + "Etcone15",&master),
    Etcone20(prefix + "Etcone20",&master),
    Etcone25(prefix + "Etcone25",&master),
    Etcone30(prefix + "Etcone30",&master),
    Etcone35(prefix + "Etcone35",&master),
    Etcone40(prefix + "Etcone40",&master),
    ptcone20(prefix + "ptcone20",&master),
    ptcone30(prefix + "ptcone30",&master),
    ptcone40(prefix + "ptcone40",&master),
    nucone20(prefix + "nucone20",&master),
    nucone30(prefix + "nucone30",&master),
    nucone40(prefix + "nucone40",&master),
    Etcone15_pt_corrected(prefix + "Etcone15_pt_corrected",&master),
    Etcone20_pt_corrected(prefix + "Etcone20_pt_corrected",&master),
    Etcone25_pt_corrected(prefix + "Etcone25_pt_corrected",&master),
    Etcone30_pt_corrected(prefix + "Etcone30_pt_corrected",&master),
    Etcone35_pt_corrected(prefix + "Etcone35_pt_corrected",&master),
    Etcone40_pt_corrected(prefix + "Etcone40_pt_corrected",&master),
    zvertex(prefix + "zvertex",&master),
    errz(prefix + "errz",&master),
    etap(prefix + "etap",&master),
    depth(prefix + "depth",&master),
    cl_E(prefix + "cl_E",&master),
    cl_pt(prefix + "cl_pt",&master),
    cl_eta(prefix + "cl_eta",&master),
    cl_phi(prefix + "cl_phi",&master),
    Es0(prefix + "Es0",&master),
    etas0(prefix + "etas0",&master),
    phis0(prefix + "phis0",&master),
    Es1(prefix + "Es1",&master),
    etas1(prefix + "etas1",&master),
    phis1(prefix + "phis1",&master),
    Es2(prefix + "Es2",&master),
    etas2(prefix + "etas2",&master),
    phis2(prefix + "phis2",&master),
    Es3(prefix + "Es3",&master),
    etas3(prefix + "etas3",&master),
    phis3(prefix + "phis3",&master),
    truth_isConv(prefix + "truth_isConv",&master),
    truth_isBrem(prefix + "truth_isBrem",&master),
    truth_isFromHardProc(prefix + "truth_isFromHardProc",&master),
    truth_isPhotonFromHardProc(prefix + "truth_isPhotonFromHardProc",&master),
    convIP(prefix + "convIP",&master),
    convIPRev(prefix + "convIPRev",&master),
    ptIsolationCone(prefix + "ptIsolationCone",&master),
    ptIsolationConePhAngle(prefix + "ptIsolationConePhAngle",&master),
    Etcone40_ED_corrected(prefix + "Etcone40_ED_corrected",&master),
    Etcone40_corrected(prefix + "Etcone40_corrected",&master),
    MET_n(prefix + "MET_n",&master),
    MET_wpx(prefix + "MET_wpx",&master),
    MET_wpy(prefix + "MET_wpy",&master),
    MET_wet(prefix + "MET_wet",&master),
    MET_statusWord(prefix + "MET_statusWord",&master),
    MET_BDTMedium_n(prefix + "MET_BDTMedium_n",&master),
    MET_BDTMedium_wpx(prefix + "MET_BDTMedium_wpx",&master),
    MET_BDTMedium_wpy(prefix + "MET_BDTMedium_wpy",&master),
    MET_BDTMedium_wet(prefix + "MET_BDTMedium_wet",&master),
    MET_BDTMedium_statusWord(prefix + "MET_BDTMedium_statusWord",&master),
    MET_em_tightpp_n(prefix + "MET_em_tightpp_n",&master),
    MET_em_tightpp_wpx(prefix + "MET_em_tightpp_wpx",&master),
    MET_em_tightpp_wpy(prefix + "MET_em_tightpp_wpy",&master),
    MET_em_tightpp_wet(prefix + "MET_em_tightpp_wet",&master),
    MET_em_tightpp_statusWord(prefix + "MET_em_tightpp_statusWord",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  PhotonD3PDCollection::PhotonD3PDCollection(const std::string& prefix):
    TObject(),
    n(prefix + "n",0),
    E(prefix + "E",0),
    Et(prefix + "Et",0),
    pt(prefix + "pt",0),
    m(prefix + "m",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    px(prefix + "px",0),
    py(prefix + "py",0),
    pz(prefix + "pz",0),
    author(prefix + "author",0),
    isRecovered(prefix + "isRecovered",0),
    isEM(prefix + "isEM",0),
    isEMLoose(prefix + "isEMLoose",0),
    isEMMedium(prefix + "isEMMedium",0),
    isEMTight(prefix + "isEMTight",0),
    OQ(prefix + "OQ",0),
    OQRecalc(prefix + "OQRecalc",0),
    convFlag(prefix + "convFlag",0),
    isConv(prefix + "isConv",0),
    nConv(prefix + "nConv",0),
    nSingleTrackConv(prefix + "nSingleTrackConv",0),
    nDoubleTrackConv(prefix + "nDoubleTrackConv",0),
    truth_deltaRRecPhoton(prefix + "truth_deltaRRecPhoton",0),
    truth_E(prefix + "truth_E",0),
    truth_pt(prefix + "truth_pt",0),
    truth_eta(prefix + "truth_eta",0),
    truth_phi(prefix + "truth_phi",0),
    truth_type(prefix + "truth_type",0),
    truth_status(prefix + "truth_status",0),
    truth_barcode(prefix + "truth_barcode",0),
    truth_mothertype(prefix + "truth_mothertype",0),
    truth_motherbarcode(prefix + "truth_motherbarcode",0),
    truth_index(prefix + "truth_index",0),
    truth_matched(prefix + "truth_matched",0),
    loose(prefix + "loose",0),
    looseIso(prefix + "looseIso",0),
    tight(prefix + "tight",0),
    tightIso(prefix + "tightIso",0),
    looseAR(prefix + "looseAR",0),
    looseARIso(prefix + "looseARIso",0),
    tightAR(prefix + "tightAR",0),
    tightARIso(prefix + "tightARIso",0),
    goodOQ(prefix + "goodOQ",0),
    Ethad(prefix + "Ethad",0),
    Ethad1(prefix + "Ethad1",0),
    f1(prefix + "f1",0),
    f1core(prefix + "f1core",0),
    Emins1(prefix + "Emins1",0),
    fside(prefix + "fside",0),
    Emax2(prefix + "Emax2",0),
    ws3(prefix + "ws3",0),
    wstot(prefix + "wstot",0),
    E132(prefix + "E132",0),
    E1152(prefix + "E1152",0),
    emaxs1(prefix + "emaxs1",0),
    E233(prefix + "E233",0),
    E237(prefix + "E237",0),
    E277(prefix + "E277",0),
    weta2(prefix + "weta2",0),
    rphiallcalo(prefix + "rphiallcalo",0),
    Etcone45(prefix + "Etcone45",0),
    Etcone15(prefix + "Etcone15",0),
    Etcone20(prefix + "Etcone20",0),
    Etcone25(prefix + "Etcone25",0),
    Etcone30(prefix + "Etcone30",0),
    Etcone35(prefix + "Etcone35",0),
    Etcone40(prefix + "Etcone40",0),
    ptcone20(prefix + "ptcone20",0),
    ptcone30(prefix + "ptcone30",0),
    ptcone40(prefix + "ptcone40",0),
    nucone20(prefix + "nucone20",0),
    nucone30(prefix + "nucone30",0),
    nucone40(prefix + "nucone40",0),
    Etcone15_pt_corrected(prefix + "Etcone15_pt_corrected",0),
    Etcone20_pt_corrected(prefix + "Etcone20_pt_corrected",0),
    Etcone25_pt_corrected(prefix + "Etcone25_pt_corrected",0),
    Etcone30_pt_corrected(prefix + "Etcone30_pt_corrected",0),
    Etcone35_pt_corrected(prefix + "Etcone35_pt_corrected",0),
    Etcone40_pt_corrected(prefix + "Etcone40_pt_corrected",0),
    zvertex(prefix + "zvertex",0),
    errz(prefix + "errz",0),
    etap(prefix + "etap",0),
    depth(prefix + "depth",0),
    cl_E(prefix + "cl_E",0),
    cl_pt(prefix + "cl_pt",0),
    cl_eta(prefix + "cl_eta",0),
    cl_phi(prefix + "cl_phi",0),
    Es0(prefix + "Es0",0),
    etas0(prefix + "etas0",0),
    phis0(prefix + "phis0",0),
    Es1(prefix + "Es1",0),
    etas1(prefix + "etas1",0),
    phis1(prefix + "phis1",0),
    Es2(prefix + "Es2",0),
    etas2(prefix + "etas2",0),
    phis2(prefix + "phis2",0),
    Es3(prefix + "Es3",0),
    etas3(prefix + "etas3",0),
    phis3(prefix + "phis3",0),
    truth_isConv(prefix + "truth_isConv",0),
    truth_isBrem(prefix + "truth_isBrem",0),
    truth_isFromHardProc(prefix + "truth_isFromHardProc",0),
    truth_isPhotonFromHardProc(prefix + "truth_isPhotonFromHardProc",0),
    convIP(prefix + "convIP",0),
    convIPRev(prefix + "convIPRev",0),
    ptIsolationCone(prefix + "ptIsolationCone",0),
    ptIsolationConePhAngle(prefix + "ptIsolationConePhAngle",0),
    Etcone40_ED_corrected(prefix + "Etcone40_ED_corrected",0),
    Etcone40_corrected(prefix + "Etcone40_corrected",0),
    MET_n(prefix + "MET_n",0),
    MET_wpx(prefix + "MET_wpx",0),
    MET_wpy(prefix + "MET_wpy",0),
    MET_wet(prefix + "MET_wet",0),
    MET_statusWord(prefix + "MET_statusWord",0),
    MET_BDTMedium_n(prefix + "MET_BDTMedium_n",0),
    MET_BDTMedium_wpx(prefix + "MET_BDTMedium_wpx",0),
    MET_BDTMedium_wpy(prefix + "MET_BDTMedium_wpy",0),
    MET_BDTMedium_wet(prefix + "MET_BDTMedium_wet",0),
    MET_BDTMedium_statusWord(prefix + "MET_BDTMedium_statusWord",0),
    MET_em_tightpp_n(prefix + "MET_em_tightpp_n",0),
    MET_em_tightpp_wpx(prefix + "MET_em_tightpp_wpx",0),
    MET_em_tightpp_wpy(prefix + "MET_em_tightpp_wpy",0),
    MET_em_tightpp_wet(prefix + "MET_em_tightpp_wet",0),
    MET_em_tightpp_statusWord(prefix + "MET_em_tightpp_statusWord",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void PhotonD3PDCollection::ReadFrom(TTree* tree)
  {
    n.ReadFrom(tree);
    E.ReadFrom(tree);
    Et.ReadFrom(tree);
    pt.ReadFrom(tree);
    m.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    px.ReadFrom(tree);
    py.ReadFrom(tree);
    pz.ReadFrom(tree);
    author.ReadFrom(tree);
    isRecovered.ReadFrom(tree);
    isEM.ReadFrom(tree);
    isEMLoose.ReadFrom(tree);
    isEMMedium.ReadFrom(tree);
    isEMTight.ReadFrom(tree);
    OQ.ReadFrom(tree);
    OQRecalc.ReadFrom(tree);
    convFlag.ReadFrom(tree);
    isConv.ReadFrom(tree);
    nConv.ReadFrom(tree);
    nSingleTrackConv.ReadFrom(tree);
    nDoubleTrackConv.ReadFrom(tree);
    truth_deltaRRecPhoton.ReadFrom(tree);
    truth_E.ReadFrom(tree);
    truth_pt.ReadFrom(tree);
    truth_eta.ReadFrom(tree);
    truth_phi.ReadFrom(tree);
    truth_type.ReadFrom(tree);
    truth_status.ReadFrom(tree);
    truth_barcode.ReadFrom(tree);
    truth_mothertype.ReadFrom(tree);
    truth_motherbarcode.ReadFrom(tree);
    truth_index.ReadFrom(tree);
    truth_matched.ReadFrom(tree);
    loose.ReadFrom(tree);
    looseIso.ReadFrom(tree);
    tight.ReadFrom(tree);
    tightIso.ReadFrom(tree);
    looseAR.ReadFrom(tree);
    looseARIso.ReadFrom(tree);
    tightAR.ReadFrom(tree);
    tightARIso.ReadFrom(tree);
    goodOQ.ReadFrom(tree);
    Ethad.ReadFrom(tree);
    Ethad1.ReadFrom(tree);
    f1.ReadFrom(tree);
    f1core.ReadFrom(tree);
    Emins1.ReadFrom(tree);
    fside.ReadFrom(tree);
    Emax2.ReadFrom(tree);
    ws3.ReadFrom(tree);
    wstot.ReadFrom(tree);
    E132.ReadFrom(tree);
    E1152.ReadFrom(tree);
    emaxs1.ReadFrom(tree);
    E233.ReadFrom(tree);
    E237.ReadFrom(tree);
    E277.ReadFrom(tree);
    weta2.ReadFrom(tree);
    rphiallcalo.ReadFrom(tree);
    Etcone45.ReadFrom(tree);
    Etcone15.ReadFrom(tree);
    Etcone20.ReadFrom(tree);
    Etcone25.ReadFrom(tree);
    Etcone30.ReadFrom(tree);
    Etcone35.ReadFrom(tree);
    Etcone40.ReadFrom(tree);
    ptcone20.ReadFrom(tree);
    ptcone30.ReadFrom(tree);
    ptcone40.ReadFrom(tree);
    nucone20.ReadFrom(tree);
    nucone30.ReadFrom(tree);
    nucone40.ReadFrom(tree);
    Etcone15_pt_corrected.ReadFrom(tree);
    Etcone20_pt_corrected.ReadFrom(tree);
    Etcone25_pt_corrected.ReadFrom(tree);
    Etcone30_pt_corrected.ReadFrom(tree);
    Etcone35_pt_corrected.ReadFrom(tree);
    Etcone40_pt_corrected.ReadFrom(tree);
    zvertex.ReadFrom(tree);
    errz.ReadFrom(tree);
    etap.ReadFrom(tree);
    depth.ReadFrom(tree);
    cl_E.ReadFrom(tree);
    cl_pt.ReadFrom(tree);
    cl_eta.ReadFrom(tree);
    cl_phi.ReadFrom(tree);
    Es0.ReadFrom(tree);
    etas0.ReadFrom(tree);
    phis0.ReadFrom(tree);
    Es1.ReadFrom(tree);
    etas1.ReadFrom(tree);
    phis1.ReadFrom(tree);
    Es2.ReadFrom(tree);
    etas2.ReadFrom(tree);
    phis2.ReadFrom(tree);
    Es3.ReadFrom(tree);
    etas3.ReadFrom(tree);
    phis3.ReadFrom(tree);
    truth_isConv.ReadFrom(tree);
    truth_isBrem.ReadFrom(tree);
    truth_isFromHardProc.ReadFrom(tree);
    truth_isPhotonFromHardProc.ReadFrom(tree);
    convIP.ReadFrom(tree);
    convIPRev.ReadFrom(tree);
    ptIsolationCone.ReadFrom(tree);
    ptIsolationConePhAngle.ReadFrom(tree);
    Etcone40_ED_corrected.ReadFrom(tree);
    Etcone40_corrected.ReadFrom(tree);
    MET_n.ReadFrom(tree);
    MET_wpx.ReadFrom(tree);
    MET_wpy.ReadFrom(tree);
    MET_wet.ReadFrom(tree);
    MET_statusWord.ReadFrom(tree);
    MET_BDTMedium_n.ReadFrom(tree);
    MET_BDTMedium_wpx.ReadFrom(tree);
    MET_BDTMedium_wpy.ReadFrom(tree);
    MET_BDTMedium_wet.ReadFrom(tree);
    MET_BDTMedium_statusWord.ReadFrom(tree);
    MET_em_tightpp_n.ReadFrom(tree);
    MET_em_tightpp_wpx.ReadFrom(tree);
    MET_em_tightpp_wpy.ReadFrom(tree);
    MET_em_tightpp_wet.ReadFrom(tree);
    MET_em_tightpp_statusWord.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void PhotonD3PDCollection::WriteTo(TTree* tree)
  {
    n.WriteTo(tree);
    E.WriteTo(tree);
    Et.WriteTo(tree);
    pt.WriteTo(tree);
    m.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    px.WriteTo(tree);
    py.WriteTo(tree);
    pz.WriteTo(tree);
    author.WriteTo(tree);
    isRecovered.WriteTo(tree);
    isEM.WriteTo(tree);
    isEMLoose.WriteTo(tree);
    isEMMedium.WriteTo(tree);
    isEMTight.WriteTo(tree);
    OQ.WriteTo(tree);
    OQRecalc.WriteTo(tree);
    convFlag.WriteTo(tree);
    isConv.WriteTo(tree);
    nConv.WriteTo(tree);
    nSingleTrackConv.WriteTo(tree);
    nDoubleTrackConv.WriteTo(tree);
    truth_deltaRRecPhoton.WriteTo(tree);
    truth_E.WriteTo(tree);
    truth_pt.WriteTo(tree);
    truth_eta.WriteTo(tree);
    truth_phi.WriteTo(tree);
    truth_type.WriteTo(tree);
    truth_status.WriteTo(tree);
    truth_barcode.WriteTo(tree);
    truth_mothertype.WriteTo(tree);
    truth_motherbarcode.WriteTo(tree);
    truth_index.WriteTo(tree);
    truth_matched.WriteTo(tree);
    loose.WriteTo(tree);
    looseIso.WriteTo(tree);
    tight.WriteTo(tree);
    tightIso.WriteTo(tree);
    looseAR.WriteTo(tree);
    looseARIso.WriteTo(tree);
    tightAR.WriteTo(tree);
    tightARIso.WriteTo(tree);
    goodOQ.WriteTo(tree);
    Ethad.WriteTo(tree);
    Ethad1.WriteTo(tree);
    f1.WriteTo(tree);
    f1core.WriteTo(tree);
    Emins1.WriteTo(tree);
    fside.WriteTo(tree);
    Emax2.WriteTo(tree);
    ws3.WriteTo(tree);
    wstot.WriteTo(tree);
    E132.WriteTo(tree);
    E1152.WriteTo(tree);
    emaxs1.WriteTo(tree);
    E233.WriteTo(tree);
    E237.WriteTo(tree);
    E277.WriteTo(tree);
    weta2.WriteTo(tree);
    rphiallcalo.WriteTo(tree);
    Etcone45.WriteTo(tree);
    Etcone15.WriteTo(tree);
    Etcone20.WriteTo(tree);
    Etcone25.WriteTo(tree);
    Etcone30.WriteTo(tree);
    Etcone35.WriteTo(tree);
    Etcone40.WriteTo(tree);
    ptcone20.WriteTo(tree);
    ptcone30.WriteTo(tree);
    ptcone40.WriteTo(tree);
    nucone20.WriteTo(tree);
    nucone30.WriteTo(tree);
    nucone40.WriteTo(tree);
    Etcone15_pt_corrected.WriteTo(tree);
    Etcone20_pt_corrected.WriteTo(tree);
    Etcone25_pt_corrected.WriteTo(tree);
    Etcone30_pt_corrected.WriteTo(tree);
    Etcone35_pt_corrected.WriteTo(tree);
    Etcone40_pt_corrected.WriteTo(tree);
    zvertex.WriteTo(tree);
    errz.WriteTo(tree);
    etap.WriteTo(tree);
    depth.WriteTo(tree);
    cl_E.WriteTo(tree);
    cl_pt.WriteTo(tree);
    cl_eta.WriteTo(tree);
    cl_phi.WriteTo(tree);
    Es0.WriteTo(tree);
    etas0.WriteTo(tree);
    phis0.WriteTo(tree);
    Es1.WriteTo(tree);
    etas1.WriteTo(tree);
    phis1.WriteTo(tree);
    Es2.WriteTo(tree);
    etas2.WriteTo(tree);
    phis2.WriteTo(tree);
    Es3.WriteTo(tree);
    etas3.WriteTo(tree);
    phis3.WriteTo(tree);
    truth_isConv.WriteTo(tree);
    truth_isBrem.WriteTo(tree);
    truth_isFromHardProc.WriteTo(tree);
    truth_isPhotonFromHardProc.WriteTo(tree);
    convIP.WriteTo(tree);
    convIPRev.WriteTo(tree);
    ptIsolationCone.WriteTo(tree);
    ptIsolationConePhAngle.WriteTo(tree);
    Etcone40_ED_corrected.WriteTo(tree);
    Etcone40_corrected.WriteTo(tree);
    MET_n.WriteTo(tree);
    MET_wpx.WriteTo(tree);
    MET_wpy.WriteTo(tree);
    MET_wet.WriteTo(tree);
    MET_statusWord.WriteTo(tree);
    MET_BDTMedium_n.WriteTo(tree);
    MET_BDTMedium_wpx.WriteTo(tree);
    MET_BDTMedium_wpy.WriteTo(tree);
    MET_BDTMedium_wet.WriteTo(tree);
    MET_BDTMedium_statusWord.WriteTo(tree);
    MET_em_tightpp_n.WriteTo(tree);
    MET_em_tightpp_wpx.WriteTo(tree);
    MET_em_tightpp_wpy.WriteTo(tree);
    MET_em_tightpp_wet.WriteTo(tree);
    MET_em_tightpp_statusWord.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void PhotonD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(n.IsAvailable()) n.SetActive(active);
     if(E.IsAvailable()) E.SetActive(active);
     if(Et.IsAvailable()) Et.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(m.IsAvailable()) m.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(px.IsAvailable()) px.SetActive(active);
     if(py.IsAvailable()) py.SetActive(active);
     if(pz.IsAvailable()) pz.SetActive(active);
     if(author.IsAvailable()) author.SetActive(active);
     if(isRecovered.IsAvailable()) isRecovered.SetActive(active);
     if(isEM.IsAvailable()) isEM.SetActive(active);
     if(isEMLoose.IsAvailable()) isEMLoose.SetActive(active);
     if(isEMMedium.IsAvailable()) isEMMedium.SetActive(active);
     if(isEMTight.IsAvailable()) isEMTight.SetActive(active);
     if(OQ.IsAvailable()) OQ.SetActive(active);
     if(OQRecalc.IsAvailable()) OQRecalc.SetActive(active);
     if(convFlag.IsAvailable()) convFlag.SetActive(active);
     if(isConv.IsAvailable()) isConv.SetActive(active);
     if(nConv.IsAvailable()) nConv.SetActive(active);
     if(nSingleTrackConv.IsAvailable()) nSingleTrackConv.SetActive(active);
     if(nDoubleTrackConv.IsAvailable()) nDoubleTrackConv.SetActive(active);
     if(truth_deltaRRecPhoton.IsAvailable()) truth_deltaRRecPhoton.SetActive(active);
     if(truth_E.IsAvailable()) truth_E.SetActive(active);
     if(truth_pt.IsAvailable()) truth_pt.SetActive(active);
     if(truth_eta.IsAvailable()) truth_eta.SetActive(active);
     if(truth_phi.IsAvailable()) truth_phi.SetActive(active);
     if(truth_type.IsAvailable()) truth_type.SetActive(active);
     if(truth_status.IsAvailable()) truth_status.SetActive(active);
     if(truth_barcode.IsAvailable()) truth_barcode.SetActive(active);
     if(truth_mothertype.IsAvailable()) truth_mothertype.SetActive(active);
     if(truth_motherbarcode.IsAvailable()) truth_motherbarcode.SetActive(active);
     if(truth_index.IsAvailable()) truth_index.SetActive(active);
     if(truth_matched.IsAvailable()) truth_matched.SetActive(active);
     if(loose.IsAvailable()) loose.SetActive(active);
     if(looseIso.IsAvailable()) looseIso.SetActive(active);
     if(tight.IsAvailable()) tight.SetActive(active);
     if(tightIso.IsAvailable()) tightIso.SetActive(active);
     if(looseAR.IsAvailable()) looseAR.SetActive(active);
     if(looseARIso.IsAvailable()) looseARIso.SetActive(active);
     if(tightAR.IsAvailable()) tightAR.SetActive(active);
     if(tightARIso.IsAvailable()) tightARIso.SetActive(active);
     if(goodOQ.IsAvailable()) goodOQ.SetActive(active);
     if(Ethad.IsAvailable()) Ethad.SetActive(active);
     if(Ethad1.IsAvailable()) Ethad1.SetActive(active);
     if(f1.IsAvailable()) f1.SetActive(active);
     if(f1core.IsAvailable()) f1core.SetActive(active);
     if(Emins1.IsAvailable()) Emins1.SetActive(active);
     if(fside.IsAvailable()) fside.SetActive(active);
     if(Emax2.IsAvailable()) Emax2.SetActive(active);
     if(ws3.IsAvailable()) ws3.SetActive(active);
     if(wstot.IsAvailable()) wstot.SetActive(active);
     if(E132.IsAvailable()) E132.SetActive(active);
     if(E1152.IsAvailable()) E1152.SetActive(active);
     if(emaxs1.IsAvailable()) emaxs1.SetActive(active);
     if(E233.IsAvailable()) E233.SetActive(active);
     if(E237.IsAvailable()) E237.SetActive(active);
     if(E277.IsAvailable()) E277.SetActive(active);
     if(weta2.IsAvailable()) weta2.SetActive(active);
     if(rphiallcalo.IsAvailable()) rphiallcalo.SetActive(active);
     if(Etcone45.IsAvailable()) Etcone45.SetActive(active);
     if(Etcone15.IsAvailable()) Etcone15.SetActive(active);
     if(Etcone20.IsAvailable()) Etcone20.SetActive(active);
     if(Etcone25.IsAvailable()) Etcone25.SetActive(active);
     if(Etcone30.IsAvailable()) Etcone30.SetActive(active);
     if(Etcone35.IsAvailable()) Etcone35.SetActive(active);
     if(Etcone40.IsAvailable()) Etcone40.SetActive(active);
     if(ptcone20.IsAvailable()) ptcone20.SetActive(active);
     if(ptcone30.IsAvailable()) ptcone30.SetActive(active);
     if(ptcone40.IsAvailable()) ptcone40.SetActive(active);
     if(nucone20.IsAvailable()) nucone20.SetActive(active);
     if(nucone30.IsAvailable()) nucone30.SetActive(active);
     if(nucone40.IsAvailable()) nucone40.SetActive(active);
     if(Etcone15_pt_corrected.IsAvailable()) Etcone15_pt_corrected.SetActive(active);
     if(Etcone20_pt_corrected.IsAvailable()) Etcone20_pt_corrected.SetActive(active);
     if(Etcone25_pt_corrected.IsAvailable()) Etcone25_pt_corrected.SetActive(active);
     if(Etcone30_pt_corrected.IsAvailable()) Etcone30_pt_corrected.SetActive(active);
     if(Etcone35_pt_corrected.IsAvailable()) Etcone35_pt_corrected.SetActive(active);
     if(Etcone40_pt_corrected.IsAvailable()) Etcone40_pt_corrected.SetActive(active);
     if(zvertex.IsAvailable()) zvertex.SetActive(active);
     if(errz.IsAvailable()) errz.SetActive(active);
     if(etap.IsAvailable()) etap.SetActive(active);
     if(depth.IsAvailable()) depth.SetActive(active);
     if(cl_E.IsAvailable()) cl_E.SetActive(active);
     if(cl_pt.IsAvailable()) cl_pt.SetActive(active);
     if(cl_eta.IsAvailable()) cl_eta.SetActive(active);
     if(cl_phi.IsAvailable()) cl_phi.SetActive(active);
     if(Es0.IsAvailable()) Es0.SetActive(active);
     if(etas0.IsAvailable()) etas0.SetActive(active);
     if(phis0.IsAvailable()) phis0.SetActive(active);
     if(Es1.IsAvailable()) Es1.SetActive(active);
     if(etas1.IsAvailable()) etas1.SetActive(active);
     if(phis1.IsAvailable()) phis1.SetActive(active);
     if(Es2.IsAvailable()) Es2.SetActive(active);
     if(etas2.IsAvailable()) etas2.SetActive(active);
     if(phis2.IsAvailable()) phis2.SetActive(active);
     if(Es3.IsAvailable()) Es3.SetActive(active);
     if(etas3.IsAvailable()) etas3.SetActive(active);
     if(phis3.IsAvailable()) phis3.SetActive(active);
     if(truth_isConv.IsAvailable()) truth_isConv.SetActive(active);
     if(truth_isBrem.IsAvailable()) truth_isBrem.SetActive(active);
     if(truth_isFromHardProc.IsAvailable()) truth_isFromHardProc.SetActive(active);
     if(truth_isPhotonFromHardProc.IsAvailable()) truth_isPhotonFromHardProc.SetActive(active);
     if(convIP.IsAvailable()) convIP.SetActive(active);
     if(convIPRev.IsAvailable()) convIPRev.SetActive(active);
     if(ptIsolationCone.IsAvailable()) ptIsolationCone.SetActive(active);
     if(ptIsolationConePhAngle.IsAvailable()) ptIsolationConePhAngle.SetActive(active);
     if(Etcone40_ED_corrected.IsAvailable()) Etcone40_ED_corrected.SetActive(active);
     if(Etcone40_corrected.IsAvailable()) Etcone40_corrected.SetActive(active);
     if(MET_n.IsAvailable()) MET_n.SetActive(active);
     if(MET_wpx.IsAvailable()) MET_wpx.SetActive(active);
     if(MET_wpy.IsAvailable()) MET_wpy.SetActive(active);
     if(MET_wet.IsAvailable()) MET_wet.SetActive(active);
     if(MET_statusWord.IsAvailable()) MET_statusWord.SetActive(active);
     if(MET_BDTMedium_n.IsAvailable()) MET_BDTMedium_n.SetActive(active);
     if(MET_BDTMedium_wpx.IsAvailable()) MET_BDTMedium_wpx.SetActive(active);
     if(MET_BDTMedium_wpy.IsAvailable()) MET_BDTMedium_wpy.SetActive(active);
     if(MET_BDTMedium_wet.IsAvailable()) MET_BDTMedium_wet.SetActive(active);
     if(MET_BDTMedium_statusWord.IsAvailable()) MET_BDTMedium_statusWord.SetActive(active);
     if(MET_em_tightpp_n.IsAvailable()) MET_em_tightpp_n.SetActive(active);
     if(MET_em_tightpp_wpx.IsAvailable()) MET_em_tightpp_wpx.SetActive(active);
     if(MET_em_tightpp_wpy.IsAvailable()) MET_em_tightpp_wpy.SetActive(active);
     if(MET_em_tightpp_wet.IsAvailable()) MET_em_tightpp_wet.SetActive(active);
     if(MET_em_tightpp_statusWord.IsAvailable()) MET_em_tightpp_statusWord.SetActive(active);
    }
    else
    {
      n.SetActive(active);
      E.SetActive(active);
      Et.SetActive(active);
      pt.SetActive(active);
      m.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      px.SetActive(active);
      py.SetActive(active);
      pz.SetActive(active);
      author.SetActive(active);
      isRecovered.SetActive(active);
      isEM.SetActive(active);
      isEMLoose.SetActive(active);
      isEMMedium.SetActive(active);
      isEMTight.SetActive(active);
      OQ.SetActive(active);
      OQRecalc.SetActive(active);
      convFlag.SetActive(active);
      isConv.SetActive(active);
      nConv.SetActive(active);
      nSingleTrackConv.SetActive(active);
      nDoubleTrackConv.SetActive(active);
      truth_deltaRRecPhoton.SetActive(active);
      truth_E.SetActive(active);
      truth_pt.SetActive(active);
      truth_eta.SetActive(active);
      truth_phi.SetActive(active);
      truth_type.SetActive(active);
      truth_status.SetActive(active);
      truth_barcode.SetActive(active);
      truth_mothertype.SetActive(active);
      truth_motherbarcode.SetActive(active);
      truth_index.SetActive(active);
      truth_matched.SetActive(active);
      loose.SetActive(active);
      looseIso.SetActive(active);
      tight.SetActive(active);
      tightIso.SetActive(active);
      looseAR.SetActive(active);
      looseARIso.SetActive(active);
      tightAR.SetActive(active);
      tightARIso.SetActive(active);
      goodOQ.SetActive(active);
      Ethad.SetActive(active);
      Ethad1.SetActive(active);
      f1.SetActive(active);
      f1core.SetActive(active);
      Emins1.SetActive(active);
      fside.SetActive(active);
      Emax2.SetActive(active);
      ws3.SetActive(active);
      wstot.SetActive(active);
      E132.SetActive(active);
      E1152.SetActive(active);
      emaxs1.SetActive(active);
      E233.SetActive(active);
      E237.SetActive(active);
      E277.SetActive(active);
      weta2.SetActive(active);
      rphiallcalo.SetActive(active);
      Etcone45.SetActive(active);
      Etcone15.SetActive(active);
      Etcone20.SetActive(active);
      Etcone25.SetActive(active);
      Etcone30.SetActive(active);
      Etcone35.SetActive(active);
      Etcone40.SetActive(active);
      ptcone20.SetActive(active);
      ptcone30.SetActive(active);
      ptcone40.SetActive(active);
      nucone20.SetActive(active);
      nucone30.SetActive(active);
      nucone40.SetActive(active);
      Etcone15_pt_corrected.SetActive(active);
      Etcone20_pt_corrected.SetActive(active);
      Etcone25_pt_corrected.SetActive(active);
      Etcone30_pt_corrected.SetActive(active);
      Etcone35_pt_corrected.SetActive(active);
      Etcone40_pt_corrected.SetActive(active);
      zvertex.SetActive(active);
      errz.SetActive(active);
      etap.SetActive(active);
      depth.SetActive(active);
      cl_E.SetActive(active);
      cl_pt.SetActive(active);
      cl_eta.SetActive(active);
      cl_phi.SetActive(active);
      Es0.SetActive(active);
      etas0.SetActive(active);
      phis0.SetActive(active);
      Es1.SetActive(active);
      etas1.SetActive(active);
      phis1.SetActive(active);
      Es2.SetActive(active);
      etas2.SetActive(active);
      phis2.SetActive(active);
      Es3.SetActive(active);
      etas3.SetActive(active);
      phis3.SetActive(active);
      truth_isConv.SetActive(active);
      truth_isBrem.SetActive(active);
      truth_isFromHardProc.SetActive(active);
      truth_isPhotonFromHardProc.SetActive(active);
      convIP.SetActive(active);
      convIPRev.SetActive(active);
      ptIsolationCone.SetActive(active);
      ptIsolationConePhAngle.SetActive(active);
      Etcone40_ED_corrected.SetActive(active);
      Etcone40_corrected.SetActive(active);
      MET_n.SetActive(active);
      MET_wpx.SetActive(active);
      MET_wpy.SetActive(active);
      MET_wet.SetActive(active);
      MET_statusWord.SetActive(active);
      MET_BDTMedium_n.SetActive(active);
      MET_BDTMedium_wpx.SetActive(active);
      MET_BDTMedium_wpy.SetActive(active);
      MET_BDTMedium_wet.SetActive(active);
      MET_BDTMedium_statusWord.SetActive(active);
      MET_em_tightpp_n.SetActive(active);
      MET_em_tightpp_wpx.SetActive(active);
      MET_em_tightpp_wpy.SetActive(active);
      MET_em_tightpp_wet.SetActive(active);
      MET_em_tightpp_statusWord.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void PhotonD3PDCollection::ReadAllActive()
  {
    if(n.IsActive()) n();
    if(E.IsActive()) E();
    if(Et.IsActive()) Et();
    if(pt.IsActive()) pt();
    if(m.IsActive()) m();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(px.IsActive()) px();
    if(py.IsActive()) py();
    if(pz.IsActive()) pz();
    if(author.IsActive()) author();
    if(isRecovered.IsActive()) isRecovered();
    if(isEM.IsActive()) isEM();
    if(isEMLoose.IsActive()) isEMLoose();
    if(isEMMedium.IsActive()) isEMMedium();
    if(isEMTight.IsActive()) isEMTight();
    if(OQ.IsActive()) OQ();
    if(OQRecalc.IsActive()) OQRecalc();
    if(convFlag.IsActive()) convFlag();
    if(isConv.IsActive()) isConv();
    if(nConv.IsActive()) nConv();
    if(nSingleTrackConv.IsActive()) nSingleTrackConv();
    if(nDoubleTrackConv.IsActive()) nDoubleTrackConv();
    if(truth_deltaRRecPhoton.IsActive()) truth_deltaRRecPhoton();
    if(truth_E.IsActive()) truth_E();
    if(truth_pt.IsActive()) truth_pt();
    if(truth_eta.IsActive()) truth_eta();
    if(truth_phi.IsActive()) truth_phi();
    if(truth_type.IsActive()) truth_type();
    if(truth_status.IsActive()) truth_status();
    if(truth_barcode.IsActive()) truth_barcode();
    if(truth_mothertype.IsActive()) truth_mothertype();
    if(truth_motherbarcode.IsActive()) truth_motherbarcode();
    if(truth_index.IsActive()) truth_index();
    if(truth_matched.IsActive()) truth_matched();
    if(loose.IsActive()) loose();
    if(looseIso.IsActive()) looseIso();
    if(tight.IsActive()) tight();
    if(tightIso.IsActive()) tightIso();
    if(looseAR.IsActive()) looseAR();
    if(looseARIso.IsActive()) looseARIso();
    if(tightAR.IsActive()) tightAR();
    if(tightARIso.IsActive()) tightARIso();
    if(goodOQ.IsActive()) goodOQ();
    if(Ethad.IsActive()) Ethad();
    if(Ethad1.IsActive()) Ethad1();
    if(f1.IsActive()) f1();
    if(f1core.IsActive()) f1core();
    if(Emins1.IsActive()) Emins1();
    if(fside.IsActive()) fside();
    if(Emax2.IsActive()) Emax2();
    if(ws3.IsActive()) ws3();
    if(wstot.IsActive()) wstot();
    if(E132.IsActive()) E132();
    if(E1152.IsActive()) E1152();
    if(emaxs1.IsActive()) emaxs1();
    if(E233.IsActive()) E233();
    if(E237.IsActive()) E237();
    if(E277.IsActive()) E277();
    if(weta2.IsActive()) weta2();
    if(rphiallcalo.IsActive()) rphiallcalo();
    if(Etcone45.IsActive()) Etcone45();
    if(Etcone15.IsActive()) Etcone15();
    if(Etcone20.IsActive()) Etcone20();
    if(Etcone25.IsActive()) Etcone25();
    if(Etcone30.IsActive()) Etcone30();
    if(Etcone35.IsActive()) Etcone35();
    if(Etcone40.IsActive()) Etcone40();
    if(ptcone20.IsActive()) ptcone20();
    if(ptcone30.IsActive()) ptcone30();
    if(ptcone40.IsActive()) ptcone40();
    if(nucone20.IsActive()) nucone20();
    if(nucone30.IsActive()) nucone30();
    if(nucone40.IsActive()) nucone40();
    if(Etcone15_pt_corrected.IsActive()) Etcone15_pt_corrected();
    if(Etcone20_pt_corrected.IsActive()) Etcone20_pt_corrected();
    if(Etcone25_pt_corrected.IsActive()) Etcone25_pt_corrected();
    if(Etcone30_pt_corrected.IsActive()) Etcone30_pt_corrected();
    if(Etcone35_pt_corrected.IsActive()) Etcone35_pt_corrected();
    if(Etcone40_pt_corrected.IsActive()) Etcone40_pt_corrected();
    if(zvertex.IsActive()) zvertex();
    if(errz.IsActive()) errz();
    if(etap.IsActive()) etap();
    if(depth.IsActive()) depth();
    if(cl_E.IsActive()) cl_E();
    if(cl_pt.IsActive()) cl_pt();
    if(cl_eta.IsActive()) cl_eta();
    if(cl_phi.IsActive()) cl_phi();
    if(Es0.IsActive()) Es0();
    if(etas0.IsActive()) etas0();
    if(phis0.IsActive()) phis0();
    if(Es1.IsActive()) Es1();
    if(etas1.IsActive()) etas1();
    if(phis1.IsActive()) phis1();
    if(Es2.IsActive()) Es2();
    if(etas2.IsActive()) etas2();
    if(phis2.IsActive()) phis2();
    if(Es3.IsActive()) Es3();
    if(etas3.IsActive()) etas3();
    if(phis3.IsActive()) phis3();
    if(truth_isConv.IsActive()) truth_isConv();
    if(truth_isBrem.IsActive()) truth_isBrem();
    if(truth_isFromHardProc.IsActive()) truth_isFromHardProc();
    if(truth_isPhotonFromHardProc.IsActive()) truth_isPhotonFromHardProc();
    if(convIP.IsActive()) convIP();
    if(convIPRev.IsActive()) convIPRev();
    if(ptIsolationCone.IsActive()) ptIsolationCone();
    if(ptIsolationConePhAngle.IsActive()) ptIsolationConePhAngle();
    if(Etcone40_ED_corrected.IsActive()) Etcone40_ED_corrected();
    if(Etcone40_corrected.IsActive()) Etcone40_corrected();
    if(MET_n.IsActive()) MET_n();
    if(MET_wpx.IsActive()) MET_wpx();
    if(MET_wpy.IsActive()) MET_wpy();
    if(MET_wet.IsActive()) MET_wet();
    if(MET_statusWord.IsActive()) MET_statusWord();
    if(MET_BDTMedium_n.IsActive()) MET_BDTMedium_n();
    if(MET_BDTMedium_wpx.IsActive()) MET_BDTMedium_wpx();
    if(MET_BDTMedium_wpy.IsActive()) MET_BDTMedium_wpy();
    if(MET_BDTMedium_wet.IsActive()) MET_BDTMedium_wet();
    if(MET_BDTMedium_statusWord.IsActive()) MET_BDTMedium_statusWord();
    if(MET_em_tightpp_n.IsActive()) MET_em_tightpp_n();
    if(MET_em_tightpp_wpx.IsActive()) MET_em_tightpp_wpx();
    if(MET_em_tightpp_wpy.IsActive()) MET_em_tightpp_wpy();
    if(MET_em_tightpp_wet.IsActive()) MET_em_tightpp_wet();
    if(MET_em_tightpp_statusWord.IsActive()) MET_em_tightpp_statusWord();
  }

} // namespace D3PDReader
#endif // D3PDREADER_PhotonD3PDCollection_CXX
