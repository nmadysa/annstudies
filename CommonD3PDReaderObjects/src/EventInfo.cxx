// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_EventInfo_CXX
#define D3PDREADER_EventInfo_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "EventInfo.h"

ClassImp(D3PDReader::EventInfo)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  EventInfo::EventInfo(const long int& master,const std::string& prefix):
    TObject(),
    EF_2tau29T_medium1(prefix + "EF_2tau29T_medium1",&master),
    EF_2tau29_medium1(prefix + "EF_2tau29_medium1",&master),
    EF_2tau29i_medium1(prefix + "EF_2tau29i_medium1",&master),
    EF_2tau38T_medium1(prefix + "EF_2tau38T_medium1",&master),
    EF_j360_a4tchad(prefix + "EF_j360_a4tchad",&master),
    EF_j360_a4tclcw(prefix + "EF_j360_a4tclcw",&master),
    EF_j460_a4tchad(prefix + "EF_j460_a4tchad",&master),
    EF_tau125_medium1(prefix + "EF_tau125_medium1",&master),
    EF_tau20T_medium1(prefix + "EF_tau20T_medium1",&master),
    EF_tau20T_medium1_e15vh_medium1(prefix + "EF_tau20T_medium1_e15vh_medium1",&master),
    EF_tau20Ti_medium1(prefix + "EF_tau20Ti_medium1",&master),
    EF_tau20Ti_medium1_e18vh_medium1(prefix + "EF_tau20Ti_medium1_e18vh_medium1",&master),
    EF_tau20Ti_medium1_llh_e18vh_medium1(prefix + "EF_tau20Ti_medium1_llh_e18vh_medium1",&master),
    EF_tau20Ti_medium_e18vh_medium1(prefix + "EF_tau20Ti_medium_e18vh_medium1",&master),
    EF_tau20_medium1(prefix + "EF_tau20_medium1",&master),
    EF_tau29T_medium1(prefix + "EF_tau29T_medium1",&master),
    EF_tau29T_medium1_tau20T_medium1(prefix + "EF_tau29T_medium1_tau20T_medium1",&master),
    EF_tau29Ti_medium1(prefix + "EF_tau29Ti_medium1",&master),
    EF_tau29Ti_medium1_tau20Ti_medium1(prefix + "EF_tau29Ti_medium1_tau20Ti_medium1",&master),
    EF_tau29_medium1(prefix + "EF_tau29_medium1",&master),
    EF_tau29i_medium1(prefix + "EF_tau29i_medium1",&master),
    EF_tau38T_medium1(prefix + "EF_tau38T_medium1",&master),
    EF_tau38T_medium1_e18vh_medium1(prefix + "EF_tau38T_medium1_e18vh_medium1",&master),
    EF_tau38T_medium1_llh_e18vh_medium1(prefix + "EF_tau38T_medium1_llh_e18vh_medium1",&master),
    EF_tau38T_medium_e18vh_medium1(prefix + "EF_tau38T_medium_e18vh_medium1",&master),
    EF_tau50_medium1_e18vh_medium1(prefix + "EF_tau50_medium1_e18vh_medium1",&master),
    EF_tau50_medium_e15vh_medium1(prefix + "EF_tau50_medium_e15vh_medium1",&master),
    mcevt_weight(prefix + "mcevt_weight",&master),
    Eventshape_rhoKt3EM(prefix + "Eventshape_rhoKt3EM",&master),
    Eventshape_rhoKt4EM(prefix + "Eventshape_rhoKt4EM",&master),
    Eventshape_rhoKt3LC(prefix + "Eventshape_rhoKt3LC",&master),
    Eventshape_rhoKt4LC(prefix + "Eventshape_rhoKt4LC",&master),
    RunNumber(prefix + "RunNumber",&master),
    EventNumber(prefix + "EventNumber",&master),
    lbn(prefix + "lbn",&master),
    actualIntPerXing(prefix + "actualIntPerXing",&master),
    averageIntPerXing(prefix + "averageIntPerXing",&master),
    mc_channel_number(prefix + "mc_channel_number",&master),
    coreFlags(prefix + "coreFlags",&master),
    larError(prefix + "larError",&master),
    tileError(prefix + "tileError",&master),
    trig_L2_passedPhysics(prefix + "trig_L2_passedPhysics",&master),
    trig_EF_passedPhysics(prefix + "trig_EF_passedPhysics",&master),
    trig_L2_passedRaw(prefix + "trig_L2_passedRaw",&master),
    trig_EF_passedRaw(prefix + "trig_EF_passedRaw",&master),
    trig_L2_resurrected(prefix + "trig_L2_resurrected",&master),
    trig_EF_resurrected(prefix + "trig_EF_resurrected",&master),
    trig_L2_passedThrough(prefix + "trig_L2_passedThrough",&master),
    trig_EF_passedThrough(prefix + "trig_EF_passedThrough",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  EventInfo::EventInfo(const std::string& prefix):
    TObject(),
    EF_2tau29T_medium1(prefix + "EF_2tau29T_medium1",0),
    EF_2tau29_medium1(prefix + "EF_2tau29_medium1",0),
    EF_2tau29i_medium1(prefix + "EF_2tau29i_medium1",0),
    EF_2tau38T_medium1(prefix + "EF_2tau38T_medium1",0),
    EF_j360_a4tchad(prefix + "EF_j360_a4tchad",0),
    EF_j360_a4tclcw(prefix + "EF_j360_a4tclcw",0),
    EF_j460_a4tchad(prefix + "EF_j460_a4tchad",0),
    EF_tau125_medium1(prefix + "EF_tau125_medium1",0),
    EF_tau20T_medium1(prefix + "EF_tau20T_medium1",0),
    EF_tau20T_medium1_e15vh_medium1(prefix + "EF_tau20T_medium1_e15vh_medium1",0),
    EF_tau20Ti_medium1(prefix + "EF_tau20Ti_medium1",0),
    EF_tau20Ti_medium1_e18vh_medium1(prefix + "EF_tau20Ti_medium1_e18vh_medium1",0),
    EF_tau20Ti_medium1_llh_e18vh_medium1(prefix + "EF_tau20Ti_medium1_llh_e18vh_medium1",0),
    EF_tau20Ti_medium_e18vh_medium1(prefix + "EF_tau20Ti_medium_e18vh_medium1",0),
    EF_tau20_medium1(prefix + "EF_tau20_medium1",0),
    EF_tau29T_medium1(prefix + "EF_tau29T_medium1",0),
    EF_tau29T_medium1_tau20T_medium1(prefix + "EF_tau29T_medium1_tau20T_medium1",0),
    EF_tau29Ti_medium1(prefix + "EF_tau29Ti_medium1",0),
    EF_tau29Ti_medium1_tau20Ti_medium1(prefix + "EF_tau29Ti_medium1_tau20Ti_medium1",0),
    EF_tau29_medium1(prefix + "EF_tau29_medium1",0),
    EF_tau29i_medium1(prefix + "EF_tau29i_medium1",0),
    EF_tau38T_medium1(prefix + "EF_tau38T_medium1",0),
    EF_tau38T_medium1_e18vh_medium1(prefix + "EF_tau38T_medium1_e18vh_medium1",0),
    EF_tau38T_medium1_llh_e18vh_medium1(prefix + "EF_tau38T_medium1_llh_e18vh_medium1",0),
    EF_tau38T_medium_e18vh_medium1(prefix + "EF_tau38T_medium_e18vh_medium1",0),
    EF_tau50_medium1_e18vh_medium1(prefix + "EF_tau50_medium1_e18vh_medium1",0),
    EF_tau50_medium_e15vh_medium1(prefix + "EF_tau50_medium_e15vh_medium1",0),
    mcevt_weight(prefix + "mcevt_weight",0),
    Eventshape_rhoKt3EM(prefix + "Eventshape_rhoKt3EM",0),
    Eventshape_rhoKt4EM(prefix + "Eventshape_rhoKt4EM",0),
    Eventshape_rhoKt3LC(prefix + "Eventshape_rhoKt3LC",0),
    Eventshape_rhoKt4LC(prefix + "Eventshape_rhoKt4LC",0),
    RunNumber(prefix + "RunNumber",0),
    EventNumber(prefix + "EventNumber",0),
    lbn(prefix + "lbn",0),
    actualIntPerXing(prefix + "actualIntPerXing",0),
    averageIntPerXing(prefix + "averageIntPerXing",0),
    mc_channel_number(prefix + "mc_channel_number",0),
    coreFlags(prefix + "coreFlags",0),
    larError(prefix + "larError",0),
    tileError(prefix + "tileError",0),
    trig_L2_passedPhysics(prefix + "trig_L2_passedPhysics",0),
    trig_EF_passedPhysics(prefix + "trig_EF_passedPhysics",0),
    trig_L2_passedRaw(prefix + "trig_L2_passedRaw",0),
    trig_EF_passedRaw(prefix + "trig_EF_passedRaw",0),
    trig_L2_resurrected(prefix + "trig_L2_resurrected",0),
    trig_EF_resurrected(prefix + "trig_EF_resurrected",0),
    trig_L2_passedThrough(prefix + "trig_L2_passedThrough",0),
    trig_EF_passedThrough(prefix + "trig_EF_passedThrough",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void EventInfo::ReadFrom(TTree* tree)
  {
    EF_2tau29T_medium1.ReadFrom(tree);
    EF_2tau29_medium1.ReadFrom(tree);
    EF_2tau29i_medium1.ReadFrom(tree);
    EF_2tau38T_medium1.ReadFrom(tree);
    EF_j360_a4tchad.ReadFrom(tree);
    EF_j360_a4tclcw.ReadFrom(tree);
    EF_j460_a4tchad.ReadFrom(tree);
    EF_tau125_medium1.ReadFrom(tree);
    EF_tau20T_medium1.ReadFrom(tree);
    EF_tau20T_medium1_e15vh_medium1.ReadFrom(tree);
    EF_tau20Ti_medium1.ReadFrom(tree);
    EF_tau20Ti_medium1_e18vh_medium1.ReadFrom(tree);
    EF_tau20Ti_medium1_llh_e18vh_medium1.ReadFrom(tree);
    EF_tau20Ti_medium_e18vh_medium1.ReadFrom(tree);
    EF_tau20_medium1.ReadFrom(tree);
    EF_tau29T_medium1.ReadFrom(tree);
    EF_tau29T_medium1_tau20T_medium1.ReadFrom(tree);
    EF_tau29Ti_medium1.ReadFrom(tree);
    EF_tau29Ti_medium1_tau20Ti_medium1.ReadFrom(tree);
    EF_tau29_medium1.ReadFrom(tree);
    EF_tau29i_medium1.ReadFrom(tree);
    EF_tau38T_medium1.ReadFrom(tree);
    EF_tau38T_medium1_e18vh_medium1.ReadFrom(tree);
    EF_tau38T_medium1_llh_e18vh_medium1.ReadFrom(tree);
    EF_tau38T_medium_e18vh_medium1.ReadFrom(tree);
    EF_tau50_medium1_e18vh_medium1.ReadFrom(tree);
    EF_tau50_medium_e15vh_medium1.ReadFrom(tree);
    mcevt_weight.ReadFrom(tree);
    Eventshape_rhoKt3EM.ReadFrom(tree);
    Eventshape_rhoKt4EM.ReadFrom(tree);
    Eventshape_rhoKt3LC.ReadFrom(tree);
    Eventshape_rhoKt4LC.ReadFrom(tree);
    RunNumber.ReadFrom(tree);
    EventNumber.ReadFrom(tree);
    lbn.ReadFrom(tree);
    actualIntPerXing.ReadFrom(tree);
    averageIntPerXing.ReadFrom(tree);
    mc_channel_number.ReadFrom(tree);
    coreFlags.ReadFrom(tree);
    larError.ReadFrom(tree);
    tileError.ReadFrom(tree);
    trig_L2_passedPhysics.ReadFrom(tree);
    trig_EF_passedPhysics.ReadFrom(tree);
    trig_L2_passedRaw.ReadFrom(tree);
    trig_EF_passedRaw.ReadFrom(tree);
    trig_L2_resurrected.ReadFrom(tree);
    trig_EF_resurrected.ReadFrom(tree);
    trig_L2_passedThrough.ReadFrom(tree);
    trig_EF_passedThrough.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void EventInfo::WriteTo(TTree* tree)
  {
    EF_2tau29T_medium1.WriteTo(tree);
    EF_2tau29_medium1.WriteTo(tree);
    EF_2tau29i_medium1.WriteTo(tree);
    EF_2tau38T_medium1.WriteTo(tree);
    EF_j360_a4tchad.WriteTo(tree);
    EF_j360_a4tclcw.WriteTo(tree);
    EF_j460_a4tchad.WriteTo(tree);
    EF_tau125_medium1.WriteTo(tree);
    EF_tau20T_medium1.WriteTo(tree);
    EF_tau20T_medium1_e15vh_medium1.WriteTo(tree);
    EF_tau20Ti_medium1.WriteTo(tree);
    EF_tau20Ti_medium1_e18vh_medium1.WriteTo(tree);
    EF_tau20Ti_medium1_llh_e18vh_medium1.WriteTo(tree);
    EF_tau20Ti_medium_e18vh_medium1.WriteTo(tree);
    EF_tau20_medium1.WriteTo(tree);
    EF_tau29T_medium1.WriteTo(tree);
    EF_tau29T_medium1_tau20T_medium1.WriteTo(tree);
    EF_tau29Ti_medium1.WriteTo(tree);
    EF_tau29Ti_medium1_tau20Ti_medium1.WriteTo(tree);
    EF_tau29_medium1.WriteTo(tree);
    EF_tau29i_medium1.WriteTo(tree);
    EF_tau38T_medium1.WriteTo(tree);
    EF_tau38T_medium1_e18vh_medium1.WriteTo(tree);
    EF_tau38T_medium1_llh_e18vh_medium1.WriteTo(tree);
    EF_tau38T_medium_e18vh_medium1.WriteTo(tree);
    EF_tau50_medium1_e18vh_medium1.WriteTo(tree);
    EF_tau50_medium_e15vh_medium1.WriteTo(tree);
    mcevt_weight.WriteTo(tree);
    Eventshape_rhoKt3EM.WriteTo(tree);
    Eventshape_rhoKt4EM.WriteTo(tree);
    Eventshape_rhoKt3LC.WriteTo(tree);
    Eventshape_rhoKt4LC.WriteTo(tree);
    RunNumber.WriteTo(tree);
    EventNumber.WriteTo(tree);
    lbn.WriteTo(tree);
    actualIntPerXing.WriteTo(tree);
    averageIntPerXing.WriteTo(tree);
    mc_channel_number.WriteTo(tree);
    coreFlags.WriteTo(tree);
    larError.WriteTo(tree);
    tileError.WriteTo(tree);
    trig_L2_passedPhysics.WriteTo(tree);
    trig_EF_passedPhysics.WriteTo(tree);
    trig_L2_passedRaw.WriteTo(tree);
    trig_EF_passedRaw.WriteTo(tree);
    trig_L2_resurrected.WriteTo(tree);
    trig_EF_resurrected.WriteTo(tree);
    trig_L2_passedThrough.WriteTo(tree);
    trig_EF_passedThrough.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void EventInfo::SetActive(bool active)
  {
    if(active)
    {
     if(EF_2tau29T_medium1.IsAvailable()) EF_2tau29T_medium1.SetActive(active);
     if(EF_2tau29_medium1.IsAvailable()) EF_2tau29_medium1.SetActive(active);
     if(EF_2tau29i_medium1.IsAvailable()) EF_2tau29i_medium1.SetActive(active);
     if(EF_2tau38T_medium1.IsAvailable()) EF_2tau38T_medium1.SetActive(active);
     if(EF_j360_a4tchad.IsAvailable()) EF_j360_a4tchad.SetActive(active);
     if(EF_j360_a4tclcw.IsAvailable()) EF_j360_a4tclcw.SetActive(active);
     if(EF_j460_a4tchad.IsAvailable()) EF_j460_a4tchad.SetActive(active);
     if(EF_tau125_medium1.IsAvailable()) EF_tau125_medium1.SetActive(active);
     if(EF_tau20T_medium1.IsAvailable()) EF_tau20T_medium1.SetActive(active);
     if(EF_tau20T_medium1_e15vh_medium1.IsAvailable()) EF_tau20T_medium1_e15vh_medium1.SetActive(active);
     if(EF_tau20Ti_medium1.IsAvailable()) EF_tau20Ti_medium1.SetActive(active);
     if(EF_tau20Ti_medium1_e18vh_medium1.IsAvailable()) EF_tau20Ti_medium1_e18vh_medium1.SetActive(active);
     if(EF_tau20Ti_medium1_llh_e18vh_medium1.IsAvailable()) EF_tau20Ti_medium1_llh_e18vh_medium1.SetActive(active);
     if(EF_tau20Ti_medium_e18vh_medium1.IsAvailable()) EF_tau20Ti_medium_e18vh_medium1.SetActive(active);
     if(EF_tau20_medium1.IsAvailable()) EF_tau20_medium1.SetActive(active);
     if(EF_tau29T_medium1.IsAvailable()) EF_tau29T_medium1.SetActive(active);
     if(EF_tau29T_medium1_tau20T_medium1.IsAvailable()) EF_tau29T_medium1_tau20T_medium1.SetActive(active);
     if(EF_tau29Ti_medium1.IsAvailable()) EF_tau29Ti_medium1.SetActive(active);
     if(EF_tau29Ti_medium1_tau20Ti_medium1.IsAvailable()) EF_tau29Ti_medium1_tau20Ti_medium1.SetActive(active);
     if(EF_tau29_medium1.IsAvailable()) EF_tau29_medium1.SetActive(active);
     if(EF_tau29i_medium1.IsAvailable()) EF_tau29i_medium1.SetActive(active);
     if(EF_tau38T_medium1.IsAvailable()) EF_tau38T_medium1.SetActive(active);
     if(EF_tau38T_medium1_e18vh_medium1.IsAvailable()) EF_tau38T_medium1_e18vh_medium1.SetActive(active);
     if(EF_tau38T_medium1_llh_e18vh_medium1.IsAvailable()) EF_tau38T_medium1_llh_e18vh_medium1.SetActive(active);
     if(EF_tau38T_medium_e18vh_medium1.IsAvailable()) EF_tau38T_medium_e18vh_medium1.SetActive(active);
     if(EF_tau50_medium1_e18vh_medium1.IsAvailable()) EF_tau50_medium1_e18vh_medium1.SetActive(active);
     if(EF_tau50_medium_e15vh_medium1.IsAvailable()) EF_tau50_medium_e15vh_medium1.SetActive(active);
     if(mcevt_weight.IsAvailable()) mcevt_weight.SetActive(active);
     if(Eventshape_rhoKt3EM.IsAvailable()) Eventshape_rhoKt3EM.SetActive(active);
     if(Eventshape_rhoKt4EM.IsAvailable()) Eventshape_rhoKt4EM.SetActive(active);
     if(Eventshape_rhoKt3LC.IsAvailable()) Eventshape_rhoKt3LC.SetActive(active);
     if(Eventshape_rhoKt4LC.IsAvailable()) Eventshape_rhoKt4LC.SetActive(active);
     if(RunNumber.IsAvailable()) RunNumber.SetActive(active);
     if(EventNumber.IsAvailable()) EventNumber.SetActive(active);
     if(lbn.IsAvailable()) lbn.SetActive(active);
     if(actualIntPerXing.IsAvailable()) actualIntPerXing.SetActive(active);
     if(averageIntPerXing.IsAvailable()) averageIntPerXing.SetActive(active);
     if(mc_channel_number.IsAvailable()) mc_channel_number.SetActive(active);
     if(coreFlags.IsAvailable()) coreFlags.SetActive(active);
     if(larError.IsAvailable()) larError.SetActive(active);
     if(tileError.IsAvailable()) tileError.SetActive(active);
     if(trig_L2_passedPhysics.IsAvailable()) trig_L2_passedPhysics.SetActive(active);
     if(trig_EF_passedPhysics.IsAvailable()) trig_EF_passedPhysics.SetActive(active);
     if(trig_L2_passedRaw.IsAvailable()) trig_L2_passedRaw.SetActive(active);
     if(trig_EF_passedRaw.IsAvailable()) trig_EF_passedRaw.SetActive(active);
     if(trig_L2_resurrected.IsAvailable()) trig_L2_resurrected.SetActive(active);
     if(trig_EF_resurrected.IsAvailable()) trig_EF_resurrected.SetActive(active);
     if(trig_L2_passedThrough.IsAvailable()) trig_L2_passedThrough.SetActive(active);
     if(trig_EF_passedThrough.IsAvailable()) trig_EF_passedThrough.SetActive(active);
    }
    else
    {
      EF_2tau29T_medium1.SetActive(active);
      EF_2tau29_medium1.SetActive(active);
      EF_2tau29i_medium1.SetActive(active);
      EF_2tau38T_medium1.SetActive(active);
      EF_j360_a4tchad.SetActive(active);
      EF_j360_a4tclcw.SetActive(active);
      EF_j460_a4tchad.SetActive(active);
      EF_tau125_medium1.SetActive(active);
      EF_tau20T_medium1.SetActive(active);
      EF_tau20T_medium1_e15vh_medium1.SetActive(active);
      EF_tau20Ti_medium1.SetActive(active);
      EF_tau20Ti_medium1_e18vh_medium1.SetActive(active);
      EF_tau20Ti_medium1_llh_e18vh_medium1.SetActive(active);
      EF_tau20Ti_medium_e18vh_medium1.SetActive(active);
      EF_tau20_medium1.SetActive(active);
      EF_tau29T_medium1.SetActive(active);
      EF_tau29T_medium1_tau20T_medium1.SetActive(active);
      EF_tau29Ti_medium1.SetActive(active);
      EF_tau29Ti_medium1_tau20Ti_medium1.SetActive(active);
      EF_tau29_medium1.SetActive(active);
      EF_tau29i_medium1.SetActive(active);
      EF_tau38T_medium1.SetActive(active);
      EF_tau38T_medium1_e18vh_medium1.SetActive(active);
      EF_tau38T_medium1_llh_e18vh_medium1.SetActive(active);
      EF_tau38T_medium_e18vh_medium1.SetActive(active);
      EF_tau50_medium1_e18vh_medium1.SetActive(active);
      EF_tau50_medium_e15vh_medium1.SetActive(active);
      mcevt_weight.SetActive(active);
      Eventshape_rhoKt3EM.SetActive(active);
      Eventshape_rhoKt4EM.SetActive(active);
      Eventshape_rhoKt3LC.SetActive(active);
      Eventshape_rhoKt4LC.SetActive(active);
      RunNumber.SetActive(active);
      EventNumber.SetActive(active);
      lbn.SetActive(active);
      actualIntPerXing.SetActive(active);
      averageIntPerXing.SetActive(active);
      mc_channel_number.SetActive(active);
      coreFlags.SetActive(active);
      larError.SetActive(active);
      tileError.SetActive(active);
      trig_L2_passedPhysics.SetActive(active);
      trig_EF_passedPhysics.SetActive(active);
      trig_L2_passedRaw.SetActive(active);
      trig_EF_passedRaw.SetActive(active);
      trig_L2_resurrected.SetActive(active);
      trig_EF_resurrected.SetActive(active);
      trig_L2_passedThrough.SetActive(active);
      trig_EF_passedThrough.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void EventInfo::ReadAllActive()
  {
    if(EF_2tau29T_medium1.IsActive()) EF_2tau29T_medium1();
    if(EF_2tau29_medium1.IsActive()) EF_2tau29_medium1();
    if(EF_2tau29i_medium1.IsActive()) EF_2tau29i_medium1();
    if(EF_2tau38T_medium1.IsActive()) EF_2tau38T_medium1();
    if(EF_j360_a4tchad.IsActive()) EF_j360_a4tchad();
    if(EF_j360_a4tclcw.IsActive()) EF_j360_a4tclcw();
    if(EF_j460_a4tchad.IsActive()) EF_j460_a4tchad();
    if(EF_tau125_medium1.IsActive()) EF_tau125_medium1();
    if(EF_tau20T_medium1.IsActive()) EF_tau20T_medium1();
    if(EF_tau20T_medium1_e15vh_medium1.IsActive()) EF_tau20T_medium1_e15vh_medium1();
    if(EF_tau20Ti_medium1.IsActive()) EF_tau20Ti_medium1();
    if(EF_tau20Ti_medium1_e18vh_medium1.IsActive()) EF_tau20Ti_medium1_e18vh_medium1();
    if(EF_tau20Ti_medium1_llh_e18vh_medium1.IsActive()) EF_tau20Ti_medium1_llh_e18vh_medium1();
    if(EF_tau20Ti_medium_e18vh_medium1.IsActive()) EF_tau20Ti_medium_e18vh_medium1();
    if(EF_tau20_medium1.IsActive()) EF_tau20_medium1();
    if(EF_tau29T_medium1.IsActive()) EF_tau29T_medium1();
    if(EF_tau29T_medium1_tau20T_medium1.IsActive()) EF_tau29T_medium1_tau20T_medium1();
    if(EF_tau29Ti_medium1.IsActive()) EF_tau29Ti_medium1();
    if(EF_tau29Ti_medium1_tau20Ti_medium1.IsActive()) EF_tau29Ti_medium1_tau20Ti_medium1();
    if(EF_tau29_medium1.IsActive()) EF_tau29_medium1();
    if(EF_tau29i_medium1.IsActive()) EF_tau29i_medium1();
    if(EF_tau38T_medium1.IsActive()) EF_tau38T_medium1();
    if(EF_tau38T_medium1_e18vh_medium1.IsActive()) EF_tau38T_medium1_e18vh_medium1();
    if(EF_tau38T_medium1_llh_e18vh_medium1.IsActive()) EF_tau38T_medium1_llh_e18vh_medium1();
    if(EF_tau38T_medium_e18vh_medium1.IsActive()) EF_tau38T_medium_e18vh_medium1();
    if(EF_tau50_medium1_e18vh_medium1.IsActive()) EF_tau50_medium1_e18vh_medium1();
    if(EF_tau50_medium_e15vh_medium1.IsActive()) EF_tau50_medium_e15vh_medium1();
    if(mcevt_weight.IsActive()) mcevt_weight();
    if(Eventshape_rhoKt3EM.IsActive()) Eventshape_rhoKt3EM();
    if(Eventshape_rhoKt4EM.IsActive()) Eventshape_rhoKt4EM();
    if(Eventshape_rhoKt3LC.IsActive()) Eventshape_rhoKt3LC();
    if(Eventshape_rhoKt4LC.IsActive()) Eventshape_rhoKt4LC();
    if(RunNumber.IsActive()) RunNumber();
    if(EventNumber.IsActive()) EventNumber();
    if(lbn.IsActive()) lbn();
    if(actualIntPerXing.IsActive()) actualIntPerXing();
    if(averageIntPerXing.IsActive()) averageIntPerXing();
    if(mc_channel_number.IsActive()) mc_channel_number();
    if(coreFlags.IsActive()) coreFlags();
    if(larError.IsActive()) larError();
    if(tileError.IsActive()) tileError();
    if(trig_L2_passedPhysics.IsActive()) trig_L2_passedPhysics();
    if(trig_EF_passedPhysics.IsActive()) trig_EF_passedPhysics();
    if(trig_L2_passedRaw.IsActive()) trig_L2_passedRaw();
    if(trig_EF_passedRaw.IsActive()) trig_EF_passedRaw();
    if(trig_L2_resurrected.IsActive()) trig_L2_resurrected();
    if(trig_EF_resurrected.IsActive()) trig_EF_resurrected();
    if(trig_L2_passedThrough.IsActive()) trig_L2_passedThrough();
    if(trig_EF_passedThrough.IsActive()) trig_EF_passedThrough();
  }

} // namespace D3PDReader
#endif // D3PDREADER_EventInfo_CXX
