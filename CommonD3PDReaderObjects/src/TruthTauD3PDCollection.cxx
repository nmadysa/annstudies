// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TruthTauD3PDCollection_CXX
#define D3PDREADER_TruthTauD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TruthTauD3PDCollection.h"

ClassImp(D3PDReader::TruthTauD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TruthTauD3PDCollection::TruthTauD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    n(prefix + "n",&master),
    pt(prefix + "pt",&master),
    m(prefix + "m",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    vis_m(prefix + "vis_m",&master),
    vis_Et(prefix + "vis_Et",&master),
    vis_eta(prefix + "vis_eta",&master),
    vis_phi(prefix + "vis_phi",&master),
    nProng(prefix + "nProng",&master),
    nPi0(prefix + "nPi0",&master),
    charge(prefix + "charge",&master),
    tauAssoc_dr(prefix + "tauAssoc_dr",&master),
    tauAssoc_index(prefix + "tauAssoc_index",&master),
    tauAssoc_matched(prefix + "tauAssoc_matched",&master),
    truthAssoc_index(prefix + "truthAssoc_index",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TruthTauD3PDCollection::TruthTauD3PDCollection(const std::string& prefix):
    TObject(),
    n(prefix + "n",0),
    pt(prefix + "pt",0),
    m(prefix + "m",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    vis_m(prefix + "vis_m",0),
    vis_Et(prefix + "vis_Et",0),
    vis_eta(prefix + "vis_eta",0),
    vis_phi(prefix + "vis_phi",0),
    nProng(prefix + "nProng",0),
    nPi0(prefix + "nPi0",0),
    charge(prefix + "charge",0),
    tauAssoc_dr(prefix + "tauAssoc_dr",0),
    tauAssoc_index(prefix + "tauAssoc_index",0),
    tauAssoc_matched(prefix + "tauAssoc_matched",0),
    truthAssoc_index(prefix + "truthAssoc_index",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TruthTauD3PDCollection::ReadFrom(TTree* tree)
  {
    n.ReadFrom(tree);
    pt.ReadFrom(tree);
    m.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    vis_m.ReadFrom(tree);
    vis_Et.ReadFrom(tree);
    vis_eta.ReadFrom(tree);
    vis_phi.ReadFrom(tree);
    nProng.ReadFrom(tree);
    nPi0.ReadFrom(tree);
    charge.ReadFrom(tree);
    tauAssoc_dr.ReadFrom(tree);
    tauAssoc_index.ReadFrom(tree);
    tauAssoc_matched.ReadFrom(tree);
    truthAssoc_index.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TruthTauD3PDCollection::WriteTo(TTree* tree)
  {
    n.WriteTo(tree);
    pt.WriteTo(tree);
    m.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    vis_m.WriteTo(tree);
    vis_Et.WriteTo(tree);
    vis_eta.WriteTo(tree);
    vis_phi.WriteTo(tree);
    nProng.WriteTo(tree);
    nPi0.WriteTo(tree);
    charge.WriteTo(tree);
    tauAssoc_dr.WriteTo(tree);
    tauAssoc_index.WriteTo(tree);
    tauAssoc_matched.WriteTo(tree);
    truthAssoc_index.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TruthTauD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(n.IsAvailable()) n.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(m.IsAvailable()) m.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(vis_m.IsAvailable()) vis_m.SetActive(active);
     if(vis_Et.IsAvailable()) vis_Et.SetActive(active);
     if(vis_eta.IsAvailable()) vis_eta.SetActive(active);
     if(vis_phi.IsAvailable()) vis_phi.SetActive(active);
     if(nProng.IsAvailable()) nProng.SetActive(active);
     if(nPi0.IsAvailable()) nPi0.SetActive(active);
     if(charge.IsAvailable()) charge.SetActive(active);
     if(tauAssoc_dr.IsAvailable()) tauAssoc_dr.SetActive(active);
     if(tauAssoc_index.IsAvailable()) tauAssoc_index.SetActive(active);
     if(tauAssoc_matched.IsAvailable()) tauAssoc_matched.SetActive(active);
     if(truthAssoc_index.IsAvailable()) truthAssoc_index.SetActive(active);
    }
    else
    {
      n.SetActive(active);
      pt.SetActive(active);
      m.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      vis_m.SetActive(active);
      vis_Et.SetActive(active);
      vis_eta.SetActive(active);
      vis_phi.SetActive(active);
      nProng.SetActive(active);
      nPi0.SetActive(active);
      charge.SetActive(active);
      tauAssoc_dr.SetActive(active);
      tauAssoc_index.SetActive(active);
      tauAssoc_matched.SetActive(active);
      truthAssoc_index.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TruthTauD3PDCollection::ReadAllActive()
  {
    if(n.IsActive()) n();
    if(pt.IsActive()) pt();
    if(m.IsActive()) m();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(vis_m.IsActive()) vis_m();
    if(vis_Et.IsActive()) vis_Et();
    if(vis_eta.IsActive()) vis_eta();
    if(vis_phi.IsActive()) vis_phi();
    if(nProng.IsActive()) nProng();
    if(nPi0.IsActive()) nPi0();
    if(charge.IsActive()) charge();
    if(tauAssoc_dr.IsActive()) tauAssoc_dr();
    if(tauAssoc_index.IsActive()) tauAssoc_index();
    if(tauAssoc_matched.IsActive()) tauAssoc_matched();
    if(truthAssoc_index.IsActive()) truthAssoc_index();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TruthTauD3PDCollection_CXX
