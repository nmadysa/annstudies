// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_ElectronD3PDCollection_CXX
#define D3PDREADER_ElectronD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "ElectronD3PDCollection.h"

ClassImp(D3PDReader::ElectronD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  ElectronD3PDCollection::ElectronD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    n(prefix + "n",&master),
    E(prefix + "E",&master),
    Et(prefix + "Et",&master),
    pt(prefix + "pt",&master),
    m(prefix + "m",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    px(prefix + "px",&master),
    py(prefix + "py",&master),
    pz(prefix + "pz",&master),
    charge(prefix + "charge",&master),
    author(prefix + "author",&master),
    isEM(prefix + "isEM",&master),
    isEMLoose(prefix + "isEMLoose",&master),
    isEMMedium(prefix + "isEMMedium",&master),
    isEMTight(prefix + "isEMTight",&master),
    OQ(prefix + "OQ",&master),
    isConv(prefix + "isConv",&master),
    nConv(prefix + "nConv",&master),
    nSingleTrackConv(prefix + "nSingleTrackConv",&master),
    nDoubleTrackConv(prefix + "nDoubleTrackConv",&master),
    type(prefix + "type",&master),
    origin(prefix + "origin",&master),
    typebkg(prefix + "typebkg",&master),
    originbkg(prefix + "originbkg",&master),
    truth_E(prefix + "truth_E",&master),
    truth_pt(prefix + "truth_pt",&master),
    truth_eta(prefix + "truth_eta",&master),
    truth_phi(prefix + "truth_phi",&master),
    truth_type(prefix + "truth_type",&master),
    truth_status(prefix + "truth_status",&master),
    truth_barcode(prefix + "truth_barcode",&master),
    truth_mothertype(prefix + "truth_mothertype",&master),
    truth_motherbarcode(prefix + "truth_motherbarcode",&master),
    truth_hasHardBrem(prefix + "truth_hasHardBrem",&master),
    truth_index(prefix + "truth_index",&master),
    truth_matched(prefix + "truth_matched",&master),
    mediumWithoutTrack(prefix + "mediumWithoutTrack",&master),
    mediumIsoWithoutTrack(prefix + "mediumIsoWithoutTrack",&master),
    tightWithoutTrack(prefix + "tightWithoutTrack",&master),
    tightIsoWithoutTrack(prefix + "tightIsoWithoutTrack",&master),
    loose(prefix + "loose",&master),
    looseIso(prefix + "looseIso",&master),
    medium(prefix + "medium",&master),
    mediumIso(prefix + "mediumIso",&master),
    tight(prefix + "tight",&master),
    tightIso(prefix + "tightIso",&master),
    loosePP(prefix + "loosePP",&master),
    loosePPIso(prefix + "loosePPIso",&master),
    mediumPP(prefix + "mediumPP",&master),
    mediumPPIso(prefix + "mediumPPIso",&master),
    tightPP(prefix + "tightPP",&master),
    tightPPIso(prefix + "tightPPIso",&master),
    goodOQ(prefix + "goodOQ",&master),
    Ethad(prefix + "Ethad",&master),
    Ethad1(prefix + "Ethad1",&master),
    f1(prefix + "f1",&master),
    f1core(prefix + "f1core",&master),
    Emins1(prefix + "Emins1",&master),
    fside(prefix + "fside",&master),
    Emax2(prefix + "Emax2",&master),
    ws3(prefix + "ws3",&master),
    wstot(prefix + "wstot",&master),
    emaxs1(prefix + "emaxs1",&master),
    deltaEs(prefix + "deltaEs",&master),
    E233(prefix + "E233",&master),
    E237(prefix + "E237",&master),
    E277(prefix + "E277",&master),
    weta2(prefix + "weta2",&master),
    f3(prefix + "f3",&master),
    f3core(prefix + "f3core",&master),
    rphiallcalo(prefix + "rphiallcalo",&master),
    Etcone45(prefix + "Etcone45",&master),
    Etcone15(prefix + "Etcone15",&master),
    Etcone20(prefix + "Etcone20",&master),
    Etcone25(prefix + "Etcone25",&master),
    Etcone30(prefix + "Etcone30",&master),
    Etcone35(prefix + "Etcone35",&master),
    Etcone40(prefix + "Etcone40",&master),
    ptcone20(prefix + "ptcone20",&master),
    ptcone30(prefix + "ptcone30",&master),
    ptcone40(prefix + "ptcone40",&master),
    nucone20(prefix + "nucone20",&master),
    nucone30(prefix + "nucone30",&master),
    nucone40(prefix + "nucone40",&master),
    hasconv(prefix + "hasconv",&master),
    Rconv(prefix + "Rconv",&master),
    zconv(prefix + "zconv",&master),
    pt1conv(prefix + "pt1conv",&master),
    pt2conv(prefix + "pt2conv",&master),
    ptconv(prefix + "ptconv",&master),
    pzconv(prefix + "pzconv",&master),
    pos7(prefix + "pos7",&master),
    etacorrmag(prefix + "etacorrmag",&master),
    deltaeta1(prefix + "deltaeta1",&master),
    deltaeta2(prefix + "deltaeta2",&master),
    deltaphi2(prefix + "deltaphi2",&master),
    deltaphiRescaled(prefix + "deltaphiRescaled",&master),
    deltaPhiFromLast(prefix + "deltaPhiFromLast",&master),
    deltaPhiRot(prefix + "deltaPhiRot",&master),
    expectHitInBLayer(prefix + "expectHitInBLayer",&master),
    trackd0_physics(prefix + "trackd0_physics",&master),
    etaSampling1(prefix + "etaSampling1",&master),
    reta(prefix + "reta",&master),
    rphi(prefix + "rphi",&master),
    topoEtcone20(prefix + "topoEtcone20",&master),
    topoEtcone30(prefix + "topoEtcone30",&master),
    topoEtcone40(prefix + "topoEtcone40",&master),
    zvertex(prefix + "zvertex",&master),
    errz(prefix + "errz",&master),
    etap(prefix + "etap",&master),
    depth(prefix + "depth",&master),
    refittedTrack_n(prefix + "refittedTrack_n",&master),
    refittedTrack_author(prefix + "refittedTrack_author",&master),
    refittedTrack_chi2(prefix + "refittedTrack_chi2",&master),
    refittedTrack_hasBrem(prefix + "refittedTrack_hasBrem",&master),
    refittedTrack_bremRadius(prefix + "refittedTrack_bremRadius",&master),
    refittedTrack_bremZ(prefix + "refittedTrack_bremZ",&master),
    refittedTrack_bremRadiusErr(prefix + "refittedTrack_bremRadiusErr",&master),
    refittedTrack_bremZErr(prefix + "refittedTrack_bremZErr",&master),
    refittedTrack_bremFitStatus(prefix + "refittedTrack_bremFitStatus",&master),
    refittedTrack_qoverp(prefix + "refittedTrack_qoverp",&master),
    refittedTrack_d0(prefix + "refittedTrack_d0",&master),
    refittedTrack_z0(prefix + "refittedTrack_z0",&master),
    refittedTrack_theta(prefix + "refittedTrack_theta",&master),
    refittedTrack_phi(prefix + "refittedTrack_phi",&master),
    refittedTrack_LMqoverp(prefix + "refittedTrack_LMqoverp",&master),
    refittedTrack_d0_wrtBL(prefix + "refittedTrack_d0_wrtBL",&master),
    refittedTrack_z0_wrtBL(prefix + "refittedTrack_z0_wrtBL",&master),
    refittedTrack_phi_wrtBL(prefix + "refittedTrack_phi_wrtBL",&master),
    refittedTrack_theta_wrtBL(prefix + "refittedTrack_theta_wrtBL",&master),
    refittedTrack_qoverp_wrtBL(prefix + "refittedTrack_qoverp_wrtBL",&master),
    etas0(prefix + "etas0",&master),
    phis0(prefix + "phis0",&master),
    etas1(prefix + "etas1",&master),
    phis1(prefix + "phis1",&master),
    etas2(prefix + "etas2",&master),
    phis2(prefix + "phis2",&master),
    etas3(prefix + "etas3",&master),
    phis3(prefix + "phis3",&master),
    cl_E(prefix + "cl_E",&master),
    cl_pt(prefix + "cl_pt",&master),
    cl_eta(prefix + "cl_eta",&master),
    cl_phi(prefix + "cl_phi",&master),
    firstEdens(prefix + "firstEdens",&master),
    cellmaxfrac(prefix + "cellmaxfrac",&master),
    longitudinal(prefix + "longitudinal",&master),
    secondlambda(prefix + "secondlambda",&master),
    lateral(prefix + "lateral",&master),
    secondR(prefix + "secondR",&master),
    centerlambda(prefix + "centerlambda",&master),
    trackd0(prefix + "trackd0",&master),
    trackz0(prefix + "trackz0",&master),
    trackphi(prefix + "trackphi",&master),
    tracktheta(prefix + "tracktheta",&master),
    trackqoverp(prefix + "trackqoverp",&master),
    trackpt(prefix + "trackpt",&master),
    tracketa(prefix + "tracketa",&master),
    nBLHits(prefix + "nBLHits",&master),
    nPixHits(prefix + "nPixHits",&master),
    nSCTHits(prefix + "nSCTHits",&master),
    nTRTHits(prefix + "nTRTHits",&master),
    nTRTHighTHits(prefix + "nTRTHighTHits",&master),
    nTRTXenonHits(prefix + "nTRTXenonHits",&master),
    nPixHoles(prefix + "nPixHoles",&master),
    nSCTHoles(prefix + "nSCTHoles",&master),
    nTRTHoles(prefix + "nTRTHoles",&master),
    nPixelDeadSensors(prefix + "nPixelDeadSensors",&master),
    nSCTDeadSensors(prefix + "nSCTDeadSensors",&master),
    nBLSharedHits(prefix + "nBLSharedHits",&master),
    nPixSharedHits(prefix + "nPixSharedHits",&master),
    nSCTSharedHits(prefix + "nSCTSharedHits",&master),
    nBLayerSplitHits(prefix + "nBLayerSplitHits",&master),
    nPixSplitHits(prefix + "nPixSplitHits",&master),
    nBLayerOutliers(prefix + "nBLayerOutliers",&master),
    nPixelOutliers(prefix + "nPixelOutliers",&master),
    nSCTOutliers(prefix + "nSCTOutliers",&master),
    nTRTOutliers(prefix + "nTRTOutliers",&master),
    nTRTHighTOutliers(prefix + "nTRTHighTOutliers",&master),
    nContribPixelLayers(prefix + "nContribPixelLayers",&master),
    nGangedPixels(prefix + "nGangedPixels",&master),
    nGangedFlaggedFakes(prefix + "nGangedFlaggedFakes",&master),
    nPixelSpoiltHits(prefix + "nPixelSpoiltHits",&master),
    nSCTDoubleHoles(prefix + "nSCTDoubleHoles",&master),
    nSCTSpoiltHits(prefix + "nSCTSpoiltHits",&master),
    expectBLayerHit(prefix + "expectBLayerHit",&master),
    nSiHits(prefix + "nSiHits",&master),
    TRTHighTHitsRatio(prefix + "TRTHighTHitsRatio",&master),
    TRTHighTOutliersRatio(prefix + "TRTHighTOutliersRatio",&master),
    pixeldEdx(prefix + "pixeldEdx",&master),
    nGoodHitsPixeldEdx(prefix + "nGoodHitsPixeldEdx",&master),
    massPixeldEdx(prefix + "massPixeldEdx",&master),
    likelihoodsPixeldEdx(prefix + "likelihoodsPixeldEdx",&master),
    vertweight(prefix + "vertweight",&master),
    trackd0beam(prefix + "trackd0beam",&master),
    trackz0beam(prefix + "trackz0beam",&master),
    tracksigd0beam(prefix + "tracksigd0beam",&master),
    tracksigz0beam(prefix + "tracksigz0beam",&master),
    trackd0pv(prefix + "trackd0pv",&master),
    trackz0pv(prefix + "trackz0pv",&master),
    tracksigd0pv(prefix + "tracksigd0pv",&master),
    tracksigz0pv(prefix + "tracksigz0pv",&master),
    trackd0pvunbiased(prefix + "trackd0pvunbiased",&master),
    trackz0pvunbiased(prefix + "trackz0pvunbiased",&master),
    tracksigd0pvunbiased(prefix + "tracksigd0pvunbiased",&master),
    tracksigz0pvunbiased(prefix + "tracksigz0pvunbiased",&master),
    theta_LM(prefix + "theta_LM",&master),
    qoverp_LM(prefix + "qoverp_LM",&master),
    hastrack(prefix + "hastrack",&master),
    deltaEmax2(prefix + "deltaEmax2",&master),
    calibHitsShowerDepth(prefix + "calibHitsShowerDepth",&master),
    isIso(prefix + "isIso",&master),
    EcellS0(prefix + "EcellS0",&master),
    etacellS0(prefix + "etacellS0",&master),
    Etcone40_ED_corrected(prefix + "Etcone40_ED_corrected",&master),
    Etcone40_corrected(prefix + "Etcone40_corrected",&master),
    topoEtcone20_corrected(prefix + "topoEtcone20_corrected",&master),
    topoEtcone30_corrected(prefix + "topoEtcone30_corrected",&master),
    topoEtcone40_corrected(prefix + "topoEtcone40_corrected",&master),
    EF_dr(prefix + "EF_dr",&master),
    L2_dr(prefix + "L2_dr",&master),
    L1_dr(prefix + "L1_dr",&master),
    L1_index(prefix + "L1_index",&master),
    MET_n(prefix + "MET_n",&master),
    MET_wpx(prefix + "MET_wpx",&master),
    MET_wpy(prefix + "MET_wpy",&master),
    MET_wet(prefix + "MET_wet",&master),
    MET_statusWord(prefix + "MET_statusWord",&master),
    MET_BDTMedium_n(prefix + "MET_BDTMedium_n",&master),
    MET_BDTMedium_wpx(prefix + "MET_BDTMedium_wpx",&master),
    MET_BDTMedium_wpy(prefix + "MET_BDTMedium_wpy",&master),
    MET_BDTMedium_wet(prefix + "MET_BDTMedium_wet",&master),
    MET_BDTMedium_statusWord(prefix + "MET_BDTMedium_statusWord",&master),
    MET_AntiKt4LCTopo_tightpp_n(prefix + "MET_AntiKt4LCTopo_tightpp_n",&master),
    MET_AntiKt4LCTopo_tightpp_wpx(prefix + "MET_AntiKt4LCTopo_tightpp_wpx",&master),
    MET_AntiKt4LCTopo_tightpp_wpy(prefix + "MET_AntiKt4LCTopo_tightpp_wpy",&master),
    MET_AntiKt4LCTopo_tightpp_wet(prefix + "MET_AntiKt4LCTopo_tightpp_wet",&master),
    MET_AntiKt4LCTopo_tightpp_statusWord(prefix + "MET_AntiKt4LCTopo_tightpp_statusWord",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  ElectronD3PDCollection::ElectronD3PDCollection(const std::string& prefix):
    TObject(),
    n(prefix + "n",0),
    E(prefix + "E",0),
    Et(prefix + "Et",0),
    pt(prefix + "pt",0),
    m(prefix + "m",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    px(prefix + "px",0),
    py(prefix + "py",0),
    pz(prefix + "pz",0),
    charge(prefix + "charge",0),
    author(prefix + "author",0),
    isEM(prefix + "isEM",0),
    isEMLoose(prefix + "isEMLoose",0),
    isEMMedium(prefix + "isEMMedium",0),
    isEMTight(prefix + "isEMTight",0),
    OQ(prefix + "OQ",0),
    isConv(prefix + "isConv",0),
    nConv(prefix + "nConv",0),
    nSingleTrackConv(prefix + "nSingleTrackConv",0),
    nDoubleTrackConv(prefix + "nDoubleTrackConv",0),
    type(prefix + "type",0),
    origin(prefix + "origin",0),
    typebkg(prefix + "typebkg",0),
    originbkg(prefix + "originbkg",0),
    truth_E(prefix + "truth_E",0),
    truth_pt(prefix + "truth_pt",0),
    truth_eta(prefix + "truth_eta",0),
    truth_phi(prefix + "truth_phi",0),
    truth_type(prefix + "truth_type",0),
    truth_status(prefix + "truth_status",0),
    truth_barcode(prefix + "truth_barcode",0),
    truth_mothertype(prefix + "truth_mothertype",0),
    truth_motherbarcode(prefix + "truth_motherbarcode",0),
    truth_hasHardBrem(prefix + "truth_hasHardBrem",0),
    truth_index(prefix + "truth_index",0),
    truth_matched(prefix + "truth_matched",0),
    mediumWithoutTrack(prefix + "mediumWithoutTrack",0),
    mediumIsoWithoutTrack(prefix + "mediumIsoWithoutTrack",0),
    tightWithoutTrack(prefix + "tightWithoutTrack",0),
    tightIsoWithoutTrack(prefix + "tightIsoWithoutTrack",0),
    loose(prefix + "loose",0),
    looseIso(prefix + "looseIso",0),
    medium(prefix + "medium",0),
    mediumIso(prefix + "mediumIso",0),
    tight(prefix + "tight",0),
    tightIso(prefix + "tightIso",0),
    loosePP(prefix + "loosePP",0),
    loosePPIso(prefix + "loosePPIso",0),
    mediumPP(prefix + "mediumPP",0),
    mediumPPIso(prefix + "mediumPPIso",0),
    tightPP(prefix + "tightPP",0),
    tightPPIso(prefix + "tightPPIso",0),
    goodOQ(prefix + "goodOQ",0),
    Ethad(prefix + "Ethad",0),
    Ethad1(prefix + "Ethad1",0),
    f1(prefix + "f1",0),
    f1core(prefix + "f1core",0),
    Emins1(prefix + "Emins1",0),
    fside(prefix + "fside",0),
    Emax2(prefix + "Emax2",0),
    ws3(prefix + "ws3",0),
    wstot(prefix + "wstot",0),
    emaxs1(prefix + "emaxs1",0),
    deltaEs(prefix + "deltaEs",0),
    E233(prefix + "E233",0),
    E237(prefix + "E237",0),
    E277(prefix + "E277",0),
    weta2(prefix + "weta2",0),
    f3(prefix + "f3",0),
    f3core(prefix + "f3core",0),
    rphiallcalo(prefix + "rphiallcalo",0),
    Etcone45(prefix + "Etcone45",0),
    Etcone15(prefix + "Etcone15",0),
    Etcone20(prefix + "Etcone20",0),
    Etcone25(prefix + "Etcone25",0),
    Etcone30(prefix + "Etcone30",0),
    Etcone35(prefix + "Etcone35",0),
    Etcone40(prefix + "Etcone40",0),
    ptcone20(prefix + "ptcone20",0),
    ptcone30(prefix + "ptcone30",0),
    ptcone40(prefix + "ptcone40",0),
    nucone20(prefix + "nucone20",0),
    nucone30(prefix + "nucone30",0),
    nucone40(prefix + "nucone40",0),
    hasconv(prefix + "hasconv",0),
    Rconv(prefix + "Rconv",0),
    zconv(prefix + "zconv",0),
    pt1conv(prefix + "pt1conv",0),
    pt2conv(prefix + "pt2conv",0),
    ptconv(prefix + "ptconv",0),
    pzconv(prefix + "pzconv",0),
    pos7(prefix + "pos7",0),
    etacorrmag(prefix + "etacorrmag",0),
    deltaeta1(prefix + "deltaeta1",0),
    deltaeta2(prefix + "deltaeta2",0),
    deltaphi2(prefix + "deltaphi2",0),
    deltaphiRescaled(prefix + "deltaphiRescaled",0),
    deltaPhiFromLast(prefix + "deltaPhiFromLast",0),
    deltaPhiRot(prefix + "deltaPhiRot",0),
    expectHitInBLayer(prefix + "expectHitInBLayer",0),
    trackd0_physics(prefix + "trackd0_physics",0),
    etaSampling1(prefix + "etaSampling1",0),
    reta(prefix + "reta",0),
    rphi(prefix + "rphi",0),
    topoEtcone20(prefix + "topoEtcone20",0),
    topoEtcone30(prefix + "topoEtcone30",0),
    topoEtcone40(prefix + "topoEtcone40",0),
    zvertex(prefix + "zvertex",0),
    errz(prefix + "errz",0),
    etap(prefix + "etap",0),
    depth(prefix + "depth",0),
    refittedTrack_n(prefix + "refittedTrack_n",0),
    refittedTrack_author(prefix + "refittedTrack_author",0),
    refittedTrack_chi2(prefix + "refittedTrack_chi2",0),
    refittedTrack_hasBrem(prefix + "refittedTrack_hasBrem",0),
    refittedTrack_bremRadius(prefix + "refittedTrack_bremRadius",0),
    refittedTrack_bremZ(prefix + "refittedTrack_bremZ",0),
    refittedTrack_bremRadiusErr(prefix + "refittedTrack_bremRadiusErr",0),
    refittedTrack_bremZErr(prefix + "refittedTrack_bremZErr",0),
    refittedTrack_bremFitStatus(prefix + "refittedTrack_bremFitStatus",0),
    refittedTrack_qoverp(prefix + "refittedTrack_qoverp",0),
    refittedTrack_d0(prefix + "refittedTrack_d0",0),
    refittedTrack_z0(prefix + "refittedTrack_z0",0),
    refittedTrack_theta(prefix + "refittedTrack_theta",0),
    refittedTrack_phi(prefix + "refittedTrack_phi",0),
    refittedTrack_LMqoverp(prefix + "refittedTrack_LMqoverp",0),
    refittedTrack_d0_wrtBL(prefix + "refittedTrack_d0_wrtBL",0),
    refittedTrack_z0_wrtBL(prefix + "refittedTrack_z0_wrtBL",0),
    refittedTrack_phi_wrtBL(prefix + "refittedTrack_phi_wrtBL",0),
    refittedTrack_theta_wrtBL(prefix + "refittedTrack_theta_wrtBL",0),
    refittedTrack_qoverp_wrtBL(prefix + "refittedTrack_qoverp_wrtBL",0),
    etas0(prefix + "etas0",0),
    phis0(prefix + "phis0",0),
    etas1(prefix + "etas1",0),
    phis1(prefix + "phis1",0),
    etas2(prefix + "etas2",0),
    phis2(prefix + "phis2",0),
    etas3(prefix + "etas3",0),
    phis3(prefix + "phis3",0),
    cl_E(prefix + "cl_E",0),
    cl_pt(prefix + "cl_pt",0),
    cl_eta(prefix + "cl_eta",0),
    cl_phi(prefix + "cl_phi",0),
    firstEdens(prefix + "firstEdens",0),
    cellmaxfrac(prefix + "cellmaxfrac",0),
    longitudinal(prefix + "longitudinal",0),
    secondlambda(prefix + "secondlambda",0),
    lateral(prefix + "lateral",0),
    secondR(prefix + "secondR",0),
    centerlambda(prefix + "centerlambda",0),
    trackd0(prefix + "trackd0",0),
    trackz0(prefix + "trackz0",0),
    trackphi(prefix + "trackphi",0),
    tracktheta(prefix + "tracktheta",0),
    trackqoverp(prefix + "trackqoverp",0),
    trackpt(prefix + "trackpt",0),
    tracketa(prefix + "tracketa",0),
    nBLHits(prefix + "nBLHits",0),
    nPixHits(prefix + "nPixHits",0),
    nSCTHits(prefix + "nSCTHits",0),
    nTRTHits(prefix + "nTRTHits",0),
    nTRTHighTHits(prefix + "nTRTHighTHits",0),
    nTRTXenonHits(prefix + "nTRTXenonHits",0),
    nPixHoles(prefix + "nPixHoles",0),
    nSCTHoles(prefix + "nSCTHoles",0),
    nTRTHoles(prefix + "nTRTHoles",0),
    nPixelDeadSensors(prefix + "nPixelDeadSensors",0),
    nSCTDeadSensors(prefix + "nSCTDeadSensors",0),
    nBLSharedHits(prefix + "nBLSharedHits",0),
    nPixSharedHits(prefix + "nPixSharedHits",0),
    nSCTSharedHits(prefix + "nSCTSharedHits",0),
    nBLayerSplitHits(prefix + "nBLayerSplitHits",0),
    nPixSplitHits(prefix + "nPixSplitHits",0),
    nBLayerOutliers(prefix + "nBLayerOutliers",0),
    nPixelOutliers(prefix + "nPixelOutliers",0),
    nSCTOutliers(prefix + "nSCTOutliers",0),
    nTRTOutliers(prefix + "nTRTOutliers",0),
    nTRTHighTOutliers(prefix + "nTRTHighTOutliers",0),
    nContribPixelLayers(prefix + "nContribPixelLayers",0),
    nGangedPixels(prefix + "nGangedPixels",0),
    nGangedFlaggedFakes(prefix + "nGangedFlaggedFakes",0),
    nPixelSpoiltHits(prefix + "nPixelSpoiltHits",0),
    nSCTDoubleHoles(prefix + "nSCTDoubleHoles",0),
    nSCTSpoiltHits(prefix + "nSCTSpoiltHits",0),
    expectBLayerHit(prefix + "expectBLayerHit",0),
    nSiHits(prefix + "nSiHits",0),
    TRTHighTHitsRatio(prefix + "TRTHighTHitsRatio",0),
    TRTHighTOutliersRatio(prefix + "TRTHighTOutliersRatio",0),
    pixeldEdx(prefix + "pixeldEdx",0),
    nGoodHitsPixeldEdx(prefix + "nGoodHitsPixeldEdx",0),
    massPixeldEdx(prefix + "massPixeldEdx",0),
    likelihoodsPixeldEdx(prefix + "likelihoodsPixeldEdx",0),
    vertweight(prefix + "vertweight",0),
    trackd0beam(prefix + "trackd0beam",0),
    trackz0beam(prefix + "trackz0beam",0),
    tracksigd0beam(prefix + "tracksigd0beam",0),
    tracksigz0beam(prefix + "tracksigz0beam",0),
    trackd0pv(prefix + "trackd0pv",0),
    trackz0pv(prefix + "trackz0pv",0),
    tracksigd0pv(prefix + "tracksigd0pv",0),
    tracksigz0pv(prefix + "tracksigz0pv",0),
    trackd0pvunbiased(prefix + "trackd0pvunbiased",0),
    trackz0pvunbiased(prefix + "trackz0pvunbiased",0),
    tracksigd0pvunbiased(prefix + "tracksigd0pvunbiased",0),
    tracksigz0pvunbiased(prefix + "tracksigz0pvunbiased",0),
    theta_LM(prefix + "theta_LM",0),
    qoverp_LM(prefix + "qoverp_LM",0),
    hastrack(prefix + "hastrack",0),
    deltaEmax2(prefix + "deltaEmax2",0),
    calibHitsShowerDepth(prefix + "calibHitsShowerDepth",0),
    isIso(prefix + "isIso",0),
    EcellS0(prefix + "EcellS0",0),
    etacellS0(prefix + "etacellS0",0),
    Etcone40_ED_corrected(prefix + "Etcone40_ED_corrected",0),
    Etcone40_corrected(prefix + "Etcone40_corrected",0),
    topoEtcone20_corrected(prefix + "topoEtcone20_corrected",0),
    topoEtcone30_corrected(prefix + "topoEtcone30_corrected",0),
    topoEtcone40_corrected(prefix + "topoEtcone40_corrected",0),
    EF_dr(prefix + "EF_dr",0),
    L2_dr(prefix + "L2_dr",0),
    L1_dr(prefix + "L1_dr",0),
    L1_index(prefix + "L1_index",0),
    MET_n(prefix + "MET_n",0),
    MET_wpx(prefix + "MET_wpx",0),
    MET_wpy(prefix + "MET_wpy",0),
    MET_wet(prefix + "MET_wet",0),
    MET_statusWord(prefix + "MET_statusWord",0),
    MET_BDTMedium_n(prefix + "MET_BDTMedium_n",0),
    MET_BDTMedium_wpx(prefix + "MET_BDTMedium_wpx",0),
    MET_BDTMedium_wpy(prefix + "MET_BDTMedium_wpy",0),
    MET_BDTMedium_wet(prefix + "MET_BDTMedium_wet",0),
    MET_BDTMedium_statusWord(prefix + "MET_BDTMedium_statusWord",0),
    MET_AntiKt4LCTopo_tightpp_n(prefix + "MET_AntiKt4LCTopo_tightpp_n",0),
    MET_AntiKt4LCTopo_tightpp_wpx(prefix + "MET_AntiKt4LCTopo_tightpp_wpx",0),
    MET_AntiKt4LCTopo_tightpp_wpy(prefix + "MET_AntiKt4LCTopo_tightpp_wpy",0),
    MET_AntiKt4LCTopo_tightpp_wet(prefix + "MET_AntiKt4LCTopo_tightpp_wet",0),
    MET_AntiKt4LCTopo_tightpp_statusWord(prefix + "MET_AntiKt4LCTopo_tightpp_statusWord",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void ElectronD3PDCollection::ReadFrom(TTree* tree)
  {
    n.ReadFrom(tree);
    E.ReadFrom(tree);
    Et.ReadFrom(tree);
    pt.ReadFrom(tree);
    m.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    px.ReadFrom(tree);
    py.ReadFrom(tree);
    pz.ReadFrom(tree);
    charge.ReadFrom(tree);
    author.ReadFrom(tree);
    isEM.ReadFrom(tree);
    isEMLoose.ReadFrom(tree);
    isEMMedium.ReadFrom(tree);
    isEMTight.ReadFrom(tree);
    OQ.ReadFrom(tree);
    isConv.ReadFrom(tree);
    nConv.ReadFrom(tree);
    nSingleTrackConv.ReadFrom(tree);
    nDoubleTrackConv.ReadFrom(tree);
    type.ReadFrom(tree);
    origin.ReadFrom(tree);
    typebkg.ReadFrom(tree);
    originbkg.ReadFrom(tree);
    truth_E.ReadFrom(tree);
    truth_pt.ReadFrom(tree);
    truth_eta.ReadFrom(tree);
    truth_phi.ReadFrom(tree);
    truth_type.ReadFrom(tree);
    truth_status.ReadFrom(tree);
    truth_barcode.ReadFrom(tree);
    truth_mothertype.ReadFrom(tree);
    truth_motherbarcode.ReadFrom(tree);
    truth_hasHardBrem.ReadFrom(tree);
    truth_index.ReadFrom(tree);
    truth_matched.ReadFrom(tree);
    mediumWithoutTrack.ReadFrom(tree);
    mediumIsoWithoutTrack.ReadFrom(tree);
    tightWithoutTrack.ReadFrom(tree);
    tightIsoWithoutTrack.ReadFrom(tree);
    loose.ReadFrom(tree);
    looseIso.ReadFrom(tree);
    medium.ReadFrom(tree);
    mediumIso.ReadFrom(tree);
    tight.ReadFrom(tree);
    tightIso.ReadFrom(tree);
    loosePP.ReadFrom(tree);
    loosePPIso.ReadFrom(tree);
    mediumPP.ReadFrom(tree);
    mediumPPIso.ReadFrom(tree);
    tightPP.ReadFrom(tree);
    tightPPIso.ReadFrom(tree);
    goodOQ.ReadFrom(tree);
    Ethad.ReadFrom(tree);
    Ethad1.ReadFrom(tree);
    f1.ReadFrom(tree);
    f1core.ReadFrom(tree);
    Emins1.ReadFrom(tree);
    fside.ReadFrom(tree);
    Emax2.ReadFrom(tree);
    ws3.ReadFrom(tree);
    wstot.ReadFrom(tree);
    emaxs1.ReadFrom(tree);
    deltaEs.ReadFrom(tree);
    E233.ReadFrom(tree);
    E237.ReadFrom(tree);
    E277.ReadFrom(tree);
    weta2.ReadFrom(tree);
    f3.ReadFrom(tree);
    f3core.ReadFrom(tree);
    rphiallcalo.ReadFrom(tree);
    Etcone45.ReadFrom(tree);
    Etcone15.ReadFrom(tree);
    Etcone20.ReadFrom(tree);
    Etcone25.ReadFrom(tree);
    Etcone30.ReadFrom(tree);
    Etcone35.ReadFrom(tree);
    Etcone40.ReadFrom(tree);
    ptcone20.ReadFrom(tree);
    ptcone30.ReadFrom(tree);
    ptcone40.ReadFrom(tree);
    nucone20.ReadFrom(tree);
    nucone30.ReadFrom(tree);
    nucone40.ReadFrom(tree);
    hasconv.ReadFrom(tree);
    Rconv.ReadFrom(tree);
    zconv.ReadFrom(tree);
    pt1conv.ReadFrom(tree);
    pt2conv.ReadFrom(tree);
    ptconv.ReadFrom(tree);
    pzconv.ReadFrom(tree);
    pos7.ReadFrom(tree);
    etacorrmag.ReadFrom(tree);
    deltaeta1.ReadFrom(tree);
    deltaeta2.ReadFrom(tree);
    deltaphi2.ReadFrom(tree);
    deltaphiRescaled.ReadFrom(tree);
    deltaPhiFromLast.ReadFrom(tree);
    deltaPhiRot.ReadFrom(tree);
    expectHitInBLayer.ReadFrom(tree);
    trackd0_physics.ReadFrom(tree);
    etaSampling1.ReadFrom(tree);
    reta.ReadFrom(tree);
    rphi.ReadFrom(tree);
    topoEtcone20.ReadFrom(tree);
    topoEtcone30.ReadFrom(tree);
    topoEtcone40.ReadFrom(tree);
    zvertex.ReadFrom(tree);
    errz.ReadFrom(tree);
    etap.ReadFrom(tree);
    depth.ReadFrom(tree);
    refittedTrack_n.ReadFrom(tree);
    refittedTrack_author.ReadFrom(tree);
    refittedTrack_chi2.ReadFrom(tree);
    refittedTrack_hasBrem.ReadFrom(tree);
    refittedTrack_bremRadius.ReadFrom(tree);
    refittedTrack_bremZ.ReadFrom(tree);
    refittedTrack_bremRadiusErr.ReadFrom(tree);
    refittedTrack_bremZErr.ReadFrom(tree);
    refittedTrack_bremFitStatus.ReadFrom(tree);
    refittedTrack_qoverp.ReadFrom(tree);
    refittedTrack_d0.ReadFrom(tree);
    refittedTrack_z0.ReadFrom(tree);
    refittedTrack_theta.ReadFrom(tree);
    refittedTrack_phi.ReadFrom(tree);
    refittedTrack_LMqoverp.ReadFrom(tree);
    refittedTrack_d0_wrtBL.ReadFrom(tree);
    refittedTrack_z0_wrtBL.ReadFrom(tree);
    refittedTrack_phi_wrtBL.ReadFrom(tree);
    refittedTrack_theta_wrtBL.ReadFrom(tree);
    refittedTrack_qoverp_wrtBL.ReadFrom(tree);
    etas0.ReadFrom(tree);
    phis0.ReadFrom(tree);
    etas1.ReadFrom(tree);
    phis1.ReadFrom(tree);
    etas2.ReadFrom(tree);
    phis2.ReadFrom(tree);
    etas3.ReadFrom(tree);
    phis3.ReadFrom(tree);
    cl_E.ReadFrom(tree);
    cl_pt.ReadFrom(tree);
    cl_eta.ReadFrom(tree);
    cl_phi.ReadFrom(tree);
    firstEdens.ReadFrom(tree);
    cellmaxfrac.ReadFrom(tree);
    longitudinal.ReadFrom(tree);
    secondlambda.ReadFrom(tree);
    lateral.ReadFrom(tree);
    secondR.ReadFrom(tree);
    centerlambda.ReadFrom(tree);
    trackd0.ReadFrom(tree);
    trackz0.ReadFrom(tree);
    trackphi.ReadFrom(tree);
    tracktheta.ReadFrom(tree);
    trackqoverp.ReadFrom(tree);
    trackpt.ReadFrom(tree);
    tracketa.ReadFrom(tree);
    nBLHits.ReadFrom(tree);
    nPixHits.ReadFrom(tree);
    nSCTHits.ReadFrom(tree);
    nTRTHits.ReadFrom(tree);
    nTRTHighTHits.ReadFrom(tree);
    nTRTXenonHits.ReadFrom(tree);
    nPixHoles.ReadFrom(tree);
    nSCTHoles.ReadFrom(tree);
    nTRTHoles.ReadFrom(tree);
    nPixelDeadSensors.ReadFrom(tree);
    nSCTDeadSensors.ReadFrom(tree);
    nBLSharedHits.ReadFrom(tree);
    nPixSharedHits.ReadFrom(tree);
    nSCTSharedHits.ReadFrom(tree);
    nBLayerSplitHits.ReadFrom(tree);
    nPixSplitHits.ReadFrom(tree);
    nBLayerOutliers.ReadFrom(tree);
    nPixelOutliers.ReadFrom(tree);
    nSCTOutliers.ReadFrom(tree);
    nTRTOutliers.ReadFrom(tree);
    nTRTHighTOutliers.ReadFrom(tree);
    nContribPixelLayers.ReadFrom(tree);
    nGangedPixels.ReadFrom(tree);
    nGangedFlaggedFakes.ReadFrom(tree);
    nPixelSpoiltHits.ReadFrom(tree);
    nSCTDoubleHoles.ReadFrom(tree);
    nSCTSpoiltHits.ReadFrom(tree);
    expectBLayerHit.ReadFrom(tree);
    nSiHits.ReadFrom(tree);
    TRTHighTHitsRatio.ReadFrom(tree);
    TRTHighTOutliersRatio.ReadFrom(tree);
    pixeldEdx.ReadFrom(tree);
    nGoodHitsPixeldEdx.ReadFrom(tree);
    massPixeldEdx.ReadFrom(tree);
    likelihoodsPixeldEdx.ReadFrom(tree);
    vertweight.ReadFrom(tree);
    trackd0beam.ReadFrom(tree);
    trackz0beam.ReadFrom(tree);
    tracksigd0beam.ReadFrom(tree);
    tracksigz0beam.ReadFrom(tree);
    trackd0pv.ReadFrom(tree);
    trackz0pv.ReadFrom(tree);
    tracksigd0pv.ReadFrom(tree);
    tracksigz0pv.ReadFrom(tree);
    trackd0pvunbiased.ReadFrom(tree);
    trackz0pvunbiased.ReadFrom(tree);
    tracksigd0pvunbiased.ReadFrom(tree);
    tracksigz0pvunbiased.ReadFrom(tree);
    theta_LM.ReadFrom(tree);
    qoverp_LM.ReadFrom(tree);
    hastrack.ReadFrom(tree);
    deltaEmax2.ReadFrom(tree);
    calibHitsShowerDepth.ReadFrom(tree);
    isIso.ReadFrom(tree);
    EcellS0.ReadFrom(tree);
    etacellS0.ReadFrom(tree);
    Etcone40_ED_corrected.ReadFrom(tree);
    Etcone40_corrected.ReadFrom(tree);
    topoEtcone20_corrected.ReadFrom(tree);
    topoEtcone30_corrected.ReadFrom(tree);
    topoEtcone40_corrected.ReadFrom(tree);
    EF_dr.ReadFrom(tree);
    L2_dr.ReadFrom(tree);
    L1_dr.ReadFrom(tree);
    L1_index.ReadFrom(tree);
    MET_n.ReadFrom(tree);
    MET_wpx.ReadFrom(tree);
    MET_wpy.ReadFrom(tree);
    MET_wet.ReadFrom(tree);
    MET_statusWord.ReadFrom(tree);
    MET_BDTMedium_n.ReadFrom(tree);
    MET_BDTMedium_wpx.ReadFrom(tree);
    MET_BDTMedium_wpy.ReadFrom(tree);
    MET_BDTMedium_wet.ReadFrom(tree);
    MET_BDTMedium_statusWord.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_n.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_wpx.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_wpy.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_wet.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_statusWord.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void ElectronD3PDCollection::WriteTo(TTree* tree)
  {
    n.WriteTo(tree);
    E.WriteTo(tree);
    Et.WriteTo(tree);
    pt.WriteTo(tree);
    m.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    px.WriteTo(tree);
    py.WriteTo(tree);
    pz.WriteTo(tree);
    charge.WriteTo(tree);
    author.WriteTo(tree);
    isEM.WriteTo(tree);
    isEMLoose.WriteTo(tree);
    isEMMedium.WriteTo(tree);
    isEMTight.WriteTo(tree);
    OQ.WriteTo(tree);
    isConv.WriteTo(tree);
    nConv.WriteTo(tree);
    nSingleTrackConv.WriteTo(tree);
    nDoubleTrackConv.WriteTo(tree);
    type.WriteTo(tree);
    origin.WriteTo(tree);
    typebkg.WriteTo(tree);
    originbkg.WriteTo(tree);
    truth_E.WriteTo(tree);
    truth_pt.WriteTo(tree);
    truth_eta.WriteTo(tree);
    truth_phi.WriteTo(tree);
    truth_type.WriteTo(tree);
    truth_status.WriteTo(tree);
    truth_barcode.WriteTo(tree);
    truth_mothertype.WriteTo(tree);
    truth_motherbarcode.WriteTo(tree);
    truth_hasHardBrem.WriteTo(tree);
    truth_index.WriteTo(tree);
    truth_matched.WriteTo(tree);
    mediumWithoutTrack.WriteTo(tree);
    mediumIsoWithoutTrack.WriteTo(tree);
    tightWithoutTrack.WriteTo(tree);
    tightIsoWithoutTrack.WriteTo(tree);
    loose.WriteTo(tree);
    looseIso.WriteTo(tree);
    medium.WriteTo(tree);
    mediumIso.WriteTo(tree);
    tight.WriteTo(tree);
    tightIso.WriteTo(tree);
    loosePP.WriteTo(tree);
    loosePPIso.WriteTo(tree);
    mediumPP.WriteTo(tree);
    mediumPPIso.WriteTo(tree);
    tightPP.WriteTo(tree);
    tightPPIso.WriteTo(tree);
    goodOQ.WriteTo(tree);
    Ethad.WriteTo(tree);
    Ethad1.WriteTo(tree);
    f1.WriteTo(tree);
    f1core.WriteTo(tree);
    Emins1.WriteTo(tree);
    fside.WriteTo(tree);
    Emax2.WriteTo(tree);
    ws3.WriteTo(tree);
    wstot.WriteTo(tree);
    emaxs1.WriteTo(tree);
    deltaEs.WriteTo(tree);
    E233.WriteTo(tree);
    E237.WriteTo(tree);
    E277.WriteTo(tree);
    weta2.WriteTo(tree);
    f3.WriteTo(tree);
    f3core.WriteTo(tree);
    rphiallcalo.WriteTo(tree);
    Etcone45.WriteTo(tree);
    Etcone15.WriteTo(tree);
    Etcone20.WriteTo(tree);
    Etcone25.WriteTo(tree);
    Etcone30.WriteTo(tree);
    Etcone35.WriteTo(tree);
    Etcone40.WriteTo(tree);
    ptcone20.WriteTo(tree);
    ptcone30.WriteTo(tree);
    ptcone40.WriteTo(tree);
    nucone20.WriteTo(tree);
    nucone30.WriteTo(tree);
    nucone40.WriteTo(tree);
    hasconv.WriteTo(tree);
    Rconv.WriteTo(tree);
    zconv.WriteTo(tree);
    pt1conv.WriteTo(tree);
    pt2conv.WriteTo(tree);
    ptconv.WriteTo(tree);
    pzconv.WriteTo(tree);
    pos7.WriteTo(tree);
    etacorrmag.WriteTo(tree);
    deltaeta1.WriteTo(tree);
    deltaeta2.WriteTo(tree);
    deltaphi2.WriteTo(tree);
    deltaphiRescaled.WriteTo(tree);
    deltaPhiFromLast.WriteTo(tree);
    deltaPhiRot.WriteTo(tree);
    expectHitInBLayer.WriteTo(tree);
    trackd0_physics.WriteTo(tree);
    etaSampling1.WriteTo(tree);
    reta.WriteTo(tree);
    rphi.WriteTo(tree);
    topoEtcone20.WriteTo(tree);
    topoEtcone30.WriteTo(tree);
    topoEtcone40.WriteTo(tree);
    zvertex.WriteTo(tree);
    errz.WriteTo(tree);
    etap.WriteTo(tree);
    depth.WriteTo(tree);
    refittedTrack_n.WriteTo(tree);
    refittedTrack_author.WriteTo(tree);
    refittedTrack_chi2.WriteTo(tree);
    refittedTrack_hasBrem.WriteTo(tree);
    refittedTrack_bremRadius.WriteTo(tree);
    refittedTrack_bremZ.WriteTo(tree);
    refittedTrack_bremRadiusErr.WriteTo(tree);
    refittedTrack_bremZErr.WriteTo(tree);
    refittedTrack_bremFitStatus.WriteTo(tree);
    refittedTrack_qoverp.WriteTo(tree);
    refittedTrack_d0.WriteTo(tree);
    refittedTrack_z0.WriteTo(tree);
    refittedTrack_theta.WriteTo(tree);
    refittedTrack_phi.WriteTo(tree);
    refittedTrack_LMqoverp.WriteTo(tree);
    refittedTrack_d0_wrtBL.WriteTo(tree);
    refittedTrack_z0_wrtBL.WriteTo(tree);
    refittedTrack_phi_wrtBL.WriteTo(tree);
    refittedTrack_theta_wrtBL.WriteTo(tree);
    refittedTrack_qoverp_wrtBL.WriteTo(tree);
    etas0.WriteTo(tree);
    phis0.WriteTo(tree);
    etas1.WriteTo(tree);
    phis1.WriteTo(tree);
    etas2.WriteTo(tree);
    phis2.WriteTo(tree);
    etas3.WriteTo(tree);
    phis3.WriteTo(tree);
    cl_E.WriteTo(tree);
    cl_pt.WriteTo(tree);
    cl_eta.WriteTo(tree);
    cl_phi.WriteTo(tree);
    firstEdens.WriteTo(tree);
    cellmaxfrac.WriteTo(tree);
    longitudinal.WriteTo(tree);
    secondlambda.WriteTo(tree);
    lateral.WriteTo(tree);
    secondR.WriteTo(tree);
    centerlambda.WriteTo(tree);
    trackd0.WriteTo(tree);
    trackz0.WriteTo(tree);
    trackphi.WriteTo(tree);
    tracktheta.WriteTo(tree);
    trackqoverp.WriteTo(tree);
    trackpt.WriteTo(tree);
    tracketa.WriteTo(tree);
    nBLHits.WriteTo(tree);
    nPixHits.WriteTo(tree);
    nSCTHits.WriteTo(tree);
    nTRTHits.WriteTo(tree);
    nTRTHighTHits.WriteTo(tree);
    nTRTXenonHits.WriteTo(tree);
    nPixHoles.WriteTo(tree);
    nSCTHoles.WriteTo(tree);
    nTRTHoles.WriteTo(tree);
    nPixelDeadSensors.WriteTo(tree);
    nSCTDeadSensors.WriteTo(tree);
    nBLSharedHits.WriteTo(tree);
    nPixSharedHits.WriteTo(tree);
    nSCTSharedHits.WriteTo(tree);
    nBLayerSplitHits.WriteTo(tree);
    nPixSplitHits.WriteTo(tree);
    nBLayerOutliers.WriteTo(tree);
    nPixelOutliers.WriteTo(tree);
    nSCTOutliers.WriteTo(tree);
    nTRTOutliers.WriteTo(tree);
    nTRTHighTOutliers.WriteTo(tree);
    nContribPixelLayers.WriteTo(tree);
    nGangedPixels.WriteTo(tree);
    nGangedFlaggedFakes.WriteTo(tree);
    nPixelSpoiltHits.WriteTo(tree);
    nSCTDoubleHoles.WriteTo(tree);
    nSCTSpoiltHits.WriteTo(tree);
    expectBLayerHit.WriteTo(tree);
    nSiHits.WriteTo(tree);
    TRTHighTHitsRatio.WriteTo(tree);
    TRTHighTOutliersRatio.WriteTo(tree);
    pixeldEdx.WriteTo(tree);
    nGoodHitsPixeldEdx.WriteTo(tree);
    massPixeldEdx.WriteTo(tree);
    likelihoodsPixeldEdx.WriteTo(tree);
    vertweight.WriteTo(tree);
    trackd0beam.WriteTo(tree);
    trackz0beam.WriteTo(tree);
    tracksigd0beam.WriteTo(tree);
    tracksigz0beam.WriteTo(tree);
    trackd0pv.WriteTo(tree);
    trackz0pv.WriteTo(tree);
    tracksigd0pv.WriteTo(tree);
    tracksigz0pv.WriteTo(tree);
    trackd0pvunbiased.WriteTo(tree);
    trackz0pvunbiased.WriteTo(tree);
    tracksigd0pvunbiased.WriteTo(tree);
    tracksigz0pvunbiased.WriteTo(tree);
    theta_LM.WriteTo(tree);
    qoverp_LM.WriteTo(tree);
    hastrack.WriteTo(tree);
    deltaEmax2.WriteTo(tree);
    calibHitsShowerDepth.WriteTo(tree);
    isIso.WriteTo(tree);
    EcellS0.WriteTo(tree);
    etacellS0.WriteTo(tree);
    Etcone40_ED_corrected.WriteTo(tree);
    Etcone40_corrected.WriteTo(tree);
    topoEtcone20_corrected.WriteTo(tree);
    topoEtcone30_corrected.WriteTo(tree);
    topoEtcone40_corrected.WriteTo(tree);
    EF_dr.WriteTo(tree);
    L2_dr.WriteTo(tree);
    L1_dr.WriteTo(tree);
    L1_index.WriteTo(tree);
    MET_n.WriteTo(tree);
    MET_wpx.WriteTo(tree);
    MET_wpy.WriteTo(tree);
    MET_wet.WriteTo(tree);
    MET_statusWord.WriteTo(tree);
    MET_BDTMedium_n.WriteTo(tree);
    MET_BDTMedium_wpx.WriteTo(tree);
    MET_BDTMedium_wpy.WriteTo(tree);
    MET_BDTMedium_wet.WriteTo(tree);
    MET_BDTMedium_statusWord.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_n.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_wpx.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_wpy.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_wet.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_statusWord.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void ElectronD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(n.IsAvailable()) n.SetActive(active);
     if(E.IsAvailable()) E.SetActive(active);
     if(Et.IsAvailable()) Et.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(m.IsAvailable()) m.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(px.IsAvailable()) px.SetActive(active);
     if(py.IsAvailable()) py.SetActive(active);
     if(pz.IsAvailable()) pz.SetActive(active);
     if(charge.IsAvailable()) charge.SetActive(active);
     if(author.IsAvailable()) author.SetActive(active);
     if(isEM.IsAvailable()) isEM.SetActive(active);
     if(isEMLoose.IsAvailable()) isEMLoose.SetActive(active);
     if(isEMMedium.IsAvailable()) isEMMedium.SetActive(active);
     if(isEMTight.IsAvailable()) isEMTight.SetActive(active);
     if(OQ.IsAvailable()) OQ.SetActive(active);
     if(isConv.IsAvailable()) isConv.SetActive(active);
     if(nConv.IsAvailable()) nConv.SetActive(active);
     if(nSingleTrackConv.IsAvailable()) nSingleTrackConv.SetActive(active);
     if(nDoubleTrackConv.IsAvailable()) nDoubleTrackConv.SetActive(active);
     if(type.IsAvailable()) type.SetActive(active);
     if(origin.IsAvailable()) origin.SetActive(active);
     if(typebkg.IsAvailable()) typebkg.SetActive(active);
     if(originbkg.IsAvailable()) originbkg.SetActive(active);
     if(truth_E.IsAvailable()) truth_E.SetActive(active);
     if(truth_pt.IsAvailable()) truth_pt.SetActive(active);
     if(truth_eta.IsAvailable()) truth_eta.SetActive(active);
     if(truth_phi.IsAvailable()) truth_phi.SetActive(active);
     if(truth_type.IsAvailable()) truth_type.SetActive(active);
     if(truth_status.IsAvailable()) truth_status.SetActive(active);
     if(truth_barcode.IsAvailable()) truth_barcode.SetActive(active);
     if(truth_mothertype.IsAvailable()) truth_mothertype.SetActive(active);
     if(truth_motherbarcode.IsAvailable()) truth_motherbarcode.SetActive(active);
     if(truth_hasHardBrem.IsAvailable()) truth_hasHardBrem.SetActive(active);
     if(truth_index.IsAvailable()) truth_index.SetActive(active);
     if(truth_matched.IsAvailable()) truth_matched.SetActive(active);
     if(mediumWithoutTrack.IsAvailable()) mediumWithoutTrack.SetActive(active);
     if(mediumIsoWithoutTrack.IsAvailable()) mediumIsoWithoutTrack.SetActive(active);
     if(tightWithoutTrack.IsAvailable()) tightWithoutTrack.SetActive(active);
     if(tightIsoWithoutTrack.IsAvailable()) tightIsoWithoutTrack.SetActive(active);
     if(loose.IsAvailable()) loose.SetActive(active);
     if(looseIso.IsAvailable()) looseIso.SetActive(active);
     if(medium.IsAvailable()) medium.SetActive(active);
     if(mediumIso.IsAvailable()) mediumIso.SetActive(active);
     if(tight.IsAvailable()) tight.SetActive(active);
     if(tightIso.IsAvailable()) tightIso.SetActive(active);
     if(loosePP.IsAvailable()) loosePP.SetActive(active);
     if(loosePPIso.IsAvailable()) loosePPIso.SetActive(active);
     if(mediumPP.IsAvailable()) mediumPP.SetActive(active);
     if(mediumPPIso.IsAvailable()) mediumPPIso.SetActive(active);
     if(tightPP.IsAvailable()) tightPP.SetActive(active);
     if(tightPPIso.IsAvailable()) tightPPIso.SetActive(active);
     if(goodOQ.IsAvailable()) goodOQ.SetActive(active);
     if(Ethad.IsAvailable()) Ethad.SetActive(active);
     if(Ethad1.IsAvailable()) Ethad1.SetActive(active);
     if(f1.IsAvailable()) f1.SetActive(active);
     if(f1core.IsAvailable()) f1core.SetActive(active);
     if(Emins1.IsAvailable()) Emins1.SetActive(active);
     if(fside.IsAvailable()) fside.SetActive(active);
     if(Emax2.IsAvailable()) Emax2.SetActive(active);
     if(ws3.IsAvailable()) ws3.SetActive(active);
     if(wstot.IsAvailable()) wstot.SetActive(active);
     if(emaxs1.IsAvailable()) emaxs1.SetActive(active);
     if(deltaEs.IsAvailable()) deltaEs.SetActive(active);
     if(E233.IsAvailable()) E233.SetActive(active);
     if(E237.IsAvailable()) E237.SetActive(active);
     if(E277.IsAvailable()) E277.SetActive(active);
     if(weta2.IsAvailable()) weta2.SetActive(active);
     if(f3.IsAvailable()) f3.SetActive(active);
     if(f3core.IsAvailable()) f3core.SetActive(active);
     if(rphiallcalo.IsAvailable()) rphiallcalo.SetActive(active);
     if(Etcone45.IsAvailable()) Etcone45.SetActive(active);
     if(Etcone15.IsAvailable()) Etcone15.SetActive(active);
     if(Etcone20.IsAvailable()) Etcone20.SetActive(active);
     if(Etcone25.IsAvailable()) Etcone25.SetActive(active);
     if(Etcone30.IsAvailable()) Etcone30.SetActive(active);
     if(Etcone35.IsAvailable()) Etcone35.SetActive(active);
     if(Etcone40.IsAvailable()) Etcone40.SetActive(active);
     if(ptcone20.IsAvailable()) ptcone20.SetActive(active);
     if(ptcone30.IsAvailable()) ptcone30.SetActive(active);
     if(ptcone40.IsAvailable()) ptcone40.SetActive(active);
     if(nucone20.IsAvailable()) nucone20.SetActive(active);
     if(nucone30.IsAvailable()) nucone30.SetActive(active);
     if(nucone40.IsAvailable()) nucone40.SetActive(active);
     if(hasconv.IsAvailable()) hasconv.SetActive(active);
     if(Rconv.IsAvailable()) Rconv.SetActive(active);
     if(zconv.IsAvailable()) zconv.SetActive(active);
     if(pt1conv.IsAvailable()) pt1conv.SetActive(active);
     if(pt2conv.IsAvailable()) pt2conv.SetActive(active);
     if(ptconv.IsAvailable()) ptconv.SetActive(active);
     if(pzconv.IsAvailable()) pzconv.SetActive(active);
     if(pos7.IsAvailable()) pos7.SetActive(active);
     if(etacorrmag.IsAvailable()) etacorrmag.SetActive(active);
     if(deltaeta1.IsAvailable()) deltaeta1.SetActive(active);
     if(deltaeta2.IsAvailable()) deltaeta2.SetActive(active);
     if(deltaphi2.IsAvailable()) deltaphi2.SetActive(active);
     if(deltaphiRescaled.IsAvailable()) deltaphiRescaled.SetActive(active);
     if(deltaPhiFromLast.IsAvailable()) deltaPhiFromLast.SetActive(active);
     if(deltaPhiRot.IsAvailable()) deltaPhiRot.SetActive(active);
     if(expectHitInBLayer.IsAvailable()) expectHitInBLayer.SetActive(active);
     if(trackd0_physics.IsAvailable()) trackd0_physics.SetActive(active);
     if(etaSampling1.IsAvailable()) etaSampling1.SetActive(active);
     if(reta.IsAvailable()) reta.SetActive(active);
     if(rphi.IsAvailable()) rphi.SetActive(active);
     if(topoEtcone20.IsAvailable()) topoEtcone20.SetActive(active);
     if(topoEtcone30.IsAvailable()) topoEtcone30.SetActive(active);
     if(topoEtcone40.IsAvailable()) topoEtcone40.SetActive(active);
     if(zvertex.IsAvailable()) zvertex.SetActive(active);
     if(errz.IsAvailable()) errz.SetActive(active);
     if(etap.IsAvailable()) etap.SetActive(active);
     if(depth.IsAvailable()) depth.SetActive(active);
     if(refittedTrack_n.IsAvailable()) refittedTrack_n.SetActive(active);
     if(refittedTrack_author.IsAvailable()) refittedTrack_author.SetActive(active);
     if(refittedTrack_chi2.IsAvailable()) refittedTrack_chi2.SetActive(active);
     if(refittedTrack_hasBrem.IsAvailable()) refittedTrack_hasBrem.SetActive(active);
     if(refittedTrack_bremRadius.IsAvailable()) refittedTrack_bremRadius.SetActive(active);
     if(refittedTrack_bremZ.IsAvailable()) refittedTrack_bremZ.SetActive(active);
     if(refittedTrack_bremRadiusErr.IsAvailable()) refittedTrack_bremRadiusErr.SetActive(active);
     if(refittedTrack_bremZErr.IsAvailable()) refittedTrack_bremZErr.SetActive(active);
     if(refittedTrack_bremFitStatus.IsAvailable()) refittedTrack_bremFitStatus.SetActive(active);
     if(refittedTrack_qoverp.IsAvailable()) refittedTrack_qoverp.SetActive(active);
     if(refittedTrack_d0.IsAvailable()) refittedTrack_d0.SetActive(active);
     if(refittedTrack_z0.IsAvailable()) refittedTrack_z0.SetActive(active);
     if(refittedTrack_theta.IsAvailable()) refittedTrack_theta.SetActive(active);
     if(refittedTrack_phi.IsAvailable()) refittedTrack_phi.SetActive(active);
     if(refittedTrack_LMqoverp.IsAvailable()) refittedTrack_LMqoverp.SetActive(active);
     if(refittedTrack_d0_wrtBL.IsAvailable()) refittedTrack_d0_wrtBL.SetActive(active);
     if(refittedTrack_z0_wrtBL.IsAvailable()) refittedTrack_z0_wrtBL.SetActive(active);
     if(refittedTrack_phi_wrtBL.IsAvailable()) refittedTrack_phi_wrtBL.SetActive(active);
     if(refittedTrack_theta_wrtBL.IsAvailable()) refittedTrack_theta_wrtBL.SetActive(active);
     if(refittedTrack_qoverp_wrtBL.IsAvailable()) refittedTrack_qoverp_wrtBL.SetActive(active);
     if(etas0.IsAvailable()) etas0.SetActive(active);
     if(phis0.IsAvailable()) phis0.SetActive(active);
     if(etas1.IsAvailable()) etas1.SetActive(active);
     if(phis1.IsAvailable()) phis1.SetActive(active);
     if(etas2.IsAvailable()) etas2.SetActive(active);
     if(phis2.IsAvailable()) phis2.SetActive(active);
     if(etas3.IsAvailable()) etas3.SetActive(active);
     if(phis3.IsAvailable()) phis3.SetActive(active);
     if(cl_E.IsAvailable()) cl_E.SetActive(active);
     if(cl_pt.IsAvailable()) cl_pt.SetActive(active);
     if(cl_eta.IsAvailable()) cl_eta.SetActive(active);
     if(cl_phi.IsAvailable()) cl_phi.SetActive(active);
     if(firstEdens.IsAvailable()) firstEdens.SetActive(active);
     if(cellmaxfrac.IsAvailable()) cellmaxfrac.SetActive(active);
     if(longitudinal.IsAvailable()) longitudinal.SetActive(active);
     if(secondlambda.IsAvailable()) secondlambda.SetActive(active);
     if(lateral.IsAvailable()) lateral.SetActive(active);
     if(secondR.IsAvailable()) secondR.SetActive(active);
     if(centerlambda.IsAvailable()) centerlambda.SetActive(active);
     if(trackd0.IsAvailable()) trackd0.SetActive(active);
     if(trackz0.IsAvailable()) trackz0.SetActive(active);
     if(trackphi.IsAvailable()) trackphi.SetActive(active);
     if(tracktheta.IsAvailable()) tracktheta.SetActive(active);
     if(trackqoverp.IsAvailable()) trackqoverp.SetActive(active);
     if(trackpt.IsAvailable()) trackpt.SetActive(active);
     if(tracketa.IsAvailable()) tracketa.SetActive(active);
     if(nBLHits.IsAvailable()) nBLHits.SetActive(active);
     if(nPixHits.IsAvailable()) nPixHits.SetActive(active);
     if(nSCTHits.IsAvailable()) nSCTHits.SetActive(active);
     if(nTRTHits.IsAvailable()) nTRTHits.SetActive(active);
     if(nTRTHighTHits.IsAvailable()) nTRTHighTHits.SetActive(active);
     if(nTRTXenonHits.IsAvailable()) nTRTXenonHits.SetActive(active);
     if(nPixHoles.IsAvailable()) nPixHoles.SetActive(active);
     if(nSCTHoles.IsAvailable()) nSCTHoles.SetActive(active);
     if(nTRTHoles.IsAvailable()) nTRTHoles.SetActive(active);
     if(nPixelDeadSensors.IsAvailable()) nPixelDeadSensors.SetActive(active);
     if(nSCTDeadSensors.IsAvailable()) nSCTDeadSensors.SetActive(active);
     if(nBLSharedHits.IsAvailable()) nBLSharedHits.SetActive(active);
     if(nPixSharedHits.IsAvailable()) nPixSharedHits.SetActive(active);
     if(nSCTSharedHits.IsAvailable()) nSCTSharedHits.SetActive(active);
     if(nBLayerSplitHits.IsAvailable()) nBLayerSplitHits.SetActive(active);
     if(nPixSplitHits.IsAvailable()) nPixSplitHits.SetActive(active);
     if(nBLayerOutliers.IsAvailable()) nBLayerOutliers.SetActive(active);
     if(nPixelOutliers.IsAvailable()) nPixelOutliers.SetActive(active);
     if(nSCTOutliers.IsAvailable()) nSCTOutliers.SetActive(active);
     if(nTRTOutliers.IsAvailable()) nTRTOutliers.SetActive(active);
     if(nTRTHighTOutliers.IsAvailable()) nTRTHighTOutliers.SetActive(active);
     if(nContribPixelLayers.IsAvailable()) nContribPixelLayers.SetActive(active);
     if(nGangedPixels.IsAvailable()) nGangedPixels.SetActive(active);
     if(nGangedFlaggedFakes.IsAvailable()) nGangedFlaggedFakes.SetActive(active);
     if(nPixelSpoiltHits.IsAvailable()) nPixelSpoiltHits.SetActive(active);
     if(nSCTDoubleHoles.IsAvailable()) nSCTDoubleHoles.SetActive(active);
     if(nSCTSpoiltHits.IsAvailable()) nSCTSpoiltHits.SetActive(active);
     if(expectBLayerHit.IsAvailable()) expectBLayerHit.SetActive(active);
     if(nSiHits.IsAvailable()) nSiHits.SetActive(active);
     if(TRTHighTHitsRatio.IsAvailable()) TRTHighTHitsRatio.SetActive(active);
     if(TRTHighTOutliersRatio.IsAvailable()) TRTHighTOutliersRatio.SetActive(active);
     if(pixeldEdx.IsAvailable()) pixeldEdx.SetActive(active);
     if(nGoodHitsPixeldEdx.IsAvailable()) nGoodHitsPixeldEdx.SetActive(active);
     if(massPixeldEdx.IsAvailable()) massPixeldEdx.SetActive(active);
     if(likelihoodsPixeldEdx.IsAvailable()) likelihoodsPixeldEdx.SetActive(active);
     if(vertweight.IsAvailable()) vertweight.SetActive(active);
     if(trackd0beam.IsAvailable()) trackd0beam.SetActive(active);
     if(trackz0beam.IsAvailable()) trackz0beam.SetActive(active);
     if(tracksigd0beam.IsAvailable()) tracksigd0beam.SetActive(active);
     if(tracksigz0beam.IsAvailable()) tracksigz0beam.SetActive(active);
     if(trackd0pv.IsAvailable()) trackd0pv.SetActive(active);
     if(trackz0pv.IsAvailable()) trackz0pv.SetActive(active);
     if(tracksigd0pv.IsAvailable()) tracksigd0pv.SetActive(active);
     if(tracksigz0pv.IsAvailable()) tracksigz0pv.SetActive(active);
     if(trackd0pvunbiased.IsAvailable()) trackd0pvunbiased.SetActive(active);
     if(trackz0pvunbiased.IsAvailable()) trackz0pvunbiased.SetActive(active);
     if(tracksigd0pvunbiased.IsAvailable()) tracksigd0pvunbiased.SetActive(active);
     if(tracksigz0pvunbiased.IsAvailable()) tracksigz0pvunbiased.SetActive(active);
     if(theta_LM.IsAvailable()) theta_LM.SetActive(active);
     if(qoverp_LM.IsAvailable()) qoverp_LM.SetActive(active);
     if(hastrack.IsAvailable()) hastrack.SetActive(active);
     if(deltaEmax2.IsAvailable()) deltaEmax2.SetActive(active);
     if(calibHitsShowerDepth.IsAvailable()) calibHitsShowerDepth.SetActive(active);
     if(isIso.IsAvailable()) isIso.SetActive(active);
     if(EcellS0.IsAvailable()) EcellS0.SetActive(active);
     if(etacellS0.IsAvailable()) etacellS0.SetActive(active);
     if(Etcone40_ED_corrected.IsAvailable()) Etcone40_ED_corrected.SetActive(active);
     if(Etcone40_corrected.IsAvailable()) Etcone40_corrected.SetActive(active);
     if(topoEtcone20_corrected.IsAvailable()) topoEtcone20_corrected.SetActive(active);
     if(topoEtcone30_corrected.IsAvailable()) topoEtcone30_corrected.SetActive(active);
     if(topoEtcone40_corrected.IsAvailable()) topoEtcone40_corrected.SetActive(active);
     if(EF_dr.IsAvailable()) EF_dr.SetActive(active);
     if(L2_dr.IsAvailable()) L2_dr.SetActive(active);
     if(L1_dr.IsAvailable()) L1_dr.SetActive(active);
     if(L1_index.IsAvailable()) L1_index.SetActive(active);
     if(MET_n.IsAvailable()) MET_n.SetActive(active);
     if(MET_wpx.IsAvailable()) MET_wpx.SetActive(active);
     if(MET_wpy.IsAvailable()) MET_wpy.SetActive(active);
     if(MET_wet.IsAvailable()) MET_wet.SetActive(active);
     if(MET_statusWord.IsAvailable()) MET_statusWord.SetActive(active);
     if(MET_BDTMedium_n.IsAvailable()) MET_BDTMedium_n.SetActive(active);
     if(MET_BDTMedium_wpx.IsAvailable()) MET_BDTMedium_wpx.SetActive(active);
     if(MET_BDTMedium_wpy.IsAvailable()) MET_BDTMedium_wpy.SetActive(active);
     if(MET_BDTMedium_wet.IsAvailable()) MET_BDTMedium_wet.SetActive(active);
     if(MET_BDTMedium_statusWord.IsAvailable()) MET_BDTMedium_statusWord.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_n.IsAvailable()) MET_AntiKt4LCTopo_tightpp_n.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_wpx.IsAvailable()) MET_AntiKt4LCTopo_tightpp_wpx.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_wpy.IsAvailable()) MET_AntiKt4LCTopo_tightpp_wpy.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_wet.IsAvailable()) MET_AntiKt4LCTopo_tightpp_wet.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_statusWord.IsAvailable()) MET_AntiKt4LCTopo_tightpp_statusWord.SetActive(active);
    }
    else
    {
      n.SetActive(active);
      E.SetActive(active);
      Et.SetActive(active);
      pt.SetActive(active);
      m.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      px.SetActive(active);
      py.SetActive(active);
      pz.SetActive(active);
      charge.SetActive(active);
      author.SetActive(active);
      isEM.SetActive(active);
      isEMLoose.SetActive(active);
      isEMMedium.SetActive(active);
      isEMTight.SetActive(active);
      OQ.SetActive(active);
      isConv.SetActive(active);
      nConv.SetActive(active);
      nSingleTrackConv.SetActive(active);
      nDoubleTrackConv.SetActive(active);
      type.SetActive(active);
      origin.SetActive(active);
      typebkg.SetActive(active);
      originbkg.SetActive(active);
      truth_E.SetActive(active);
      truth_pt.SetActive(active);
      truth_eta.SetActive(active);
      truth_phi.SetActive(active);
      truth_type.SetActive(active);
      truth_status.SetActive(active);
      truth_barcode.SetActive(active);
      truth_mothertype.SetActive(active);
      truth_motherbarcode.SetActive(active);
      truth_hasHardBrem.SetActive(active);
      truth_index.SetActive(active);
      truth_matched.SetActive(active);
      mediumWithoutTrack.SetActive(active);
      mediumIsoWithoutTrack.SetActive(active);
      tightWithoutTrack.SetActive(active);
      tightIsoWithoutTrack.SetActive(active);
      loose.SetActive(active);
      looseIso.SetActive(active);
      medium.SetActive(active);
      mediumIso.SetActive(active);
      tight.SetActive(active);
      tightIso.SetActive(active);
      loosePP.SetActive(active);
      loosePPIso.SetActive(active);
      mediumPP.SetActive(active);
      mediumPPIso.SetActive(active);
      tightPP.SetActive(active);
      tightPPIso.SetActive(active);
      goodOQ.SetActive(active);
      Ethad.SetActive(active);
      Ethad1.SetActive(active);
      f1.SetActive(active);
      f1core.SetActive(active);
      Emins1.SetActive(active);
      fside.SetActive(active);
      Emax2.SetActive(active);
      ws3.SetActive(active);
      wstot.SetActive(active);
      emaxs1.SetActive(active);
      deltaEs.SetActive(active);
      E233.SetActive(active);
      E237.SetActive(active);
      E277.SetActive(active);
      weta2.SetActive(active);
      f3.SetActive(active);
      f3core.SetActive(active);
      rphiallcalo.SetActive(active);
      Etcone45.SetActive(active);
      Etcone15.SetActive(active);
      Etcone20.SetActive(active);
      Etcone25.SetActive(active);
      Etcone30.SetActive(active);
      Etcone35.SetActive(active);
      Etcone40.SetActive(active);
      ptcone20.SetActive(active);
      ptcone30.SetActive(active);
      ptcone40.SetActive(active);
      nucone20.SetActive(active);
      nucone30.SetActive(active);
      nucone40.SetActive(active);
      hasconv.SetActive(active);
      Rconv.SetActive(active);
      zconv.SetActive(active);
      pt1conv.SetActive(active);
      pt2conv.SetActive(active);
      ptconv.SetActive(active);
      pzconv.SetActive(active);
      pos7.SetActive(active);
      etacorrmag.SetActive(active);
      deltaeta1.SetActive(active);
      deltaeta2.SetActive(active);
      deltaphi2.SetActive(active);
      deltaphiRescaled.SetActive(active);
      deltaPhiFromLast.SetActive(active);
      deltaPhiRot.SetActive(active);
      expectHitInBLayer.SetActive(active);
      trackd0_physics.SetActive(active);
      etaSampling1.SetActive(active);
      reta.SetActive(active);
      rphi.SetActive(active);
      topoEtcone20.SetActive(active);
      topoEtcone30.SetActive(active);
      topoEtcone40.SetActive(active);
      zvertex.SetActive(active);
      errz.SetActive(active);
      etap.SetActive(active);
      depth.SetActive(active);
      refittedTrack_n.SetActive(active);
      refittedTrack_author.SetActive(active);
      refittedTrack_chi2.SetActive(active);
      refittedTrack_hasBrem.SetActive(active);
      refittedTrack_bremRadius.SetActive(active);
      refittedTrack_bremZ.SetActive(active);
      refittedTrack_bremRadiusErr.SetActive(active);
      refittedTrack_bremZErr.SetActive(active);
      refittedTrack_bremFitStatus.SetActive(active);
      refittedTrack_qoverp.SetActive(active);
      refittedTrack_d0.SetActive(active);
      refittedTrack_z0.SetActive(active);
      refittedTrack_theta.SetActive(active);
      refittedTrack_phi.SetActive(active);
      refittedTrack_LMqoverp.SetActive(active);
      refittedTrack_d0_wrtBL.SetActive(active);
      refittedTrack_z0_wrtBL.SetActive(active);
      refittedTrack_phi_wrtBL.SetActive(active);
      refittedTrack_theta_wrtBL.SetActive(active);
      refittedTrack_qoverp_wrtBL.SetActive(active);
      etas0.SetActive(active);
      phis0.SetActive(active);
      etas1.SetActive(active);
      phis1.SetActive(active);
      etas2.SetActive(active);
      phis2.SetActive(active);
      etas3.SetActive(active);
      phis3.SetActive(active);
      cl_E.SetActive(active);
      cl_pt.SetActive(active);
      cl_eta.SetActive(active);
      cl_phi.SetActive(active);
      firstEdens.SetActive(active);
      cellmaxfrac.SetActive(active);
      longitudinal.SetActive(active);
      secondlambda.SetActive(active);
      lateral.SetActive(active);
      secondR.SetActive(active);
      centerlambda.SetActive(active);
      trackd0.SetActive(active);
      trackz0.SetActive(active);
      trackphi.SetActive(active);
      tracktheta.SetActive(active);
      trackqoverp.SetActive(active);
      trackpt.SetActive(active);
      tracketa.SetActive(active);
      nBLHits.SetActive(active);
      nPixHits.SetActive(active);
      nSCTHits.SetActive(active);
      nTRTHits.SetActive(active);
      nTRTHighTHits.SetActive(active);
      nTRTXenonHits.SetActive(active);
      nPixHoles.SetActive(active);
      nSCTHoles.SetActive(active);
      nTRTHoles.SetActive(active);
      nPixelDeadSensors.SetActive(active);
      nSCTDeadSensors.SetActive(active);
      nBLSharedHits.SetActive(active);
      nPixSharedHits.SetActive(active);
      nSCTSharedHits.SetActive(active);
      nBLayerSplitHits.SetActive(active);
      nPixSplitHits.SetActive(active);
      nBLayerOutliers.SetActive(active);
      nPixelOutliers.SetActive(active);
      nSCTOutliers.SetActive(active);
      nTRTOutliers.SetActive(active);
      nTRTHighTOutliers.SetActive(active);
      nContribPixelLayers.SetActive(active);
      nGangedPixels.SetActive(active);
      nGangedFlaggedFakes.SetActive(active);
      nPixelSpoiltHits.SetActive(active);
      nSCTDoubleHoles.SetActive(active);
      nSCTSpoiltHits.SetActive(active);
      expectBLayerHit.SetActive(active);
      nSiHits.SetActive(active);
      TRTHighTHitsRatio.SetActive(active);
      TRTHighTOutliersRatio.SetActive(active);
      pixeldEdx.SetActive(active);
      nGoodHitsPixeldEdx.SetActive(active);
      massPixeldEdx.SetActive(active);
      likelihoodsPixeldEdx.SetActive(active);
      vertweight.SetActive(active);
      trackd0beam.SetActive(active);
      trackz0beam.SetActive(active);
      tracksigd0beam.SetActive(active);
      tracksigz0beam.SetActive(active);
      trackd0pv.SetActive(active);
      trackz0pv.SetActive(active);
      tracksigd0pv.SetActive(active);
      tracksigz0pv.SetActive(active);
      trackd0pvunbiased.SetActive(active);
      trackz0pvunbiased.SetActive(active);
      tracksigd0pvunbiased.SetActive(active);
      tracksigz0pvunbiased.SetActive(active);
      theta_LM.SetActive(active);
      qoverp_LM.SetActive(active);
      hastrack.SetActive(active);
      deltaEmax2.SetActive(active);
      calibHitsShowerDepth.SetActive(active);
      isIso.SetActive(active);
      EcellS0.SetActive(active);
      etacellS0.SetActive(active);
      Etcone40_ED_corrected.SetActive(active);
      Etcone40_corrected.SetActive(active);
      topoEtcone20_corrected.SetActive(active);
      topoEtcone30_corrected.SetActive(active);
      topoEtcone40_corrected.SetActive(active);
      EF_dr.SetActive(active);
      L2_dr.SetActive(active);
      L1_dr.SetActive(active);
      L1_index.SetActive(active);
      MET_n.SetActive(active);
      MET_wpx.SetActive(active);
      MET_wpy.SetActive(active);
      MET_wet.SetActive(active);
      MET_statusWord.SetActive(active);
      MET_BDTMedium_n.SetActive(active);
      MET_BDTMedium_wpx.SetActive(active);
      MET_BDTMedium_wpy.SetActive(active);
      MET_BDTMedium_wet.SetActive(active);
      MET_BDTMedium_statusWord.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_n.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_wpx.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_wpy.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_wet.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_statusWord.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void ElectronD3PDCollection::ReadAllActive()
  {
    if(n.IsActive()) n();
    if(E.IsActive()) E();
    if(Et.IsActive()) Et();
    if(pt.IsActive()) pt();
    if(m.IsActive()) m();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(px.IsActive()) px();
    if(py.IsActive()) py();
    if(pz.IsActive()) pz();
    if(charge.IsActive()) charge();
    if(author.IsActive()) author();
    if(isEM.IsActive()) isEM();
    if(isEMLoose.IsActive()) isEMLoose();
    if(isEMMedium.IsActive()) isEMMedium();
    if(isEMTight.IsActive()) isEMTight();
    if(OQ.IsActive()) OQ();
    if(isConv.IsActive()) isConv();
    if(nConv.IsActive()) nConv();
    if(nSingleTrackConv.IsActive()) nSingleTrackConv();
    if(nDoubleTrackConv.IsActive()) nDoubleTrackConv();
    if(type.IsActive()) type();
    if(origin.IsActive()) origin();
    if(typebkg.IsActive()) typebkg();
    if(originbkg.IsActive()) originbkg();
    if(truth_E.IsActive()) truth_E();
    if(truth_pt.IsActive()) truth_pt();
    if(truth_eta.IsActive()) truth_eta();
    if(truth_phi.IsActive()) truth_phi();
    if(truth_type.IsActive()) truth_type();
    if(truth_status.IsActive()) truth_status();
    if(truth_barcode.IsActive()) truth_barcode();
    if(truth_mothertype.IsActive()) truth_mothertype();
    if(truth_motherbarcode.IsActive()) truth_motherbarcode();
    if(truth_hasHardBrem.IsActive()) truth_hasHardBrem();
    if(truth_index.IsActive()) truth_index();
    if(truth_matched.IsActive()) truth_matched();
    if(mediumWithoutTrack.IsActive()) mediumWithoutTrack();
    if(mediumIsoWithoutTrack.IsActive()) mediumIsoWithoutTrack();
    if(tightWithoutTrack.IsActive()) tightWithoutTrack();
    if(tightIsoWithoutTrack.IsActive()) tightIsoWithoutTrack();
    if(loose.IsActive()) loose();
    if(looseIso.IsActive()) looseIso();
    if(medium.IsActive()) medium();
    if(mediumIso.IsActive()) mediumIso();
    if(tight.IsActive()) tight();
    if(tightIso.IsActive()) tightIso();
    if(loosePP.IsActive()) loosePP();
    if(loosePPIso.IsActive()) loosePPIso();
    if(mediumPP.IsActive()) mediumPP();
    if(mediumPPIso.IsActive()) mediumPPIso();
    if(tightPP.IsActive()) tightPP();
    if(tightPPIso.IsActive()) tightPPIso();
    if(goodOQ.IsActive()) goodOQ();
    if(Ethad.IsActive()) Ethad();
    if(Ethad1.IsActive()) Ethad1();
    if(f1.IsActive()) f1();
    if(f1core.IsActive()) f1core();
    if(Emins1.IsActive()) Emins1();
    if(fside.IsActive()) fside();
    if(Emax2.IsActive()) Emax2();
    if(ws3.IsActive()) ws3();
    if(wstot.IsActive()) wstot();
    if(emaxs1.IsActive()) emaxs1();
    if(deltaEs.IsActive()) deltaEs();
    if(E233.IsActive()) E233();
    if(E237.IsActive()) E237();
    if(E277.IsActive()) E277();
    if(weta2.IsActive()) weta2();
    if(f3.IsActive()) f3();
    if(f3core.IsActive()) f3core();
    if(rphiallcalo.IsActive()) rphiallcalo();
    if(Etcone45.IsActive()) Etcone45();
    if(Etcone15.IsActive()) Etcone15();
    if(Etcone20.IsActive()) Etcone20();
    if(Etcone25.IsActive()) Etcone25();
    if(Etcone30.IsActive()) Etcone30();
    if(Etcone35.IsActive()) Etcone35();
    if(Etcone40.IsActive()) Etcone40();
    if(ptcone20.IsActive()) ptcone20();
    if(ptcone30.IsActive()) ptcone30();
    if(ptcone40.IsActive()) ptcone40();
    if(nucone20.IsActive()) nucone20();
    if(nucone30.IsActive()) nucone30();
    if(nucone40.IsActive()) nucone40();
    if(hasconv.IsActive()) hasconv();
    if(Rconv.IsActive()) Rconv();
    if(zconv.IsActive()) zconv();
    if(pt1conv.IsActive()) pt1conv();
    if(pt2conv.IsActive()) pt2conv();
    if(ptconv.IsActive()) ptconv();
    if(pzconv.IsActive()) pzconv();
    if(pos7.IsActive()) pos7();
    if(etacorrmag.IsActive()) etacorrmag();
    if(deltaeta1.IsActive()) deltaeta1();
    if(deltaeta2.IsActive()) deltaeta2();
    if(deltaphi2.IsActive()) deltaphi2();
    if(deltaphiRescaled.IsActive()) deltaphiRescaled();
    if(deltaPhiFromLast.IsActive()) deltaPhiFromLast();
    if(deltaPhiRot.IsActive()) deltaPhiRot();
    if(expectHitInBLayer.IsActive()) expectHitInBLayer();
    if(trackd0_physics.IsActive()) trackd0_physics();
    if(etaSampling1.IsActive()) etaSampling1();
    if(reta.IsActive()) reta();
    if(rphi.IsActive()) rphi();
    if(topoEtcone20.IsActive()) topoEtcone20();
    if(topoEtcone30.IsActive()) topoEtcone30();
    if(topoEtcone40.IsActive()) topoEtcone40();
    if(zvertex.IsActive()) zvertex();
    if(errz.IsActive()) errz();
    if(etap.IsActive()) etap();
    if(depth.IsActive()) depth();
    if(refittedTrack_n.IsActive()) refittedTrack_n();
    if(refittedTrack_author.IsActive()) refittedTrack_author();
    if(refittedTrack_chi2.IsActive()) refittedTrack_chi2();
    if(refittedTrack_hasBrem.IsActive()) refittedTrack_hasBrem();
    if(refittedTrack_bremRadius.IsActive()) refittedTrack_bremRadius();
    if(refittedTrack_bremZ.IsActive()) refittedTrack_bremZ();
    if(refittedTrack_bremRadiusErr.IsActive()) refittedTrack_bremRadiusErr();
    if(refittedTrack_bremZErr.IsActive()) refittedTrack_bremZErr();
    if(refittedTrack_bremFitStatus.IsActive()) refittedTrack_bremFitStatus();
    if(refittedTrack_qoverp.IsActive()) refittedTrack_qoverp();
    if(refittedTrack_d0.IsActive()) refittedTrack_d0();
    if(refittedTrack_z0.IsActive()) refittedTrack_z0();
    if(refittedTrack_theta.IsActive()) refittedTrack_theta();
    if(refittedTrack_phi.IsActive()) refittedTrack_phi();
    if(refittedTrack_LMqoverp.IsActive()) refittedTrack_LMqoverp();
    if(refittedTrack_d0_wrtBL.IsActive()) refittedTrack_d0_wrtBL();
    if(refittedTrack_z0_wrtBL.IsActive()) refittedTrack_z0_wrtBL();
    if(refittedTrack_phi_wrtBL.IsActive()) refittedTrack_phi_wrtBL();
    if(refittedTrack_theta_wrtBL.IsActive()) refittedTrack_theta_wrtBL();
    if(refittedTrack_qoverp_wrtBL.IsActive()) refittedTrack_qoverp_wrtBL();
    if(etas0.IsActive()) etas0();
    if(phis0.IsActive()) phis0();
    if(etas1.IsActive()) etas1();
    if(phis1.IsActive()) phis1();
    if(etas2.IsActive()) etas2();
    if(phis2.IsActive()) phis2();
    if(etas3.IsActive()) etas3();
    if(phis3.IsActive()) phis3();
    if(cl_E.IsActive()) cl_E();
    if(cl_pt.IsActive()) cl_pt();
    if(cl_eta.IsActive()) cl_eta();
    if(cl_phi.IsActive()) cl_phi();
    if(firstEdens.IsActive()) firstEdens();
    if(cellmaxfrac.IsActive()) cellmaxfrac();
    if(longitudinal.IsActive()) longitudinal();
    if(secondlambda.IsActive()) secondlambda();
    if(lateral.IsActive()) lateral();
    if(secondR.IsActive()) secondR();
    if(centerlambda.IsActive()) centerlambda();
    if(trackd0.IsActive()) trackd0();
    if(trackz0.IsActive()) trackz0();
    if(trackphi.IsActive()) trackphi();
    if(tracktheta.IsActive()) tracktheta();
    if(trackqoverp.IsActive()) trackqoverp();
    if(trackpt.IsActive()) trackpt();
    if(tracketa.IsActive()) tracketa();
    if(nBLHits.IsActive()) nBLHits();
    if(nPixHits.IsActive()) nPixHits();
    if(nSCTHits.IsActive()) nSCTHits();
    if(nTRTHits.IsActive()) nTRTHits();
    if(nTRTHighTHits.IsActive()) nTRTHighTHits();
    if(nTRTXenonHits.IsActive()) nTRTXenonHits();
    if(nPixHoles.IsActive()) nPixHoles();
    if(nSCTHoles.IsActive()) nSCTHoles();
    if(nTRTHoles.IsActive()) nTRTHoles();
    if(nPixelDeadSensors.IsActive()) nPixelDeadSensors();
    if(nSCTDeadSensors.IsActive()) nSCTDeadSensors();
    if(nBLSharedHits.IsActive()) nBLSharedHits();
    if(nPixSharedHits.IsActive()) nPixSharedHits();
    if(nSCTSharedHits.IsActive()) nSCTSharedHits();
    if(nBLayerSplitHits.IsActive()) nBLayerSplitHits();
    if(nPixSplitHits.IsActive()) nPixSplitHits();
    if(nBLayerOutliers.IsActive()) nBLayerOutliers();
    if(nPixelOutliers.IsActive()) nPixelOutliers();
    if(nSCTOutliers.IsActive()) nSCTOutliers();
    if(nTRTOutliers.IsActive()) nTRTOutliers();
    if(nTRTHighTOutliers.IsActive()) nTRTHighTOutliers();
    if(nContribPixelLayers.IsActive()) nContribPixelLayers();
    if(nGangedPixels.IsActive()) nGangedPixels();
    if(nGangedFlaggedFakes.IsActive()) nGangedFlaggedFakes();
    if(nPixelSpoiltHits.IsActive()) nPixelSpoiltHits();
    if(nSCTDoubleHoles.IsActive()) nSCTDoubleHoles();
    if(nSCTSpoiltHits.IsActive()) nSCTSpoiltHits();
    if(expectBLayerHit.IsActive()) expectBLayerHit();
    if(nSiHits.IsActive()) nSiHits();
    if(TRTHighTHitsRatio.IsActive()) TRTHighTHitsRatio();
    if(TRTHighTOutliersRatio.IsActive()) TRTHighTOutliersRatio();
    if(pixeldEdx.IsActive()) pixeldEdx();
    if(nGoodHitsPixeldEdx.IsActive()) nGoodHitsPixeldEdx();
    if(massPixeldEdx.IsActive()) massPixeldEdx();
    if(likelihoodsPixeldEdx.IsActive()) likelihoodsPixeldEdx();
    if(vertweight.IsActive()) vertweight();
    if(trackd0beam.IsActive()) trackd0beam();
    if(trackz0beam.IsActive()) trackz0beam();
    if(tracksigd0beam.IsActive()) tracksigd0beam();
    if(tracksigz0beam.IsActive()) tracksigz0beam();
    if(trackd0pv.IsActive()) trackd0pv();
    if(trackz0pv.IsActive()) trackz0pv();
    if(tracksigd0pv.IsActive()) tracksigd0pv();
    if(tracksigz0pv.IsActive()) tracksigz0pv();
    if(trackd0pvunbiased.IsActive()) trackd0pvunbiased();
    if(trackz0pvunbiased.IsActive()) trackz0pvunbiased();
    if(tracksigd0pvunbiased.IsActive()) tracksigd0pvunbiased();
    if(tracksigz0pvunbiased.IsActive()) tracksigz0pvunbiased();
    if(theta_LM.IsActive()) theta_LM();
    if(qoverp_LM.IsActive()) qoverp_LM();
    if(hastrack.IsActive()) hastrack();
    if(deltaEmax2.IsActive()) deltaEmax2();
    if(calibHitsShowerDepth.IsActive()) calibHitsShowerDepth();
    if(isIso.IsActive()) isIso();
    if(EcellS0.IsActive()) EcellS0();
    if(etacellS0.IsActive()) etacellS0();
    if(Etcone40_ED_corrected.IsActive()) Etcone40_ED_corrected();
    if(Etcone40_corrected.IsActive()) Etcone40_corrected();
    if(topoEtcone20_corrected.IsActive()) topoEtcone20_corrected();
    if(topoEtcone30_corrected.IsActive()) topoEtcone30_corrected();
    if(topoEtcone40_corrected.IsActive()) topoEtcone40_corrected();
    if(EF_dr.IsActive()) EF_dr();
    if(L2_dr.IsActive()) L2_dr();
    if(L1_dr.IsActive()) L1_dr();
    if(L1_index.IsActive()) L1_index();
    if(MET_n.IsActive()) MET_n();
    if(MET_wpx.IsActive()) MET_wpx();
    if(MET_wpy.IsActive()) MET_wpy();
    if(MET_wet.IsActive()) MET_wet();
    if(MET_statusWord.IsActive()) MET_statusWord();
    if(MET_BDTMedium_n.IsActive()) MET_BDTMedium_n();
    if(MET_BDTMedium_wpx.IsActive()) MET_BDTMedium_wpx();
    if(MET_BDTMedium_wpy.IsActive()) MET_BDTMedium_wpy();
    if(MET_BDTMedium_wet.IsActive()) MET_BDTMedium_wet();
    if(MET_BDTMedium_statusWord.IsActive()) MET_BDTMedium_statusWord();
    if(MET_AntiKt4LCTopo_tightpp_n.IsActive()) MET_AntiKt4LCTopo_tightpp_n();
    if(MET_AntiKt4LCTopo_tightpp_wpx.IsActive()) MET_AntiKt4LCTopo_tightpp_wpx();
    if(MET_AntiKt4LCTopo_tightpp_wpy.IsActive()) MET_AntiKt4LCTopo_tightpp_wpy();
    if(MET_AntiKt4LCTopo_tightpp_wet.IsActive()) MET_AntiKt4LCTopo_tightpp_wet();
    if(MET_AntiKt4LCTopo_tightpp_statusWord.IsActive()) MET_AntiKt4LCTopo_tightpp_statusWord();
  }

} // namespace D3PDReader
#endif // D3PDREADER_ElectronD3PDCollection_CXX
