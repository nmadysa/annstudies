// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TrigEFMuonD3PDCollection_CXX
#define D3PDREADER_TrigEFMuonD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TrigEFMuonD3PDCollection.h"

ClassImp(D3PDReader::TrigEFMuonD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigEFMuonD3PDCollection::TrigEFMuonD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    EF_2mu10(prefix + "EF_2mu10",&master),
    EF_2mu10_MSonly_g10_loose(prefix + "EF_2mu10_MSonly_g10_loose",&master),
    EF_2mu10_MSonly_g10_loose_EMPTY(prefix + "EF_2mu10_MSonly_g10_loose_EMPTY",&master),
    EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO(prefix + "EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO",&master),
    EF_2mu13(prefix + "EF_2mu13",&master),
    EF_2mu13_Zmumu_IDTrkNoCut(prefix + "EF_2mu13_Zmumu_IDTrkNoCut",&master),
    EF_2mu13_l2muonSA(prefix + "EF_2mu13_l2muonSA",&master),
    EF_2mu15(prefix + "EF_2mu15",&master),
    EF_2mu4T(prefix + "EF_2mu4T",&master),
    EF_2mu4T_2e5_tight1(prefix + "EF_2mu4T_2e5_tight1",&master),
    EF_2mu4T_Bmumu(prefix + "EF_2mu4T_Bmumu",&master),
    EF_2mu4T_Bmumu_Barrel(prefix + "EF_2mu4T_Bmumu_Barrel",&master),
    EF_2mu4T_Bmumu_BarrelOnly(prefix + "EF_2mu4T_Bmumu_BarrelOnly",&master),
    EF_2mu4T_Bmumux(prefix + "EF_2mu4T_Bmumux",&master),
    EF_2mu4T_Bmumux_Barrel(prefix + "EF_2mu4T_Bmumux_Barrel",&master),
    EF_2mu4T_Bmumux_BarrelOnly(prefix + "EF_2mu4T_Bmumux_BarrelOnly",&master),
    EF_2mu4T_DiMu(prefix + "EF_2mu4T_DiMu",&master),
    EF_2mu4T_DiMu_Barrel(prefix + "EF_2mu4T_DiMu_Barrel",&master),
    EF_2mu4T_DiMu_BarrelOnly(prefix + "EF_2mu4T_DiMu_BarrelOnly",&master),
    EF_2mu4T_DiMu_L2StarB(prefix + "EF_2mu4T_DiMu_L2StarB",&master),
    EF_2mu4T_DiMu_L2StarC(prefix + "EF_2mu4T_DiMu_L2StarC",&master),
    EF_2mu4T_DiMu_e5_tight1(prefix + "EF_2mu4T_DiMu_e5_tight1",&master),
    EF_2mu4T_DiMu_l2muonSA(prefix + "EF_2mu4T_DiMu_l2muonSA",&master),
    EF_2mu4T_DiMu_noVtx_noOS(prefix + "EF_2mu4T_DiMu_noVtx_noOS",&master),
    EF_2mu4T_Jpsimumu(prefix + "EF_2mu4T_Jpsimumu",&master),
    EF_2mu4T_Jpsimumu_Barrel(prefix + "EF_2mu4T_Jpsimumu_Barrel",&master),
    EF_2mu4T_Jpsimumu_BarrelOnly(prefix + "EF_2mu4T_Jpsimumu_BarrelOnly",&master),
    EF_2mu4T_Jpsimumu_IDTrkNoCut(prefix + "EF_2mu4T_Jpsimumu_IDTrkNoCut",&master),
    EF_2mu4T_Upsimumu(prefix + "EF_2mu4T_Upsimumu",&master),
    EF_2mu4T_Upsimumu_Barrel(prefix + "EF_2mu4T_Upsimumu_Barrel",&master),
    EF_2mu4T_Upsimumu_BarrelOnly(prefix + "EF_2mu4T_Upsimumu_BarrelOnly",&master),
    EF_2mu4T_xe50_tclcw(prefix + "EF_2mu4T_xe50_tclcw",&master),
    EF_2mu4T_xe60(prefix + "EF_2mu4T_xe60",&master),
    EF_2mu4T_xe60_tclcw(prefix + "EF_2mu4T_xe60_tclcw",&master),
    EF_2mu6(prefix + "EF_2mu6",&master),
    EF_2mu6_Bmumu(prefix + "EF_2mu6_Bmumu",&master),
    EF_2mu6_Bmumux(prefix + "EF_2mu6_Bmumux",&master),
    EF_2mu6_DiMu(prefix + "EF_2mu6_DiMu",&master),
    EF_2mu6_DiMu_DY20(prefix + "EF_2mu6_DiMu_DY20",&master),
    EF_2mu6_DiMu_DY25(prefix + "EF_2mu6_DiMu_DY25",&master),
    EF_2mu6_DiMu_noVtx_noOS(prefix + "EF_2mu6_DiMu_noVtx_noOS",&master),
    EF_2mu6_Jpsimumu(prefix + "EF_2mu6_Jpsimumu",&master),
    EF_2mu6_Upsimumu(prefix + "EF_2mu6_Upsimumu",&master),
    EF_2mu6i_DiMu_DY(prefix + "EF_2mu6i_DiMu_DY",&master),
    EF_2mu6i_DiMu_DY_2j25_a4tchad(prefix + "EF_2mu6i_DiMu_DY_2j25_a4tchad",&master),
    EF_2mu6i_DiMu_DY_noVtx_noOS(prefix + "EF_2mu6i_DiMu_DY_noVtx_noOS",&master),
    EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad(prefix + "EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad",&master),
    EF_2mu8_EFxe30(prefix + "EF_2mu8_EFxe30",&master),
    EF_2mu8_EFxe30_tclcw(prefix + "EF_2mu8_EFxe30_tclcw",&master),
    EF_mu10(prefix + "EF_mu10",&master),
    EF_mu10_Jpsimumu(prefix + "EF_mu10_Jpsimumu",&master),
    EF_mu10_MSonly(prefix + "EF_mu10_MSonly",&master),
    EF_mu10_Upsimumu_tight_FS(prefix + "EF_mu10_Upsimumu_tight_FS",&master),
    EF_mu10i_g10_loose(prefix + "EF_mu10i_g10_loose",&master),
    EF_mu10i_g10_loose_TauMass(prefix + "EF_mu10i_g10_loose_TauMass",&master),
    EF_mu10i_g10_medium(prefix + "EF_mu10i_g10_medium",&master),
    EF_mu10i_g10_medium_TauMass(prefix + "EF_mu10i_g10_medium_TauMass",&master),
    EF_mu10i_loose_g12Tvh_loose(prefix + "EF_mu10i_loose_g12Tvh_loose",&master),
    EF_mu10i_loose_g12Tvh_loose_TauMass(prefix + "EF_mu10i_loose_g12Tvh_loose_TauMass",&master),
    EF_mu10i_loose_g12Tvh_medium(prefix + "EF_mu10i_loose_g12Tvh_medium",&master),
    EF_mu10i_loose_g12Tvh_medium_TauMass(prefix + "EF_mu10i_loose_g12Tvh_medium_TauMass",&master),
    EF_mu11_empty_NoAlg(prefix + "EF_mu11_empty_NoAlg",&master),
    EF_mu13(prefix + "EF_mu13",&master),
    EF_mu15(prefix + "EF_mu15",&master),
    EF_mu18(prefix + "EF_mu18",&master),
    EF_mu18_2g10_loose(prefix + "EF_mu18_2g10_loose",&master),
    EF_mu18_2g10_medium(prefix + "EF_mu18_2g10_medium",&master),
    EF_mu18_2g15_loose(prefix + "EF_mu18_2g15_loose",&master),
    EF_mu18_IDTrkNoCut_tight(prefix + "EF_mu18_IDTrkNoCut_tight",&master),
    EF_mu18_g20vh_loose(prefix + "EF_mu18_g20vh_loose",&master),
    EF_mu18_medium(prefix + "EF_mu18_medium",&master),
    EF_mu18_tight(prefix + "EF_mu18_tight",&master),
    EF_mu18_tight_2mu4_EFFS(prefix + "EF_mu18_tight_2mu4_EFFS",&master),
    EF_mu18_tight_e7_medium1(prefix + "EF_mu18_tight_e7_medium1",&master),
    EF_mu18_tight_mu8_EFFS(prefix + "EF_mu18_tight_mu8_EFFS",&master),
    EF_mu18i4_tight(prefix + "EF_mu18i4_tight",&master),
    EF_mu18it_tight(prefix + "EF_mu18it_tight",&master),
    EF_mu20i_tight_g5_loose(prefix + "EF_mu20i_tight_g5_loose",&master),
    EF_mu20i_tight_g5_loose_TauMass(prefix + "EF_mu20i_tight_g5_loose_TauMass",&master),
    EF_mu20i_tight_g5_medium(prefix + "EF_mu20i_tight_g5_medium",&master),
    EF_mu20i_tight_g5_medium_TauMass(prefix + "EF_mu20i_tight_g5_medium_TauMass",&master),
    EF_mu20it_tight(prefix + "EF_mu20it_tight",&master),
    EF_mu22_IDTrkNoCut_tight(prefix + "EF_mu22_IDTrkNoCut_tight",&master),
    EF_mu24(prefix + "EF_mu24",&master),
    EF_mu24_g20vh_loose(prefix + "EF_mu24_g20vh_loose",&master),
    EF_mu24_g20vh_medium(prefix + "EF_mu24_g20vh_medium",&master),
    EF_mu24_j65_a4tchad(prefix + "EF_mu24_j65_a4tchad",&master),
    EF_mu24_j65_a4tchad_EFxe40(prefix + "EF_mu24_j65_a4tchad_EFxe40",&master),
    EF_mu24_j65_a4tchad_EFxe40_tclcw(prefix + "EF_mu24_j65_a4tchad_EFxe40_tclcw",&master),
    EF_mu24_j65_a4tchad_EFxe50_tclcw(prefix + "EF_mu24_j65_a4tchad_EFxe50_tclcw",&master),
    EF_mu24_j65_a4tchad_EFxe60_tclcw(prefix + "EF_mu24_j65_a4tchad_EFxe60_tclcw",&master),
    EF_mu24_medium(prefix + "EF_mu24_medium",&master),
    EF_mu24_muCombTag_NoEF_tight(prefix + "EF_mu24_muCombTag_NoEF_tight",&master),
    EF_mu24_tight(prefix + "EF_mu24_tight",&master),
    EF_mu24_tight_2j35_a4tchad(prefix + "EF_mu24_tight_2j35_a4tchad",&master),
    EF_mu24_tight_3j35_a4tchad(prefix + "EF_mu24_tight_3j35_a4tchad",&master),
    EF_mu24_tight_4j35_a4tchad(prefix + "EF_mu24_tight_4j35_a4tchad",&master),
    EF_mu24_tight_EFxe40(prefix + "EF_mu24_tight_EFxe40",&master),
    EF_mu24_tight_L2StarB(prefix + "EF_mu24_tight_L2StarB",&master),
    EF_mu24_tight_L2StarC(prefix + "EF_mu24_tight_L2StarC",&master),
    EF_mu24_tight_MG(prefix + "EF_mu24_tight_MG",&master),
    EF_mu24_tight_MuonEF(prefix + "EF_mu24_tight_MuonEF",&master),
    EF_mu24_tight_b35_mediumEF_j35_a4tchad(prefix + "EF_mu24_tight_b35_mediumEF_j35_a4tchad",&master),
    EF_mu24_tight_mu6_EFFS(prefix + "EF_mu24_tight_mu6_EFFS",&master),
    EF_mu24i_tight(prefix + "EF_mu24i_tight",&master),
    EF_mu24i_tight_MG(prefix + "EF_mu24i_tight_MG",&master),
    EF_mu24i_tight_MuonEF(prefix + "EF_mu24i_tight_MuonEF",&master),
    EF_mu24i_tight_l2muonSA(prefix + "EF_mu24i_tight_l2muonSA",&master),
    EF_mu36_tight(prefix + "EF_mu36_tight",&master),
    EF_mu40_MSonly_barrel_tight(prefix + "EF_mu40_MSonly_barrel_tight",&master),
    EF_mu40_muCombTag_NoEF(prefix + "EF_mu40_muCombTag_NoEF",&master),
    EF_mu40_slow_outOfTime_tight(prefix + "EF_mu40_slow_outOfTime_tight",&master),
    EF_mu40_slow_tight(prefix + "EF_mu40_slow_tight",&master),
    EF_mu40_tight(prefix + "EF_mu40_tight",&master),
    EF_mu4T(prefix + "EF_mu4T",&master),
    EF_mu4T_Trk_Jpsi(prefix + "EF_mu4T_Trk_Jpsi",&master),
    EF_mu4T_cosmic(prefix + "EF_mu4T_cosmic",&master),
    EF_mu4T_j110_a4tchad_L2FS_matched(prefix + "EF_mu4T_j110_a4tchad_L2FS_matched",&master),
    EF_mu4T_j110_a4tchad_matched(prefix + "EF_mu4T_j110_a4tchad_matched",&master),
    EF_mu4T_j145_a4tchad_L2FS_matched(prefix + "EF_mu4T_j145_a4tchad_L2FS_matched",&master),
    EF_mu4T_j145_a4tchad_matched(prefix + "EF_mu4T_j145_a4tchad_matched",&master),
    EF_mu4T_j15_a4tchad_matched(prefix + "EF_mu4T_j15_a4tchad_matched",&master),
    EF_mu4T_j15_a4tchad_matchedZ(prefix + "EF_mu4T_j15_a4tchad_matchedZ",&master),
    EF_mu4T_j180_a4tchad_L2FS_matched(prefix + "EF_mu4T_j180_a4tchad_L2FS_matched",&master),
    EF_mu4T_j180_a4tchad_matched(prefix + "EF_mu4T_j180_a4tchad_matched",&master),
    EF_mu4T_j220_a4tchad_L2FS_matched(prefix + "EF_mu4T_j220_a4tchad_L2FS_matched",&master),
    EF_mu4T_j220_a4tchad_matched(prefix + "EF_mu4T_j220_a4tchad_matched",&master),
    EF_mu4T_j25_a4tchad_matched(prefix + "EF_mu4T_j25_a4tchad_matched",&master),
    EF_mu4T_j25_a4tchad_matchedZ(prefix + "EF_mu4T_j25_a4tchad_matchedZ",&master),
    EF_mu4T_j280_a4tchad_L2FS_matched(prefix + "EF_mu4T_j280_a4tchad_L2FS_matched",&master),
    EF_mu4T_j280_a4tchad_matched(prefix + "EF_mu4T_j280_a4tchad_matched",&master),
    EF_mu4T_j35_a4tchad_matched(prefix + "EF_mu4T_j35_a4tchad_matched",&master),
    EF_mu4T_j35_a4tchad_matchedZ(prefix + "EF_mu4T_j35_a4tchad_matchedZ",&master),
    EF_mu4T_j360_a4tchad_L2FS_matched(prefix + "EF_mu4T_j360_a4tchad_L2FS_matched",&master),
    EF_mu4T_j360_a4tchad_matched(prefix + "EF_mu4T_j360_a4tchad_matched",&master),
    EF_mu4T_j45_a4tchad_L2FS_matched(prefix + "EF_mu4T_j45_a4tchad_L2FS_matched",&master),
    EF_mu4T_j45_a4tchad_L2FS_matchedZ(prefix + "EF_mu4T_j45_a4tchad_L2FS_matchedZ",&master),
    EF_mu4T_j45_a4tchad_matched(prefix + "EF_mu4T_j45_a4tchad_matched",&master),
    EF_mu4T_j45_a4tchad_matchedZ(prefix + "EF_mu4T_j45_a4tchad_matchedZ",&master),
    EF_mu4T_j55_a4tchad_L2FS_matched(prefix + "EF_mu4T_j55_a4tchad_L2FS_matched",&master),
    EF_mu4T_j55_a4tchad_L2FS_matchedZ(prefix + "EF_mu4T_j55_a4tchad_L2FS_matchedZ",&master),
    EF_mu4T_j55_a4tchad_matched(prefix + "EF_mu4T_j55_a4tchad_matched",&master),
    EF_mu4T_j55_a4tchad_matchedZ(prefix + "EF_mu4T_j55_a4tchad_matchedZ",&master),
    EF_mu4T_j65_a4tchad_L2FS_matched(prefix + "EF_mu4T_j65_a4tchad_L2FS_matched",&master),
    EF_mu4T_j65_a4tchad_matched(prefix + "EF_mu4T_j65_a4tchad_matched",&master),
    EF_mu4T_j65_a4tchad_xe60_tclcw_loose(prefix + "EF_mu4T_j65_a4tchad_xe60_tclcw_loose",&master),
    EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose(prefix + "EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose",&master),
    EF_mu4T_j80_a4tchad_L2FS_matched(prefix + "EF_mu4T_j80_a4tchad_L2FS_matched",&master),
    EF_mu4T_j80_a4tchad_matched(prefix + "EF_mu4T_j80_a4tchad_matched",&master),
    EF_mu4Ti_g20Tvh_loose(prefix + "EF_mu4Ti_g20Tvh_loose",&master),
    EF_mu4Ti_g20Tvh_loose_TauMass(prefix + "EF_mu4Ti_g20Tvh_loose_TauMass",&master),
    EF_mu4Ti_g20Tvh_medium(prefix + "EF_mu4Ti_g20Tvh_medium",&master),
    EF_mu4Ti_g20Tvh_medium_TauMass(prefix + "EF_mu4Ti_g20Tvh_medium_TauMass",&master),
    EF_mu4Tmu6_Bmumu(prefix + "EF_mu4Tmu6_Bmumu",&master),
    EF_mu4Tmu6_Bmumu_Barrel(prefix + "EF_mu4Tmu6_Bmumu_Barrel",&master),
    EF_mu4Tmu6_Bmumux(prefix + "EF_mu4Tmu6_Bmumux",&master),
    EF_mu4Tmu6_Bmumux_Barrel(prefix + "EF_mu4Tmu6_Bmumux_Barrel",&master),
    EF_mu4Tmu6_DiMu(prefix + "EF_mu4Tmu6_DiMu",&master),
    EF_mu4Tmu6_DiMu_Barrel(prefix + "EF_mu4Tmu6_DiMu_Barrel",&master),
    EF_mu4Tmu6_DiMu_noVtx_noOS(prefix + "EF_mu4Tmu6_DiMu_noVtx_noOS",&master),
    EF_mu4Tmu6_Jpsimumu(prefix + "EF_mu4Tmu6_Jpsimumu",&master),
    EF_mu4Tmu6_Jpsimumu_Barrel(prefix + "EF_mu4Tmu6_Jpsimumu_Barrel",&master),
    EF_mu4Tmu6_Jpsimumu_IDTrkNoCut(prefix + "EF_mu4Tmu6_Jpsimumu_IDTrkNoCut",&master),
    EF_mu4Tmu6_Upsimumu(prefix + "EF_mu4Tmu6_Upsimumu",&master),
    EF_mu4Tmu6_Upsimumu_Barrel(prefix + "EF_mu4Tmu6_Upsimumu_Barrel",&master),
    EF_mu4_L1MU11_MSonly_cosmic(prefix + "EF_mu4_L1MU11_MSonly_cosmic",&master),
    EF_mu4_L1MU11_cosmic(prefix + "EF_mu4_L1MU11_cosmic",&master),
    EF_mu4_empty_NoAlg(prefix + "EF_mu4_empty_NoAlg",&master),
    EF_mu4_firstempty_NoAlg(prefix + "EF_mu4_firstempty_NoAlg",&master),
    EF_mu4_unpaired_iso_NoAlg(prefix + "EF_mu4_unpaired_iso_NoAlg",&master),
    EF_mu50_MSonly_barrel_tight(prefix + "EF_mu50_MSonly_barrel_tight",&master),
    EF_mu6(prefix + "EF_mu6",&master),
    EF_mu60_slow_outOfTime_tight1(prefix + "EF_mu60_slow_outOfTime_tight1",&master),
    EF_mu60_slow_tight1(prefix + "EF_mu60_slow_tight1",&master),
    EF_mu6_Jpsimumu_tight(prefix + "EF_mu6_Jpsimumu_tight",&master),
    EF_mu6_MSonly(prefix + "EF_mu6_MSonly",&master),
    EF_mu6_Trk_Jpsi_loose(prefix + "EF_mu6_Trk_Jpsi_loose",&master),
    EF_mu6i(prefix + "EF_mu6i",&master),
    EF_mu8(prefix + "EF_mu8",&master),
    EF_mu8_4j45_a4tchad_L2FS(prefix + "EF_mu8_4j45_a4tchad_L2FS",&master),
    n(prefix + "n",&master),
    track_n(prefix + "track_n",&master),
    track_MuonType(prefix + "track_MuonType",&master),
    track_MS_pt(prefix + "track_MS_pt",&master),
    track_MS_eta(prefix + "track_MS_eta",&master),
    track_MS_phi(prefix + "track_MS_phi",&master),
    track_MS_charge(prefix + "track_MS_charge",&master),
    track_MS_d0(prefix + "track_MS_d0",&master),
    track_MS_z0(prefix + "track_MS_z0",&master),
    track_MS_chi2(prefix + "track_MS_chi2",&master),
    track_MS_chi2prob(prefix + "track_MS_chi2prob",&master),
    track_MS_posX(prefix + "track_MS_posX",&master),
    track_MS_posY(prefix + "track_MS_posY",&master),
    track_MS_posZ(prefix + "track_MS_posZ",&master),
    track_MS_hasMS(prefix + "track_MS_hasMS",&master),
    track_SA_pt(prefix + "track_SA_pt",&master),
    track_SA_eta(prefix + "track_SA_eta",&master),
    track_SA_phi(prefix + "track_SA_phi",&master),
    track_SA_charge(prefix + "track_SA_charge",&master),
    track_SA_d0(prefix + "track_SA_d0",&master),
    track_SA_z0(prefix + "track_SA_z0",&master),
    track_SA_chi2(prefix + "track_SA_chi2",&master),
    track_SA_chi2prob(prefix + "track_SA_chi2prob",&master),
    track_SA_posX(prefix + "track_SA_posX",&master),
    track_SA_posY(prefix + "track_SA_posY",&master),
    track_SA_posZ(prefix + "track_SA_posZ",&master),
    track_SA_hasSA(prefix + "track_SA_hasSA",&master),
    track_CB_pt(prefix + "track_CB_pt",&master),
    track_CB_eta(prefix + "track_CB_eta",&master),
    track_CB_phi(prefix + "track_CB_phi",&master),
    track_CB_charge(prefix + "track_CB_charge",&master),
    track_CB_d0(prefix + "track_CB_d0",&master),
    track_CB_z0(prefix + "track_CB_z0",&master),
    track_CB_chi2(prefix + "track_CB_chi2",&master),
    track_CB_chi2prob(prefix + "track_CB_chi2prob",&master),
    track_CB_posX(prefix + "track_CB_posX",&master),
    track_CB_posY(prefix + "track_CB_posY",&master),
    track_CB_posZ(prefix + "track_CB_posZ",&master),
    track_CB_matchChi2(prefix + "track_CB_matchChi2",&master),
    track_CB_hasCB(prefix + "track_CB_hasCB",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigEFMuonD3PDCollection::TrigEFMuonD3PDCollection(const std::string& prefix):
    TObject(),
    EF_2mu10(prefix + "EF_2mu10",0),
    EF_2mu10_MSonly_g10_loose(prefix + "EF_2mu10_MSonly_g10_loose",0),
    EF_2mu10_MSonly_g10_loose_EMPTY(prefix + "EF_2mu10_MSonly_g10_loose_EMPTY",0),
    EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO(prefix + "EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO",0),
    EF_2mu13(prefix + "EF_2mu13",0),
    EF_2mu13_Zmumu_IDTrkNoCut(prefix + "EF_2mu13_Zmumu_IDTrkNoCut",0),
    EF_2mu13_l2muonSA(prefix + "EF_2mu13_l2muonSA",0),
    EF_2mu15(prefix + "EF_2mu15",0),
    EF_2mu4T(prefix + "EF_2mu4T",0),
    EF_2mu4T_2e5_tight1(prefix + "EF_2mu4T_2e5_tight1",0),
    EF_2mu4T_Bmumu(prefix + "EF_2mu4T_Bmumu",0),
    EF_2mu4T_Bmumu_Barrel(prefix + "EF_2mu4T_Bmumu_Barrel",0),
    EF_2mu4T_Bmumu_BarrelOnly(prefix + "EF_2mu4T_Bmumu_BarrelOnly",0),
    EF_2mu4T_Bmumux(prefix + "EF_2mu4T_Bmumux",0),
    EF_2mu4T_Bmumux_Barrel(prefix + "EF_2mu4T_Bmumux_Barrel",0),
    EF_2mu4T_Bmumux_BarrelOnly(prefix + "EF_2mu4T_Bmumux_BarrelOnly",0),
    EF_2mu4T_DiMu(prefix + "EF_2mu4T_DiMu",0),
    EF_2mu4T_DiMu_Barrel(prefix + "EF_2mu4T_DiMu_Barrel",0),
    EF_2mu4T_DiMu_BarrelOnly(prefix + "EF_2mu4T_DiMu_BarrelOnly",0),
    EF_2mu4T_DiMu_L2StarB(prefix + "EF_2mu4T_DiMu_L2StarB",0),
    EF_2mu4T_DiMu_L2StarC(prefix + "EF_2mu4T_DiMu_L2StarC",0),
    EF_2mu4T_DiMu_e5_tight1(prefix + "EF_2mu4T_DiMu_e5_tight1",0),
    EF_2mu4T_DiMu_l2muonSA(prefix + "EF_2mu4T_DiMu_l2muonSA",0),
    EF_2mu4T_DiMu_noVtx_noOS(prefix + "EF_2mu4T_DiMu_noVtx_noOS",0),
    EF_2mu4T_Jpsimumu(prefix + "EF_2mu4T_Jpsimumu",0),
    EF_2mu4T_Jpsimumu_Barrel(prefix + "EF_2mu4T_Jpsimumu_Barrel",0),
    EF_2mu4T_Jpsimumu_BarrelOnly(prefix + "EF_2mu4T_Jpsimumu_BarrelOnly",0),
    EF_2mu4T_Jpsimumu_IDTrkNoCut(prefix + "EF_2mu4T_Jpsimumu_IDTrkNoCut",0),
    EF_2mu4T_Upsimumu(prefix + "EF_2mu4T_Upsimumu",0),
    EF_2mu4T_Upsimumu_Barrel(prefix + "EF_2mu4T_Upsimumu_Barrel",0),
    EF_2mu4T_Upsimumu_BarrelOnly(prefix + "EF_2mu4T_Upsimumu_BarrelOnly",0),
    EF_2mu4T_xe50_tclcw(prefix + "EF_2mu4T_xe50_tclcw",0),
    EF_2mu4T_xe60(prefix + "EF_2mu4T_xe60",0),
    EF_2mu4T_xe60_tclcw(prefix + "EF_2mu4T_xe60_tclcw",0),
    EF_2mu6(prefix + "EF_2mu6",0),
    EF_2mu6_Bmumu(prefix + "EF_2mu6_Bmumu",0),
    EF_2mu6_Bmumux(prefix + "EF_2mu6_Bmumux",0),
    EF_2mu6_DiMu(prefix + "EF_2mu6_DiMu",0),
    EF_2mu6_DiMu_DY20(prefix + "EF_2mu6_DiMu_DY20",0),
    EF_2mu6_DiMu_DY25(prefix + "EF_2mu6_DiMu_DY25",0),
    EF_2mu6_DiMu_noVtx_noOS(prefix + "EF_2mu6_DiMu_noVtx_noOS",0),
    EF_2mu6_Jpsimumu(prefix + "EF_2mu6_Jpsimumu",0),
    EF_2mu6_Upsimumu(prefix + "EF_2mu6_Upsimumu",0),
    EF_2mu6i_DiMu_DY(prefix + "EF_2mu6i_DiMu_DY",0),
    EF_2mu6i_DiMu_DY_2j25_a4tchad(prefix + "EF_2mu6i_DiMu_DY_2j25_a4tchad",0),
    EF_2mu6i_DiMu_DY_noVtx_noOS(prefix + "EF_2mu6i_DiMu_DY_noVtx_noOS",0),
    EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad(prefix + "EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad",0),
    EF_2mu8_EFxe30(prefix + "EF_2mu8_EFxe30",0),
    EF_2mu8_EFxe30_tclcw(prefix + "EF_2mu8_EFxe30_tclcw",0),
    EF_mu10(prefix + "EF_mu10",0),
    EF_mu10_Jpsimumu(prefix + "EF_mu10_Jpsimumu",0),
    EF_mu10_MSonly(prefix + "EF_mu10_MSonly",0),
    EF_mu10_Upsimumu_tight_FS(prefix + "EF_mu10_Upsimumu_tight_FS",0),
    EF_mu10i_g10_loose(prefix + "EF_mu10i_g10_loose",0),
    EF_mu10i_g10_loose_TauMass(prefix + "EF_mu10i_g10_loose_TauMass",0),
    EF_mu10i_g10_medium(prefix + "EF_mu10i_g10_medium",0),
    EF_mu10i_g10_medium_TauMass(prefix + "EF_mu10i_g10_medium_TauMass",0),
    EF_mu10i_loose_g12Tvh_loose(prefix + "EF_mu10i_loose_g12Tvh_loose",0),
    EF_mu10i_loose_g12Tvh_loose_TauMass(prefix + "EF_mu10i_loose_g12Tvh_loose_TauMass",0),
    EF_mu10i_loose_g12Tvh_medium(prefix + "EF_mu10i_loose_g12Tvh_medium",0),
    EF_mu10i_loose_g12Tvh_medium_TauMass(prefix + "EF_mu10i_loose_g12Tvh_medium_TauMass",0),
    EF_mu11_empty_NoAlg(prefix + "EF_mu11_empty_NoAlg",0),
    EF_mu13(prefix + "EF_mu13",0),
    EF_mu15(prefix + "EF_mu15",0),
    EF_mu18(prefix + "EF_mu18",0),
    EF_mu18_2g10_loose(prefix + "EF_mu18_2g10_loose",0),
    EF_mu18_2g10_medium(prefix + "EF_mu18_2g10_medium",0),
    EF_mu18_2g15_loose(prefix + "EF_mu18_2g15_loose",0),
    EF_mu18_IDTrkNoCut_tight(prefix + "EF_mu18_IDTrkNoCut_tight",0),
    EF_mu18_g20vh_loose(prefix + "EF_mu18_g20vh_loose",0),
    EF_mu18_medium(prefix + "EF_mu18_medium",0),
    EF_mu18_tight(prefix + "EF_mu18_tight",0),
    EF_mu18_tight_2mu4_EFFS(prefix + "EF_mu18_tight_2mu4_EFFS",0),
    EF_mu18_tight_e7_medium1(prefix + "EF_mu18_tight_e7_medium1",0),
    EF_mu18_tight_mu8_EFFS(prefix + "EF_mu18_tight_mu8_EFFS",0),
    EF_mu18i4_tight(prefix + "EF_mu18i4_tight",0),
    EF_mu18it_tight(prefix + "EF_mu18it_tight",0),
    EF_mu20i_tight_g5_loose(prefix + "EF_mu20i_tight_g5_loose",0),
    EF_mu20i_tight_g5_loose_TauMass(prefix + "EF_mu20i_tight_g5_loose_TauMass",0),
    EF_mu20i_tight_g5_medium(prefix + "EF_mu20i_tight_g5_medium",0),
    EF_mu20i_tight_g5_medium_TauMass(prefix + "EF_mu20i_tight_g5_medium_TauMass",0),
    EF_mu20it_tight(prefix + "EF_mu20it_tight",0),
    EF_mu22_IDTrkNoCut_tight(prefix + "EF_mu22_IDTrkNoCut_tight",0),
    EF_mu24(prefix + "EF_mu24",0),
    EF_mu24_g20vh_loose(prefix + "EF_mu24_g20vh_loose",0),
    EF_mu24_g20vh_medium(prefix + "EF_mu24_g20vh_medium",0),
    EF_mu24_j65_a4tchad(prefix + "EF_mu24_j65_a4tchad",0),
    EF_mu24_j65_a4tchad_EFxe40(prefix + "EF_mu24_j65_a4tchad_EFxe40",0),
    EF_mu24_j65_a4tchad_EFxe40_tclcw(prefix + "EF_mu24_j65_a4tchad_EFxe40_tclcw",0),
    EF_mu24_j65_a4tchad_EFxe50_tclcw(prefix + "EF_mu24_j65_a4tchad_EFxe50_tclcw",0),
    EF_mu24_j65_a4tchad_EFxe60_tclcw(prefix + "EF_mu24_j65_a4tchad_EFxe60_tclcw",0),
    EF_mu24_medium(prefix + "EF_mu24_medium",0),
    EF_mu24_muCombTag_NoEF_tight(prefix + "EF_mu24_muCombTag_NoEF_tight",0),
    EF_mu24_tight(prefix + "EF_mu24_tight",0),
    EF_mu24_tight_2j35_a4tchad(prefix + "EF_mu24_tight_2j35_a4tchad",0),
    EF_mu24_tight_3j35_a4tchad(prefix + "EF_mu24_tight_3j35_a4tchad",0),
    EF_mu24_tight_4j35_a4tchad(prefix + "EF_mu24_tight_4j35_a4tchad",0),
    EF_mu24_tight_EFxe40(prefix + "EF_mu24_tight_EFxe40",0),
    EF_mu24_tight_L2StarB(prefix + "EF_mu24_tight_L2StarB",0),
    EF_mu24_tight_L2StarC(prefix + "EF_mu24_tight_L2StarC",0),
    EF_mu24_tight_MG(prefix + "EF_mu24_tight_MG",0),
    EF_mu24_tight_MuonEF(prefix + "EF_mu24_tight_MuonEF",0),
    EF_mu24_tight_b35_mediumEF_j35_a4tchad(prefix + "EF_mu24_tight_b35_mediumEF_j35_a4tchad",0),
    EF_mu24_tight_mu6_EFFS(prefix + "EF_mu24_tight_mu6_EFFS",0),
    EF_mu24i_tight(prefix + "EF_mu24i_tight",0),
    EF_mu24i_tight_MG(prefix + "EF_mu24i_tight_MG",0),
    EF_mu24i_tight_MuonEF(prefix + "EF_mu24i_tight_MuonEF",0),
    EF_mu24i_tight_l2muonSA(prefix + "EF_mu24i_tight_l2muonSA",0),
    EF_mu36_tight(prefix + "EF_mu36_tight",0),
    EF_mu40_MSonly_barrel_tight(prefix + "EF_mu40_MSonly_barrel_tight",0),
    EF_mu40_muCombTag_NoEF(prefix + "EF_mu40_muCombTag_NoEF",0),
    EF_mu40_slow_outOfTime_tight(prefix + "EF_mu40_slow_outOfTime_tight",0),
    EF_mu40_slow_tight(prefix + "EF_mu40_slow_tight",0),
    EF_mu40_tight(prefix + "EF_mu40_tight",0),
    EF_mu4T(prefix + "EF_mu4T",0),
    EF_mu4T_Trk_Jpsi(prefix + "EF_mu4T_Trk_Jpsi",0),
    EF_mu4T_cosmic(prefix + "EF_mu4T_cosmic",0),
    EF_mu4T_j110_a4tchad_L2FS_matched(prefix + "EF_mu4T_j110_a4tchad_L2FS_matched",0),
    EF_mu4T_j110_a4tchad_matched(prefix + "EF_mu4T_j110_a4tchad_matched",0),
    EF_mu4T_j145_a4tchad_L2FS_matched(prefix + "EF_mu4T_j145_a4tchad_L2FS_matched",0),
    EF_mu4T_j145_a4tchad_matched(prefix + "EF_mu4T_j145_a4tchad_matched",0),
    EF_mu4T_j15_a4tchad_matched(prefix + "EF_mu4T_j15_a4tchad_matched",0),
    EF_mu4T_j15_a4tchad_matchedZ(prefix + "EF_mu4T_j15_a4tchad_matchedZ",0),
    EF_mu4T_j180_a4tchad_L2FS_matched(prefix + "EF_mu4T_j180_a4tchad_L2FS_matched",0),
    EF_mu4T_j180_a4tchad_matched(prefix + "EF_mu4T_j180_a4tchad_matched",0),
    EF_mu4T_j220_a4tchad_L2FS_matched(prefix + "EF_mu4T_j220_a4tchad_L2FS_matched",0),
    EF_mu4T_j220_a4tchad_matched(prefix + "EF_mu4T_j220_a4tchad_matched",0),
    EF_mu4T_j25_a4tchad_matched(prefix + "EF_mu4T_j25_a4tchad_matched",0),
    EF_mu4T_j25_a4tchad_matchedZ(prefix + "EF_mu4T_j25_a4tchad_matchedZ",0),
    EF_mu4T_j280_a4tchad_L2FS_matched(prefix + "EF_mu4T_j280_a4tchad_L2FS_matched",0),
    EF_mu4T_j280_a4tchad_matched(prefix + "EF_mu4T_j280_a4tchad_matched",0),
    EF_mu4T_j35_a4tchad_matched(prefix + "EF_mu4T_j35_a4tchad_matched",0),
    EF_mu4T_j35_a4tchad_matchedZ(prefix + "EF_mu4T_j35_a4tchad_matchedZ",0),
    EF_mu4T_j360_a4tchad_L2FS_matched(prefix + "EF_mu4T_j360_a4tchad_L2FS_matched",0),
    EF_mu4T_j360_a4tchad_matched(prefix + "EF_mu4T_j360_a4tchad_matched",0),
    EF_mu4T_j45_a4tchad_L2FS_matched(prefix + "EF_mu4T_j45_a4tchad_L2FS_matched",0),
    EF_mu4T_j45_a4tchad_L2FS_matchedZ(prefix + "EF_mu4T_j45_a4tchad_L2FS_matchedZ",0),
    EF_mu4T_j45_a4tchad_matched(prefix + "EF_mu4T_j45_a4tchad_matched",0),
    EF_mu4T_j45_a4tchad_matchedZ(prefix + "EF_mu4T_j45_a4tchad_matchedZ",0),
    EF_mu4T_j55_a4tchad_L2FS_matched(prefix + "EF_mu4T_j55_a4tchad_L2FS_matched",0),
    EF_mu4T_j55_a4tchad_L2FS_matchedZ(prefix + "EF_mu4T_j55_a4tchad_L2FS_matchedZ",0),
    EF_mu4T_j55_a4tchad_matched(prefix + "EF_mu4T_j55_a4tchad_matched",0),
    EF_mu4T_j55_a4tchad_matchedZ(prefix + "EF_mu4T_j55_a4tchad_matchedZ",0),
    EF_mu4T_j65_a4tchad_L2FS_matched(prefix + "EF_mu4T_j65_a4tchad_L2FS_matched",0),
    EF_mu4T_j65_a4tchad_matched(prefix + "EF_mu4T_j65_a4tchad_matched",0),
    EF_mu4T_j65_a4tchad_xe60_tclcw_loose(prefix + "EF_mu4T_j65_a4tchad_xe60_tclcw_loose",0),
    EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose(prefix + "EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose",0),
    EF_mu4T_j80_a4tchad_L2FS_matched(prefix + "EF_mu4T_j80_a4tchad_L2FS_matched",0),
    EF_mu4T_j80_a4tchad_matched(prefix + "EF_mu4T_j80_a4tchad_matched",0),
    EF_mu4Ti_g20Tvh_loose(prefix + "EF_mu4Ti_g20Tvh_loose",0),
    EF_mu4Ti_g20Tvh_loose_TauMass(prefix + "EF_mu4Ti_g20Tvh_loose_TauMass",0),
    EF_mu4Ti_g20Tvh_medium(prefix + "EF_mu4Ti_g20Tvh_medium",0),
    EF_mu4Ti_g20Tvh_medium_TauMass(prefix + "EF_mu4Ti_g20Tvh_medium_TauMass",0),
    EF_mu4Tmu6_Bmumu(prefix + "EF_mu4Tmu6_Bmumu",0),
    EF_mu4Tmu6_Bmumu_Barrel(prefix + "EF_mu4Tmu6_Bmumu_Barrel",0),
    EF_mu4Tmu6_Bmumux(prefix + "EF_mu4Tmu6_Bmumux",0),
    EF_mu4Tmu6_Bmumux_Barrel(prefix + "EF_mu4Tmu6_Bmumux_Barrel",0),
    EF_mu4Tmu6_DiMu(prefix + "EF_mu4Tmu6_DiMu",0),
    EF_mu4Tmu6_DiMu_Barrel(prefix + "EF_mu4Tmu6_DiMu_Barrel",0),
    EF_mu4Tmu6_DiMu_noVtx_noOS(prefix + "EF_mu4Tmu6_DiMu_noVtx_noOS",0),
    EF_mu4Tmu6_Jpsimumu(prefix + "EF_mu4Tmu6_Jpsimumu",0),
    EF_mu4Tmu6_Jpsimumu_Barrel(prefix + "EF_mu4Tmu6_Jpsimumu_Barrel",0),
    EF_mu4Tmu6_Jpsimumu_IDTrkNoCut(prefix + "EF_mu4Tmu6_Jpsimumu_IDTrkNoCut",0),
    EF_mu4Tmu6_Upsimumu(prefix + "EF_mu4Tmu6_Upsimumu",0),
    EF_mu4Tmu6_Upsimumu_Barrel(prefix + "EF_mu4Tmu6_Upsimumu_Barrel",0),
    EF_mu4_L1MU11_MSonly_cosmic(prefix + "EF_mu4_L1MU11_MSonly_cosmic",0),
    EF_mu4_L1MU11_cosmic(prefix + "EF_mu4_L1MU11_cosmic",0),
    EF_mu4_empty_NoAlg(prefix + "EF_mu4_empty_NoAlg",0),
    EF_mu4_firstempty_NoAlg(prefix + "EF_mu4_firstempty_NoAlg",0),
    EF_mu4_unpaired_iso_NoAlg(prefix + "EF_mu4_unpaired_iso_NoAlg",0),
    EF_mu50_MSonly_barrel_tight(prefix + "EF_mu50_MSonly_barrel_tight",0),
    EF_mu6(prefix + "EF_mu6",0),
    EF_mu60_slow_outOfTime_tight1(prefix + "EF_mu60_slow_outOfTime_tight1",0),
    EF_mu60_slow_tight1(prefix + "EF_mu60_slow_tight1",0),
    EF_mu6_Jpsimumu_tight(prefix + "EF_mu6_Jpsimumu_tight",0),
    EF_mu6_MSonly(prefix + "EF_mu6_MSonly",0),
    EF_mu6_Trk_Jpsi_loose(prefix + "EF_mu6_Trk_Jpsi_loose",0),
    EF_mu6i(prefix + "EF_mu6i",0),
    EF_mu8(prefix + "EF_mu8",0),
    EF_mu8_4j45_a4tchad_L2FS(prefix + "EF_mu8_4j45_a4tchad_L2FS",0),
    n(prefix + "n",0),
    track_n(prefix + "track_n",0),
    track_MuonType(prefix + "track_MuonType",0),
    track_MS_pt(prefix + "track_MS_pt",0),
    track_MS_eta(prefix + "track_MS_eta",0),
    track_MS_phi(prefix + "track_MS_phi",0),
    track_MS_charge(prefix + "track_MS_charge",0),
    track_MS_d0(prefix + "track_MS_d0",0),
    track_MS_z0(prefix + "track_MS_z0",0),
    track_MS_chi2(prefix + "track_MS_chi2",0),
    track_MS_chi2prob(prefix + "track_MS_chi2prob",0),
    track_MS_posX(prefix + "track_MS_posX",0),
    track_MS_posY(prefix + "track_MS_posY",0),
    track_MS_posZ(prefix + "track_MS_posZ",0),
    track_MS_hasMS(prefix + "track_MS_hasMS",0),
    track_SA_pt(prefix + "track_SA_pt",0),
    track_SA_eta(prefix + "track_SA_eta",0),
    track_SA_phi(prefix + "track_SA_phi",0),
    track_SA_charge(prefix + "track_SA_charge",0),
    track_SA_d0(prefix + "track_SA_d0",0),
    track_SA_z0(prefix + "track_SA_z0",0),
    track_SA_chi2(prefix + "track_SA_chi2",0),
    track_SA_chi2prob(prefix + "track_SA_chi2prob",0),
    track_SA_posX(prefix + "track_SA_posX",0),
    track_SA_posY(prefix + "track_SA_posY",0),
    track_SA_posZ(prefix + "track_SA_posZ",0),
    track_SA_hasSA(prefix + "track_SA_hasSA",0),
    track_CB_pt(prefix + "track_CB_pt",0),
    track_CB_eta(prefix + "track_CB_eta",0),
    track_CB_phi(prefix + "track_CB_phi",0),
    track_CB_charge(prefix + "track_CB_charge",0),
    track_CB_d0(prefix + "track_CB_d0",0),
    track_CB_z0(prefix + "track_CB_z0",0),
    track_CB_chi2(prefix + "track_CB_chi2",0),
    track_CB_chi2prob(prefix + "track_CB_chi2prob",0),
    track_CB_posX(prefix + "track_CB_posX",0),
    track_CB_posY(prefix + "track_CB_posY",0),
    track_CB_posZ(prefix + "track_CB_posZ",0),
    track_CB_matchChi2(prefix + "track_CB_matchChi2",0),
    track_CB_hasCB(prefix + "track_CB_hasCB",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TrigEFMuonD3PDCollection::ReadFrom(TTree* tree)
  {
    EF_2mu10.ReadFrom(tree);
    EF_2mu10_MSonly_g10_loose.ReadFrom(tree);
    EF_2mu10_MSonly_g10_loose_EMPTY.ReadFrom(tree);
    EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO.ReadFrom(tree);
    EF_2mu13.ReadFrom(tree);
    EF_2mu13_Zmumu_IDTrkNoCut.ReadFrom(tree);
    EF_2mu13_l2muonSA.ReadFrom(tree);
    EF_2mu15.ReadFrom(tree);
    EF_2mu4T.ReadFrom(tree);
    EF_2mu4T_2e5_tight1.ReadFrom(tree);
    EF_2mu4T_Bmumu.ReadFrom(tree);
    EF_2mu4T_Bmumu_Barrel.ReadFrom(tree);
    EF_2mu4T_Bmumu_BarrelOnly.ReadFrom(tree);
    EF_2mu4T_Bmumux.ReadFrom(tree);
    EF_2mu4T_Bmumux_Barrel.ReadFrom(tree);
    EF_2mu4T_Bmumux_BarrelOnly.ReadFrom(tree);
    EF_2mu4T_DiMu.ReadFrom(tree);
    EF_2mu4T_DiMu_Barrel.ReadFrom(tree);
    EF_2mu4T_DiMu_BarrelOnly.ReadFrom(tree);
    EF_2mu4T_DiMu_L2StarB.ReadFrom(tree);
    EF_2mu4T_DiMu_L2StarC.ReadFrom(tree);
    EF_2mu4T_DiMu_e5_tight1.ReadFrom(tree);
    EF_2mu4T_DiMu_l2muonSA.ReadFrom(tree);
    EF_2mu4T_DiMu_noVtx_noOS.ReadFrom(tree);
    EF_2mu4T_Jpsimumu.ReadFrom(tree);
    EF_2mu4T_Jpsimumu_Barrel.ReadFrom(tree);
    EF_2mu4T_Jpsimumu_BarrelOnly.ReadFrom(tree);
    EF_2mu4T_Jpsimumu_IDTrkNoCut.ReadFrom(tree);
    EF_2mu4T_Upsimumu.ReadFrom(tree);
    EF_2mu4T_Upsimumu_Barrel.ReadFrom(tree);
    EF_2mu4T_Upsimumu_BarrelOnly.ReadFrom(tree);
    EF_2mu4T_xe50_tclcw.ReadFrom(tree);
    EF_2mu4T_xe60.ReadFrom(tree);
    EF_2mu4T_xe60_tclcw.ReadFrom(tree);
    EF_2mu6.ReadFrom(tree);
    EF_2mu6_Bmumu.ReadFrom(tree);
    EF_2mu6_Bmumux.ReadFrom(tree);
    EF_2mu6_DiMu.ReadFrom(tree);
    EF_2mu6_DiMu_DY20.ReadFrom(tree);
    EF_2mu6_DiMu_DY25.ReadFrom(tree);
    EF_2mu6_DiMu_noVtx_noOS.ReadFrom(tree);
    EF_2mu6_Jpsimumu.ReadFrom(tree);
    EF_2mu6_Upsimumu.ReadFrom(tree);
    EF_2mu6i_DiMu_DY.ReadFrom(tree);
    EF_2mu6i_DiMu_DY_2j25_a4tchad.ReadFrom(tree);
    EF_2mu6i_DiMu_DY_noVtx_noOS.ReadFrom(tree);
    EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad.ReadFrom(tree);
    EF_2mu8_EFxe30.ReadFrom(tree);
    EF_2mu8_EFxe30_tclcw.ReadFrom(tree);
    EF_mu10.ReadFrom(tree);
    EF_mu10_Jpsimumu.ReadFrom(tree);
    EF_mu10_MSonly.ReadFrom(tree);
    EF_mu10_Upsimumu_tight_FS.ReadFrom(tree);
    EF_mu10i_g10_loose.ReadFrom(tree);
    EF_mu10i_g10_loose_TauMass.ReadFrom(tree);
    EF_mu10i_g10_medium.ReadFrom(tree);
    EF_mu10i_g10_medium_TauMass.ReadFrom(tree);
    EF_mu10i_loose_g12Tvh_loose.ReadFrom(tree);
    EF_mu10i_loose_g12Tvh_loose_TauMass.ReadFrom(tree);
    EF_mu10i_loose_g12Tvh_medium.ReadFrom(tree);
    EF_mu10i_loose_g12Tvh_medium_TauMass.ReadFrom(tree);
    EF_mu11_empty_NoAlg.ReadFrom(tree);
    EF_mu13.ReadFrom(tree);
    EF_mu15.ReadFrom(tree);
    EF_mu18.ReadFrom(tree);
    EF_mu18_2g10_loose.ReadFrom(tree);
    EF_mu18_2g10_medium.ReadFrom(tree);
    EF_mu18_2g15_loose.ReadFrom(tree);
    EF_mu18_IDTrkNoCut_tight.ReadFrom(tree);
    EF_mu18_g20vh_loose.ReadFrom(tree);
    EF_mu18_medium.ReadFrom(tree);
    EF_mu18_tight.ReadFrom(tree);
    EF_mu18_tight_2mu4_EFFS.ReadFrom(tree);
    EF_mu18_tight_e7_medium1.ReadFrom(tree);
    EF_mu18_tight_mu8_EFFS.ReadFrom(tree);
    EF_mu18i4_tight.ReadFrom(tree);
    EF_mu18it_tight.ReadFrom(tree);
    EF_mu20i_tight_g5_loose.ReadFrom(tree);
    EF_mu20i_tight_g5_loose_TauMass.ReadFrom(tree);
    EF_mu20i_tight_g5_medium.ReadFrom(tree);
    EF_mu20i_tight_g5_medium_TauMass.ReadFrom(tree);
    EF_mu20it_tight.ReadFrom(tree);
    EF_mu22_IDTrkNoCut_tight.ReadFrom(tree);
    EF_mu24.ReadFrom(tree);
    EF_mu24_g20vh_loose.ReadFrom(tree);
    EF_mu24_g20vh_medium.ReadFrom(tree);
    EF_mu24_j65_a4tchad.ReadFrom(tree);
    EF_mu24_j65_a4tchad_EFxe40.ReadFrom(tree);
    EF_mu24_j65_a4tchad_EFxe40_tclcw.ReadFrom(tree);
    EF_mu24_j65_a4tchad_EFxe50_tclcw.ReadFrom(tree);
    EF_mu24_j65_a4tchad_EFxe60_tclcw.ReadFrom(tree);
    EF_mu24_medium.ReadFrom(tree);
    EF_mu24_muCombTag_NoEF_tight.ReadFrom(tree);
    EF_mu24_tight.ReadFrom(tree);
    EF_mu24_tight_2j35_a4tchad.ReadFrom(tree);
    EF_mu24_tight_3j35_a4tchad.ReadFrom(tree);
    EF_mu24_tight_4j35_a4tchad.ReadFrom(tree);
    EF_mu24_tight_EFxe40.ReadFrom(tree);
    EF_mu24_tight_L2StarB.ReadFrom(tree);
    EF_mu24_tight_L2StarC.ReadFrom(tree);
    EF_mu24_tight_MG.ReadFrom(tree);
    EF_mu24_tight_MuonEF.ReadFrom(tree);
    EF_mu24_tight_b35_mediumEF_j35_a4tchad.ReadFrom(tree);
    EF_mu24_tight_mu6_EFFS.ReadFrom(tree);
    EF_mu24i_tight.ReadFrom(tree);
    EF_mu24i_tight_MG.ReadFrom(tree);
    EF_mu24i_tight_MuonEF.ReadFrom(tree);
    EF_mu24i_tight_l2muonSA.ReadFrom(tree);
    EF_mu36_tight.ReadFrom(tree);
    EF_mu40_MSonly_barrel_tight.ReadFrom(tree);
    EF_mu40_muCombTag_NoEF.ReadFrom(tree);
    EF_mu40_slow_outOfTime_tight.ReadFrom(tree);
    EF_mu40_slow_tight.ReadFrom(tree);
    EF_mu40_tight.ReadFrom(tree);
    EF_mu4T.ReadFrom(tree);
    EF_mu4T_Trk_Jpsi.ReadFrom(tree);
    EF_mu4T_cosmic.ReadFrom(tree);
    EF_mu4T_j110_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j110_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j145_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j145_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j15_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j15_a4tchad_matchedZ.ReadFrom(tree);
    EF_mu4T_j180_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j180_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j220_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j220_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j25_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j25_a4tchad_matchedZ.ReadFrom(tree);
    EF_mu4T_j280_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j280_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j35_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j35_a4tchad_matchedZ.ReadFrom(tree);
    EF_mu4T_j360_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j360_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j45_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j45_a4tchad_L2FS_matchedZ.ReadFrom(tree);
    EF_mu4T_j45_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j45_a4tchad_matchedZ.ReadFrom(tree);
    EF_mu4T_j55_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j55_a4tchad_L2FS_matchedZ.ReadFrom(tree);
    EF_mu4T_j55_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j55_a4tchad_matchedZ.ReadFrom(tree);
    EF_mu4T_j65_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j65_a4tchad_matched.ReadFrom(tree);
    EF_mu4T_j65_a4tchad_xe60_tclcw_loose.ReadFrom(tree);
    EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose.ReadFrom(tree);
    EF_mu4T_j80_a4tchad_L2FS_matched.ReadFrom(tree);
    EF_mu4T_j80_a4tchad_matched.ReadFrom(tree);
    EF_mu4Ti_g20Tvh_loose.ReadFrom(tree);
    EF_mu4Ti_g20Tvh_loose_TauMass.ReadFrom(tree);
    EF_mu4Ti_g20Tvh_medium.ReadFrom(tree);
    EF_mu4Ti_g20Tvh_medium_TauMass.ReadFrom(tree);
    EF_mu4Tmu6_Bmumu.ReadFrom(tree);
    EF_mu4Tmu6_Bmumu_Barrel.ReadFrom(tree);
    EF_mu4Tmu6_Bmumux.ReadFrom(tree);
    EF_mu4Tmu6_Bmumux_Barrel.ReadFrom(tree);
    EF_mu4Tmu6_DiMu.ReadFrom(tree);
    EF_mu4Tmu6_DiMu_Barrel.ReadFrom(tree);
    EF_mu4Tmu6_DiMu_noVtx_noOS.ReadFrom(tree);
    EF_mu4Tmu6_Jpsimumu.ReadFrom(tree);
    EF_mu4Tmu6_Jpsimumu_Barrel.ReadFrom(tree);
    EF_mu4Tmu6_Jpsimumu_IDTrkNoCut.ReadFrom(tree);
    EF_mu4Tmu6_Upsimumu.ReadFrom(tree);
    EF_mu4Tmu6_Upsimumu_Barrel.ReadFrom(tree);
    EF_mu4_L1MU11_MSonly_cosmic.ReadFrom(tree);
    EF_mu4_L1MU11_cosmic.ReadFrom(tree);
    EF_mu4_empty_NoAlg.ReadFrom(tree);
    EF_mu4_firstempty_NoAlg.ReadFrom(tree);
    EF_mu4_unpaired_iso_NoAlg.ReadFrom(tree);
    EF_mu50_MSonly_barrel_tight.ReadFrom(tree);
    EF_mu6.ReadFrom(tree);
    EF_mu60_slow_outOfTime_tight1.ReadFrom(tree);
    EF_mu60_slow_tight1.ReadFrom(tree);
    EF_mu6_Jpsimumu_tight.ReadFrom(tree);
    EF_mu6_MSonly.ReadFrom(tree);
    EF_mu6_Trk_Jpsi_loose.ReadFrom(tree);
    EF_mu6i.ReadFrom(tree);
    EF_mu8.ReadFrom(tree);
    EF_mu8_4j45_a4tchad_L2FS.ReadFrom(tree);
    n.ReadFrom(tree);
    track_n.ReadFrom(tree);
    track_MuonType.ReadFrom(tree);
    track_MS_pt.ReadFrom(tree);
    track_MS_eta.ReadFrom(tree);
    track_MS_phi.ReadFrom(tree);
    track_MS_charge.ReadFrom(tree);
    track_MS_d0.ReadFrom(tree);
    track_MS_z0.ReadFrom(tree);
    track_MS_chi2.ReadFrom(tree);
    track_MS_chi2prob.ReadFrom(tree);
    track_MS_posX.ReadFrom(tree);
    track_MS_posY.ReadFrom(tree);
    track_MS_posZ.ReadFrom(tree);
    track_MS_hasMS.ReadFrom(tree);
    track_SA_pt.ReadFrom(tree);
    track_SA_eta.ReadFrom(tree);
    track_SA_phi.ReadFrom(tree);
    track_SA_charge.ReadFrom(tree);
    track_SA_d0.ReadFrom(tree);
    track_SA_z0.ReadFrom(tree);
    track_SA_chi2.ReadFrom(tree);
    track_SA_chi2prob.ReadFrom(tree);
    track_SA_posX.ReadFrom(tree);
    track_SA_posY.ReadFrom(tree);
    track_SA_posZ.ReadFrom(tree);
    track_SA_hasSA.ReadFrom(tree);
    track_CB_pt.ReadFrom(tree);
    track_CB_eta.ReadFrom(tree);
    track_CB_phi.ReadFrom(tree);
    track_CB_charge.ReadFrom(tree);
    track_CB_d0.ReadFrom(tree);
    track_CB_z0.ReadFrom(tree);
    track_CB_chi2.ReadFrom(tree);
    track_CB_chi2prob.ReadFrom(tree);
    track_CB_posX.ReadFrom(tree);
    track_CB_posY.ReadFrom(tree);
    track_CB_posZ.ReadFrom(tree);
    track_CB_matchChi2.ReadFrom(tree);
    track_CB_hasCB.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TrigEFMuonD3PDCollection::WriteTo(TTree* tree)
  {
    EF_2mu10.WriteTo(tree);
    EF_2mu10_MSonly_g10_loose.WriteTo(tree);
    EF_2mu10_MSonly_g10_loose_EMPTY.WriteTo(tree);
    EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO.WriteTo(tree);
    EF_2mu13.WriteTo(tree);
    EF_2mu13_Zmumu_IDTrkNoCut.WriteTo(tree);
    EF_2mu13_l2muonSA.WriteTo(tree);
    EF_2mu15.WriteTo(tree);
    EF_2mu4T.WriteTo(tree);
    EF_2mu4T_2e5_tight1.WriteTo(tree);
    EF_2mu4T_Bmumu.WriteTo(tree);
    EF_2mu4T_Bmumu_Barrel.WriteTo(tree);
    EF_2mu4T_Bmumu_BarrelOnly.WriteTo(tree);
    EF_2mu4T_Bmumux.WriteTo(tree);
    EF_2mu4T_Bmumux_Barrel.WriteTo(tree);
    EF_2mu4T_Bmumux_BarrelOnly.WriteTo(tree);
    EF_2mu4T_DiMu.WriteTo(tree);
    EF_2mu4T_DiMu_Barrel.WriteTo(tree);
    EF_2mu4T_DiMu_BarrelOnly.WriteTo(tree);
    EF_2mu4T_DiMu_L2StarB.WriteTo(tree);
    EF_2mu4T_DiMu_L2StarC.WriteTo(tree);
    EF_2mu4T_DiMu_e5_tight1.WriteTo(tree);
    EF_2mu4T_DiMu_l2muonSA.WriteTo(tree);
    EF_2mu4T_DiMu_noVtx_noOS.WriteTo(tree);
    EF_2mu4T_Jpsimumu.WriteTo(tree);
    EF_2mu4T_Jpsimumu_Barrel.WriteTo(tree);
    EF_2mu4T_Jpsimumu_BarrelOnly.WriteTo(tree);
    EF_2mu4T_Jpsimumu_IDTrkNoCut.WriteTo(tree);
    EF_2mu4T_Upsimumu.WriteTo(tree);
    EF_2mu4T_Upsimumu_Barrel.WriteTo(tree);
    EF_2mu4T_Upsimumu_BarrelOnly.WriteTo(tree);
    EF_2mu4T_xe50_tclcw.WriteTo(tree);
    EF_2mu4T_xe60.WriteTo(tree);
    EF_2mu4T_xe60_tclcw.WriteTo(tree);
    EF_2mu6.WriteTo(tree);
    EF_2mu6_Bmumu.WriteTo(tree);
    EF_2mu6_Bmumux.WriteTo(tree);
    EF_2mu6_DiMu.WriteTo(tree);
    EF_2mu6_DiMu_DY20.WriteTo(tree);
    EF_2mu6_DiMu_DY25.WriteTo(tree);
    EF_2mu6_DiMu_noVtx_noOS.WriteTo(tree);
    EF_2mu6_Jpsimumu.WriteTo(tree);
    EF_2mu6_Upsimumu.WriteTo(tree);
    EF_2mu6i_DiMu_DY.WriteTo(tree);
    EF_2mu6i_DiMu_DY_2j25_a4tchad.WriteTo(tree);
    EF_2mu6i_DiMu_DY_noVtx_noOS.WriteTo(tree);
    EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad.WriteTo(tree);
    EF_2mu8_EFxe30.WriteTo(tree);
    EF_2mu8_EFxe30_tclcw.WriteTo(tree);
    EF_mu10.WriteTo(tree);
    EF_mu10_Jpsimumu.WriteTo(tree);
    EF_mu10_MSonly.WriteTo(tree);
    EF_mu10_Upsimumu_tight_FS.WriteTo(tree);
    EF_mu10i_g10_loose.WriteTo(tree);
    EF_mu10i_g10_loose_TauMass.WriteTo(tree);
    EF_mu10i_g10_medium.WriteTo(tree);
    EF_mu10i_g10_medium_TauMass.WriteTo(tree);
    EF_mu10i_loose_g12Tvh_loose.WriteTo(tree);
    EF_mu10i_loose_g12Tvh_loose_TauMass.WriteTo(tree);
    EF_mu10i_loose_g12Tvh_medium.WriteTo(tree);
    EF_mu10i_loose_g12Tvh_medium_TauMass.WriteTo(tree);
    EF_mu11_empty_NoAlg.WriteTo(tree);
    EF_mu13.WriteTo(tree);
    EF_mu15.WriteTo(tree);
    EF_mu18.WriteTo(tree);
    EF_mu18_2g10_loose.WriteTo(tree);
    EF_mu18_2g10_medium.WriteTo(tree);
    EF_mu18_2g15_loose.WriteTo(tree);
    EF_mu18_IDTrkNoCut_tight.WriteTo(tree);
    EF_mu18_g20vh_loose.WriteTo(tree);
    EF_mu18_medium.WriteTo(tree);
    EF_mu18_tight.WriteTo(tree);
    EF_mu18_tight_2mu4_EFFS.WriteTo(tree);
    EF_mu18_tight_e7_medium1.WriteTo(tree);
    EF_mu18_tight_mu8_EFFS.WriteTo(tree);
    EF_mu18i4_tight.WriteTo(tree);
    EF_mu18it_tight.WriteTo(tree);
    EF_mu20i_tight_g5_loose.WriteTo(tree);
    EF_mu20i_tight_g5_loose_TauMass.WriteTo(tree);
    EF_mu20i_tight_g5_medium.WriteTo(tree);
    EF_mu20i_tight_g5_medium_TauMass.WriteTo(tree);
    EF_mu20it_tight.WriteTo(tree);
    EF_mu22_IDTrkNoCut_tight.WriteTo(tree);
    EF_mu24.WriteTo(tree);
    EF_mu24_g20vh_loose.WriteTo(tree);
    EF_mu24_g20vh_medium.WriteTo(tree);
    EF_mu24_j65_a4tchad.WriteTo(tree);
    EF_mu24_j65_a4tchad_EFxe40.WriteTo(tree);
    EF_mu24_j65_a4tchad_EFxe40_tclcw.WriteTo(tree);
    EF_mu24_j65_a4tchad_EFxe50_tclcw.WriteTo(tree);
    EF_mu24_j65_a4tchad_EFxe60_tclcw.WriteTo(tree);
    EF_mu24_medium.WriteTo(tree);
    EF_mu24_muCombTag_NoEF_tight.WriteTo(tree);
    EF_mu24_tight.WriteTo(tree);
    EF_mu24_tight_2j35_a4tchad.WriteTo(tree);
    EF_mu24_tight_3j35_a4tchad.WriteTo(tree);
    EF_mu24_tight_4j35_a4tchad.WriteTo(tree);
    EF_mu24_tight_EFxe40.WriteTo(tree);
    EF_mu24_tight_L2StarB.WriteTo(tree);
    EF_mu24_tight_L2StarC.WriteTo(tree);
    EF_mu24_tight_MG.WriteTo(tree);
    EF_mu24_tight_MuonEF.WriteTo(tree);
    EF_mu24_tight_b35_mediumEF_j35_a4tchad.WriteTo(tree);
    EF_mu24_tight_mu6_EFFS.WriteTo(tree);
    EF_mu24i_tight.WriteTo(tree);
    EF_mu24i_tight_MG.WriteTo(tree);
    EF_mu24i_tight_MuonEF.WriteTo(tree);
    EF_mu24i_tight_l2muonSA.WriteTo(tree);
    EF_mu36_tight.WriteTo(tree);
    EF_mu40_MSonly_barrel_tight.WriteTo(tree);
    EF_mu40_muCombTag_NoEF.WriteTo(tree);
    EF_mu40_slow_outOfTime_tight.WriteTo(tree);
    EF_mu40_slow_tight.WriteTo(tree);
    EF_mu40_tight.WriteTo(tree);
    EF_mu4T.WriteTo(tree);
    EF_mu4T_Trk_Jpsi.WriteTo(tree);
    EF_mu4T_cosmic.WriteTo(tree);
    EF_mu4T_j110_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j110_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j145_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j145_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j15_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j15_a4tchad_matchedZ.WriteTo(tree);
    EF_mu4T_j180_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j180_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j220_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j220_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j25_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j25_a4tchad_matchedZ.WriteTo(tree);
    EF_mu4T_j280_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j280_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j35_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j35_a4tchad_matchedZ.WriteTo(tree);
    EF_mu4T_j360_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j360_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j45_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j45_a4tchad_L2FS_matchedZ.WriteTo(tree);
    EF_mu4T_j45_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j45_a4tchad_matchedZ.WriteTo(tree);
    EF_mu4T_j55_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j55_a4tchad_L2FS_matchedZ.WriteTo(tree);
    EF_mu4T_j55_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j55_a4tchad_matchedZ.WriteTo(tree);
    EF_mu4T_j65_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j65_a4tchad_matched.WriteTo(tree);
    EF_mu4T_j65_a4tchad_xe60_tclcw_loose.WriteTo(tree);
    EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose.WriteTo(tree);
    EF_mu4T_j80_a4tchad_L2FS_matched.WriteTo(tree);
    EF_mu4T_j80_a4tchad_matched.WriteTo(tree);
    EF_mu4Ti_g20Tvh_loose.WriteTo(tree);
    EF_mu4Ti_g20Tvh_loose_TauMass.WriteTo(tree);
    EF_mu4Ti_g20Tvh_medium.WriteTo(tree);
    EF_mu4Ti_g20Tvh_medium_TauMass.WriteTo(tree);
    EF_mu4Tmu6_Bmumu.WriteTo(tree);
    EF_mu4Tmu6_Bmumu_Barrel.WriteTo(tree);
    EF_mu4Tmu6_Bmumux.WriteTo(tree);
    EF_mu4Tmu6_Bmumux_Barrel.WriteTo(tree);
    EF_mu4Tmu6_DiMu.WriteTo(tree);
    EF_mu4Tmu6_DiMu_Barrel.WriteTo(tree);
    EF_mu4Tmu6_DiMu_noVtx_noOS.WriteTo(tree);
    EF_mu4Tmu6_Jpsimumu.WriteTo(tree);
    EF_mu4Tmu6_Jpsimumu_Barrel.WriteTo(tree);
    EF_mu4Tmu6_Jpsimumu_IDTrkNoCut.WriteTo(tree);
    EF_mu4Tmu6_Upsimumu.WriteTo(tree);
    EF_mu4Tmu6_Upsimumu_Barrel.WriteTo(tree);
    EF_mu4_L1MU11_MSonly_cosmic.WriteTo(tree);
    EF_mu4_L1MU11_cosmic.WriteTo(tree);
    EF_mu4_empty_NoAlg.WriteTo(tree);
    EF_mu4_firstempty_NoAlg.WriteTo(tree);
    EF_mu4_unpaired_iso_NoAlg.WriteTo(tree);
    EF_mu50_MSonly_barrel_tight.WriteTo(tree);
    EF_mu6.WriteTo(tree);
    EF_mu60_slow_outOfTime_tight1.WriteTo(tree);
    EF_mu60_slow_tight1.WriteTo(tree);
    EF_mu6_Jpsimumu_tight.WriteTo(tree);
    EF_mu6_MSonly.WriteTo(tree);
    EF_mu6_Trk_Jpsi_loose.WriteTo(tree);
    EF_mu6i.WriteTo(tree);
    EF_mu8.WriteTo(tree);
    EF_mu8_4j45_a4tchad_L2FS.WriteTo(tree);
    n.WriteTo(tree);
    track_n.WriteTo(tree);
    track_MuonType.WriteTo(tree);
    track_MS_pt.WriteTo(tree);
    track_MS_eta.WriteTo(tree);
    track_MS_phi.WriteTo(tree);
    track_MS_charge.WriteTo(tree);
    track_MS_d0.WriteTo(tree);
    track_MS_z0.WriteTo(tree);
    track_MS_chi2.WriteTo(tree);
    track_MS_chi2prob.WriteTo(tree);
    track_MS_posX.WriteTo(tree);
    track_MS_posY.WriteTo(tree);
    track_MS_posZ.WriteTo(tree);
    track_MS_hasMS.WriteTo(tree);
    track_SA_pt.WriteTo(tree);
    track_SA_eta.WriteTo(tree);
    track_SA_phi.WriteTo(tree);
    track_SA_charge.WriteTo(tree);
    track_SA_d0.WriteTo(tree);
    track_SA_z0.WriteTo(tree);
    track_SA_chi2.WriteTo(tree);
    track_SA_chi2prob.WriteTo(tree);
    track_SA_posX.WriteTo(tree);
    track_SA_posY.WriteTo(tree);
    track_SA_posZ.WriteTo(tree);
    track_SA_hasSA.WriteTo(tree);
    track_CB_pt.WriteTo(tree);
    track_CB_eta.WriteTo(tree);
    track_CB_phi.WriteTo(tree);
    track_CB_charge.WriteTo(tree);
    track_CB_d0.WriteTo(tree);
    track_CB_z0.WriteTo(tree);
    track_CB_chi2.WriteTo(tree);
    track_CB_chi2prob.WriteTo(tree);
    track_CB_posX.WriteTo(tree);
    track_CB_posY.WriteTo(tree);
    track_CB_posZ.WriteTo(tree);
    track_CB_matchChi2.WriteTo(tree);
    track_CB_hasCB.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TrigEFMuonD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(EF_2mu10.IsAvailable()) EF_2mu10.SetActive(active);
     if(EF_2mu10_MSonly_g10_loose.IsAvailable()) EF_2mu10_MSonly_g10_loose.SetActive(active);
     if(EF_2mu10_MSonly_g10_loose_EMPTY.IsAvailable()) EF_2mu10_MSonly_g10_loose_EMPTY.SetActive(active);
     if(EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO.IsAvailable()) EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO.SetActive(active);
     if(EF_2mu13.IsAvailable()) EF_2mu13.SetActive(active);
     if(EF_2mu13_Zmumu_IDTrkNoCut.IsAvailable()) EF_2mu13_Zmumu_IDTrkNoCut.SetActive(active);
     if(EF_2mu13_l2muonSA.IsAvailable()) EF_2mu13_l2muonSA.SetActive(active);
     if(EF_2mu15.IsAvailable()) EF_2mu15.SetActive(active);
     if(EF_2mu4T.IsAvailable()) EF_2mu4T.SetActive(active);
     if(EF_2mu4T_2e5_tight1.IsAvailable()) EF_2mu4T_2e5_tight1.SetActive(active);
     if(EF_2mu4T_Bmumu.IsAvailable()) EF_2mu4T_Bmumu.SetActive(active);
     if(EF_2mu4T_Bmumu_Barrel.IsAvailable()) EF_2mu4T_Bmumu_Barrel.SetActive(active);
     if(EF_2mu4T_Bmumu_BarrelOnly.IsAvailable()) EF_2mu4T_Bmumu_BarrelOnly.SetActive(active);
     if(EF_2mu4T_Bmumux.IsAvailable()) EF_2mu4T_Bmumux.SetActive(active);
     if(EF_2mu4T_Bmumux_Barrel.IsAvailable()) EF_2mu4T_Bmumux_Barrel.SetActive(active);
     if(EF_2mu4T_Bmumux_BarrelOnly.IsAvailable()) EF_2mu4T_Bmumux_BarrelOnly.SetActive(active);
     if(EF_2mu4T_DiMu.IsAvailable()) EF_2mu4T_DiMu.SetActive(active);
     if(EF_2mu4T_DiMu_Barrel.IsAvailable()) EF_2mu4T_DiMu_Barrel.SetActive(active);
     if(EF_2mu4T_DiMu_BarrelOnly.IsAvailable()) EF_2mu4T_DiMu_BarrelOnly.SetActive(active);
     if(EF_2mu4T_DiMu_L2StarB.IsAvailable()) EF_2mu4T_DiMu_L2StarB.SetActive(active);
     if(EF_2mu4T_DiMu_L2StarC.IsAvailable()) EF_2mu4T_DiMu_L2StarC.SetActive(active);
     if(EF_2mu4T_DiMu_e5_tight1.IsAvailable()) EF_2mu4T_DiMu_e5_tight1.SetActive(active);
     if(EF_2mu4T_DiMu_l2muonSA.IsAvailable()) EF_2mu4T_DiMu_l2muonSA.SetActive(active);
     if(EF_2mu4T_DiMu_noVtx_noOS.IsAvailable()) EF_2mu4T_DiMu_noVtx_noOS.SetActive(active);
     if(EF_2mu4T_Jpsimumu.IsAvailable()) EF_2mu4T_Jpsimumu.SetActive(active);
     if(EF_2mu4T_Jpsimumu_Barrel.IsAvailable()) EF_2mu4T_Jpsimumu_Barrel.SetActive(active);
     if(EF_2mu4T_Jpsimumu_BarrelOnly.IsAvailable()) EF_2mu4T_Jpsimumu_BarrelOnly.SetActive(active);
     if(EF_2mu4T_Jpsimumu_IDTrkNoCut.IsAvailable()) EF_2mu4T_Jpsimumu_IDTrkNoCut.SetActive(active);
     if(EF_2mu4T_Upsimumu.IsAvailable()) EF_2mu4T_Upsimumu.SetActive(active);
     if(EF_2mu4T_Upsimumu_Barrel.IsAvailable()) EF_2mu4T_Upsimumu_Barrel.SetActive(active);
     if(EF_2mu4T_Upsimumu_BarrelOnly.IsAvailable()) EF_2mu4T_Upsimumu_BarrelOnly.SetActive(active);
     if(EF_2mu4T_xe50_tclcw.IsAvailable()) EF_2mu4T_xe50_tclcw.SetActive(active);
     if(EF_2mu4T_xe60.IsAvailable()) EF_2mu4T_xe60.SetActive(active);
     if(EF_2mu4T_xe60_tclcw.IsAvailable()) EF_2mu4T_xe60_tclcw.SetActive(active);
     if(EF_2mu6.IsAvailable()) EF_2mu6.SetActive(active);
     if(EF_2mu6_Bmumu.IsAvailable()) EF_2mu6_Bmumu.SetActive(active);
     if(EF_2mu6_Bmumux.IsAvailable()) EF_2mu6_Bmumux.SetActive(active);
     if(EF_2mu6_DiMu.IsAvailable()) EF_2mu6_DiMu.SetActive(active);
     if(EF_2mu6_DiMu_DY20.IsAvailable()) EF_2mu6_DiMu_DY20.SetActive(active);
     if(EF_2mu6_DiMu_DY25.IsAvailable()) EF_2mu6_DiMu_DY25.SetActive(active);
     if(EF_2mu6_DiMu_noVtx_noOS.IsAvailable()) EF_2mu6_DiMu_noVtx_noOS.SetActive(active);
     if(EF_2mu6_Jpsimumu.IsAvailable()) EF_2mu6_Jpsimumu.SetActive(active);
     if(EF_2mu6_Upsimumu.IsAvailable()) EF_2mu6_Upsimumu.SetActive(active);
     if(EF_2mu6i_DiMu_DY.IsAvailable()) EF_2mu6i_DiMu_DY.SetActive(active);
     if(EF_2mu6i_DiMu_DY_2j25_a4tchad.IsAvailable()) EF_2mu6i_DiMu_DY_2j25_a4tchad.SetActive(active);
     if(EF_2mu6i_DiMu_DY_noVtx_noOS.IsAvailable()) EF_2mu6i_DiMu_DY_noVtx_noOS.SetActive(active);
     if(EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad.IsAvailable()) EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad.SetActive(active);
     if(EF_2mu8_EFxe30.IsAvailable()) EF_2mu8_EFxe30.SetActive(active);
     if(EF_2mu8_EFxe30_tclcw.IsAvailable()) EF_2mu8_EFxe30_tclcw.SetActive(active);
     if(EF_mu10.IsAvailable()) EF_mu10.SetActive(active);
     if(EF_mu10_Jpsimumu.IsAvailable()) EF_mu10_Jpsimumu.SetActive(active);
     if(EF_mu10_MSonly.IsAvailable()) EF_mu10_MSonly.SetActive(active);
     if(EF_mu10_Upsimumu_tight_FS.IsAvailable()) EF_mu10_Upsimumu_tight_FS.SetActive(active);
     if(EF_mu10i_g10_loose.IsAvailable()) EF_mu10i_g10_loose.SetActive(active);
     if(EF_mu10i_g10_loose_TauMass.IsAvailable()) EF_mu10i_g10_loose_TauMass.SetActive(active);
     if(EF_mu10i_g10_medium.IsAvailable()) EF_mu10i_g10_medium.SetActive(active);
     if(EF_mu10i_g10_medium_TauMass.IsAvailable()) EF_mu10i_g10_medium_TauMass.SetActive(active);
     if(EF_mu10i_loose_g12Tvh_loose.IsAvailable()) EF_mu10i_loose_g12Tvh_loose.SetActive(active);
     if(EF_mu10i_loose_g12Tvh_loose_TauMass.IsAvailable()) EF_mu10i_loose_g12Tvh_loose_TauMass.SetActive(active);
     if(EF_mu10i_loose_g12Tvh_medium.IsAvailable()) EF_mu10i_loose_g12Tvh_medium.SetActive(active);
     if(EF_mu10i_loose_g12Tvh_medium_TauMass.IsAvailable()) EF_mu10i_loose_g12Tvh_medium_TauMass.SetActive(active);
     if(EF_mu11_empty_NoAlg.IsAvailable()) EF_mu11_empty_NoAlg.SetActive(active);
     if(EF_mu13.IsAvailable()) EF_mu13.SetActive(active);
     if(EF_mu15.IsAvailable()) EF_mu15.SetActive(active);
     if(EF_mu18.IsAvailable()) EF_mu18.SetActive(active);
     if(EF_mu18_2g10_loose.IsAvailable()) EF_mu18_2g10_loose.SetActive(active);
     if(EF_mu18_2g10_medium.IsAvailable()) EF_mu18_2g10_medium.SetActive(active);
     if(EF_mu18_2g15_loose.IsAvailable()) EF_mu18_2g15_loose.SetActive(active);
     if(EF_mu18_IDTrkNoCut_tight.IsAvailable()) EF_mu18_IDTrkNoCut_tight.SetActive(active);
     if(EF_mu18_g20vh_loose.IsAvailable()) EF_mu18_g20vh_loose.SetActive(active);
     if(EF_mu18_medium.IsAvailable()) EF_mu18_medium.SetActive(active);
     if(EF_mu18_tight.IsAvailable()) EF_mu18_tight.SetActive(active);
     if(EF_mu18_tight_2mu4_EFFS.IsAvailable()) EF_mu18_tight_2mu4_EFFS.SetActive(active);
     if(EF_mu18_tight_e7_medium1.IsAvailable()) EF_mu18_tight_e7_medium1.SetActive(active);
     if(EF_mu18_tight_mu8_EFFS.IsAvailable()) EF_mu18_tight_mu8_EFFS.SetActive(active);
     if(EF_mu18i4_tight.IsAvailable()) EF_mu18i4_tight.SetActive(active);
     if(EF_mu18it_tight.IsAvailable()) EF_mu18it_tight.SetActive(active);
     if(EF_mu20i_tight_g5_loose.IsAvailable()) EF_mu20i_tight_g5_loose.SetActive(active);
     if(EF_mu20i_tight_g5_loose_TauMass.IsAvailable()) EF_mu20i_tight_g5_loose_TauMass.SetActive(active);
     if(EF_mu20i_tight_g5_medium.IsAvailable()) EF_mu20i_tight_g5_medium.SetActive(active);
     if(EF_mu20i_tight_g5_medium_TauMass.IsAvailable()) EF_mu20i_tight_g5_medium_TauMass.SetActive(active);
     if(EF_mu20it_tight.IsAvailable()) EF_mu20it_tight.SetActive(active);
     if(EF_mu22_IDTrkNoCut_tight.IsAvailable()) EF_mu22_IDTrkNoCut_tight.SetActive(active);
     if(EF_mu24.IsAvailable()) EF_mu24.SetActive(active);
     if(EF_mu24_g20vh_loose.IsAvailable()) EF_mu24_g20vh_loose.SetActive(active);
     if(EF_mu24_g20vh_medium.IsAvailable()) EF_mu24_g20vh_medium.SetActive(active);
     if(EF_mu24_j65_a4tchad.IsAvailable()) EF_mu24_j65_a4tchad.SetActive(active);
     if(EF_mu24_j65_a4tchad_EFxe40.IsAvailable()) EF_mu24_j65_a4tchad_EFxe40.SetActive(active);
     if(EF_mu24_j65_a4tchad_EFxe40_tclcw.IsAvailable()) EF_mu24_j65_a4tchad_EFxe40_tclcw.SetActive(active);
     if(EF_mu24_j65_a4tchad_EFxe50_tclcw.IsAvailable()) EF_mu24_j65_a4tchad_EFxe50_tclcw.SetActive(active);
     if(EF_mu24_j65_a4tchad_EFxe60_tclcw.IsAvailable()) EF_mu24_j65_a4tchad_EFxe60_tclcw.SetActive(active);
     if(EF_mu24_medium.IsAvailable()) EF_mu24_medium.SetActive(active);
     if(EF_mu24_muCombTag_NoEF_tight.IsAvailable()) EF_mu24_muCombTag_NoEF_tight.SetActive(active);
     if(EF_mu24_tight.IsAvailable()) EF_mu24_tight.SetActive(active);
     if(EF_mu24_tight_2j35_a4tchad.IsAvailable()) EF_mu24_tight_2j35_a4tchad.SetActive(active);
     if(EF_mu24_tight_3j35_a4tchad.IsAvailable()) EF_mu24_tight_3j35_a4tchad.SetActive(active);
     if(EF_mu24_tight_4j35_a4tchad.IsAvailable()) EF_mu24_tight_4j35_a4tchad.SetActive(active);
     if(EF_mu24_tight_EFxe40.IsAvailable()) EF_mu24_tight_EFxe40.SetActive(active);
     if(EF_mu24_tight_L2StarB.IsAvailable()) EF_mu24_tight_L2StarB.SetActive(active);
     if(EF_mu24_tight_L2StarC.IsAvailable()) EF_mu24_tight_L2StarC.SetActive(active);
     if(EF_mu24_tight_MG.IsAvailable()) EF_mu24_tight_MG.SetActive(active);
     if(EF_mu24_tight_MuonEF.IsAvailable()) EF_mu24_tight_MuonEF.SetActive(active);
     if(EF_mu24_tight_b35_mediumEF_j35_a4tchad.IsAvailable()) EF_mu24_tight_b35_mediumEF_j35_a4tchad.SetActive(active);
     if(EF_mu24_tight_mu6_EFFS.IsAvailable()) EF_mu24_tight_mu6_EFFS.SetActive(active);
     if(EF_mu24i_tight.IsAvailable()) EF_mu24i_tight.SetActive(active);
     if(EF_mu24i_tight_MG.IsAvailable()) EF_mu24i_tight_MG.SetActive(active);
     if(EF_mu24i_tight_MuonEF.IsAvailable()) EF_mu24i_tight_MuonEF.SetActive(active);
     if(EF_mu24i_tight_l2muonSA.IsAvailable()) EF_mu24i_tight_l2muonSA.SetActive(active);
     if(EF_mu36_tight.IsAvailable()) EF_mu36_tight.SetActive(active);
     if(EF_mu40_MSonly_barrel_tight.IsAvailable()) EF_mu40_MSonly_barrel_tight.SetActive(active);
     if(EF_mu40_muCombTag_NoEF.IsAvailable()) EF_mu40_muCombTag_NoEF.SetActive(active);
     if(EF_mu40_slow_outOfTime_tight.IsAvailable()) EF_mu40_slow_outOfTime_tight.SetActive(active);
     if(EF_mu40_slow_tight.IsAvailable()) EF_mu40_slow_tight.SetActive(active);
     if(EF_mu40_tight.IsAvailable()) EF_mu40_tight.SetActive(active);
     if(EF_mu4T.IsAvailable()) EF_mu4T.SetActive(active);
     if(EF_mu4T_Trk_Jpsi.IsAvailable()) EF_mu4T_Trk_Jpsi.SetActive(active);
     if(EF_mu4T_cosmic.IsAvailable()) EF_mu4T_cosmic.SetActive(active);
     if(EF_mu4T_j110_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j110_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j110_a4tchad_matched.IsAvailable()) EF_mu4T_j110_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j145_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j145_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j145_a4tchad_matched.IsAvailable()) EF_mu4T_j145_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j15_a4tchad_matched.IsAvailable()) EF_mu4T_j15_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j15_a4tchad_matchedZ.IsAvailable()) EF_mu4T_j15_a4tchad_matchedZ.SetActive(active);
     if(EF_mu4T_j180_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j180_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j180_a4tchad_matched.IsAvailable()) EF_mu4T_j180_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j220_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j220_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j220_a4tchad_matched.IsAvailable()) EF_mu4T_j220_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j25_a4tchad_matched.IsAvailable()) EF_mu4T_j25_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j25_a4tchad_matchedZ.IsAvailable()) EF_mu4T_j25_a4tchad_matchedZ.SetActive(active);
     if(EF_mu4T_j280_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j280_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j280_a4tchad_matched.IsAvailable()) EF_mu4T_j280_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j35_a4tchad_matched.IsAvailable()) EF_mu4T_j35_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j35_a4tchad_matchedZ.IsAvailable()) EF_mu4T_j35_a4tchad_matchedZ.SetActive(active);
     if(EF_mu4T_j360_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j360_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j360_a4tchad_matched.IsAvailable()) EF_mu4T_j360_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j45_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j45_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j45_a4tchad_L2FS_matchedZ.IsAvailable()) EF_mu4T_j45_a4tchad_L2FS_matchedZ.SetActive(active);
     if(EF_mu4T_j45_a4tchad_matched.IsAvailable()) EF_mu4T_j45_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j45_a4tchad_matchedZ.IsAvailable()) EF_mu4T_j45_a4tchad_matchedZ.SetActive(active);
     if(EF_mu4T_j55_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j55_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j55_a4tchad_L2FS_matchedZ.IsAvailable()) EF_mu4T_j55_a4tchad_L2FS_matchedZ.SetActive(active);
     if(EF_mu4T_j55_a4tchad_matched.IsAvailable()) EF_mu4T_j55_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j55_a4tchad_matchedZ.IsAvailable()) EF_mu4T_j55_a4tchad_matchedZ.SetActive(active);
     if(EF_mu4T_j65_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j65_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j65_a4tchad_matched.IsAvailable()) EF_mu4T_j65_a4tchad_matched.SetActive(active);
     if(EF_mu4T_j65_a4tchad_xe60_tclcw_loose.IsAvailable()) EF_mu4T_j65_a4tchad_xe60_tclcw_loose.SetActive(active);
     if(EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose.IsAvailable()) EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose.SetActive(active);
     if(EF_mu4T_j80_a4tchad_L2FS_matched.IsAvailable()) EF_mu4T_j80_a4tchad_L2FS_matched.SetActive(active);
     if(EF_mu4T_j80_a4tchad_matched.IsAvailable()) EF_mu4T_j80_a4tchad_matched.SetActive(active);
     if(EF_mu4Ti_g20Tvh_loose.IsAvailable()) EF_mu4Ti_g20Tvh_loose.SetActive(active);
     if(EF_mu4Ti_g20Tvh_loose_TauMass.IsAvailable()) EF_mu4Ti_g20Tvh_loose_TauMass.SetActive(active);
     if(EF_mu4Ti_g20Tvh_medium.IsAvailable()) EF_mu4Ti_g20Tvh_medium.SetActive(active);
     if(EF_mu4Ti_g20Tvh_medium_TauMass.IsAvailable()) EF_mu4Ti_g20Tvh_medium_TauMass.SetActive(active);
     if(EF_mu4Tmu6_Bmumu.IsAvailable()) EF_mu4Tmu6_Bmumu.SetActive(active);
     if(EF_mu4Tmu6_Bmumu_Barrel.IsAvailable()) EF_mu4Tmu6_Bmumu_Barrel.SetActive(active);
     if(EF_mu4Tmu6_Bmumux.IsAvailable()) EF_mu4Tmu6_Bmumux.SetActive(active);
     if(EF_mu4Tmu6_Bmumux_Barrel.IsAvailable()) EF_mu4Tmu6_Bmumux_Barrel.SetActive(active);
     if(EF_mu4Tmu6_DiMu.IsAvailable()) EF_mu4Tmu6_DiMu.SetActive(active);
     if(EF_mu4Tmu6_DiMu_Barrel.IsAvailable()) EF_mu4Tmu6_DiMu_Barrel.SetActive(active);
     if(EF_mu4Tmu6_DiMu_noVtx_noOS.IsAvailable()) EF_mu4Tmu6_DiMu_noVtx_noOS.SetActive(active);
     if(EF_mu4Tmu6_Jpsimumu.IsAvailable()) EF_mu4Tmu6_Jpsimumu.SetActive(active);
     if(EF_mu4Tmu6_Jpsimumu_Barrel.IsAvailable()) EF_mu4Tmu6_Jpsimumu_Barrel.SetActive(active);
     if(EF_mu4Tmu6_Jpsimumu_IDTrkNoCut.IsAvailable()) EF_mu4Tmu6_Jpsimumu_IDTrkNoCut.SetActive(active);
     if(EF_mu4Tmu6_Upsimumu.IsAvailable()) EF_mu4Tmu6_Upsimumu.SetActive(active);
     if(EF_mu4Tmu6_Upsimumu_Barrel.IsAvailable()) EF_mu4Tmu6_Upsimumu_Barrel.SetActive(active);
     if(EF_mu4_L1MU11_MSonly_cosmic.IsAvailable()) EF_mu4_L1MU11_MSonly_cosmic.SetActive(active);
     if(EF_mu4_L1MU11_cosmic.IsAvailable()) EF_mu4_L1MU11_cosmic.SetActive(active);
     if(EF_mu4_empty_NoAlg.IsAvailable()) EF_mu4_empty_NoAlg.SetActive(active);
     if(EF_mu4_firstempty_NoAlg.IsAvailable()) EF_mu4_firstempty_NoAlg.SetActive(active);
     if(EF_mu4_unpaired_iso_NoAlg.IsAvailable()) EF_mu4_unpaired_iso_NoAlg.SetActive(active);
     if(EF_mu50_MSonly_barrel_tight.IsAvailable()) EF_mu50_MSonly_barrel_tight.SetActive(active);
     if(EF_mu6.IsAvailable()) EF_mu6.SetActive(active);
     if(EF_mu60_slow_outOfTime_tight1.IsAvailable()) EF_mu60_slow_outOfTime_tight1.SetActive(active);
     if(EF_mu60_slow_tight1.IsAvailable()) EF_mu60_slow_tight1.SetActive(active);
     if(EF_mu6_Jpsimumu_tight.IsAvailable()) EF_mu6_Jpsimumu_tight.SetActive(active);
     if(EF_mu6_MSonly.IsAvailable()) EF_mu6_MSonly.SetActive(active);
     if(EF_mu6_Trk_Jpsi_loose.IsAvailable()) EF_mu6_Trk_Jpsi_loose.SetActive(active);
     if(EF_mu6i.IsAvailable()) EF_mu6i.SetActive(active);
     if(EF_mu8.IsAvailable()) EF_mu8.SetActive(active);
     if(EF_mu8_4j45_a4tchad_L2FS.IsAvailable()) EF_mu8_4j45_a4tchad_L2FS.SetActive(active);
     if(n.IsAvailable()) n.SetActive(active);
     if(track_n.IsAvailable()) track_n.SetActive(active);
     if(track_MuonType.IsAvailable()) track_MuonType.SetActive(active);
     if(track_MS_pt.IsAvailable()) track_MS_pt.SetActive(active);
     if(track_MS_eta.IsAvailable()) track_MS_eta.SetActive(active);
     if(track_MS_phi.IsAvailable()) track_MS_phi.SetActive(active);
     if(track_MS_charge.IsAvailable()) track_MS_charge.SetActive(active);
     if(track_MS_d0.IsAvailable()) track_MS_d0.SetActive(active);
     if(track_MS_z0.IsAvailable()) track_MS_z0.SetActive(active);
     if(track_MS_chi2.IsAvailable()) track_MS_chi2.SetActive(active);
     if(track_MS_chi2prob.IsAvailable()) track_MS_chi2prob.SetActive(active);
     if(track_MS_posX.IsAvailable()) track_MS_posX.SetActive(active);
     if(track_MS_posY.IsAvailable()) track_MS_posY.SetActive(active);
     if(track_MS_posZ.IsAvailable()) track_MS_posZ.SetActive(active);
     if(track_MS_hasMS.IsAvailable()) track_MS_hasMS.SetActive(active);
     if(track_SA_pt.IsAvailable()) track_SA_pt.SetActive(active);
     if(track_SA_eta.IsAvailable()) track_SA_eta.SetActive(active);
     if(track_SA_phi.IsAvailable()) track_SA_phi.SetActive(active);
     if(track_SA_charge.IsAvailable()) track_SA_charge.SetActive(active);
     if(track_SA_d0.IsAvailable()) track_SA_d0.SetActive(active);
     if(track_SA_z0.IsAvailable()) track_SA_z0.SetActive(active);
     if(track_SA_chi2.IsAvailable()) track_SA_chi2.SetActive(active);
     if(track_SA_chi2prob.IsAvailable()) track_SA_chi2prob.SetActive(active);
     if(track_SA_posX.IsAvailable()) track_SA_posX.SetActive(active);
     if(track_SA_posY.IsAvailable()) track_SA_posY.SetActive(active);
     if(track_SA_posZ.IsAvailable()) track_SA_posZ.SetActive(active);
     if(track_SA_hasSA.IsAvailable()) track_SA_hasSA.SetActive(active);
     if(track_CB_pt.IsAvailable()) track_CB_pt.SetActive(active);
     if(track_CB_eta.IsAvailable()) track_CB_eta.SetActive(active);
     if(track_CB_phi.IsAvailable()) track_CB_phi.SetActive(active);
     if(track_CB_charge.IsAvailable()) track_CB_charge.SetActive(active);
     if(track_CB_d0.IsAvailable()) track_CB_d0.SetActive(active);
     if(track_CB_z0.IsAvailable()) track_CB_z0.SetActive(active);
     if(track_CB_chi2.IsAvailable()) track_CB_chi2.SetActive(active);
     if(track_CB_chi2prob.IsAvailable()) track_CB_chi2prob.SetActive(active);
     if(track_CB_posX.IsAvailable()) track_CB_posX.SetActive(active);
     if(track_CB_posY.IsAvailable()) track_CB_posY.SetActive(active);
     if(track_CB_posZ.IsAvailable()) track_CB_posZ.SetActive(active);
     if(track_CB_matchChi2.IsAvailable()) track_CB_matchChi2.SetActive(active);
     if(track_CB_hasCB.IsAvailable()) track_CB_hasCB.SetActive(active);
    }
    else
    {
      EF_2mu10.SetActive(active);
      EF_2mu10_MSonly_g10_loose.SetActive(active);
      EF_2mu10_MSonly_g10_loose_EMPTY.SetActive(active);
      EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO.SetActive(active);
      EF_2mu13.SetActive(active);
      EF_2mu13_Zmumu_IDTrkNoCut.SetActive(active);
      EF_2mu13_l2muonSA.SetActive(active);
      EF_2mu15.SetActive(active);
      EF_2mu4T.SetActive(active);
      EF_2mu4T_2e5_tight1.SetActive(active);
      EF_2mu4T_Bmumu.SetActive(active);
      EF_2mu4T_Bmumu_Barrel.SetActive(active);
      EF_2mu4T_Bmumu_BarrelOnly.SetActive(active);
      EF_2mu4T_Bmumux.SetActive(active);
      EF_2mu4T_Bmumux_Barrel.SetActive(active);
      EF_2mu4T_Bmumux_BarrelOnly.SetActive(active);
      EF_2mu4T_DiMu.SetActive(active);
      EF_2mu4T_DiMu_Barrel.SetActive(active);
      EF_2mu4T_DiMu_BarrelOnly.SetActive(active);
      EF_2mu4T_DiMu_L2StarB.SetActive(active);
      EF_2mu4T_DiMu_L2StarC.SetActive(active);
      EF_2mu4T_DiMu_e5_tight1.SetActive(active);
      EF_2mu4T_DiMu_l2muonSA.SetActive(active);
      EF_2mu4T_DiMu_noVtx_noOS.SetActive(active);
      EF_2mu4T_Jpsimumu.SetActive(active);
      EF_2mu4T_Jpsimumu_Barrel.SetActive(active);
      EF_2mu4T_Jpsimumu_BarrelOnly.SetActive(active);
      EF_2mu4T_Jpsimumu_IDTrkNoCut.SetActive(active);
      EF_2mu4T_Upsimumu.SetActive(active);
      EF_2mu4T_Upsimumu_Barrel.SetActive(active);
      EF_2mu4T_Upsimumu_BarrelOnly.SetActive(active);
      EF_2mu4T_xe50_tclcw.SetActive(active);
      EF_2mu4T_xe60.SetActive(active);
      EF_2mu4T_xe60_tclcw.SetActive(active);
      EF_2mu6.SetActive(active);
      EF_2mu6_Bmumu.SetActive(active);
      EF_2mu6_Bmumux.SetActive(active);
      EF_2mu6_DiMu.SetActive(active);
      EF_2mu6_DiMu_DY20.SetActive(active);
      EF_2mu6_DiMu_DY25.SetActive(active);
      EF_2mu6_DiMu_noVtx_noOS.SetActive(active);
      EF_2mu6_Jpsimumu.SetActive(active);
      EF_2mu6_Upsimumu.SetActive(active);
      EF_2mu6i_DiMu_DY.SetActive(active);
      EF_2mu6i_DiMu_DY_2j25_a4tchad.SetActive(active);
      EF_2mu6i_DiMu_DY_noVtx_noOS.SetActive(active);
      EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad.SetActive(active);
      EF_2mu8_EFxe30.SetActive(active);
      EF_2mu8_EFxe30_tclcw.SetActive(active);
      EF_mu10.SetActive(active);
      EF_mu10_Jpsimumu.SetActive(active);
      EF_mu10_MSonly.SetActive(active);
      EF_mu10_Upsimumu_tight_FS.SetActive(active);
      EF_mu10i_g10_loose.SetActive(active);
      EF_mu10i_g10_loose_TauMass.SetActive(active);
      EF_mu10i_g10_medium.SetActive(active);
      EF_mu10i_g10_medium_TauMass.SetActive(active);
      EF_mu10i_loose_g12Tvh_loose.SetActive(active);
      EF_mu10i_loose_g12Tvh_loose_TauMass.SetActive(active);
      EF_mu10i_loose_g12Tvh_medium.SetActive(active);
      EF_mu10i_loose_g12Tvh_medium_TauMass.SetActive(active);
      EF_mu11_empty_NoAlg.SetActive(active);
      EF_mu13.SetActive(active);
      EF_mu15.SetActive(active);
      EF_mu18.SetActive(active);
      EF_mu18_2g10_loose.SetActive(active);
      EF_mu18_2g10_medium.SetActive(active);
      EF_mu18_2g15_loose.SetActive(active);
      EF_mu18_IDTrkNoCut_tight.SetActive(active);
      EF_mu18_g20vh_loose.SetActive(active);
      EF_mu18_medium.SetActive(active);
      EF_mu18_tight.SetActive(active);
      EF_mu18_tight_2mu4_EFFS.SetActive(active);
      EF_mu18_tight_e7_medium1.SetActive(active);
      EF_mu18_tight_mu8_EFFS.SetActive(active);
      EF_mu18i4_tight.SetActive(active);
      EF_mu18it_tight.SetActive(active);
      EF_mu20i_tight_g5_loose.SetActive(active);
      EF_mu20i_tight_g5_loose_TauMass.SetActive(active);
      EF_mu20i_tight_g5_medium.SetActive(active);
      EF_mu20i_tight_g5_medium_TauMass.SetActive(active);
      EF_mu20it_tight.SetActive(active);
      EF_mu22_IDTrkNoCut_tight.SetActive(active);
      EF_mu24.SetActive(active);
      EF_mu24_g20vh_loose.SetActive(active);
      EF_mu24_g20vh_medium.SetActive(active);
      EF_mu24_j65_a4tchad.SetActive(active);
      EF_mu24_j65_a4tchad_EFxe40.SetActive(active);
      EF_mu24_j65_a4tchad_EFxe40_tclcw.SetActive(active);
      EF_mu24_j65_a4tchad_EFxe50_tclcw.SetActive(active);
      EF_mu24_j65_a4tchad_EFxe60_tclcw.SetActive(active);
      EF_mu24_medium.SetActive(active);
      EF_mu24_muCombTag_NoEF_tight.SetActive(active);
      EF_mu24_tight.SetActive(active);
      EF_mu24_tight_2j35_a4tchad.SetActive(active);
      EF_mu24_tight_3j35_a4tchad.SetActive(active);
      EF_mu24_tight_4j35_a4tchad.SetActive(active);
      EF_mu24_tight_EFxe40.SetActive(active);
      EF_mu24_tight_L2StarB.SetActive(active);
      EF_mu24_tight_L2StarC.SetActive(active);
      EF_mu24_tight_MG.SetActive(active);
      EF_mu24_tight_MuonEF.SetActive(active);
      EF_mu24_tight_b35_mediumEF_j35_a4tchad.SetActive(active);
      EF_mu24_tight_mu6_EFFS.SetActive(active);
      EF_mu24i_tight.SetActive(active);
      EF_mu24i_tight_MG.SetActive(active);
      EF_mu24i_tight_MuonEF.SetActive(active);
      EF_mu24i_tight_l2muonSA.SetActive(active);
      EF_mu36_tight.SetActive(active);
      EF_mu40_MSonly_barrel_tight.SetActive(active);
      EF_mu40_muCombTag_NoEF.SetActive(active);
      EF_mu40_slow_outOfTime_tight.SetActive(active);
      EF_mu40_slow_tight.SetActive(active);
      EF_mu40_tight.SetActive(active);
      EF_mu4T.SetActive(active);
      EF_mu4T_Trk_Jpsi.SetActive(active);
      EF_mu4T_cosmic.SetActive(active);
      EF_mu4T_j110_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j110_a4tchad_matched.SetActive(active);
      EF_mu4T_j145_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j145_a4tchad_matched.SetActive(active);
      EF_mu4T_j15_a4tchad_matched.SetActive(active);
      EF_mu4T_j15_a4tchad_matchedZ.SetActive(active);
      EF_mu4T_j180_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j180_a4tchad_matched.SetActive(active);
      EF_mu4T_j220_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j220_a4tchad_matched.SetActive(active);
      EF_mu4T_j25_a4tchad_matched.SetActive(active);
      EF_mu4T_j25_a4tchad_matchedZ.SetActive(active);
      EF_mu4T_j280_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j280_a4tchad_matched.SetActive(active);
      EF_mu4T_j35_a4tchad_matched.SetActive(active);
      EF_mu4T_j35_a4tchad_matchedZ.SetActive(active);
      EF_mu4T_j360_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j360_a4tchad_matched.SetActive(active);
      EF_mu4T_j45_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j45_a4tchad_L2FS_matchedZ.SetActive(active);
      EF_mu4T_j45_a4tchad_matched.SetActive(active);
      EF_mu4T_j45_a4tchad_matchedZ.SetActive(active);
      EF_mu4T_j55_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j55_a4tchad_L2FS_matchedZ.SetActive(active);
      EF_mu4T_j55_a4tchad_matched.SetActive(active);
      EF_mu4T_j55_a4tchad_matchedZ.SetActive(active);
      EF_mu4T_j65_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j65_a4tchad_matched.SetActive(active);
      EF_mu4T_j65_a4tchad_xe60_tclcw_loose.SetActive(active);
      EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose.SetActive(active);
      EF_mu4T_j80_a4tchad_L2FS_matched.SetActive(active);
      EF_mu4T_j80_a4tchad_matched.SetActive(active);
      EF_mu4Ti_g20Tvh_loose.SetActive(active);
      EF_mu4Ti_g20Tvh_loose_TauMass.SetActive(active);
      EF_mu4Ti_g20Tvh_medium.SetActive(active);
      EF_mu4Ti_g20Tvh_medium_TauMass.SetActive(active);
      EF_mu4Tmu6_Bmumu.SetActive(active);
      EF_mu4Tmu6_Bmumu_Barrel.SetActive(active);
      EF_mu4Tmu6_Bmumux.SetActive(active);
      EF_mu4Tmu6_Bmumux_Barrel.SetActive(active);
      EF_mu4Tmu6_DiMu.SetActive(active);
      EF_mu4Tmu6_DiMu_Barrel.SetActive(active);
      EF_mu4Tmu6_DiMu_noVtx_noOS.SetActive(active);
      EF_mu4Tmu6_Jpsimumu.SetActive(active);
      EF_mu4Tmu6_Jpsimumu_Barrel.SetActive(active);
      EF_mu4Tmu6_Jpsimumu_IDTrkNoCut.SetActive(active);
      EF_mu4Tmu6_Upsimumu.SetActive(active);
      EF_mu4Tmu6_Upsimumu_Barrel.SetActive(active);
      EF_mu4_L1MU11_MSonly_cosmic.SetActive(active);
      EF_mu4_L1MU11_cosmic.SetActive(active);
      EF_mu4_empty_NoAlg.SetActive(active);
      EF_mu4_firstempty_NoAlg.SetActive(active);
      EF_mu4_unpaired_iso_NoAlg.SetActive(active);
      EF_mu50_MSonly_barrel_tight.SetActive(active);
      EF_mu6.SetActive(active);
      EF_mu60_slow_outOfTime_tight1.SetActive(active);
      EF_mu60_slow_tight1.SetActive(active);
      EF_mu6_Jpsimumu_tight.SetActive(active);
      EF_mu6_MSonly.SetActive(active);
      EF_mu6_Trk_Jpsi_loose.SetActive(active);
      EF_mu6i.SetActive(active);
      EF_mu8.SetActive(active);
      EF_mu8_4j45_a4tchad_L2FS.SetActive(active);
      n.SetActive(active);
      track_n.SetActive(active);
      track_MuonType.SetActive(active);
      track_MS_pt.SetActive(active);
      track_MS_eta.SetActive(active);
      track_MS_phi.SetActive(active);
      track_MS_charge.SetActive(active);
      track_MS_d0.SetActive(active);
      track_MS_z0.SetActive(active);
      track_MS_chi2.SetActive(active);
      track_MS_chi2prob.SetActive(active);
      track_MS_posX.SetActive(active);
      track_MS_posY.SetActive(active);
      track_MS_posZ.SetActive(active);
      track_MS_hasMS.SetActive(active);
      track_SA_pt.SetActive(active);
      track_SA_eta.SetActive(active);
      track_SA_phi.SetActive(active);
      track_SA_charge.SetActive(active);
      track_SA_d0.SetActive(active);
      track_SA_z0.SetActive(active);
      track_SA_chi2.SetActive(active);
      track_SA_chi2prob.SetActive(active);
      track_SA_posX.SetActive(active);
      track_SA_posY.SetActive(active);
      track_SA_posZ.SetActive(active);
      track_SA_hasSA.SetActive(active);
      track_CB_pt.SetActive(active);
      track_CB_eta.SetActive(active);
      track_CB_phi.SetActive(active);
      track_CB_charge.SetActive(active);
      track_CB_d0.SetActive(active);
      track_CB_z0.SetActive(active);
      track_CB_chi2.SetActive(active);
      track_CB_chi2prob.SetActive(active);
      track_CB_posX.SetActive(active);
      track_CB_posY.SetActive(active);
      track_CB_posZ.SetActive(active);
      track_CB_matchChi2.SetActive(active);
      track_CB_hasCB.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TrigEFMuonD3PDCollection::ReadAllActive()
  {
    if(EF_2mu10.IsActive()) EF_2mu10();
    if(EF_2mu10_MSonly_g10_loose.IsActive()) EF_2mu10_MSonly_g10_loose();
    if(EF_2mu10_MSonly_g10_loose_EMPTY.IsActive()) EF_2mu10_MSonly_g10_loose_EMPTY();
    if(EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO.IsActive()) EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO();
    if(EF_2mu13.IsActive()) EF_2mu13();
    if(EF_2mu13_Zmumu_IDTrkNoCut.IsActive()) EF_2mu13_Zmumu_IDTrkNoCut();
    if(EF_2mu13_l2muonSA.IsActive()) EF_2mu13_l2muonSA();
    if(EF_2mu15.IsActive()) EF_2mu15();
    if(EF_2mu4T.IsActive()) EF_2mu4T();
    if(EF_2mu4T_2e5_tight1.IsActive()) EF_2mu4T_2e5_tight1();
    if(EF_2mu4T_Bmumu.IsActive()) EF_2mu4T_Bmumu();
    if(EF_2mu4T_Bmumu_Barrel.IsActive()) EF_2mu4T_Bmumu_Barrel();
    if(EF_2mu4T_Bmumu_BarrelOnly.IsActive()) EF_2mu4T_Bmumu_BarrelOnly();
    if(EF_2mu4T_Bmumux.IsActive()) EF_2mu4T_Bmumux();
    if(EF_2mu4T_Bmumux_Barrel.IsActive()) EF_2mu4T_Bmumux_Barrel();
    if(EF_2mu4T_Bmumux_BarrelOnly.IsActive()) EF_2mu4T_Bmumux_BarrelOnly();
    if(EF_2mu4T_DiMu.IsActive()) EF_2mu4T_DiMu();
    if(EF_2mu4T_DiMu_Barrel.IsActive()) EF_2mu4T_DiMu_Barrel();
    if(EF_2mu4T_DiMu_BarrelOnly.IsActive()) EF_2mu4T_DiMu_BarrelOnly();
    if(EF_2mu4T_DiMu_L2StarB.IsActive()) EF_2mu4T_DiMu_L2StarB();
    if(EF_2mu4T_DiMu_L2StarC.IsActive()) EF_2mu4T_DiMu_L2StarC();
    if(EF_2mu4T_DiMu_e5_tight1.IsActive()) EF_2mu4T_DiMu_e5_tight1();
    if(EF_2mu4T_DiMu_l2muonSA.IsActive()) EF_2mu4T_DiMu_l2muonSA();
    if(EF_2mu4T_DiMu_noVtx_noOS.IsActive()) EF_2mu4T_DiMu_noVtx_noOS();
    if(EF_2mu4T_Jpsimumu.IsActive()) EF_2mu4T_Jpsimumu();
    if(EF_2mu4T_Jpsimumu_Barrel.IsActive()) EF_2mu4T_Jpsimumu_Barrel();
    if(EF_2mu4T_Jpsimumu_BarrelOnly.IsActive()) EF_2mu4T_Jpsimumu_BarrelOnly();
    if(EF_2mu4T_Jpsimumu_IDTrkNoCut.IsActive()) EF_2mu4T_Jpsimumu_IDTrkNoCut();
    if(EF_2mu4T_Upsimumu.IsActive()) EF_2mu4T_Upsimumu();
    if(EF_2mu4T_Upsimumu_Barrel.IsActive()) EF_2mu4T_Upsimumu_Barrel();
    if(EF_2mu4T_Upsimumu_BarrelOnly.IsActive()) EF_2mu4T_Upsimumu_BarrelOnly();
    if(EF_2mu4T_xe50_tclcw.IsActive()) EF_2mu4T_xe50_tclcw();
    if(EF_2mu4T_xe60.IsActive()) EF_2mu4T_xe60();
    if(EF_2mu4T_xe60_tclcw.IsActive()) EF_2mu4T_xe60_tclcw();
    if(EF_2mu6.IsActive()) EF_2mu6();
    if(EF_2mu6_Bmumu.IsActive()) EF_2mu6_Bmumu();
    if(EF_2mu6_Bmumux.IsActive()) EF_2mu6_Bmumux();
    if(EF_2mu6_DiMu.IsActive()) EF_2mu6_DiMu();
    if(EF_2mu6_DiMu_DY20.IsActive()) EF_2mu6_DiMu_DY20();
    if(EF_2mu6_DiMu_DY25.IsActive()) EF_2mu6_DiMu_DY25();
    if(EF_2mu6_DiMu_noVtx_noOS.IsActive()) EF_2mu6_DiMu_noVtx_noOS();
    if(EF_2mu6_Jpsimumu.IsActive()) EF_2mu6_Jpsimumu();
    if(EF_2mu6_Upsimumu.IsActive()) EF_2mu6_Upsimumu();
    if(EF_2mu6i_DiMu_DY.IsActive()) EF_2mu6i_DiMu_DY();
    if(EF_2mu6i_DiMu_DY_2j25_a4tchad.IsActive()) EF_2mu6i_DiMu_DY_2j25_a4tchad();
    if(EF_2mu6i_DiMu_DY_noVtx_noOS.IsActive()) EF_2mu6i_DiMu_DY_noVtx_noOS();
    if(EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad.IsActive()) EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad();
    if(EF_2mu8_EFxe30.IsActive()) EF_2mu8_EFxe30();
    if(EF_2mu8_EFxe30_tclcw.IsActive()) EF_2mu8_EFxe30_tclcw();
    if(EF_mu10.IsActive()) EF_mu10();
    if(EF_mu10_Jpsimumu.IsActive()) EF_mu10_Jpsimumu();
    if(EF_mu10_MSonly.IsActive()) EF_mu10_MSonly();
    if(EF_mu10_Upsimumu_tight_FS.IsActive()) EF_mu10_Upsimumu_tight_FS();
    if(EF_mu10i_g10_loose.IsActive()) EF_mu10i_g10_loose();
    if(EF_mu10i_g10_loose_TauMass.IsActive()) EF_mu10i_g10_loose_TauMass();
    if(EF_mu10i_g10_medium.IsActive()) EF_mu10i_g10_medium();
    if(EF_mu10i_g10_medium_TauMass.IsActive()) EF_mu10i_g10_medium_TauMass();
    if(EF_mu10i_loose_g12Tvh_loose.IsActive()) EF_mu10i_loose_g12Tvh_loose();
    if(EF_mu10i_loose_g12Tvh_loose_TauMass.IsActive()) EF_mu10i_loose_g12Tvh_loose_TauMass();
    if(EF_mu10i_loose_g12Tvh_medium.IsActive()) EF_mu10i_loose_g12Tvh_medium();
    if(EF_mu10i_loose_g12Tvh_medium_TauMass.IsActive()) EF_mu10i_loose_g12Tvh_medium_TauMass();
    if(EF_mu11_empty_NoAlg.IsActive()) EF_mu11_empty_NoAlg();
    if(EF_mu13.IsActive()) EF_mu13();
    if(EF_mu15.IsActive()) EF_mu15();
    if(EF_mu18.IsActive()) EF_mu18();
    if(EF_mu18_2g10_loose.IsActive()) EF_mu18_2g10_loose();
    if(EF_mu18_2g10_medium.IsActive()) EF_mu18_2g10_medium();
    if(EF_mu18_2g15_loose.IsActive()) EF_mu18_2g15_loose();
    if(EF_mu18_IDTrkNoCut_tight.IsActive()) EF_mu18_IDTrkNoCut_tight();
    if(EF_mu18_g20vh_loose.IsActive()) EF_mu18_g20vh_loose();
    if(EF_mu18_medium.IsActive()) EF_mu18_medium();
    if(EF_mu18_tight.IsActive()) EF_mu18_tight();
    if(EF_mu18_tight_2mu4_EFFS.IsActive()) EF_mu18_tight_2mu4_EFFS();
    if(EF_mu18_tight_e7_medium1.IsActive()) EF_mu18_tight_e7_medium1();
    if(EF_mu18_tight_mu8_EFFS.IsActive()) EF_mu18_tight_mu8_EFFS();
    if(EF_mu18i4_tight.IsActive()) EF_mu18i4_tight();
    if(EF_mu18it_tight.IsActive()) EF_mu18it_tight();
    if(EF_mu20i_tight_g5_loose.IsActive()) EF_mu20i_tight_g5_loose();
    if(EF_mu20i_tight_g5_loose_TauMass.IsActive()) EF_mu20i_tight_g5_loose_TauMass();
    if(EF_mu20i_tight_g5_medium.IsActive()) EF_mu20i_tight_g5_medium();
    if(EF_mu20i_tight_g5_medium_TauMass.IsActive()) EF_mu20i_tight_g5_medium_TauMass();
    if(EF_mu20it_tight.IsActive()) EF_mu20it_tight();
    if(EF_mu22_IDTrkNoCut_tight.IsActive()) EF_mu22_IDTrkNoCut_tight();
    if(EF_mu24.IsActive()) EF_mu24();
    if(EF_mu24_g20vh_loose.IsActive()) EF_mu24_g20vh_loose();
    if(EF_mu24_g20vh_medium.IsActive()) EF_mu24_g20vh_medium();
    if(EF_mu24_j65_a4tchad.IsActive()) EF_mu24_j65_a4tchad();
    if(EF_mu24_j65_a4tchad_EFxe40.IsActive()) EF_mu24_j65_a4tchad_EFxe40();
    if(EF_mu24_j65_a4tchad_EFxe40_tclcw.IsActive()) EF_mu24_j65_a4tchad_EFxe40_tclcw();
    if(EF_mu24_j65_a4tchad_EFxe50_tclcw.IsActive()) EF_mu24_j65_a4tchad_EFxe50_tclcw();
    if(EF_mu24_j65_a4tchad_EFxe60_tclcw.IsActive()) EF_mu24_j65_a4tchad_EFxe60_tclcw();
    if(EF_mu24_medium.IsActive()) EF_mu24_medium();
    if(EF_mu24_muCombTag_NoEF_tight.IsActive()) EF_mu24_muCombTag_NoEF_tight();
    if(EF_mu24_tight.IsActive()) EF_mu24_tight();
    if(EF_mu24_tight_2j35_a4tchad.IsActive()) EF_mu24_tight_2j35_a4tchad();
    if(EF_mu24_tight_3j35_a4tchad.IsActive()) EF_mu24_tight_3j35_a4tchad();
    if(EF_mu24_tight_4j35_a4tchad.IsActive()) EF_mu24_tight_4j35_a4tchad();
    if(EF_mu24_tight_EFxe40.IsActive()) EF_mu24_tight_EFxe40();
    if(EF_mu24_tight_L2StarB.IsActive()) EF_mu24_tight_L2StarB();
    if(EF_mu24_tight_L2StarC.IsActive()) EF_mu24_tight_L2StarC();
    if(EF_mu24_tight_MG.IsActive()) EF_mu24_tight_MG();
    if(EF_mu24_tight_MuonEF.IsActive()) EF_mu24_tight_MuonEF();
    if(EF_mu24_tight_b35_mediumEF_j35_a4tchad.IsActive()) EF_mu24_tight_b35_mediumEF_j35_a4tchad();
    if(EF_mu24_tight_mu6_EFFS.IsActive()) EF_mu24_tight_mu6_EFFS();
    if(EF_mu24i_tight.IsActive()) EF_mu24i_tight();
    if(EF_mu24i_tight_MG.IsActive()) EF_mu24i_tight_MG();
    if(EF_mu24i_tight_MuonEF.IsActive()) EF_mu24i_tight_MuonEF();
    if(EF_mu24i_tight_l2muonSA.IsActive()) EF_mu24i_tight_l2muonSA();
    if(EF_mu36_tight.IsActive()) EF_mu36_tight();
    if(EF_mu40_MSonly_barrel_tight.IsActive()) EF_mu40_MSonly_barrel_tight();
    if(EF_mu40_muCombTag_NoEF.IsActive()) EF_mu40_muCombTag_NoEF();
    if(EF_mu40_slow_outOfTime_tight.IsActive()) EF_mu40_slow_outOfTime_tight();
    if(EF_mu40_slow_tight.IsActive()) EF_mu40_slow_tight();
    if(EF_mu40_tight.IsActive()) EF_mu40_tight();
    if(EF_mu4T.IsActive()) EF_mu4T();
    if(EF_mu4T_Trk_Jpsi.IsActive()) EF_mu4T_Trk_Jpsi();
    if(EF_mu4T_cosmic.IsActive()) EF_mu4T_cosmic();
    if(EF_mu4T_j110_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j110_a4tchad_L2FS_matched();
    if(EF_mu4T_j110_a4tchad_matched.IsActive()) EF_mu4T_j110_a4tchad_matched();
    if(EF_mu4T_j145_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j145_a4tchad_L2FS_matched();
    if(EF_mu4T_j145_a4tchad_matched.IsActive()) EF_mu4T_j145_a4tchad_matched();
    if(EF_mu4T_j15_a4tchad_matched.IsActive()) EF_mu4T_j15_a4tchad_matched();
    if(EF_mu4T_j15_a4tchad_matchedZ.IsActive()) EF_mu4T_j15_a4tchad_matchedZ();
    if(EF_mu4T_j180_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j180_a4tchad_L2FS_matched();
    if(EF_mu4T_j180_a4tchad_matched.IsActive()) EF_mu4T_j180_a4tchad_matched();
    if(EF_mu4T_j220_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j220_a4tchad_L2FS_matched();
    if(EF_mu4T_j220_a4tchad_matched.IsActive()) EF_mu4T_j220_a4tchad_matched();
    if(EF_mu4T_j25_a4tchad_matched.IsActive()) EF_mu4T_j25_a4tchad_matched();
    if(EF_mu4T_j25_a4tchad_matchedZ.IsActive()) EF_mu4T_j25_a4tchad_matchedZ();
    if(EF_mu4T_j280_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j280_a4tchad_L2FS_matched();
    if(EF_mu4T_j280_a4tchad_matched.IsActive()) EF_mu4T_j280_a4tchad_matched();
    if(EF_mu4T_j35_a4tchad_matched.IsActive()) EF_mu4T_j35_a4tchad_matched();
    if(EF_mu4T_j35_a4tchad_matchedZ.IsActive()) EF_mu4T_j35_a4tchad_matchedZ();
    if(EF_mu4T_j360_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j360_a4tchad_L2FS_matched();
    if(EF_mu4T_j360_a4tchad_matched.IsActive()) EF_mu4T_j360_a4tchad_matched();
    if(EF_mu4T_j45_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j45_a4tchad_L2FS_matched();
    if(EF_mu4T_j45_a4tchad_L2FS_matchedZ.IsActive()) EF_mu4T_j45_a4tchad_L2FS_matchedZ();
    if(EF_mu4T_j45_a4tchad_matched.IsActive()) EF_mu4T_j45_a4tchad_matched();
    if(EF_mu4T_j45_a4tchad_matchedZ.IsActive()) EF_mu4T_j45_a4tchad_matchedZ();
    if(EF_mu4T_j55_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j55_a4tchad_L2FS_matched();
    if(EF_mu4T_j55_a4tchad_L2FS_matchedZ.IsActive()) EF_mu4T_j55_a4tchad_L2FS_matchedZ();
    if(EF_mu4T_j55_a4tchad_matched.IsActive()) EF_mu4T_j55_a4tchad_matched();
    if(EF_mu4T_j55_a4tchad_matchedZ.IsActive()) EF_mu4T_j55_a4tchad_matchedZ();
    if(EF_mu4T_j65_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j65_a4tchad_L2FS_matched();
    if(EF_mu4T_j65_a4tchad_matched.IsActive()) EF_mu4T_j65_a4tchad_matched();
    if(EF_mu4T_j65_a4tchad_xe60_tclcw_loose.IsActive()) EF_mu4T_j65_a4tchad_xe60_tclcw_loose();
    if(EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose.IsActive()) EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose();
    if(EF_mu4T_j80_a4tchad_L2FS_matched.IsActive()) EF_mu4T_j80_a4tchad_L2FS_matched();
    if(EF_mu4T_j80_a4tchad_matched.IsActive()) EF_mu4T_j80_a4tchad_matched();
    if(EF_mu4Ti_g20Tvh_loose.IsActive()) EF_mu4Ti_g20Tvh_loose();
    if(EF_mu4Ti_g20Tvh_loose_TauMass.IsActive()) EF_mu4Ti_g20Tvh_loose_TauMass();
    if(EF_mu4Ti_g20Tvh_medium.IsActive()) EF_mu4Ti_g20Tvh_medium();
    if(EF_mu4Ti_g20Tvh_medium_TauMass.IsActive()) EF_mu4Ti_g20Tvh_medium_TauMass();
    if(EF_mu4Tmu6_Bmumu.IsActive()) EF_mu4Tmu6_Bmumu();
    if(EF_mu4Tmu6_Bmumu_Barrel.IsActive()) EF_mu4Tmu6_Bmumu_Barrel();
    if(EF_mu4Tmu6_Bmumux.IsActive()) EF_mu4Tmu6_Bmumux();
    if(EF_mu4Tmu6_Bmumux_Barrel.IsActive()) EF_mu4Tmu6_Bmumux_Barrel();
    if(EF_mu4Tmu6_DiMu.IsActive()) EF_mu4Tmu6_DiMu();
    if(EF_mu4Tmu6_DiMu_Barrel.IsActive()) EF_mu4Tmu6_DiMu_Barrel();
    if(EF_mu4Tmu6_DiMu_noVtx_noOS.IsActive()) EF_mu4Tmu6_DiMu_noVtx_noOS();
    if(EF_mu4Tmu6_Jpsimumu.IsActive()) EF_mu4Tmu6_Jpsimumu();
    if(EF_mu4Tmu6_Jpsimumu_Barrel.IsActive()) EF_mu4Tmu6_Jpsimumu_Barrel();
    if(EF_mu4Tmu6_Jpsimumu_IDTrkNoCut.IsActive()) EF_mu4Tmu6_Jpsimumu_IDTrkNoCut();
    if(EF_mu4Tmu6_Upsimumu.IsActive()) EF_mu4Tmu6_Upsimumu();
    if(EF_mu4Tmu6_Upsimumu_Barrel.IsActive()) EF_mu4Tmu6_Upsimumu_Barrel();
    if(EF_mu4_L1MU11_MSonly_cosmic.IsActive()) EF_mu4_L1MU11_MSonly_cosmic();
    if(EF_mu4_L1MU11_cosmic.IsActive()) EF_mu4_L1MU11_cosmic();
    if(EF_mu4_empty_NoAlg.IsActive()) EF_mu4_empty_NoAlg();
    if(EF_mu4_firstempty_NoAlg.IsActive()) EF_mu4_firstempty_NoAlg();
    if(EF_mu4_unpaired_iso_NoAlg.IsActive()) EF_mu4_unpaired_iso_NoAlg();
    if(EF_mu50_MSonly_barrel_tight.IsActive()) EF_mu50_MSonly_barrel_tight();
    if(EF_mu6.IsActive()) EF_mu6();
    if(EF_mu60_slow_outOfTime_tight1.IsActive()) EF_mu60_slow_outOfTime_tight1();
    if(EF_mu60_slow_tight1.IsActive()) EF_mu60_slow_tight1();
    if(EF_mu6_Jpsimumu_tight.IsActive()) EF_mu6_Jpsimumu_tight();
    if(EF_mu6_MSonly.IsActive()) EF_mu6_MSonly();
    if(EF_mu6_Trk_Jpsi_loose.IsActive()) EF_mu6_Trk_Jpsi_loose();
    if(EF_mu6i.IsActive()) EF_mu6i();
    if(EF_mu8.IsActive()) EF_mu8();
    if(EF_mu8_4j45_a4tchad_L2FS.IsActive()) EF_mu8_4j45_a4tchad_L2FS();
    if(n.IsActive()) n();
    if(track_n.IsActive()) track_n();
    if(track_MuonType.IsActive()) track_MuonType();
    if(track_MS_pt.IsActive()) track_MS_pt();
    if(track_MS_eta.IsActive()) track_MS_eta();
    if(track_MS_phi.IsActive()) track_MS_phi();
    if(track_MS_charge.IsActive()) track_MS_charge();
    if(track_MS_d0.IsActive()) track_MS_d0();
    if(track_MS_z0.IsActive()) track_MS_z0();
    if(track_MS_chi2.IsActive()) track_MS_chi2();
    if(track_MS_chi2prob.IsActive()) track_MS_chi2prob();
    if(track_MS_posX.IsActive()) track_MS_posX();
    if(track_MS_posY.IsActive()) track_MS_posY();
    if(track_MS_posZ.IsActive()) track_MS_posZ();
    if(track_MS_hasMS.IsActive()) track_MS_hasMS();
    if(track_SA_pt.IsActive()) track_SA_pt();
    if(track_SA_eta.IsActive()) track_SA_eta();
    if(track_SA_phi.IsActive()) track_SA_phi();
    if(track_SA_charge.IsActive()) track_SA_charge();
    if(track_SA_d0.IsActive()) track_SA_d0();
    if(track_SA_z0.IsActive()) track_SA_z0();
    if(track_SA_chi2.IsActive()) track_SA_chi2();
    if(track_SA_chi2prob.IsActive()) track_SA_chi2prob();
    if(track_SA_posX.IsActive()) track_SA_posX();
    if(track_SA_posY.IsActive()) track_SA_posY();
    if(track_SA_posZ.IsActive()) track_SA_posZ();
    if(track_SA_hasSA.IsActive()) track_SA_hasSA();
    if(track_CB_pt.IsActive()) track_CB_pt();
    if(track_CB_eta.IsActive()) track_CB_eta();
    if(track_CB_phi.IsActive()) track_CB_phi();
    if(track_CB_charge.IsActive()) track_CB_charge();
    if(track_CB_d0.IsActive()) track_CB_d0();
    if(track_CB_z0.IsActive()) track_CB_z0();
    if(track_CB_chi2.IsActive()) track_CB_chi2();
    if(track_CB_chi2prob.IsActive()) track_CB_chi2prob();
    if(track_CB_posX.IsActive()) track_CB_posX();
    if(track_CB_posY.IsActive()) track_CB_posY();
    if(track_CB_posZ.IsActive()) track_CB_posZ();
    if(track_CB_matchChi2.IsActive()) track_CB_matchChi2();
    if(track_CB_hasCB.IsActive()) track_CB_hasCB();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TrigEFMuonD3PDCollection_CXX
