// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TrigL2TauD3PDCollection_CXX
#define D3PDREADER_TrigL2TauD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TrigL2TauD3PDCollection.h"

ClassImp(D3PDReader::TrigL2TauD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigL2TauD3PDCollection::TrigL2TauD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    n(prefix + "n",&master),
    pt(prefix + "pt",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    px(prefix + "px",&master),
    py(prefix + "py",&master),
    pz(prefix + "pz",&master),
    RoIWord(prefix + "RoIWord",&master),
    simpleEtFlow(prefix + "simpleEtFlow",&master),
    nMatchedTracks(prefix + "nMatchedTracks",&master),
    trkAvgDist(prefix + "trkAvgDist",&master),
    etOverPtLeadTrk(prefix + "etOverPtLeadTrk",&master),
    cluster_quality(prefix + "cluster_quality",&master),
    cluster_EMenergy(prefix + "cluster_EMenergy",&master),
    cluster_HADenergy(prefix + "cluster_HADenergy",&master),
    cluster_eta(prefix + "cluster_eta",&master),
    cluster_phi(prefix + "cluster_phi",&master),
    cluster_CaloRadius(prefix + "cluster_CaloRadius",&master),
    cluster_EMRadius3S(prefix + "cluster_EMRadius3S",&master),
    cluster_CoreFrac(prefix + "cluster_CoreFrac",&master),
    cluster_EMFrac(prefix + "cluster_EMFrac",&master),
    cluster_HADRadius(prefix + "cluster_HADRadius",&master),
    cluster_IsoFrac(prefix + "cluster_IsoFrac",&master),
    cluster_numTotCells(prefix + "cluster_numTotCells",&master),
    cluster_stripWidth(prefix + "cluster_stripWidth",&master),
    cluster_stripWidthOffline(prefix + "cluster_stripWidthOffline",&master),
    cluster_EMenergyWide(prefix + "cluster_EMenergyWide",&master),
    cluster_EMenergyNarrow(prefix + "cluster_EMenergyNarrow",&master),
    cluster_EMenergyMedium(prefix + "cluster_EMenergyMedium",&master),
    cluster_HADenergyWide(prefix + "cluster_HADenergyWide",&master),
    cluster_HADenergyNarrow(prefix + "cluster_HADenergyNarrow",&master),
    cluster_HADenergyMedium(prefix + "cluster_HADenergyMedium",&master),
    cluster_HADRadiusSamp(prefix + "cluster_HADRadiusSamp",&master),
    cluster_EMRadiusSamp(prefix + "cluster_EMRadiusSamp",&master),
    cluster_etNarrow(prefix + "cluster_etNarrow",&master),
    cluster_etWide(prefix + "cluster_etWide",&master),
    cluster_etMedium(prefix + "cluster_etMedium",&master),
    tracksinfo_nCoreTracks(prefix + "tracksinfo_nCoreTracks",&master),
    tracksinfo_nSlowTracks(prefix + "tracksinfo_nSlowTracks",&master),
    tracksinfo_nIsoTracks(prefix + "tracksinfo_nIsoTracks",&master),
    tracksinfo_charge(prefix + "tracksinfo_charge",&master),
    tracksinfo_leadingTrackPt(prefix + "tracksinfo_leadingTrackPt",&master),
    tracksinfo_scalarPtSumCore(prefix + "tracksinfo_scalarPtSumCore",&master),
    tracksinfo_scalarPtSumIso(prefix + "tracksinfo_scalarPtSumIso",&master),
    tracksinfo_3fastest_pt(prefix + "tracksinfo_3fastest_pt",&master),
    tracksinfo_3fastest_eta(prefix + "tracksinfo_3fastest_eta",&master),
    tracksinfo_3fastest_phi(prefix + "tracksinfo_3fastest_phi",&master),
    tracksinfo_3fastest_m(prefix + "tracksinfo_3fastest_m",&master),
    tracks_algorithmId(prefix + "tracks_algorithmId",&master),
    idscan_trk_n(prefix + "idscan_trk_n",&master),
    idscan_trk_index(prefix + "idscan_trk_index",&master),
    sitrack_trk_n(prefix + "sitrack_trk_n",&master),
    sitrack_trk_index(prefix + "sitrack_trk_index",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigL2TauD3PDCollection::TrigL2TauD3PDCollection(const std::string& prefix):
    TObject(),
    n(prefix + "n",0),
    pt(prefix + "pt",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    px(prefix + "px",0),
    py(prefix + "py",0),
    pz(prefix + "pz",0),
    RoIWord(prefix + "RoIWord",0),
    simpleEtFlow(prefix + "simpleEtFlow",0),
    nMatchedTracks(prefix + "nMatchedTracks",0),
    trkAvgDist(prefix + "trkAvgDist",0),
    etOverPtLeadTrk(prefix + "etOverPtLeadTrk",0),
    cluster_quality(prefix + "cluster_quality",0),
    cluster_EMenergy(prefix + "cluster_EMenergy",0),
    cluster_HADenergy(prefix + "cluster_HADenergy",0),
    cluster_eta(prefix + "cluster_eta",0),
    cluster_phi(prefix + "cluster_phi",0),
    cluster_CaloRadius(prefix + "cluster_CaloRadius",0),
    cluster_EMRadius3S(prefix + "cluster_EMRadius3S",0),
    cluster_CoreFrac(prefix + "cluster_CoreFrac",0),
    cluster_EMFrac(prefix + "cluster_EMFrac",0),
    cluster_HADRadius(prefix + "cluster_HADRadius",0),
    cluster_IsoFrac(prefix + "cluster_IsoFrac",0),
    cluster_numTotCells(prefix + "cluster_numTotCells",0),
    cluster_stripWidth(prefix + "cluster_stripWidth",0),
    cluster_stripWidthOffline(prefix + "cluster_stripWidthOffline",0),
    cluster_EMenergyWide(prefix + "cluster_EMenergyWide",0),
    cluster_EMenergyNarrow(prefix + "cluster_EMenergyNarrow",0),
    cluster_EMenergyMedium(prefix + "cluster_EMenergyMedium",0),
    cluster_HADenergyWide(prefix + "cluster_HADenergyWide",0),
    cluster_HADenergyNarrow(prefix + "cluster_HADenergyNarrow",0),
    cluster_HADenergyMedium(prefix + "cluster_HADenergyMedium",0),
    cluster_HADRadiusSamp(prefix + "cluster_HADRadiusSamp",0),
    cluster_EMRadiusSamp(prefix + "cluster_EMRadiusSamp",0),
    cluster_etNarrow(prefix + "cluster_etNarrow",0),
    cluster_etWide(prefix + "cluster_etWide",0),
    cluster_etMedium(prefix + "cluster_etMedium",0),
    tracksinfo_nCoreTracks(prefix + "tracksinfo_nCoreTracks",0),
    tracksinfo_nSlowTracks(prefix + "tracksinfo_nSlowTracks",0),
    tracksinfo_nIsoTracks(prefix + "tracksinfo_nIsoTracks",0),
    tracksinfo_charge(prefix + "tracksinfo_charge",0),
    tracksinfo_leadingTrackPt(prefix + "tracksinfo_leadingTrackPt",0),
    tracksinfo_scalarPtSumCore(prefix + "tracksinfo_scalarPtSumCore",0),
    tracksinfo_scalarPtSumIso(prefix + "tracksinfo_scalarPtSumIso",0),
    tracksinfo_3fastest_pt(prefix + "tracksinfo_3fastest_pt",0),
    tracksinfo_3fastest_eta(prefix + "tracksinfo_3fastest_eta",0),
    tracksinfo_3fastest_phi(prefix + "tracksinfo_3fastest_phi",0),
    tracksinfo_3fastest_m(prefix + "tracksinfo_3fastest_m",0),
    tracks_algorithmId(prefix + "tracks_algorithmId",0),
    idscan_trk_n(prefix + "idscan_trk_n",0),
    idscan_trk_index(prefix + "idscan_trk_index",0),
    sitrack_trk_n(prefix + "sitrack_trk_n",0),
    sitrack_trk_index(prefix + "sitrack_trk_index",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TrigL2TauD3PDCollection::ReadFrom(TTree* tree)
  {
    n.ReadFrom(tree);
    pt.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    px.ReadFrom(tree);
    py.ReadFrom(tree);
    pz.ReadFrom(tree);
    RoIWord.ReadFrom(tree);
    simpleEtFlow.ReadFrom(tree);
    nMatchedTracks.ReadFrom(tree);
    trkAvgDist.ReadFrom(tree);
    etOverPtLeadTrk.ReadFrom(tree);
    cluster_quality.ReadFrom(tree);
    cluster_EMenergy.ReadFrom(tree);
    cluster_HADenergy.ReadFrom(tree);
    cluster_eta.ReadFrom(tree);
    cluster_phi.ReadFrom(tree);
    cluster_CaloRadius.ReadFrom(tree);
    cluster_EMRadius3S.ReadFrom(tree);
    cluster_CoreFrac.ReadFrom(tree);
    cluster_EMFrac.ReadFrom(tree);
    cluster_HADRadius.ReadFrom(tree);
    cluster_IsoFrac.ReadFrom(tree);
    cluster_numTotCells.ReadFrom(tree);
    cluster_stripWidth.ReadFrom(tree);
    cluster_stripWidthOffline.ReadFrom(tree);
    cluster_EMenergyWide.ReadFrom(tree);
    cluster_EMenergyNarrow.ReadFrom(tree);
    cluster_EMenergyMedium.ReadFrom(tree);
    cluster_HADenergyWide.ReadFrom(tree);
    cluster_HADenergyNarrow.ReadFrom(tree);
    cluster_HADenergyMedium.ReadFrom(tree);
    cluster_HADRadiusSamp.ReadFrom(tree);
    cluster_EMRadiusSamp.ReadFrom(tree);
    cluster_etNarrow.ReadFrom(tree);
    cluster_etWide.ReadFrom(tree);
    cluster_etMedium.ReadFrom(tree);
    tracksinfo_nCoreTracks.ReadFrom(tree);
    tracksinfo_nSlowTracks.ReadFrom(tree);
    tracksinfo_nIsoTracks.ReadFrom(tree);
    tracksinfo_charge.ReadFrom(tree);
    tracksinfo_leadingTrackPt.ReadFrom(tree);
    tracksinfo_scalarPtSumCore.ReadFrom(tree);
    tracksinfo_scalarPtSumIso.ReadFrom(tree);
    tracksinfo_3fastest_pt.ReadFrom(tree);
    tracksinfo_3fastest_eta.ReadFrom(tree);
    tracksinfo_3fastest_phi.ReadFrom(tree);
    tracksinfo_3fastest_m.ReadFrom(tree);
    tracks_algorithmId.ReadFrom(tree);
    idscan_trk_n.ReadFrom(tree);
    idscan_trk_index.ReadFrom(tree);
    sitrack_trk_n.ReadFrom(tree);
    sitrack_trk_index.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TrigL2TauD3PDCollection::WriteTo(TTree* tree)
  {
    n.WriteTo(tree);
    pt.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    px.WriteTo(tree);
    py.WriteTo(tree);
    pz.WriteTo(tree);
    RoIWord.WriteTo(tree);
    simpleEtFlow.WriteTo(tree);
    nMatchedTracks.WriteTo(tree);
    trkAvgDist.WriteTo(tree);
    etOverPtLeadTrk.WriteTo(tree);
    cluster_quality.WriteTo(tree);
    cluster_EMenergy.WriteTo(tree);
    cluster_HADenergy.WriteTo(tree);
    cluster_eta.WriteTo(tree);
    cluster_phi.WriteTo(tree);
    cluster_CaloRadius.WriteTo(tree);
    cluster_EMRadius3S.WriteTo(tree);
    cluster_CoreFrac.WriteTo(tree);
    cluster_EMFrac.WriteTo(tree);
    cluster_HADRadius.WriteTo(tree);
    cluster_IsoFrac.WriteTo(tree);
    cluster_numTotCells.WriteTo(tree);
    cluster_stripWidth.WriteTo(tree);
    cluster_stripWidthOffline.WriteTo(tree);
    cluster_EMenergyWide.WriteTo(tree);
    cluster_EMenergyNarrow.WriteTo(tree);
    cluster_EMenergyMedium.WriteTo(tree);
    cluster_HADenergyWide.WriteTo(tree);
    cluster_HADenergyNarrow.WriteTo(tree);
    cluster_HADenergyMedium.WriteTo(tree);
    cluster_HADRadiusSamp.WriteTo(tree);
    cluster_EMRadiusSamp.WriteTo(tree);
    cluster_etNarrow.WriteTo(tree);
    cluster_etWide.WriteTo(tree);
    cluster_etMedium.WriteTo(tree);
    tracksinfo_nCoreTracks.WriteTo(tree);
    tracksinfo_nSlowTracks.WriteTo(tree);
    tracksinfo_nIsoTracks.WriteTo(tree);
    tracksinfo_charge.WriteTo(tree);
    tracksinfo_leadingTrackPt.WriteTo(tree);
    tracksinfo_scalarPtSumCore.WriteTo(tree);
    tracksinfo_scalarPtSumIso.WriteTo(tree);
    tracksinfo_3fastest_pt.WriteTo(tree);
    tracksinfo_3fastest_eta.WriteTo(tree);
    tracksinfo_3fastest_phi.WriteTo(tree);
    tracksinfo_3fastest_m.WriteTo(tree);
    tracks_algorithmId.WriteTo(tree);
    idscan_trk_n.WriteTo(tree);
    idscan_trk_index.WriteTo(tree);
    sitrack_trk_n.WriteTo(tree);
    sitrack_trk_index.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TrigL2TauD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(n.IsAvailable()) n.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(px.IsAvailable()) px.SetActive(active);
     if(py.IsAvailable()) py.SetActive(active);
     if(pz.IsAvailable()) pz.SetActive(active);
     if(RoIWord.IsAvailable()) RoIWord.SetActive(active);
     if(simpleEtFlow.IsAvailable()) simpleEtFlow.SetActive(active);
     if(nMatchedTracks.IsAvailable()) nMatchedTracks.SetActive(active);
     if(trkAvgDist.IsAvailable()) trkAvgDist.SetActive(active);
     if(etOverPtLeadTrk.IsAvailable()) etOverPtLeadTrk.SetActive(active);
     if(cluster_quality.IsAvailable()) cluster_quality.SetActive(active);
     if(cluster_EMenergy.IsAvailable()) cluster_EMenergy.SetActive(active);
     if(cluster_HADenergy.IsAvailable()) cluster_HADenergy.SetActive(active);
     if(cluster_eta.IsAvailable()) cluster_eta.SetActive(active);
     if(cluster_phi.IsAvailable()) cluster_phi.SetActive(active);
     if(cluster_CaloRadius.IsAvailable()) cluster_CaloRadius.SetActive(active);
     if(cluster_EMRadius3S.IsAvailable()) cluster_EMRadius3S.SetActive(active);
     if(cluster_CoreFrac.IsAvailable()) cluster_CoreFrac.SetActive(active);
     if(cluster_EMFrac.IsAvailable()) cluster_EMFrac.SetActive(active);
     if(cluster_HADRadius.IsAvailable()) cluster_HADRadius.SetActive(active);
     if(cluster_IsoFrac.IsAvailable()) cluster_IsoFrac.SetActive(active);
     if(cluster_numTotCells.IsAvailable()) cluster_numTotCells.SetActive(active);
     if(cluster_stripWidth.IsAvailable()) cluster_stripWidth.SetActive(active);
     if(cluster_stripWidthOffline.IsAvailable()) cluster_stripWidthOffline.SetActive(active);
     if(cluster_EMenergyWide.IsAvailable()) cluster_EMenergyWide.SetActive(active);
     if(cluster_EMenergyNarrow.IsAvailable()) cluster_EMenergyNarrow.SetActive(active);
     if(cluster_EMenergyMedium.IsAvailable()) cluster_EMenergyMedium.SetActive(active);
     if(cluster_HADenergyWide.IsAvailable()) cluster_HADenergyWide.SetActive(active);
     if(cluster_HADenergyNarrow.IsAvailable()) cluster_HADenergyNarrow.SetActive(active);
     if(cluster_HADenergyMedium.IsAvailable()) cluster_HADenergyMedium.SetActive(active);
     if(cluster_HADRadiusSamp.IsAvailable()) cluster_HADRadiusSamp.SetActive(active);
     if(cluster_EMRadiusSamp.IsAvailable()) cluster_EMRadiusSamp.SetActive(active);
     if(cluster_etNarrow.IsAvailable()) cluster_etNarrow.SetActive(active);
     if(cluster_etWide.IsAvailable()) cluster_etWide.SetActive(active);
     if(cluster_etMedium.IsAvailable()) cluster_etMedium.SetActive(active);
     if(tracksinfo_nCoreTracks.IsAvailable()) tracksinfo_nCoreTracks.SetActive(active);
     if(tracksinfo_nSlowTracks.IsAvailable()) tracksinfo_nSlowTracks.SetActive(active);
     if(tracksinfo_nIsoTracks.IsAvailable()) tracksinfo_nIsoTracks.SetActive(active);
     if(tracksinfo_charge.IsAvailable()) tracksinfo_charge.SetActive(active);
     if(tracksinfo_leadingTrackPt.IsAvailable()) tracksinfo_leadingTrackPt.SetActive(active);
     if(tracksinfo_scalarPtSumCore.IsAvailable()) tracksinfo_scalarPtSumCore.SetActive(active);
     if(tracksinfo_scalarPtSumIso.IsAvailable()) tracksinfo_scalarPtSumIso.SetActive(active);
     if(tracksinfo_3fastest_pt.IsAvailable()) tracksinfo_3fastest_pt.SetActive(active);
     if(tracksinfo_3fastest_eta.IsAvailable()) tracksinfo_3fastest_eta.SetActive(active);
     if(tracksinfo_3fastest_phi.IsAvailable()) tracksinfo_3fastest_phi.SetActive(active);
     if(tracksinfo_3fastest_m.IsAvailable()) tracksinfo_3fastest_m.SetActive(active);
     if(tracks_algorithmId.IsAvailable()) tracks_algorithmId.SetActive(active);
     if(idscan_trk_n.IsAvailable()) idscan_trk_n.SetActive(active);
     if(idscan_trk_index.IsAvailable()) idscan_trk_index.SetActive(active);
     if(sitrack_trk_n.IsAvailable()) sitrack_trk_n.SetActive(active);
     if(sitrack_trk_index.IsAvailable()) sitrack_trk_index.SetActive(active);
    }
    else
    {
      n.SetActive(active);
      pt.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      px.SetActive(active);
      py.SetActive(active);
      pz.SetActive(active);
      RoIWord.SetActive(active);
      simpleEtFlow.SetActive(active);
      nMatchedTracks.SetActive(active);
      trkAvgDist.SetActive(active);
      etOverPtLeadTrk.SetActive(active);
      cluster_quality.SetActive(active);
      cluster_EMenergy.SetActive(active);
      cluster_HADenergy.SetActive(active);
      cluster_eta.SetActive(active);
      cluster_phi.SetActive(active);
      cluster_CaloRadius.SetActive(active);
      cluster_EMRadius3S.SetActive(active);
      cluster_CoreFrac.SetActive(active);
      cluster_EMFrac.SetActive(active);
      cluster_HADRadius.SetActive(active);
      cluster_IsoFrac.SetActive(active);
      cluster_numTotCells.SetActive(active);
      cluster_stripWidth.SetActive(active);
      cluster_stripWidthOffline.SetActive(active);
      cluster_EMenergyWide.SetActive(active);
      cluster_EMenergyNarrow.SetActive(active);
      cluster_EMenergyMedium.SetActive(active);
      cluster_HADenergyWide.SetActive(active);
      cluster_HADenergyNarrow.SetActive(active);
      cluster_HADenergyMedium.SetActive(active);
      cluster_HADRadiusSamp.SetActive(active);
      cluster_EMRadiusSamp.SetActive(active);
      cluster_etNarrow.SetActive(active);
      cluster_etWide.SetActive(active);
      cluster_etMedium.SetActive(active);
      tracksinfo_nCoreTracks.SetActive(active);
      tracksinfo_nSlowTracks.SetActive(active);
      tracksinfo_nIsoTracks.SetActive(active);
      tracksinfo_charge.SetActive(active);
      tracksinfo_leadingTrackPt.SetActive(active);
      tracksinfo_scalarPtSumCore.SetActive(active);
      tracksinfo_scalarPtSumIso.SetActive(active);
      tracksinfo_3fastest_pt.SetActive(active);
      tracksinfo_3fastest_eta.SetActive(active);
      tracksinfo_3fastest_phi.SetActive(active);
      tracksinfo_3fastest_m.SetActive(active);
      tracks_algorithmId.SetActive(active);
      idscan_trk_n.SetActive(active);
      idscan_trk_index.SetActive(active);
      sitrack_trk_n.SetActive(active);
      sitrack_trk_index.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TrigL2TauD3PDCollection::ReadAllActive()
  {
    if(n.IsActive()) n();
    if(pt.IsActive()) pt();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(px.IsActive()) px();
    if(py.IsActive()) py();
    if(pz.IsActive()) pz();
    if(RoIWord.IsActive()) RoIWord();
    if(simpleEtFlow.IsActive()) simpleEtFlow();
    if(nMatchedTracks.IsActive()) nMatchedTracks();
    if(trkAvgDist.IsActive()) trkAvgDist();
    if(etOverPtLeadTrk.IsActive()) etOverPtLeadTrk();
    if(cluster_quality.IsActive()) cluster_quality();
    if(cluster_EMenergy.IsActive()) cluster_EMenergy();
    if(cluster_HADenergy.IsActive()) cluster_HADenergy();
    if(cluster_eta.IsActive()) cluster_eta();
    if(cluster_phi.IsActive()) cluster_phi();
    if(cluster_CaloRadius.IsActive()) cluster_CaloRadius();
    if(cluster_EMRadius3S.IsActive()) cluster_EMRadius3S();
    if(cluster_CoreFrac.IsActive()) cluster_CoreFrac();
    if(cluster_EMFrac.IsActive()) cluster_EMFrac();
    if(cluster_HADRadius.IsActive()) cluster_HADRadius();
    if(cluster_IsoFrac.IsActive()) cluster_IsoFrac();
    if(cluster_numTotCells.IsActive()) cluster_numTotCells();
    if(cluster_stripWidth.IsActive()) cluster_stripWidth();
    if(cluster_stripWidthOffline.IsActive()) cluster_stripWidthOffline();
    if(cluster_EMenergyWide.IsActive()) cluster_EMenergyWide();
    if(cluster_EMenergyNarrow.IsActive()) cluster_EMenergyNarrow();
    if(cluster_EMenergyMedium.IsActive()) cluster_EMenergyMedium();
    if(cluster_HADenergyWide.IsActive()) cluster_HADenergyWide();
    if(cluster_HADenergyNarrow.IsActive()) cluster_HADenergyNarrow();
    if(cluster_HADenergyMedium.IsActive()) cluster_HADenergyMedium();
    if(cluster_HADRadiusSamp.IsActive()) cluster_HADRadiusSamp();
    if(cluster_EMRadiusSamp.IsActive()) cluster_EMRadiusSamp();
    if(cluster_etNarrow.IsActive()) cluster_etNarrow();
    if(cluster_etWide.IsActive()) cluster_etWide();
    if(cluster_etMedium.IsActive()) cluster_etMedium();
    if(tracksinfo_nCoreTracks.IsActive()) tracksinfo_nCoreTracks();
    if(tracksinfo_nSlowTracks.IsActive()) tracksinfo_nSlowTracks();
    if(tracksinfo_nIsoTracks.IsActive()) tracksinfo_nIsoTracks();
    if(tracksinfo_charge.IsActive()) tracksinfo_charge();
    if(tracksinfo_leadingTrackPt.IsActive()) tracksinfo_leadingTrackPt();
    if(tracksinfo_scalarPtSumCore.IsActive()) tracksinfo_scalarPtSumCore();
    if(tracksinfo_scalarPtSumIso.IsActive()) tracksinfo_scalarPtSumIso();
    if(tracksinfo_3fastest_pt.IsActive()) tracksinfo_3fastest_pt();
    if(tracksinfo_3fastest_eta.IsActive()) tracksinfo_3fastest_eta();
    if(tracksinfo_3fastest_phi.IsActive()) tracksinfo_3fastest_phi();
    if(tracksinfo_3fastest_m.IsActive()) tracksinfo_3fastest_m();
    if(tracks_algorithmId.IsActive()) tracks_algorithmId();
    if(idscan_trk_n.IsActive()) idscan_trk_n();
    if(idscan_trk_index.IsActive()) idscan_trk_index();
    if(sitrack_trk_n.IsActive()) sitrack_trk_n();
    if(sitrack_trk_index.IsActive()) sitrack_trk_index();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TrigL2TauD3PDCollection_CXX
