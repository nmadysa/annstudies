// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TrigEFTauD3PDCollection_CXX
#define D3PDREADER_TrigEFTauD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TrigEFTauD3PDCollection.h"

ClassImp(D3PDReader::TrigEFTauD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigEFTauD3PDCollection::TrigEFTauD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    EF_tau125_medium1(prefix + "EF_tau125_medium1",&master),
    EF_tau125_medium1_L2StarA(prefix + "EF_tau125_medium1_L2StarA",&master),
    EF_tau125_medium1_L2StarB(prefix + "EF_tau125_medium1_L2StarB",&master),
    EF_tau125_medium1_L2StarC(prefix + "EF_tau125_medium1_L2StarC",&master),
    EF_tau125_medium1_llh(prefix + "EF_tau125_medium1_llh",&master),
    EF_tau20T_medium(prefix + "EF_tau20T_medium",&master),
    EF_tau20T_medium1(prefix + "EF_tau20T_medium1",&master),
    EF_tau20T_medium1_e15vh_medium1(prefix + "EF_tau20T_medium1_e15vh_medium1",&master),
    EF_tau20T_medium1_mu15i(prefix + "EF_tau20T_medium1_mu15i",&master),
    EF_tau20T_medium_mu15(prefix + "EF_tau20T_medium_mu15",&master),
    EF_tau20Ti_medium(prefix + "EF_tau20Ti_medium",&master),
    EF_tau20Ti_medium1(prefix + "EF_tau20Ti_medium1",&master),
    EF_tau20Ti_medium1_e18vh_medium1(prefix + "EF_tau20Ti_medium1_e18vh_medium1",&master),
    EF_tau20Ti_medium1_llh_e18vh_medium1(prefix + "EF_tau20Ti_medium1_llh_e18vh_medium1",&master),
    EF_tau20Ti_medium_e18vh_medium1(prefix + "EF_tau20Ti_medium_e18vh_medium1",&master),
    EF_tau20Ti_tight1(prefix + "EF_tau20Ti_tight1",&master),
    EF_tau20Ti_tight1_llh(prefix + "EF_tau20Ti_tight1_llh",&master),
    EF_tau20_medium(prefix + "EF_tau20_medium",&master),
    EF_tau20_medium1(prefix + "EF_tau20_medium1",&master),
    EF_tau20_medium1_llh(prefix + "EF_tau20_medium1_llh",&master),
    EF_tau20_medium1_llh_mu15(prefix + "EF_tau20_medium1_llh_mu15",&master),
    EF_tau20_medium1_mu15(prefix + "EF_tau20_medium1_mu15",&master),
    EF_tau20_medium1_mu15i(prefix + "EF_tau20_medium1_mu15i",&master),
    EF_tau20_medium1_mu18(prefix + "EF_tau20_medium1_mu18",&master),
    EF_tau20_medium_llh(prefix + "EF_tau20_medium_llh",&master),
    EF_tau20_medium_mu15(prefix + "EF_tau20_medium_mu15",&master),
    EF_tau29T_medium(prefix + "EF_tau29T_medium",&master),
    EF_tau29T_medium1(prefix + "EF_tau29T_medium1",&master),
    EF_tau29T_medium1_tau20T_medium1(prefix + "EF_tau29T_medium1_tau20T_medium1",&master),
    EF_tau29T_medium1_xe40_tight(prefix + "EF_tau29T_medium1_xe40_tight",&master),
    EF_tau29T_medium1_xe45_tight(prefix + "EF_tau29T_medium1_xe45_tight",&master),
    EF_tau29T_medium_xe40_tight(prefix + "EF_tau29T_medium_xe40_tight",&master),
    EF_tau29T_medium_xe45_tight(prefix + "EF_tau29T_medium_xe45_tight",&master),
    EF_tau29T_tight1(prefix + "EF_tau29T_tight1",&master),
    EF_tau29T_tight1_llh(prefix + "EF_tau29T_tight1_llh",&master),
    EF_tau29Ti_medium1(prefix + "EF_tau29Ti_medium1",&master),
    EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh(prefix + "EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh",&master),
    EF_tau29Ti_medium1_llh_xe40_tight(prefix + "EF_tau29Ti_medium1_llh_xe40_tight",&master),
    EF_tau29Ti_medium1_llh_xe45_tight(prefix + "EF_tau29Ti_medium1_llh_xe45_tight",&master),
    EF_tau29Ti_medium1_tau20Ti_medium1(prefix + "EF_tau29Ti_medium1_tau20Ti_medium1",&master),
    EF_tau29Ti_medium1_xe40_tight(prefix + "EF_tau29Ti_medium1_xe40_tight",&master),
    EF_tau29Ti_medium1_xe45_tight(prefix + "EF_tau29Ti_medium1_xe45_tight",&master),
    EF_tau29Ti_medium1_xe55_tclcw(prefix + "EF_tau29Ti_medium1_xe55_tclcw",&master),
    EF_tau29Ti_medium1_xe55_tclcw_tight(prefix + "EF_tau29Ti_medium1_xe55_tclcw_tight",&master),
    EF_tau29Ti_medium_xe40_tight(prefix + "EF_tau29Ti_medium_xe40_tight",&master),
    EF_tau29Ti_medium_xe45_tight(prefix + "EF_tau29Ti_medium_xe45_tight",&master),
    EF_tau29Ti_tight1(prefix + "EF_tau29Ti_tight1",&master),
    EF_tau29Ti_tight1_llh(prefix + "EF_tau29Ti_tight1_llh",&master),
    EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh(prefix + "EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh",&master),
    EF_tau29Ti_tight1_tau20Ti_tight1(prefix + "EF_tau29Ti_tight1_tau20Ti_tight1",&master),
    EF_tau29_IDTrkNoCut(prefix + "EF_tau29_IDTrkNoCut",&master),
    EF_tau29_medium(prefix + "EF_tau29_medium",&master),
    EF_tau29_medium1(prefix + "EF_tau29_medium1",&master),
    EF_tau29_medium1_llh(prefix + "EF_tau29_medium1_llh",&master),
    EF_tau29_medium_2stTest(prefix + "EF_tau29_medium_2stTest",&master),
    EF_tau29_medium_L2StarA(prefix + "EF_tau29_medium_L2StarA",&master),
    EF_tau29_medium_L2StarB(prefix + "EF_tau29_medium_L2StarB",&master),
    EF_tau29_medium_L2StarC(prefix + "EF_tau29_medium_L2StarC",&master),
    EF_tau29_medium_llh(prefix + "EF_tau29_medium_llh",&master),
    EF_tau29i_medium(prefix + "EF_tau29i_medium",&master),
    EF_tau29i_medium1(prefix + "EF_tau29i_medium1",&master),
    EF_tau38T_medium(prefix + "EF_tau38T_medium",&master),
    EF_tau38T_medium1(prefix + "EF_tau38T_medium1",&master),
    EF_tau38T_medium1_e18vh_medium1(prefix + "EF_tau38T_medium1_e18vh_medium1",&master),
    EF_tau38T_medium1_llh_e18vh_medium1(prefix + "EF_tau38T_medium1_llh_e18vh_medium1",&master),
    EF_tau38T_medium1_xe40_tight(prefix + "EF_tau38T_medium1_xe40_tight",&master),
    EF_tau38T_medium1_xe45_tight(prefix + "EF_tau38T_medium1_xe45_tight",&master),
    EF_tau38T_medium1_xe55_tclcw_tight(prefix + "EF_tau38T_medium1_xe55_tclcw_tight",&master),
    EF_tau38T_medium_e18vh_medium1(prefix + "EF_tau38T_medium_e18vh_medium1",&master),
    n(prefix + "n",&master),
    Et(prefix + "Et",&master),
    pt(prefix + "pt",&master),
    m(prefix + "m",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    px(prefix + "px",&master),
    py(prefix + "py",&master),
    pz(prefix + "pz",&master),
    author(prefix + "author",&master),
    RoIWord(prefix + "RoIWord",&master),
    nProng(prefix + "nProng",&master),
    numTrack(prefix + "numTrack",&master),
    seedCalo_numTrack(prefix + "seedCalo_numTrack",&master),
    seedCalo_nWideTrk(prefix + "seedCalo_nWideTrk",&master),
    nOtherTrk(prefix + "nOtherTrk",&master),
    seedCalo_EMRadius(prefix + "seedCalo_EMRadius",&master),
    seedCalo_hadRadius(prefix + "seedCalo_hadRadius",&master),
    seedCalo_etEMAtEMScale(prefix + "seedCalo_etEMAtEMScale",&master),
    seedCalo_etHadAtEMScale(prefix + "seedCalo_etHadAtEMScale",&master),
    seedCalo_isolFrac(prefix + "seedCalo_isolFrac",&master),
    seedCalo_centFrac(prefix + "seedCalo_centFrac",&master),
    seedCalo_etEMCalib(prefix + "seedCalo_etEMCalib",&master),
    seedCalo_etHadCalib(prefix + "seedCalo_etHadCalib",&master),
    seedCalo_eta(prefix + "seedCalo_eta",&master),
    seedCalo_phi(prefix + "seedCalo_phi",&master),
    seedCalo_stripWidth2(prefix + "seedCalo_stripWidth2",&master),
    seedCalo_nStrip(prefix + "seedCalo_nStrip",&master),
    seedCalo_trkAvgDist(prefix + "seedCalo_trkAvgDist",&master),
    seedCalo_trkRmsDist(prefix + "seedCalo_trkRmsDist",&master),
    seedCalo_dRmax(prefix + "seedCalo_dRmax",&master),
    seedCalo_lead2ClusterEOverAllClusterE(prefix + "seedCalo_lead2ClusterEOverAllClusterE",&master),
    seedCalo_lead3ClusterEOverAllClusterE(prefix + "seedCalo_lead3ClusterEOverAllClusterE",&master),
    etOverPtLeadTrk(prefix + "etOverPtLeadTrk",&master),
    ipZ0SinThetaSigLeadTrk(prefix + "ipZ0SinThetaSigLeadTrk",&master),
    LC_TES_precalib(prefix + "LC_TES_precalib",&master),
    leadTrkPt(prefix + "leadTrkPt",&master),
    ipSigLeadTrk(prefix + "ipSigLeadTrk",&master),
    trFlightPathSig(prefix + "trFlightPathSig",&master),
    numTopoClusters(prefix + "numTopoClusters",&master),
    numEffTopoClusters(prefix + "numEffTopoClusters",&master),
    topoInvMass(prefix + "topoInvMass",&master),
    effTopoInvMass(prefix + "effTopoInvMass",&master),
    topoMeanDeltaR(prefix + "topoMeanDeltaR",&master),
    effTopoMeanDeltaR(prefix + "effTopoMeanDeltaR",&master),
    numCells(prefix + "numCells",&master),
    massTrkSys(prefix + "massTrkSys",&master),
    BDTJetScore(prefix + "BDTJetScore",&master),
    likelihood(prefix + "likelihood",&master),
    track_n(prefix + "track_n",&master),
    track_d0(prefix + "track_d0",&master),
    track_z0(prefix + "track_z0",&master),
    track_phi(prefix + "track_phi",&master),
    track_theta(prefix + "track_theta",&master),
    track_qoverp(prefix + "track_qoverp",&master),
    track_pt(prefix + "track_pt",&master),
    track_eta(prefix + "track_eta",&master),
    track_atPV_d0(prefix + "track_atPV_d0",&master),
    track_atPV_z0(prefix + "track_atPV_z0",&master),
    track_atPV_phi(prefix + "track_atPV_phi",&master),
    track_atPV_theta(prefix + "track_atPV_theta",&master),
    track_atPV_qoverp(prefix + "track_atPV_qoverp",&master),
    track_atPV_pt(prefix + "track_atPV_pt",&master),
    track_atPV_eta(prefix + "track_atPV_eta",&master),
    track_nBLHits(prefix + "track_nBLHits",&master),
    track_nPixHits(prefix + "track_nPixHits",&master),
    track_nSCTHits(prefix + "track_nSCTHits",&master),
    track_nTRTHits(prefix + "track_nTRTHits",&master),
    track_nTRTHighTHits(prefix + "track_nTRTHighTHits",&master),
    track_nPixHoles(prefix + "track_nPixHoles",&master),
    track_nSCTHoles(prefix + "track_nSCTHoles",&master),
    track_nTRTHoles(prefix + "track_nTRTHoles",&master),
    track_nPixelDeadSensors(prefix + "track_nPixelDeadSensors",&master),
    track_nSCTDeadSensors(prefix + "track_nSCTDeadSensors",&master),
    track_nBLSharedHits(prefix + "track_nBLSharedHits",&master),
    track_nPixSharedHits(prefix + "track_nPixSharedHits",&master),
    track_nSCTSharedHits(prefix + "track_nSCTSharedHits",&master),
    track_nBLayerSplitHits(prefix + "track_nBLayerSplitHits",&master),
    track_nPixSplitHits(prefix + "track_nPixSplitHits",&master),
    track_nBLayerOutliers(prefix + "track_nBLayerOutliers",&master),
    track_nPixelOutliers(prefix + "track_nPixelOutliers",&master),
    track_nSCTOutliers(prefix + "track_nSCTOutliers",&master),
    track_nTRTOutliers(prefix + "track_nTRTOutliers",&master),
    track_nTRTHighTOutliers(prefix + "track_nTRTHighTOutliers",&master),
    track_nContribPixelLayers(prefix + "track_nContribPixelLayers",&master),
    track_nGangedPixels(prefix + "track_nGangedPixels",&master),
    track_nGangedFlaggedFakes(prefix + "track_nGangedFlaggedFakes",&master),
    track_nPixelSpoiltHits(prefix + "track_nPixelSpoiltHits",&master),
    track_nSCTDoubleHoles(prefix + "track_nSCTDoubleHoles",&master),
    track_nSCTSpoiltHits(prefix + "track_nSCTSpoiltHits",&master),
    track_expectBLayerHit(prefix + "track_expectBLayerHit",&master),
    track_nHits(prefix + "track_nHits",&master),
    track_TRTHighTHitsRatio(prefix + "track_TRTHighTHitsRatio",&master),
    track_TRTHighTOutliersRatio(prefix + "track_TRTHighTOutliersRatio",&master),
    seedCalo_track_n(prefix + "seedCalo_track_n",&master),
    seedCalo_wideTrk_n(prefix + "seedCalo_wideTrk_n",&master),
    seedCalo_wideTrk_d0(prefix + "seedCalo_wideTrk_d0",&master),
    seedCalo_wideTrk_z0(prefix + "seedCalo_wideTrk_z0",&master),
    seedCalo_wideTrk_phi(prefix + "seedCalo_wideTrk_phi",&master),
    seedCalo_wideTrk_theta(prefix + "seedCalo_wideTrk_theta",&master),
    seedCalo_wideTrk_qoverp(prefix + "seedCalo_wideTrk_qoverp",&master),
    seedCalo_wideTrk_pt(prefix + "seedCalo_wideTrk_pt",&master),
    seedCalo_wideTrk_eta(prefix + "seedCalo_wideTrk_eta",&master),
    seedCalo_wideTrk_atPV_d0(prefix + "seedCalo_wideTrk_atPV_d0",&master),
    seedCalo_wideTrk_atPV_z0(prefix + "seedCalo_wideTrk_atPV_z0",&master),
    seedCalo_wideTrk_atPV_phi(prefix + "seedCalo_wideTrk_atPV_phi",&master),
    seedCalo_wideTrk_atPV_theta(prefix + "seedCalo_wideTrk_atPV_theta",&master),
    seedCalo_wideTrk_atPV_qoverp(prefix + "seedCalo_wideTrk_atPV_qoverp",&master),
    seedCalo_wideTrk_atPV_pt(prefix + "seedCalo_wideTrk_atPV_pt",&master),
    seedCalo_wideTrk_atPV_eta(prefix + "seedCalo_wideTrk_atPV_eta",&master),
    seedCalo_wideTrk_nBLHits(prefix + "seedCalo_wideTrk_nBLHits",&master),
    seedCalo_wideTrk_nPixHits(prefix + "seedCalo_wideTrk_nPixHits",&master),
    seedCalo_wideTrk_nSCTHits(prefix + "seedCalo_wideTrk_nSCTHits",&master),
    seedCalo_wideTrk_nTRTHits(prefix + "seedCalo_wideTrk_nTRTHits",&master),
    seedCalo_wideTrk_nTRTHighTHits(prefix + "seedCalo_wideTrk_nTRTHighTHits",&master),
    seedCalo_wideTrk_nPixHoles(prefix + "seedCalo_wideTrk_nPixHoles",&master),
    seedCalo_wideTrk_nSCTHoles(prefix + "seedCalo_wideTrk_nSCTHoles",&master),
    seedCalo_wideTrk_nTRTHoles(prefix + "seedCalo_wideTrk_nTRTHoles",&master),
    seedCalo_wideTrk_nPixelDeadSensors(prefix + "seedCalo_wideTrk_nPixelDeadSensors",&master),
    seedCalo_wideTrk_nSCTDeadSensors(prefix + "seedCalo_wideTrk_nSCTDeadSensors",&master),
    seedCalo_wideTrk_nBLSharedHits(prefix + "seedCalo_wideTrk_nBLSharedHits",&master),
    seedCalo_wideTrk_nPixSharedHits(prefix + "seedCalo_wideTrk_nPixSharedHits",&master),
    seedCalo_wideTrk_nSCTSharedHits(prefix + "seedCalo_wideTrk_nSCTSharedHits",&master),
    seedCalo_wideTrk_nBLayerSplitHits(prefix + "seedCalo_wideTrk_nBLayerSplitHits",&master),
    seedCalo_wideTrk_nPixSplitHits(prefix + "seedCalo_wideTrk_nPixSplitHits",&master),
    seedCalo_wideTrk_nBLayerOutliers(prefix + "seedCalo_wideTrk_nBLayerOutliers",&master),
    seedCalo_wideTrk_nPixelOutliers(prefix + "seedCalo_wideTrk_nPixelOutliers",&master),
    seedCalo_wideTrk_nSCTOutliers(prefix + "seedCalo_wideTrk_nSCTOutliers",&master),
    seedCalo_wideTrk_nTRTOutliers(prefix + "seedCalo_wideTrk_nTRTOutliers",&master),
    seedCalo_wideTrk_nTRTHighTOutliers(prefix + "seedCalo_wideTrk_nTRTHighTOutliers",&master),
    seedCalo_wideTrk_nContribPixelLayers(prefix + "seedCalo_wideTrk_nContribPixelLayers",&master),
    seedCalo_wideTrk_nGangedPixels(prefix + "seedCalo_wideTrk_nGangedPixels",&master),
    seedCalo_wideTrk_nGangedFlaggedFakes(prefix + "seedCalo_wideTrk_nGangedFlaggedFakes",&master),
    seedCalo_wideTrk_nPixelSpoiltHits(prefix + "seedCalo_wideTrk_nPixelSpoiltHits",&master),
    seedCalo_wideTrk_nSCTDoubleHoles(prefix + "seedCalo_wideTrk_nSCTDoubleHoles",&master),
    seedCalo_wideTrk_nSCTSpoiltHits(prefix + "seedCalo_wideTrk_nSCTSpoiltHits",&master),
    seedCalo_wideTrk_expectBLayerHit(prefix + "seedCalo_wideTrk_expectBLayerHit",&master),
    seedCalo_wideTrk_nHits(prefix + "seedCalo_wideTrk_nHits",&master),
    otherTrk_n(prefix + "otherTrk_n",&master),
    otherTrk_d0(prefix + "otherTrk_d0",&master),
    otherTrk_z0(prefix + "otherTrk_z0",&master),
    otherTrk_phi(prefix + "otherTrk_phi",&master),
    otherTrk_theta(prefix + "otherTrk_theta",&master),
    otherTrk_qoverp(prefix + "otherTrk_qoverp",&master),
    otherTrk_pt(prefix + "otherTrk_pt",&master),
    otherTrk_eta(prefix + "otherTrk_eta",&master),
    otherTrk_atPV_d0(prefix + "otherTrk_atPV_d0",&master),
    otherTrk_atPV_z0(prefix + "otherTrk_atPV_z0",&master),
    otherTrk_atPV_phi(prefix + "otherTrk_atPV_phi",&master),
    otherTrk_atPV_theta(prefix + "otherTrk_atPV_theta",&master),
    otherTrk_atPV_qoverp(prefix + "otherTrk_atPV_qoverp",&master),
    otherTrk_atPV_pt(prefix + "otherTrk_atPV_pt",&master),
    otherTrk_atPV_eta(prefix + "otherTrk_atPV_eta",&master),
    otherTrk_nBLHits(prefix + "otherTrk_nBLHits",&master),
    otherTrk_nPixHits(prefix + "otherTrk_nPixHits",&master),
    otherTrk_nSCTHits(prefix + "otherTrk_nSCTHits",&master),
    otherTrk_nTRTHits(prefix + "otherTrk_nTRTHits",&master),
    otherTrk_nTRTHighTHits(prefix + "otherTrk_nTRTHighTHits",&master),
    otherTrk_nPixHoles(prefix + "otherTrk_nPixHoles",&master),
    otherTrk_nSCTHoles(prefix + "otherTrk_nSCTHoles",&master),
    otherTrk_nTRTHoles(prefix + "otherTrk_nTRTHoles",&master),
    otherTrk_nPixelDeadSensors(prefix + "otherTrk_nPixelDeadSensors",&master),
    otherTrk_nSCTDeadSensors(prefix + "otherTrk_nSCTDeadSensors",&master),
    otherTrk_nBLSharedHits(prefix + "otherTrk_nBLSharedHits",&master),
    otherTrk_nPixSharedHits(prefix + "otherTrk_nPixSharedHits",&master),
    otherTrk_nSCTSharedHits(prefix + "otherTrk_nSCTSharedHits",&master),
    otherTrk_nBLayerSplitHits(prefix + "otherTrk_nBLayerSplitHits",&master),
    otherTrk_nPixSplitHits(prefix + "otherTrk_nPixSplitHits",&master),
    otherTrk_nBLayerOutliers(prefix + "otherTrk_nBLayerOutliers",&master),
    otherTrk_nPixelOutliers(prefix + "otherTrk_nPixelOutliers",&master),
    otherTrk_nSCTOutliers(prefix + "otherTrk_nSCTOutliers",&master),
    otherTrk_nTRTOutliers(prefix + "otherTrk_nTRTOutliers",&master),
    otherTrk_nTRTHighTOutliers(prefix + "otherTrk_nTRTHighTOutliers",&master),
    otherTrk_nContribPixelLayers(prefix + "otherTrk_nContribPixelLayers",&master),
    otherTrk_nGangedPixels(prefix + "otherTrk_nGangedPixels",&master),
    otherTrk_nGangedFlaggedFakes(prefix + "otherTrk_nGangedFlaggedFakes",&master),
    otherTrk_nPixelSpoiltHits(prefix + "otherTrk_nPixelSpoiltHits",&master),
    otherTrk_nSCTDoubleHoles(prefix + "otherTrk_nSCTDoubleHoles",&master),
    otherTrk_nSCTSpoiltHits(prefix + "otherTrk_nSCTSpoiltHits",&master),
    otherTrk_expectBLayerHit(prefix + "otherTrk_expectBLayerHit",&master),
    otherTrk_nHits(prefix + "otherTrk_nHits",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigEFTauD3PDCollection::TrigEFTauD3PDCollection(const std::string& prefix):
    TObject(),
    EF_tau125_medium1(prefix + "EF_tau125_medium1",0),
    EF_tau125_medium1_L2StarA(prefix + "EF_tau125_medium1_L2StarA",0),
    EF_tau125_medium1_L2StarB(prefix + "EF_tau125_medium1_L2StarB",0),
    EF_tau125_medium1_L2StarC(prefix + "EF_tau125_medium1_L2StarC",0),
    EF_tau125_medium1_llh(prefix + "EF_tau125_medium1_llh",0),
    EF_tau20T_medium(prefix + "EF_tau20T_medium",0),
    EF_tau20T_medium1(prefix + "EF_tau20T_medium1",0),
    EF_tau20T_medium1_e15vh_medium1(prefix + "EF_tau20T_medium1_e15vh_medium1",0),
    EF_tau20T_medium1_mu15i(prefix + "EF_tau20T_medium1_mu15i",0),
    EF_tau20T_medium_mu15(prefix + "EF_tau20T_medium_mu15",0),
    EF_tau20Ti_medium(prefix + "EF_tau20Ti_medium",0),
    EF_tau20Ti_medium1(prefix + "EF_tau20Ti_medium1",0),
    EF_tau20Ti_medium1_e18vh_medium1(prefix + "EF_tau20Ti_medium1_e18vh_medium1",0),
    EF_tau20Ti_medium1_llh_e18vh_medium1(prefix + "EF_tau20Ti_medium1_llh_e18vh_medium1",0),
    EF_tau20Ti_medium_e18vh_medium1(prefix + "EF_tau20Ti_medium_e18vh_medium1",0),
    EF_tau20Ti_tight1(prefix + "EF_tau20Ti_tight1",0),
    EF_tau20Ti_tight1_llh(prefix + "EF_tau20Ti_tight1_llh",0),
    EF_tau20_medium(prefix + "EF_tau20_medium",0),
    EF_tau20_medium1(prefix + "EF_tau20_medium1",0),
    EF_tau20_medium1_llh(prefix + "EF_tau20_medium1_llh",0),
    EF_tau20_medium1_llh_mu15(prefix + "EF_tau20_medium1_llh_mu15",0),
    EF_tau20_medium1_mu15(prefix + "EF_tau20_medium1_mu15",0),
    EF_tau20_medium1_mu15i(prefix + "EF_tau20_medium1_mu15i",0),
    EF_tau20_medium1_mu18(prefix + "EF_tau20_medium1_mu18",0),
    EF_tau20_medium_llh(prefix + "EF_tau20_medium_llh",0),
    EF_tau20_medium_mu15(prefix + "EF_tau20_medium_mu15",0),
    EF_tau29T_medium(prefix + "EF_tau29T_medium",0),
    EF_tau29T_medium1(prefix + "EF_tau29T_medium1",0),
    EF_tau29T_medium1_tau20T_medium1(prefix + "EF_tau29T_medium1_tau20T_medium1",0),
    EF_tau29T_medium1_xe40_tight(prefix + "EF_tau29T_medium1_xe40_tight",0),
    EF_tau29T_medium1_xe45_tight(prefix + "EF_tau29T_medium1_xe45_tight",0),
    EF_tau29T_medium_xe40_tight(prefix + "EF_tau29T_medium_xe40_tight",0),
    EF_tau29T_medium_xe45_tight(prefix + "EF_tau29T_medium_xe45_tight",0),
    EF_tau29T_tight1(prefix + "EF_tau29T_tight1",0),
    EF_tau29T_tight1_llh(prefix + "EF_tau29T_tight1_llh",0),
    EF_tau29Ti_medium1(prefix + "EF_tau29Ti_medium1",0),
    EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh(prefix + "EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh",0),
    EF_tau29Ti_medium1_llh_xe40_tight(prefix + "EF_tau29Ti_medium1_llh_xe40_tight",0),
    EF_tau29Ti_medium1_llh_xe45_tight(prefix + "EF_tau29Ti_medium1_llh_xe45_tight",0),
    EF_tau29Ti_medium1_tau20Ti_medium1(prefix + "EF_tau29Ti_medium1_tau20Ti_medium1",0),
    EF_tau29Ti_medium1_xe40_tight(prefix + "EF_tau29Ti_medium1_xe40_tight",0),
    EF_tau29Ti_medium1_xe45_tight(prefix + "EF_tau29Ti_medium1_xe45_tight",0),
    EF_tau29Ti_medium1_xe55_tclcw(prefix + "EF_tau29Ti_medium1_xe55_tclcw",0),
    EF_tau29Ti_medium1_xe55_tclcw_tight(prefix + "EF_tau29Ti_medium1_xe55_tclcw_tight",0),
    EF_tau29Ti_medium_xe40_tight(prefix + "EF_tau29Ti_medium_xe40_tight",0),
    EF_tau29Ti_medium_xe45_tight(prefix + "EF_tau29Ti_medium_xe45_tight",0),
    EF_tau29Ti_tight1(prefix + "EF_tau29Ti_tight1",0),
    EF_tau29Ti_tight1_llh(prefix + "EF_tau29Ti_tight1_llh",0),
    EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh(prefix + "EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh",0),
    EF_tau29Ti_tight1_tau20Ti_tight1(prefix + "EF_tau29Ti_tight1_tau20Ti_tight1",0),
    EF_tau29_IDTrkNoCut(prefix + "EF_tau29_IDTrkNoCut",0),
    EF_tau29_medium(prefix + "EF_tau29_medium",0),
    EF_tau29_medium1(prefix + "EF_tau29_medium1",0),
    EF_tau29_medium1_llh(prefix + "EF_tau29_medium1_llh",0),
    EF_tau29_medium_2stTest(prefix + "EF_tau29_medium_2stTest",0),
    EF_tau29_medium_L2StarA(prefix + "EF_tau29_medium_L2StarA",0),
    EF_tau29_medium_L2StarB(prefix + "EF_tau29_medium_L2StarB",0),
    EF_tau29_medium_L2StarC(prefix + "EF_tau29_medium_L2StarC",0),
    EF_tau29_medium_llh(prefix + "EF_tau29_medium_llh",0),
    EF_tau29i_medium(prefix + "EF_tau29i_medium",0),
    EF_tau29i_medium1(prefix + "EF_tau29i_medium1",0),
    EF_tau38T_medium(prefix + "EF_tau38T_medium",0),
    EF_tau38T_medium1(prefix + "EF_tau38T_medium1",0),
    EF_tau38T_medium1_e18vh_medium1(prefix + "EF_tau38T_medium1_e18vh_medium1",0),
    EF_tau38T_medium1_llh_e18vh_medium1(prefix + "EF_tau38T_medium1_llh_e18vh_medium1",0),
    EF_tau38T_medium1_xe40_tight(prefix + "EF_tau38T_medium1_xe40_tight",0),
    EF_tau38T_medium1_xe45_tight(prefix + "EF_tau38T_medium1_xe45_tight",0),
    EF_tau38T_medium1_xe55_tclcw_tight(prefix + "EF_tau38T_medium1_xe55_tclcw_tight",0),
    EF_tau38T_medium_e18vh_medium1(prefix + "EF_tau38T_medium_e18vh_medium1",0),
    n(prefix + "n",0),
    Et(prefix + "Et",0),
    pt(prefix + "pt",0),
    m(prefix + "m",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    px(prefix + "px",0),
    py(prefix + "py",0),
    pz(prefix + "pz",0),
    author(prefix + "author",0),
    RoIWord(prefix + "RoIWord",0),
    nProng(prefix + "nProng",0),
    numTrack(prefix + "numTrack",0),
    seedCalo_numTrack(prefix + "seedCalo_numTrack",0),
    seedCalo_nWideTrk(prefix + "seedCalo_nWideTrk",0),
    nOtherTrk(prefix + "nOtherTrk",0),
    seedCalo_EMRadius(prefix + "seedCalo_EMRadius",0),
    seedCalo_hadRadius(prefix + "seedCalo_hadRadius",0),
    seedCalo_etEMAtEMScale(prefix + "seedCalo_etEMAtEMScale",0),
    seedCalo_etHadAtEMScale(prefix + "seedCalo_etHadAtEMScale",0),
    seedCalo_isolFrac(prefix + "seedCalo_isolFrac",0),
    seedCalo_centFrac(prefix + "seedCalo_centFrac",0),
    seedCalo_etEMCalib(prefix + "seedCalo_etEMCalib",0),
    seedCalo_etHadCalib(prefix + "seedCalo_etHadCalib",0),
    seedCalo_eta(prefix + "seedCalo_eta",0),
    seedCalo_phi(prefix + "seedCalo_phi",0),
    seedCalo_stripWidth2(prefix + "seedCalo_stripWidth2",0),
    seedCalo_nStrip(prefix + "seedCalo_nStrip",0),
    seedCalo_trkAvgDist(prefix + "seedCalo_trkAvgDist",0),
    seedCalo_trkRmsDist(prefix + "seedCalo_trkRmsDist",0),
    seedCalo_dRmax(prefix + "seedCalo_dRmax",0),
    seedCalo_lead2ClusterEOverAllClusterE(prefix + "seedCalo_lead2ClusterEOverAllClusterE",0),
    seedCalo_lead3ClusterEOverAllClusterE(prefix + "seedCalo_lead3ClusterEOverAllClusterE",0),
    etOverPtLeadTrk(prefix + "etOverPtLeadTrk",0),
    ipZ0SinThetaSigLeadTrk(prefix + "ipZ0SinThetaSigLeadTrk",0),
    LC_TES_precalib(prefix + "LC_TES_precalib",0),
    leadTrkPt(prefix + "leadTrkPt",0),
    ipSigLeadTrk(prefix + "ipSigLeadTrk",0),
    trFlightPathSig(prefix + "trFlightPathSig",0),
    numTopoClusters(prefix + "numTopoClusters",0),
    numEffTopoClusters(prefix + "numEffTopoClusters",0),
    topoInvMass(prefix + "topoInvMass",0),
    effTopoInvMass(prefix + "effTopoInvMass",0),
    topoMeanDeltaR(prefix + "topoMeanDeltaR",0),
    effTopoMeanDeltaR(prefix + "effTopoMeanDeltaR",0),
    numCells(prefix + "numCells",0),
    massTrkSys(prefix + "massTrkSys",0),
    BDTJetScore(prefix + "BDTJetScore",0),
    likelihood(prefix + "likelihood",0),
    track_n(prefix + "track_n",0),
    track_d0(prefix + "track_d0",0),
    track_z0(prefix + "track_z0",0),
    track_phi(prefix + "track_phi",0),
    track_theta(prefix + "track_theta",0),
    track_qoverp(prefix + "track_qoverp",0),
    track_pt(prefix + "track_pt",0),
    track_eta(prefix + "track_eta",0),
    track_atPV_d0(prefix + "track_atPV_d0",0),
    track_atPV_z0(prefix + "track_atPV_z0",0),
    track_atPV_phi(prefix + "track_atPV_phi",0),
    track_atPV_theta(prefix + "track_atPV_theta",0),
    track_atPV_qoverp(prefix + "track_atPV_qoverp",0),
    track_atPV_pt(prefix + "track_atPV_pt",0),
    track_atPV_eta(prefix + "track_atPV_eta",0),
    track_nBLHits(prefix + "track_nBLHits",0),
    track_nPixHits(prefix + "track_nPixHits",0),
    track_nSCTHits(prefix + "track_nSCTHits",0),
    track_nTRTHits(prefix + "track_nTRTHits",0),
    track_nTRTHighTHits(prefix + "track_nTRTHighTHits",0),
    track_nPixHoles(prefix + "track_nPixHoles",0),
    track_nSCTHoles(prefix + "track_nSCTHoles",0),
    track_nTRTHoles(prefix + "track_nTRTHoles",0),
    track_nPixelDeadSensors(prefix + "track_nPixelDeadSensors",0),
    track_nSCTDeadSensors(prefix + "track_nSCTDeadSensors",0),
    track_nBLSharedHits(prefix + "track_nBLSharedHits",0),
    track_nPixSharedHits(prefix + "track_nPixSharedHits",0),
    track_nSCTSharedHits(prefix + "track_nSCTSharedHits",0),
    track_nBLayerSplitHits(prefix + "track_nBLayerSplitHits",0),
    track_nPixSplitHits(prefix + "track_nPixSplitHits",0),
    track_nBLayerOutliers(prefix + "track_nBLayerOutliers",0),
    track_nPixelOutliers(prefix + "track_nPixelOutliers",0),
    track_nSCTOutliers(prefix + "track_nSCTOutliers",0),
    track_nTRTOutliers(prefix + "track_nTRTOutliers",0),
    track_nTRTHighTOutliers(prefix + "track_nTRTHighTOutliers",0),
    track_nContribPixelLayers(prefix + "track_nContribPixelLayers",0),
    track_nGangedPixels(prefix + "track_nGangedPixels",0),
    track_nGangedFlaggedFakes(prefix + "track_nGangedFlaggedFakes",0),
    track_nPixelSpoiltHits(prefix + "track_nPixelSpoiltHits",0),
    track_nSCTDoubleHoles(prefix + "track_nSCTDoubleHoles",0),
    track_nSCTSpoiltHits(prefix + "track_nSCTSpoiltHits",0),
    track_expectBLayerHit(prefix + "track_expectBLayerHit",0),
    track_nHits(prefix + "track_nHits",0),
    track_TRTHighTHitsRatio(prefix + "track_TRTHighTHitsRatio",0),
    track_TRTHighTOutliersRatio(prefix + "track_TRTHighTOutliersRatio",0),
    seedCalo_track_n(prefix + "seedCalo_track_n",0),
    seedCalo_wideTrk_n(prefix + "seedCalo_wideTrk_n",0),
    seedCalo_wideTrk_d0(prefix + "seedCalo_wideTrk_d0",0),
    seedCalo_wideTrk_z0(prefix + "seedCalo_wideTrk_z0",0),
    seedCalo_wideTrk_phi(prefix + "seedCalo_wideTrk_phi",0),
    seedCalo_wideTrk_theta(prefix + "seedCalo_wideTrk_theta",0),
    seedCalo_wideTrk_qoverp(prefix + "seedCalo_wideTrk_qoverp",0),
    seedCalo_wideTrk_pt(prefix + "seedCalo_wideTrk_pt",0),
    seedCalo_wideTrk_eta(prefix + "seedCalo_wideTrk_eta",0),
    seedCalo_wideTrk_atPV_d0(prefix + "seedCalo_wideTrk_atPV_d0",0),
    seedCalo_wideTrk_atPV_z0(prefix + "seedCalo_wideTrk_atPV_z0",0),
    seedCalo_wideTrk_atPV_phi(prefix + "seedCalo_wideTrk_atPV_phi",0),
    seedCalo_wideTrk_atPV_theta(prefix + "seedCalo_wideTrk_atPV_theta",0),
    seedCalo_wideTrk_atPV_qoverp(prefix + "seedCalo_wideTrk_atPV_qoverp",0),
    seedCalo_wideTrk_atPV_pt(prefix + "seedCalo_wideTrk_atPV_pt",0),
    seedCalo_wideTrk_atPV_eta(prefix + "seedCalo_wideTrk_atPV_eta",0),
    seedCalo_wideTrk_nBLHits(prefix + "seedCalo_wideTrk_nBLHits",0),
    seedCalo_wideTrk_nPixHits(prefix + "seedCalo_wideTrk_nPixHits",0),
    seedCalo_wideTrk_nSCTHits(prefix + "seedCalo_wideTrk_nSCTHits",0),
    seedCalo_wideTrk_nTRTHits(prefix + "seedCalo_wideTrk_nTRTHits",0),
    seedCalo_wideTrk_nTRTHighTHits(prefix + "seedCalo_wideTrk_nTRTHighTHits",0),
    seedCalo_wideTrk_nPixHoles(prefix + "seedCalo_wideTrk_nPixHoles",0),
    seedCalo_wideTrk_nSCTHoles(prefix + "seedCalo_wideTrk_nSCTHoles",0),
    seedCalo_wideTrk_nTRTHoles(prefix + "seedCalo_wideTrk_nTRTHoles",0),
    seedCalo_wideTrk_nPixelDeadSensors(prefix + "seedCalo_wideTrk_nPixelDeadSensors",0),
    seedCalo_wideTrk_nSCTDeadSensors(prefix + "seedCalo_wideTrk_nSCTDeadSensors",0),
    seedCalo_wideTrk_nBLSharedHits(prefix + "seedCalo_wideTrk_nBLSharedHits",0),
    seedCalo_wideTrk_nPixSharedHits(prefix + "seedCalo_wideTrk_nPixSharedHits",0),
    seedCalo_wideTrk_nSCTSharedHits(prefix + "seedCalo_wideTrk_nSCTSharedHits",0),
    seedCalo_wideTrk_nBLayerSplitHits(prefix + "seedCalo_wideTrk_nBLayerSplitHits",0),
    seedCalo_wideTrk_nPixSplitHits(prefix + "seedCalo_wideTrk_nPixSplitHits",0),
    seedCalo_wideTrk_nBLayerOutliers(prefix + "seedCalo_wideTrk_nBLayerOutliers",0),
    seedCalo_wideTrk_nPixelOutliers(prefix + "seedCalo_wideTrk_nPixelOutliers",0),
    seedCalo_wideTrk_nSCTOutliers(prefix + "seedCalo_wideTrk_nSCTOutliers",0),
    seedCalo_wideTrk_nTRTOutliers(prefix + "seedCalo_wideTrk_nTRTOutliers",0),
    seedCalo_wideTrk_nTRTHighTOutliers(prefix + "seedCalo_wideTrk_nTRTHighTOutliers",0),
    seedCalo_wideTrk_nContribPixelLayers(prefix + "seedCalo_wideTrk_nContribPixelLayers",0),
    seedCalo_wideTrk_nGangedPixels(prefix + "seedCalo_wideTrk_nGangedPixels",0),
    seedCalo_wideTrk_nGangedFlaggedFakes(prefix + "seedCalo_wideTrk_nGangedFlaggedFakes",0),
    seedCalo_wideTrk_nPixelSpoiltHits(prefix + "seedCalo_wideTrk_nPixelSpoiltHits",0),
    seedCalo_wideTrk_nSCTDoubleHoles(prefix + "seedCalo_wideTrk_nSCTDoubleHoles",0),
    seedCalo_wideTrk_nSCTSpoiltHits(prefix + "seedCalo_wideTrk_nSCTSpoiltHits",0),
    seedCalo_wideTrk_expectBLayerHit(prefix + "seedCalo_wideTrk_expectBLayerHit",0),
    seedCalo_wideTrk_nHits(prefix + "seedCalo_wideTrk_nHits",0),
    otherTrk_n(prefix + "otherTrk_n",0),
    otherTrk_d0(prefix + "otherTrk_d0",0),
    otherTrk_z0(prefix + "otherTrk_z0",0),
    otherTrk_phi(prefix + "otherTrk_phi",0),
    otherTrk_theta(prefix + "otherTrk_theta",0),
    otherTrk_qoverp(prefix + "otherTrk_qoverp",0),
    otherTrk_pt(prefix + "otherTrk_pt",0),
    otherTrk_eta(prefix + "otherTrk_eta",0),
    otherTrk_atPV_d0(prefix + "otherTrk_atPV_d0",0),
    otherTrk_atPV_z0(prefix + "otherTrk_atPV_z0",0),
    otherTrk_atPV_phi(prefix + "otherTrk_atPV_phi",0),
    otherTrk_atPV_theta(prefix + "otherTrk_atPV_theta",0),
    otherTrk_atPV_qoverp(prefix + "otherTrk_atPV_qoverp",0),
    otherTrk_atPV_pt(prefix + "otherTrk_atPV_pt",0),
    otherTrk_atPV_eta(prefix + "otherTrk_atPV_eta",0),
    otherTrk_nBLHits(prefix + "otherTrk_nBLHits",0),
    otherTrk_nPixHits(prefix + "otherTrk_nPixHits",0),
    otherTrk_nSCTHits(prefix + "otherTrk_nSCTHits",0),
    otherTrk_nTRTHits(prefix + "otherTrk_nTRTHits",0),
    otherTrk_nTRTHighTHits(prefix + "otherTrk_nTRTHighTHits",0),
    otherTrk_nPixHoles(prefix + "otherTrk_nPixHoles",0),
    otherTrk_nSCTHoles(prefix + "otherTrk_nSCTHoles",0),
    otherTrk_nTRTHoles(prefix + "otherTrk_nTRTHoles",0),
    otherTrk_nPixelDeadSensors(prefix + "otherTrk_nPixelDeadSensors",0),
    otherTrk_nSCTDeadSensors(prefix + "otherTrk_nSCTDeadSensors",0),
    otherTrk_nBLSharedHits(prefix + "otherTrk_nBLSharedHits",0),
    otherTrk_nPixSharedHits(prefix + "otherTrk_nPixSharedHits",0),
    otherTrk_nSCTSharedHits(prefix + "otherTrk_nSCTSharedHits",0),
    otherTrk_nBLayerSplitHits(prefix + "otherTrk_nBLayerSplitHits",0),
    otherTrk_nPixSplitHits(prefix + "otherTrk_nPixSplitHits",0),
    otherTrk_nBLayerOutliers(prefix + "otherTrk_nBLayerOutliers",0),
    otherTrk_nPixelOutliers(prefix + "otherTrk_nPixelOutliers",0),
    otherTrk_nSCTOutliers(prefix + "otherTrk_nSCTOutliers",0),
    otherTrk_nTRTOutliers(prefix + "otherTrk_nTRTOutliers",0),
    otherTrk_nTRTHighTOutliers(prefix + "otherTrk_nTRTHighTOutliers",0),
    otherTrk_nContribPixelLayers(prefix + "otherTrk_nContribPixelLayers",0),
    otherTrk_nGangedPixels(prefix + "otherTrk_nGangedPixels",0),
    otherTrk_nGangedFlaggedFakes(prefix + "otherTrk_nGangedFlaggedFakes",0),
    otherTrk_nPixelSpoiltHits(prefix + "otherTrk_nPixelSpoiltHits",0),
    otherTrk_nSCTDoubleHoles(prefix + "otherTrk_nSCTDoubleHoles",0),
    otherTrk_nSCTSpoiltHits(prefix + "otherTrk_nSCTSpoiltHits",0),
    otherTrk_expectBLayerHit(prefix + "otherTrk_expectBLayerHit",0),
    otherTrk_nHits(prefix + "otherTrk_nHits",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TrigEFTauD3PDCollection::ReadFrom(TTree* tree)
  {
    EF_tau125_medium1.ReadFrom(tree);
    EF_tau125_medium1_L2StarA.ReadFrom(tree);
    EF_tau125_medium1_L2StarB.ReadFrom(tree);
    EF_tau125_medium1_L2StarC.ReadFrom(tree);
    EF_tau125_medium1_llh.ReadFrom(tree);
    EF_tau20T_medium.ReadFrom(tree);
    EF_tau20T_medium1.ReadFrom(tree);
    EF_tau20T_medium1_e15vh_medium1.ReadFrom(tree);
    EF_tau20T_medium1_mu15i.ReadFrom(tree);
    EF_tau20T_medium_mu15.ReadFrom(tree);
    EF_tau20Ti_medium.ReadFrom(tree);
    EF_tau20Ti_medium1.ReadFrom(tree);
    EF_tau20Ti_medium1_e18vh_medium1.ReadFrom(tree);
    EF_tau20Ti_medium1_llh_e18vh_medium1.ReadFrom(tree);
    EF_tau20Ti_medium_e18vh_medium1.ReadFrom(tree);
    EF_tau20Ti_tight1.ReadFrom(tree);
    EF_tau20Ti_tight1_llh.ReadFrom(tree);
    EF_tau20_medium.ReadFrom(tree);
    EF_tau20_medium1.ReadFrom(tree);
    EF_tau20_medium1_llh.ReadFrom(tree);
    EF_tau20_medium1_llh_mu15.ReadFrom(tree);
    EF_tau20_medium1_mu15.ReadFrom(tree);
    EF_tau20_medium1_mu15i.ReadFrom(tree);
    EF_tau20_medium1_mu18.ReadFrom(tree);
    EF_tau20_medium_llh.ReadFrom(tree);
    EF_tau20_medium_mu15.ReadFrom(tree);
    EF_tau29T_medium.ReadFrom(tree);
    EF_tau29T_medium1.ReadFrom(tree);
    EF_tau29T_medium1_tau20T_medium1.ReadFrom(tree);
    EF_tau29T_medium1_xe40_tight.ReadFrom(tree);
    EF_tau29T_medium1_xe45_tight.ReadFrom(tree);
    EF_tau29T_medium_xe40_tight.ReadFrom(tree);
    EF_tau29T_medium_xe45_tight.ReadFrom(tree);
    EF_tau29T_tight1.ReadFrom(tree);
    EF_tau29T_tight1_llh.ReadFrom(tree);
    EF_tau29Ti_medium1.ReadFrom(tree);
    EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh.ReadFrom(tree);
    EF_tau29Ti_medium1_llh_xe40_tight.ReadFrom(tree);
    EF_tau29Ti_medium1_llh_xe45_tight.ReadFrom(tree);
    EF_tau29Ti_medium1_tau20Ti_medium1.ReadFrom(tree);
    EF_tau29Ti_medium1_xe40_tight.ReadFrom(tree);
    EF_tau29Ti_medium1_xe45_tight.ReadFrom(tree);
    EF_tau29Ti_medium1_xe55_tclcw.ReadFrom(tree);
    EF_tau29Ti_medium1_xe55_tclcw_tight.ReadFrom(tree);
    EF_tau29Ti_medium_xe40_tight.ReadFrom(tree);
    EF_tau29Ti_medium_xe45_tight.ReadFrom(tree);
    EF_tau29Ti_tight1.ReadFrom(tree);
    EF_tau29Ti_tight1_llh.ReadFrom(tree);
    EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh.ReadFrom(tree);
    EF_tau29Ti_tight1_tau20Ti_tight1.ReadFrom(tree);
    EF_tau29_IDTrkNoCut.ReadFrom(tree);
    EF_tau29_medium.ReadFrom(tree);
    EF_tau29_medium1.ReadFrom(tree);
    EF_tau29_medium1_llh.ReadFrom(tree);
    EF_tau29_medium_2stTest.ReadFrom(tree);
    EF_tau29_medium_L2StarA.ReadFrom(tree);
    EF_tau29_medium_L2StarB.ReadFrom(tree);
    EF_tau29_medium_L2StarC.ReadFrom(tree);
    EF_tau29_medium_llh.ReadFrom(tree);
    EF_tau29i_medium.ReadFrom(tree);
    EF_tau29i_medium1.ReadFrom(tree);
    EF_tau38T_medium.ReadFrom(tree);
    EF_tau38T_medium1.ReadFrom(tree);
    EF_tau38T_medium1_e18vh_medium1.ReadFrom(tree);
    EF_tau38T_medium1_llh_e18vh_medium1.ReadFrom(tree);
    EF_tau38T_medium1_xe40_tight.ReadFrom(tree);
    EF_tau38T_medium1_xe45_tight.ReadFrom(tree);
    EF_tau38T_medium1_xe55_tclcw_tight.ReadFrom(tree);
    EF_tau38T_medium_e18vh_medium1.ReadFrom(tree);
    n.ReadFrom(tree);
    Et.ReadFrom(tree);
    pt.ReadFrom(tree);
    m.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    px.ReadFrom(tree);
    py.ReadFrom(tree);
    pz.ReadFrom(tree);
    author.ReadFrom(tree);
    RoIWord.ReadFrom(tree);
    nProng.ReadFrom(tree);
    numTrack.ReadFrom(tree);
    seedCalo_numTrack.ReadFrom(tree);
    seedCalo_nWideTrk.ReadFrom(tree);
    nOtherTrk.ReadFrom(tree);
    seedCalo_EMRadius.ReadFrom(tree);
    seedCalo_hadRadius.ReadFrom(tree);
    seedCalo_etEMAtEMScale.ReadFrom(tree);
    seedCalo_etHadAtEMScale.ReadFrom(tree);
    seedCalo_isolFrac.ReadFrom(tree);
    seedCalo_centFrac.ReadFrom(tree);
    seedCalo_etEMCalib.ReadFrom(tree);
    seedCalo_etHadCalib.ReadFrom(tree);
    seedCalo_eta.ReadFrom(tree);
    seedCalo_phi.ReadFrom(tree);
    seedCalo_stripWidth2.ReadFrom(tree);
    seedCalo_nStrip.ReadFrom(tree);
    seedCalo_trkAvgDist.ReadFrom(tree);
    seedCalo_trkRmsDist.ReadFrom(tree);
    seedCalo_dRmax.ReadFrom(tree);
    seedCalo_lead2ClusterEOverAllClusterE.ReadFrom(tree);
    seedCalo_lead3ClusterEOverAllClusterE.ReadFrom(tree);
    etOverPtLeadTrk.ReadFrom(tree);
    ipZ0SinThetaSigLeadTrk.ReadFrom(tree);
    LC_TES_precalib.ReadFrom(tree);
    leadTrkPt.ReadFrom(tree);
    ipSigLeadTrk.ReadFrom(tree);
    trFlightPathSig.ReadFrom(tree);
    numTopoClusters.ReadFrom(tree);
    numEffTopoClusters.ReadFrom(tree);
    topoInvMass.ReadFrom(tree);
    effTopoInvMass.ReadFrom(tree);
    topoMeanDeltaR.ReadFrom(tree);
    effTopoMeanDeltaR.ReadFrom(tree);
    numCells.ReadFrom(tree);
    massTrkSys.ReadFrom(tree);
    BDTJetScore.ReadFrom(tree);
    likelihood.ReadFrom(tree);
    track_n.ReadFrom(tree);
    track_d0.ReadFrom(tree);
    track_z0.ReadFrom(tree);
    track_phi.ReadFrom(tree);
    track_theta.ReadFrom(tree);
    track_qoverp.ReadFrom(tree);
    track_pt.ReadFrom(tree);
    track_eta.ReadFrom(tree);
    track_atPV_d0.ReadFrom(tree);
    track_atPV_z0.ReadFrom(tree);
    track_atPV_phi.ReadFrom(tree);
    track_atPV_theta.ReadFrom(tree);
    track_atPV_qoverp.ReadFrom(tree);
    track_atPV_pt.ReadFrom(tree);
    track_atPV_eta.ReadFrom(tree);
    track_nBLHits.ReadFrom(tree);
    track_nPixHits.ReadFrom(tree);
    track_nSCTHits.ReadFrom(tree);
    track_nTRTHits.ReadFrom(tree);
    track_nTRTHighTHits.ReadFrom(tree);
    track_nPixHoles.ReadFrom(tree);
    track_nSCTHoles.ReadFrom(tree);
    track_nTRTHoles.ReadFrom(tree);
    track_nPixelDeadSensors.ReadFrom(tree);
    track_nSCTDeadSensors.ReadFrom(tree);
    track_nBLSharedHits.ReadFrom(tree);
    track_nPixSharedHits.ReadFrom(tree);
    track_nSCTSharedHits.ReadFrom(tree);
    track_nBLayerSplitHits.ReadFrom(tree);
    track_nPixSplitHits.ReadFrom(tree);
    track_nBLayerOutliers.ReadFrom(tree);
    track_nPixelOutliers.ReadFrom(tree);
    track_nSCTOutliers.ReadFrom(tree);
    track_nTRTOutliers.ReadFrom(tree);
    track_nTRTHighTOutliers.ReadFrom(tree);
    track_nContribPixelLayers.ReadFrom(tree);
    track_nGangedPixels.ReadFrom(tree);
    track_nGangedFlaggedFakes.ReadFrom(tree);
    track_nPixelSpoiltHits.ReadFrom(tree);
    track_nSCTDoubleHoles.ReadFrom(tree);
    track_nSCTSpoiltHits.ReadFrom(tree);
    track_expectBLayerHit.ReadFrom(tree);
    track_nHits.ReadFrom(tree);
    track_TRTHighTHitsRatio.ReadFrom(tree);
    track_TRTHighTOutliersRatio.ReadFrom(tree);
    seedCalo_track_n.ReadFrom(tree);
    seedCalo_wideTrk_n.ReadFrom(tree);
    seedCalo_wideTrk_d0.ReadFrom(tree);
    seedCalo_wideTrk_z0.ReadFrom(tree);
    seedCalo_wideTrk_phi.ReadFrom(tree);
    seedCalo_wideTrk_theta.ReadFrom(tree);
    seedCalo_wideTrk_qoverp.ReadFrom(tree);
    seedCalo_wideTrk_pt.ReadFrom(tree);
    seedCalo_wideTrk_eta.ReadFrom(tree);
    seedCalo_wideTrk_atPV_d0.ReadFrom(tree);
    seedCalo_wideTrk_atPV_z0.ReadFrom(tree);
    seedCalo_wideTrk_atPV_phi.ReadFrom(tree);
    seedCalo_wideTrk_atPV_theta.ReadFrom(tree);
    seedCalo_wideTrk_atPV_qoverp.ReadFrom(tree);
    seedCalo_wideTrk_atPV_pt.ReadFrom(tree);
    seedCalo_wideTrk_atPV_eta.ReadFrom(tree);
    seedCalo_wideTrk_nBLHits.ReadFrom(tree);
    seedCalo_wideTrk_nPixHits.ReadFrom(tree);
    seedCalo_wideTrk_nSCTHits.ReadFrom(tree);
    seedCalo_wideTrk_nTRTHits.ReadFrom(tree);
    seedCalo_wideTrk_nTRTHighTHits.ReadFrom(tree);
    seedCalo_wideTrk_nPixHoles.ReadFrom(tree);
    seedCalo_wideTrk_nSCTHoles.ReadFrom(tree);
    seedCalo_wideTrk_nTRTHoles.ReadFrom(tree);
    seedCalo_wideTrk_nPixelDeadSensors.ReadFrom(tree);
    seedCalo_wideTrk_nSCTDeadSensors.ReadFrom(tree);
    seedCalo_wideTrk_nBLSharedHits.ReadFrom(tree);
    seedCalo_wideTrk_nPixSharedHits.ReadFrom(tree);
    seedCalo_wideTrk_nSCTSharedHits.ReadFrom(tree);
    seedCalo_wideTrk_nBLayerSplitHits.ReadFrom(tree);
    seedCalo_wideTrk_nPixSplitHits.ReadFrom(tree);
    seedCalo_wideTrk_nBLayerOutliers.ReadFrom(tree);
    seedCalo_wideTrk_nPixelOutliers.ReadFrom(tree);
    seedCalo_wideTrk_nSCTOutliers.ReadFrom(tree);
    seedCalo_wideTrk_nTRTOutliers.ReadFrom(tree);
    seedCalo_wideTrk_nTRTHighTOutliers.ReadFrom(tree);
    seedCalo_wideTrk_nContribPixelLayers.ReadFrom(tree);
    seedCalo_wideTrk_nGangedPixels.ReadFrom(tree);
    seedCalo_wideTrk_nGangedFlaggedFakes.ReadFrom(tree);
    seedCalo_wideTrk_nPixelSpoiltHits.ReadFrom(tree);
    seedCalo_wideTrk_nSCTDoubleHoles.ReadFrom(tree);
    seedCalo_wideTrk_nSCTSpoiltHits.ReadFrom(tree);
    seedCalo_wideTrk_expectBLayerHit.ReadFrom(tree);
    seedCalo_wideTrk_nHits.ReadFrom(tree);
    otherTrk_n.ReadFrom(tree);
    otherTrk_d0.ReadFrom(tree);
    otherTrk_z0.ReadFrom(tree);
    otherTrk_phi.ReadFrom(tree);
    otherTrk_theta.ReadFrom(tree);
    otherTrk_qoverp.ReadFrom(tree);
    otherTrk_pt.ReadFrom(tree);
    otherTrk_eta.ReadFrom(tree);
    otherTrk_atPV_d0.ReadFrom(tree);
    otherTrk_atPV_z0.ReadFrom(tree);
    otherTrk_atPV_phi.ReadFrom(tree);
    otherTrk_atPV_theta.ReadFrom(tree);
    otherTrk_atPV_qoverp.ReadFrom(tree);
    otherTrk_atPV_pt.ReadFrom(tree);
    otherTrk_atPV_eta.ReadFrom(tree);
    otherTrk_nBLHits.ReadFrom(tree);
    otherTrk_nPixHits.ReadFrom(tree);
    otherTrk_nSCTHits.ReadFrom(tree);
    otherTrk_nTRTHits.ReadFrom(tree);
    otherTrk_nTRTHighTHits.ReadFrom(tree);
    otherTrk_nPixHoles.ReadFrom(tree);
    otherTrk_nSCTHoles.ReadFrom(tree);
    otherTrk_nTRTHoles.ReadFrom(tree);
    otherTrk_nPixelDeadSensors.ReadFrom(tree);
    otherTrk_nSCTDeadSensors.ReadFrom(tree);
    otherTrk_nBLSharedHits.ReadFrom(tree);
    otherTrk_nPixSharedHits.ReadFrom(tree);
    otherTrk_nSCTSharedHits.ReadFrom(tree);
    otherTrk_nBLayerSplitHits.ReadFrom(tree);
    otherTrk_nPixSplitHits.ReadFrom(tree);
    otherTrk_nBLayerOutliers.ReadFrom(tree);
    otherTrk_nPixelOutliers.ReadFrom(tree);
    otherTrk_nSCTOutliers.ReadFrom(tree);
    otherTrk_nTRTOutliers.ReadFrom(tree);
    otherTrk_nTRTHighTOutliers.ReadFrom(tree);
    otherTrk_nContribPixelLayers.ReadFrom(tree);
    otherTrk_nGangedPixels.ReadFrom(tree);
    otherTrk_nGangedFlaggedFakes.ReadFrom(tree);
    otherTrk_nPixelSpoiltHits.ReadFrom(tree);
    otherTrk_nSCTDoubleHoles.ReadFrom(tree);
    otherTrk_nSCTSpoiltHits.ReadFrom(tree);
    otherTrk_expectBLayerHit.ReadFrom(tree);
    otherTrk_nHits.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TrigEFTauD3PDCollection::WriteTo(TTree* tree)
  {
    EF_tau125_medium1.WriteTo(tree);
    EF_tau125_medium1_L2StarA.WriteTo(tree);
    EF_tau125_medium1_L2StarB.WriteTo(tree);
    EF_tau125_medium1_L2StarC.WriteTo(tree);
    EF_tau125_medium1_llh.WriteTo(tree);
    EF_tau20T_medium.WriteTo(tree);
    EF_tau20T_medium1.WriteTo(tree);
    EF_tau20T_medium1_e15vh_medium1.WriteTo(tree);
    EF_tau20T_medium1_mu15i.WriteTo(tree);
    EF_tau20T_medium_mu15.WriteTo(tree);
    EF_tau20Ti_medium.WriteTo(tree);
    EF_tau20Ti_medium1.WriteTo(tree);
    EF_tau20Ti_medium1_e18vh_medium1.WriteTo(tree);
    EF_tau20Ti_medium1_llh_e18vh_medium1.WriteTo(tree);
    EF_tau20Ti_medium_e18vh_medium1.WriteTo(tree);
    EF_tau20Ti_tight1.WriteTo(tree);
    EF_tau20Ti_tight1_llh.WriteTo(tree);
    EF_tau20_medium.WriteTo(tree);
    EF_tau20_medium1.WriteTo(tree);
    EF_tau20_medium1_llh.WriteTo(tree);
    EF_tau20_medium1_llh_mu15.WriteTo(tree);
    EF_tau20_medium1_mu15.WriteTo(tree);
    EF_tau20_medium1_mu15i.WriteTo(tree);
    EF_tau20_medium1_mu18.WriteTo(tree);
    EF_tau20_medium_llh.WriteTo(tree);
    EF_tau20_medium_mu15.WriteTo(tree);
    EF_tau29T_medium.WriteTo(tree);
    EF_tau29T_medium1.WriteTo(tree);
    EF_tau29T_medium1_tau20T_medium1.WriteTo(tree);
    EF_tau29T_medium1_xe40_tight.WriteTo(tree);
    EF_tau29T_medium1_xe45_tight.WriteTo(tree);
    EF_tau29T_medium_xe40_tight.WriteTo(tree);
    EF_tau29T_medium_xe45_tight.WriteTo(tree);
    EF_tau29T_tight1.WriteTo(tree);
    EF_tau29T_tight1_llh.WriteTo(tree);
    EF_tau29Ti_medium1.WriteTo(tree);
    EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh.WriteTo(tree);
    EF_tau29Ti_medium1_llh_xe40_tight.WriteTo(tree);
    EF_tau29Ti_medium1_llh_xe45_tight.WriteTo(tree);
    EF_tau29Ti_medium1_tau20Ti_medium1.WriteTo(tree);
    EF_tau29Ti_medium1_xe40_tight.WriteTo(tree);
    EF_tau29Ti_medium1_xe45_tight.WriteTo(tree);
    EF_tau29Ti_medium1_xe55_tclcw.WriteTo(tree);
    EF_tau29Ti_medium1_xe55_tclcw_tight.WriteTo(tree);
    EF_tau29Ti_medium_xe40_tight.WriteTo(tree);
    EF_tau29Ti_medium_xe45_tight.WriteTo(tree);
    EF_tau29Ti_tight1.WriteTo(tree);
    EF_tau29Ti_tight1_llh.WriteTo(tree);
    EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh.WriteTo(tree);
    EF_tau29Ti_tight1_tau20Ti_tight1.WriteTo(tree);
    EF_tau29_IDTrkNoCut.WriteTo(tree);
    EF_tau29_medium.WriteTo(tree);
    EF_tau29_medium1.WriteTo(tree);
    EF_tau29_medium1_llh.WriteTo(tree);
    EF_tau29_medium_2stTest.WriteTo(tree);
    EF_tau29_medium_L2StarA.WriteTo(tree);
    EF_tau29_medium_L2StarB.WriteTo(tree);
    EF_tau29_medium_L2StarC.WriteTo(tree);
    EF_tau29_medium_llh.WriteTo(tree);
    EF_tau29i_medium.WriteTo(tree);
    EF_tau29i_medium1.WriteTo(tree);
    EF_tau38T_medium.WriteTo(tree);
    EF_tau38T_medium1.WriteTo(tree);
    EF_tau38T_medium1_e18vh_medium1.WriteTo(tree);
    EF_tau38T_medium1_llh_e18vh_medium1.WriteTo(tree);
    EF_tau38T_medium1_xe40_tight.WriteTo(tree);
    EF_tau38T_medium1_xe45_tight.WriteTo(tree);
    EF_tau38T_medium1_xe55_tclcw_tight.WriteTo(tree);
    EF_tau38T_medium_e18vh_medium1.WriteTo(tree);
    n.WriteTo(tree);
    Et.WriteTo(tree);
    pt.WriteTo(tree);
    m.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    px.WriteTo(tree);
    py.WriteTo(tree);
    pz.WriteTo(tree);
    author.WriteTo(tree);
    RoIWord.WriteTo(tree);
    nProng.WriteTo(tree);
    numTrack.WriteTo(tree);
    seedCalo_numTrack.WriteTo(tree);
    seedCalo_nWideTrk.WriteTo(tree);
    nOtherTrk.WriteTo(tree);
    seedCalo_EMRadius.WriteTo(tree);
    seedCalo_hadRadius.WriteTo(tree);
    seedCalo_etEMAtEMScale.WriteTo(tree);
    seedCalo_etHadAtEMScale.WriteTo(tree);
    seedCalo_isolFrac.WriteTo(tree);
    seedCalo_centFrac.WriteTo(tree);
    seedCalo_etEMCalib.WriteTo(tree);
    seedCalo_etHadCalib.WriteTo(tree);
    seedCalo_eta.WriteTo(tree);
    seedCalo_phi.WriteTo(tree);
    seedCalo_stripWidth2.WriteTo(tree);
    seedCalo_nStrip.WriteTo(tree);
    seedCalo_trkAvgDist.WriteTo(tree);
    seedCalo_trkRmsDist.WriteTo(tree);
    seedCalo_dRmax.WriteTo(tree);
    seedCalo_lead2ClusterEOverAllClusterE.WriteTo(tree);
    seedCalo_lead3ClusterEOverAllClusterE.WriteTo(tree);
    etOverPtLeadTrk.WriteTo(tree);
    ipZ0SinThetaSigLeadTrk.WriteTo(tree);
    LC_TES_precalib.WriteTo(tree);
    leadTrkPt.WriteTo(tree);
    ipSigLeadTrk.WriteTo(tree);
    trFlightPathSig.WriteTo(tree);
    numTopoClusters.WriteTo(tree);
    numEffTopoClusters.WriteTo(tree);
    topoInvMass.WriteTo(tree);
    effTopoInvMass.WriteTo(tree);
    topoMeanDeltaR.WriteTo(tree);
    effTopoMeanDeltaR.WriteTo(tree);
    numCells.WriteTo(tree);
    massTrkSys.WriteTo(tree);
    BDTJetScore.WriteTo(tree);
    likelihood.WriteTo(tree);
    track_n.WriteTo(tree);
    track_d0.WriteTo(tree);
    track_z0.WriteTo(tree);
    track_phi.WriteTo(tree);
    track_theta.WriteTo(tree);
    track_qoverp.WriteTo(tree);
    track_pt.WriteTo(tree);
    track_eta.WriteTo(tree);
    track_atPV_d0.WriteTo(tree);
    track_atPV_z0.WriteTo(tree);
    track_atPV_phi.WriteTo(tree);
    track_atPV_theta.WriteTo(tree);
    track_atPV_qoverp.WriteTo(tree);
    track_atPV_pt.WriteTo(tree);
    track_atPV_eta.WriteTo(tree);
    track_nBLHits.WriteTo(tree);
    track_nPixHits.WriteTo(tree);
    track_nSCTHits.WriteTo(tree);
    track_nTRTHits.WriteTo(tree);
    track_nTRTHighTHits.WriteTo(tree);
    track_nPixHoles.WriteTo(tree);
    track_nSCTHoles.WriteTo(tree);
    track_nTRTHoles.WriteTo(tree);
    track_nPixelDeadSensors.WriteTo(tree);
    track_nSCTDeadSensors.WriteTo(tree);
    track_nBLSharedHits.WriteTo(tree);
    track_nPixSharedHits.WriteTo(tree);
    track_nSCTSharedHits.WriteTo(tree);
    track_nBLayerSplitHits.WriteTo(tree);
    track_nPixSplitHits.WriteTo(tree);
    track_nBLayerOutliers.WriteTo(tree);
    track_nPixelOutliers.WriteTo(tree);
    track_nSCTOutliers.WriteTo(tree);
    track_nTRTOutliers.WriteTo(tree);
    track_nTRTHighTOutliers.WriteTo(tree);
    track_nContribPixelLayers.WriteTo(tree);
    track_nGangedPixels.WriteTo(tree);
    track_nGangedFlaggedFakes.WriteTo(tree);
    track_nPixelSpoiltHits.WriteTo(tree);
    track_nSCTDoubleHoles.WriteTo(tree);
    track_nSCTSpoiltHits.WriteTo(tree);
    track_expectBLayerHit.WriteTo(tree);
    track_nHits.WriteTo(tree);
    track_TRTHighTHitsRatio.WriteTo(tree);
    track_TRTHighTOutliersRatio.WriteTo(tree);
    seedCalo_track_n.WriteTo(tree);
    seedCalo_wideTrk_n.WriteTo(tree);
    seedCalo_wideTrk_d0.WriteTo(tree);
    seedCalo_wideTrk_z0.WriteTo(tree);
    seedCalo_wideTrk_phi.WriteTo(tree);
    seedCalo_wideTrk_theta.WriteTo(tree);
    seedCalo_wideTrk_qoverp.WriteTo(tree);
    seedCalo_wideTrk_pt.WriteTo(tree);
    seedCalo_wideTrk_eta.WriteTo(tree);
    seedCalo_wideTrk_atPV_d0.WriteTo(tree);
    seedCalo_wideTrk_atPV_z0.WriteTo(tree);
    seedCalo_wideTrk_atPV_phi.WriteTo(tree);
    seedCalo_wideTrk_atPV_theta.WriteTo(tree);
    seedCalo_wideTrk_atPV_qoverp.WriteTo(tree);
    seedCalo_wideTrk_atPV_pt.WriteTo(tree);
    seedCalo_wideTrk_atPV_eta.WriteTo(tree);
    seedCalo_wideTrk_nBLHits.WriteTo(tree);
    seedCalo_wideTrk_nPixHits.WriteTo(tree);
    seedCalo_wideTrk_nSCTHits.WriteTo(tree);
    seedCalo_wideTrk_nTRTHits.WriteTo(tree);
    seedCalo_wideTrk_nTRTHighTHits.WriteTo(tree);
    seedCalo_wideTrk_nPixHoles.WriteTo(tree);
    seedCalo_wideTrk_nSCTHoles.WriteTo(tree);
    seedCalo_wideTrk_nTRTHoles.WriteTo(tree);
    seedCalo_wideTrk_nPixelDeadSensors.WriteTo(tree);
    seedCalo_wideTrk_nSCTDeadSensors.WriteTo(tree);
    seedCalo_wideTrk_nBLSharedHits.WriteTo(tree);
    seedCalo_wideTrk_nPixSharedHits.WriteTo(tree);
    seedCalo_wideTrk_nSCTSharedHits.WriteTo(tree);
    seedCalo_wideTrk_nBLayerSplitHits.WriteTo(tree);
    seedCalo_wideTrk_nPixSplitHits.WriteTo(tree);
    seedCalo_wideTrk_nBLayerOutliers.WriteTo(tree);
    seedCalo_wideTrk_nPixelOutliers.WriteTo(tree);
    seedCalo_wideTrk_nSCTOutliers.WriteTo(tree);
    seedCalo_wideTrk_nTRTOutliers.WriteTo(tree);
    seedCalo_wideTrk_nTRTHighTOutliers.WriteTo(tree);
    seedCalo_wideTrk_nContribPixelLayers.WriteTo(tree);
    seedCalo_wideTrk_nGangedPixels.WriteTo(tree);
    seedCalo_wideTrk_nGangedFlaggedFakes.WriteTo(tree);
    seedCalo_wideTrk_nPixelSpoiltHits.WriteTo(tree);
    seedCalo_wideTrk_nSCTDoubleHoles.WriteTo(tree);
    seedCalo_wideTrk_nSCTSpoiltHits.WriteTo(tree);
    seedCalo_wideTrk_expectBLayerHit.WriteTo(tree);
    seedCalo_wideTrk_nHits.WriteTo(tree);
    otherTrk_n.WriteTo(tree);
    otherTrk_d0.WriteTo(tree);
    otherTrk_z0.WriteTo(tree);
    otherTrk_phi.WriteTo(tree);
    otherTrk_theta.WriteTo(tree);
    otherTrk_qoverp.WriteTo(tree);
    otherTrk_pt.WriteTo(tree);
    otherTrk_eta.WriteTo(tree);
    otherTrk_atPV_d0.WriteTo(tree);
    otherTrk_atPV_z0.WriteTo(tree);
    otherTrk_atPV_phi.WriteTo(tree);
    otherTrk_atPV_theta.WriteTo(tree);
    otherTrk_atPV_qoverp.WriteTo(tree);
    otherTrk_atPV_pt.WriteTo(tree);
    otherTrk_atPV_eta.WriteTo(tree);
    otherTrk_nBLHits.WriteTo(tree);
    otherTrk_nPixHits.WriteTo(tree);
    otherTrk_nSCTHits.WriteTo(tree);
    otherTrk_nTRTHits.WriteTo(tree);
    otherTrk_nTRTHighTHits.WriteTo(tree);
    otherTrk_nPixHoles.WriteTo(tree);
    otherTrk_nSCTHoles.WriteTo(tree);
    otherTrk_nTRTHoles.WriteTo(tree);
    otherTrk_nPixelDeadSensors.WriteTo(tree);
    otherTrk_nSCTDeadSensors.WriteTo(tree);
    otherTrk_nBLSharedHits.WriteTo(tree);
    otherTrk_nPixSharedHits.WriteTo(tree);
    otherTrk_nSCTSharedHits.WriteTo(tree);
    otherTrk_nBLayerSplitHits.WriteTo(tree);
    otherTrk_nPixSplitHits.WriteTo(tree);
    otherTrk_nBLayerOutliers.WriteTo(tree);
    otherTrk_nPixelOutliers.WriteTo(tree);
    otherTrk_nSCTOutliers.WriteTo(tree);
    otherTrk_nTRTOutliers.WriteTo(tree);
    otherTrk_nTRTHighTOutliers.WriteTo(tree);
    otherTrk_nContribPixelLayers.WriteTo(tree);
    otherTrk_nGangedPixels.WriteTo(tree);
    otherTrk_nGangedFlaggedFakes.WriteTo(tree);
    otherTrk_nPixelSpoiltHits.WriteTo(tree);
    otherTrk_nSCTDoubleHoles.WriteTo(tree);
    otherTrk_nSCTSpoiltHits.WriteTo(tree);
    otherTrk_expectBLayerHit.WriteTo(tree);
    otherTrk_nHits.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TrigEFTauD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(EF_tau125_medium1.IsAvailable()) EF_tau125_medium1.SetActive(active);
     if(EF_tau125_medium1_L2StarA.IsAvailable()) EF_tau125_medium1_L2StarA.SetActive(active);
     if(EF_tau125_medium1_L2StarB.IsAvailable()) EF_tau125_medium1_L2StarB.SetActive(active);
     if(EF_tau125_medium1_L2StarC.IsAvailable()) EF_tau125_medium1_L2StarC.SetActive(active);
     if(EF_tau125_medium1_llh.IsAvailable()) EF_tau125_medium1_llh.SetActive(active);
     if(EF_tau20T_medium.IsAvailable()) EF_tau20T_medium.SetActive(active);
     if(EF_tau20T_medium1.IsAvailable()) EF_tau20T_medium1.SetActive(active);
     if(EF_tau20T_medium1_e15vh_medium1.IsAvailable()) EF_tau20T_medium1_e15vh_medium1.SetActive(active);
     if(EF_tau20T_medium1_mu15i.IsAvailable()) EF_tau20T_medium1_mu15i.SetActive(active);
     if(EF_tau20T_medium_mu15.IsAvailable()) EF_tau20T_medium_mu15.SetActive(active);
     if(EF_tau20Ti_medium.IsAvailable()) EF_tau20Ti_medium.SetActive(active);
     if(EF_tau20Ti_medium1.IsAvailable()) EF_tau20Ti_medium1.SetActive(active);
     if(EF_tau20Ti_medium1_e18vh_medium1.IsAvailable()) EF_tau20Ti_medium1_e18vh_medium1.SetActive(active);
     if(EF_tau20Ti_medium1_llh_e18vh_medium1.IsAvailable()) EF_tau20Ti_medium1_llh_e18vh_medium1.SetActive(active);
     if(EF_tau20Ti_medium_e18vh_medium1.IsAvailable()) EF_tau20Ti_medium_e18vh_medium1.SetActive(active);
     if(EF_tau20Ti_tight1.IsAvailable()) EF_tau20Ti_tight1.SetActive(active);
     if(EF_tau20Ti_tight1_llh.IsAvailable()) EF_tau20Ti_tight1_llh.SetActive(active);
     if(EF_tau20_medium.IsAvailable()) EF_tau20_medium.SetActive(active);
     if(EF_tau20_medium1.IsAvailable()) EF_tau20_medium1.SetActive(active);
     if(EF_tau20_medium1_llh.IsAvailable()) EF_tau20_medium1_llh.SetActive(active);
     if(EF_tau20_medium1_llh_mu15.IsAvailable()) EF_tau20_medium1_llh_mu15.SetActive(active);
     if(EF_tau20_medium1_mu15.IsAvailable()) EF_tau20_medium1_mu15.SetActive(active);
     if(EF_tau20_medium1_mu15i.IsAvailable()) EF_tau20_medium1_mu15i.SetActive(active);
     if(EF_tau20_medium1_mu18.IsAvailable()) EF_tau20_medium1_mu18.SetActive(active);
     if(EF_tau20_medium_llh.IsAvailable()) EF_tau20_medium_llh.SetActive(active);
     if(EF_tau20_medium_mu15.IsAvailable()) EF_tau20_medium_mu15.SetActive(active);
     if(EF_tau29T_medium.IsAvailable()) EF_tau29T_medium.SetActive(active);
     if(EF_tau29T_medium1.IsAvailable()) EF_tau29T_medium1.SetActive(active);
     if(EF_tau29T_medium1_tau20T_medium1.IsAvailable()) EF_tau29T_medium1_tau20T_medium1.SetActive(active);
     if(EF_tau29T_medium1_xe40_tight.IsAvailable()) EF_tau29T_medium1_xe40_tight.SetActive(active);
     if(EF_tau29T_medium1_xe45_tight.IsAvailable()) EF_tau29T_medium1_xe45_tight.SetActive(active);
     if(EF_tau29T_medium_xe40_tight.IsAvailable()) EF_tau29T_medium_xe40_tight.SetActive(active);
     if(EF_tau29T_medium_xe45_tight.IsAvailable()) EF_tau29T_medium_xe45_tight.SetActive(active);
     if(EF_tau29T_tight1.IsAvailable()) EF_tau29T_tight1.SetActive(active);
     if(EF_tau29T_tight1_llh.IsAvailable()) EF_tau29T_tight1_llh.SetActive(active);
     if(EF_tau29Ti_medium1.IsAvailable()) EF_tau29Ti_medium1.SetActive(active);
     if(EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh.IsAvailable()) EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh.SetActive(active);
     if(EF_tau29Ti_medium1_llh_xe40_tight.IsAvailable()) EF_tau29Ti_medium1_llh_xe40_tight.SetActive(active);
     if(EF_tau29Ti_medium1_llh_xe45_tight.IsAvailable()) EF_tau29Ti_medium1_llh_xe45_tight.SetActive(active);
     if(EF_tau29Ti_medium1_tau20Ti_medium1.IsAvailable()) EF_tau29Ti_medium1_tau20Ti_medium1.SetActive(active);
     if(EF_tau29Ti_medium1_xe40_tight.IsAvailable()) EF_tau29Ti_medium1_xe40_tight.SetActive(active);
     if(EF_tau29Ti_medium1_xe45_tight.IsAvailable()) EF_tau29Ti_medium1_xe45_tight.SetActive(active);
     if(EF_tau29Ti_medium1_xe55_tclcw.IsAvailable()) EF_tau29Ti_medium1_xe55_tclcw.SetActive(active);
     if(EF_tau29Ti_medium1_xe55_tclcw_tight.IsAvailable()) EF_tau29Ti_medium1_xe55_tclcw_tight.SetActive(active);
     if(EF_tau29Ti_medium_xe40_tight.IsAvailable()) EF_tau29Ti_medium_xe40_tight.SetActive(active);
     if(EF_tau29Ti_medium_xe45_tight.IsAvailable()) EF_tau29Ti_medium_xe45_tight.SetActive(active);
     if(EF_tau29Ti_tight1.IsAvailable()) EF_tau29Ti_tight1.SetActive(active);
     if(EF_tau29Ti_tight1_llh.IsAvailable()) EF_tau29Ti_tight1_llh.SetActive(active);
     if(EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh.IsAvailable()) EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh.SetActive(active);
     if(EF_tau29Ti_tight1_tau20Ti_tight1.IsAvailable()) EF_tau29Ti_tight1_tau20Ti_tight1.SetActive(active);
     if(EF_tau29_IDTrkNoCut.IsAvailable()) EF_tau29_IDTrkNoCut.SetActive(active);
     if(EF_tau29_medium.IsAvailable()) EF_tau29_medium.SetActive(active);
     if(EF_tau29_medium1.IsAvailable()) EF_tau29_medium1.SetActive(active);
     if(EF_tau29_medium1_llh.IsAvailable()) EF_tau29_medium1_llh.SetActive(active);
     if(EF_tau29_medium_2stTest.IsAvailable()) EF_tau29_medium_2stTest.SetActive(active);
     if(EF_tau29_medium_L2StarA.IsAvailable()) EF_tau29_medium_L2StarA.SetActive(active);
     if(EF_tau29_medium_L2StarB.IsAvailable()) EF_tau29_medium_L2StarB.SetActive(active);
     if(EF_tau29_medium_L2StarC.IsAvailable()) EF_tau29_medium_L2StarC.SetActive(active);
     if(EF_tau29_medium_llh.IsAvailable()) EF_tau29_medium_llh.SetActive(active);
     if(EF_tau29i_medium.IsAvailable()) EF_tau29i_medium.SetActive(active);
     if(EF_tau29i_medium1.IsAvailable()) EF_tau29i_medium1.SetActive(active);
     if(EF_tau38T_medium.IsAvailable()) EF_tau38T_medium.SetActive(active);
     if(EF_tau38T_medium1.IsAvailable()) EF_tau38T_medium1.SetActive(active);
     if(EF_tau38T_medium1_e18vh_medium1.IsAvailable()) EF_tau38T_medium1_e18vh_medium1.SetActive(active);
     if(EF_tau38T_medium1_llh_e18vh_medium1.IsAvailable()) EF_tau38T_medium1_llh_e18vh_medium1.SetActive(active);
     if(EF_tau38T_medium1_xe40_tight.IsAvailable()) EF_tau38T_medium1_xe40_tight.SetActive(active);
     if(EF_tau38T_medium1_xe45_tight.IsAvailable()) EF_tau38T_medium1_xe45_tight.SetActive(active);
     if(EF_tau38T_medium1_xe55_tclcw_tight.IsAvailable()) EF_tau38T_medium1_xe55_tclcw_tight.SetActive(active);
     if(EF_tau38T_medium_e18vh_medium1.IsAvailable()) EF_tau38T_medium_e18vh_medium1.SetActive(active);
     if(n.IsAvailable()) n.SetActive(active);
     if(Et.IsAvailable()) Et.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(m.IsAvailable()) m.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(px.IsAvailable()) px.SetActive(active);
     if(py.IsAvailable()) py.SetActive(active);
     if(pz.IsAvailable()) pz.SetActive(active);
     if(author.IsAvailable()) author.SetActive(active);
     if(RoIWord.IsAvailable()) RoIWord.SetActive(active);
     if(nProng.IsAvailable()) nProng.SetActive(active);
     if(numTrack.IsAvailable()) numTrack.SetActive(active);
     if(seedCalo_numTrack.IsAvailable()) seedCalo_numTrack.SetActive(active);
     if(seedCalo_nWideTrk.IsAvailable()) seedCalo_nWideTrk.SetActive(active);
     if(nOtherTrk.IsAvailable()) nOtherTrk.SetActive(active);
     if(seedCalo_EMRadius.IsAvailable()) seedCalo_EMRadius.SetActive(active);
     if(seedCalo_hadRadius.IsAvailable()) seedCalo_hadRadius.SetActive(active);
     if(seedCalo_etEMAtEMScale.IsAvailable()) seedCalo_etEMAtEMScale.SetActive(active);
     if(seedCalo_etHadAtEMScale.IsAvailable()) seedCalo_etHadAtEMScale.SetActive(active);
     if(seedCalo_isolFrac.IsAvailable()) seedCalo_isolFrac.SetActive(active);
     if(seedCalo_centFrac.IsAvailable()) seedCalo_centFrac.SetActive(active);
     if(seedCalo_etEMCalib.IsAvailable()) seedCalo_etEMCalib.SetActive(active);
     if(seedCalo_etHadCalib.IsAvailable()) seedCalo_etHadCalib.SetActive(active);
     if(seedCalo_eta.IsAvailable()) seedCalo_eta.SetActive(active);
     if(seedCalo_phi.IsAvailable()) seedCalo_phi.SetActive(active);
     if(seedCalo_stripWidth2.IsAvailable()) seedCalo_stripWidth2.SetActive(active);
     if(seedCalo_nStrip.IsAvailable()) seedCalo_nStrip.SetActive(active);
     if(seedCalo_trkAvgDist.IsAvailable()) seedCalo_trkAvgDist.SetActive(active);
     if(seedCalo_trkRmsDist.IsAvailable()) seedCalo_trkRmsDist.SetActive(active);
     if(seedCalo_dRmax.IsAvailable()) seedCalo_dRmax.SetActive(active);
     if(seedCalo_lead2ClusterEOverAllClusterE.IsAvailable()) seedCalo_lead2ClusterEOverAllClusterE.SetActive(active);
     if(seedCalo_lead3ClusterEOverAllClusterE.IsAvailable()) seedCalo_lead3ClusterEOverAllClusterE.SetActive(active);
     if(etOverPtLeadTrk.IsAvailable()) etOverPtLeadTrk.SetActive(active);
     if(ipZ0SinThetaSigLeadTrk.IsAvailable()) ipZ0SinThetaSigLeadTrk.SetActive(active);
     if(LC_TES_precalib.IsAvailable()) LC_TES_precalib.SetActive(active);
     if(leadTrkPt.IsAvailable()) leadTrkPt.SetActive(active);
     if(ipSigLeadTrk.IsAvailable()) ipSigLeadTrk.SetActive(active);
     if(trFlightPathSig.IsAvailable()) trFlightPathSig.SetActive(active);
     if(numTopoClusters.IsAvailable()) numTopoClusters.SetActive(active);
     if(numEffTopoClusters.IsAvailable()) numEffTopoClusters.SetActive(active);
     if(topoInvMass.IsAvailable()) topoInvMass.SetActive(active);
     if(effTopoInvMass.IsAvailable()) effTopoInvMass.SetActive(active);
     if(topoMeanDeltaR.IsAvailable()) topoMeanDeltaR.SetActive(active);
     if(effTopoMeanDeltaR.IsAvailable()) effTopoMeanDeltaR.SetActive(active);
     if(numCells.IsAvailable()) numCells.SetActive(active);
     if(massTrkSys.IsAvailable()) massTrkSys.SetActive(active);
     if(BDTJetScore.IsAvailable()) BDTJetScore.SetActive(active);
     if(likelihood.IsAvailable()) likelihood.SetActive(active);
     if(track_n.IsAvailable()) track_n.SetActive(active);
     if(track_d0.IsAvailable()) track_d0.SetActive(active);
     if(track_z0.IsAvailable()) track_z0.SetActive(active);
     if(track_phi.IsAvailable()) track_phi.SetActive(active);
     if(track_theta.IsAvailable()) track_theta.SetActive(active);
     if(track_qoverp.IsAvailable()) track_qoverp.SetActive(active);
     if(track_pt.IsAvailable()) track_pt.SetActive(active);
     if(track_eta.IsAvailable()) track_eta.SetActive(active);
     if(track_atPV_d0.IsAvailable()) track_atPV_d0.SetActive(active);
     if(track_atPV_z0.IsAvailable()) track_atPV_z0.SetActive(active);
     if(track_atPV_phi.IsAvailable()) track_atPV_phi.SetActive(active);
     if(track_atPV_theta.IsAvailable()) track_atPV_theta.SetActive(active);
     if(track_atPV_qoverp.IsAvailable()) track_atPV_qoverp.SetActive(active);
     if(track_atPV_pt.IsAvailable()) track_atPV_pt.SetActive(active);
     if(track_atPV_eta.IsAvailable()) track_atPV_eta.SetActive(active);
     if(track_nBLHits.IsAvailable()) track_nBLHits.SetActive(active);
     if(track_nPixHits.IsAvailable()) track_nPixHits.SetActive(active);
     if(track_nSCTHits.IsAvailable()) track_nSCTHits.SetActive(active);
     if(track_nTRTHits.IsAvailable()) track_nTRTHits.SetActive(active);
     if(track_nTRTHighTHits.IsAvailable()) track_nTRTHighTHits.SetActive(active);
     if(track_nPixHoles.IsAvailable()) track_nPixHoles.SetActive(active);
     if(track_nSCTHoles.IsAvailable()) track_nSCTHoles.SetActive(active);
     if(track_nTRTHoles.IsAvailable()) track_nTRTHoles.SetActive(active);
     if(track_nPixelDeadSensors.IsAvailable()) track_nPixelDeadSensors.SetActive(active);
     if(track_nSCTDeadSensors.IsAvailable()) track_nSCTDeadSensors.SetActive(active);
     if(track_nBLSharedHits.IsAvailable()) track_nBLSharedHits.SetActive(active);
     if(track_nPixSharedHits.IsAvailable()) track_nPixSharedHits.SetActive(active);
     if(track_nSCTSharedHits.IsAvailable()) track_nSCTSharedHits.SetActive(active);
     if(track_nBLayerSplitHits.IsAvailable()) track_nBLayerSplitHits.SetActive(active);
     if(track_nPixSplitHits.IsAvailable()) track_nPixSplitHits.SetActive(active);
     if(track_nBLayerOutliers.IsAvailable()) track_nBLayerOutliers.SetActive(active);
     if(track_nPixelOutliers.IsAvailable()) track_nPixelOutliers.SetActive(active);
     if(track_nSCTOutliers.IsAvailable()) track_nSCTOutliers.SetActive(active);
     if(track_nTRTOutliers.IsAvailable()) track_nTRTOutliers.SetActive(active);
     if(track_nTRTHighTOutliers.IsAvailable()) track_nTRTHighTOutliers.SetActive(active);
     if(track_nContribPixelLayers.IsAvailable()) track_nContribPixelLayers.SetActive(active);
     if(track_nGangedPixels.IsAvailable()) track_nGangedPixels.SetActive(active);
     if(track_nGangedFlaggedFakes.IsAvailable()) track_nGangedFlaggedFakes.SetActive(active);
     if(track_nPixelSpoiltHits.IsAvailable()) track_nPixelSpoiltHits.SetActive(active);
     if(track_nSCTDoubleHoles.IsAvailable()) track_nSCTDoubleHoles.SetActive(active);
     if(track_nSCTSpoiltHits.IsAvailable()) track_nSCTSpoiltHits.SetActive(active);
     if(track_expectBLayerHit.IsAvailable()) track_expectBLayerHit.SetActive(active);
     if(track_nHits.IsAvailable()) track_nHits.SetActive(active);
     if(track_TRTHighTHitsRatio.IsAvailable()) track_TRTHighTHitsRatio.SetActive(active);
     if(track_TRTHighTOutliersRatio.IsAvailable()) track_TRTHighTOutliersRatio.SetActive(active);
     if(seedCalo_track_n.IsAvailable()) seedCalo_track_n.SetActive(active);
     if(seedCalo_wideTrk_n.IsAvailable()) seedCalo_wideTrk_n.SetActive(active);
     if(seedCalo_wideTrk_d0.IsAvailable()) seedCalo_wideTrk_d0.SetActive(active);
     if(seedCalo_wideTrk_z0.IsAvailable()) seedCalo_wideTrk_z0.SetActive(active);
     if(seedCalo_wideTrk_phi.IsAvailable()) seedCalo_wideTrk_phi.SetActive(active);
     if(seedCalo_wideTrk_theta.IsAvailable()) seedCalo_wideTrk_theta.SetActive(active);
     if(seedCalo_wideTrk_qoverp.IsAvailable()) seedCalo_wideTrk_qoverp.SetActive(active);
     if(seedCalo_wideTrk_pt.IsAvailable()) seedCalo_wideTrk_pt.SetActive(active);
     if(seedCalo_wideTrk_eta.IsAvailable()) seedCalo_wideTrk_eta.SetActive(active);
     if(seedCalo_wideTrk_atPV_d0.IsAvailable()) seedCalo_wideTrk_atPV_d0.SetActive(active);
     if(seedCalo_wideTrk_atPV_z0.IsAvailable()) seedCalo_wideTrk_atPV_z0.SetActive(active);
     if(seedCalo_wideTrk_atPV_phi.IsAvailable()) seedCalo_wideTrk_atPV_phi.SetActive(active);
     if(seedCalo_wideTrk_atPV_theta.IsAvailable()) seedCalo_wideTrk_atPV_theta.SetActive(active);
     if(seedCalo_wideTrk_atPV_qoverp.IsAvailable()) seedCalo_wideTrk_atPV_qoverp.SetActive(active);
     if(seedCalo_wideTrk_atPV_pt.IsAvailable()) seedCalo_wideTrk_atPV_pt.SetActive(active);
     if(seedCalo_wideTrk_atPV_eta.IsAvailable()) seedCalo_wideTrk_atPV_eta.SetActive(active);
     if(seedCalo_wideTrk_nBLHits.IsAvailable()) seedCalo_wideTrk_nBLHits.SetActive(active);
     if(seedCalo_wideTrk_nPixHits.IsAvailable()) seedCalo_wideTrk_nPixHits.SetActive(active);
     if(seedCalo_wideTrk_nSCTHits.IsAvailable()) seedCalo_wideTrk_nSCTHits.SetActive(active);
     if(seedCalo_wideTrk_nTRTHits.IsAvailable()) seedCalo_wideTrk_nTRTHits.SetActive(active);
     if(seedCalo_wideTrk_nTRTHighTHits.IsAvailable()) seedCalo_wideTrk_nTRTHighTHits.SetActive(active);
     if(seedCalo_wideTrk_nPixHoles.IsAvailable()) seedCalo_wideTrk_nPixHoles.SetActive(active);
     if(seedCalo_wideTrk_nSCTHoles.IsAvailable()) seedCalo_wideTrk_nSCTHoles.SetActive(active);
     if(seedCalo_wideTrk_nTRTHoles.IsAvailable()) seedCalo_wideTrk_nTRTHoles.SetActive(active);
     if(seedCalo_wideTrk_nPixelDeadSensors.IsAvailable()) seedCalo_wideTrk_nPixelDeadSensors.SetActive(active);
     if(seedCalo_wideTrk_nSCTDeadSensors.IsAvailable()) seedCalo_wideTrk_nSCTDeadSensors.SetActive(active);
     if(seedCalo_wideTrk_nBLSharedHits.IsAvailable()) seedCalo_wideTrk_nBLSharedHits.SetActive(active);
     if(seedCalo_wideTrk_nPixSharedHits.IsAvailable()) seedCalo_wideTrk_nPixSharedHits.SetActive(active);
     if(seedCalo_wideTrk_nSCTSharedHits.IsAvailable()) seedCalo_wideTrk_nSCTSharedHits.SetActive(active);
     if(seedCalo_wideTrk_nBLayerSplitHits.IsAvailable()) seedCalo_wideTrk_nBLayerSplitHits.SetActive(active);
     if(seedCalo_wideTrk_nPixSplitHits.IsAvailable()) seedCalo_wideTrk_nPixSplitHits.SetActive(active);
     if(seedCalo_wideTrk_nBLayerOutliers.IsAvailable()) seedCalo_wideTrk_nBLayerOutliers.SetActive(active);
     if(seedCalo_wideTrk_nPixelOutliers.IsAvailable()) seedCalo_wideTrk_nPixelOutliers.SetActive(active);
     if(seedCalo_wideTrk_nSCTOutliers.IsAvailable()) seedCalo_wideTrk_nSCTOutliers.SetActive(active);
     if(seedCalo_wideTrk_nTRTOutliers.IsAvailable()) seedCalo_wideTrk_nTRTOutliers.SetActive(active);
     if(seedCalo_wideTrk_nTRTHighTOutliers.IsAvailable()) seedCalo_wideTrk_nTRTHighTOutliers.SetActive(active);
     if(seedCalo_wideTrk_nContribPixelLayers.IsAvailable()) seedCalo_wideTrk_nContribPixelLayers.SetActive(active);
     if(seedCalo_wideTrk_nGangedPixels.IsAvailable()) seedCalo_wideTrk_nGangedPixels.SetActive(active);
     if(seedCalo_wideTrk_nGangedFlaggedFakes.IsAvailable()) seedCalo_wideTrk_nGangedFlaggedFakes.SetActive(active);
     if(seedCalo_wideTrk_nPixelSpoiltHits.IsAvailable()) seedCalo_wideTrk_nPixelSpoiltHits.SetActive(active);
     if(seedCalo_wideTrk_nSCTDoubleHoles.IsAvailable()) seedCalo_wideTrk_nSCTDoubleHoles.SetActive(active);
     if(seedCalo_wideTrk_nSCTSpoiltHits.IsAvailable()) seedCalo_wideTrk_nSCTSpoiltHits.SetActive(active);
     if(seedCalo_wideTrk_expectBLayerHit.IsAvailable()) seedCalo_wideTrk_expectBLayerHit.SetActive(active);
     if(seedCalo_wideTrk_nHits.IsAvailable()) seedCalo_wideTrk_nHits.SetActive(active);
     if(otherTrk_n.IsAvailable()) otherTrk_n.SetActive(active);
     if(otherTrk_d0.IsAvailable()) otherTrk_d0.SetActive(active);
     if(otherTrk_z0.IsAvailable()) otherTrk_z0.SetActive(active);
     if(otherTrk_phi.IsAvailable()) otherTrk_phi.SetActive(active);
     if(otherTrk_theta.IsAvailable()) otherTrk_theta.SetActive(active);
     if(otherTrk_qoverp.IsAvailable()) otherTrk_qoverp.SetActive(active);
     if(otherTrk_pt.IsAvailable()) otherTrk_pt.SetActive(active);
     if(otherTrk_eta.IsAvailable()) otherTrk_eta.SetActive(active);
     if(otherTrk_atPV_d0.IsAvailable()) otherTrk_atPV_d0.SetActive(active);
     if(otherTrk_atPV_z0.IsAvailable()) otherTrk_atPV_z0.SetActive(active);
     if(otherTrk_atPV_phi.IsAvailable()) otherTrk_atPV_phi.SetActive(active);
     if(otherTrk_atPV_theta.IsAvailable()) otherTrk_atPV_theta.SetActive(active);
     if(otherTrk_atPV_qoverp.IsAvailable()) otherTrk_atPV_qoverp.SetActive(active);
     if(otherTrk_atPV_pt.IsAvailable()) otherTrk_atPV_pt.SetActive(active);
     if(otherTrk_atPV_eta.IsAvailable()) otherTrk_atPV_eta.SetActive(active);
     if(otherTrk_nBLHits.IsAvailable()) otherTrk_nBLHits.SetActive(active);
     if(otherTrk_nPixHits.IsAvailable()) otherTrk_nPixHits.SetActive(active);
     if(otherTrk_nSCTHits.IsAvailable()) otherTrk_nSCTHits.SetActive(active);
     if(otherTrk_nTRTHits.IsAvailable()) otherTrk_nTRTHits.SetActive(active);
     if(otherTrk_nTRTHighTHits.IsAvailable()) otherTrk_nTRTHighTHits.SetActive(active);
     if(otherTrk_nPixHoles.IsAvailable()) otherTrk_nPixHoles.SetActive(active);
     if(otherTrk_nSCTHoles.IsAvailable()) otherTrk_nSCTHoles.SetActive(active);
     if(otherTrk_nTRTHoles.IsAvailable()) otherTrk_nTRTHoles.SetActive(active);
     if(otherTrk_nPixelDeadSensors.IsAvailable()) otherTrk_nPixelDeadSensors.SetActive(active);
     if(otherTrk_nSCTDeadSensors.IsAvailable()) otherTrk_nSCTDeadSensors.SetActive(active);
     if(otherTrk_nBLSharedHits.IsAvailable()) otherTrk_nBLSharedHits.SetActive(active);
     if(otherTrk_nPixSharedHits.IsAvailable()) otherTrk_nPixSharedHits.SetActive(active);
     if(otherTrk_nSCTSharedHits.IsAvailable()) otherTrk_nSCTSharedHits.SetActive(active);
     if(otherTrk_nBLayerSplitHits.IsAvailable()) otherTrk_nBLayerSplitHits.SetActive(active);
     if(otherTrk_nPixSplitHits.IsAvailable()) otherTrk_nPixSplitHits.SetActive(active);
     if(otherTrk_nBLayerOutliers.IsAvailable()) otherTrk_nBLayerOutliers.SetActive(active);
     if(otherTrk_nPixelOutliers.IsAvailable()) otherTrk_nPixelOutliers.SetActive(active);
     if(otherTrk_nSCTOutliers.IsAvailable()) otherTrk_nSCTOutliers.SetActive(active);
     if(otherTrk_nTRTOutliers.IsAvailable()) otherTrk_nTRTOutliers.SetActive(active);
     if(otherTrk_nTRTHighTOutliers.IsAvailable()) otherTrk_nTRTHighTOutliers.SetActive(active);
     if(otherTrk_nContribPixelLayers.IsAvailable()) otherTrk_nContribPixelLayers.SetActive(active);
     if(otherTrk_nGangedPixels.IsAvailable()) otherTrk_nGangedPixels.SetActive(active);
     if(otherTrk_nGangedFlaggedFakes.IsAvailable()) otherTrk_nGangedFlaggedFakes.SetActive(active);
     if(otherTrk_nPixelSpoiltHits.IsAvailable()) otherTrk_nPixelSpoiltHits.SetActive(active);
     if(otherTrk_nSCTDoubleHoles.IsAvailable()) otherTrk_nSCTDoubleHoles.SetActive(active);
     if(otherTrk_nSCTSpoiltHits.IsAvailable()) otherTrk_nSCTSpoiltHits.SetActive(active);
     if(otherTrk_expectBLayerHit.IsAvailable()) otherTrk_expectBLayerHit.SetActive(active);
     if(otherTrk_nHits.IsAvailable()) otherTrk_nHits.SetActive(active);
    }
    else
    {
      EF_tau125_medium1.SetActive(active);
      EF_tau125_medium1_L2StarA.SetActive(active);
      EF_tau125_medium1_L2StarB.SetActive(active);
      EF_tau125_medium1_L2StarC.SetActive(active);
      EF_tau125_medium1_llh.SetActive(active);
      EF_tau20T_medium.SetActive(active);
      EF_tau20T_medium1.SetActive(active);
      EF_tau20T_medium1_e15vh_medium1.SetActive(active);
      EF_tau20T_medium1_mu15i.SetActive(active);
      EF_tau20T_medium_mu15.SetActive(active);
      EF_tau20Ti_medium.SetActive(active);
      EF_tau20Ti_medium1.SetActive(active);
      EF_tau20Ti_medium1_e18vh_medium1.SetActive(active);
      EF_tau20Ti_medium1_llh_e18vh_medium1.SetActive(active);
      EF_tau20Ti_medium_e18vh_medium1.SetActive(active);
      EF_tau20Ti_tight1.SetActive(active);
      EF_tau20Ti_tight1_llh.SetActive(active);
      EF_tau20_medium.SetActive(active);
      EF_tau20_medium1.SetActive(active);
      EF_tau20_medium1_llh.SetActive(active);
      EF_tau20_medium1_llh_mu15.SetActive(active);
      EF_tau20_medium1_mu15.SetActive(active);
      EF_tau20_medium1_mu15i.SetActive(active);
      EF_tau20_medium1_mu18.SetActive(active);
      EF_tau20_medium_llh.SetActive(active);
      EF_tau20_medium_mu15.SetActive(active);
      EF_tau29T_medium.SetActive(active);
      EF_tau29T_medium1.SetActive(active);
      EF_tau29T_medium1_tau20T_medium1.SetActive(active);
      EF_tau29T_medium1_xe40_tight.SetActive(active);
      EF_tau29T_medium1_xe45_tight.SetActive(active);
      EF_tau29T_medium_xe40_tight.SetActive(active);
      EF_tau29T_medium_xe45_tight.SetActive(active);
      EF_tau29T_tight1.SetActive(active);
      EF_tau29T_tight1_llh.SetActive(active);
      EF_tau29Ti_medium1.SetActive(active);
      EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh.SetActive(active);
      EF_tau29Ti_medium1_llh_xe40_tight.SetActive(active);
      EF_tau29Ti_medium1_llh_xe45_tight.SetActive(active);
      EF_tau29Ti_medium1_tau20Ti_medium1.SetActive(active);
      EF_tau29Ti_medium1_xe40_tight.SetActive(active);
      EF_tau29Ti_medium1_xe45_tight.SetActive(active);
      EF_tau29Ti_medium1_xe55_tclcw.SetActive(active);
      EF_tau29Ti_medium1_xe55_tclcw_tight.SetActive(active);
      EF_tau29Ti_medium_xe40_tight.SetActive(active);
      EF_tau29Ti_medium_xe45_tight.SetActive(active);
      EF_tau29Ti_tight1.SetActive(active);
      EF_tau29Ti_tight1_llh.SetActive(active);
      EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh.SetActive(active);
      EF_tau29Ti_tight1_tau20Ti_tight1.SetActive(active);
      EF_tau29_IDTrkNoCut.SetActive(active);
      EF_tau29_medium.SetActive(active);
      EF_tau29_medium1.SetActive(active);
      EF_tau29_medium1_llh.SetActive(active);
      EF_tau29_medium_2stTest.SetActive(active);
      EF_tau29_medium_L2StarA.SetActive(active);
      EF_tau29_medium_L2StarB.SetActive(active);
      EF_tau29_medium_L2StarC.SetActive(active);
      EF_tau29_medium_llh.SetActive(active);
      EF_tau29i_medium.SetActive(active);
      EF_tau29i_medium1.SetActive(active);
      EF_tau38T_medium.SetActive(active);
      EF_tau38T_medium1.SetActive(active);
      EF_tau38T_medium1_e18vh_medium1.SetActive(active);
      EF_tau38T_medium1_llh_e18vh_medium1.SetActive(active);
      EF_tau38T_medium1_xe40_tight.SetActive(active);
      EF_tau38T_medium1_xe45_tight.SetActive(active);
      EF_tau38T_medium1_xe55_tclcw_tight.SetActive(active);
      EF_tau38T_medium_e18vh_medium1.SetActive(active);
      n.SetActive(active);
      Et.SetActive(active);
      pt.SetActive(active);
      m.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      px.SetActive(active);
      py.SetActive(active);
      pz.SetActive(active);
      author.SetActive(active);
      RoIWord.SetActive(active);
      nProng.SetActive(active);
      numTrack.SetActive(active);
      seedCalo_numTrack.SetActive(active);
      seedCalo_nWideTrk.SetActive(active);
      nOtherTrk.SetActive(active);
      seedCalo_EMRadius.SetActive(active);
      seedCalo_hadRadius.SetActive(active);
      seedCalo_etEMAtEMScale.SetActive(active);
      seedCalo_etHadAtEMScale.SetActive(active);
      seedCalo_isolFrac.SetActive(active);
      seedCalo_centFrac.SetActive(active);
      seedCalo_etEMCalib.SetActive(active);
      seedCalo_etHadCalib.SetActive(active);
      seedCalo_eta.SetActive(active);
      seedCalo_phi.SetActive(active);
      seedCalo_stripWidth2.SetActive(active);
      seedCalo_nStrip.SetActive(active);
      seedCalo_trkAvgDist.SetActive(active);
      seedCalo_trkRmsDist.SetActive(active);
      seedCalo_dRmax.SetActive(active);
      seedCalo_lead2ClusterEOverAllClusterE.SetActive(active);
      seedCalo_lead3ClusterEOverAllClusterE.SetActive(active);
      etOverPtLeadTrk.SetActive(active);
      ipZ0SinThetaSigLeadTrk.SetActive(active);
      LC_TES_precalib.SetActive(active);
      leadTrkPt.SetActive(active);
      ipSigLeadTrk.SetActive(active);
      trFlightPathSig.SetActive(active);
      numTopoClusters.SetActive(active);
      numEffTopoClusters.SetActive(active);
      topoInvMass.SetActive(active);
      effTopoInvMass.SetActive(active);
      topoMeanDeltaR.SetActive(active);
      effTopoMeanDeltaR.SetActive(active);
      numCells.SetActive(active);
      massTrkSys.SetActive(active);
      BDTJetScore.SetActive(active);
      likelihood.SetActive(active);
      track_n.SetActive(active);
      track_d0.SetActive(active);
      track_z0.SetActive(active);
      track_phi.SetActive(active);
      track_theta.SetActive(active);
      track_qoverp.SetActive(active);
      track_pt.SetActive(active);
      track_eta.SetActive(active);
      track_atPV_d0.SetActive(active);
      track_atPV_z0.SetActive(active);
      track_atPV_phi.SetActive(active);
      track_atPV_theta.SetActive(active);
      track_atPV_qoverp.SetActive(active);
      track_atPV_pt.SetActive(active);
      track_atPV_eta.SetActive(active);
      track_nBLHits.SetActive(active);
      track_nPixHits.SetActive(active);
      track_nSCTHits.SetActive(active);
      track_nTRTHits.SetActive(active);
      track_nTRTHighTHits.SetActive(active);
      track_nPixHoles.SetActive(active);
      track_nSCTHoles.SetActive(active);
      track_nTRTHoles.SetActive(active);
      track_nPixelDeadSensors.SetActive(active);
      track_nSCTDeadSensors.SetActive(active);
      track_nBLSharedHits.SetActive(active);
      track_nPixSharedHits.SetActive(active);
      track_nSCTSharedHits.SetActive(active);
      track_nBLayerSplitHits.SetActive(active);
      track_nPixSplitHits.SetActive(active);
      track_nBLayerOutliers.SetActive(active);
      track_nPixelOutliers.SetActive(active);
      track_nSCTOutliers.SetActive(active);
      track_nTRTOutliers.SetActive(active);
      track_nTRTHighTOutliers.SetActive(active);
      track_nContribPixelLayers.SetActive(active);
      track_nGangedPixels.SetActive(active);
      track_nGangedFlaggedFakes.SetActive(active);
      track_nPixelSpoiltHits.SetActive(active);
      track_nSCTDoubleHoles.SetActive(active);
      track_nSCTSpoiltHits.SetActive(active);
      track_expectBLayerHit.SetActive(active);
      track_nHits.SetActive(active);
      track_TRTHighTHitsRatio.SetActive(active);
      track_TRTHighTOutliersRatio.SetActive(active);
      seedCalo_track_n.SetActive(active);
      seedCalo_wideTrk_n.SetActive(active);
      seedCalo_wideTrk_d0.SetActive(active);
      seedCalo_wideTrk_z0.SetActive(active);
      seedCalo_wideTrk_phi.SetActive(active);
      seedCalo_wideTrk_theta.SetActive(active);
      seedCalo_wideTrk_qoverp.SetActive(active);
      seedCalo_wideTrk_pt.SetActive(active);
      seedCalo_wideTrk_eta.SetActive(active);
      seedCalo_wideTrk_atPV_d0.SetActive(active);
      seedCalo_wideTrk_atPV_z0.SetActive(active);
      seedCalo_wideTrk_atPV_phi.SetActive(active);
      seedCalo_wideTrk_atPV_theta.SetActive(active);
      seedCalo_wideTrk_atPV_qoverp.SetActive(active);
      seedCalo_wideTrk_atPV_pt.SetActive(active);
      seedCalo_wideTrk_atPV_eta.SetActive(active);
      seedCalo_wideTrk_nBLHits.SetActive(active);
      seedCalo_wideTrk_nPixHits.SetActive(active);
      seedCalo_wideTrk_nSCTHits.SetActive(active);
      seedCalo_wideTrk_nTRTHits.SetActive(active);
      seedCalo_wideTrk_nTRTHighTHits.SetActive(active);
      seedCalo_wideTrk_nPixHoles.SetActive(active);
      seedCalo_wideTrk_nSCTHoles.SetActive(active);
      seedCalo_wideTrk_nTRTHoles.SetActive(active);
      seedCalo_wideTrk_nPixelDeadSensors.SetActive(active);
      seedCalo_wideTrk_nSCTDeadSensors.SetActive(active);
      seedCalo_wideTrk_nBLSharedHits.SetActive(active);
      seedCalo_wideTrk_nPixSharedHits.SetActive(active);
      seedCalo_wideTrk_nSCTSharedHits.SetActive(active);
      seedCalo_wideTrk_nBLayerSplitHits.SetActive(active);
      seedCalo_wideTrk_nPixSplitHits.SetActive(active);
      seedCalo_wideTrk_nBLayerOutliers.SetActive(active);
      seedCalo_wideTrk_nPixelOutliers.SetActive(active);
      seedCalo_wideTrk_nSCTOutliers.SetActive(active);
      seedCalo_wideTrk_nTRTOutliers.SetActive(active);
      seedCalo_wideTrk_nTRTHighTOutliers.SetActive(active);
      seedCalo_wideTrk_nContribPixelLayers.SetActive(active);
      seedCalo_wideTrk_nGangedPixels.SetActive(active);
      seedCalo_wideTrk_nGangedFlaggedFakes.SetActive(active);
      seedCalo_wideTrk_nPixelSpoiltHits.SetActive(active);
      seedCalo_wideTrk_nSCTDoubleHoles.SetActive(active);
      seedCalo_wideTrk_nSCTSpoiltHits.SetActive(active);
      seedCalo_wideTrk_expectBLayerHit.SetActive(active);
      seedCalo_wideTrk_nHits.SetActive(active);
      otherTrk_n.SetActive(active);
      otherTrk_d0.SetActive(active);
      otherTrk_z0.SetActive(active);
      otherTrk_phi.SetActive(active);
      otherTrk_theta.SetActive(active);
      otherTrk_qoverp.SetActive(active);
      otherTrk_pt.SetActive(active);
      otherTrk_eta.SetActive(active);
      otherTrk_atPV_d0.SetActive(active);
      otherTrk_atPV_z0.SetActive(active);
      otherTrk_atPV_phi.SetActive(active);
      otherTrk_atPV_theta.SetActive(active);
      otherTrk_atPV_qoverp.SetActive(active);
      otherTrk_atPV_pt.SetActive(active);
      otherTrk_atPV_eta.SetActive(active);
      otherTrk_nBLHits.SetActive(active);
      otherTrk_nPixHits.SetActive(active);
      otherTrk_nSCTHits.SetActive(active);
      otherTrk_nTRTHits.SetActive(active);
      otherTrk_nTRTHighTHits.SetActive(active);
      otherTrk_nPixHoles.SetActive(active);
      otherTrk_nSCTHoles.SetActive(active);
      otherTrk_nTRTHoles.SetActive(active);
      otherTrk_nPixelDeadSensors.SetActive(active);
      otherTrk_nSCTDeadSensors.SetActive(active);
      otherTrk_nBLSharedHits.SetActive(active);
      otherTrk_nPixSharedHits.SetActive(active);
      otherTrk_nSCTSharedHits.SetActive(active);
      otherTrk_nBLayerSplitHits.SetActive(active);
      otherTrk_nPixSplitHits.SetActive(active);
      otherTrk_nBLayerOutliers.SetActive(active);
      otherTrk_nPixelOutliers.SetActive(active);
      otherTrk_nSCTOutliers.SetActive(active);
      otherTrk_nTRTOutliers.SetActive(active);
      otherTrk_nTRTHighTOutliers.SetActive(active);
      otherTrk_nContribPixelLayers.SetActive(active);
      otherTrk_nGangedPixels.SetActive(active);
      otherTrk_nGangedFlaggedFakes.SetActive(active);
      otherTrk_nPixelSpoiltHits.SetActive(active);
      otherTrk_nSCTDoubleHoles.SetActive(active);
      otherTrk_nSCTSpoiltHits.SetActive(active);
      otherTrk_expectBLayerHit.SetActive(active);
      otherTrk_nHits.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TrigEFTauD3PDCollection::ReadAllActive()
  {
    if(EF_tau125_medium1.IsActive()) EF_tau125_medium1();
    if(EF_tau125_medium1_L2StarA.IsActive()) EF_tau125_medium1_L2StarA();
    if(EF_tau125_medium1_L2StarB.IsActive()) EF_tau125_medium1_L2StarB();
    if(EF_tau125_medium1_L2StarC.IsActive()) EF_tau125_medium1_L2StarC();
    if(EF_tau125_medium1_llh.IsActive()) EF_tau125_medium1_llh();
    if(EF_tau20T_medium.IsActive()) EF_tau20T_medium();
    if(EF_tau20T_medium1.IsActive()) EF_tau20T_medium1();
    if(EF_tau20T_medium1_e15vh_medium1.IsActive()) EF_tau20T_medium1_e15vh_medium1();
    if(EF_tau20T_medium1_mu15i.IsActive()) EF_tau20T_medium1_mu15i();
    if(EF_tau20T_medium_mu15.IsActive()) EF_tau20T_medium_mu15();
    if(EF_tau20Ti_medium.IsActive()) EF_tau20Ti_medium();
    if(EF_tau20Ti_medium1.IsActive()) EF_tau20Ti_medium1();
    if(EF_tau20Ti_medium1_e18vh_medium1.IsActive()) EF_tau20Ti_medium1_e18vh_medium1();
    if(EF_tau20Ti_medium1_llh_e18vh_medium1.IsActive()) EF_tau20Ti_medium1_llh_e18vh_medium1();
    if(EF_tau20Ti_medium_e18vh_medium1.IsActive()) EF_tau20Ti_medium_e18vh_medium1();
    if(EF_tau20Ti_tight1.IsActive()) EF_tau20Ti_tight1();
    if(EF_tau20Ti_tight1_llh.IsActive()) EF_tau20Ti_tight1_llh();
    if(EF_tau20_medium.IsActive()) EF_tau20_medium();
    if(EF_tau20_medium1.IsActive()) EF_tau20_medium1();
    if(EF_tau20_medium1_llh.IsActive()) EF_tau20_medium1_llh();
    if(EF_tau20_medium1_llh_mu15.IsActive()) EF_tau20_medium1_llh_mu15();
    if(EF_tau20_medium1_mu15.IsActive()) EF_tau20_medium1_mu15();
    if(EF_tau20_medium1_mu15i.IsActive()) EF_tau20_medium1_mu15i();
    if(EF_tau20_medium1_mu18.IsActive()) EF_tau20_medium1_mu18();
    if(EF_tau20_medium_llh.IsActive()) EF_tau20_medium_llh();
    if(EF_tau20_medium_mu15.IsActive()) EF_tau20_medium_mu15();
    if(EF_tau29T_medium.IsActive()) EF_tau29T_medium();
    if(EF_tau29T_medium1.IsActive()) EF_tau29T_medium1();
    if(EF_tau29T_medium1_tau20T_medium1.IsActive()) EF_tau29T_medium1_tau20T_medium1();
    if(EF_tau29T_medium1_xe40_tight.IsActive()) EF_tau29T_medium1_xe40_tight();
    if(EF_tau29T_medium1_xe45_tight.IsActive()) EF_tau29T_medium1_xe45_tight();
    if(EF_tau29T_medium_xe40_tight.IsActive()) EF_tau29T_medium_xe40_tight();
    if(EF_tau29T_medium_xe45_tight.IsActive()) EF_tau29T_medium_xe45_tight();
    if(EF_tau29T_tight1.IsActive()) EF_tau29T_tight1();
    if(EF_tau29T_tight1_llh.IsActive()) EF_tau29T_tight1_llh();
    if(EF_tau29Ti_medium1.IsActive()) EF_tau29Ti_medium1();
    if(EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh.IsActive()) EF_tau29Ti_medium1_llh_tau20Ti_medium1_llh();
    if(EF_tau29Ti_medium1_llh_xe40_tight.IsActive()) EF_tau29Ti_medium1_llh_xe40_tight();
    if(EF_tau29Ti_medium1_llh_xe45_tight.IsActive()) EF_tau29Ti_medium1_llh_xe45_tight();
    if(EF_tau29Ti_medium1_tau20Ti_medium1.IsActive()) EF_tau29Ti_medium1_tau20Ti_medium1();
    if(EF_tau29Ti_medium1_xe40_tight.IsActive()) EF_tau29Ti_medium1_xe40_tight();
    if(EF_tau29Ti_medium1_xe45_tight.IsActive()) EF_tau29Ti_medium1_xe45_tight();
    if(EF_tau29Ti_medium1_xe55_tclcw.IsActive()) EF_tau29Ti_medium1_xe55_tclcw();
    if(EF_tau29Ti_medium1_xe55_tclcw_tight.IsActive()) EF_tau29Ti_medium1_xe55_tclcw_tight();
    if(EF_tau29Ti_medium_xe40_tight.IsActive()) EF_tau29Ti_medium_xe40_tight();
    if(EF_tau29Ti_medium_xe45_tight.IsActive()) EF_tau29Ti_medium_xe45_tight();
    if(EF_tau29Ti_tight1.IsActive()) EF_tau29Ti_tight1();
    if(EF_tau29Ti_tight1_llh.IsActive()) EF_tau29Ti_tight1_llh();
    if(EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh.IsActive()) EF_tau29Ti_tight1_llh_tau20Ti_tight1_llh();
    if(EF_tau29Ti_tight1_tau20Ti_tight1.IsActive()) EF_tau29Ti_tight1_tau20Ti_tight1();
    if(EF_tau29_IDTrkNoCut.IsActive()) EF_tau29_IDTrkNoCut();
    if(EF_tau29_medium.IsActive()) EF_tau29_medium();
    if(EF_tau29_medium1.IsActive()) EF_tau29_medium1();
    if(EF_tau29_medium1_llh.IsActive()) EF_tau29_medium1_llh();
    if(EF_tau29_medium_2stTest.IsActive()) EF_tau29_medium_2stTest();
    if(EF_tau29_medium_L2StarA.IsActive()) EF_tau29_medium_L2StarA();
    if(EF_tau29_medium_L2StarB.IsActive()) EF_tau29_medium_L2StarB();
    if(EF_tau29_medium_L2StarC.IsActive()) EF_tau29_medium_L2StarC();
    if(EF_tau29_medium_llh.IsActive()) EF_tau29_medium_llh();
    if(EF_tau29i_medium.IsActive()) EF_tau29i_medium();
    if(EF_tau29i_medium1.IsActive()) EF_tau29i_medium1();
    if(EF_tau38T_medium.IsActive()) EF_tau38T_medium();
    if(EF_tau38T_medium1.IsActive()) EF_tau38T_medium1();
    if(EF_tau38T_medium1_e18vh_medium1.IsActive()) EF_tau38T_medium1_e18vh_medium1();
    if(EF_tau38T_medium1_llh_e18vh_medium1.IsActive()) EF_tau38T_medium1_llh_e18vh_medium1();
    if(EF_tau38T_medium1_xe40_tight.IsActive()) EF_tau38T_medium1_xe40_tight();
    if(EF_tau38T_medium1_xe45_tight.IsActive()) EF_tau38T_medium1_xe45_tight();
    if(EF_tau38T_medium1_xe55_tclcw_tight.IsActive()) EF_tau38T_medium1_xe55_tclcw_tight();
    if(EF_tau38T_medium_e18vh_medium1.IsActive()) EF_tau38T_medium_e18vh_medium1();
    if(n.IsActive()) n();
    if(Et.IsActive()) Et();
    if(pt.IsActive()) pt();
    if(m.IsActive()) m();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(px.IsActive()) px();
    if(py.IsActive()) py();
    if(pz.IsActive()) pz();
    if(author.IsActive()) author();
    if(RoIWord.IsActive()) RoIWord();
    if(nProng.IsActive()) nProng();
    if(numTrack.IsActive()) numTrack();
    if(seedCalo_numTrack.IsActive()) seedCalo_numTrack();
    if(seedCalo_nWideTrk.IsActive()) seedCalo_nWideTrk();
    if(nOtherTrk.IsActive()) nOtherTrk();
    if(seedCalo_EMRadius.IsActive()) seedCalo_EMRadius();
    if(seedCalo_hadRadius.IsActive()) seedCalo_hadRadius();
    if(seedCalo_etEMAtEMScale.IsActive()) seedCalo_etEMAtEMScale();
    if(seedCalo_etHadAtEMScale.IsActive()) seedCalo_etHadAtEMScale();
    if(seedCalo_isolFrac.IsActive()) seedCalo_isolFrac();
    if(seedCalo_centFrac.IsActive()) seedCalo_centFrac();
    if(seedCalo_etEMCalib.IsActive()) seedCalo_etEMCalib();
    if(seedCalo_etHadCalib.IsActive()) seedCalo_etHadCalib();
    if(seedCalo_eta.IsActive()) seedCalo_eta();
    if(seedCalo_phi.IsActive()) seedCalo_phi();
    if(seedCalo_stripWidth2.IsActive()) seedCalo_stripWidth2();
    if(seedCalo_nStrip.IsActive()) seedCalo_nStrip();
    if(seedCalo_trkAvgDist.IsActive()) seedCalo_trkAvgDist();
    if(seedCalo_trkRmsDist.IsActive()) seedCalo_trkRmsDist();
    if(seedCalo_dRmax.IsActive()) seedCalo_dRmax();
    if(seedCalo_lead2ClusterEOverAllClusterE.IsActive()) seedCalo_lead2ClusterEOverAllClusterE();
    if(seedCalo_lead3ClusterEOverAllClusterE.IsActive()) seedCalo_lead3ClusterEOverAllClusterE();
    if(etOverPtLeadTrk.IsActive()) etOverPtLeadTrk();
    if(ipZ0SinThetaSigLeadTrk.IsActive()) ipZ0SinThetaSigLeadTrk();
    if(LC_TES_precalib.IsActive()) LC_TES_precalib();
    if(leadTrkPt.IsActive()) leadTrkPt();
    if(ipSigLeadTrk.IsActive()) ipSigLeadTrk();
    if(trFlightPathSig.IsActive()) trFlightPathSig();
    if(numTopoClusters.IsActive()) numTopoClusters();
    if(numEffTopoClusters.IsActive()) numEffTopoClusters();
    if(topoInvMass.IsActive()) topoInvMass();
    if(effTopoInvMass.IsActive()) effTopoInvMass();
    if(topoMeanDeltaR.IsActive()) topoMeanDeltaR();
    if(effTopoMeanDeltaR.IsActive()) effTopoMeanDeltaR();
    if(numCells.IsActive()) numCells();
    if(massTrkSys.IsActive()) massTrkSys();
    if(BDTJetScore.IsActive()) BDTJetScore();
    if(likelihood.IsActive()) likelihood();
    if(track_n.IsActive()) track_n();
    if(track_d0.IsActive()) track_d0();
    if(track_z0.IsActive()) track_z0();
    if(track_phi.IsActive()) track_phi();
    if(track_theta.IsActive()) track_theta();
    if(track_qoverp.IsActive()) track_qoverp();
    if(track_pt.IsActive()) track_pt();
    if(track_eta.IsActive()) track_eta();
    if(track_atPV_d0.IsActive()) track_atPV_d0();
    if(track_atPV_z0.IsActive()) track_atPV_z0();
    if(track_atPV_phi.IsActive()) track_atPV_phi();
    if(track_atPV_theta.IsActive()) track_atPV_theta();
    if(track_atPV_qoverp.IsActive()) track_atPV_qoverp();
    if(track_atPV_pt.IsActive()) track_atPV_pt();
    if(track_atPV_eta.IsActive()) track_atPV_eta();
    if(track_nBLHits.IsActive()) track_nBLHits();
    if(track_nPixHits.IsActive()) track_nPixHits();
    if(track_nSCTHits.IsActive()) track_nSCTHits();
    if(track_nTRTHits.IsActive()) track_nTRTHits();
    if(track_nTRTHighTHits.IsActive()) track_nTRTHighTHits();
    if(track_nPixHoles.IsActive()) track_nPixHoles();
    if(track_nSCTHoles.IsActive()) track_nSCTHoles();
    if(track_nTRTHoles.IsActive()) track_nTRTHoles();
    if(track_nPixelDeadSensors.IsActive()) track_nPixelDeadSensors();
    if(track_nSCTDeadSensors.IsActive()) track_nSCTDeadSensors();
    if(track_nBLSharedHits.IsActive()) track_nBLSharedHits();
    if(track_nPixSharedHits.IsActive()) track_nPixSharedHits();
    if(track_nSCTSharedHits.IsActive()) track_nSCTSharedHits();
    if(track_nBLayerSplitHits.IsActive()) track_nBLayerSplitHits();
    if(track_nPixSplitHits.IsActive()) track_nPixSplitHits();
    if(track_nBLayerOutliers.IsActive()) track_nBLayerOutliers();
    if(track_nPixelOutliers.IsActive()) track_nPixelOutliers();
    if(track_nSCTOutliers.IsActive()) track_nSCTOutliers();
    if(track_nTRTOutliers.IsActive()) track_nTRTOutliers();
    if(track_nTRTHighTOutliers.IsActive()) track_nTRTHighTOutliers();
    if(track_nContribPixelLayers.IsActive()) track_nContribPixelLayers();
    if(track_nGangedPixels.IsActive()) track_nGangedPixels();
    if(track_nGangedFlaggedFakes.IsActive()) track_nGangedFlaggedFakes();
    if(track_nPixelSpoiltHits.IsActive()) track_nPixelSpoiltHits();
    if(track_nSCTDoubleHoles.IsActive()) track_nSCTDoubleHoles();
    if(track_nSCTSpoiltHits.IsActive()) track_nSCTSpoiltHits();
    if(track_expectBLayerHit.IsActive()) track_expectBLayerHit();
    if(track_nHits.IsActive()) track_nHits();
    if(track_TRTHighTHitsRatio.IsActive()) track_TRTHighTHitsRatio();
    if(track_TRTHighTOutliersRatio.IsActive()) track_TRTHighTOutliersRatio();
    if(seedCalo_track_n.IsActive()) seedCalo_track_n();
    if(seedCalo_wideTrk_n.IsActive()) seedCalo_wideTrk_n();
    if(seedCalo_wideTrk_d0.IsActive()) seedCalo_wideTrk_d0();
    if(seedCalo_wideTrk_z0.IsActive()) seedCalo_wideTrk_z0();
    if(seedCalo_wideTrk_phi.IsActive()) seedCalo_wideTrk_phi();
    if(seedCalo_wideTrk_theta.IsActive()) seedCalo_wideTrk_theta();
    if(seedCalo_wideTrk_qoverp.IsActive()) seedCalo_wideTrk_qoverp();
    if(seedCalo_wideTrk_pt.IsActive()) seedCalo_wideTrk_pt();
    if(seedCalo_wideTrk_eta.IsActive()) seedCalo_wideTrk_eta();
    if(seedCalo_wideTrk_atPV_d0.IsActive()) seedCalo_wideTrk_atPV_d0();
    if(seedCalo_wideTrk_atPV_z0.IsActive()) seedCalo_wideTrk_atPV_z0();
    if(seedCalo_wideTrk_atPV_phi.IsActive()) seedCalo_wideTrk_atPV_phi();
    if(seedCalo_wideTrk_atPV_theta.IsActive()) seedCalo_wideTrk_atPV_theta();
    if(seedCalo_wideTrk_atPV_qoverp.IsActive()) seedCalo_wideTrk_atPV_qoverp();
    if(seedCalo_wideTrk_atPV_pt.IsActive()) seedCalo_wideTrk_atPV_pt();
    if(seedCalo_wideTrk_atPV_eta.IsActive()) seedCalo_wideTrk_atPV_eta();
    if(seedCalo_wideTrk_nBLHits.IsActive()) seedCalo_wideTrk_nBLHits();
    if(seedCalo_wideTrk_nPixHits.IsActive()) seedCalo_wideTrk_nPixHits();
    if(seedCalo_wideTrk_nSCTHits.IsActive()) seedCalo_wideTrk_nSCTHits();
    if(seedCalo_wideTrk_nTRTHits.IsActive()) seedCalo_wideTrk_nTRTHits();
    if(seedCalo_wideTrk_nTRTHighTHits.IsActive()) seedCalo_wideTrk_nTRTHighTHits();
    if(seedCalo_wideTrk_nPixHoles.IsActive()) seedCalo_wideTrk_nPixHoles();
    if(seedCalo_wideTrk_nSCTHoles.IsActive()) seedCalo_wideTrk_nSCTHoles();
    if(seedCalo_wideTrk_nTRTHoles.IsActive()) seedCalo_wideTrk_nTRTHoles();
    if(seedCalo_wideTrk_nPixelDeadSensors.IsActive()) seedCalo_wideTrk_nPixelDeadSensors();
    if(seedCalo_wideTrk_nSCTDeadSensors.IsActive()) seedCalo_wideTrk_nSCTDeadSensors();
    if(seedCalo_wideTrk_nBLSharedHits.IsActive()) seedCalo_wideTrk_nBLSharedHits();
    if(seedCalo_wideTrk_nPixSharedHits.IsActive()) seedCalo_wideTrk_nPixSharedHits();
    if(seedCalo_wideTrk_nSCTSharedHits.IsActive()) seedCalo_wideTrk_nSCTSharedHits();
    if(seedCalo_wideTrk_nBLayerSplitHits.IsActive()) seedCalo_wideTrk_nBLayerSplitHits();
    if(seedCalo_wideTrk_nPixSplitHits.IsActive()) seedCalo_wideTrk_nPixSplitHits();
    if(seedCalo_wideTrk_nBLayerOutliers.IsActive()) seedCalo_wideTrk_nBLayerOutliers();
    if(seedCalo_wideTrk_nPixelOutliers.IsActive()) seedCalo_wideTrk_nPixelOutliers();
    if(seedCalo_wideTrk_nSCTOutliers.IsActive()) seedCalo_wideTrk_nSCTOutliers();
    if(seedCalo_wideTrk_nTRTOutliers.IsActive()) seedCalo_wideTrk_nTRTOutliers();
    if(seedCalo_wideTrk_nTRTHighTOutliers.IsActive()) seedCalo_wideTrk_nTRTHighTOutliers();
    if(seedCalo_wideTrk_nContribPixelLayers.IsActive()) seedCalo_wideTrk_nContribPixelLayers();
    if(seedCalo_wideTrk_nGangedPixels.IsActive()) seedCalo_wideTrk_nGangedPixels();
    if(seedCalo_wideTrk_nGangedFlaggedFakes.IsActive()) seedCalo_wideTrk_nGangedFlaggedFakes();
    if(seedCalo_wideTrk_nPixelSpoiltHits.IsActive()) seedCalo_wideTrk_nPixelSpoiltHits();
    if(seedCalo_wideTrk_nSCTDoubleHoles.IsActive()) seedCalo_wideTrk_nSCTDoubleHoles();
    if(seedCalo_wideTrk_nSCTSpoiltHits.IsActive()) seedCalo_wideTrk_nSCTSpoiltHits();
    if(seedCalo_wideTrk_expectBLayerHit.IsActive()) seedCalo_wideTrk_expectBLayerHit();
    if(seedCalo_wideTrk_nHits.IsActive()) seedCalo_wideTrk_nHits();
    if(otherTrk_n.IsActive()) otherTrk_n();
    if(otherTrk_d0.IsActive()) otherTrk_d0();
    if(otherTrk_z0.IsActive()) otherTrk_z0();
    if(otherTrk_phi.IsActive()) otherTrk_phi();
    if(otherTrk_theta.IsActive()) otherTrk_theta();
    if(otherTrk_qoverp.IsActive()) otherTrk_qoverp();
    if(otherTrk_pt.IsActive()) otherTrk_pt();
    if(otherTrk_eta.IsActive()) otherTrk_eta();
    if(otherTrk_atPV_d0.IsActive()) otherTrk_atPV_d0();
    if(otherTrk_atPV_z0.IsActive()) otherTrk_atPV_z0();
    if(otherTrk_atPV_phi.IsActive()) otherTrk_atPV_phi();
    if(otherTrk_atPV_theta.IsActive()) otherTrk_atPV_theta();
    if(otherTrk_atPV_qoverp.IsActive()) otherTrk_atPV_qoverp();
    if(otherTrk_atPV_pt.IsActive()) otherTrk_atPV_pt();
    if(otherTrk_atPV_eta.IsActive()) otherTrk_atPV_eta();
    if(otherTrk_nBLHits.IsActive()) otherTrk_nBLHits();
    if(otherTrk_nPixHits.IsActive()) otherTrk_nPixHits();
    if(otherTrk_nSCTHits.IsActive()) otherTrk_nSCTHits();
    if(otherTrk_nTRTHits.IsActive()) otherTrk_nTRTHits();
    if(otherTrk_nTRTHighTHits.IsActive()) otherTrk_nTRTHighTHits();
    if(otherTrk_nPixHoles.IsActive()) otherTrk_nPixHoles();
    if(otherTrk_nSCTHoles.IsActive()) otherTrk_nSCTHoles();
    if(otherTrk_nTRTHoles.IsActive()) otherTrk_nTRTHoles();
    if(otherTrk_nPixelDeadSensors.IsActive()) otherTrk_nPixelDeadSensors();
    if(otherTrk_nSCTDeadSensors.IsActive()) otherTrk_nSCTDeadSensors();
    if(otherTrk_nBLSharedHits.IsActive()) otherTrk_nBLSharedHits();
    if(otherTrk_nPixSharedHits.IsActive()) otherTrk_nPixSharedHits();
    if(otherTrk_nSCTSharedHits.IsActive()) otherTrk_nSCTSharedHits();
    if(otherTrk_nBLayerSplitHits.IsActive()) otherTrk_nBLayerSplitHits();
    if(otherTrk_nPixSplitHits.IsActive()) otherTrk_nPixSplitHits();
    if(otherTrk_nBLayerOutliers.IsActive()) otherTrk_nBLayerOutliers();
    if(otherTrk_nPixelOutliers.IsActive()) otherTrk_nPixelOutliers();
    if(otherTrk_nSCTOutliers.IsActive()) otherTrk_nSCTOutliers();
    if(otherTrk_nTRTOutliers.IsActive()) otherTrk_nTRTOutliers();
    if(otherTrk_nTRTHighTOutliers.IsActive()) otherTrk_nTRTHighTOutliers();
    if(otherTrk_nContribPixelLayers.IsActive()) otherTrk_nContribPixelLayers();
    if(otherTrk_nGangedPixels.IsActive()) otherTrk_nGangedPixels();
    if(otherTrk_nGangedFlaggedFakes.IsActive()) otherTrk_nGangedFlaggedFakes();
    if(otherTrk_nPixelSpoiltHits.IsActive()) otherTrk_nPixelSpoiltHits();
    if(otherTrk_nSCTDoubleHoles.IsActive()) otherTrk_nSCTDoubleHoles();
    if(otherTrk_nSCTSpoiltHits.IsActive()) otherTrk_nSCTSpoiltHits();
    if(otherTrk_expectBLayerHit.IsActive()) otherTrk_expectBLayerHit();
    if(otherTrk_nHits.IsActive()) otherTrk_nHits();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TrigEFTauD3PDCollection_CXX
