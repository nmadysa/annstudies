// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_ClusterD3PDCollection_CXX
#define D3PDREADER_ClusterD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "ClusterD3PDCollection.h"

ClassImp(D3PDReader::ClusterD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  ClusterD3PDCollection::ClusterD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    MET_n(prefix + "MET_n",&master),
    MET_wpx(prefix + "MET_wpx",&master),
    MET_wpy(prefix + "MET_wpy",&master),
    MET_wet(prefix + "MET_wet",&master),
    MET_statusWord(prefix + "MET_statusWord",&master),
    MET_em_tightpp_n(prefix + "MET_em_tightpp_n",&master),
    MET_em_tightpp_wpx(prefix + "MET_em_tightpp_wpx",&master),
    MET_em_tightpp_wpy(prefix + "MET_em_tightpp_wpy",&master),
    MET_em_tightpp_wet(prefix + "MET_em_tightpp_wet",&master),
    MET_em_tightpp_statusWord(prefix + "MET_em_tightpp_statusWord",&master),
    MET_STVF_n(prefix + "MET_STVF_n",&master),
    MET_STVF_wpx(prefix + "MET_STVF_wpx",&master),
    MET_STVF_wpy(prefix + "MET_STVF_wpy",&master),
    MET_STVF_wet(prefix + "MET_STVF_wet",&master),
    MET_STVF_statusWord(prefix + "MET_STVF_statusWord",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  ClusterD3PDCollection::ClusterD3PDCollection(const std::string& prefix):
    TObject(),
    MET_n(prefix + "MET_n",0),
    MET_wpx(prefix + "MET_wpx",0),
    MET_wpy(prefix + "MET_wpy",0),
    MET_wet(prefix + "MET_wet",0),
    MET_statusWord(prefix + "MET_statusWord",0),
    MET_em_tightpp_n(prefix + "MET_em_tightpp_n",0),
    MET_em_tightpp_wpx(prefix + "MET_em_tightpp_wpx",0),
    MET_em_tightpp_wpy(prefix + "MET_em_tightpp_wpy",0),
    MET_em_tightpp_wet(prefix + "MET_em_tightpp_wet",0),
    MET_em_tightpp_statusWord(prefix + "MET_em_tightpp_statusWord",0),
    MET_STVF_n(prefix + "MET_STVF_n",0),
    MET_STVF_wpx(prefix + "MET_STVF_wpx",0),
    MET_STVF_wpy(prefix + "MET_STVF_wpy",0),
    MET_STVF_wet(prefix + "MET_STVF_wet",0),
    MET_STVF_statusWord(prefix + "MET_STVF_statusWord",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void ClusterD3PDCollection::ReadFrom(TTree* tree)
  {
    MET_n.ReadFrom(tree);
    MET_wpx.ReadFrom(tree);
    MET_wpy.ReadFrom(tree);
    MET_wet.ReadFrom(tree);
    MET_statusWord.ReadFrom(tree);
    MET_em_tightpp_n.ReadFrom(tree);
    MET_em_tightpp_wpx.ReadFrom(tree);
    MET_em_tightpp_wpy.ReadFrom(tree);
    MET_em_tightpp_wet.ReadFrom(tree);
    MET_em_tightpp_statusWord.ReadFrom(tree);
    MET_STVF_n.ReadFrom(tree);
    MET_STVF_wpx.ReadFrom(tree);
    MET_STVF_wpy.ReadFrom(tree);
    MET_STVF_wet.ReadFrom(tree);
    MET_STVF_statusWord.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void ClusterD3PDCollection::WriteTo(TTree* tree)
  {
    MET_n.WriteTo(tree);
    MET_wpx.WriteTo(tree);
    MET_wpy.WriteTo(tree);
    MET_wet.WriteTo(tree);
    MET_statusWord.WriteTo(tree);
    MET_em_tightpp_n.WriteTo(tree);
    MET_em_tightpp_wpx.WriteTo(tree);
    MET_em_tightpp_wpy.WriteTo(tree);
    MET_em_tightpp_wet.WriteTo(tree);
    MET_em_tightpp_statusWord.WriteTo(tree);
    MET_STVF_n.WriteTo(tree);
    MET_STVF_wpx.WriteTo(tree);
    MET_STVF_wpy.WriteTo(tree);
    MET_STVF_wet.WriteTo(tree);
    MET_STVF_statusWord.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void ClusterD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(MET_n.IsAvailable()) MET_n.SetActive(active);
     if(MET_wpx.IsAvailable()) MET_wpx.SetActive(active);
     if(MET_wpy.IsAvailable()) MET_wpy.SetActive(active);
     if(MET_wet.IsAvailable()) MET_wet.SetActive(active);
     if(MET_statusWord.IsAvailable()) MET_statusWord.SetActive(active);
     if(MET_em_tightpp_n.IsAvailable()) MET_em_tightpp_n.SetActive(active);
     if(MET_em_tightpp_wpx.IsAvailable()) MET_em_tightpp_wpx.SetActive(active);
     if(MET_em_tightpp_wpy.IsAvailable()) MET_em_tightpp_wpy.SetActive(active);
     if(MET_em_tightpp_wet.IsAvailable()) MET_em_tightpp_wet.SetActive(active);
     if(MET_em_tightpp_statusWord.IsAvailable()) MET_em_tightpp_statusWord.SetActive(active);
     if(MET_STVF_n.IsAvailable()) MET_STVF_n.SetActive(active);
     if(MET_STVF_wpx.IsAvailable()) MET_STVF_wpx.SetActive(active);
     if(MET_STVF_wpy.IsAvailable()) MET_STVF_wpy.SetActive(active);
     if(MET_STVF_wet.IsAvailable()) MET_STVF_wet.SetActive(active);
     if(MET_STVF_statusWord.IsAvailable()) MET_STVF_statusWord.SetActive(active);
    }
    else
    {
      MET_n.SetActive(active);
      MET_wpx.SetActive(active);
      MET_wpy.SetActive(active);
      MET_wet.SetActive(active);
      MET_statusWord.SetActive(active);
      MET_em_tightpp_n.SetActive(active);
      MET_em_tightpp_wpx.SetActive(active);
      MET_em_tightpp_wpy.SetActive(active);
      MET_em_tightpp_wet.SetActive(active);
      MET_em_tightpp_statusWord.SetActive(active);
      MET_STVF_n.SetActive(active);
      MET_STVF_wpx.SetActive(active);
      MET_STVF_wpy.SetActive(active);
      MET_STVF_wet.SetActive(active);
      MET_STVF_statusWord.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void ClusterD3PDCollection::ReadAllActive()
  {
    if(MET_n.IsActive()) MET_n();
    if(MET_wpx.IsActive()) MET_wpx();
    if(MET_wpy.IsActive()) MET_wpy();
    if(MET_wet.IsActive()) MET_wet();
    if(MET_statusWord.IsActive()) MET_statusWord();
    if(MET_em_tightpp_n.IsActive()) MET_em_tightpp_n();
    if(MET_em_tightpp_wpx.IsActive()) MET_em_tightpp_wpx();
    if(MET_em_tightpp_wpy.IsActive()) MET_em_tightpp_wpy();
    if(MET_em_tightpp_wet.IsActive()) MET_em_tightpp_wet();
    if(MET_em_tightpp_statusWord.IsActive()) MET_em_tightpp_statusWord();
    if(MET_STVF_n.IsActive()) MET_STVF_n();
    if(MET_STVF_wpx.IsActive()) MET_STVF_wpx();
    if(MET_STVF_wpy.IsActive()) MET_STVF_wpy();
    if(MET_STVF_wet.IsActive()) MET_STVF_wet();
    if(MET_STVF_statusWord.IsActive()) MET_STVF_statusWord();
  }

} // namespace D3PDReader
#endif // D3PDREADER_ClusterD3PDCollection_CXX
