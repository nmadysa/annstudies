// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TrackD3PDCollection_CXX
#define D3PDREADER_TrackD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TrackD3PDCollection.h"

ClassImp(D3PDReader::TrackD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TrackD3PDCollection::TrackD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    MET_n(prefix + "MET_n",&master),
    MET_wpx(prefix + "MET_wpx",&master),
    MET_wpy(prefix + "MET_wpy",&master),
    MET_wet(prefix + "MET_wet",&master),
    MET_statusWord(prefix + "MET_statusWord",&master),
    MET_em_tightpp_n(prefix + "MET_em_tightpp_n",&master),
    MET_em_tightpp_wpx(prefix + "MET_em_tightpp_wpx",&master),
    MET_em_tightpp_wpy(prefix + "MET_em_tightpp_wpy",&master),
    MET_em_tightpp_wet(prefix + "MET_em_tightpp_wet",&master),
    MET_em_tightpp_statusWord(prefix + "MET_em_tightpp_statusWord",&master),
    MET_STVF_n(prefix + "MET_STVF_n",&master),
    MET_STVF_wpx(prefix + "MET_STVF_wpx",&master),
    MET_STVF_wpy(prefix + "MET_STVF_wpy",&master),
    MET_STVF_wet(prefix + "MET_STVF_wet",&master),
    MET_STVF_statusWord(prefix + "MET_STVF_statusWord",&master),
    n(prefix + "n",&master),
    d0(prefix + "d0",&master),
    z0(prefix + "z0",&master),
    phi(prefix + "phi",&master),
    theta(prefix + "theta",&master),
    qoverp(prefix + "qoverp",&master),
    pt(prefix + "pt",&master),
    eta(prefix + "eta",&master),
    d0_wrtPV(prefix + "d0_wrtPV",&master),
    z0_wrtPV(prefix + "z0_wrtPV",&master),
    phi_wrtPV(prefix + "phi_wrtPV",&master),
    d0_wrtBS(prefix + "d0_wrtBS",&master),
    z0_wrtBS(prefix + "z0_wrtBS",&master),
    phi_wrtBS(prefix + "phi_wrtBS",&master),
    err_d0_wrtBS(prefix + "err_d0_wrtBS",&master),
    err_z0_wrtBS(prefix + "err_z0_wrtBS",&master),
    err_phi_wrtBS(prefix + "err_phi_wrtBS",&master),
    err_theta_wrtBS(prefix + "err_theta_wrtBS",&master),
    err_qoverp_wrtBS(prefix + "err_qoverp_wrtBS",&master),
    cov_d0_z0_wrtBS(prefix + "cov_d0_z0_wrtBS",&master),
    cov_d0_phi_wrtBS(prefix + "cov_d0_phi_wrtBS",&master),
    cov_d0_theta_wrtBS(prefix + "cov_d0_theta_wrtBS",&master),
    cov_d0_qoverp_wrtBS(prefix + "cov_d0_qoverp_wrtBS",&master),
    cov_z0_phi_wrtBS(prefix + "cov_z0_phi_wrtBS",&master),
    cov_z0_theta_wrtBS(prefix + "cov_z0_theta_wrtBS",&master),
    cov_z0_qoverp_wrtBS(prefix + "cov_z0_qoverp_wrtBS",&master),
    cov_phi_theta_wrtBS(prefix + "cov_phi_theta_wrtBS",&master),
    cov_phi_qoverp_wrtBS(prefix + "cov_phi_qoverp_wrtBS",&master),
    cov_theta_qoverp_wrtBS(prefix + "cov_theta_qoverp_wrtBS",&master),
    d0_wrtBL(prefix + "d0_wrtBL",&master),
    z0_wrtBL(prefix + "z0_wrtBL",&master),
    phi_wrtBL(prefix + "phi_wrtBL",&master),
    d0_err_wrtBL(prefix + "d0_err_wrtBL",&master),
    z0_err_wrtBL(prefix + "z0_err_wrtBL",&master),
    phi_err_wrtBL(prefix + "phi_err_wrtBL",&master),
    theta_err_wrtBL(prefix + "theta_err_wrtBL",&master),
    qoverp_err_wrtBL(prefix + "qoverp_err_wrtBL",&master),
    d0_z0_err_wrtBL(prefix + "d0_z0_err_wrtBL",&master),
    d0_phi_err_wrtBL(prefix + "d0_phi_err_wrtBL",&master),
    d0_theta_err_wrtBL(prefix + "d0_theta_err_wrtBL",&master),
    d0_qoverp_err_wrtBL(prefix + "d0_qoverp_err_wrtBL",&master),
    z0_phi_err_wrtBL(prefix + "z0_phi_err_wrtBL",&master),
    z0_theta_err_wrtBL(prefix + "z0_theta_err_wrtBL",&master),
    z0_qoverp_err_wrtBL(prefix + "z0_qoverp_err_wrtBL",&master),
    phi_theta_err_wrtBL(prefix + "phi_theta_err_wrtBL",&master),
    phi_qoverp_err_wrtBL(prefix + "phi_qoverp_err_wrtBL",&master),
    theta_qoverp_err_wrtBL(prefix + "theta_qoverp_err_wrtBL",&master),
    chi2(prefix + "chi2",&master),
    ndof(prefix + "ndof",&master),
    nBLHits(prefix + "nBLHits",&master),
    nPixHits(prefix + "nPixHits",&master),
    nSCTHits(prefix + "nSCTHits",&master),
    nTRTHits(prefix + "nTRTHits",&master),
    nTRTHighTHits(prefix + "nTRTHighTHits",&master),
    nPixHoles(prefix + "nPixHoles",&master),
    nSCTHoles(prefix + "nSCTHoles",&master),
    nTRTHoles(prefix + "nTRTHoles",&master),
    nPixelDeadSensors(prefix + "nPixelDeadSensors",&master),
    nSCTDeadSensors(prefix + "nSCTDeadSensors",&master),
    nBLSharedHits(prefix + "nBLSharedHits",&master),
    nPixSharedHits(prefix + "nPixSharedHits",&master),
    nSCTSharedHits(prefix + "nSCTSharedHits",&master),
    nBLayerSplitHits(prefix + "nBLayerSplitHits",&master),
    nPixSplitHits(prefix + "nPixSplitHits",&master),
    expectBLayerHit(prefix + "expectBLayerHit",&master),
    nMDTHits(prefix + "nMDTHits",&master),
    nCSCEtaHits(prefix + "nCSCEtaHits",&master),
    nCSCPhiHits(prefix + "nCSCPhiHits",&master),
    nRPCEtaHits(prefix + "nRPCEtaHits",&master),
    nRPCPhiHits(prefix + "nRPCPhiHits",&master),
    nTGCEtaHits(prefix + "nTGCEtaHits",&master),
    nTGCPhiHits(prefix + "nTGCPhiHits",&master),
    nHits(prefix + "nHits",&master),
    nHoles(prefix + "nHoles",&master),
    hitPattern(prefix + "hitPattern",&master),
    TRTHighTHitsRatio(prefix + "TRTHighTHitsRatio",&master),
    TRTHighTOutliersRatio(prefix + "TRTHighTOutliersRatio",&master),
    fitter(prefix + "fitter",&master),
    patternReco1(prefix + "patternReco1",&master),
    patternReco2(prefix + "patternReco2",&master),
    trackProperties(prefix + "trackProperties",&master),
    particleHypothesis(prefix + "particleHypothesis",&master),
    mc_probability(prefix + "mc_probability",&master),
    mc_barcode(prefix + "mc_barcode",&master),
    atTJVA_phi(prefix + "atTJVA_phi",&master),
    atTJVA_d0(prefix + "atTJVA_d0",&master),
    atTJVA_z0(prefix + "atTJVA_z0",&master),
    atTJVA_theta(prefix + "atTJVA_theta",&master),
    atTJVA_qoverp(prefix + "atTJVA_qoverp",&master),
    atTJVA_n(prefix + "atTJVA_n",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TrackD3PDCollection::TrackD3PDCollection(const std::string& prefix):
    TObject(),
    MET_n(prefix + "MET_n",0),
    MET_wpx(prefix + "MET_wpx",0),
    MET_wpy(prefix + "MET_wpy",0),
    MET_wet(prefix + "MET_wet",0),
    MET_statusWord(prefix + "MET_statusWord",0),
    MET_em_tightpp_n(prefix + "MET_em_tightpp_n",0),
    MET_em_tightpp_wpx(prefix + "MET_em_tightpp_wpx",0),
    MET_em_tightpp_wpy(prefix + "MET_em_tightpp_wpy",0),
    MET_em_tightpp_wet(prefix + "MET_em_tightpp_wet",0),
    MET_em_tightpp_statusWord(prefix + "MET_em_tightpp_statusWord",0),
    MET_STVF_n(prefix + "MET_STVF_n",0),
    MET_STVF_wpx(prefix + "MET_STVF_wpx",0),
    MET_STVF_wpy(prefix + "MET_STVF_wpy",0),
    MET_STVF_wet(prefix + "MET_STVF_wet",0),
    MET_STVF_statusWord(prefix + "MET_STVF_statusWord",0),
    n(prefix + "n",0),
    d0(prefix + "d0",0),
    z0(prefix + "z0",0),
    phi(prefix + "phi",0),
    theta(prefix + "theta",0),
    qoverp(prefix + "qoverp",0),
    pt(prefix + "pt",0),
    eta(prefix + "eta",0),
    d0_wrtPV(prefix + "d0_wrtPV",0),
    z0_wrtPV(prefix + "z0_wrtPV",0),
    phi_wrtPV(prefix + "phi_wrtPV",0),
    d0_wrtBS(prefix + "d0_wrtBS",0),
    z0_wrtBS(prefix + "z0_wrtBS",0),
    phi_wrtBS(prefix + "phi_wrtBS",0),
    err_d0_wrtBS(prefix + "err_d0_wrtBS",0),
    err_z0_wrtBS(prefix + "err_z0_wrtBS",0),
    err_phi_wrtBS(prefix + "err_phi_wrtBS",0),
    err_theta_wrtBS(prefix + "err_theta_wrtBS",0),
    err_qoverp_wrtBS(prefix + "err_qoverp_wrtBS",0),
    cov_d0_z0_wrtBS(prefix + "cov_d0_z0_wrtBS",0),
    cov_d0_phi_wrtBS(prefix + "cov_d0_phi_wrtBS",0),
    cov_d0_theta_wrtBS(prefix + "cov_d0_theta_wrtBS",0),
    cov_d0_qoverp_wrtBS(prefix + "cov_d0_qoverp_wrtBS",0),
    cov_z0_phi_wrtBS(prefix + "cov_z0_phi_wrtBS",0),
    cov_z0_theta_wrtBS(prefix + "cov_z0_theta_wrtBS",0),
    cov_z0_qoverp_wrtBS(prefix + "cov_z0_qoverp_wrtBS",0),
    cov_phi_theta_wrtBS(prefix + "cov_phi_theta_wrtBS",0),
    cov_phi_qoverp_wrtBS(prefix + "cov_phi_qoverp_wrtBS",0),
    cov_theta_qoverp_wrtBS(prefix + "cov_theta_qoverp_wrtBS",0),
    d0_wrtBL(prefix + "d0_wrtBL",0),
    z0_wrtBL(prefix + "z0_wrtBL",0),
    phi_wrtBL(prefix + "phi_wrtBL",0),
    d0_err_wrtBL(prefix + "d0_err_wrtBL",0),
    z0_err_wrtBL(prefix + "z0_err_wrtBL",0),
    phi_err_wrtBL(prefix + "phi_err_wrtBL",0),
    theta_err_wrtBL(prefix + "theta_err_wrtBL",0),
    qoverp_err_wrtBL(prefix + "qoverp_err_wrtBL",0),
    d0_z0_err_wrtBL(prefix + "d0_z0_err_wrtBL",0),
    d0_phi_err_wrtBL(prefix + "d0_phi_err_wrtBL",0),
    d0_theta_err_wrtBL(prefix + "d0_theta_err_wrtBL",0),
    d0_qoverp_err_wrtBL(prefix + "d0_qoverp_err_wrtBL",0),
    z0_phi_err_wrtBL(prefix + "z0_phi_err_wrtBL",0),
    z0_theta_err_wrtBL(prefix + "z0_theta_err_wrtBL",0),
    z0_qoverp_err_wrtBL(prefix + "z0_qoverp_err_wrtBL",0),
    phi_theta_err_wrtBL(prefix + "phi_theta_err_wrtBL",0),
    phi_qoverp_err_wrtBL(prefix + "phi_qoverp_err_wrtBL",0),
    theta_qoverp_err_wrtBL(prefix + "theta_qoverp_err_wrtBL",0),
    chi2(prefix + "chi2",0),
    ndof(prefix + "ndof",0),
    nBLHits(prefix + "nBLHits",0),
    nPixHits(prefix + "nPixHits",0),
    nSCTHits(prefix + "nSCTHits",0),
    nTRTHits(prefix + "nTRTHits",0),
    nTRTHighTHits(prefix + "nTRTHighTHits",0),
    nPixHoles(prefix + "nPixHoles",0),
    nSCTHoles(prefix + "nSCTHoles",0),
    nTRTHoles(prefix + "nTRTHoles",0),
    nPixelDeadSensors(prefix + "nPixelDeadSensors",0),
    nSCTDeadSensors(prefix + "nSCTDeadSensors",0),
    nBLSharedHits(prefix + "nBLSharedHits",0),
    nPixSharedHits(prefix + "nPixSharedHits",0),
    nSCTSharedHits(prefix + "nSCTSharedHits",0),
    nBLayerSplitHits(prefix + "nBLayerSplitHits",0),
    nPixSplitHits(prefix + "nPixSplitHits",0),
    expectBLayerHit(prefix + "expectBLayerHit",0),
    nMDTHits(prefix + "nMDTHits",0),
    nCSCEtaHits(prefix + "nCSCEtaHits",0),
    nCSCPhiHits(prefix + "nCSCPhiHits",0),
    nRPCEtaHits(prefix + "nRPCEtaHits",0),
    nRPCPhiHits(prefix + "nRPCPhiHits",0),
    nTGCEtaHits(prefix + "nTGCEtaHits",0),
    nTGCPhiHits(prefix + "nTGCPhiHits",0),
    nHits(prefix + "nHits",0),
    nHoles(prefix + "nHoles",0),
    hitPattern(prefix + "hitPattern",0),
    TRTHighTHitsRatio(prefix + "TRTHighTHitsRatio",0),
    TRTHighTOutliersRatio(prefix + "TRTHighTOutliersRatio",0),
    fitter(prefix + "fitter",0),
    patternReco1(prefix + "patternReco1",0),
    patternReco2(prefix + "patternReco2",0),
    trackProperties(prefix + "trackProperties",0),
    particleHypothesis(prefix + "particleHypothesis",0),
    mc_probability(prefix + "mc_probability",0),
    mc_barcode(prefix + "mc_barcode",0),
    atTJVA_phi(prefix + "atTJVA_phi",0),
    atTJVA_d0(prefix + "atTJVA_d0",0),
    atTJVA_z0(prefix + "atTJVA_z0",0),
    atTJVA_theta(prefix + "atTJVA_theta",0),
    atTJVA_qoverp(prefix + "atTJVA_qoverp",0),
    atTJVA_n(prefix + "atTJVA_n",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TrackD3PDCollection::ReadFrom(TTree* tree)
  {
    MET_n.ReadFrom(tree);
    MET_wpx.ReadFrom(tree);
    MET_wpy.ReadFrom(tree);
    MET_wet.ReadFrom(tree);
    MET_statusWord.ReadFrom(tree);
    MET_em_tightpp_n.ReadFrom(tree);
    MET_em_tightpp_wpx.ReadFrom(tree);
    MET_em_tightpp_wpy.ReadFrom(tree);
    MET_em_tightpp_wet.ReadFrom(tree);
    MET_em_tightpp_statusWord.ReadFrom(tree);
    MET_STVF_n.ReadFrom(tree);
    MET_STVF_wpx.ReadFrom(tree);
    MET_STVF_wpy.ReadFrom(tree);
    MET_STVF_wet.ReadFrom(tree);
    MET_STVF_statusWord.ReadFrom(tree);
    n.ReadFrom(tree);
    d0.ReadFrom(tree);
    z0.ReadFrom(tree);
    phi.ReadFrom(tree);
    theta.ReadFrom(tree);
    qoverp.ReadFrom(tree);
    pt.ReadFrom(tree);
    eta.ReadFrom(tree);
    d0_wrtPV.ReadFrom(tree);
    z0_wrtPV.ReadFrom(tree);
    phi_wrtPV.ReadFrom(tree);
    d0_wrtBS.ReadFrom(tree);
    z0_wrtBS.ReadFrom(tree);
    phi_wrtBS.ReadFrom(tree);
    err_d0_wrtBS.ReadFrom(tree);
    err_z0_wrtBS.ReadFrom(tree);
    err_phi_wrtBS.ReadFrom(tree);
    err_theta_wrtBS.ReadFrom(tree);
    err_qoverp_wrtBS.ReadFrom(tree);
    cov_d0_z0_wrtBS.ReadFrom(tree);
    cov_d0_phi_wrtBS.ReadFrom(tree);
    cov_d0_theta_wrtBS.ReadFrom(tree);
    cov_d0_qoverp_wrtBS.ReadFrom(tree);
    cov_z0_phi_wrtBS.ReadFrom(tree);
    cov_z0_theta_wrtBS.ReadFrom(tree);
    cov_z0_qoverp_wrtBS.ReadFrom(tree);
    cov_phi_theta_wrtBS.ReadFrom(tree);
    cov_phi_qoverp_wrtBS.ReadFrom(tree);
    cov_theta_qoverp_wrtBS.ReadFrom(tree);
    d0_wrtBL.ReadFrom(tree);
    z0_wrtBL.ReadFrom(tree);
    phi_wrtBL.ReadFrom(tree);
    d0_err_wrtBL.ReadFrom(tree);
    z0_err_wrtBL.ReadFrom(tree);
    phi_err_wrtBL.ReadFrom(tree);
    theta_err_wrtBL.ReadFrom(tree);
    qoverp_err_wrtBL.ReadFrom(tree);
    d0_z0_err_wrtBL.ReadFrom(tree);
    d0_phi_err_wrtBL.ReadFrom(tree);
    d0_theta_err_wrtBL.ReadFrom(tree);
    d0_qoverp_err_wrtBL.ReadFrom(tree);
    z0_phi_err_wrtBL.ReadFrom(tree);
    z0_theta_err_wrtBL.ReadFrom(tree);
    z0_qoverp_err_wrtBL.ReadFrom(tree);
    phi_theta_err_wrtBL.ReadFrom(tree);
    phi_qoverp_err_wrtBL.ReadFrom(tree);
    theta_qoverp_err_wrtBL.ReadFrom(tree);
    chi2.ReadFrom(tree);
    ndof.ReadFrom(tree);
    nBLHits.ReadFrom(tree);
    nPixHits.ReadFrom(tree);
    nSCTHits.ReadFrom(tree);
    nTRTHits.ReadFrom(tree);
    nTRTHighTHits.ReadFrom(tree);
    nPixHoles.ReadFrom(tree);
    nSCTHoles.ReadFrom(tree);
    nTRTHoles.ReadFrom(tree);
    nPixelDeadSensors.ReadFrom(tree);
    nSCTDeadSensors.ReadFrom(tree);
    nBLSharedHits.ReadFrom(tree);
    nPixSharedHits.ReadFrom(tree);
    nSCTSharedHits.ReadFrom(tree);
    nBLayerSplitHits.ReadFrom(tree);
    nPixSplitHits.ReadFrom(tree);
    expectBLayerHit.ReadFrom(tree);
    nMDTHits.ReadFrom(tree);
    nCSCEtaHits.ReadFrom(tree);
    nCSCPhiHits.ReadFrom(tree);
    nRPCEtaHits.ReadFrom(tree);
    nRPCPhiHits.ReadFrom(tree);
    nTGCEtaHits.ReadFrom(tree);
    nTGCPhiHits.ReadFrom(tree);
    nHits.ReadFrom(tree);
    nHoles.ReadFrom(tree);
    hitPattern.ReadFrom(tree);
    TRTHighTHitsRatio.ReadFrom(tree);
    TRTHighTOutliersRatio.ReadFrom(tree);
    fitter.ReadFrom(tree);
    patternReco1.ReadFrom(tree);
    patternReco2.ReadFrom(tree);
    trackProperties.ReadFrom(tree);
    particleHypothesis.ReadFrom(tree);
    mc_probability.ReadFrom(tree);
    mc_barcode.ReadFrom(tree);
    atTJVA_phi.ReadFrom(tree);
    atTJVA_d0.ReadFrom(tree);
    atTJVA_z0.ReadFrom(tree);
    atTJVA_theta.ReadFrom(tree);
    atTJVA_qoverp.ReadFrom(tree);
    atTJVA_n.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TrackD3PDCollection::WriteTo(TTree* tree)
  {
    MET_n.WriteTo(tree);
    MET_wpx.WriteTo(tree);
    MET_wpy.WriteTo(tree);
    MET_wet.WriteTo(tree);
    MET_statusWord.WriteTo(tree);
    MET_em_tightpp_n.WriteTo(tree);
    MET_em_tightpp_wpx.WriteTo(tree);
    MET_em_tightpp_wpy.WriteTo(tree);
    MET_em_tightpp_wet.WriteTo(tree);
    MET_em_tightpp_statusWord.WriteTo(tree);
    MET_STVF_n.WriteTo(tree);
    MET_STVF_wpx.WriteTo(tree);
    MET_STVF_wpy.WriteTo(tree);
    MET_STVF_wet.WriteTo(tree);
    MET_STVF_statusWord.WriteTo(tree);
    n.WriteTo(tree);
    d0.WriteTo(tree);
    z0.WriteTo(tree);
    phi.WriteTo(tree);
    theta.WriteTo(tree);
    qoverp.WriteTo(tree);
    pt.WriteTo(tree);
    eta.WriteTo(tree);
    d0_wrtPV.WriteTo(tree);
    z0_wrtPV.WriteTo(tree);
    phi_wrtPV.WriteTo(tree);
    d0_wrtBS.WriteTo(tree);
    z0_wrtBS.WriteTo(tree);
    phi_wrtBS.WriteTo(tree);
    err_d0_wrtBS.WriteTo(tree);
    err_z0_wrtBS.WriteTo(tree);
    err_phi_wrtBS.WriteTo(tree);
    err_theta_wrtBS.WriteTo(tree);
    err_qoverp_wrtBS.WriteTo(tree);
    cov_d0_z0_wrtBS.WriteTo(tree);
    cov_d0_phi_wrtBS.WriteTo(tree);
    cov_d0_theta_wrtBS.WriteTo(tree);
    cov_d0_qoverp_wrtBS.WriteTo(tree);
    cov_z0_phi_wrtBS.WriteTo(tree);
    cov_z0_theta_wrtBS.WriteTo(tree);
    cov_z0_qoverp_wrtBS.WriteTo(tree);
    cov_phi_theta_wrtBS.WriteTo(tree);
    cov_phi_qoverp_wrtBS.WriteTo(tree);
    cov_theta_qoverp_wrtBS.WriteTo(tree);
    d0_wrtBL.WriteTo(tree);
    z0_wrtBL.WriteTo(tree);
    phi_wrtBL.WriteTo(tree);
    d0_err_wrtBL.WriteTo(tree);
    z0_err_wrtBL.WriteTo(tree);
    phi_err_wrtBL.WriteTo(tree);
    theta_err_wrtBL.WriteTo(tree);
    qoverp_err_wrtBL.WriteTo(tree);
    d0_z0_err_wrtBL.WriteTo(tree);
    d0_phi_err_wrtBL.WriteTo(tree);
    d0_theta_err_wrtBL.WriteTo(tree);
    d0_qoverp_err_wrtBL.WriteTo(tree);
    z0_phi_err_wrtBL.WriteTo(tree);
    z0_theta_err_wrtBL.WriteTo(tree);
    z0_qoverp_err_wrtBL.WriteTo(tree);
    phi_theta_err_wrtBL.WriteTo(tree);
    phi_qoverp_err_wrtBL.WriteTo(tree);
    theta_qoverp_err_wrtBL.WriteTo(tree);
    chi2.WriteTo(tree);
    ndof.WriteTo(tree);
    nBLHits.WriteTo(tree);
    nPixHits.WriteTo(tree);
    nSCTHits.WriteTo(tree);
    nTRTHits.WriteTo(tree);
    nTRTHighTHits.WriteTo(tree);
    nPixHoles.WriteTo(tree);
    nSCTHoles.WriteTo(tree);
    nTRTHoles.WriteTo(tree);
    nPixelDeadSensors.WriteTo(tree);
    nSCTDeadSensors.WriteTo(tree);
    nBLSharedHits.WriteTo(tree);
    nPixSharedHits.WriteTo(tree);
    nSCTSharedHits.WriteTo(tree);
    nBLayerSplitHits.WriteTo(tree);
    nPixSplitHits.WriteTo(tree);
    expectBLayerHit.WriteTo(tree);
    nMDTHits.WriteTo(tree);
    nCSCEtaHits.WriteTo(tree);
    nCSCPhiHits.WriteTo(tree);
    nRPCEtaHits.WriteTo(tree);
    nRPCPhiHits.WriteTo(tree);
    nTGCEtaHits.WriteTo(tree);
    nTGCPhiHits.WriteTo(tree);
    nHits.WriteTo(tree);
    nHoles.WriteTo(tree);
    hitPattern.WriteTo(tree);
    TRTHighTHitsRatio.WriteTo(tree);
    TRTHighTOutliersRatio.WriteTo(tree);
    fitter.WriteTo(tree);
    patternReco1.WriteTo(tree);
    patternReco2.WriteTo(tree);
    trackProperties.WriteTo(tree);
    particleHypothesis.WriteTo(tree);
    mc_probability.WriteTo(tree);
    mc_barcode.WriteTo(tree);
    atTJVA_phi.WriteTo(tree);
    atTJVA_d0.WriteTo(tree);
    atTJVA_z0.WriteTo(tree);
    atTJVA_theta.WriteTo(tree);
    atTJVA_qoverp.WriteTo(tree);
    atTJVA_n.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TrackD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(MET_n.IsAvailable()) MET_n.SetActive(active);
     if(MET_wpx.IsAvailable()) MET_wpx.SetActive(active);
     if(MET_wpy.IsAvailable()) MET_wpy.SetActive(active);
     if(MET_wet.IsAvailable()) MET_wet.SetActive(active);
     if(MET_statusWord.IsAvailable()) MET_statusWord.SetActive(active);
     if(MET_em_tightpp_n.IsAvailable()) MET_em_tightpp_n.SetActive(active);
     if(MET_em_tightpp_wpx.IsAvailable()) MET_em_tightpp_wpx.SetActive(active);
     if(MET_em_tightpp_wpy.IsAvailable()) MET_em_tightpp_wpy.SetActive(active);
     if(MET_em_tightpp_wet.IsAvailable()) MET_em_tightpp_wet.SetActive(active);
     if(MET_em_tightpp_statusWord.IsAvailable()) MET_em_tightpp_statusWord.SetActive(active);
     if(MET_STVF_n.IsAvailable()) MET_STVF_n.SetActive(active);
     if(MET_STVF_wpx.IsAvailable()) MET_STVF_wpx.SetActive(active);
     if(MET_STVF_wpy.IsAvailable()) MET_STVF_wpy.SetActive(active);
     if(MET_STVF_wet.IsAvailable()) MET_STVF_wet.SetActive(active);
     if(MET_STVF_statusWord.IsAvailable()) MET_STVF_statusWord.SetActive(active);
     if(n.IsAvailable()) n.SetActive(active);
     if(d0.IsAvailable()) d0.SetActive(active);
     if(z0.IsAvailable()) z0.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(theta.IsAvailable()) theta.SetActive(active);
     if(qoverp.IsAvailable()) qoverp.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(d0_wrtPV.IsAvailable()) d0_wrtPV.SetActive(active);
     if(z0_wrtPV.IsAvailable()) z0_wrtPV.SetActive(active);
     if(phi_wrtPV.IsAvailable()) phi_wrtPV.SetActive(active);
     if(d0_wrtBS.IsAvailable()) d0_wrtBS.SetActive(active);
     if(z0_wrtBS.IsAvailable()) z0_wrtBS.SetActive(active);
     if(phi_wrtBS.IsAvailable()) phi_wrtBS.SetActive(active);
     if(err_d0_wrtBS.IsAvailable()) err_d0_wrtBS.SetActive(active);
     if(err_z0_wrtBS.IsAvailable()) err_z0_wrtBS.SetActive(active);
     if(err_phi_wrtBS.IsAvailable()) err_phi_wrtBS.SetActive(active);
     if(err_theta_wrtBS.IsAvailable()) err_theta_wrtBS.SetActive(active);
     if(err_qoverp_wrtBS.IsAvailable()) err_qoverp_wrtBS.SetActive(active);
     if(cov_d0_z0_wrtBS.IsAvailable()) cov_d0_z0_wrtBS.SetActive(active);
     if(cov_d0_phi_wrtBS.IsAvailable()) cov_d0_phi_wrtBS.SetActive(active);
     if(cov_d0_theta_wrtBS.IsAvailable()) cov_d0_theta_wrtBS.SetActive(active);
     if(cov_d0_qoverp_wrtBS.IsAvailable()) cov_d0_qoverp_wrtBS.SetActive(active);
     if(cov_z0_phi_wrtBS.IsAvailable()) cov_z0_phi_wrtBS.SetActive(active);
     if(cov_z0_theta_wrtBS.IsAvailable()) cov_z0_theta_wrtBS.SetActive(active);
     if(cov_z0_qoverp_wrtBS.IsAvailable()) cov_z0_qoverp_wrtBS.SetActive(active);
     if(cov_phi_theta_wrtBS.IsAvailable()) cov_phi_theta_wrtBS.SetActive(active);
     if(cov_phi_qoverp_wrtBS.IsAvailable()) cov_phi_qoverp_wrtBS.SetActive(active);
     if(cov_theta_qoverp_wrtBS.IsAvailable()) cov_theta_qoverp_wrtBS.SetActive(active);
     if(d0_wrtBL.IsAvailable()) d0_wrtBL.SetActive(active);
     if(z0_wrtBL.IsAvailable()) z0_wrtBL.SetActive(active);
     if(phi_wrtBL.IsAvailable()) phi_wrtBL.SetActive(active);
     if(d0_err_wrtBL.IsAvailable()) d0_err_wrtBL.SetActive(active);
     if(z0_err_wrtBL.IsAvailable()) z0_err_wrtBL.SetActive(active);
     if(phi_err_wrtBL.IsAvailable()) phi_err_wrtBL.SetActive(active);
     if(theta_err_wrtBL.IsAvailable()) theta_err_wrtBL.SetActive(active);
     if(qoverp_err_wrtBL.IsAvailable()) qoverp_err_wrtBL.SetActive(active);
     if(d0_z0_err_wrtBL.IsAvailable()) d0_z0_err_wrtBL.SetActive(active);
     if(d0_phi_err_wrtBL.IsAvailable()) d0_phi_err_wrtBL.SetActive(active);
     if(d0_theta_err_wrtBL.IsAvailable()) d0_theta_err_wrtBL.SetActive(active);
     if(d0_qoverp_err_wrtBL.IsAvailable()) d0_qoverp_err_wrtBL.SetActive(active);
     if(z0_phi_err_wrtBL.IsAvailable()) z0_phi_err_wrtBL.SetActive(active);
     if(z0_theta_err_wrtBL.IsAvailable()) z0_theta_err_wrtBL.SetActive(active);
     if(z0_qoverp_err_wrtBL.IsAvailable()) z0_qoverp_err_wrtBL.SetActive(active);
     if(phi_theta_err_wrtBL.IsAvailable()) phi_theta_err_wrtBL.SetActive(active);
     if(phi_qoverp_err_wrtBL.IsAvailable()) phi_qoverp_err_wrtBL.SetActive(active);
     if(theta_qoverp_err_wrtBL.IsAvailable()) theta_qoverp_err_wrtBL.SetActive(active);
     if(chi2.IsAvailable()) chi2.SetActive(active);
     if(ndof.IsAvailable()) ndof.SetActive(active);
     if(nBLHits.IsAvailable()) nBLHits.SetActive(active);
     if(nPixHits.IsAvailable()) nPixHits.SetActive(active);
     if(nSCTHits.IsAvailable()) nSCTHits.SetActive(active);
     if(nTRTHits.IsAvailable()) nTRTHits.SetActive(active);
     if(nTRTHighTHits.IsAvailable()) nTRTHighTHits.SetActive(active);
     if(nPixHoles.IsAvailable()) nPixHoles.SetActive(active);
     if(nSCTHoles.IsAvailable()) nSCTHoles.SetActive(active);
     if(nTRTHoles.IsAvailable()) nTRTHoles.SetActive(active);
     if(nPixelDeadSensors.IsAvailable()) nPixelDeadSensors.SetActive(active);
     if(nSCTDeadSensors.IsAvailable()) nSCTDeadSensors.SetActive(active);
     if(nBLSharedHits.IsAvailable()) nBLSharedHits.SetActive(active);
     if(nPixSharedHits.IsAvailable()) nPixSharedHits.SetActive(active);
     if(nSCTSharedHits.IsAvailable()) nSCTSharedHits.SetActive(active);
     if(nBLayerSplitHits.IsAvailable()) nBLayerSplitHits.SetActive(active);
     if(nPixSplitHits.IsAvailable()) nPixSplitHits.SetActive(active);
     if(expectBLayerHit.IsAvailable()) expectBLayerHit.SetActive(active);
     if(nMDTHits.IsAvailable()) nMDTHits.SetActive(active);
     if(nCSCEtaHits.IsAvailable()) nCSCEtaHits.SetActive(active);
     if(nCSCPhiHits.IsAvailable()) nCSCPhiHits.SetActive(active);
     if(nRPCEtaHits.IsAvailable()) nRPCEtaHits.SetActive(active);
     if(nRPCPhiHits.IsAvailable()) nRPCPhiHits.SetActive(active);
     if(nTGCEtaHits.IsAvailable()) nTGCEtaHits.SetActive(active);
     if(nTGCPhiHits.IsAvailable()) nTGCPhiHits.SetActive(active);
     if(nHits.IsAvailable()) nHits.SetActive(active);
     if(nHoles.IsAvailable()) nHoles.SetActive(active);
     if(hitPattern.IsAvailable()) hitPattern.SetActive(active);
     if(TRTHighTHitsRatio.IsAvailable()) TRTHighTHitsRatio.SetActive(active);
     if(TRTHighTOutliersRatio.IsAvailable()) TRTHighTOutliersRatio.SetActive(active);
     if(fitter.IsAvailable()) fitter.SetActive(active);
     if(patternReco1.IsAvailable()) patternReco1.SetActive(active);
     if(patternReco2.IsAvailable()) patternReco2.SetActive(active);
     if(trackProperties.IsAvailable()) trackProperties.SetActive(active);
     if(particleHypothesis.IsAvailable()) particleHypothesis.SetActive(active);
     if(mc_probability.IsAvailable()) mc_probability.SetActive(active);
     if(mc_barcode.IsAvailable()) mc_barcode.SetActive(active);
     if(atTJVA_phi.IsAvailable()) atTJVA_phi.SetActive(active);
     if(atTJVA_d0.IsAvailable()) atTJVA_d0.SetActive(active);
     if(atTJVA_z0.IsAvailable()) atTJVA_z0.SetActive(active);
     if(atTJVA_theta.IsAvailable()) atTJVA_theta.SetActive(active);
     if(atTJVA_qoverp.IsAvailable()) atTJVA_qoverp.SetActive(active);
     if(atTJVA_n.IsAvailable()) atTJVA_n.SetActive(active);
    }
    else
    {
      MET_n.SetActive(active);
      MET_wpx.SetActive(active);
      MET_wpy.SetActive(active);
      MET_wet.SetActive(active);
      MET_statusWord.SetActive(active);
      MET_em_tightpp_n.SetActive(active);
      MET_em_tightpp_wpx.SetActive(active);
      MET_em_tightpp_wpy.SetActive(active);
      MET_em_tightpp_wet.SetActive(active);
      MET_em_tightpp_statusWord.SetActive(active);
      MET_STVF_n.SetActive(active);
      MET_STVF_wpx.SetActive(active);
      MET_STVF_wpy.SetActive(active);
      MET_STVF_wet.SetActive(active);
      MET_STVF_statusWord.SetActive(active);
      n.SetActive(active);
      d0.SetActive(active);
      z0.SetActive(active);
      phi.SetActive(active);
      theta.SetActive(active);
      qoverp.SetActive(active);
      pt.SetActive(active);
      eta.SetActive(active);
      d0_wrtPV.SetActive(active);
      z0_wrtPV.SetActive(active);
      phi_wrtPV.SetActive(active);
      d0_wrtBS.SetActive(active);
      z0_wrtBS.SetActive(active);
      phi_wrtBS.SetActive(active);
      err_d0_wrtBS.SetActive(active);
      err_z0_wrtBS.SetActive(active);
      err_phi_wrtBS.SetActive(active);
      err_theta_wrtBS.SetActive(active);
      err_qoverp_wrtBS.SetActive(active);
      cov_d0_z0_wrtBS.SetActive(active);
      cov_d0_phi_wrtBS.SetActive(active);
      cov_d0_theta_wrtBS.SetActive(active);
      cov_d0_qoverp_wrtBS.SetActive(active);
      cov_z0_phi_wrtBS.SetActive(active);
      cov_z0_theta_wrtBS.SetActive(active);
      cov_z0_qoverp_wrtBS.SetActive(active);
      cov_phi_theta_wrtBS.SetActive(active);
      cov_phi_qoverp_wrtBS.SetActive(active);
      cov_theta_qoverp_wrtBS.SetActive(active);
      d0_wrtBL.SetActive(active);
      z0_wrtBL.SetActive(active);
      phi_wrtBL.SetActive(active);
      d0_err_wrtBL.SetActive(active);
      z0_err_wrtBL.SetActive(active);
      phi_err_wrtBL.SetActive(active);
      theta_err_wrtBL.SetActive(active);
      qoverp_err_wrtBL.SetActive(active);
      d0_z0_err_wrtBL.SetActive(active);
      d0_phi_err_wrtBL.SetActive(active);
      d0_theta_err_wrtBL.SetActive(active);
      d0_qoverp_err_wrtBL.SetActive(active);
      z0_phi_err_wrtBL.SetActive(active);
      z0_theta_err_wrtBL.SetActive(active);
      z0_qoverp_err_wrtBL.SetActive(active);
      phi_theta_err_wrtBL.SetActive(active);
      phi_qoverp_err_wrtBL.SetActive(active);
      theta_qoverp_err_wrtBL.SetActive(active);
      chi2.SetActive(active);
      ndof.SetActive(active);
      nBLHits.SetActive(active);
      nPixHits.SetActive(active);
      nSCTHits.SetActive(active);
      nTRTHits.SetActive(active);
      nTRTHighTHits.SetActive(active);
      nPixHoles.SetActive(active);
      nSCTHoles.SetActive(active);
      nTRTHoles.SetActive(active);
      nPixelDeadSensors.SetActive(active);
      nSCTDeadSensors.SetActive(active);
      nBLSharedHits.SetActive(active);
      nPixSharedHits.SetActive(active);
      nSCTSharedHits.SetActive(active);
      nBLayerSplitHits.SetActive(active);
      nPixSplitHits.SetActive(active);
      expectBLayerHit.SetActive(active);
      nMDTHits.SetActive(active);
      nCSCEtaHits.SetActive(active);
      nCSCPhiHits.SetActive(active);
      nRPCEtaHits.SetActive(active);
      nRPCPhiHits.SetActive(active);
      nTGCEtaHits.SetActive(active);
      nTGCPhiHits.SetActive(active);
      nHits.SetActive(active);
      nHoles.SetActive(active);
      hitPattern.SetActive(active);
      TRTHighTHitsRatio.SetActive(active);
      TRTHighTOutliersRatio.SetActive(active);
      fitter.SetActive(active);
      patternReco1.SetActive(active);
      patternReco2.SetActive(active);
      trackProperties.SetActive(active);
      particleHypothesis.SetActive(active);
      mc_probability.SetActive(active);
      mc_barcode.SetActive(active);
      atTJVA_phi.SetActive(active);
      atTJVA_d0.SetActive(active);
      atTJVA_z0.SetActive(active);
      atTJVA_theta.SetActive(active);
      atTJVA_qoverp.SetActive(active);
      atTJVA_n.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TrackD3PDCollection::ReadAllActive()
  {
    if(MET_n.IsActive()) MET_n();
    if(MET_wpx.IsActive()) MET_wpx();
    if(MET_wpy.IsActive()) MET_wpy();
    if(MET_wet.IsActive()) MET_wet();
    if(MET_statusWord.IsActive()) MET_statusWord();
    if(MET_em_tightpp_n.IsActive()) MET_em_tightpp_n();
    if(MET_em_tightpp_wpx.IsActive()) MET_em_tightpp_wpx();
    if(MET_em_tightpp_wpy.IsActive()) MET_em_tightpp_wpy();
    if(MET_em_tightpp_wet.IsActive()) MET_em_tightpp_wet();
    if(MET_em_tightpp_statusWord.IsActive()) MET_em_tightpp_statusWord();
    if(MET_STVF_n.IsActive()) MET_STVF_n();
    if(MET_STVF_wpx.IsActive()) MET_STVF_wpx();
    if(MET_STVF_wpy.IsActive()) MET_STVF_wpy();
    if(MET_STVF_wet.IsActive()) MET_STVF_wet();
    if(MET_STVF_statusWord.IsActive()) MET_STVF_statusWord();
    if(n.IsActive()) n();
    if(d0.IsActive()) d0();
    if(z0.IsActive()) z0();
    if(phi.IsActive()) phi();
    if(theta.IsActive()) theta();
    if(qoverp.IsActive()) qoverp();
    if(pt.IsActive()) pt();
    if(eta.IsActive()) eta();
    if(d0_wrtPV.IsActive()) d0_wrtPV();
    if(z0_wrtPV.IsActive()) z0_wrtPV();
    if(phi_wrtPV.IsActive()) phi_wrtPV();
    if(d0_wrtBS.IsActive()) d0_wrtBS();
    if(z0_wrtBS.IsActive()) z0_wrtBS();
    if(phi_wrtBS.IsActive()) phi_wrtBS();
    if(err_d0_wrtBS.IsActive()) err_d0_wrtBS();
    if(err_z0_wrtBS.IsActive()) err_z0_wrtBS();
    if(err_phi_wrtBS.IsActive()) err_phi_wrtBS();
    if(err_theta_wrtBS.IsActive()) err_theta_wrtBS();
    if(err_qoverp_wrtBS.IsActive()) err_qoverp_wrtBS();
    if(cov_d0_z0_wrtBS.IsActive()) cov_d0_z0_wrtBS();
    if(cov_d0_phi_wrtBS.IsActive()) cov_d0_phi_wrtBS();
    if(cov_d0_theta_wrtBS.IsActive()) cov_d0_theta_wrtBS();
    if(cov_d0_qoverp_wrtBS.IsActive()) cov_d0_qoverp_wrtBS();
    if(cov_z0_phi_wrtBS.IsActive()) cov_z0_phi_wrtBS();
    if(cov_z0_theta_wrtBS.IsActive()) cov_z0_theta_wrtBS();
    if(cov_z0_qoverp_wrtBS.IsActive()) cov_z0_qoverp_wrtBS();
    if(cov_phi_theta_wrtBS.IsActive()) cov_phi_theta_wrtBS();
    if(cov_phi_qoverp_wrtBS.IsActive()) cov_phi_qoverp_wrtBS();
    if(cov_theta_qoverp_wrtBS.IsActive()) cov_theta_qoverp_wrtBS();
    if(d0_wrtBL.IsActive()) d0_wrtBL();
    if(z0_wrtBL.IsActive()) z0_wrtBL();
    if(phi_wrtBL.IsActive()) phi_wrtBL();
    if(d0_err_wrtBL.IsActive()) d0_err_wrtBL();
    if(z0_err_wrtBL.IsActive()) z0_err_wrtBL();
    if(phi_err_wrtBL.IsActive()) phi_err_wrtBL();
    if(theta_err_wrtBL.IsActive()) theta_err_wrtBL();
    if(qoverp_err_wrtBL.IsActive()) qoverp_err_wrtBL();
    if(d0_z0_err_wrtBL.IsActive()) d0_z0_err_wrtBL();
    if(d0_phi_err_wrtBL.IsActive()) d0_phi_err_wrtBL();
    if(d0_theta_err_wrtBL.IsActive()) d0_theta_err_wrtBL();
    if(d0_qoverp_err_wrtBL.IsActive()) d0_qoverp_err_wrtBL();
    if(z0_phi_err_wrtBL.IsActive()) z0_phi_err_wrtBL();
    if(z0_theta_err_wrtBL.IsActive()) z0_theta_err_wrtBL();
    if(z0_qoverp_err_wrtBL.IsActive()) z0_qoverp_err_wrtBL();
    if(phi_theta_err_wrtBL.IsActive()) phi_theta_err_wrtBL();
    if(phi_qoverp_err_wrtBL.IsActive()) phi_qoverp_err_wrtBL();
    if(theta_qoverp_err_wrtBL.IsActive()) theta_qoverp_err_wrtBL();
    if(chi2.IsActive()) chi2();
    if(ndof.IsActive()) ndof();
    if(nBLHits.IsActive()) nBLHits();
    if(nPixHits.IsActive()) nPixHits();
    if(nSCTHits.IsActive()) nSCTHits();
    if(nTRTHits.IsActive()) nTRTHits();
    if(nTRTHighTHits.IsActive()) nTRTHighTHits();
    if(nPixHoles.IsActive()) nPixHoles();
    if(nSCTHoles.IsActive()) nSCTHoles();
    if(nTRTHoles.IsActive()) nTRTHoles();
    if(nPixelDeadSensors.IsActive()) nPixelDeadSensors();
    if(nSCTDeadSensors.IsActive()) nSCTDeadSensors();
    if(nBLSharedHits.IsActive()) nBLSharedHits();
    if(nPixSharedHits.IsActive()) nPixSharedHits();
    if(nSCTSharedHits.IsActive()) nSCTSharedHits();
    if(nBLayerSplitHits.IsActive()) nBLayerSplitHits();
    if(nPixSplitHits.IsActive()) nPixSplitHits();
    if(expectBLayerHit.IsActive()) expectBLayerHit();
    if(nMDTHits.IsActive()) nMDTHits();
    if(nCSCEtaHits.IsActive()) nCSCEtaHits();
    if(nCSCPhiHits.IsActive()) nCSCPhiHits();
    if(nRPCEtaHits.IsActive()) nRPCEtaHits();
    if(nRPCPhiHits.IsActive()) nRPCPhiHits();
    if(nTGCEtaHits.IsActive()) nTGCEtaHits();
    if(nTGCPhiHits.IsActive()) nTGCPhiHits();
    if(nHits.IsActive()) nHits();
    if(nHoles.IsActive()) nHoles();
    if(hitPattern.IsActive()) hitPattern();
    if(TRTHighTHitsRatio.IsActive()) TRTHighTHitsRatio();
    if(TRTHighTOutliersRatio.IsActive()) TRTHighTOutliersRatio();
    if(fitter.IsActive()) fitter();
    if(patternReco1.IsActive()) patternReco1();
    if(patternReco2.IsActive()) patternReco2();
    if(trackProperties.IsActive()) trackProperties();
    if(particleHypothesis.IsActive()) particleHypothesis();
    if(mc_probability.IsActive()) mc_probability();
    if(mc_barcode.IsActive()) mc_barcode();
    if(atTJVA_phi.IsActive()) atTJVA_phi();
    if(atTJVA_d0.IsActive()) atTJVA_d0();
    if(atTJVA_z0.IsActive()) atTJVA_z0();
    if(atTJVA_theta.IsActive()) atTJVA_theta();
    if(atTJVA_qoverp.IsActive()) atTJVA_qoverp();
    if(atTJVA_n.IsActive()) atTJVA_n();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TrackD3PDCollection_CXX
