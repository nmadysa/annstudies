// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TrigL1TauD3PDCollection_CXX
#define D3PDREADER_TrigL1TauD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TrigL1TauD3PDCollection.h"

ClassImp(D3PDReader::TrigL1TauD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigL1TauD3PDCollection::TrigL1TauD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    n(prefix + "n",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    thrNames(prefix + "thrNames",&master),
    thrValues(prefix + "thrValues",&master),
    core(prefix + "core",&master),
    EMClus(prefix + "EMClus",&master),
    tauClus(prefix + "tauClus",&master),
    EMIsol(prefix + "EMIsol",&master),
    hadIsol(prefix + "hadIsol",&master),
    hadCore(prefix + "hadCore",&master),
    thrPattern(prefix + "thrPattern",&master),
    RoIWord(prefix + "RoIWord",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigL1TauD3PDCollection::TrigL1TauD3PDCollection(const std::string& prefix):
    TObject(),
    n(prefix + "n",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    thrNames(prefix + "thrNames",0),
    thrValues(prefix + "thrValues",0),
    core(prefix + "core",0),
    EMClus(prefix + "EMClus",0),
    tauClus(prefix + "tauClus",0),
    EMIsol(prefix + "EMIsol",0),
    hadIsol(prefix + "hadIsol",0),
    hadCore(prefix + "hadCore",0),
    thrPattern(prefix + "thrPattern",0),
    RoIWord(prefix + "RoIWord",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TrigL1TauD3PDCollection::ReadFrom(TTree* tree)
  {
    n.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    thrNames.ReadFrom(tree);
    thrValues.ReadFrom(tree);
    core.ReadFrom(tree);
    EMClus.ReadFrom(tree);
    tauClus.ReadFrom(tree);
    EMIsol.ReadFrom(tree);
    hadIsol.ReadFrom(tree);
    hadCore.ReadFrom(tree);
    thrPattern.ReadFrom(tree);
    RoIWord.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TrigL1TauD3PDCollection::WriteTo(TTree* tree)
  {
    n.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    thrNames.WriteTo(tree);
    thrValues.WriteTo(tree);
    core.WriteTo(tree);
    EMClus.WriteTo(tree);
    tauClus.WriteTo(tree);
    EMIsol.WriteTo(tree);
    hadIsol.WriteTo(tree);
    hadCore.WriteTo(tree);
    thrPattern.WriteTo(tree);
    RoIWord.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TrigL1TauD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(n.IsAvailable()) n.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(thrNames.IsAvailable()) thrNames.SetActive(active);
     if(thrValues.IsAvailable()) thrValues.SetActive(active);
     if(core.IsAvailable()) core.SetActive(active);
     if(EMClus.IsAvailable()) EMClus.SetActive(active);
     if(tauClus.IsAvailable()) tauClus.SetActive(active);
     if(EMIsol.IsAvailable()) EMIsol.SetActive(active);
     if(hadIsol.IsAvailable()) hadIsol.SetActive(active);
     if(hadCore.IsAvailable()) hadCore.SetActive(active);
     if(thrPattern.IsAvailable()) thrPattern.SetActive(active);
     if(RoIWord.IsAvailable()) RoIWord.SetActive(active);
    }
    else
    {
      n.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      thrNames.SetActive(active);
      thrValues.SetActive(active);
      core.SetActive(active);
      EMClus.SetActive(active);
      tauClus.SetActive(active);
      EMIsol.SetActive(active);
      hadIsol.SetActive(active);
      hadCore.SetActive(active);
      thrPattern.SetActive(active);
      RoIWord.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TrigL1TauD3PDCollection::ReadAllActive()
  {
    if(n.IsActive()) n();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(thrNames.IsActive()) thrNames();
    if(thrValues.IsActive()) thrValues();
    if(core.IsActive()) core();
    if(EMClus.IsActive()) EMClus();
    if(tauClus.IsActive()) tauClus();
    if(EMIsol.IsActive()) EMIsol();
    if(hadIsol.IsActive()) hadIsol();
    if(hadCore.IsActive()) hadCore();
    if(thrPattern.IsActive()) thrPattern();
    if(RoIWord.IsActive()) RoIWord();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TrigL1TauD3PDCollection_CXX
