// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_MET_CXX
#define D3PDREADER_MET_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "MET.h"

ClassImp(D3PDReader::MET)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  MET::MET(const long int& master,const std::string& prefix):
    TObject(),
    RefFinal_etx(prefix + "RefFinal_etx",&master),
    RefFinal_ety(prefix + "RefFinal_ety",&master),
    RefFinal_phi(prefix + "RefFinal_phi",&master),
    RefFinal_et(prefix + "RefFinal_et",&master),
    RefFinal_sumet(prefix + "RefFinal_sumet",&master),
    RefFinal_etx_CentralReg(prefix + "RefFinal_etx_CentralReg",&master),
    RefFinal_ety_CentralReg(prefix + "RefFinal_ety_CentralReg",&master),
    RefFinal_sumet_CentralReg(prefix + "RefFinal_sumet_CentralReg",&master),
    RefFinal_phi_CentralReg(prefix + "RefFinal_phi_CentralReg",&master),
    RefFinal_etx_EndcapRegion(prefix + "RefFinal_etx_EndcapRegion",&master),
    RefFinal_ety_EndcapRegion(prefix + "RefFinal_ety_EndcapRegion",&master),
    RefFinal_sumet_EndcapRegion(prefix + "RefFinal_sumet_EndcapRegion",&master),
    RefFinal_phi_EndcapRegion(prefix + "RefFinal_phi_EndcapRegion",&master),
    RefFinal_etx_ForwardReg(prefix + "RefFinal_etx_ForwardReg",&master),
    RefFinal_ety_ForwardReg(prefix + "RefFinal_ety_ForwardReg",&master),
    RefFinal_sumet_ForwardReg(prefix + "RefFinal_sumet_ForwardReg",&master),
    RefFinal_phi_ForwardReg(prefix + "RefFinal_phi_ForwardReg",&master),
    RefEle_etx(prefix + "RefEle_etx",&master),
    RefEle_ety(prefix + "RefEle_ety",&master),
    RefEle_phi(prefix + "RefEle_phi",&master),
    RefEle_et(prefix + "RefEle_et",&master),
    RefEle_sumet(prefix + "RefEle_sumet",&master),
    RefEle_etx_CentralReg(prefix + "RefEle_etx_CentralReg",&master),
    RefEle_ety_CentralReg(prefix + "RefEle_ety_CentralReg",&master),
    RefEle_sumet_CentralReg(prefix + "RefEle_sumet_CentralReg",&master),
    RefEle_phi_CentralReg(prefix + "RefEle_phi_CentralReg",&master),
    RefEle_etx_EndcapRegion(prefix + "RefEle_etx_EndcapRegion",&master),
    RefEle_ety_EndcapRegion(prefix + "RefEle_ety_EndcapRegion",&master),
    RefEle_sumet_EndcapRegion(prefix + "RefEle_sumet_EndcapRegion",&master),
    RefEle_phi_EndcapRegion(prefix + "RefEle_phi_EndcapRegion",&master),
    RefEle_etx_ForwardReg(prefix + "RefEle_etx_ForwardReg",&master),
    RefEle_ety_ForwardReg(prefix + "RefEle_ety_ForwardReg",&master),
    RefEle_sumet_ForwardReg(prefix + "RefEle_sumet_ForwardReg",&master),
    RefEle_phi_ForwardReg(prefix + "RefEle_phi_ForwardReg",&master),
    RefJet_etx(prefix + "RefJet_etx",&master),
    RefJet_ety(prefix + "RefJet_ety",&master),
    RefJet_phi(prefix + "RefJet_phi",&master),
    RefJet_et(prefix + "RefJet_et",&master),
    RefJet_sumet(prefix + "RefJet_sumet",&master),
    RefJet_etx_CentralReg(prefix + "RefJet_etx_CentralReg",&master),
    RefJet_ety_CentralReg(prefix + "RefJet_ety_CentralReg",&master),
    RefJet_sumet_CentralReg(prefix + "RefJet_sumet_CentralReg",&master),
    RefJet_phi_CentralReg(prefix + "RefJet_phi_CentralReg",&master),
    RefJet_etx_EndcapRegion(prefix + "RefJet_etx_EndcapRegion",&master),
    RefJet_ety_EndcapRegion(prefix + "RefJet_ety_EndcapRegion",&master),
    RefJet_sumet_EndcapRegion(prefix + "RefJet_sumet_EndcapRegion",&master),
    RefJet_phi_EndcapRegion(prefix + "RefJet_phi_EndcapRegion",&master),
    RefJet_etx_ForwardReg(prefix + "RefJet_etx_ForwardReg",&master),
    RefJet_ety_ForwardReg(prefix + "RefJet_ety_ForwardReg",&master),
    RefJet_sumet_ForwardReg(prefix + "RefJet_sumet_ForwardReg",&master),
    RefJet_phi_ForwardReg(prefix + "RefJet_phi_ForwardReg",&master),
    RefMuon_etx(prefix + "RefMuon_etx",&master),
    RefMuon_ety(prefix + "RefMuon_ety",&master),
    RefMuon_phi(prefix + "RefMuon_phi",&master),
    RefMuon_et(prefix + "RefMuon_et",&master),
    RefMuon_sumet(prefix + "RefMuon_sumet",&master),
    RefMuon_etx_CentralReg(prefix + "RefMuon_etx_CentralReg",&master),
    RefMuon_ety_CentralReg(prefix + "RefMuon_ety_CentralReg",&master),
    RefMuon_sumet_CentralReg(prefix + "RefMuon_sumet_CentralReg",&master),
    RefMuon_phi_CentralReg(prefix + "RefMuon_phi_CentralReg",&master),
    RefMuon_etx_EndcapRegion(prefix + "RefMuon_etx_EndcapRegion",&master),
    RefMuon_ety_EndcapRegion(prefix + "RefMuon_ety_EndcapRegion",&master),
    RefMuon_sumet_EndcapRegion(prefix + "RefMuon_sumet_EndcapRegion",&master),
    RefMuon_phi_EndcapRegion(prefix + "RefMuon_phi_EndcapRegion",&master),
    RefMuon_etx_ForwardReg(prefix + "RefMuon_etx_ForwardReg",&master),
    RefMuon_ety_ForwardReg(prefix + "RefMuon_ety_ForwardReg",&master),
    RefMuon_sumet_ForwardReg(prefix + "RefMuon_sumet_ForwardReg",&master),
    RefMuon_phi_ForwardReg(prefix + "RefMuon_phi_ForwardReg",&master),
    RefMuon_Staco_etx(prefix + "RefMuon_Staco_etx",&master),
    RefMuon_Staco_ety(prefix + "RefMuon_Staco_ety",&master),
    RefMuon_Staco_phi(prefix + "RefMuon_Staco_phi",&master),
    RefMuon_Staco_et(prefix + "RefMuon_Staco_et",&master),
    RefMuon_Staco_sumet(prefix + "RefMuon_Staco_sumet",&master),
    RefMuon_Staco_etx_CentralReg(prefix + "RefMuon_Staco_etx_CentralReg",&master),
    RefMuon_Staco_ety_CentralReg(prefix + "RefMuon_Staco_ety_CentralReg",&master),
    RefMuon_Staco_sumet_CentralReg(prefix + "RefMuon_Staco_sumet_CentralReg",&master),
    RefMuon_Staco_phi_CentralReg(prefix + "RefMuon_Staco_phi_CentralReg",&master),
    RefMuon_Staco_etx_EndcapRegion(prefix + "RefMuon_Staco_etx_EndcapRegion",&master),
    RefMuon_Staco_ety_EndcapRegion(prefix + "RefMuon_Staco_ety_EndcapRegion",&master),
    RefMuon_Staco_sumet_EndcapRegion(prefix + "RefMuon_Staco_sumet_EndcapRegion",&master),
    RefMuon_Staco_phi_EndcapRegion(prefix + "RefMuon_Staco_phi_EndcapRegion",&master),
    RefMuon_Staco_etx_ForwardReg(prefix + "RefMuon_Staco_etx_ForwardReg",&master),
    RefMuon_Staco_ety_ForwardReg(prefix + "RefMuon_Staco_ety_ForwardReg",&master),
    RefMuon_Staco_sumet_ForwardReg(prefix + "RefMuon_Staco_sumet_ForwardReg",&master),
    RefMuon_Staco_phi_ForwardReg(prefix + "RefMuon_Staco_phi_ForwardReg",&master),
    RefMuon_Muid_etx(prefix + "RefMuon_Muid_etx",&master),
    RefMuon_Muid_ety(prefix + "RefMuon_Muid_ety",&master),
    RefMuon_Muid_phi(prefix + "RefMuon_Muid_phi",&master),
    RefMuon_Muid_et(prefix + "RefMuon_Muid_et",&master),
    RefMuon_Muid_sumet(prefix + "RefMuon_Muid_sumet",&master),
    RefMuon_Muid_etx_CentralReg(prefix + "RefMuon_Muid_etx_CentralReg",&master),
    RefMuon_Muid_ety_CentralReg(prefix + "RefMuon_Muid_ety_CentralReg",&master),
    RefMuon_Muid_sumet_CentralReg(prefix + "RefMuon_Muid_sumet_CentralReg",&master),
    RefMuon_Muid_phi_CentralReg(prefix + "RefMuon_Muid_phi_CentralReg",&master),
    RefMuon_Muid_etx_EndcapRegion(prefix + "RefMuon_Muid_etx_EndcapRegion",&master),
    RefMuon_Muid_ety_EndcapRegion(prefix + "RefMuon_Muid_ety_EndcapRegion",&master),
    RefMuon_Muid_sumet_EndcapRegion(prefix + "RefMuon_Muid_sumet_EndcapRegion",&master),
    RefMuon_Muid_phi_EndcapRegion(prefix + "RefMuon_Muid_phi_EndcapRegion",&master),
    RefMuon_Muid_etx_ForwardReg(prefix + "RefMuon_Muid_etx_ForwardReg",&master),
    RefMuon_Muid_ety_ForwardReg(prefix + "RefMuon_Muid_ety_ForwardReg",&master),
    RefMuon_Muid_sumet_ForwardReg(prefix + "RefMuon_Muid_sumet_ForwardReg",&master),
    RefMuon_Muid_phi_ForwardReg(prefix + "RefMuon_Muid_phi_ForwardReg",&master),
    RefMuons_etx(prefix + "RefMuons_etx",&master),
    RefMuons_ety(prefix + "RefMuons_ety",&master),
    RefMuons_phi(prefix + "RefMuons_phi",&master),
    RefMuons_et(prefix + "RefMuons_et",&master),
    RefMuons_sumet(prefix + "RefMuons_sumet",&master),
    RefMuons_etx_CentralReg(prefix + "RefMuons_etx_CentralReg",&master),
    RefMuons_ety_CentralReg(prefix + "RefMuons_ety_CentralReg",&master),
    RefMuons_sumet_CentralReg(prefix + "RefMuons_sumet_CentralReg",&master),
    RefMuons_phi_CentralReg(prefix + "RefMuons_phi_CentralReg",&master),
    RefMuons_etx_EndcapRegion(prefix + "RefMuons_etx_EndcapRegion",&master),
    RefMuons_ety_EndcapRegion(prefix + "RefMuons_ety_EndcapRegion",&master),
    RefMuons_sumet_EndcapRegion(prefix + "RefMuons_sumet_EndcapRegion",&master),
    RefMuons_phi_EndcapRegion(prefix + "RefMuons_phi_EndcapRegion",&master),
    RefMuons_etx_ForwardReg(prefix + "RefMuons_etx_ForwardReg",&master),
    RefMuons_ety_ForwardReg(prefix + "RefMuons_ety_ForwardReg",&master),
    RefMuons_sumet_ForwardReg(prefix + "RefMuons_sumet_ForwardReg",&master),
    RefMuons_phi_ForwardReg(prefix + "RefMuons_phi_ForwardReg",&master),
    RefGamma_etx(prefix + "RefGamma_etx",&master),
    RefGamma_ety(prefix + "RefGamma_ety",&master),
    RefGamma_phi(prefix + "RefGamma_phi",&master),
    RefGamma_et(prefix + "RefGamma_et",&master),
    RefGamma_sumet(prefix + "RefGamma_sumet",&master),
    RefGamma_etx_CentralReg(prefix + "RefGamma_etx_CentralReg",&master),
    RefGamma_ety_CentralReg(prefix + "RefGamma_ety_CentralReg",&master),
    RefGamma_sumet_CentralReg(prefix + "RefGamma_sumet_CentralReg",&master),
    RefGamma_phi_CentralReg(prefix + "RefGamma_phi_CentralReg",&master),
    RefGamma_etx_EndcapRegion(prefix + "RefGamma_etx_EndcapRegion",&master),
    RefGamma_ety_EndcapRegion(prefix + "RefGamma_ety_EndcapRegion",&master),
    RefGamma_sumet_EndcapRegion(prefix + "RefGamma_sumet_EndcapRegion",&master),
    RefGamma_phi_EndcapRegion(prefix + "RefGamma_phi_EndcapRegion",&master),
    RefGamma_etx_ForwardReg(prefix + "RefGamma_etx_ForwardReg",&master),
    RefGamma_ety_ForwardReg(prefix + "RefGamma_ety_ForwardReg",&master),
    RefGamma_sumet_ForwardReg(prefix + "RefGamma_sumet_ForwardReg",&master),
    RefGamma_phi_ForwardReg(prefix + "RefGamma_phi_ForwardReg",&master),
    RefTau_etx(prefix + "RefTau_etx",&master),
    RefTau_ety(prefix + "RefTau_ety",&master),
    RefTau_phi(prefix + "RefTau_phi",&master),
    RefTau_et(prefix + "RefTau_et",&master),
    RefTau_sumet(prefix + "RefTau_sumet",&master),
    RefTau_etx_CentralReg(prefix + "RefTau_etx_CentralReg",&master),
    RefTau_ety_CentralReg(prefix + "RefTau_ety_CentralReg",&master),
    RefTau_sumet_CentralReg(prefix + "RefTau_sumet_CentralReg",&master),
    RefTau_phi_CentralReg(prefix + "RefTau_phi_CentralReg",&master),
    RefTau_etx_EndcapRegion(prefix + "RefTau_etx_EndcapRegion",&master),
    RefTau_ety_EndcapRegion(prefix + "RefTau_ety_EndcapRegion",&master),
    RefTau_sumet_EndcapRegion(prefix + "RefTau_sumet_EndcapRegion",&master),
    RefTau_phi_EndcapRegion(prefix + "RefTau_phi_EndcapRegion",&master),
    RefTau_etx_ForwardReg(prefix + "RefTau_etx_ForwardReg",&master),
    RefTau_ety_ForwardReg(prefix + "RefTau_ety_ForwardReg",&master),
    RefTau_sumet_ForwardReg(prefix + "RefTau_sumet_ForwardReg",&master),
    RefTau_phi_ForwardReg(prefix + "RefTau_phi_ForwardReg",&master),
    RefFinal_em_etx(prefix + "RefFinal_em_etx",&master),
    RefFinal_em_ety(prefix + "RefFinal_em_ety",&master),
    RefFinal_em_phi(prefix + "RefFinal_em_phi",&master),
    RefFinal_em_et(prefix + "RefFinal_em_et",&master),
    RefFinal_em_sumet(prefix + "RefFinal_em_sumet",&master),
    RefFinal_em_etx_CentralReg(prefix + "RefFinal_em_etx_CentralReg",&master),
    RefFinal_em_ety_CentralReg(prefix + "RefFinal_em_ety_CentralReg",&master),
    RefFinal_em_sumet_CentralReg(prefix + "RefFinal_em_sumet_CentralReg",&master),
    RefFinal_em_phi_CentralReg(prefix + "RefFinal_em_phi_CentralReg",&master),
    RefFinal_em_etx_EndcapRegion(prefix + "RefFinal_em_etx_EndcapRegion",&master),
    RefFinal_em_ety_EndcapRegion(prefix + "RefFinal_em_ety_EndcapRegion",&master),
    RefFinal_em_sumet_EndcapRegion(prefix + "RefFinal_em_sumet_EndcapRegion",&master),
    RefFinal_em_phi_EndcapRegion(prefix + "RefFinal_em_phi_EndcapRegion",&master),
    RefFinal_em_etx_ForwardReg(prefix + "RefFinal_em_etx_ForwardReg",&master),
    RefFinal_em_ety_ForwardReg(prefix + "RefFinal_em_ety_ForwardReg",&master),
    RefFinal_em_sumet_ForwardReg(prefix + "RefFinal_em_sumet_ForwardReg",&master),
    RefFinal_em_phi_ForwardReg(prefix + "RefFinal_em_phi_ForwardReg",&master),
    RefEle_em_etx(prefix + "RefEle_em_etx",&master),
    RefEle_em_ety(prefix + "RefEle_em_ety",&master),
    RefEle_em_phi(prefix + "RefEle_em_phi",&master),
    RefEle_em_et(prefix + "RefEle_em_et",&master),
    RefEle_em_sumet(prefix + "RefEle_em_sumet",&master),
    RefEle_em_etx_CentralReg(prefix + "RefEle_em_etx_CentralReg",&master),
    RefEle_em_ety_CentralReg(prefix + "RefEle_em_ety_CentralReg",&master),
    RefEle_em_sumet_CentralReg(prefix + "RefEle_em_sumet_CentralReg",&master),
    RefEle_em_phi_CentralReg(prefix + "RefEle_em_phi_CentralReg",&master),
    RefEle_em_etx_EndcapRegion(prefix + "RefEle_em_etx_EndcapRegion",&master),
    RefEle_em_ety_EndcapRegion(prefix + "RefEle_em_ety_EndcapRegion",&master),
    RefEle_em_sumet_EndcapRegion(prefix + "RefEle_em_sumet_EndcapRegion",&master),
    RefEle_em_phi_EndcapRegion(prefix + "RefEle_em_phi_EndcapRegion",&master),
    RefEle_em_etx_ForwardReg(prefix + "RefEle_em_etx_ForwardReg",&master),
    RefEle_em_ety_ForwardReg(prefix + "RefEle_em_ety_ForwardReg",&master),
    RefEle_em_sumet_ForwardReg(prefix + "RefEle_em_sumet_ForwardReg",&master),
    RefEle_em_phi_ForwardReg(prefix + "RefEle_em_phi_ForwardReg",&master),
    RefJet_em_etx(prefix + "RefJet_em_etx",&master),
    RefJet_em_ety(prefix + "RefJet_em_ety",&master),
    RefJet_em_phi(prefix + "RefJet_em_phi",&master),
    RefJet_em_et(prefix + "RefJet_em_et",&master),
    RefJet_em_sumet(prefix + "RefJet_em_sumet",&master),
    RefJet_em_etx_CentralReg(prefix + "RefJet_em_etx_CentralReg",&master),
    RefJet_em_ety_CentralReg(prefix + "RefJet_em_ety_CentralReg",&master),
    RefJet_em_sumet_CentralReg(prefix + "RefJet_em_sumet_CentralReg",&master),
    RefJet_em_phi_CentralReg(prefix + "RefJet_em_phi_CentralReg",&master),
    RefJet_em_etx_EndcapRegion(prefix + "RefJet_em_etx_EndcapRegion",&master),
    RefJet_em_ety_EndcapRegion(prefix + "RefJet_em_ety_EndcapRegion",&master),
    RefJet_em_sumet_EndcapRegion(prefix + "RefJet_em_sumet_EndcapRegion",&master),
    RefJet_em_phi_EndcapRegion(prefix + "RefJet_em_phi_EndcapRegion",&master),
    RefJet_em_etx_ForwardReg(prefix + "RefJet_em_etx_ForwardReg",&master),
    RefJet_em_ety_ForwardReg(prefix + "RefJet_em_ety_ForwardReg",&master),
    RefJet_em_sumet_ForwardReg(prefix + "RefJet_em_sumet_ForwardReg",&master),
    RefJet_em_phi_ForwardReg(prefix + "RefJet_em_phi_ForwardReg",&master),
    RefMuon_em_etx(prefix + "RefMuon_em_etx",&master),
    RefMuon_em_ety(prefix + "RefMuon_em_ety",&master),
    RefMuon_em_phi(prefix + "RefMuon_em_phi",&master),
    RefMuon_em_et(prefix + "RefMuon_em_et",&master),
    RefMuon_em_sumet(prefix + "RefMuon_em_sumet",&master),
    RefMuon_em_etx_CentralReg(prefix + "RefMuon_em_etx_CentralReg",&master),
    RefMuon_em_ety_CentralReg(prefix + "RefMuon_em_ety_CentralReg",&master),
    RefMuon_em_sumet_CentralReg(prefix + "RefMuon_em_sumet_CentralReg",&master),
    RefMuon_em_phi_CentralReg(prefix + "RefMuon_em_phi_CentralReg",&master),
    RefMuon_em_etx_EndcapRegion(prefix + "RefMuon_em_etx_EndcapRegion",&master),
    RefMuon_em_ety_EndcapRegion(prefix + "RefMuon_em_ety_EndcapRegion",&master),
    RefMuon_em_sumet_EndcapRegion(prefix + "RefMuon_em_sumet_EndcapRegion",&master),
    RefMuon_em_phi_EndcapRegion(prefix + "RefMuon_em_phi_EndcapRegion",&master),
    RefMuon_em_etx_ForwardReg(prefix + "RefMuon_em_etx_ForwardReg",&master),
    RefMuon_em_ety_ForwardReg(prefix + "RefMuon_em_ety_ForwardReg",&master),
    RefMuon_em_sumet_ForwardReg(prefix + "RefMuon_em_sumet_ForwardReg",&master),
    RefMuon_em_phi_ForwardReg(prefix + "RefMuon_em_phi_ForwardReg",&master),
    RefMuon_Track_em_etx(prefix + "RefMuon_Track_em_etx",&master),
    RefMuon_Track_em_ety(prefix + "RefMuon_Track_em_ety",&master),
    RefMuon_Track_em_phi(prefix + "RefMuon_Track_em_phi",&master),
    RefMuon_Track_em_et(prefix + "RefMuon_Track_em_et",&master),
    RefMuon_Track_em_sumet(prefix + "RefMuon_Track_em_sumet",&master),
    RefMuon_Track_em_etx_CentralReg(prefix + "RefMuon_Track_em_etx_CentralReg",&master),
    RefMuon_Track_em_ety_CentralReg(prefix + "RefMuon_Track_em_ety_CentralReg",&master),
    RefMuon_Track_em_sumet_CentralReg(prefix + "RefMuon_Track_em_sumet_CentralReg",&master),
    RefMuon_Track_em_phi_CentralReg(prefix + "RefMuon_Track_em_phi_CentralReg",&master),
    RefMuon_Track_em_etx_EndcapRegion(prefix + "RefMuon_Track_em_etx_EndcapRegion",&master),
    RefMuon_Track_em_ety_EndcapRegion(prefix + "RefMuon_Track_em_ety_EndcapRegion",&master),
    RefMuon_Track_em_sumet_EndcapRegion(prefix + "RefMuon_Track_em_sumet_EndcapRegion",&master),
    RefMuon_Track_em_phi_EndcapRegion(prefix + "RefMuon_Track_em_phi_EndcapRegion",&master),
    RefMuon_Track_em_etx_ForwardReg(prefix + "RefMuon_Track_em_etx_ForwardReg",&master),
    RefMuon_Track_em_ety_ForwardReg(prefix + "RefMuon_Track_em_ety_ForwardReg",&master),
    RefMuon_Track_em_sumet_ForwardReg(prefix + "RefMuon_Track_em_sumet_ForwardReg",&master),
    RefMuon_Track_em_phi_ForwardReg(prefix + "RefMuon_Track_em_phi_ForwardReg",&master),
    RefGamma_em_etx(prefix + "RefGamma_em_etx",&master),
    RefGamma_em_ety(prefix + "RefGamma_em_ety",&master),
    RefGamma_em_phi(prefix + "RefGamma_em_phi",&master),
    RefGamma_em_et(prefix + "RefGamma_em_et",&master),
    RefGamma_em_sumet(prefix + "RefGamma_em_sumet",&master),
    RefGamma_em_etx_CentralReg(prefix + "RefGamma_em_etx_CentralReg",&master),
    RefGamma_em_ety_CentralReg(prefix + "RefGamma_em_ety_CentralReg",&master),
    RefGamma_em_sumet_CentralReg(prefix + "RefGamma_em_sumet_CentralReg",&master),
    RefGamma_em_phi_CentralReg(prefix + "RefGamma_em_phi_CentralReg",&master),
    RefGamma_em_etx_EndcapRegion(prefix + "RefGamma_em_etx_EndcapRegion",&master),
    RefGamma_em_ety_EndcapRegion(prefix + "RefGamma_em_ety_EndcapRegion",&master),
    RefGamma_em_sumet_EndcapRegion(prefix + "RefGamma_em_sumet_EndcapRegion",&master),
    RefGamma_em_phi_EndcapRegion(prefix + "RefGamma_em_phi_EndcapRegion",&master),
    RefGamma_em_etx_ForwardReg(prefix + "RefGamma_em_etx_ForwardReg",&master),
    RefGamma_em_ety_ForwardReg(prefix + "RefGamma_em_ety_ForwardReg",&master),
    RefGamma_em_sumet_ForwardReg(prefix + "RefGamma_em_sumet_ForwardReg",&master),
    RefGamma_em_phi_ForwardReg(prefix + "RefGamma_em_phi_ForwardReg",&master),
    RefTau_em_etx(prefix + "RefTau_em_etx",&master),
    RefTau_em_ety(prefix + "RefTau_em_ety",&master),
    RefTau_em_phi(prefix + "RefTau_em_phi",&master),
    RefTau_em_et(prefix + "RefTau_em_et",&master),
    RefTau_em_sumet(prefix + "RefTau_em_sumet",&master),
    RefTau_em_etx_CentralReg(prefix + "RefTau_em_etx_CentralReg",&master),
    RefTau_em_ety_CentralReg(prefix + "RefTau_em_ety_CentralReg",&master),
    RefTau_em_sumet_CentralReg(prefix + "RefTau_em_sumet_CentralReg",&master),
    RefTau_em_phi_CentralReg(prefix + "RefTau_em_phi_CentralReg",&master),
    RefTau_em_etx_EndcapRegion(prefix + "RefTau_em_etx_EndcapRegion",&master),
    RefTau_em_ety_EndcapRegion(prefix + "RefTau_em_ety_EndcapRegion",&master),
    RefTau_em_sumet_EndcapRegion(prefix + "RefTau_em_sumet_EndcapRegion",&master),
    RefTau_em_phi_EndcapRegion(prefix + "RefTau_em_phi_EndcapRegion",&master),
    RefTau_em_etx_ForwardReg(prefix + "RefTau_em_etx_ForwardReg",&master),
    RefTau_em_ety_ForwardReg(prefix + "RefTau_em_ety_ForwardReg",&master),
    RefTau_em_sumet_ForwardReg(prefix + "RefTau_em_sumet_ForwardReg",&master),
    RefTau_em_phi_ForwardReg(prefix + "RefTau_em_phi_ForwardReg",&master),
    RefJet_JVF_etx(prefix + "RefJet_JVF_etx",&master),
    RefJet_JVF_ety(prefix + "RefJet_JVF_ety",&master),
    RefJet_JVF_phi(prefix + "RefJet_JVF_phi",&master),
    RefJet_JVF_et(prefix + "RefJet_JVF_et",&master),
    RefJet_JVF_sumet(prefix + "RefJet_JVF_sumet",&master),
    RefJet_JVF_etx_CentralReg(prefix + "RefJet_JVF_etx_CentralReg",&master),
    RefJet_JVF_ety_CentralReg(prefix + "RefJet_JVF_ety_CentralReg",&master),
    RefJet_JVF_sumet_CentralReg(prefix + "RefJet_JVF_sumet_CentralReg",&master),
    RefJet_JVF_phi_CentralReg(prefix + "RefJet_JVF_phi_CentralReg",&master),
    RefJet_JVF_etx_EndcapRegion(prefix + "RefJet_JVF_etx_EndcapRegion",&master),
    RefJet_JVF_ety_EndcapRegion(prefix + "RefJet_JVF_ety_EndcapRegion",&master),
    RefJet_JVF_sumet_EndcapRegion(prefix + "RefJet_JVF_sumet_EndcapRegion",&master),
    RefJet_JVF_phi_EndcapRegion(prefix + "RefJet_JVF_phi_EndcapRegion",&master),
    RefJet_JVF_etx_ForwardReg(prefix + "RefJet_JVF_etx_ForwardReg",&master),
    RefJet_JVF_ety_ForwardReg(prefix + "RefJet_JVF_ety_ForwardReg",&master),
    RefJet_JVF_sumet_ForwardReg(prefix + "RefJet_JVF_sumet_ForwardReg",&master),
    RefJet_JVF_phi_ForwardReg(prefix + "RefJet_JVF_phi_ForwardReg",&master),
    RefJet_JVFCut_etx(prefix + "RefJet_JVFCut_etx",&master),
    RefJet_JVFCut_ety(prefix + "RefJet_JVFCut_ety",&master),
    RefJet_JVFCut_phi(prefix + "RefJet_JVFCut_phi",&master),
    RefJet_JVFCut_et(prefix + "RefJet_JVFCut_et",&master),
    RefJet_JVFCut_sumet(prefix + "RefJet_JVFCut_sumet",&master),
    RefJet_JVFCut_etx_CentralReg(prefix + "RefJet_JVFCut_etx_CentralReg",&master),
    RefJet_JVFCut_ety_CentralReg(prefix + "RefJet_JVFCut_ety_CentralReg",&master),
    RefJet_JVFCut_sumet_CentralReg(prefix + "RefJet_JVFCut_sumet_CentralReg",&master),
    RefJet_JVFCut_phi_CentralReg(prefix + "RefJet_JVFCut_phi_CentralReg",&master),
    RefJet_JVFCut_etx_EndcapRegion(prefix + "RefJet_JVFCut_etx_EndcapRegion",&master),
    RefJet_JVFCut_ety_EndcapRegion(prefix + "RefJet_JVFCut_ety_EndcapRegion",&master),
    RefJet_JVFCut_sumet_EndcapRegion(prefix + "RefJet_JVFCut_sumet_EndcapRegion",&master),
    RefJet_JVFCut_phi_EndcapRegion(prefix + "RefJet_JVFCut_phi_EndcapRegion",&master),
    RefJet_JVFCut_etx_ForwardReg(prefix + "RefJet_JVFCut_etx_ForwardReg",&master),
    RefJet_JVFCut_ety_ForwardReg(prefix + "RefJet_JVFCut_ety_ForwardReg",&master),
    RefJet_JVFCut_sumet_ForwardReg(prefix + "RefJet_JVFCut_sumet_ForwardReg",&master),
    RefJet_JVFCut_phi_ForwardReg(prefix + "RefJet_JVFCut_phi_ForwardReg",&master),
    CellOut_Eflow_STVF_etx(prefix + "CellOut_Eflow_STVF_etx",&master),
    CellOut_Eflow_STVF_ety(prefix + "CellOut_Eflow_STVF_ety",&master),
    CellOut_Eflow_STVF_phi(prefix + "CellOut_Eflow_STVF_phi",&master),
    CellOut_Eflow_STVF_et(prefix + "CellOut_Eflow_STVF_et",&master),
    CellOut_Eflow_STVF_sumet(prefix + "CellOut_Eflow_STVF_sumet",&master),
    CellOut_Eflow_STVF_etx_CentralReg(prefix + "CellOut_Eflow_STVF_etx_CentralReg",&master),
    CellOut_Eflow_STVF_ety_CentralReg(prefix + "CellOut_Eflow_STVF_ety_CentralReg",&master),
    CellOut_Eflow_STVF_sumet_CentralReg(prefix + "CellOut_Eflow_STVF_sumet_CentralReg",&master),
    CellOut_Eflow_STVF_phi_CentralReg(prefix + "CellOut_Eflow_STVF_phi_CentralReg",&master),
    CellOut_Eflow_STVF_etx_EndcapRegion(prefix + "CellOut_Eflow_STVF_etx_EndcapRegion",&master),
    CellOut_Eflow_STVF_ety_EndcapRegion(prefix + "CellOut_Eflow_STVF_ety_EndcapRegion",&master),
    CellOut_Eflow_STVF_sumet_EndcapRegion(prefix + "CellOut_Eflow_STVF_sumet_EndcapRegion",&master),
    CellOut_Eflow_STVF_phi_EndcapRegion(prefix + "CellOut_Eflow_STVF_phi_EndcapRegion",&master),
    CellOut_Eflow_STVF_etx_ForwardReg(prefix + "CellOut_Eflow_STVF_etx_ForwardReg",&master),
    CellOut_Eflow_STVF_ety_ForwardReg(prefix + "CellOut_Eflow_STVF_ety_ForwardReg",&master),
    CellOut_Eflow_STVF_sumet_ForwardReg(prefix + "CellOut_Eflow_STVF_sumet_ForwardReg",&master),
    CellOut_Eflow_STVF_phi_ForwardReg(prefix + "CellOut_Eflow_STVF_phi_ForwardReg",&master),
    CellOut_Eflow_JetArea_etx(prefix + "CellOut_Eflow_JetArea_etx",&master),
    CellOut_Eflow_JetArea_ety(prefix + "CellOut_Eflow_JetArea_ety",&master),
    CellOut_Eflow_JetArea_phi(prefix + "CellOut_Eflow_JetArea_phi",&master),
    CellOut_Eflow_JetArea_et(prefix + "CellOut_Eflow_JetArea_et",&master),
    CellOut_Eflow_JetArea_sumet(prefix + "CellOut_Eflow_JetArea_sumet",&master),
    CellOut_Eflow_JetArea_etx_CentralReg(prefix + "CellOut_Eflow_JetArea_etx_CentralReg",&master),
    CellOut_Eflow_JetArea_ety_CentralReg(prefix + "CellOut_Eflow_JetArea_ety_CentralReg",&master),
    CellOut_Eflow_JetArea_sumet_CentralReg(prefix + "CellOut_Eflow_JetArea_sumet_CentralReg",&master),
    CellOut_Eflow_JetArea_phi_CentralReg(prefix + "CellOut_Eflow_JetArea_phi_CentralReg",&master),
    CellOut_Eflow_JetArea_etx_EndcapRegion(prefix + "CellOut_Eflow_JetArea_etx_EndcapRegion",&master),
    CellOut_Eflow_JetArea_ety_EndcapRegion(prefix + "CellOut_Eflow_JetArea_ety_EndcapRegion",&master),
    CellOut_Eflow_JetArea_sumet_EndcapRegion(prefix + "CellOut_Eflow_JetArea_sumet_EndcapRegion",&master),
    CellOut_Eflow_JetArea_phi_EndcapRegion(prefix + "CellOut_Eflow_JetArea_phi_EndcapRegion",&master),
    CellOut_Eflow_JetArea_etx_ForwardReg(prefix + "CellOut_Eflow_JetArea_etx_ForwardReg",&master),
    CellOut_Eflow_JetArea_ety_ForwardReg(prefix + "CellOut_Eflow_JetArea_ety_ForwardReg",&master),
    CellOut_Eflow_JetArea_sumet_ForwardReg(prefix + "CellOut_Eflow_JetArea_sumet_ForwardReg",&master),
    CellOut_Eflow_JetArea_phi_ForwardReg(prefix + "CellOut_Eflow_JetArea_phi_ForwardReg",&master),
    CellOut_Eflow_JetAreaJVF_etx(prefix + "CellOut_Eflow_JetAreaJVF_etx",&master),
    CellOut_Eflow_JetAreaJVF_ety(prefix + "CellOut_Eflow_JetAreaJVF_ety",&master),
    CellOut_Eflow_JetAreaJVF_phi(prefix + "CellOut_Eflow_JetAreaJVF_phi",&master),
    CellOut_Eflow_JetAreaJVF_et(prefix + "CellOut_Eflow_JetAreaJVF_et",&master),
    CellOut_Eflow_JetAreaJVF_sumet(prefix + "CellOut_Eflow_JetAreaJVF_sumet",&master),
    CellOut_Eflow_JetAreaJVF_etx_CentralReg(prefix + "CellOut_Eflow_JetAreaJVF_etx_CentralReg",&master),
    CellOut_Eflow_JetAreaJVF_ety_CentralReg(prefix + "CellOut_Eflow_JetAreaJVF_ety_CentralReg",&master),
    CellOut_Eflow_JetAreaJVF_sumet_CentralReg(prefix + "CellOut_Eflow_JetAreaJVF_sumet_CentralReg",&master),
    CellOut_Eflow_JetAreaJVF_phi_CentralReg(prefix + "CellOut_Eflow_JetAreaJVF_phi_CentralReg",&master),
    CellOut_Eflow_JetAreaJVF_etx_EndcapRegion(prefix + "CellOut_Eflow_JetAreaJVF_etx_EndcapRegion",&master),
    CellOut_Eflow_JetAreaJVF_ety_EndcapRegion(prefix + "CellOut_Eflow_JetAreaJVF_ety_EndcapRegion",&master),
    CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion(prefix + "CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion",&master),
    CellOut_Eflow_JetAreaJVF_phi_EndcapRegion(prefix + "CellOut_Eflow_JetAreaJVF_phi_EndcapRegion",&master),
    CellOut_Eflow_JetAreaJVF_etx_ForwardReg(prefix + "CellOut_Eflow_JetAreaJVF_etx_ForwardReg",&master),
    CellOut_Eflow_JetAreaJVF_ety_ForwardReg(prefix + "CellOut_Eflow_JetAreaJVF_ety_ForwardReg",&master),
    CellOut_Eflow_JetAreaJVF_sumet_ForwardReg(prefix + "CellOut_Eflow_JetAreaJVF_sumet_ForwardReg",&master),
    CellOut_Eflow_JetAreaJVF_phi_ForwardReg(prefix + "CellOut_Eflow_JetAreaJVF_phi_ForwardReg",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_etx(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_etx",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_ety(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_ety",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_phi(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_phi",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_et(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_et",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_sumet",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg",&master),
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg",&master),
    RefFinal_STVF_etx(prefix + "RefFinal_STVF_etx",&master),
    RefFinal_STVF_ety(prefix + "RefFinal_STVF_ety",&master),
    RefFinal_STVF_phi(prefix + "RefFinal_STVF_phi",&master),
    RefFinal_STVF_et(prefix + "RefFinal_STVF_et",&master),
    RefFinal_STVF_sumet(prefix + "RefFinal_STVF_sumet",&master),
    RefFinal_STVF_etx_CentralReg(prefix + "RefFinal_STVF_etx_CentralReg",&master),
    RefFinal_STVF_ety_CentralReg(prefix + "RefFinal_STVF_ety_CentralReg",&master),
    RefFinal_STVF_sumet_CentralReg(prefix + "RefFinal_STVF_sumet_CentralReg",&master),
    RefFinal_STVF_phi_CentralReg(prefix + "RefFinal_STVF_phi_CentralReg",&master),
    RefFinal_STVF_etx_EndcapRegion(prefix + "RefFinal_STVF_etx_EndcapRegion",&master),
    RefFinal_STVF_ety_EndcapRegion(prefix + "RefFinal_STVF_ety_EndcapRegion",&master),
    RefFinal_STVF_sumet_EndcapRegion(prefix + "RefFinal_STVF_sumet_EndcapRegion",&master),
    RefFinal_STVF_phi_EndcapRegion(prefix + "RefFinal_STVF_phi_EndcapRegion",&master),
    RefFinal_STVF_etx_ForwardReg(prefix + "RefFinal_STVF_etx_ForwardReg",&master),
    RefFinal_STVF_ety_ForwardReg(prefix + "RefFinal_STVF_ety_ForwardReg",&master),
    RefFinal_STVF_sumet_ForwardReg(prefix + "RefFinal_STVF_sumet_ForwardReg",&master),
    RefFinal_STVF_phi_ForwardReg(prefix + "RefFinal_STVF_phi_ForwardReg",&master),
    CellOut_Eflow_etx(prefix + "CellOut_Eflow_etx",&master),
    CellOut_Eflow_ety(prefix + "CellOut_Eflow_ety",&master),
    CellOut_Eflow_phi(prefix + "CellOut_Eflow_phi",&master),
    CellOut_Eflow_et(prefix + "CellOut_Eflow_et",&master),
    CellOut_Eflow_sumet(prefix + "CellOut_Eflow_sumet",&master),
    CellOut_Eflow_etx_CentralReg(prefix + "CellOut_Eflow_etx_CentralReg",&master),
    CellOut_Eflow_ety_CentralReg(prefix + "CellOut_Eflow_ety_CentralReg",&master),
    CellOut_Eflow_sumet_CentralReg(prefix + "CellOut_Eflow_sumet_CentralReg",&master),
    CellOut_Eflow_phi_CentralReg(prefix + "CellOut_Eflow_phi_CentralReg",&master),
    CellOut_Eflow_etx_EndcapRegion(prefix + "CellOut_Eflow_etx_EndcapRegion",&master),
    CellOut_Eflow_ety_EndcapRegion(prefix + "CellOut_Eflow_ety_EndcapRegion",&master),
    CellOut_Eflow_sumet_EndcapRegion(prefix + "CellOut_Eflow_sumet_EndcapRegion",&master),
    CellOut_Eflow_phi_EndcapRegion(prefix + "CellOut_Eflow_phi_EndcapRegion",&master),
    CellOut_Eflow_etx_ForwardReg(prefix + "CellOut_Eflow_etx_ForwardReg",&master),
    CellOut_Eflow_ety_ForwardReg(prefix + "CellOut_Eflow_ety_ForwardReg",&master),
    CellOut_Eflow_sumet_ForwardReg(prefix + "CellOut_Eflow_sumet_ForwardReg",&master),
    CellOut_Eflow_phi_ForwardReg(prefix + "CellOut_Eflow_phi_ForwardReg",&master),
    CellOut_Eflow_Muid_etx(prefix + "CellOut_Eflow_Muid_etx",&master),
    CellOut_Eflow_Muid_ety(prefix + "CellOut_Eflow_Muid_ety",&master),
    CellOut_Eflow_Muid_phi(prefix + "CellOut_Eflow_Muid_phi",&master),
    CellOut_Eflow_Muid_et(prefix + "CellOut_Eflow_Muid_et",&master),
    CellOut_Eflow_Muid_sumet(prefix + "CellOut_Eflow_Muid_sumet",&master),
    CellOut_Eflow_Muid_etx_CentralReg(prefix + "CellOut_Eflow_Muid_etx_CentralReg",&master),
    CellOut_Eflow_Muid_ety_CentralReg(prefix + "CellOut_Eflow_Muid_ety_CentralReg",&master),
    CellOut_Eflow_Muid_sumet_CentralReg(prefix + "CellOut_Eflow_Muid_sumet_CentralReg",&master),
    CellOut_Eflow_Muid_phi_CentralReg(prefix + "CellOut_Eflow_Muid_phi_CentralReg",&master),
    CellOut_Eflow_Muid_etx_EndcapRegion(prefix + "CellOut_Eflow_Muid_etx_EndcapRegion",&master),
    CellOut_Eflow_Muid_ety_EndcapRegion(prefix + "CellOut_Eflow_Muid_ety_EndcapRegion",&master),
    CellOut_Eflow_Muid_sumet_EndcapRegion(prefix + "CellOut_Eflow_Muid_sumet_EndcapRegion",&master),
    CellOut_Eflow_Muid_phi_EndcapRegion(prefix + "CellOut_Eflow_Muid_phi_EndcapRegion",&master),
    CellOut_Eflow_Muid_etx_ForwardReg(prefix + "CellOut_Eflow_Muid_etx_ForwardReg",&master),
    CellOut_Eflow_Muid_ety_ForwardReg(prefix + "CellOut_Eflow_Muid_ety_ForwardReg",&master),
    CellOut_Eflow_Muid_sumet_ForwardReg(prefix + "CellOut_Eflow_Muid_sumet_ForwardReg",&master),
    CellOut_Eflow_Muid_phi_ForwardReg(prefix + "CellOut_Eflow_Muid_phi_ForwardReg",&master),
    CellOut_Eflow_Muons_etx(prefix + "CellOut_Eflow_Muons_etx",&master),
    CellOut_Eflow_Muons_ety(prefix + "CellOut_Eflow_Muons_ety",&master),
    CellOut_Eflow_Muons_phi(prefix + "CellOut_Eflow_Muons_phi",&master),
    CellOut_Eflow_Muons_et(prefix + "CellOut_Eflow_Muons_et",&master),
    CellOut_Eflow_Muons_sumet(prefix + "CellOut_Eflow_Muons_sumet",&master),
    CellOut_Eflow_Muons_etx_CentralReg(prefix + "CellOut_Eflow_Muons_etx_CentralReg",&master),
    CellOut_Eflow_Muons_ety_CentralReg(prefix + "CellOut_Eflow_Muons_ety_CentralReg",&master),
    CellOut_Eflow_Muons_sumet_CentralReg(prefix + "CellOut_Eflow_Muons_sumet_CentralReg",&master),
    CellOut_Eflow_Muons_phi_CentralReg(prefix + "CellOut_Eflow_Muons_phi_CentralReg",&master),
    CellOut_Eflow_Muons_etx_EndcapRegion(prefix + "CellOut_Eflow_Muons_etx_EndcapRegion",&master),
    CellOut_Eflow_Muons_ety_EndcapRegion(prefix + "CellOut_Eflow_Muons_ety_EndcapRegion",&master),
    CellOut_Eflow_Muons_sumet_EndcapRegion(prefix + "CellOut_Eflow_Muons_sumet_EndcapRegion",&master),
    CellOut_Eflow_Muons_phi_EndcapRegion(prefix + "CellOut_Eflow_Muons_phi_EndcapRegion",&master),
    CellOut_Eflow_Muons_etx_ForwardReg(prefix + "CellOut_Eflow_Muons_etx_ForwardReg",&master),
    CellOut_Eflow_Muons_ety_ForwardReg(prefix + "CellOut_Eflow_Muons_ety_ForwardReg",&master),
    CellOut_Eflow_Muons_sumet_ForwardReg(prefix + "CellOut_Eflow_Muons_sumet_ForwardReg",&master),
    CellOut_Eflow_Muons_phi_ForwardReg(prefix + "CellOut_Eflow_Muons_phi_ForwardReg",&master),
    RefMuon_Track_etx(prefix + "RefMuon_Track_etx",&master),
    RefMuon_Track_ety(prefix + "RefMuon_Track_ety",&master),
    RefMuon_Track_phi(prefix + "RefMuon_Track_phi",&master),
    RefMuon_Track_et(prefix + "RefMuon_Track_et",&master),
    RefMuon_Track_sumet(prefix + "RefMuon_Track_sumet",&master),
    RefMuon_Track_etx_CentralReg(prefix + "RefMuon_Track_etx_CentralReg",&master),
    RefMuon_Track_ety_CentralReg(prefix + "RefMuon_Track_ety_CentralReg",&master),
    RefMuon_Track_sumet_CentralReg(prefix + "RefMuon_Track_sumet_CentralReg",&master),
    RefMuon_Track_phi_CentralReg(prefix + "RefMuon_Track_phi_CentralReg",&master),
    RefMuon_Track_etx_EndcapRegion(prefix + "RefMuon_Track_etx_EndcapRegion",&master),
    RefMuon_Track_ety_EndcapRegion(prefix + "RefMuon_Track_ety_EndcapRegion",&master),
    RefMuon_Track_sumet_EndcapRegion(prefix + "RefMuon_Track_sumet_EndcapRegion",&master),
    RefMuon_Track_phi_EndcapRegion(prefix + "RefMuon_Track_phi_EndcapRegion",&master),
    RefMuon_Track_etx_ForwardReg(prefix + "RefMuon_Track_etx_ForwardReg",&master),
    RefMuon_Track_ety_ForwardReg(prefix + "RefMuon_Track_ety_ForwardReg",&master),
    RefMuon_Track_sumet_ForwardReg(prefix + "RefMuon_Track_sumet_ForwardReg",&master),
    RefMuon_Track_phi_ForwardReg(prefix + "RefMuon_Track_phi_ForwardReg",&master),
    RefMuon_Track_Staco_etx(prefix + "RefMuon_Track_Staco_etx",&master),
    RefMuon_Track_Staco_ety(prefix + "RefMuon_Track_Staco_ety",&master),
    RefMuon_Track_Staco_phi(prefix + "RefMuon_Track_Staco_phi",&master),
    RefMuon_Track_Staco_et(prefix + "RefMuon_Track_Staco_et",&master),
    RefMuon_Track_Staco_sumet(prefix + "RefMuon_Track_Staco_sumet",&master),
    RefMuon_Track_Staco_etx_CentralReg(prefix + "RefMuon_Track_Staco_etx_CentralReg",&master),
    RefMuon_Track_Staco_ety_CentralReg(prefix + "RefMuon_Track_Staco_ety_CentralReg",&master),
    RefMuon_Track_Staco_sumet_CentralReg(prefix + "RefMuon_Track_Staco_sumet_CentralReg",&master),
    RefMuon_Track_Staco_phi_CentralReg(prefix + "RefMuon_Track_Staco_phi_CentralReg",&master),
    RefMuon_Track_Staco_etx_EndcapRegion(prefix + "RefMuon_Track_Staco_etx_EndcapRegion",&master),
    RefMuon_Track_Staco_ety_EndcapRegion(prefix + "RefMuon_Track_Staco_ety_EndcapRegion",&master),
    RefMuon_Track_Staco_sumet_EndcapRegion(prefix + "RefMuon_Track_Staco_sumet_EndcapRegion",&master),
    RefMuon_Track_Staco_phi_EndcapRegion(prefix + "RefMuon_Track_Staco_phi_EndcapRegion",&master),
    RefMuon_Track_Staco_etx_ForwardReg(prefix + "RefMuon_Track_Staco_etx_ForwardReg",&master),
    RefMuon_Track_Staco_ety_ForwardReg(prefix + "RefMuon_Track_Staco_ety_ForwardReg",&master),
    RefMuon_Track_Staco_sumet_ForwardReg(prefix + "RefMuon_Track_Staco_sumet_ForwardReg",&master),
    RefMuon_Track_Staco_phi_ForwardReg(prefix + "RefMuon_Track_Staco_phi_ForwardReg",&master),
    RefMuon_Track_Muid_etx(prefix + "RefMuon_Track_Muid_etx",&master),
    RefMuon_Track_Muid_ety(prefix + "RefMuon_Track_Muid_ety",&master),
    RefMuon_Track_Muid_phi(prefix + "RefMuon_Track_Muid_phi",&master),
    RefMuon_Track_Muid_et(prefix + "RefMuon_Track_Muid_et",&master),
    RefMuon_Track_Muid_sumet(prefix + "RefMuon_Track_Muid_sumet",&master),
    RefMuon_Track_Muid_etx_CentralReg(prefix + "RefMuon_Track_Muid_etx_CentralReg",&master),
    RefMuon_Track_Muid_ety_CentralReg(prefix + "RefMuon_Track_Muid_ety_CentralReg",&master),
    RefMuon_Track_Muid_sumet_CentralReg(prefix + "RefMuon_Track_Muid_sumet_CentralReg",&master),
    RefMuon_Track_Muid_phi_CentralReg(prefix + "RefMuon_Track_Muid_phi_CentralReg",&master),
    RefMuon_Track_Muid_etx_EndcapRegion(prefix + "RefMuon_Track_Muid_etx_EndcapRegion",&master),
    RefMuon_Track_Muid_ety_EndcapRegion(prefix + "RefMuon_Track_Muid_ety_EndcapRegion",&master),
    RefMuon_Track_Muid_sumet_EndcapRegion(prefix + "RefMuon_Track_Muid_sumet_EndcapRegion",&master),
    RefMuon_Track_Muid_phi_EndcapRegion(prefix + "RefMuon_Track_Muid_phi_EndcapRegion",&master),
    RefMuon_Track_Muid_etx_ForwardReg(prefix + "RefMuon_Track_Muid_etx_ForwardReg",&master),
    RefMuon_Track_Muid_ety_ForwardReg(prefix + "RefMuon_Track_Muid_ety_ForwardReg",&master),
    RefMuon_Track_Muid_sumet_ForwardReg(prefix + "RefMuon_Track_Muid_sumet_ForwardReg",&master),
    RefMuon_Track_Muid_phi_ForwardReg(prefix + "RefMuon_Track_Muid_phi_ForwardReg",&master),
    RefMuons_Track_etx(prefix + "RefMuons_Track_etx",&master),
    RefMuons_Track_ety(prefix + "RefMuons_Track_ety",&master),
    RefMuons_Track_phi(prefix + "RefMuons_Track_phi",&master),
    RefMuons_Track_et(prefix + "RefMuons_Track_et",&master),
    RefMuons_Track_sumet(prefix + "RefMuons_Track_sumet",&master),
    RefMuons_Track_etx_CentralReg(prefix + "RefMuons_Track_etx_CentralReg",&master),
    RefMuons_Track_ety_CentralReg(prefix + "RefMuons_Track_ety_CentralReg",&master),
    RefMuons_Track_sumet_CentralReg(prefix + "RefMuons_Track_sumet_CentralReg",&master),
    RefMuons_Track_phi_CentralReg(prefix + "RefMuons_Track_phi_CentralReg",&master),
    RefMuons_Track_etx_EndcapRegion(prefix + "RefMuons_Track_etx_EndcapRegion",&master),
    RefMuons_Track_ety_EndcapRegion(prefix + "RefMuons_Track_ety_EndcapRegion",&master),
    RefMuons_Track_sumet_EndcapRegion(prefix + "RefMuons_Track_sumet_EndcapRegion",&master),
    RefMuons_Track_phi_EndcapRegion(prefix + "RefMuons_Track_phi_EndcapRegion",&master),
    RefMuons_Track_etx_ForwardReg(prefix + "RefMuons_Track_etx_ForwardReg",&master),
    RefMuons_Track_ety_ForwardReg(prefix + "RefMuons_Track_ety_ForwardReg",&master),
    RefMuons_Track_sumet_ForwardReg(prefix + "RefMuons_Track_sumet_ForwardReg",&master),
    RefMuons_Track_phi_ForwardReg(prefix + "RefMuons_Track_phi_ForwardReg",&master),
    RefFinal_BDTMedium_etx(prefix + "RefFinal_BDTMedium_etx",&master),
    RefFinal_BDTMedium_ety(prefix + "RefFinal_BDTMedium_ety",&master),
    RefFinal_BDTMedium_phi(prefix + "RefFinal_BDTMedium_phi",&master),
    RefFinal_BDTMedium_et(prefix + "RefFinal_BDTMedium_et",&master),
    RefFinal_BDTMedium_sumet(prefix + "RefFinal_BDTMedium_sumet",&master),
    RefFinal_BDTMedium_etx_CentralReg(prefix + "RefFinal_BDTMedium_etx_CentralReg",&master),
    RefFinal_BDTMedium_ety_CentralReg(prefix + "RefFinal_BDTMedium_ety_CentralReg",&master),
    RefFinal_BDTMedium_sumet_CentralReg(prefix + "RefFinal_BDTMedium_sumet_CentralReg",&master),
    RefFinal_BDTMedium_phi_CentralReg(prefix + "RefFinal_BDTMedium_phi_CentralReg",&master),
    RefFinal_BDTMedium_etx_EndcapRegion(prefix + "RefFinal_BDTMedium_etx_EndcapRegion",&master),
    RefFinal_BDTMedium_ety_EndcapRegion(prefix + "RefFinal_BDTMedium_ety_EndcapRegion",&master),
    RefFinal_BDTMedium_sumet_EndcapRegion(prefix + "RefFinal_BDTMedium_sumet_EndcapRegion",&master),
    RefFinal_BDTMedium_phi_EndcapRegion(prefix + "RefFinal_BDTMedium_phi_EndcapRegion",&master),
    RefFinal_BDTMedium_etx_ForwardReg(prefix + "RefFinal_BDTMedium_etx_ForwardReg",&master),
    RefFinal_BDTMedium_ety_ForwardReg(prefix + "RefFinal_BDTMedium_ety_ForwardReg",&master),
    RefFinal_BDTMedium_sumet_ForwardReg(prefix + "RefFinal_BDTMedium_sumet_ForwardReg",&master),
    RefFinal_BDTMedium_phi_ForwardReg(prefix + "RefFinal_BDTMedium_phi_ForwardReg",&master),
    RefGamma_BDTMedium_etx(prefix + "RefGamma_BDTMedium_etx",&master),
    RefGamma_BDTMedium_ety(prefix + "RefGamma_BDTMedium_ety",&master),
    RefGamma_BDTMedium_phi(prefix + "RefGamma_BDTMedium_phi",&master),
    RefGamma_BDTMedium_et(prefix + "RefGamma_BDTMedium_et",&master),
    RefGamma_BDTMedium_sumet(prefix + "RefGamma_BDTMedium_sumet",&master),
    RefGamma_BDTMedium_etx_CentralReg(prefix + "RefGamma_BDTMedium_etx_CentralReg",&master),
    RefGamma_BDTMedium_ety_CentralReg(prefix + "RefGamma_BDTMedium_ety_CentralReg",&master),
    RefGamma_BDTMedium_sumet_CentralReg(prefix + "RefGamma_BDTMedium_sumet_CentralReg",&master),
    RefGamma_BDTMedium_phi_CentralReg(prefix + "RefGamma_BDTMedium_phi_CentralReg",&master),
    RefGamma_BDTMedium_etx_EndcapRegion(prefix + "RefGamma_BDTMedium_etx_EndcapRegion",&master),
    RefGamma_BDTMedium_ety_EndcapRegion(prefix + "RefGamma_BDTMedium_ety_EndcapRegion",&master),
    RefGamma_BDTMedium_sumet_EndcapRegion(prefix + "RefGamma_BDTMedium_sumet_EndcapRegion",&master),
    RefGamma_BDTMedium_phi_EndcapRegion(prefix + "RefGamma_BDTMedium_phi_EndcapRegion",&master),
    RefGamma_BDTMedium_etx_ForwardReg(prefix + "RefGamma_BDTMedium_etx_ForwardReg",&master),
    RefGamma_BDTMedium_ety_ForwardReg(prefix + "RefGamma_BDTMedium_ety_ForwardReg",&master),
    RefGamma_BDTMedium_sumet_ForwardReg(prefix + "RefGamma_BDTMedium_sumet_ForwardReg",&master),
    RefGamma_BDTMedium_phi_ForwardReg(prefix + "RefGamma_BDTMedium_phi_ForwardReg",&master),
    RefEle_BDTMedium_etx(prefix + "RefEle_BDTMedium_etx",&master),
    RefEle_BDTMedium_ety(prefix + "RefEle_BDTMedium_ety",&master),
    RefEle_BDTMedium_phi(prefix + "RefEle_BDTMedium_phi",&master),
    RefEle_BDTMedium_et(prefix + "RefEle_BDTMedium_et",&master),
    RefEle_BDTMedium_sumet(prefix + "RefEle_BDTMedium_sumet",&master),
    RefEle_BDTMedium_etx_CentralReg(prefix + "RefEle_BDTMedium_etx_CentralReg",&master),
    RefEle_BDTMedium_ety_CentralReg(prefix + "RefEle_BDTMedium_ety_CentralReg",&master),
    RefEle_BDTMedium_sumet_CentralReg(prefix + "RefEle_BDTMedium_sumet_CentralReg",&master),
    RefEle_BDTMedium_phi_CentralReg(prefix + "RefEle_BDTMedium_phi_CentralReg",&master),
    RefEle_BDTMedium_etx_EndcapRegion(prefix + "RefEle_BDTMedium_etx_EndcapRegion",&master),
    RefEle_BDTMedium_ety_EndcapRegion(prefix + "RefEle_BDTMedium_ety_EndcapRegion",&master),
    RefEle_BDTMedium_sumet_EndcapRegion(prefix + "RefEle_BDTMedium_sumet_EndcapRegion",&master),
    RefEle_BDTMedium_phi_EndcapRegion(prefix + "RefEle_BDTMedium_phi_EndcapRegion",&master),
    RefEle_BDTMedium_etx_ForwardReg(prefix + "RefEle_BDTMedium_etx_ForwardReg",&master),
    RefEle_BDTMedium_ety_ForwardReg(prefix + "RefEle_BDTMedium_ety_ForwardReg",&master),
    RefEle_BDTMedium_sumet_ForwardReg(prefix + "RefEle_BDTMedium_sumet_ForwardReg",&master),
    RefEle_BDTMedium_phi_ForwardReg(prefix + "RefEle_BDTMedium_phi_ForwardReg",&master),
    RefTau_BDTMedium_etx(prefix + "RefTau_BDTMedium_etx",&master),
    RefTau_BDTMedium_ety(prefix + "RefTau_BDTMedium_ety",&master),
    RefTau_BDTMedium_phi(prefix + "RefTau_BDTMedium_phi",&master),
    RefTau_BDTMedium_et(prefix + "RefTau_BDTMedium_et",&master),
    RefTau_BDTMedium_sumet(prefix + "RefTau_BDTMedium_sumet",&master),
    RefTau_BDTMedium_etx_CentralReg(prefix + "RefTau_BDTMedium_etx_CentralReg",&master),
    RefTau_BDTMedium_ety_CentralReg(prefix + "RefTau_BDTMedium_ety_CentralReg",&master),
    RefTau_BDTMedium_sumet_CentralReg(prefix + "RefTau_BDTMedium_sumet_CentralReg",&master),
    RefTau_BDTMedium_phi_CentralReg(prefix + "RefTau_BDTMedium_phi_CentralReg",&master),
    RefTau_BDTMedium_etx_EndcapRegion(prefix + "RefTau_BDTMedium_etx_EndcapRegion",&master),
    RefTau_BDTMedium_ety_EndcapRegion(prefix + "RefTau_BDTMedium_ety_EndcapRegion",&master),
    RefTau_BDTMedium_sumet_EndcapRegion(prefix + "RefTau_BDTMedium_sumet_EndcapRegion",&master),
    RefTau_BDTMedium_phi_EndcapRegion(prefix + "RefTau_BDTMedium_phi_EndcapRegion",&master),
    RefTau_BDTMedium_etx_ForwardReg(prefix + "RefTau_BDTMedium_etx_ForwardReg",&master),
    RefTau_BDTMedium_ety_ForwardReg(prefix + "RefTau_BDTMedium_ety_ForwardReg",&master),
    RefTau_BDTMedium_sumet_ForwardReg(prefix + "RefTau_BDTMedium_sumet_ForwardReg",&master),
    RefTau_BDTMedium_phi_ForwardReg(prefix + "RefTau_BDTMedium_phi_ForwardReg",&master),
    RefJet_BDTMedium_etx(prefix + "RefJet_BDTMedium_etx",&master),
    RefJet_BDTMedium_ety(prefix + "RefJet_BDTMedium_ety",&master),
    RefJet_BDTMedium_phi(prefix + "RefJet_BDTMedium_phi",&master),
    RefJet_BDTMedium_et(prefix + "RefJet_BDTMedium_et",&master),
    RefJet_BDTMedium_sumet(prefix + "RefJet_BDTMedium_sumet",&master),
    RefJet_BDTMedium_etx_CentralReg(prefix + "RefJet_BDTMedium_etx_CentralReg",&master),
    RefJet_BDTMedium_ety_CentralReg(prefix + "RefJet_BDTMedium_ety_CentralReg",&master),
    RefJet_BDTMedium_sumet_CentralReg(prefix + "RefJet_BDTMedium_sumet_CentralReg",&master),
    RefJet_BDTMedium_phi_CentralReg(prefix + "RefJet_BDTMedium_phi_CentralReg",&master),
    RefJet_BDTMedium_etx_EndcapRegion(prefix + "RefJet_BDTMedium_etx_EndcapRegion",&master),
    RefJet_BDTMedium_ety_EndcapRegion(prefix + "RefJet_BDTMedium_ety_EndcapRegion",&master),
    RefJet_BDTMedium_sumet_EndcapRegion(prefix + "RefJet_BDTMedium_sumet_EndcapRegion",&master),
    RefJet_BDTMedium_phi_EndcapRegion(prefix + "RefJet_BDTMedium_phi_EndcapRegion",&master),
    RefJet_BDTMedium_etx_ForwardReg(prefix + "RefJet_BDTMedium_etx_ForwardReg",&master),
    RefJet_BDTMedium_ety_ForwardReg(prefix + "RefJet_BDTMedium_ety_ForwardReg",&master),
    RefJet_BDTMedium_sumet_ForwardReg(prefix + "RefJet_BDTMedium_sumet_ForwardReg",&master),
    RefJet_BDTMedium_phi_ForwardReg(prefix + "RefJet_BDTMedium_phi_ForwardReg",&master),
    RefMuon_Staco_BDTMedium_etx(prefix + "RefMuon_Staco_BDTMedium_etx",&master),
    RefMuon_Staco_BDTMedium_ety(prefix + "RefMuon_Staco_BDTMedium_ety",&master),
    RefMuon_Staco_BDTMedium_phi(prefix + "RefMuon_Staco_BDTMedium_phi",&master),
    RefMuon_Staco_BDTMedium_et(prefix + "RefMuon_Staco_BDTMedium_et",&master),
    RefMuon_Staco_BDTMedium_sumet(prefix + "RefMuon_Staco_BDTMedium_sumet",&master),
    RefMuon_Staco_BDTMedium_etx_CentralReg(prefix + "RefMuon_Staco_BDTMedium_etx_CentralReg",&master),
    RefMuon_Staco_BDTMedium_ety_CentralReg(prefix + "RefMuon_Staco_BDTMedium_ety_CentralReg",&master),
    RefMuon_Staco_BDTMedium_sumet_CentralReg(prefix + "RefMuon_Staco_BDTMedium_sumet_CentralReg",&master),
    RefMuon_Staco_BDTMedium_phi_CentralReg(prefix + "RefMuon_Staco_BDTMedium_phi_CentralReg",&master),
    RefMuon_Staco_BDTMedium_etx_EndcapRegion(prefix + "RefMuon_Staco_BDTMedium_etx_EndcapRegion",&master),
    RefMuon_Staco_BDTMedium_ety_EndcapRegion(prefix + "RefMuon_Staco_BDTMedium_ety_EndcapRegion",&master),
    RefMuon_Staco_BDTMedium_sumet_EndcapRegion(prefix + "RefMuon_Staco_BDTMedium_sumet_EndcapRegion",&master),
    RefMuon_Staco_BDTMedium_phi_EndcapRegion(prefix + "RefMuon_Staco_BDTMedium_phi_EndcapRegion",&master),
    RefMuon_Staco_BDTMedium_etx_ForwardReg(prefix + "RefMuon_Staco_BDTMedium_etx_ForwardReg",&master),
    RefMuon_Staco_BDTMedium_ety_ForwardReg(prefix + "RefMuon_Staco_BDTMedium_ety_ForwardReg",&master),
    RefMuon_Staco_BDTMedium_sumet_ForwardReg(prefix + "RefMuon_Staco_BDTMedium_sumet_ForwardReg",&master),
    RefMuon_Staco_BDTMedium_phi_ForwardReg(prefix + "RefMuon_Staco_BDTMedium_phi_ForwardReg",&master),
    RefFinal_AntiKt4LCTopo_tightpp_etx(prefix + "RefFinal_AntiKt4LCTopo_tightpp_etx",&master),
    RefFinal_AntiKt4LCTopo_tightpp_ety(prefix + "RefFinal_AntiKt4LCTopo_tightpp_ety",&master),
    RefFinal_AntiKt4LCTopo_tightpp_phi(prefix + "RefFinal_AntiKt4LCTopo_tightpp_phi",&master),
    RefFinal_AntiKt4LCTopo_tightpp_et(prefix + "RefFinal_AntiKt4LCTopo_tightpp_et",&master),
    RefFinal_AntiKt4LCTopo_tightpp_sumet(prefix + "RefFinal_AntiKt4LCTopo_tightpp_sumet",&master),
    RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg",&master),
    RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg",&master),
    RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg",&master),
    RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg",&master),
    RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion",&master),
    RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion",&master),
    RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",&master),
    RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion",&master),
    RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg",&master),
    RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg",&master),
    RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg",&master),
    RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg",&master),
    RefGamma_AntiKt4LCTopo_tightpp_etx(prefix + "RefGamma_AntiKt4LCTopo_tightpp_etx",&master),
    RefGamma_AntiKt4LCTopo_tightpp_ety(prefix + "RefGamma_AntiKt4LCTopo_tightpp_ety",&master),
    RefGamma_AntiKt4LCTopo_tightpp_phi(prefix + "RefGamma_AntiKt4LCTopo_tightpp_phi",&master),
    RefGamma_AntiKt4LCTopo_tightpp_et(prefix + "RefGamma_AntiKt4LCTopo_tightpp_et",&master),
    RefGamma_AntiKt4LCTopo_tightpp_sumet(prefix + "RefGamma_AntiKt4LCTopo_tightpp_sumet",&master),
    RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg",&master),
    RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg",&master),
    RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg",&master),
    RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg",&master),
    RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion",&master),
    RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion",&master),
    RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",&master),
    RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion",&master),
    RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg",&master),
    RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg",&master),
    RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg",&master),
    RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg",&master),
    RefEle_AntiKt4LCTopo_tightpp_etx(prefix + "RefEle_AntiKt4LCTopo_tightpp_etx",&master),
    RefEle_AntiKt4LCTopo_tightpp_ety(prefix + "RefEle_AntiKt4LCTopo_tightpp_ety",&master),
    RefEle_AntiKt4LCTopo_tightpp_phi(prefix + "RefEle_AntiKt4LCTopo_tightpp_phi",&master),
    RefEle_AntiKt4LCTopo_tightpp_et(prefix + "RefEle_AntiKt4LCTopo_tightpp_et",&master),
    RefEle_AntiKt4LCTopo_tightpp_sumet(prefix + "RefEle_AntiKt4LCTopo_tightpp_sumet",&master),
    RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg",&master),
    RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg",&master),
    RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg",&master),
    RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg",&master),
    RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion",&master),
    RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion",&master),
    RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",&master),
    RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion",&master),
    RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg",&master),
    RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg",&master),
    RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg",&master),
    RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg",&master),
    RefTau_AntiKt4LCTopo_tightpp_etx(prefix + "RefTau_AntiKt4LCTopo_tightpp_etx",&master),
    RefTau_AntiKt4LCTopo_tightpp_ety(prefix + "RefTau_AntiKt4LCTopo_tightpp_ety",&master),
    RefTau_AntiKt4LCTopo_tightpp_phi(prefix + "RefTau_AntiKt4LCTopo_tightpp_phi",&master),
    RefTau_AntiKt4LCTopo_tightpp_et(prefix + "RefTau_AntiKt4LCTopo_tightpp_et",&master),
    RefTau_AntiKt4LCTopo_tightpp_sumet(prefix + "RefTau_AntiKt4LCTopo_tightpp_sumet",&master),
    RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg",&master),
    RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg",&master),
    RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg",&master),
    RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg",&master),
    RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion",&master),
    RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion",&master),
    RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",&master),
    RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion",&master),
    RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg",&master),
    RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg",&master),
    RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg",&master),
    RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg",&master),
    RefJet_AntiKt4LCTopo_tightpp_etx(prefix + "RefJet_AntiKt4LCTopo_tightpp_etx",&master),
    RefJet_AntiKt4LCTopo_tightpp_ety(prefix + "RefJet_AntiKt4LCTopo_tightpp_ety",&master),
    RefJet_AntiKt4LCTopo_tightpp_phi(prefix + "RefJet_AntiKt4LCTopo_tightpp_phi",&master),
    RefJet_AntiKt4LCTopo_tightpp_et(prefix + "RefJet_AntiKt4LCTopo_tightpp_et",&master),
    RefJet_AntiKt4LCTopo_tightpp_sumet(prefix + "RefJet_AntiKt4LCTopo_tightpp_sumet",&master),
    RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg",&master),
    RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg",&master),
    RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg",&master),
    RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg",&master),
    RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion",&master),
    RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion",&master),
    RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",&master),
    RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion",&master),
    RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg",&master),
    RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg",&master),
    RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg",&master),
    RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_etx",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_ety",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_phi",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_et(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_et",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg",&master),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  MET::MET(const std::string& prefix):
    TObject(),
    RefFinal_etx(prefix + "RefFinal_etx",0),
    RefFinal_ety(prefix + "RefFinal_ety",0),
    RefFinal_phi(prefix + "RefFinal_phi",0),
    RefFinal_et(prefix + "RefFinal_et",0),
    RefFinal_sumet(prefix + "RefFinal_sumet",0),
    RefFinal_etx_CentralReg(prefix + "RefFinal_etx_CentralReg",0),
    RefFinal_ety_CentralReg(prefix + "RefFinal_ety_CentralReg",0),
    RefFinal_sumet_CentralReg(prefix + "RefFinal_sumet_CentralReg",0),
    RefFinal_phi_CentralReg(prefix + "RefFinal_phi_CentralReg",0),
    RefFinal_etx_EndcapRegion(prefix + "RefFinal_etx_EndcapRegion",0),
    RefFinal_ety_EndcapRegion(prefix + "RefFinal_ety_EndcapRegion",0),
    RefFinal_sumet_EndcapRegion(prefix + "RefFinal_sumet_EndcapRegion",0),
    RefFinal_phi_EndcapRegion(prefix + "RefFinal_phi_EndcapRegion",0),
    RefFinal_etx_ForwardReg(prefix + "RefFinal_etx_ForwardReg",0),
    RefFinal_ety_ForwardReg(prefix + "RefFinal_ety_ForwardReg",0),
    RefFinal_sumet_ForwardReg(prefix + "RefFinal_sumet_ForwardReg",0),
    RefFinal_phi_ForwardReg(prefix + "RefFinal_phi_ForwardReg",0),
    RefEle_etx(prefix + "RefEle_etx",0),
    RefEle_ety(prefix + "RefEle_ety",0),
    RefEle_phi(prefix + "RefEle_phi",0),
    RefEle_et(prefix + "RefEle_et",0),
    RefEle_sumet(prefix + "RefEle_sumet",0),
    RefEle_etx_CentralReg(prefix + "RefEle_etx_CentralReg",0),
    RefEle_ety_CentralReg(prefix + "RefEle_ety_CentralReg",0),
    RefEle_sumet_CentralReg(prefix + "RefEle_sumet_CentralReg",0),
    RefEle_phi_CentralReg(prefix + "RefEle_phi_CentralReg",0),
    RefEle_etx_EndcapRegion(prefix + "RefEle_etx_EndcapRegion",0),
    RefEle_ety_EndcapRegion(prefix + "RefEle_ety_EndcapRegion",0),
    RefEle_sumet_EndcapRegion(prefix + "RefEle_sumet_EndcapRegion",0),
    RefEle_phi_EndcapRegion(prefix + "RefEle_phi_EndcapRegion",0),
    RefEle_etx_ForwardReg(prefix + "RefEle_etx_ForwardReg",0),
    RefEle_ety_ForwardReg(prefix + "RefEle_ety_ForwardReg",0),
    RefEle_sumet_ForwardReg(prefix + "RefEle_sumet_ForwardReg",0),
    RefEle_phi_ForwardReg(prefix + "RefEle_phi_ForwardReg",0),
    RefJet_etx(prefix + "RefJet_etx",0),
    RefJet_ety(prefix + "RefJet_ety",0),
    RefJet_phi(prefix + "RefJet_phi",0),
    RefJet_et(prefix + "RefJet_et",0),
    RefJet_sumet(prefix + "RefJet_sumet",0),
    RefJet_etx_CentralReg(prefix + "RefJet_etx_CentralReg",0),
    RefJet_ety_CentralReg(prefix + "RefJet_ety_CentralReg",0),
    RefJet_sumet_CentralReg(prefix + "RefJet_sumet_CentralReg",0),
    RefJet_phi_CentralReg(prefix + "RefJet_phi_CentralReg",0),
    RefJet_etx_EndcapRegion(prefix + "RefJet_etx_EndcapRegion",0),
    RefJet_ety_EndcapRegion(prefix + "RefJet_ety_EndcapRegion",0),
    RefJet_sumet_EndcapRegion(prefix + "RefJet_sumet_EndcapRegion",0),
    RefJet_phi_EndcapRegion(prefix + "RefJet_phi_EndcapRegion",0),
    RefJet_etx_ForwardReg(prefix + "RefJet_etx_ForwardReg",0),
    RefJet_ety_ForwardReg(prefix + "RefJet_ety_ForwardReg",0),
    RefJet_sumet_ForwardReg(prefix + "RefJet_sumet_ForwardReg",0),
    RefJet_phi_ForwardReg(prefix + "RefJet_phi_ForwardReg",0),
    RefMuon_etx(prefix + "RefMuon_etx",0),
    RefMuon_ety(prefix + "RefMuon_ety",0),
    RefMuon_phi(prefix + "RefMuon_phi",0),
    RefMuon_et(prefix + "RefMuon_et",0),
    RefMuon_sumet(prefix + "RefMuon_sumet",0),
    RefMuon_etx_CentralReg(prefix + "RefMuon_etx_CentralReg",0),
    RefMuon_ety_CentralReg(prefix + "RefMuon_ety_CentralReg",0),
    RefMuon_sumet_CentralReg(prefix + "RefMuon_sumet_CentralReg",0),
    RefMuon_phi_CentralReg(prefix + "RefMuon_phi_CentralReg",0),
    RefMuon_etx_EndcapRegion(prefix + "RefMuon_etx_EndcapRegion",0),
    RefMuon_ety_EndcapRegion(prefix + "RefMuon_ety_EndcapRegion",0),
    RefMuon_sumet_EndcapRegion(prefix + "RefMuon_sumet_EndcapRegion",0),
    RefMuon_phi_EndcapRegion(prefix + "RefMuon_phi_EndcapRegion",0),
    RefMuon_etx_ForwardReg(prefix + "RefMuon_etx_ForwardReg",0),
    RefMuon_ety_ForwardReg(prefix + "RefMuon_ety_ForwardReg",0),
    RefMuon_sumet_ForwardReg(prefix + "RefMuon_sumet_ForwardReg",0),
    RefMuon_phi_ForwardReg(prefix + "RefMuon_phi_ForwardReg",0),
    RefMuon_Staco_etx(prefix + "RefMuon_Staco_etx",0),
    RefMuon_Staco_ety(prefix + "RefMuon_Staco_ety",0),
    RefMuon_Staco_phi(prefix + "RefMuon_Staco_phi",0),
    RefMuon_Staco_et(prefix + "RefMuon_Staco_et",0),
    RefMuon_Staco_sumet(prefix + "RefMuon_Staco_sumet",0),
    RefMuon_Staco_etx_CentralReg(prefix + "RefMuon_Staco_etx_CentralReg",0),
    RefMuon_Staco_ety_CentralReg(prefix + "RefMuon_Staco_ety_CentralReg",0),
    RefMuon_Staco_sumet_CentralReg(prefix + "RefMuon_Staco_sumet_CentralReg",0),
    RefMuon_Staco_phi_CentralReg(prefix + "RefMuon_Staco_phi_CentralReg",0),
    RefMuon_Staco_etx_EndcapRegion(prefix + "RefMuon_Staco_etx_EndcapRegion",0),
    RefMuon_Staco_ety_EndcapRegion(prefix + "RefMuon_Staco_ety_EndcapRegion",0),
    RefMuon_Staco_sumet_EndcapRegion(prefix + "RefMuon_Staco_sumet_EndcapRegion",0),
    RefMuon_Staco_phi_EndcapRegion(prefix + "RefMuon_Staco_phi_EndcapRegion",0),
    RefMuon_Staco_etx_ForwardReg(prefix + "RefMuon_Staco_etx_ForwardReg",0),
    RefMuon_Staco_ety_ForwardReg(prefix + "RefMuon_Staco_ety_ForwardReg",0),
    RefMuon_Staco_sumet_ForwardReg(prefix + "RefMuon_Staco_sumet_ForwardReg",0),
    RefMuon_Staco_phi_ForwardReg(prefix + "RefMuon_Staco_phi_ForwardReg",0),
    RefMuon_Muid_etx(prefix + "RefMuon_Muid_etx",0),
    RefMuon_Muid_ety(prefix + "RefMuon_Muid_ety",0),
    RefMuon_Muid_phi(prefix + "RefMuon_Muid_phi",0),
    RefMuon_Muid_et(prefix + "RefMuon_Muid_et",0),
    RefMuon_Muid_sumet(prefix + "RefMuon_Muid_sumet",0),
    RefMuon_Muid_etx_CentralReg(prefix + "RefMuon_Muid_etx_CentralReg",0),
    RefMuon_Muid_ety_CentralReg(prefix + "RefMuon_Muid_ety_CentralReg",0),
    RefMuon_Muid_sumet_CentralReg(prefix + "RefMuon_Muid_sumet_CentralReg",0),
    RefMuon_Muid_phi_CentralReg(prefix + "RefMuon_Muid_phi_CentralReg",0),
    RefMuon_Muid_etx_EndcapRegion(prefix + "RefMuon_Muid_etx_EndcapRegion",0),
    RefMuon_Muid_ety_EndcapRegion(prefix + "RefMuon_Muid_ety_EndcapRegion",0),
    RefMuon_Muid_sumet_EndcapRegion(prefix + "RefMuon_Muid_sumet_EndcapRegion",0),
    RefMuon_Muid_phi_EndcapRegion(prefix + "RefMuon_Muid_phi_EndcapRegion",0),
    RefMuon_Muid_etx_ForwardReg(prefix + "RefMuon_Muid_etx_ForwardReg",0),
    RefMuon_Muid_ety_ForwardReg(prefix + "RefMuon_Muid_ety_ForwardReg",0),
    RefMuon_Muid_sumet_ForwardReg(prefix + "RefMuon_Muid_sumet_ForwardReg",0),
    RefMuon_Muid_phi_ForwardReg(prefix + "RefMuon_Muid_phi_ForwardReg",0),
    RefMuons_etx(prefix + "RefMuons_etx",0),
    RefMuons_ety(prefix + "RefMuons_ety",0),
    RefMuons_phi(prefix + "RefMuons_phi",0),
    RefMuons_et(prefix + "RefMuons_et",0),
    RefMuons_sumet(prefix + "RefMuons_sumet",0),
    RefMuons_etx_CentralReg(prefix + "RefMuons_etx_CentralReg",0),
    RefMuons_ety_CentralReg(prefix + "RefMuons_ety_CentralReg",0),
    RefMuons_sumet_CentralReg(prefix + "RefMuons_sumet_CentralReg",0),
    RefMuons_phi_CentralReg(prefix + "RefMuons_phi_CentralReg",0),
    RefMuons_etx_EndcapRegion(prefix + "RefMuons_etx_EndcapRegion",0),
    RefMuons_ety_EndcapRegion(prefix + "RefMuons_ety_EndcapRegion",0),
    RefMuons_sumet_EndcapRegion(prefix + "RefMuons_sumet_EndcapRegion",0),
    RefMuons_phi_EndcapRegion(prefix + "RefMuons_phi_EndcapRegion",0),
    RefMuons_etx_ForwardReg(prefix + "RefMuons_etx_ForwardReg",0),
    RefMuons_ety_ForwardReg(prefix + "RefMuons_ety_ForwardReg",0),
    RefMuons_sumet_ForwardReg(prefix + "RefMuons_sumet_ForwardReg",0),
    RefMuons_phi_ForwardReg(prefix + "RefMuons_phi_ForwardReg",0),
    RefGamma_etx(prefix + "RefGamma_etx",0),
    RefGamma_ety(prefix + "RefGamma_ety",0),
    RefGamma_phi(prefix + "RefGamma_phi",0),
    RefGamma_et(prefix + "RefGamma_et",0),
    RefGamma_sumet(prefix + "RefGamma_sumet",0),
    RefGamma_etx_CentralReg(prefix + "RefGamma_etx_CentralReg",0),
    RefGamma_ety_CentralReg(prefix + "RefGamma_ety_CentralReg",0),
    RefGamma_sumet_CentralReg(prefix + "RefGamma_sumet_CentralReg",0),
    RefGamma_phi_CentralReg(prefix + "RefGamma_phi_CentralReg",0),
    RefGamma_etx_EndcapRegion(prefix + "RefGamma_etx_EndcapRegion",0),
    RefGamma_ety_EndcapRegion(prefix + "RefGamma_ety_EndcapRegion",0),
    RefGamma_sumet_EndcapRegion(prefix + "RefGamma_sumet_EndcapRegion",0),
    RefGamma_phi_EndcapRegion(prefix + "RefGamma_phi_EndcapRegion",0),
    RefGamma_etx_ForwardReg(prefix + "RefGamma_etx_ForwardReg",0),
    RefGamma_ety_ForwardReg(prefix + "RefGamma_ety_ForwardReg",0),
    RefGamma_sumet_ForwardReg(prefix + "RefGamma_sumet_ForwardReg",0),
    RefGamma_phi_ForwardReg(prefix + "RefGamma_phi_ForwardReg",0),
    RefTau_etx(prefix + "RefTau_etx",0),
    RefTau_ety(prefix + "RefTau_ety",0),
    RefTau_phi(prefix + "RefTau_phi",0),
    RefTau_et(prefix + "RefTau_et",0),
    RefTau_sumet(prefix + "RefTau_sumet",0),
    RefTau_etx_CentralReg(prefix + "RefTau_etx_CentralReg",0),
    RefTau_ety_CentralReg(prefix + "RefTau_ety_CentralReg",0),
    RefTau_sumet_CentralReg(prefix + "RefTau_sumet_CentralReg",0),
    RefTau_phi_CentralReg(prefix + "RefTau_phi_CentralReg",0),
    RefTau_etx_EndcapRegion(prefix + "RefTau_etx_EndcapRegion",0),
    RefTau_ety_EndcapRegion(prefix + "RefTau_ety_EndcapRegion",0),
    RefTau_sumet_EndcapRegion(prefix + "RefTau_sumet_EndcapRegion",0),
    RefTau_phi_EndcapRegion(prefix + "RefTau_phi_EndcapRegion",0),
    RefTau_etx_ForwardReg(prefix + "RefTau_etx_ForwardReg",0),
    RefTau_ety_ForwardReg(prefix + "RefTau_ety_ForwardReg",0),
    RefTau_sumet_ForwardReg(prefix + "RefTau_sumet_ForwardReg",0),
    RefTau_phi_ForwardReg(prefix + "RefTau_phi_ForwardReg",0),
    RefFinal_em_etx(prefix + "RefFinal_em_etx",0),
    RefFinal_em_ety(prefix + "RefFinal_em_ety",0),
    RefFinal_em_phi(prefix + "RefFinal_em_phi",0),
    RefFinal_em_et(prefix + "RefFinal_em_et",0),
    RefFinal_em_sumet(prefix + "RefFinal_em_sumet",0),
    RefFinal_em_etx_CentralReg(prefix + "RefFinal_em_etx_CentralReg",0),
    RefFinal_em_ety_CentralReg(prefix + "RefFinal_em_ety_CentralReg",0),
    RefFinal_em_sumet_CentralReg(prefix + "RefFinal_em_sumet_CentralReg",0),
    RefFinal_em_phi_CentralReg(prefix + "RefFinal_em_phi_CentralReg",0),
    RefFinal_em_etx_EndcapRegion(prefix + "RefFinal_em_etx_EndcapRegion",0),
    RefFinal_em_ety_EndcapRegion(prefix + "RefFinal_em_ety_EndcapRegion",0),
    RefFinal_em_sumet_EndcapRegion(prefix + "RefFinal_em_sumet_EndcapRegion",0),
    RefFinal_em_phi_EndcapRegion(prefix + "RefFinal_em_phi_EndcapRegion",0),
    RefFinal_em_etx_ForwardReg(prefix + "RefFinal_em_etx_ForwardReg",0),
    RefFinal_em_ety_ForwardReg(prefix + "RefFinal_em_ety_ForwardReg",0),
    RefFinal_em_sumet_ForwardReg(prefix + "RefFinal_em_sumet_ForwardReg",0),
    RefFinal_em_phi_ForwardReg(prefix + "RefFinal_em_phi_ForwardReg",0),
    RefEle_em_etx(prefix + "RefEle_em_etx",0),
    RefEle_em_ety(prefix + "RefEle_em_ety",0),
    RefEle_em_phi(prefix + "RefEle_em_phi",0),
    RefEle_em_et(prefix + "RefEle_em_et",0),
    RefEle_em_sumet(prefix + "RefEle_em_sumet",0),
    RefEle_em_etx_CentralReg(prefix + "RefEle_em_etx_CentralReg",0),
    RefEle_em_ety_CentralReg(prefix + "RefEle_em_ety_CentralReg",0),
    RefEle_em_sumet_CentralReg(prefix + "RefEle_em_sumet_CentralReg",0),
    RefEle_em_phi_CentralReg(prefix + "RefEle_em_phi_CentralReg",0),
    RefEle_em_etx_EndcapRegion(prefix + "RefEle_em_etx_EndcapRegion",0),
    RefEle_em_ety_EndcapRegion(prefix + "RefEle_em_ety_EndcapRegion",0),
    RefEle_em_sumet_EndcapRegion(prefix + "RefEle_em_sumet_EndcapRegion",0),
    RefEle_em_phi_EndcapRegion(prefix + "RefEle_em_phi_EndcapRegion",0),
    RefEle_em_etx_ForwardReg(prefix + "RefEle_em_etx_ForwardReg",0),
    RefEle_em_ety_ForwardReg(prefix + "RefEle_em_ety_ForwardReg",0),
    RefEle_em_sumet_ForwardReg(prefix + "RefEle_em_sumet_ForwardReg",0),
    RefEle_em_phi_ForwardReg(prefix + "RefEle_em_phi_ForwardReg",0),
    RefJet_em_etx(prefix + "RefJet_em_etx",0),
    RefJet_em_ety(prefix + "RefJet_em_ety",0),
    RefJet_em_phi(prefix + "RefJet_em_phi",0),
    RefJet_em_et(prefix + "RefJet_em_et",0),
    RefJet_em_sumet(prefix + "RefJet_em_sumet",0),
    RefJet_em_etx_CentralReg(prefix + "RefJet_em_etx_CentralReg",0),
    RefJet_em_ety_CentralReg(prefix + "RefJet_em_ety_CentralReg",0),
    RefJet_em_sumet_CentralReg(prefix + "RefJet_em_sumet_CentralReg",0),
    RefJet_em_phi_CentralReg(prefix + "RefJet_em_phi_CentralReg",0),
    RefJet_em_etx_EndcapRegion(prefix + "RefJet_em_etx_EndcapRegion",0),
    RefJet_em_ety_EndcapRegion(prefix + "RefJet_em_ety_EndcapRegion",0),
    RefJet_em_sumet_EndcapRegion(prefix + "RefJet_em_sumet_EndcapRegion",0),
    RefJet_em_phi_EndcapRegion(prefix + "RefJet_em_phi_EndcapRegion",0),
    RefJet_em_etx_ForwardReg(prefix + "RefJet_em_etx_ForwardReg",0),
    RefJet_em_ety_ForwardReg(prefix + "RefJet_em_ety_ForwardReg",0),
    RefJet_em_sumet_ForwardReg(prefix + "RefJet_em_sumet_ForwardReg",0),
    RefJet_em_phi_ForwardReg(prefix + "RefJet_em_phi_ForwardReg",0),
    RefMuon_em_etx(prefix + "RefMuon_em_etx",0),
    RefMuon_em_ety(prefix + "RefMuon_em_ety",0),
    RefMuon_em_phi(prefix + "RefMuon_em_phi",0),
    RefMuon_em_et(prefix + "RefMuon_em_et",0),
    RefMuon_em_sumet(prefix + "RefMuon_em_sumet",0),
    RefMuon_em_etx_CentralReg(prefix + "RefMuon_em_etx_CentralReg",0),
    RefMuon_em_ety_CentralReg(prefix + "RefMuon_em_ety_CentralReg",0),
    RefMuon_em_sumet_CentralReg(prefix + "RefMuon_em_sumet_CentralReg",0),
    RefMuon_em_phi_CentralReg(prefix + "RefMuon_em_phi_CentralReg",0),
    RefMuon_em_etx_EndcapRegion(prefix + "RefMuon_em_etx_EndcapRegion",0),
    RefMuon_em_ety_EndcapRegion(prefix + "RefMuon_em_ety_EndcapRegion",0),
    RefMuon_em_sumet_EndcapRegion(prefix + "RefMuon_em_sumet_EndcapRegion",0),
    RefMuon_em_phi_EndcapRegion(prefix + "RefMuon_em_phi_EndcapRegion",0),
    RefMuon_em_etx_ForwardReg(prefix + "RefMuon_em_etx_ForwardReg",0),
    RefMuon_em_ety_ForwardReg(prefix + "RefMuon_em_ety_ForwardReg",0),
    RefMuon_em_sumet_ForwardReg(prefix + "RefMuon_em_sumet_ForwardReg",0),
    RefMuon_em_phi_ForwardReg(prefix + "RefMuon_em_phi_ForwardReg",0),
    RefMuon_Track_em_etx(prefix + "RefMuon_Track_em_etx",0),
    RefMuon_Track_em_ety(prefix + "RefMuon_Track_em_ety",0),
    RefMuon_Track_em_phi(prefix + "RefMuon_Track_em_phi",0),
    RefMuon_Track_em_et(prefix + "RefMuon_Track_em_et",0),
    RefMuon_Track_em_sumet(prefix + "RefMuon_Track_em_sumet",0),
    RefMuon_Track_em_etx_CentralReg(prefix + "RefMuon_Track_em_etx_CentralReg",0),
    RefMuon_Track_em_ety_CentralReg(prefix + "RefMuon_Track_em_ety_CentralReg",0),
    RefMuon_Track_em_sumet_CentralReg(prefix + "RefMuon_Track_em_sumet_CentralReg",0),
    RefMuon_Track_em_phi_CentralReg(prefix + "RefMuon_Track_em_phi_CentralReg",0),
    RefMuon_Track_em_etx_EndcapRegion(prefix + "RefMuon_Track_em_etx_EndcapRegion",0),
    RefMuon_Track_em_ety_EndcapRegion(prefix + "RefMuon_Track_em_ety_EndcapRegion",0),
    RefMuon_Track_em_sumet_EndcapRegion(prefix + "RefMuon_Track_em_sumet_EndcapRegion",0),
    RefMuon_Track_em_phi_EndcapRegion(prefix + "RefMuon_Track_em_phi_EndcapRegion",0),
    RefMuon_Track_em_etx_ForwardReg(prefix + "RefMuon_Track_em_etx_ForwardReg",0),
    RefMuon_Track_em_ety_ForwardReg(prefix + "RefMuon_Track_em_ety_ForwardReg",0),
    RefMuon_Track_em_sumet_ForwardReg(prefix + "RefMuon_Track_em_sumet_ForwardReg",0),
    RefMuon_Track_em_phi_ForwardReg(prefix + "RefMuon_Track_em_phi_ForwardReg",0),
    RefGamma_em_etx(prefix + "RefGamma_em_etx",0),
    RefGamma_em_ety(prefix + "RefGamma_em_ety",0),
    RefGamma_em_phi(prefix + "RefGamma_em_phi",0),
    RefGamma_em_et(prefix + "RefGamma_em_et",0),
    RefGamma_em_sumet(prefix + "RefGamma_em_sumet",0),
    RefGamma_em_etx_CentralReg(prefix + "RefGamma_em_etx_CentralReg",0),
    RefGamma_em_ety_CentralReg(prefix + "RefGamma_em_ety_CentralReg",0),
    RefGamma_em_sumet_CentralReg(prefix + "RefGamma_em_sumet_CentralReg",0),
    RefGamma_em_phi_CentralReg(prefix + "RefGamma_em_phi_CentralReg",0),
    RefGamma_em_etx_EndcapRegion(prefix + "RefGamma_em_etx_EndcapRegion",0),
    RefGamma_em_ety_EndcapRegion(prefix + "RefGamma_em_ety_EndcapRegion",0),
    RefGamma_em_sumet_EndcapRegion(prefix + "RefGamma_em_sumet_EndcapRegion",0),
    RefGamma_em_phi_EndcapRegion(prefix + "RefGamma_em_phi_EndcapRegion",0),
    RefGamma_em_etx_ForwardReg(prefix + "RefGamma_em_etx_ForwardReg",0),
    RefGamma_em_ety_ForwardReg(prefix + "RefGamma_em_ety_ForwardReg",0),
    RefGamma_em_sumet_ForwardReg(prefix + "RefGamma_em_sumet_ForwardReg",0),
    RefGamma_em_phi_ForwardReg(prefix + "RefGamma_em_phi_ForwardReg",0),
    RefTau_em_etx(prefix + "RefTau_em_etx",0),
    RefTau_em_ety(prefix + "RefTau_em_ety",0),
    RefTau_em_phi(prefix + "RefTau_em_phi",0),
    RefTau_em_et(prefix + "RefTau_em_et",0),
    RefTau_em_sumet(prefix + "RefTau_em_sumet",0),
    RefTau_em_etx_CentralReg(prefix + "RefTau_em_etx_CentralReg",0),
    RefTau_em_ety_CentralReg(prefix + "RefTau_em_ety_CentralReg",0),
    RefTau_em_sumet_CentralReg(prefix + "RefTau_em_sumet_CentralReg",0),
    RefTau_em_phi_CentralReg(prefix + "RefTau_em_phi_CentralReg",0),
    RefTau_em_etx_EndcapRegion(prefix + "RefTau_em_etx_EndcapRegion",0),
    RefTau_em_ety_EndcapRegion(prefix + "RefTau_em_ety_EndcapRegion",0),
    RefTau_em_sumet_EndcapRegion(prefix + "RefTau_em_sumet_EndcapRegion",0),
    RefTau_em_phi_EndcapRegion(prefix + "RefTau_em_phi_EndcapRegion",0),
    RefTau_em_etx_ForwardReg(prefix + "RefTau_em_etx_ForwardReg",0),
    RefTau_em_ety_ForwardReg(prefix + "RefTau_em_ety_ForwardReg",0),
    RefTau_em_sumet_ForwardReg(prefix + "RefTau_em_sumet_ForwardReg",0),
    RefTau_em_phi_ForwardReg(prefix + "RefTau_em_phi_ForwardReg",0),
    RefJet_JVF_etx(prefix + "RefJet_JVF_etx",0),
    RefJet_JVF_ety(prefix + "RefJet_JVF_ety",0),
    RefJet_JVF_phi(prefix + "RefJet_JVF_phi",0),
    RefJet_JVF_et(prefix + "RefJet_JVF_et",0),
    RefJet_JVF_sumet(prefix + "RefJet_JVF_sumet",0),
    RefJet_JVF_etx_CentralReg(prefix + "RefJet_JVF_etx_CentralReg",0),
    RefJet_JVF_ety_CentralReg(prefix + "RefJet_JVF_ety_CentralReg",0),
    RefJet_JVF_sumet_CentralReg(prefix + "RefJet_JVF_sumet_CentralReg",0),
    RefJet_JVF_phi_CentralReg(prefix + "RefJet_JVF_phi_CentralReg",0),
    RefJet_JVF_etx_EndcapRegion(prefix + "RefJet_JVF_etx_EndcapRegion",0),
    RefJet_JVF_ety_EndcapRegion(prefix + "RefJet_JVF_ety_EndcapRegion",0),
    RefJet_JVF_sumet_EndcapRegion(prefix + "RefJet_JVF_sumet_EndcapRegion",0),
    RefJet_JVF_phi_EndcapRegion(prefix + "RefJet_JVF_phi_EndcapRegion",0),
    RefJet_JVF_etx_ForwardReg(prefix + "RefJet_JVF_etx_ForwardReg",0),
    RefJet_JVF_ety_ForwardReg(prefix + "RefJet_JVF_ety_ForwardReg",0),
    RefJet_JVF_sumet_ForwardReg(prefix + "RefJet_JVF_sumet_ForwardReg",0),
    RefJet_JVF_phi_ForwardReg(prefix + "RefJet_JVF_phi_ForwardReg",0),
    RefJet_JVFCut_etx(prefix + "RefJet_JVFCut_etx",0),
    RefJet_JVFCut_ety(prefix + "RefJet_JVFCut_ety",0),
    RefJet_JVFCut_phi(prefix + "RefJet_JVFCut_phi",0),
    RefJet_JVFCut_et(prefix + "RefJet_JVFCut_et",0),
    RefJet_JVFCut_sumet(prefix + "RefJet_JVFCut_sumet",0),
    RefJet_JVFCut_etx_CentralReg(prefix + "RefJet_JVFCut_etx_CentralReg",0),
    RefJet_JVFCut_ety_CentralReg(prefix + "RefJet_JVFCut_ety_CentralReg",0),
    RefJet_JVFCut_sumet_CentralReg(prefix + "RefJet_JVFCut_sumet_CentralReg",0),
    RefJet_JVFCut_phi_CentralReg(prefix + "RefJet_JVFCut_phi_CentralReg",0),
    RefJet_JVFCut_etx_EndcapRegion(prefix + "RefJet_JVFCut_etx_EndcapRegion",0),
    RefJet_JVFCut_ety_EndcapRegion(prefix + "RefJet_JVFCut_ety_EndcapRegion",0),
    RefJet_JVFCut_sumet_EndcapRegion(prefix + "RefJet_JVFCut_sumet_EndcapRegion",0),
    RefJet_JVFCut_phi_EndcapRegion(prefix + "RefJet_JVFCut_phi_EndcapRegion",0),
    RefJet_JVFCut_etx_ForwardReg(prefix + "RefJet_JVFCut_etx_ForwardReg",0),
    RefJet_JVFCut_ety_ForwardReg(prefix + "RefJet_JVFCut_ety_ForwardReg",0),
    RefJet_JVFCut_sumet_ForwardReg(prefix + "RefJet_JVFCut_sumet_ForwardReg",0),
    RefJet_JVFCut_phi_ForwardReg(prefix + "RefJet_JVFCut_phi_ForwardReg",0),
    CellOut_Eflow_STVF_etx(prefix + "CellOut_Eflow_STVF_etx",0),
    CellOut_Eflow_STVF_ety(prefix + "CellOut_Eflow_STVF_ety",0),
    CellOut_Eflow_STVF_phi(prefix + "CellOut_Eflow_STVF_phi",0),
    CellOut_Eflow_STVF_et(prefix + "CellOut_Eflow_STVF_et",0),
    CellOut_Eflow_STVF_sumet(prefix + "CellOut_Eflow_STVF_sumet",0),
    CellOut_Eflow_STVF_etx_CentralReg(prefix + "CellOut_Eflow_STVF_etx_CentralReg",0),
    CellOut_Eflow_STVF_ety_CentralReg(prefix + "CellOut_Eflow_STVF_ety_CentralReg",0),
    CellOut_Eflow_STVF_sumet_CentralReg(prefix + "CellOut_Eflow_STVF_sumet_CentralReg",0),
    CellOut_Eflow_STVF_phi_CentralReg(prefix + "CellOut_Eflow_STVF_phi_CentralReg",0),
    CellOut_Eflow_STVF_etx_EndcapRegion(prefix + "CellOut_Eflow_STVF_etx_EndcapRegion",0),
    CellOut_Eflow_STVF_ety_EndcapRegion(prefix + "CellOut_Eflow_STVF_ety_EndcapRegion",0),
    CellOut_Eflow_STVF_sumet_EndcapRegion(prefix + "CellOut_Eflow_STVF_sumet_EndcapRegion",0),
    CellOut_Eflow_STVF_phi_EndcapRegion(prefix + "CellOut_Eflow_STVF_phi_EndcapRegion",0),
    CellOut_Eflow_STVF_etx_ForwardReg(prefix + "CellOut_Eflow_STVF_etx_ForwardReg",0),
    CellOut_Eflow_STVF_ety_ForwardReg(prefix + "CellOut_Eflow_STVF_ety_ForwardReg",0),
    CellOut_Eflow_STVF_sumet_ForwardReg(prefix + "CellOut_Eflow_STVF_sumet_ForwardReg",0),
    CellOut_Eflow_STVF_phi_ForwardReg(prefix + "CellOut_Eflow_STVF_phi_ForwardReg",0),
    CellOut_Eflow_JetArea_etx(prefix + "CellOut_Eflow_JetArea_etx",0),
    CellOut_Eflow_JetArea_ety(prefix + "CellOut_Eflow_JetArea_ety",0),
    CellOut_Eflow_JetArea_phi(prefix + "CellOut_Eflow_JetArea_phi",0),
    CellOut_Eflow_JetArea_et(prefix + "CellOut_Eflow_JetArea_et",0),
    CellOut_Eflow_JetArea_sumet(prefix + "CellOut_Eflow_JetArea_sumet",0),
    CellOut_Eflow_JetArea_etx_CentralReg(prefix + "CellOut_Eflow_JetArea_etx_CentralReg",0),
    CellOut_Eflow_JetArea_ety_CentralReg(prefix + "CellOut_Eflow_JetArea_ety_CentralReg",0),
    CellOut_Eflow_JetArea_sumet_CentralReg(prefix + "CellOut_Eflow_JetArea_sumet_CentralReg",0),
    CellOut_Eflow_JetArea_phi_CentralReg(prefix + "CellOut_Eflow_JetArea_phi_CentralReg",0),
    CellOut_Eflow_JetArea_etx_EndcapRegion(prefix + "CellOut_Eflow_JetArea_etx_EndcapRegion",0),
    CellOut_Eflow_JetArea_ety_EndcapRegion(prefix + "CellOut_Eflow_JetArea_ety_EndcapRegion",0),
    CellOut_Eflow_JetArea_sumet_EndcapRegion(prefix + "CellOut_Eflow_JetArea_sumet_EndcapRegion",0),
    CellOut_Eflow_JetArea_phi_EndcapRegion(prefix + "CellOut_Eflow_JetArea_phi_EndcapRegion",0),
    CellOut_Eflow_JetArea_etx_ForwardReg(prefix + "CellOut_Eflow_JetArea_etx_ForwardReg",0),
    CellOut_Eflow_JetArea_ety_ForwardReg(prefix + "CellOut_Eflow_JetArea_ety_ForwardReg",0),
    CellOut_Eflow_JetArea_sumet_ForwardReg(prefix + "CellOut_Eflow_JetArea_sumet_ForwardReg",0),
    CellOut_Eflow_JetArea_phi_ForwardReg(prefix + "CellOut_Eflow_JetArea_phi_ForwardReg",0),
    CellOut_Eflow_JetAreaJVF_etx(prefix + "CellOut_Eflow_JetAreaJVF_etx",0),
    CellOut_Eflow_JetAreaJVF_ety(prefix + "CellOut_Eflow_JetAreaJVF_ety",0),
    CellOut_Eflow_JetAreaJVF_phi(prefix + "CellOut_Eflow_JetAreaJVF_phi",0),
    CellOut_Eflow_JetAreaJVF_et(prefix + "CellOut_Eflow_JetAreaJVF_et",0),
    CellOut_Eflow_JetAreaJVF_sumet(prefix + "CellOut_Eflow_JetAreaJVF_sumet",0),
    CellOut_Eflow_JetAreaJVF_etx_CentralReg(prefix + "CellOut_Eflow_JetAreaJVF_etx_CentralReg",0),
    CellOut_Eflow_JetAreaJVF_ety_CentralReg(prefix + "CellOut_Eflow_JetAreaJVF_ety_CentralReg",0),
    CellOut_Eflow_JetAreaJVF_sumet_CentralReg(prefix + "CellOut_Eflow_JetAreaJVF_sumet_CentralReg",0),
    CellOut_Eflow_JetAreaJVF_phi_CentralReg(prefix + "CellOut_Eflow_JetAreaJVF_phi_CentralReg",0),
    CellOut_Eflow_JetAreaJVF_etx_EndcapRegion(prefix + "CellOut_Eflow_JetAreaJVF_etx_EndcapRegion",0),
    CellOut_Eflow_JetAreaJVF_ety_EndcapRegion(prefix + "CellOut_Eflow_JetAreaJVF_ety_EndcapRegion",0),
    CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion(prefix + "CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion",0),
    CellOut_Eflow_JetAreaJVF_phi_EndcapRegion(prefix + "CellOut_Eflow_JetAreaJVF_phi_EndcapRegion",0),
    CellOut_Eflow_JetAreaJVF_etx_ForwardReg(prefix + "CellOut_Eflow_JetAreaJVF_etx_ForwardReg",0),
    CellOut_Eflow_JetAreaJVF_ety_ForwardReg(prefix + "CellOut_Eflow_JetAreaJVF_ety_ForwardReg",0),
    CellOut_Eflow_JetAreaJVF_sumet_ForwardReg(prefix + "CellOut_Eflow_JetAreaJVF_sumet_ForwardReg",0),
    CellOut_Eflow_JetAreaJVF_phi_ForwardReg(prefix + "CellOut_Eflow_JetAreaJVF_phi_ForwardReg",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_etx(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_etx",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_ety(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_ety",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_phi(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_phi",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_et(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_et",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_sumet",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg",0),
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg(prefix + "CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg",0),
    RefFinal_STVF_etx(prefix + "RefFinal_STVF_etx",0),
    RefFinal_STVF_ety(prefix + "RefFinal_STVF_ety",0),
    RefFinal_STVF_phi(prefix + "RefFinal_STVF_phi",0),
    RefFinal_STVF_et(prefix + "RefFinal_STVF_et",0),
    RefFinal_STVF_sumet(prefix + "RefFinal_STVF_sumet",0),
    RefFinal_STVF_etx_CentralReg(prefix + "RefFinal_STVF_etx_CentralReg",0),
    RefFinal_STVF_ety_CentralReg(prefix + "RefFinal_STVF_ety_CentralReg",0),
    RefFinal_STVF_sumet_CentralReg(prefix + "RefFinal_STVF_sumet_CentralReg",0),
    RefFinal_STVF_phi_CentralReg(prefix + "RefFinal_STVF_phi_CentralReg",0),
    RefFinal_STVF_etx_EndcapRegion(prefix + "RefFinal_STVF_etx_EndcapRegion",0),
    RefFinal_STVF_ety_EndcapRegion(prefix + "RefFinal_STVF_ety_EndcapRegion",0),
    RefFinal_STVF_sumet_EndcapRegion(prefix + "RefFinal_STVF_sumet_EndcapRegion",0),
    RefFinal_STVF_phi_EndcapRegion(prefix + "RefFinal_STVF_phi_EndcapRegion",0),
    RefFinal_STVF_etx_ForwardReg(prefix + "RefFinal_STVF_etx_ForwardReg",0),
    RefFinal_STVF_ety_ForwardReg(prefix + "RefFinal_STVF_ety_ForwardReg",0),
    RefFinal_STVF_sumet_ForwardReg(prefix + "RefFinal_STVF_sumet_ForwardReg",0),
    RefFinal_STVF_phi_ForwardReg(prefix + "RefFinal_STVF_phi_ForwardReg",0),
    CellOut_Eflow_etx(prefix + "CellOut_Eflow_etx",0),
    CellOut_Eflow_ety(prefix + "CellOut_Eflow_ety",0),
    CellOut_Eflow_phi(prefix + "CellOut_Eflow_phi",0),
    CellOut_Eflow_et(prefix + "CellOut_Eflow_et",0),
    CellOut_Eflow_sumet(prefix + "CellOut_Eflow_sumet",0),
    CellOut_Eflow_etx_CentralReg(prefix + "CellOut_Eflow_etx_CentralReg",0),
    CellOut_Eflow_ety_CentralReg(prefix + "CellOut_Eflow_ety_CentralReg",0),
    CellOut_Eflow_sumet_CentralReg(prefix + "CellOut_Eflow_sumet_CentralReg",0),
    CellOut_Eflow_phi_CentralReg(prefix + "CellOut_Eflow_phi_CentralReg",0),
    CellOut_Eflow_etx_EndcapRegion(prefix + "CellOut_Eflow_etx_EndcapRegion",0),
    CellOut_Eflow_ety_EndcapRegion(prefix + "CellOut_Eflow_ety_EndcapRegion",0),
    CellOut_Eflow_sumet_EndcapRegion(prefix + "CellOut_Eflow_sumet_EndcapRegion",0),
    CellOut_Eflow_phi_EndcapRegion(prefix + "CellOut_Eflow_phi_EndcapRegion",0),
    CellOut_Eflow_etx_ForwardReg(prefix + "CellOut_Eflow_etx_ForwardReg",0),
    CellOut_Eflow_ety_ForwardReg(prefix + "CellOut_Eflow_ety_ForwardReg",0),
    CellOut_Eflow_sumet_ForwardReg(prefix + "CellOut_Eflow_sumet_ForwardReg",0),
    CellOut_Eflow_phi_ForwardReg(prefix + "CellOut_Eflow_phi_ForwardReg",0),
    CellOut_Eflow_Muid_etx(prefix + "CellOut_Eflow_Muid_etx",0),
    CellOut_Eflow_Muid_ety(prefix + "CellOut_Eflow_Muid_ety",0),
    CellOut_Eflow_Muid_phi(prefix + "CellOut_Eflow_Muid_phi",0),
    CellOut_Eflow_Muid_et(prefix + "CellOut_Eflow_Muid_et",0),
    CellOut_Eflow_Muid_sumet(prefix + "CellOut_Eflow_Muid_sumet",0),
    CellOut_Eflow_Muid_etx_CentralReg(prefix + "CellOut_Eflow_Muid_etx_CentralReg",0),
    CellOut_Eflow_Muid_ety_CentralReg(prefix + "CellOut_Eflow_Muid_ety_CentralReg",0),
    CellOut_Eflow_Muid_sumet_CentralReg(prefix + "CellOut_Eflow_Muid_sumet_CentralReg",0),
    CellOut_Eflow_Muid_phi_CentralReg(prefix + "CellOut_Eflow_Muid_phi_CentralReg",0),
    CellOut_Eflow_Muid_etx_EndcapRegion(prefix + "CellOut_Eflow_Muid_etx_EndcapRegion",0),
    CellOut_Eflow_Muid_ety_EndcapRegion(prefix + "CellOut_Eflow_Muid_ety_EndcapRegion",0),
    CellOut_Eflow_Muid_sumet_EndcapRegion(prefix + "CellOut_Eflow_Muid_sumet_EndcapRegion",0),
    CellOut_Eflow_Muid_phi_EndcapRegion(prefix + "CellOut_Eflow_Muid_phi_EndcapRegion",0),
    CellOut_Eflow_Muid_etx_ForwardReg(prefix + "CellOut_Eflow_Muid_etx_ForwardReg",0),
    CellOut_Eflow_Muid_ety_ForwardReg(prefix + "CellOut_Eflow_Muid_ety_ForwardReg",0),
    CellOut_Eflow_Muid_sumet_ForwardReg(prefix + "CellOut_Eflow_Muid_sumet_ForwardReg",0),
    CellOut_Eflow_Muid_phi_ForwardReg(prefix + "CellOut_Eflow_Muid_phi_ForwardReg",0),
    CellOut_Eflow_Muons_etx(prefix + "CellOut_Eflow_Muons_etx",0),
    CellOut_Eflow_Muons_ety(prefix + "CellOut_Eflow_Muons_ety",0),
    CellOut_Eflow_Muons_phi(prefix + "CellOut_Eflow_Muons_phi",0),
    CellOut_Eflow_Muons_et(prefix + "CellOut_Eflow_Muons_et",0),
    CellOut_Eflow_Muons_sumet(prefix + "CellOut_Eflow_Muons_sumet",0),
    CellOut_Eflow_Muons_etx_CentralReg(prefix + "CellOut_Eflow_Muons_etx_CentralReg",0),
    CellOut_Eflow_Muons_ety_CentralReg(prefix + "CellOut_Eflow_Muons_ety_CentralReg",0),
    CellOut_Eflow_Muons_sumet_CentralReg(prefix + "CellOut_Eflow_Muons_sumet_CentralReg",0),
    CellOut_Eflow_Muons_phi_CentralReg(prefix + "CellOut_Eflow_Muons_phi_CentralReg",0),
    CellOut_Eflow_Muons_etx_EndcapRegion(prefix + "CellOut_Eflow_Muons_etx_EndcapRegion",0),
    CellOut_Eflow_Muons_ety_EndcapRegion(prefix + "CellOut_Eflow_Muons_ety_EndcapRegion",0),
    CellOut_Eflow_Muons_sumet_EndcapRegion(prefix + "CellOut_Eflow_Muons_sumet_EndcapRegion",0),
    CellOut_Eflow_Muons_phi_EndcapRegion(prefix + "CellOut_Eflow_Muons_phi_EndcapRegion",0),
    CellOut_Eflow_Muons_etx_ForwardReg(prefix + "CellOut_Eflow_Muons_etx_ForwardReg",0),
    CellOut_Eflow_Muons_ety_ForwardReg(prefix + "CellOut_Eflow_Muons_ety_ForwardReg",0),
    CellOut_Eflow_Muons_sumet_ForwardReg(prefix + "CellOut_Eflow_Muons_sumet_ForwardReg",0),
    CellOut_Eflow_Muons_phi_ForwardReg(prefix + "CellOut_Eflow_Muons_phi_ForwardReg",0),
    RefMuon_Track_etx(prefix + "RefMuon_Track_etx",0),
    RefMuon_Track_ety(prefix + "RefMuon_Track_ety",0),
    RefMuon_Track_phi(prefix + "RefMuon_Track_phi",0),
    RefMuon_Track_et(prefix + "RefMuon_Track_et",0),
    RefMuon_Track_sumet(prefix + "RefMuon_Track_sumet",0),
    RefMuon_Track_etx_CentralReg(prefix + "RefMuon_Track_etx_CentralReg",0),
    RefMuon_Track_ety_CentralReg(prefix + "RefMuon_Track_ety_CentralReg",0),
    RefMuon_Track_sumet_CentralReg(prefix + "RefMuon_Track_sumet_CentralReg",0),
    RefMuon_Track_phi_CentralReg(prefix + "RefMuon_Track_phi_CentralReg",0),
    RefMuon_Track_etx_EndcapRegion(prefix + "RefMuon_Track_etx_EndcapRegion",0),
    RefMuon_Track_ety_EndcapRegion(prefix + "RefMuon_Track_ety_EndcapRegion",0),
    RefMuon_Track_sumet_EndcapRegion(prefix + "RefMuon_Track_sumet_EndcapRegion",0),
    RefMuon_Track_phi_EndcapRegion(prefix + "RefMuon_Track_phi_EndcapRegion",0),
    RefMuon_Track_etx_ForwardReg(prefix + "RefMuon_Track_etx_ForwardReg",0),
    RefMuon_Track_ety_ForwardReg(prefix + "RefMuon_Track_ety_ForwardReg",0),
    RefMuon_Track_sumet_ForwardReg(prefix + "RefMuon_Track_sumet_ForwardReg",0),
    RefMuon_Track_phi_ForwardReg(prefix + "RefMuon_Track_phi_ForwardReg",0),
    RefMuon_Track_Staco_etx(prefix + "RefMuon_Track_Staco_etx",0),
    RefMuon_Track_Staco_ety(prefix + "RefMuon_Track_Staco_ety",0),
    RefMuon_Track_Staco_phi(prefix + "RefMuon_Track_Staco_phi",0),
    RefMuon_Track_Staco_et(prefix + "RefMuon_Track_Staco_et",0),
    RefMuon_Track_Staco_sumet(prefix + "RefMuon_Track_Staco_sumet",0),
    RefMuon_Track_Staco_etx_CentralReg(prefix + "RefMuon_Track_Staco_etx_CentralReg",0),
    RefMuon_Track_Staco_ety_CentralReg(prefix + "RefMuon_Track_Staco_ety_CentralReg",0),
    RefMuon_Track_Staco_sumet_CentralReg(prefix + "RefMuon_Track_Staco_sumet_CentralReg",0),
    RefMuon_Track_Staco_phi_CentralReg(prefix + "RefMuon_Track_Staco_phi_CentralReg",0),
    RefMuon_Track_Staco_etx_EndcapRegion(prefix + "RefMuon_Track_Staco_etx_EndcapRegion",0),
    RefMuon_Track_Staco_ety_EndcapRegion(prefix + "RefMuon_Track_Staco_ety_EndcapRegion",0),
    RefMuon_Track_Staco_sumet_EndcapRegion(prefix + "RefMuon_Track_Staco_sumet_EndcapRegion",0),
    RefMuon_Track_Staco_phi_EndcapRegion(prefix + "RefMuon_Track_Staco_phi_EndcapRegion",0),
    RefMuon_Track_Staco_etx_ForwardReg(prefix + "RefMuon_Track_Staco_etx_ForwardReg",0),
    RefMuon_Track_Staco_ety_ForwardReg(prefix + "RefMuon_Track_Staco_ety_ForwardReg",0),
    RefMuon_Track_Staco_sumet_ForwardReg(prefix + "RefMuon_Track_Staco_sumet_ForwardReg",0),
    RefMuon_Track_Staco_phi_ForwardReg(prefix + "RefMuon_Track_Staco_phi_ForwardReg",0),
    RefMuon_Track_Muid_etx(prefix + "RefMuon_Track_Muid_etx",0),
    RefMuon_Track_Muid_ety(prefix + "RefMuon_Track_Muid_ety",0),
    RefMuon_Track_Muid_phi(prefix + "RefMuon_Track_Muid_phi",0),
    RefMuon_Track_Muid_et(prefix + "RefMuon_Track_Muid_et",0),
    RefMuon_Track_Muid_sumet(prefix + "RefMuon_Track_Muid_sumet",0),
    RefMuon_Track_Muid_etx_CentralReg(prefix + "RefMuon_Track_Muid_etx_CentralReg",0),
    RefMuon_Track_Muid_ety_CentralReg(prefix + "RefMuon_Track_Muid_ety_CentralReg",0),
    RefMuon_Track_Muid_sumet_CentralReg(prefix + "RefMuon_Track_Muid_sumet_CentralReg",0),
    RefMuon_Track_Muid_phi_CentralReg(prefix + "RefMuon_Track_Muid_phi_CentralReg",0),
    RefMuon_Track_Muid_etx_EndcapRegion(prefix + "RefMuon_Track_Muid_etx_EndcapRegion",0),
    RefMuon_Track_Muid_ety_EndcapRegion(prefix + "RefMuon_Track_Muid_ety_EndcapRegion",0),
    RefMuon_Track_Muid_sumet_EndcapRegion(prefix + "RefMuon_Track_Muid_sumet_EndcapRegion",0),
    RefMuon_Track_Muid_phi_EndcapRegion(prefix + "RefMuon_Track_Muid_phi_EndcapRegion",0),
    RefMuon_Track_Muid_etx_ForwardReg(prefix + "RefMuon_Track_Muid_etx_ForwardReg",0),
    RefMuon_Track_Muid_ety_ForwardReg(prefix + "RefMuon_Track_Muid_ety_ForwardReg",0),
    RefMuon_Track_Muid_sumet_ForwardReg(prefix + "RefMuon_Track_Muid_sumet_ForwardReg",0),
    RefMuon_Track_Muid_phi_ForwardReg(prefix + "RefMuon_Track_Muid_phi_ForwardReg",0),
    RefMuons_Track_etx(prefix + "RefMuons_Track_etx",0),
    RefMuons_Track_ety(prefix + "RefMuons_Track_ety",0),
    RefMuons_Track_phi(prefix + "RefMuons_Track_phi",0),
    RefMuons_Track_et(prefix + "RefMuons_Track_et",0),
    RefMuons_Track_sumet(prefix + "RefMuons_Track_sumet",0),
    RefMuons_Track_etx_CentralReg(prefix + "RefMuons_Track_etx_CentralReg",0),
    RefMuons_Track_ety_CentralReg(prefix + "RefMuons_Track_ety_CentralReg",0),
    RefMuons_Track_sumet_CentralReg(prefix + "RefMuons_Track_sumet_CentralReg",0),
    RefMuons_Track_phi_CentralReg(prefix + "RefMuons_Track_phi_CentralReg",0),
    RefMuons_Track_etx_EndcapRegion(prefix + "RefMuons_Track_etx_EndcapRegion",0),
    RefMuons_Track_ety_EndcapRegion(prefix + "RefMuons_Track_ety_EndcapRegion",0),
    RefMuons_Track_sumet_EndcapRegion(prefix + "RefMuons_Track_sumet_EndcapRegion",0),
    RefMuons_Track_phi_EndcapRegion(prefix + "RefMuons_Track_phi_EndcapRegion",0),
    RefMuons_Track_etx_ForwardReg(prefix + "RefMuons_Track_etx_ForwardReg",0),
    RefMuons_Track_ety_ForwardReg(prefix + "RefMuons_Track_ety_ForwardReg",0),
    RefMuons_Track_sumet_ForwardReg(prefix + "RefMuons_Track_sumet_ForwardReg",0),
    RefMuons_Track_phi_ForwardReg(prefix + "RefMuons_Track_phi_ForwardReg",0),
    RefFinal_BDTMedium_etx(prefix + "RefFinal_BDTMedium_etx",0),
    RefFinal_BDTMedium_ety(prefix + "RefFinal_BDTMedium_ety",0),
    RefFinal_BDTMedium_phi(prefix + "RefFinal_BDTMedium_phi",0),
    RefFinal_BDTMedium_et(prefix + "RefFinal_BDTMedium_et",0),
    RefFinal_BDTMedium_sumet(prefix + "RefFinal_BDTMedium_sumet",0),
    RefFinal_BDTMedium_etx_CentralReg(prefix + "RefFinal_BDTMedium_etx_CentralReg",0),
    RefFinal_BDTMedium_ety_CentralReg(prefix + "RefFinal_BDTMedium_ety_CentralReg",0),
    RefFinal_BDTMedium_sumet_CentralReg(prefix + "RefFinal_BDTMedium_sumet_CentralReg",0),
    RefFinal_BDTMedium_phi_CentralReg(prefix + "RefFinal_BDTMedium_phi_CentralReg",0),
    RefFinal_BDTMedium_etx_EndcapRegion(prefix + "RefFinal_BDTMedium_etx_EndcapRegion",0),
    RefFinal_BDTMedium_ety_EndcapRegion(prefix + "RefFinal_BDTMedium_ety_EndcapRegion",0),
    RefFinal_BDTMedium_sumet_EndcapRegion(prefix + "RefFinal_BDTMedium_sumet_EndcapRegion",0),
    RefFinal_BDTMedium_phi_EndcapRegion(prefix + "RefFinal_BDTMedium_phi_EndcapRegion",0),
    RefFinal_BDTMedium_etx_ForwardReg(prefix + "RefFinal_BDTMedium_etx_ForwardReg",0),
    RefFinal_BDTMedium_ety_ForwardReg(prefix + "RefFinal_BDTMedium_ety_ForwardReg",0),
    RefFinal_BDTMedium_sumet_ForwardReg(prefix + "RefFinal_BDTMedium_sumet_ForwardReg",0),
    RefFinal_BDTMedium_phi_ForwardReg(prefix + "RefFinal_BDTMedium_phi_ForwardReg",0),
    RefGamma_BDTMedium_etx(prefix + "RefGamma_BDTMedium_etx",0),
    RefGamma_BDTMedium_ety(prefix + "RefGamma_BDTMedium_ety",0),
    RefGamma_BDTMedium_phi(prefix + "RefGamma_BDTMedium_phi",0),
    RefGamma_BDTMedium_et(prefix + "RefGamma_BDTMedium_et",0),
    RefGamma_BDTMedium_sumet(prefix + "RefGamma_BDTMedium_sumet",0),
    RefGamma_BDTMedium_etx_CentralReg(prefix + "RefGamma_BDTMedium_etx_CentralReg",0),
    RefGamma_BDTMedium_ety_CentralReg(prefix + "RefGamma_BDTMedium_ety_CentralReg",0),
    RefGamma_BDTMedium_sumet_CentralReg(prefix + "RefGamma_BDTMedium_sumet_CentralReg",0),
    RefGamma_BDTMedium_phi_CentralReg(prefix + "RefGamma_BDTMedium_phi_CentralReg",0),
    RefGamma_BDTMedium_etx_EndcapRegion(prefix + "RefGamma_BDTMedium_etx_EndcapRegion",0),
    RefGamma_BDTMedium_ety_EndcapRegion(prefix + "RefGamma_BDTMedium_ety_EndcapRegion",0),
    RefGamma_BDTMedium_sumet_EndcapRegion(prefix + "RefGamma_BDTMedium_sumet_EndcapRegion",0),
    RefGamma_BDTMedium_phi_EndcapRegion(prefix + "RefGamma_BDTMedium_phi_EndcapRegion",0),
    RefGamma_BDTMedium_etx_ForwardReg(prefix + "RefGamma_BDTMedium_etx_ForwardReg",0),
    RefGamma_BDTMedium_ety_ForwardReg(prefix + "RefGamma_BDTMedium_ety_ForwardReg",0),
    RefGamma_BDTMedium_sumet_ForwardReg(prefix + "RefGamma_BDTMedium_sumet_ForwardReg",0),
    RefGamma_BDTMedium_phi_ForwardReg(prefix + "RefGamma_BDTMedium_phi_ForwardReg",0),
    RefEle_BDTMedium_etx(prefix + "RefEle_BDTMedium_etx",0),
    RefEle_BDTMedium_ety(prefix + "RefEle_BDTMedium_ety",0),
    RefEle_BDTMedium_phi(prefix + "RefEle_BDTMedium_phi",0),
    RefEle_BDTMedium_et(prefix + "RefEle_BDTMedium_et",0),
    RefEle_BDTMedium_sumet(prefix + "RefEle_BDTMedium_sumet",0),
    RefEle_BDTMedium_etx_CentralReg(prefix + "RefEle_BDTMedium_etx_CentralReg",0),
    RefEle_BDTMedium_ety_CentralReg(prefix + "RefEle_BDTMedium_ety_CentralReg",0),
    RefEle_BDTMedium_sumet_CentralReg(prefix + "RefEle_BDTMedium_sumet_CentralReg",0),
    RefEle_BDTMedium_phi_CentralReg(prefix + "RefEle_BDTMedium_phi_CentralReg",0),
    RefEle_BDTMedium_etx_EndcapRegion(prefix + "RefEle_BDTMedium_etx_EndcapRegion",0),
    RefEle_BDTMedium_ety_EndcapRegion(prefix + "RefEle_BDTMedium_ety_EndcapRegion",0),
    RefEle_BDTMedium_sumet_EndcapRegion(prefix + "RefEle_BDTMedium_sumet_EndcapRegion",0),
    RefEle_BDTMedium_phi_EndcapRegion(prefix + "RefEle_BDTMedium_phi_EndcapRegion",0),
    RefEle_BDTMedium_etx_ForwardReg(prefix + "RefEle_BDTMedium_etx_ForwardReg",0),
    RefEle_BDTMedium_ety_ForwardReg(prefix + "RefEle_BDTMedium_ety_ForwardReg",0),
    RefEle_BDTMedium_sumet_ForwardReg(prefix + "RefEle_BDTMedium_sumet_ForwardReg",0),
    RefEle_BDTMedium_phi_ForwardReg(prefix + "RefEle_BDTMedium_phi_ForwardReg",0),
    RefTau_BDTMedium_etx(prefix + "RefTau_BDTMedium_etx",0),
    RefTau_BDTMedium_ety(prefix + "RefTau_BDTMedium_ety",0),
    RefTau_BDTMedium_phi(prefix + "RefTau_BDTMedium_phi",0),
    RefTau_BDTMedium_et(prefix + "RefTau_BDTMedium_et",0),
    RefTau_BDTMedium_sumet(prefix + "RefTau_BDTMedium_sumet",0),
    RefTau_BDTMedium_etx_CentralReg(prefix + "RefTau_BDTMedium_etx_CentralReg",0),
    RefTau_BDTMedium_ety_CentralReg(prefix + "RefTau_BDTMedium_ety_CentralReg",0),
    RefTau_BDTMedium_sumet_CentralReg(prefix + "RefTau_BDTMedium_sumet_CentralReg",0),
    RefTau_BDTMedium_phi_CentralReg(prefix + "RefTau_BDTMedium_phi_CentralReg",0),
    RefTau_BDTMedium_etx_EndcapRegion(prefix + "RefTau_BDTMedium_etx_EndcapRegion",0),
    RefTau_BDTMedium_ety_EndcapRegion(prefix + "RefTau_BDTMedium_ety_EndcapRegion",0),
    RefTau_BDTMedium_sumet_EndcapRegion(prefix + "RefTau_BDTMedium_sumet_EndcapRegion",0),
    RefTau_BDTMedium_phi_EndcapRegion(prefix + "RefTau_BDTMedium_phi_EndcapRegion",0),
    RefTau_BDTMedium_etx_ForwardReg(prefix + "RefTau_BDTMedium_etx_ForwardReg",0),
    RefTau_BDTMedium_ety_ForwardReg(prefix + "RefTau_BDTMedium_ety_ForwardReg",0),
    RefTau_BDTMedium_sumet_ForwardReg(prefix + "RefTau_BDTMedium_sumet_ForwardReg",0),
    RefTau_BDTMedium_phi_ForwardReg(prefix + "RefTau_BDTMedium_phi_ForwardReg",0),
    RefJet_BDTMedium_etx(prefix + "RefJet_BDTMedium_etx",0),
    RefJet_BDTMedium_ety(prefix + "RefJet_BDTMedium_ety",0),
    RefJet_BDTMedium_phi(prefix + "RefJet_BDTMedium_phi",0),
    RefJet_BDTMedium_et(prefix + "RefJet_BDTMedium_et",0),
    RefJet_BDTMedium_sumet(prefix + "RefJet_BDTMedium_sumet",0),
    RefJet_BDTMedium_etx_CentralReg(prefix + "RefJet_BDTMedium_etx_CentralReg",0),
    RefJet_BDTMedium_ety_CentralReg(prefix + "RefJet_BDTMedium_ety_CentralReg",0),
    RefJet_BDTMedium_sumet_CentralReg(prefix + "RefJet_BDTMedium_sumet_CentralReg",0),
    RefJet_BDTMedium_phi_CentralReg(prefix + "RefJet_BDTMedium_phi_CentralReg",0),
    RefJet_BDTMedium_etx_EndcapRegion(prefix + "RefJet_BDTMedium_etx_EndcapRegion",0),
    RefJet_BDTMedium_ety_EndcapRegion(prefix + "RefJet_BDTMedium_ety_EndcapRegion",0),
    RefJet_BDTMedium_sumet_EndcapRegion(prefix + "RefJet_BDTMedium_sumet_EndcapRegion",0),
    RefJet_BDTMedium_phi_EndcapRegion(prefix + "RefJet_BDTMedium_phi_EndcapRegion",0),
    RefJet_BDTMedium_etx_ForwardReg(prefix + "RefJet_BDTMedium_etx_ForwardReg",0),
    RefJet_BDTMedium_ety_ForwardReg(prefix + "RefJet_BDTMedium_ety_ForwardReg",0),
    RefJet_BDTMedium_sumet_ForwardReg(prefix + "RefJet_BDTMedium_sumet_ForwardReg",0),
    RefJet_BDTMedium_phi_ForwardReg(prefix + "RefJet_BDTMedium_phi_ForwardReg",0),
    RefMuon_Staco_BDTMedium_etx(prefix + "RefMuon_Staco_BDTMedium_etx",0),
    RefMuon_Staco_BDTMedium_ety(prefix + "RefMuon_Staco_BDTMedium_ety",0),
    RefMuon_Staco_BDTMedium_phi(prefix + "RefMuon_Staco_BDTMedium_phi",0),
    RefMuon_Staco_BDTMedium_et(prefix + "RefMuon_Staco_BDTMedium_et",0),
    RefMuon_Staco_BDTMedium_sumet(prefix + "RefMuon_Staco_BDTMedium_sumet",0),
    RefMuon_Staco_BDTMedium_etx_CentralReg(prefix + "RefMuon_Staco_BDTMedium_etx_CentralReg",0),
    RefMuon_Staco_BDTMedium_ety_CentralReg(prefix + "RefMuon_Staco_BDTMedium_ety_CentralReg",0),
    RefMuon_Staco_BDTMedium_sumet_CentralReg(prefix + "RefMuon_Staco_BDTMedium_sumet_CentralReg",0),
    RefMuon_Staco_BDTMedium_phi_CentralReg(prefix + "RefMuon_Staco_BDTMedium_phi_CentralReg",0),
    RefMuon_Staco_BDTMedium_etx_EndcapRegion(prefix + "RefMuon_Staco_BDTMedium_etx_EndcapRegion",0),
    RefMuon_Staco_BDTMedium_ety_EndcapRegion(prefix + "RefMuon_Staco_BDTMedium_ety_EndcapRegion",0),
    RefMuon_Staco_BDTMedium_sumet_EndcapRegion(prefix + "RefMuon_Staco_BDTMedium_sumet_EndcapRegion",0),
    RefMuon_Staco_BDTMedium_phi_EndcapRegion(prefix + "RefMuon_Staco_BDTMedium_phi_EndcapRegion",0),
    RefMuon_Staco_BDTMedium_etx_ForwardReg(prefix + "RefMuon_Staco_BDTMedium_etx_ForwardReg",0),
    RefMuon_Staco_BDTMedium_ety_ForwardReg(prefix + "RefMuon_Staco_BDTMedium_ety_ForwardReg",0),
    RefMuon_Staco_BDTMedium_sumet_ForwardReg(prefix + "RefMuon_Staco_BDTMedium_sumet_ForwardReg",0),
    RefMuon_Staco_BDTMedium_phi_ForwardReg(prefix + "RefMuon_Staco_BDTMedium_phi_ForwardReg",0),
    RefFinal_AntiKt4LCTopo_tightpp_etx(prefix + "RefFinal_AntiKt4LCTopo_tightpp_etx",0),
    RefFinal_AntiKt4LCTopo_tightpp_ety(prefix + "RefFinal_AntiKt4LCTopo_tightpp_ety",0),
    RefFinal_AntiKt4LCTopo_tightpp_phi(prefix + "RefFinal_AntiKt4LCTopo_tightpp_phi",0),
    RefFinal_AntiKt4LCTopo_tightpp_et(prefix + "RefFinal_AntiKt4LCTopo_tightpp_et",0),
    RefFinal_AntiKt4LCTopo_tightpp_sumet(prefix + "RefFinal_AntiKt4LCTopo_tightpp_sumet",0),
    RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg",0),
    RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg",0),
    RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg",0),
    RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg",0),
    RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion",0),
    RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion",0),
    RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",0),
    RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion",0),
    RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg",0),
    RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg",0),
    RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg",0),
    RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg",0),
    RefGamma_AntiKt4LCTopo_tightpp_etx(prefix + "RefGamma_AntiKt4LCTopo_tightpp_etx",0),
    RefGamma_AntiKt4LCTopo_tightpp_ety(prefix + "RefGamma_AntiKt4LCTopo_tightpp_ety",0),
    RefGamma_AntiKt4LCTopo_tightpp_phi(prefix + "RefGamma_AntiKt4LCTopo_tightpp_phi",0),
    RefGamma_AntiKt4LCTopo_tightpp_et(prefix + "RefGamma_AntiKt4LCTopo_tightpp_et",0),
    RefGamma_AntiKt4LCTopo_tightpp_sumet(prefix + "RefGamma_AntiKt4LCTopo_tightpp_sumet",0),
    RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg",0),
    RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg",0),
    RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg",0),
    RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg",0),
    RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion",0),
    RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion",0),
    RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",0),
    RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion",0),
    RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg",0),
    RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg",0),
    RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg",0),
    RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg",0),
    RefEle_AntiKt4LCTopo_tightpp_etx(prefix + "RefEle_AntiKt4LCTopo_tightpp_etx",0),
    RefEle_AntiKt4LCTopo_tightpp_ety(prefix + "RefEle_AntiKt4LCTopo_tightpp_ety",0),
    RefEle_AntiKt4LCTopo_tightpp_phi(prefix + "RefEle_AntiKt4LCTopo_tightpp_phi",0),
    RefEle_AntiKt4LCTopo_tightpp_et(prefix + "RefEle_AntiKt4LCTopo_tightpp_et",0),
    RefEle_AntiKt4LCTopo_tightpp_sumet(prefix + "RefEle_AntiKt4LCTopo_tightpp_sumet",0),
    RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg",0),
    RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg",0),
    RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg",0),
    RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg",0),
    RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion",0),
    RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion",0),
    RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",0),
    RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion",0),
    RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg",0),
    RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg",0),
    RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg",0),
    RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg",0),
    RefTau_AntiKt4LCTopo_tightpp_etx(prefix + "RefTau_AntiKt4LCTopo_tightpp_etx",0),
    RefTau_AntiKt4LCTopo_tightpp_ety(prefix + "RefTau_AntiKt4LCTopo_tightpp_ety",0),
    RefTau_AntiKt4LCTopo_tightpp_phi(prefix + "RefTau_AntiKt4LCTopo_tightpp_phi",0),
    RefTau_AntiKt4LCTopo_tightpp_et(prefix + "RefTau_AntiKt4LCTopo_tightpp_et",0),
    RefTau_AntiKt4LCTopo_tightpp_sumet(prefix + "RefTau_AntiKt4LCTopo_tightpp_sumet",0),
    RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg",0),
    RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg",0),
    RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg",0),
    RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg",0),
    RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion",0),
    RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion",0),
    RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",0),
    RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion",0),
    RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg",0),
    RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg",0),
    RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg",0),
    RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg",0),
    RefJet_AntiKt4LCTopo_tightpp_etx(prefix + "RefJet_AntiKt4LCTopo_tightpp_etx",0),
    RefJet_AntiKt4LCTopo_tightpp_ety(prefix + "RefJet_AntiKt4LCTopo_tightpp_ety",0),
    RefJet_AntiKt4LCTopo_tightpp_phi(prefix + "RefJet_AntiKt4LCTopo_tightpp_phi",0),
    RefJet_AntiKt4LCTopo_tightpp_et(prefix + "RefJet_AntiKt4LCTopo_tightpp_et",0),
    RefJet_AntiKt4LCTopo_tightpp_sumet(prefix + "RefJet_AntiKt4LCTopo_tightpp_sumet",0),
    RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg",0),
    RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg",0),
    RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg",0),
    RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg",0),
    RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion",0),
    RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion",0),
    RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",0),
    RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion",0),
    RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg",0),
    RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg",0),
    RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg",0),
    RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_etx",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_ety",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_phi",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_et(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_et",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg",0),
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg(prefix + "RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void MET::ReadFrom(TTree* tree)
  {
    RefFinal_etx.ReadFrom(tree);
    RefFinal_ety.ReadFrom(tree);
    RefFinal_phi.ReadFrom(tree);
    RefFinal_et.ReadFrom(tree);
    RefFinal_sumet.ReadFrom(tree);
    RefFinal_etx_CentralReg.ReadFrom(tree);
    RefFinal_ety_CentralReg.ReadFrom(tree);
    RefFinal_sumet_CentralReg.ReadFrom(tree);
    RefFinal_phi_CentralReg.ReadFrom(tree);
    RefFinal_etx_EndcapRegion.ReadFrom(tree);
    RefFinal_ety_EndcapRegion.ReadFrom(tree);
    RefFinal_sumet_EndcapRegion.ReadFrom(tree);
    RefFinal_phi_EndcapRegion.ReadFrom(tree);
    RefFinal_etx_ForwardReg.ReadFrom(tree);
    RefFinal_ety_ForwardReg.ReadFrom(tree);
    RefFinal_sumet_ForwardReg.ReadFrom(tree);
    RefFinal_phi_ForwardReg.ReadFrom(tree);
    RefEle_etx.ReadFrom(tree);
    RefEle_ety.ReadFrom(tree);
    RefEle_phi.ReadFrom(tree);
    RefEle_et.ReadFrom(tree);
    RefEle_sumet.ReadFrom(tree);
    RefEle_etx_CentralReg.ReadFrom(tree);
    RefEle_ety_CentralReg.ReadFrom(tree);
    RefEle_sumet_CentralReg.ReadFrom(tree);
    RefEle_phi_CentralReg.ReadFrom(tree);
    RefEle_etx_EndcapRegion.ReadFrom(tree);
    RefEle_ety_EndcapRegion.ReadFrom(tree);
    RefEle_sumet_EndcapRegion.ReadFrom(tree);
    RefEle_phi_EndcapRegion.ReadFrom(tree);
    RefEle_etx_ForwardReg.ReadFrom(tree);
    RefEle_ety_ForwardReg.ReadFrom(tree);
    RefEle_sumet_ForwardReg.ReadFrom(tree);
    RefEle_phi_ForwardReg.ReadFrom(tree);
    RefJet_etx.ReadFrom(tree);
    RefJet_ety.ReadFrom(tree);
    RefJet_phi.ReadFrom(tree);
    RefJet_et.ReadFrom(tree);
    RefJet_sumet.ReadFrom(tree);
    RefJet_etx_CentralReg.ReadFrom(tree);
    RefJet_ety_CentralReg.ReadFrom(tree);
    RefJet_sumet_CentralReg.ReadFrom(tree);
    RefJet_phi_CentralReg.ReadFrom(tree);
    RefJet_etx_EndcapRegion.ReadFrom(tree);
    RefJet_ety_EndcapRegion.ReadFrom(tree);
    RefJet_sumet_EndcapRegion.ReadFrom(tree);
    RefJet_phi_EndcapRegion.ReadFrom(tree);
    RefJet_etx_ForwardReg.ReadFrom(tree);
    RefJet_ety_ForwardReg.ReadFrom(tree);
    RefJet_sumet_ForwardReg.ReadFrom(tree);
    RefJet_phi_ForwardReg.ReadFrom(tree);
    RefMuon_etx.ReadFrom(tree);
    RefMuon_ety.ReadFrom(tree);
    RefMuon_phi.ReadFrom(tree);
    RefMuon_et.ReadFrom(tree);
    RefMuon_sumet.ReadFrom(tree);
    RefMuon_etx_CentralReg.ReadFrom(tree);
    RefMuon_ety_CentralReg.ReadFrom(tree);
    RefMuon_sumet_CentralReg.ReadFrom(tree);
    RefMuon_phi_CentralReg.ReadFrom(tree);
    RefMuon_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_etx_ForwardReg.ReadFrom(tree);
    RefMuon_ety_ForwardReg.ReadFrom(tree);
    RefMuon_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_phi_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_etx.ReadFrom(tree);
    RefMuon_Staco_ety.ReadFrom(tree);
    RefMuon_Staco_phi.ReadFrom(tree);
    RefMuon_Staco_et.ReadFrom(tree);
    RefMuon_Staco_sumet.ReadFrom(tree);
    RefMuon_Staco_etx_CentralReg.ReadFrom(tree);
    RefMuon_Staco_ety_CentralReg.ReadFrom(tree);
    RefMuon_Staco_sumet_CentralReg.ReadFrom(tree);
    RefMuon_Staco_phi_CentralReg.ReadFrom(tree);
    RefMuon_Staco_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_etx_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_ety_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_phi_ForwardReg.ReadFrom(tree);
    RefMuon_Muid_etx.ReadFrom(tree);
    RefMuon_Muid_ety.ReadFrom(tree);
    RefMuon_Muid_phi.ReadFrom(tree);
    RefMuon_Muid_et.ReadFrom(tree);
    RefMuon_Muid_sumet.ReadFrom(tree);
    RefMuon_Muid_etx_CentralReg.ReadFrom(tree);
    RefMuon_Muid_ety_CentralReg.ReadFrom(tree);
    RefMuon_Muid_sumet_CentralReg.ReadFrom(tree);
    RefMuon_Muid_phi_CentralReg.ReadFrom(tree);
    RefMuon_Muid_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_Muid_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_Muid_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_Muid_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_Muid_etx_ForwardReg.ReadFrom(tree);
    RefMuon_Muid_ety_ForwardReg.ReadFrom(tree);
    RefMuon_Muid_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_Muid_phi_ForwardReg.ReadFrom(tree);
    RefMuons_etx.ReadFrom(tree);
    RefMuons_ety.ReadFrom(tree);
    RefMuons_phi.ReadFrom(tree);
    RefMuons_et.ReadFrom(tree);
    RefMuons_sumet.ReadFrom(tree);
    RefMuons_etx_CentralReg.ReadFrom(tree);
    RefMuons_ety_CentralReg.ReadFrom(tree);
    RefMuons_sumet_CentralReg.ReadFrom(tree);
    RefMuons_phi_CentralReg.ReadFrom(tree);
    RefMuons_etx_EndcapRegion.ReadFrom(tree);
    RefMuons_ety_EndcapRegion.ReadFrom(tree);
    RefMuons_sumet_EndcapRegion.ReadFrom(tree);
    RefMuons_phi_EndcapRegion.ReadFrom(tree);
    RefMuons_etx_ForwardReg.ReadFrom(tree);
    RefMuons_ety_ForwardReg.ReadFrom(tree);
    RefMuons_sumet_ForwardReg.ReadFrom(tree);
    RefMuons_phi_ForwardReg.ReadFrom(tree);
    RefGamma_etx.ReadFrom(tree);
    RefGamma_ety.ReadFrom(tree);
    RefGamma_phi.ReadFrom(tree);
    RefGamma_et.ReadFrom(tree);
    RefGamma_sumet.ReadFrom(tree);
    RefGamma_etx_CentralReg.ReadFrom(tree);
    RefGamma_ety_CentralReg.ReadFrom(tree);
    RefGamma_sumet_CentralReg.ReadFrom(tree);
    RefGamma_phi_CentralReg.ReadFrom(tree);
    RefGamma_etx_EndcapRegion.ReadFrom(tree);
    RefGamma_ety_EndcapRegion.ReadFrom(tree);
    RefGamma_sumet_EndcapRegion.ReadFrom(tree);
    RefGamma_phi_EndcapRegion.ReadFrom(tree);
    RefGamma_etx_ForwardReg.ReadFrom(tree);
    RefGamma_ety_ForwardReg.ReadFrom(tree);
    RefGamma_sumet_ForwardReg.ReadFrom(tree);
    RefGamma_phi_ForwardReg.ReadFrom(tree);
    RefTau_etx.ReadFrom(tree);
    RefTau_ety.ReadFrom(tree);
    RefTau_phi.ReadFrom(tree);
    RefTau_et.ReadFrom(tree);
    RefTau_sumet.ReadFrom(tree);
    RefTau_etx_CentralReg.ReadFrom(tree);
    RefTau_ety_CentralReg.ReadFrom(tree);
    RefTau_sumet_CentralReg.ReadFrom(tree);
    RefTau_phi_CentralReg.ReadFrom(tree);
    RefTau_etx_EndcapRegion.ReadFrom(tree);
    RefTau_ety_EndcapRegion.ReadFrom(tree);
    RefTau_sumet_EndcapRegion.ReadFrom(tree);
    RefTau_phi_EndcapRegion.ReadFrom(tree);
    RefTau_etx_ForwardReg.ReadFrom(tree);
    RefTau_ety_ForwardReg.ReadFrom(tree);
    RefTau_sumet_ForwardReg.ReadFrom(tree);
    RefTau_phi_ForwardReg.ReadFrom(tree);
    RefFinal_em_etx.ReadFrom(tree);
    RefFinal_em_ety.ReadFrom(tree);
    RefFinal_em_phi.ReadFrom(tree);
    RefFinal_em_et.ReadFrom(tree);
    RefFinal_em_sumet.ReadFrom(tree);
    RefFinal_em_etx_CentralReg.ReadFrom(tree);
    RefFinal_em_ety_CentralReg.ReadFrom(tree);
    RefFinal_em_sumet_CentralReg.ReadFrom(tree);
    RefFinal_em_phi_CentralReg.ReadFrom(tree);
    RefFinal_em_etx_EndcapRegion.ReadFrom(tree);
    RefFinal_em_ety_EndcapRegion.ReadFrom(tree);
    RefFinal_em_sumet_EndcapRegion.ReadFrom(tree);
    RefFinal_em_phi_EndcapRegion.ReadFrom(tree);
    RefFinal_em_etx_ForwardReg.ReadFrom(tree);
    RefFinal_em_ety_ForwardReg.ReadFrom(tree);
    RefFinal_em_sumet_ForwardReg.ReadFrom(tree);
    RefFinal_em_phi_ForwardReg.ReadFrom(tree);
    RefEle_em_etx.ReadFrom(tree);
    RefEle_em_ety.ReadFrom(tree);
    RefEle_em_phi.ReadFrom(tree);
    RefEle_em_et.ReadFrom(tree);
    RefEle_em_sumet.ReadFrom(tree);
    RefEle_em_etx_CentralReg.ReadFrom(tree);
    RefEle_em_ety_CentralReg.ReadFrom(tree);
    RefEle_em_sumet_CentralReg.ReadFrom(tree);
    RefEle_em_phi_CentralReg.ReadFrom(tree);
    RefEle_em_etx_EndcapRegion.ReadFrom(tree);
    RefEle_em_ety_EndcapRegion.ReadFrom(tree);
    RefEle_em_sumet_EndcapRegion.ReadFrom(tree);
    RefEle_em_phi_EndcapRegion.ReadFrom(tree);
    RefEle_em_etx_ForwardReg.ReadFrom(tree);
    RefEle_em_ety_ForwardReg.ReadFrom(tree);
    RefEle_em_sumet_ForwardReg.ReadFrom(tree);
    RefEle_em_phi_ForwardReg.ReadFrom(tree);
    RefJet_em_etx.ReadFrom(tree);
    RefJet_em_ety.ReadFrom(tree);
    RefJet_em_phi.ReadFrom(tree);
    RefJet_em_et.ReadFrom(tree);
    RefJet_em_sumet.ReadFrom(tree);
    RefJet_em_etx_CentralReg.ReadFrom(tree);
    RefJet_em_ety_CentralReg.ReadFrom(tree);
    RefJet_em_sumet_CentralReg.ReadFrom(tree);
    RefJet_em_phi_CentralReg.ReadFrom(tree);
    RefJet_em_etx_EndcapRegion.ReadFrom(tree);
    RefJet_em_ety_EndcapRegion.ReadFrom(tree);
    RefJet_em_sumet_EndcapRegion.ReadFrom(tree);
    RefJet_em_phi_EndcapRegion.ReadFrom(tree);
    RefJet_em_etx_ForwardReg.ReadFrom(tree);
    RefJet_em_ety_ForwardReg.ReadFrom(tree);
    RefJet_em_sumet_ForwardReg.ReadFrom(tree);
    RefJet_em_phi_ForwardReg.ReadFrom(tree);
    RefMuon_em_etx.ReadFrom(tree);
    RefMuon_em_ety.ReadFrom(tree);
    RefMuon_em_phi.ReadFrom(tree);
    RefMuon_em_et.ReadFrom(tree);
    RefMuon_em_sumet.ReadFrom(tree);
    RefMuon_em_etx_CentralReg.ReadFrom(tree);
    RefMuon_em_ety_CentralReg.ReadFrom(tree);
    RefMuon_em_sumet_CentralReg.ReadFrom(tree);
    RefMuon_em_phi_CentralReg.ReadFrom(tree);
    RefMuon_em_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_em_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_em_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_em_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_em_etx_ForwardReg.ReadFrom(tree);
    RefMuon_em_ety_ForwardReg.ReadFrom(tree);
    RefMuon_em_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_em_phi_ForwardReg.ReadFrom(tree);
    RefMuon_Track_em_etx.ReadFrom(tree);
    RefMuon_Track_em_ety.ReadFrom(tree);
    RefMuon_Track_em_phi.ReadFrom(tree);
    RefMuon_Track_em_et.ReadFrom(tree);
    RefMuon_Track_em_sumet.ReadFrom(tree);
    RefMuon_Track_em_etx_CentralReg.ReadFrom(tree);
    RefMuon_Track_em_ety_CentralReg.ReadFrom(tree);
    RefMuon_Track_em_sumet_CentralReg.ReadFrom(tree);
    RefMuon_Track_em_phi_CentralReg.ReadFrom(tree);
    RefMuon_Track_em_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_em_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_em_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_em_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_em_etx_ForwardReg.ReadFrom(tree);
    RefMuon_Track_em_ety_ForwardReg.ReadFrom(tree);
    RefMuon_Track_em_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_Track_em_phi_ForwardReg.ReadFrom(tree);
    RefGamma_em_etx.ReadFrom(tree);
    RefGamma_em_ety.ReadFrom(tree);
    RefGamma_em_phi.ReadFrom(tree);
    RefGamma_em_et.ReadFrom(tree);
    RefGamma_em_sumet.ReadFrom(tree);
    RefGamma_em_etx_CentralReg.ReadFrom(tree);
    RefGamma_em_ety_CentralReg.ReadFrom(tree);
    RefGamma_em_sumet_CentralReg.ReadFrom(tree);
    RefGamma_em_phi_CentralReg.ReadFrom(tree);
    RefGamma_em_etx_EndcapRegion.ReadFrom(tree);
    RefGamma_em_ety_EndcapRegion.ReadFrom(tree);
    RefGamma_em_sumet_EndcapRegion.ReadFrom(tree);
    RefGamma_em_phi_EndcapRegion.ReadFrom(tree);
    RefGamma_em_etx_ForwardReg.ReadFrom(tree);
    RefGamma_em_ety_ForwardReg.ReadFrom(tree);
    RefGamma_em_sumet_ForwardReg.ReadFrom(tree);
    RefGamma_em_phi_ForwardReg.ReadFrom(tree);
    RefTau_em_etx.ReadFrom(tree);
    RefTau_em_ety.ReadFrom(tree);
    RefTau_em_phi.ReadFrom(tree);
    RefTau_em_et.ReadFrom(tree);
    RefTau_em_sumet.ReadFrom(tree);
    RefTau_em_etx_CentralReg.ReadFrom(tree);
    RefTau_em_ety_CentralReg.ReadFrom(tree);
    RefTau_em_sumet_CentralReg.ReadFrom(tree);
    RefTau_em_phi_CentralReg.ReadFrom(tree);
    RefTau_em_etx_EndcapRegion.ReadFrom(tree);
    RefTau_em_ety_EndcapRegion.ReadFrom(tree);
    RefTau_em_sumet_EndcapRegion.ReadFrom(tree);
    RefTau_em_phi_EndcapRegion.ReadFrom(tree);
    RefTau_em_etx_ForwardReg.ReadFrom(tree);
    RefTau_em_ety_ForwardReg.ReadFrom(tree);
    RefTau_em_sumet_ForwardReg.ReadFrom(tree);
    RefTau_em_phi_ForwardReg.ReadFrom(tree);
    RefJet_JVF_etx.ReadFrom(tree);
    RefJet_JVF_ety.ReadFrom(tree);
    RefJet_JVF_phi.ReadFrom(tree);
    RefJet_JVF_et.ReadFrom(tree);
    RefJet_JVF_sumet.ReadFrom(tree);
    RefJet_JVF_etx_CentralReg.ReadFrom(tree);
    RefJet_JVF_ety_CentralReg.ReadFrom(tree);
    RefJet_JVF_sumet_CentralReg.ReadFrom(tree);
    RefJet_JVF_phi_CentralReg.ReadFrom(tree);
    RefJet_JVF_etx_EndcapRegion.ReadFrom(tree);
    RefJet_JVF_ety_EndcapRegion.ReadFrom(tree);
    RefJet_JVF_sumet_EndcapRegion.ReadFrom(tree);
    RefJet_JVF_phi_EndcapRegion.ReadFrom(tree);
    RefJet_JVF_etx_ForwardReg.ReadFrom(tree);
    RefJet_JVF_ety_ForwardReg.ReadFrom(tree);
    RefJet_JVF_sumet_ForwardReg.ReadFrom(tree);
    RefJet_JVF_phi_ForwardReg.ReadFrom(tree);
    RefJet_JVFCut_etx.ReadFrom(tree);
    RefJet_JVFCut_ety.ReadFrom(tree);
    RefJet_JVFCut_phi.ReadFrom(tree);
    RefJet_JVFCut_et.ReadFrom(tree);
    RefJet_JVFCut_sumet.ReadFrom(tree);
    RefJet_JVFCut_etx_CentralReg.ReadFrom(tree);
    RefJet_JVFCut_ety_CentralReg.ReadFrom(tree);
    RefJet_JVFCut_sumet_CentralReg.ReadFrom(tree);
    RefJet_JVFCut_phi_CentralReg.ReadFrom(tree);
    RefJet_JVFCut_etx_EndcapRegion.ReadFrom(tree);
    RefJet_JVFCut_ety_EndcapRegion.ReadFrom(tree);
    RefJet_JVFCut_sumet_EndcapRegion.ReadFrom(tree);
    RefJet_JVFCut_phi_EndcapRegion.ReadFrom(tree);
    RefJet_JVFCut_etx_ForwardReg.ReadFrom(tree);
    RefJet_JVFCut_ety_ForwardReg.ReadFrom(tree);
    RefJet_JVFCut_sumet_ForwardReg.ReadFrom(tree);
    RefJet_JVFCut_phi_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_STVF_etx.ReadFrom(tree);
    CellOut_Eflow_STVF_ety.ReadFrom(tree);
    CellOut_Eflow_STVF_phi.ReadFrom(tree);
    CellOut_Eflow_STVF_et.ReadFrom(tree);
    CellOut_Eflow_STVF_sumet.ReadFrom(tree);
    CellOut_Eflow_STVF_etx_CentralReg.ReadFrom(tree);
    CellOut_Eflow_STVF_ety_CentralReg.ReadFrom(tree);
    CellOut_Eflow_STVF_sumet_CentralReg.ReadFrom(tree);
    CellOut_Eflow_STVF_phi_CentralReg.ReadFrom(tree);
    CellOut_Eflow_STVF_etx_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_STVF_ety_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_STVF_sumet_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_STVF_phi_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_STVF_etx_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_STVF_ety_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_STVF_sumet_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_STVF_phi_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetArea_etx.ReadFrom(tree);
    CellOut_Eflow_JetArea_ety.ReadFrom(tree);
    CellOut_Eflow_JetArea_phi.ReadFrom(tree);
    CellOut_Eflow_JetArea_et.ReadFrom(tree);
    CellOut_Eflow_JetArea_sumet.ReadFrom(tree);
    CellOut_Eflow_JetArea_etx_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetArea_ety_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetArea_sumet_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetArea_phi_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetArea_etx_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetArea_ety_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetArea_sumet_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetArea_phi_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetArea_etx_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetArea_ety_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetArea_sumet_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetArea_phi_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_etx.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_ety.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_phi.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_et.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_sumet.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_etx_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_ety_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_sumet_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_phi_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_etx_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_ety_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_phi_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_etx_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_ety_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_sumet_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaJVF_phi_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_etx.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_ety.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_phi.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_et.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg.ReadFrom(tree);
    RefFinal_STVF_etx.ReadFrom(tree);
    RefFinal_STVF_ety.ReadFrom(tree);
    RefFinal_STVF_phi.ReadFrom(tree);
    RefFinal_STVF_et.ReadFrom(tree);
    RefFinal_STVF_sumet.ReadFrom(tree);
    RefFinal_STVF_etx_CentralReg.ReadFrom(tree);
    RefFinal_STVF_ety_CentralReg.ReadFrom(tree);
    RefFinal_STVF_sumet_CentralReg.ReadFrom(tree);
    RefFinal_STVF_phi_CentralReg.ReadFrom(tree);
    RefFinal_STVF_etx_EndcapRegion.ReadFrom(tree);
    RefFinal_STVF_ety_EndcapRegion.ReadFrom(tree);
    RefFinal_STVF_sumet_EndcapRegion.ReadFrom(tree);
    RefFinal_STVF_phi_EndcapRegion.ReadFrom(tree);
    RefFinal_STVF_etx_ForwardReg.ReadFrom(tree);
    RefFinal_STVF_ety_ForwardReg.ReadFrom(tree);
    RefFinal_STVF_sumet_ForwardReg.ReadFrom(tree);
    RefFinal_STVF_phi_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_etx.ReadFrom(tree);
    CellOut_Eflow_ety.ReadFrom(tree);
    CellOut_Eflow_phi.ReadFrom(tree);
    CellOut_Eflow_et.ReadFrom(tree);
    CellOut_Eflow_sumet.ReadFrom(tree);
    CellOut_Eflow_etx_CentralReg.ReadFrom(tree);
    CellOut_Eflow_ety_CentralReg.ReadFrom(tree);
    CellOut_Eflow_sumet_CentralReg.ReadFrom(tree);
    CellOut_Eflow_phi_CentralReg.ReadFrom(tree);
    CellOut_Eflow_etx_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_ety_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_sumet_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_phi_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_etx_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_ety_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_sumet_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_phi_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_Muid_etx.ReadFrom(tree);
    CellOut_Eflow_Muid_ety.ReadFrom(tree);
    CellOut_Eflow_Muid_phi.ReadFrom(tree);
    CellOut_Eflow_Muid_et.ReadFrom(tree);
    CellOut_Eflow_Muid_sumet.ReadFrom(tree);
    CellOut_Eflow_Muid_etx_CentralReg.ReadFrom(tree);
    CellOut_Eflow_Muid_ety_CentralReg.ReadFrom(tree);
    CellOut_Eflow_Muid_sumet_CentralReg.ReadFrom(tree);
    CellOut_Eflow_Muid_phi_CentralReg.ReadFrom(tree);
    CellOut_Eflow_Muid_etx_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_Muid_ety_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_Muid_sumet_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_Muid_phi_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_Muid_etx_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_Muid_ety_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_Muid_sumet_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_Muid_phi_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_Muons_etx.ReadFrom(tree);
    CellOut_Eflow_Muons_ety.ReadFrom(tree);
    CellOut_Eflow_Muons_phi.ReadFrom(tree);
    CellOut_Eflow_Muons_et.ReadFrom(tree);
    CellOut_Eflow_Muons_sumet.ReadFrom(tree);
    CellOut_Eflow_Muons_etx_CentralReg.ReadFrom(tree);
    CellOut_Eflow_Muons_ety_CentralReg.ReadFrom(tree);
    CellOut_Eflow_Muons_sumet_CentralReg.ReadFrom(tree);
    CellOut_Eflow_Muons_phi_CentralReg.ReadFrom(tree);
    CellOut_Eflow_Muons_etx_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_Muons_ety_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_Muons_sumet_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_Muons_phi_EndcapRegion.ReadFrom(tree);
    CellOut_Eflow_Muons_etx_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_Muons_ety_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_Muons_sumet_ForwardReg.ReadFrom(tree);
    CellOut_Eflow_Muons_phi_ForwardReg.ReadFrom(tree);
    RefMuon_Track_etx.ReadFrom(tree);
    RefMuon_Track_ety.ReadFrom(tree);
    RefMuon_Track_phi.ReadFrom(tree);
    RefMuon_Track_et.ReadFrom(tree);
    RefMuon_Track_sumet.ReadFrom(tree);
    RefMuon_Track_etx_CentralReg.ReadFrom(tree);
    RefMuon_Track_ety_CentralReg.ReadFrom(tree);
    RefMuon_Track_sumet_CentralReg.ReadFrom(tree);
    RefMuon_Track_phi_CentralReg.ReadFrom(tree);
    RefMuon_Track_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_etx_ForwardReg.ReadFrom(tree);
    RefMuon_Track_ety_ForwardReg.ReadFrom(tree);
    RefMuon_Track_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_Track_phi_ForwardReg.ReadFrom(tree);
    RefMuon_Track_Staco_etx.ReadFrom(tree);
    RefMuon_Track_Staco_ety.ReadFrom(tree);
    RefMuon_Track_Staco_phi.ReadFrom(tree);
    RefMuon_Track_Staco_et.ReadFrom(tree);
    RefMuon_Track_Staco_sumet.ReadFrom(tree);
    RefMuon_Track_Staco_etx_CentralReg.ReadFrom(tree);
    RefMuon_Track_Staco_ety_CentralReg.ReadFrom(tree);
    RefMuon_Track_Staco_sumet_CentralReg.ReadFrom(tree);
    RefMuon_Track_Staco_phi_CentralReg.ReadFrom(tree);
    RefMuon_Track_Staco_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_Staco_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_Staco_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_Staco_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_Staco_etx_ForwardReg.ReadFrom(tree);
    RefMuon_Track_Staco_ety_ForwardReg.ReadFrom(tree);
    RefMuon_Track_Staco_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_Track_Staco_phi_ForwardReg.ReadFrom(tree);
    RefMuon_Track_Muid_etx.ReadFrom(tree);
    RefMuon_Track_Muid_ety.ReadFrom(tree);
    RefMuon_Track_Muid_phi.ReadFrom(tree);
    RefMuon_Track_Muid_et.ReadFrom(tree);
    RefMuon_Track_Muid_sumet.ReadFrom(tree);
    RefMuon_Track_Muid_etx_CentralReg.ReadFrom(tree);
    RefMuon_Track_Muid_ety_CentralReg.ReadFrom(tree);
    RefMuon_Track_Muid_sumet_CentralReg.ReadFrom(tree);
    RefMuon_Track_Muid_phi_CentralReg.ReadFrom(tree);
    RefMuon_Track_Muid_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_Muid_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_Muid_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_Muid_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_Track_Muid_etx_ForwardReg.ReadFrom(tree);
    RefMuon_Track_Muid_ety_ForwardReg.ReadFrom(tree);
    RefMuon_Track_Muid_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_Track_Muid_phi_ForwardReg.ReadFrom(tree);
    RefMuons_Track_etx.ReadFrom(tree);
    RefMuons_Track_ety.ReadFrom(tree);
    RefMuons_Track_phi.ReadFrom(tree);
    RefMuons_Track_et.ReadFrom(tree);
    RefMuons_Track_sumet.ReadFrom(tree);
    RefMuons_Track_etx_CentralReg.ReadFrom(tree);
    RefMuons_Track_ety_CentralReg.ReadFrom(tree);
    RefMuons_Track_sumet_CentralReg.ReadFrom(tree);
    RefMuons_Track_phi_CentralReg.ReadFrom(tree);
    RefMuons_Track_etx_EndcapRegion.ReadFrom(tree);
    RefMuons_Track_ety_EndcapRegion.ReadFrom(tree);
    RefMuons_Track_sumet_EndcapRegion.ReadFrom(tree);
    RefMuons_Track_phi_EndcapRegion.ReadFrom(tree);
    RefMuons_Track_etx_ForwardReg.ReadFrom(tree);
    RefMuons_Track_ety_ForwardReg.ReadFrom(tree);
    RefMuons_Track_sumet_ForwardReg.ReadFrom(tree);
    RefMuons_Track_phi_ForwardReg.ReadFrom(tree);
    RefFinal_BDTMedium_etx.ReadFrom(tree);
    RefFinal_BDTMedium_ety.ReadFrom(tree);
    RefFinal_BDTMedium_phi.ReadFrom(tree);
    RefFinal_BDTMedium_et.ReadFrom(tree);
    RefFinal_BDTMedium_sumet.ReadFrom(tree);
    RefFinal_BDTMedium_etx_CentralReg.ReadFrom(tree);
    RefFinal_BDTMedium_ety_CentralReg.ReadFrom(tree);
    RefFinal_BDTMedium_sumet_CentralReg.ReadFrom(tree);
    RefFinal_BDTMedium_phi_CentralReg.ReadFrom(tree);
    RefFinal_BDTMedium_etx_EndcapRegion.ReadFrom(tree);
    RefFinal_BDTMedium_ety_EndcapRegion.ReadFrom(tree);
    RefFinal_BDTMedium_sumet_EndcapRegion.ReadFrom(tree);
    RefFinal_BDTMedium_phi_EndcapRegion.ReadFrom(tree);
    RefFinal_BDTMedium_etx_ForwardReg.ReadFrom(tree);
    RefFinal_BDTMedium_ety_ForwardReg.ReadFrom(tree);
    RefFinal_BDTMedium_sumet_ForwardReg.ReadFrom(tree);
    RefFinal_BDTMedium_phi_ForwardReg.ReadFrom(tree);
    RefGamma_BDTMedium_etx.ReadFrom(tree);
    RefGamma_BDTMedium_ety.ReadFrom(tree);
    RefGamma_BDTMedium_phi.ReadFrom(tree);
    RefGamma_BDTMedium_et.ReadFrom(tree);
    RefGamma_BDTMedium_sumet.ReadFrom(tree);
    RefGamma_BDTMedium_etx_CentralReg.ReadFrom(tree);
    RefGamma_BDTMedium_ety_CentralReg.ReadFrom(tree);
    RefGamma_BDTMedium_sumet_CentralReg.ReadFrom(tree);
    RefGamma_BDTMedium_phi_CentralReg.ReadFrom(tree);
    RefGamma_BDTMedium_etx_EndcapRegion.ReadFrom(tree);
    RefGamma_BDTMedium_ety_EndcapRegion.ReadFrom(tree);
    RefGamma_BDTMedium_sumet_EndcapRegion.ReadFrom(tree);
    RefGamma_BDTMedium_phi_EndcapRegion.ReadFrom(tree);
    RefGamma_BDTMedium_etx_ForwardReg.ReadFrom(tree);
    RefGamma_BDTMedium_ety_ForwardReg.ReadFrom(tree);
    RefGamma_BDTMedium_sumet_ForwardReg.ReadFrom(tree);
    RefGamma_BDTMedium_phi_ForwardReg.ReadFrom(tree);
    RefEle_BDTMedium_etx.ReadFrom(tree);
    RefEle_BDTMedium_ety.ReadFrom(tree);
    RefEle_BDTMedium_phi.ReadFrom(tree);
    RefEle_BDTMedium_et.ReadFrom(tree);
    RefEle_BDTMedium_sumet.ReadFrom(tree);
    RefEle_BDTMedium_etx_CentralReg.ReadFrom(tree);
    RefEle_BDTMedium_ety_CentralReg.ReadFrom(tree);
    RefEle_BDTMedium_sumet_CentralReg.ReadFrom(tree);
    RefEle_BDTMedium_phi_CentralReg.ReadFrom(tree);
    RefEle_BDTMedium_etx_EndcapRegion.ReadFrom(tree);
    RefEle_BDTMedium_ety_EndcapRegion.ReadFrom(tree);
    RefEle_BDTMedium_sumet_EndcapRegion.ReadFrom(tree);
    RefEle_BDTMedium_phi_EndcapRegion.ReadFrom(tree);
    RefEle_BDTMedium_etx_ForwardReg.ReadFrom(tree);
    RefEle_BDTMedium_ety_ForwardReg.ReadFrom(tree);
    RefEle_BDTMedium_sumet_ForwardReg.ReadFrom(tree);
    RefEle_BDTMedium_phi_ForwardReg.ReadFrom(tree);
    RefTau_BDTMedium_etx.ReadFrom(tree);
    RefTau_BDTMedium_ety.ReadFrom(tree);
    RefTau_BDTMedium_phi.ReadFrom(tree);
    RefTau_BDTMedium_et.ReadFrom(tree);
    RefTau_BDTMedium_sumet.ReadFrom(tree);
    RefTau_BDTMedium_etx_CentralReg.ReadFrom(tree);
    RefTau_BDTMedium_ety_CentralReg.ReadFrom(tree);
    RefTau_BDTMedium_sumet_CentralReg.ReadFrom(tree);
    RefTau_BDTMedium_phi_CentralReg.ReadFrom(tree);
    RefTau_BDTMedium_etx_EndcapRegion.ReadFrom(tree);
    RefTau_BDTMedium_ety_EndcapRegion.ReadFrom(tree);
    RefTau_BDTMedium_sumet_EndcapRegion.ReadFrom(tree);
    RefTau_BDTMedium_phi_EndcapRegion.ReadFrom(tree);
    RefTau_BDTMedium_etx_ForwardReg.ReadFrom(tree);
    RefTau_BDTMedium_ety_ForwardReg.ReadFrom(tree);
    RefTau_BDTMedium_sumet_ForwardReg.ReadFrom(tree);
    RefTau_BDTMedium_phi_ForwardReg.ReadFrom(tree);
    RefJet_BDTMedium_etx.ReadFrom(tree);
    RefJet_BDTMedium_ety.ReadFrom(tree);
    RefJet_BDTMedium_phi.ReadFrom(tree);
    RefJet_BDTMedium_et.ReadFrom(tree);
    RefJet_BDTMedium_sumet.ReadFrom(tree);
    RefJet_BDTMedium_etx_CentralReg.ReadFrom(tree);
    RefJet_BDTMedium_ety_CentralReg.ReadFrom(tree);
    RefJet_BDTMedium_sumet_CentralReg.ReadFrom(tree);
    RefJet_BDTMedium_phi_CentralReg.ReadFrom(tree);
    RefJet_BDTMedium_etx_EndcapRegion.ReadFrom(tree);
    RefJet_BDTMedium_ety_EndcapRegion.ReadFrom(tree);
    RefJet_BDTMedium_sumet_EndcapRegion.ReadFrom(tree);
    RefJet_BDTMedium_phi_EndcapRegion.ReadFrom(tree);
    RefJet_BDTMedium_etx_ForwardReg.ReadFrom(tree);
    RefJet_BDTMedium_ety_ForwardReg.ReadFrom(tree);
    RefJet_BDTMedium_sumet_ForwardReg.ReadFrom(tree);
    RefJet_BDTMedium_phi_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_etx.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_ety.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_phi.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_et.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_sumet.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_etx_CentralReg.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_ety_CentralReg.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_sumet_CentralReg.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_phi_CentralReg.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_etx_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_ety_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_BDTMedium_phi_ForwardReg.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_etx.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_ety.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_phi.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_et.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_sumet.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg.ReadFrom(tree);
    RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_etx.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_ety.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_phi.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_et.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_sumet.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg.ReadFrom(tree);
    RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_etx.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_ety.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_phi.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_et.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_sumet.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg.ReadFrom(tree);
    RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_etx.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_ety.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_phi.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_et.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_sumet.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg.ReadFrom(tree);
    RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_etx.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_ety.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_phi.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_et.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_sumet.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg.ReadFrom(tree);
    RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_et.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg.ReadFrom(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void MET::WriteTo(TTree* tree)
  {
    RefFinal_etx.WriteTo(tree);
    RefFinal_ety.WriteTo(tree);
    RefFinal_phi.WriteTo(tree);
    RefFinal_et.WriteTo(tree);
    RefFinal_sumet.WriteTo(tree);
    RefFinal_etx_CentralReg.WriteTo(tree);
    RefFinal_ety_CentralReg.WriteTo(tree);
    RefFinal_sumet_CentralReg.WriteTo(tree);
    RefFinal_phi_CentralReg.WriteTo(tree);
    RefFinal_etx_EndcapRegion.WriteTo(tree);
    RefFinal_ety_EndcapRegion.WriteTo(tree);
    RefFinal_sumet_EndcapRegion.WriteTo(tree);
    RefFinal_phi_EndcapRegion.WriteTo(tree);
    RefFinal_etx_ForwardReg.WriteTo(tree);
    RefFinal_ety_ForwardReg.WriteTo(tree);
    RefFinal_sumet_ForwardReg.WriteTo(tree);
    RefFinal_phi_ForwardReg.WriteTo(tree);
    RefEle_etx.WriteTo(tree);
    RefEle_ety.WriteTo(tree);
    RefEle_phi.WriteTo(tree);
    RefEle_et.WriteTo(tree);
    RefEle_sumet.WriteTo(tree);
    RefEle_etx_CentralReg.WriteTo(tree);
    RefEle_ety_CentralReg.WriteTo(tree);
    RefEle_sumet_CentralReg.WriteTo(tree);
    RefEle_phi_CentralReg.WriteTo(tree);
    RefEle_etx_EndcapRegion.WriteTo(tree);
    RefEle_ety_EndcapRegion.WriteTo(tree);
    RefEle_sumet_EndcapRegion.WriteTo(tree);
    RefEle_phi_EndcapRegion.WriteTo(tree);
    RefEle_etx_ForwardReg.WriteTo(tree);
    RefEle_ety_ForwardReg.WriteTo(tree);
    RefEle_sumet_ForwardReg.WriteTo(tree);
    RefEle_phi_ForwardReg.WriteTo(tree);
    RefJet_etx.WriteTo(tree);
    RefJet_ety.WriteTo(tree);
    RefJet_phi.WriteTo(tree);
    RefJet_et.WriteTo(tree);
    RefJet_sumet.WriteTo(tree);
    RefJet_etx_CentralReg.WriteTo(tree);
    RefJet_ety_CentralReg.WriteTo(tree);
    RefJet_sumet_CentralReg.WriteTo(tree);
    RefJet_phi_CentralReg.WriteTo(tree);
    RefJet_etx_EndcapRegion.WriteTo(tree);
    RefJet_ety_EndcapRegion.WriteTo(tree);
    RefJet_sumet_EndcapRegion.WriteTo(tree);
    RefJet_phi_EndcapRegion.WriteTo(tree);
    RefJet_etx_ForwardReg.WriteTo(tree);
    RefJet_ety_ForwardReg.WriteTo(tree);
    RefJet_sumet_ForwardReg.WriteTo(tree);
    RefJet_phi_ForwardReg.WriteTo(tree);
    RefMuon_etx.WriteTo(tree);
    RefMuon_ety.WriteTo(tree);
    RefMuon_phi.WriteTo(tree);
    RefMuon_et.WriteTo(tree);
    RefMuon_sumet.WriteTo(tree);
    RefMuon_etx_CentralReg.WriteTo(tree);
    RefMuon_ety_CentralReg.WriteTo(tree);
    RefMuon_sumet_CentralReg.WriteTo(tree);
    RefMuon_phi_CentralReg.WriteTo(tree);
    RefMuon_etx_EndcapRegion.WriteTo(tree);
    RefMuon_ety_EndcapRegion.WriteTo(tree);
    RefMuon_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_phi_EndcapRegion.WriteTo(tree);
    RefMuon_etx_ForwardReg.WriteTo(tree);
    RefMuon_ety_ForwardReg.WriteTo(tree);
    RefMuon_sumet_ForwardReg.WriteTo(tree);
    RefMuon_phi_ForwardReg.WriteTo(tree);
    RefMuon_Staco_etx.WriteTo(tree);
    RefMuon_Staco_ety.WriteTo(tree);
    RefMuon_Staco_phi.WriteTo(tree);
    RefMuon_Staco_et.WriteTo(tree);
    RefMuon_Staco_sumet.WriteTo(tree);
    RefMuon_Staco_etx_CentralReg.WriteTo(tree);
    RefMuon_Staco_ety_CentralReg.WriteTo(tree);
    RefMuon_Staco_sumet_CentralReg.WriteTo(tree);
    RefMuon_Staco_phi_CentralReg.WriteTo(tree);
    RefMuon_Staco_etx_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_ety_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_phi_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_etx_ForwardReg.WriteTo(tree);
    RefMuon_Staco_ety_ForwardReg.WriteTo(tree);
    RefMuon_Staco_sumet_ForwardReg.WriteTo(tree);
    RefMuon_Staco_phi_ForwardReg.WriteTo(tree);
    RefMuon_Muid_etx.WriteTo(tree);
    RefMuon_Muid_ety.WriteTo(tree);
    RefMuon_Muid_phi.WriteTo(tree);
    RefMuon_Muid_et.WriteTo(tree);
    RefMuon_Muid_sumet.WriteTo(tree);
    RefMuon_Muid_etx_CentralReg.WriteTo(tree);
    RefMuon_Muid_ety_CentralReg.WriteTo(tree);
    RefMuon_Muid_sumet_CentralReg.WriteTo(tree);
    RefMuon_Muid_phi_CentralReg.WriteTo(tree);
    RefMuon_Muid_etx_EndcapRegion.WriteTo(tree);
    RefMuon_Muid_ety_EndcapRegion.WriteTo(tree);
    RefMuon_Muid_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_Muid_phi_EndcapRegion.WriteTo(tree);
    RefMuon_Muid_etx_ForwardReg.WriteTo(tree);
    RefMuon_Muid_ety_ForwardReg.WriteTo(tree);
    RefMuon_Muid_sumet_ForwardReg.WriteTo(tree);
    RefMuon_Muid_phi_ForwardReg.WriteTo(tree);
    RefMuons_etx.WriteTo(tree);
    RefMuons_ety.WriteTo(tree);
    RefMuons_phi.WriteTo(tree);
    RefMuons_et.WriteTo(tree);
    RefMuons_sumet.WriteTo(tree);
    RefMuons_etx_CentralReg.WriteTo(tree);
    RefMuons_ety_CentralReg.WriteTo(tree);
    RefMuons_sumet_CentralReg.WriteTo(tree);
    RefMuons_phi_CentralReg.WriteTo(tree);
    RefMuons_etx_EndcapRegion.WriteTo(tree);
    RefMuons_ety_EndcapRegion.WriteTo(tree);
    RefMuons_sumet_EndcapRegion.WriteTo(tree);
    RefMuons_phi_EndcapRegion.WriteTo(tree);
    RefMuons_etx_ForwardReg.WriteTo(tree);
    RefMuons_ety_ForwardReg.WriteTo(tree);
    RefMuons_sumet_ForwardReg.WriteTo(tree);
    RefMuons_phi_ForwardReg.WriteTo(tree);
    RefGamma_etx.WriteTo(tree);
    RefGamma_ety.WriteTo(tree);
    RefGamma_phi.WriteTo(tree);
    RefGamma_et.WriteTo(tree);
    RefGamma_sumet.WriteTo(tree);
    RefGamma_etx_CentralReg.WriteTo(tree);
    RefGamma_ety_CentralReg.WriteTo(tree);
    RefGamma_sumet_CentralReg.WriteTo(tree);
    RefGamma_phi_CentralReg.WriteTo(tree);
    RefGamma_etx_EndcapRegion.WriteTo(tree);
    RefGamma_ety_EndcapRegion.WriteTo(tree);
    RefGamma_sumet_EndcapRegion.WriteTo(tree);
    RefGamma_phi_EndcapRegion.WriteTo(tree);
    RefGamma_etx_ForwardReg.WriteTo(tree);
    RefGamma_ety_ForwardReg.WriteTo(tree);
    RefGamma_sumet_ForwardReg.WriteTo(tree);
    RefGamma_phi_ForwardReg.WriteTo(tree);
    RefTau_etx.WriteTo(tree);
    RefTau_ety.WriteTo(tree);
    RefTau_phi.WriteTo(tree);
    RefTau_et.WriteTo(tree);
    RefTau_sumet.WriteTo(tree);
    RefTau_etx_CentralReg.WriteTo(tree);
    RefTau_ety_CentralReg.WriteTo(tree);
    RefTau_sumet_CentralReg.WriteTo(tree);
    RefTau_phi_CentralReg.WriteTo(tree);
    RefTau_etx_EndcapRegion.WriteTo(tree);
    RefTau_ety_EndcapRegion.WriteTo(tree);
    RefTau_sumet_EndcapRegion.WriteTo(tree);
    RefTau_phi_EndcapRegion.WriteTo(tree);
    RefTau_etx_ForwardReg.WriteTo(tree);
    RefTau_ety_ForwardReg.WriteTo(tree);
    RefTau_sumet_ForwardReg.WriteTo(tree);
    RefTau_phi_ForwardReg.WriteTo(tree);
    RefFinal_em_etx.WriteTo(tree);
    RefFinal_em_ety.WriteTo(tree);
    RefFinal_em_phi.WriteTo(tree);
    RefFinal_em_et.WriteTo(tree);
    RefFinal_em_sumet.WriteTo(tree);
    RefFinal_em_etx_CentralReg.WriteTo(tree);
    RefFinal_em_ety_CentralReg.WriteTo(tree);
    RefFinal_em_sumet_CentralReg.WriteTo(tree);
    RefFinal_em_phi_CentralReg.WriteTo(tree);
    RefFinal_em_etx_EndcapRegion.WriteTo(tree);
    RefFinal_em_ety_EndcapRegion.WriteTo(tree);
    RefFinal_em_sumet_EndcapRegion.WriteTo(tree);
    RefFinal_em_phi_EndcapRegion.WriteTo(tree);
    RefFinal_em_etx_ForwardReg.WriteTo(tree);
    RefFinal_em_ety_ForwardReg.WriteTo(tree);
    RefFinal_em_sumet_ForwardReg.WriteTo(tree);
    RefFinal_em_phi_ForwardReg.WriteTo(tree);
    RefEle_em_etx.WriteTo(tree);
    RefEle_em_ety.WriteTo(tree);
    RefEle_em_phi.WriteTo(tree);
    RefEle_em_et.WriteTo(tree);
    RefEle_em_sumet.WriteTo(tree);
    RefEle_em_etx_CentralReg.WriteTo(tree);
    RefEle_em_ety_CentralReg.WriteTo(tree);
    RefEle_em_sumet_CentralReg.WriteTo(tree);
    RefEle_em_phi_CentralReg.WriteTo(tree);
    RefEle_em_etx_EndcapRegion.WriteTo(tree);
    RefEle_em_ety_EndcapRegion.WriteTo(tree);
    RefEle_em_sumet_EndcapRegion.WriteTo(tree);
    RefEle_em_phi_EndcapRegion.WriteTo(tree);
    RefEle_em_etx_ForwardReg.WriteTo(tree);
    RefEle_em_ety_ForwardReg.WriteTo(tree);
    RefEle_em_sumet_ForwardReg.WriteTo(tree);
    RefEle_em_phi_ForwardReg.WriteTo(tree);
    RefJet_em_etx.WriteTo(tree);
    RefJet_em_ety.WriteTo(tree);
    RefJet_em_phi.WriteTo(tree);
    RefJet_em_et.WriteTo(tree);
    RefJet_em_sumet.WriteTo(tree);
    RefJet_em_etx_CentralReg.WriteTo(tree);
    RefJet_em_ety_CentralReg.WriteTo(tree);
    RefJet_em_sumet_CentralReg.WriteTo(tree);
    RefJet_em_phi_CentralReg.WriteTo(tree);
    RefJet_em_etx_EndcapRegion.WriteTo(tree);
    RefJet_em_ety_EndcapRegion.WriteTo(tree);
    RefJet_em_sumet_EndcapRegion.WriteTo(tree);
    RefJet_em_phi_EndcapRegion.WriteTo(tree);
    RefJet_em_etx_ForwardReg.WriteTo(tree);
    RefJet_em_ety_ForwardReg.WriteTo(tree);
    RefJet_em_sumet_ForwardReg.WriteTo(tree);
    RefJet_em_phi_ForwardReg.WriteTo(tree);
    RefMuon_em_etx.WriteTo(tree);
    RefMuon_em_ety.WriteTo(tree);
    RefMuon_em_phi.WriteTo(tree);
    RefMuon_em_et.WriteTo(tree);
    RefMuon_em_sumet.WriteTo(tree);
    RefMuon_em_etx_CentralReg.WriteTo(tree);
    RefMuon_em_ety_CentralReg.WriteTo(tree);
    RefMuon_em_sumet_CentralReg.WriteTo(tree);
    RefMuon_em_phi_CentralReg.WriteTo(tree);
    RefMuon_em_etx_EndcapRegion.WriteTo(tree);
    RefMuon_em_ety_EndcapRegion.WriteTo(tree);
    RefMuon_em_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_em_phi_EndcapRegion.WriteTo(tree);
    RefMuon_em_etx_ForwardReg.WriteTo(tree);
    RefMuon_em_ety_ForwardReg.WriteTo(tree);
    RefMuon_em_sumet_ForwardReg.WriteTo(tree);
    RefMuon_em_phi_ForwardReg.WriteTo(tree);
    RefMuon_Track_em_etx.WriteTo(tree);
    RefMuon_Track_em_ety.WriteTo(tree);
    RefMuon_Track_em_phi.WriteTo(tree);
    RefMuon_Track_em_et.WriteTo(tree);
    RefMuon_Track_em_sumet.WriteTo(tree);
    RefMuon_Track_em_etx_CentralReg.WriteTo(tree);
    RefMuon_Track_em_ety_CentralReg.WriteTo(tree);
    RefMuon_Track_em_sumet_CentralReg.WriteTo(tree);
    RefMuon_Track_em_phi_CentralReg.WriteTo(tree);
    RefMuon_Track_em_etx_EndcapRegion.WriteTo(tree);
    RefMuon_Track_em_ety_EndcapRegion.WriteTo(tree);
    RefMuon_Track_em_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_Track_em_phi_EndcapRegion.WriteTo(tree);
    RefMuon_Track_em_etx_ForwardReg.WriteTo(tree);
    RefMuon_Track_em_ety_ForwardReg.WriteTo(tree);
    RefMuon_Track_em_sumet_ForwardReg.WriteTo(tree);
    RefMuon_Track_em_phi_ForwardReg.WriteTo(tree);
    RefGamma_em_etx.WriteTo(tree);
    RefGamma_em_ety.WriteTo(tree);
    RefGamma_em_phi.WriteTo(tree);
    RefGamma_em_et.WriteTo(tree);
    RefGamma_em_sumet.WriteTo(tree);
    RefGamma_em_etx_CentralReg.WriteTo(tree);
    RefGamma_em_ety_CentralReg.WriteTo(tree);
    RefGamma_em_sumet_CentralReg.WriteTo(tree);
    RefGamma_em_phi_CentralReg.WriteTo(tree);
    RefGamma_em_etx_EndcapRegion.WriteTo(tree);
    RefGamma_em_ety_EndcapRegion.WriteTo(tree);
    RefGamma_em_sumet_EndcapRegion.WriteTo(tree);
    RefGamma_em_phi_EndcapRegion.WriteTo(tree);
    RefGamma_em_etx_ForwardReg.WriteTo(tree);
    RefGamma_em_ety_ForwardReg.WriteTo(tree);
    RefGamma_em_sumet_ForwardReg.WriteTo(tree);
    RefGamma_em_phi_ForwardReg.WriteTo(tree);
    RefTau_em_etx.WriteTo(tree);
    RefTau_em_ety.WriteTo(tree);
    RefTau_em_phi.WriteTo(tree);
    RefTau_em_et.WriteTo(tree);
    RefTau_em_sumet.WriteTo(tree);
    RefTau_em_etx_CentralReg.WriteTo(tree);
    RefTau_em_ety_CentralReg.WriteTo(tree);
    RefTau_em_sumet_CentralReg.WriteTo(tree);
    RefTau_em_phi_CentralReg.WriteTo(tree);
    RefTau_em_etx_EndcapRegion.WriteTo(tree);
    RefTau_em_ety_EndcapRegion.WriteTo(tree);
    RefTau_em_sumet_EndcapRegion.WriteTo(tree);
    RefTau_em_phi_EndcapRegion.WriteTo(tree);
    RefTau_em_etx_ForwardReg.WriteTo(tree);
    RefTau_em_ety_ForwardReg.WriteTo(tree);
    RefTau_em_sumet_ForwardReg.WriteTo(tree);
    RefTau_em_phi_ForwardReg.WriteTo(tree);
    RefJet_JVF_etx.WriteTo(tree);
    RefJet_JVF_ety.WriteTo(tree);
    RefJet_JVF_phi.WriteTo(tree);
    RefJet_JVF_et.WriteTo(tree);
    RefJet_JVF_sumet.WriteTo(tree);
    RefJet_JVF_etx_CentralReg.WriteTo(tree);
    RefJet_JVF_ety_CentralReg.WriteTo(tree);
    RefJet_JVF_sumet_CentralReg.WriteTo(tree);
    RefJet_JVF_phi_CentralReg.WriteTo(tree);
    RefJet_JVF_etx_EndcapRegion.WriteTo(tree);
    RefJet_JVF_ety_EndcapRegion.WriteTo(tree);
    RefJet_JVF_sumet_EndcapRegion.WriteTo(tree);
    RefJet_JVF_phi_EndcapRegion.WriteTo(tree);
    RefJet_JVF_etx_ForwardReg.WriteTo(tree);
    RefJet_JVF_ety_ForwardReg.WriteTo(tree);
    RefJet_JVF_sumet_ForwardReg.WriteTo(tree);
    RefJet_JVF_phi_ForwardReg.WriteTo(tree);
    RefJet_JVFCut_etx.WriteTo(tree);
    RefJet_JVFCut_ety.WriteTo(tree);
    RefJet_JVFCut_phi.WriteTo(tree);
    RefJet_JVFCut_et.WriteTo(tree);
    RefJet_JVFCut_sumet.WriteTo(tree);
    RefJet_JVFCut_etx_CentralReg.WriteTo(tree);
    RefJet_JVFCut_ety_CentralReg.WriteTo(tree);
    RefJet_JVFCut_sumet_CentralReg.WriteTo(tree);
    RefJet_JVFCut_phi_CentralReg.WriteTo(tree);
    RefJet_JVFCut_etx_EndcapRegion.WriteTo(tree);
    RefJet_JVFCut_ety_EndcapRegion.WriteTo(tree);
    RefJet_JVFCut_sumet_EndcapRegion.WriteTo(tree);
    RefJet_JVFCut_phi_EndcapRegion.WriteTo(tree);
    RefJet_JVFCut_etx_ForwardReg.WriteTo(tree);
    RefJet_JVFCut_ety_ForwardReg.WriteTo(tree);
    RefJet_JVFCut_sumet_ForwardReg.WriteTo(tree);
    RefJet_JVFCut_phi_ForwardReg.WriteTo(tree);
    CellOut_Eflow_STVF_etx.WriteTo(tree);
    CellOut_Eflow_STVF_ety.WriteTo(tree);
    CellOut_Eflow_STVF_phi.WriteTo(tree);
    CellOut_Eflow_STVF_et.WriteTo(tree);
    CellOut_Eflow_STVF_sumet.WriteTo(tree);
    CellOut_Eflow_STVF_etx_CentralReg.WriteTo(tree);
    CellOut_Eflow_STVF_ety_CentralReg.WriteTo(tree);
    CellOut_Eflow_STVF_sumet_CentralReg.WriteTo(tree);
    CellOut_Eflow_STVF_phi_CentralReg.WriteTo(tree);
    CellOut_Eflow_STVF_etx_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_STVF_ety_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_STVF_sumet_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_STVF_phi_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_STVF_etx_ForwardReg.WriteTo(tree);
    CellOut_Eflow_STVF_ety_ForwardReg.WriteTo(tree);
    CellOut_Eflow_STVF_sumet_ForwardReg.WriteTo(tree);
    CellOut_Eflow_STVF_phi_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetArea_etx.WriteTo(tree);
    CellOut_Eflow_JetArea_ety.WriteTo(tree);
    CellOut_Eflow_JetArea_phi.WriteTo(tree);
    CellOut_Eflow_JetArea_et.WriteTo(tree);
    CellOut_Eflow_JetArea_sumet.WriteTo(tree);
    CellOut_Eflow_JetArea_etx_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetArea_ety_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetArea_sumet_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetArea_phi_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetArea_etx_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetArea_ety_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetArea_sumet_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetArea_phi_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetArea_etx_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetArea_ety_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetArea_sumet_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetArea_phi_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_etx.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_ety.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_phi.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_et.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_sumet.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_etx_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_ety_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_sumet_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_phi_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_etx_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_ety_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_phi_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_etx_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_ety_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_sumet_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetAreaJVF_phi_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_etx.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_ety.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_phi.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_et.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg.WriteTo(tree);
    CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg.WriteTo(tree);
    RefFinal_STVF_etx.WriteTo(tree);
    RefFinal_STVF_ety.WriteTo(tree);
    RefFinal_STVF_phi.WriteTo(tree);
    RefFinal_STVF_et.WriteTo(tree);
    RefFinal_STVF_sumet.WriteTo(tree);
    RefFinal_STVF_etx_CentralReg.WriteTo(tree);
    RefFinal_STVF_ety_CentralReg.WriteTo(tree);
    RefFinal_STVF_sumet_CentralReg.WriteTo(tree);
    RefFinal_STVF_phi_CentralReg.WriteTo(tree);
    RefFinal_STVF_etx_EndcapRegion.WriteTo(tree);
    RefFinal_STVF_ety_EndcapRegion.WriteTo(tree);
    RefFinal_STVF_sumet_EndcapRegion.WriteTo(tree);
    RefFinal_STVF_phi_EndcapRegion.WriteTo(tree);
    RefFinal_STVF_etx_ForwardReg.WriteTo(tree);
    RefFinal_STVF_ety_ForwardReg.WriteTo(tree);
    RefFinal_STVF_sumet_ForwardReg.WriteTo(tree);
    RefFinal_STVF_phi_ForwardReg.WriteTo(tree);
    CellOut_Eflow_etx.WriteTo(tree);
    CellOut_Eflow_ety.WriteTo(tree);
    CellOut_Eflow_phi.WriteTo(tree);
    CellOut_Eflow_et.WriteTo(tree);
    CellOut_Eflow_sumet.WriteTo(tree);
    CellOut_Eflow_etx_CentralReg.WriteTo(tree);
    CellOut_Eflow_ety_CentralReg.WriteTo(tree);
    CellOut_Eflow_sumet_CentralReg.WriteTo(tree);
    CellOut_Eflow_phi_CentralReg.WriteTo(tree);
    CellOut_Eflow_etx_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_ety_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_sumet_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_phi_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_etx_ForwardReg.WriteTo(tree);
    CellOut_Eflow_ety_ForwardReg.WriteTo(tree);
    CellOut_Eflow_sumet_ForwardReg.WriteTo(tree);
    CellOut_Eflow_phi_ForwardReg.WriteTo(tree);
    CellOut_Eflow_Muid_etx.WriteTo(tree);
    CellOut_Eflow_Muid_ety.WriteTo(tree);
    CellOut_Eflow_Muid_phi.WriteTo(tree);
    CellOut_Eflow_Muid_et.WriteTo(tree);
    CellOut_Eflow_Muid_sumet.WriteTo(tree);
    CellOut_Eflow_Muid_etx_CentralReg.WriteTo(tree);
    CellOut_Eflow_Muid_ety_CentralReg.WriteTo(tree);
    CellOut_Eflow_Muid_sumet_CentralReg.WriteTo(tree);
    CellOut_Eflow_Muid_phi_CentralReg.WriteTo(tree);
    CellOut_Eflow_Muid_etx_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_Muid_ety_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_Muid_sumet_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_Muid_phi_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_Muid_etx_ForwardReg.WriteTo(tree);
    CellOut_Eflow_Muid_ety_ForwardReg.WriteTo(tree);
    CellOut_Eflow_Muid_sumet_ForwardReg.WriteTo(tree);
    CellOut_Eflow_Muid_phi_ForwardReg.WriteTo(tree);
    CellOut_Eflow_Muons_etx.WriteTo(tree);
    CellOut_Eflow_Muons_ety.WriteTo(tree);
    CellOut_Eflow_Muons_phi.WriteTo(tree);
    CellOut_Eflow_Muons_et.WriteTo(tree);
    CellOut_Eflow_Muons_sumet.WriteTo(tree);
    CellOut_Eflow_Muons_etx_CentralReg.WriteTo(tree);
    CellOut_Eflow_Muons_ety_CentralReg.WriteTo(tree);
    CellOut_Eflow_Muons_sumet_CentralReg.WriteTo(tree);
    CellOut_Eflow_Muons_phi_CentralReg.WriteTo(tree);
    CellOut_Eflow_Muons_etx_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_Muons_ety_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_Muons_sumet_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_Muons_phi_EndcapRegion.WriteTo(tree);
    CellOut_Eflow_Muons_etx_ForwardReg.WriteTo(tree);
    CellOut_Eflow_Muons_ety_ForwardReg.WriteTo(tree);
    CellOut_Eflow_Muons_sumet_ForwardReg.WriteTo(tree);
    CellOut_Eflow_Muons_phi_ForwardReg.WriteTo(tree);
    RefMuon_Track_etx.WriteTo(tree);
    RefMuon_Track_ety.WriteTo(tree);
    RefMuon_Track_phi.WriteTo(tree);
    RefMuon_Track_et.WriteTo(tree);
    RefMuon_Track_sumet.WriteTo(tree);
    RefMuon_Track_etx_CentralReg.WriteTo(tree);
    RefMuon_Track_ety_CentralReg.WriteTo(tree);
    RefMuon_Track_sumet_CentralReg.WriteTo(tree);
    RefMuon_Track_phi_CentralReg.WriteTo(tree);
    RefMuon_Track_etx_EndcapRegion.WriteTo(tree);
    RefMuon_Track_ety_EndcapRegion.WriteTo(tree);
    RefMuon_Track_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_Track_phi_EndcapRegion.WriteTo(tree);
    RefMuon_Track_etx_ForwardReg.WriteTo(tree);
    RefMuon_Track_ety_ForwardReg.WriteTo(tree);
    RefMuon_Track_sumet_ForwardReg.WriteTo(tree);
    RefMuon_Track_phi_ForwardReg.WriteTo(tree);
    RefMuon_Track_Staco_etx.WriteTo(tree);
    RefMuon_Track_Staco_ety.WriteTo(tree);
    RefMuon_Track_Staco_phi.WriteTo(tree);
    RefMuon_Track_Staco_et.WriteTo(tree);
    RefMuon_Track_Staco_sumet.WriteTo(tree);
    RefMuon_Track_Staco_etx_CentralReg.WriteTo(tree);
    RefMuon_Track_Staco_ety_CentralReg.WriteTo(tree);
    RefMuon_Track_Staco_sumet_CentralReg.WriteTo(tree);
    RefMuon_Track_Staco_phi_CentralReg.WriteTo(tree);
    RefMuon_Track_Staco_etx_EndcapRegion.WriteTo(tree);
    RefMuon_Track_Staco_ety_EndcapRegion.WriteTo(tree);
    RefMuon_Track_Staco_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_Track_Staco_phi_EndcapRegion.WriteTo(tree);
    RefMuon_Track_Staco_etx_ForwardReg.WriteTo(tree);
    RefMuon_Track_Staco_ety_ForwardReg.WriteTo(tree);
    RefMuon_Track_Staco_sumet_ForwardReg.WriteTo(tree);
    RefMuon_Track_Staco_phi_ForwardReg.WriteTo(tree);
    RefMuon_Track_Muid_etx.WriteTo(tree);
    RefMuon_Track_Muid_ety.WriteTo(tree);
    RefMuon_Track_Muid_phi.WriteTo(tree);
    RefMuon_Track_Muid_et.WriteTo(tree);
    RefMuon_Track_Muid_sumet.WriteTo(tree);
    RefMuon_Track_Muid_etx_CentralReg.WriteTo(tree);
    RefMuon_Track_Muid_ety_CentralReg.WriteTo(tree);
    RefMuon_Track_Muid_sumet_CentralReg.WriteTo(tree);
    RefMuon_Track_Muid_phi_CentralReg.WriteTo(tree);
    RefMuon_Track_Muid_etx_EndcapRegion.WriteTo(tree);
    RefMuon_Track_Muid_ety_EndcapRegion.WriteTo(tree);
    RefMuon_Track_Muid_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_Track_Muid_phi_EndcapRegion.WriteTo(tree);
    RefMuon_Track_Muid_etx_ForwardReg.WriteTo(tree);
    RefMuon_Track_Muid_ety_ForwardReg.WriteTo(tree);
    RefMuon_Track_Muid_sumet_ForwardReg.WriteTo(tree);
    RefMuon_Track_Muid_phi_ForwardReg.WriteTo(tree);
    RefMuons_Track_etx.WriteTo(tree);
    RefMuons_Track_ety.WriteTo(tree);
    RefMuons_Track_phi.WriteTo(tree);
    RefMuons_Track_et.WriteTo(tree);
    RefMuons_Track_sumet.WriteTo(tree);
    RefMuons_Track_etx_CentralReg.WriteTo(tree);
    RefMuons_Track_ety_CentralReg.WriteTo(tree);
    RefMuons_Track_sumet_CentralReg.WriteTo(tree);
    RefMuons_Track_phi_CentralReg.WriteTo(tree);
    RefMuons_Track_etx_EndcapRegion.WriteTo(tree);
    RefMuons_Track_ety_EndcapRegion.WriteTo(tree);
    RefMuons_Track_sumet_EndcapRegion.WriteTo(tree);
    RefMuons_Track_phi_EndcapRegion.WriteTo(tree);
    RefMuons_Track_etx_ForwardReg.WriteTo(tree);
    RefMuons_Track_ety_ForwardReg.WriteTo(tree);
    RefMuons_Track_sumet_ForwardReg.WriteTo(tree);
    RefMuons_Track_phi_ForwardReg.WriteTo(tree);
    RefFinal_BDTMedium_etx.WriteTo(tree);
    RefFinal_BDTMedium_ety.WriteTo(tree);
    RefFinal_BDTMedium_phi.WriteTo(tree);
    RefFinal_BDTMedium_et.WriteTo(tree);
    RefFinal_BDTMedium_sumet.WriteTo(tree);
    RefFinal_BDTMedium_etx_CentralReg.WriteTo(tree);
    RefFinal_BDTMedium_ety_CentralReg.WriteTo(tree);
    RefFinal_BDTMedium_sumet_CentralReg.WriteTo(tree);
    RefFinal_BDTMedium_phi_CentralReg.WriteTo(tree);
    RefFinal_BDTMedium_etx_EndcapRegion.WriteTo(tree);
    RefFinal_BDTMedium_ety_EndcapRegion.WriteTo(tree);
    RefFinal_BDTMedium_sumet_EndcapRegion.WriteTo(tree);
    RefFinal_BDTMedium_phi_EndcapRegion.WriteTo(tree);
    RefFinal_BDTMedium_etx_ForwardReg.WriteTo(tree);
    RefFinal_BDTMedium_ety_ForwardReg.WriteTo(tree);
    RefFinal_BDTMedium_sumet_ForwardReg.WriteTo(tree);
    RefFinal_BDTMedium_phi_ForwardReg.WriteTo(tree);
    RefGamma_BDTMedium_etx.WriteTo(tree);
    RefGamma_BDTMedium_ety.WriteTo(tree);
    RefGamma_BDTMedium_phi.WriteTo(tree);
    RefGamma_BDTMedium_et.WriteTo(tree);
    RefGamma_BDTMedium_sumet.WriteTo(tree);
    RefGamma_BDTMedium_etx_CentralReg.WriteTo(tree);
    RefGamma_BDTMedium_ety_CentralReg.WriteTo(tree);
    RefGamma_BDTMedium_sumet_CentralReg.WriteTo(tree);
    RefGamma_BDTMedium_phi_CentralReg.WriteTo(tree);
    RefGamma_BDTMedium_etx_EndcapRegion.WriteTo(tree);
    RefGamma_BDTMedium_ety_EndcapRegion.WriteTo(tree);
    RefGamma_BDTMedium_sumet_EndcapRegion.WriteTo(tree);
    RefGamma_BDTMedium_phi_EndcapRegion.WriteTo(tree);
    RefGamma_BDTMedium_etx_ForwardReg.WriteTo(tree);
    RefGamma_BDTMedium_ety_ForwardReg.WriteTo(tree);
    RefGamma_BDTMedium_sumet_ForwardReg.WriteTo(tree);
    RefGamma_BDTMedium_phi_ForwardReg.WriteTo(tree);
    RefEle_BDTMedium_etx.WriteTo(tree);
    RefEle_BDTMedium_ety.WriteTo(tree);
    RefEle_BDTMedium_phi.WriteTo(tree);
    RefEle_BDTMedium_et.WriteTo(tree);
    RefEle_BDTMedium_sumet.WriteTo(tree);
    RefEle_BDTMedium_etx_CentralReg.WriteTo(tree);
    RefEle_BDTMedium_ety_CentralReg.WriteTo(tree);
    RefEle_BDTMedium_sumet_CentralReg.WriteTo(tree);
    RefEle_BDTMedium_phi_CentralReg.WriteTo(tree);
    RefEle_BDTMedium_etx_EndcapRegion.WriteTo(tree);
    RefEle_BDTMedium_ety_EndcapRegion.WriteTo(tree);
    RefEle_BDTMedium_sumet_EndcapRegion.WriteTo(tree);
    RefEle_BDTMedium_phi_EndcapRegion.WriteTo(tree);
    RefEle_BDTMedium_etx_ForwardReg.WriteTo(tree);
    RefEle_BDTMedium_ety_ForwardReg.WriteTo(tree);
    RefEle_BDTMedium_sumet_ForwardReg.WriteTo(tree);
    RefEle_BDTMedium_phi_ForwardReg.WriteTo(tree);
    RefTau_BDTMedium_etx.WriteTo(tree);
    RefTau_BDTMedium_ety.WriteTo(tree);
    RefTau_BDTMedium_phi.WriteTo(tree);
    RefTau_BDTMedium_et.WriteTo(tree);
    RefTau_BDTMedium_sumet.WriteTo(tree);
    RefTau_BDTMedium_etx_CentralReg.WriteTo(tree);
    RefTau_BDTMedium_ety_CentralReg.WriteTo(tree);
    RefTau_BDTMedium_sumet_CentralReg.WriteTo(tree);
    RefTau_BDTMedium_phi_CentralReg.WriteTo(tree);
    RefTau_BDTMedium_etx_EndcapRegion.WriteTo(tree);
    RefTau_BDTMedium_ety_EndcapRegion.WriteTo(tree);
    RefTau_BDTMedium_sumet_EndcapRegion.WriteTo(tree);
    RefTau_BDTMedium_phi_EndcapRegion.WriteTo(tree);
    RefTau_BDTMedium_etx_ForwardReg.WriteTo(tree);
    RefTau_BDTMedium_ety_ForwardReg.WriteTo(tree);
    RefTau_BDTMedium_sumet_ForwardReg.WriteTo(tree);
    RefTau_BDTMedium_phi_ForwardReg.WriteTo(tree);
    RefJet_BDTMedium_etx.WriteTo(tree);
    RefJet_BDTMedium_ety.WriteTo(tree);
    RefJet_BDTMedium_phi.WriteTo(tree);
    RefJet_BDTMedium_et.WriteTo(tree);
    RefJet_BDTMedium_sumet.WriteTo(tree);
    RefJet_BDTMedium_etx_CentralReg.WriteTo(tree);
    RefJet_BDTMedium_ety_CentralReg.WriteTo(tree);
    RefJet_BDTMedium_sumet_CentralReg.WriteTo(tree);
    RefJet_BDTMedium_phi_CentralReg.WriteTo(tree);
    RefJet_BDTMedium_etx_EndcapRegion.WriteTo(tree);
    RefJet_BDTMedium_ety_EndcapRegion.WriteTo(tree);
    RefJet_BDTMedium_sumet_EndcapRegion.WriteTo(tree);
    RefJet_BDTMedium_phi_EndcapRegion.WriteTo(tree);
    RefJet_BDTMedium_etx_ForwardReg.WriteTo(tree);
    RefJet_BDTMedium_ety_ForwardReg.WriteTo(tree);
    RefJet_BDTMedium_sumet_ForwardReg.WriteTo(tree);
    RefJet_BDTMedium_phi_ForwardReg.WriteTo(tree);
    RefMuon_Staco_BDTMedium_etx.WriteTo(tree);
    RefMuon_Staco_BDTMedium_ety.WriteTo(tree);
    RefMuon_Staco_BDTMedium_phi.WriteTo(tree);
    RefMuon_Staco_BDTMedium_et.WriteTo(tree);
    RefMuon_Staco_BDTMedium_sumet.WriteTo(tree);
    RefMuon_Staco_BDTMedium_etx_CentralReg.WriteTo(tree);
    RefMuon_Staco_BDTMedium_ety_CentralReg.WriteTo(tree);
    RefMuon_Staco_BDTMedium_sumet_CentralReg.WriteTo(tree);
    RefMuon_Staco_BDTMedium_phi_CentralReg.WriteTo(tree);
    RefMuon_Staco_BDTMedium_etx_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_BDTMedium_ety_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_BDTMedium_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_BDTMedium_phi_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_BDTMedium_etx_ForwardReg.WriteTo(tree);
    RefMuon_Staco_BDTMedium_ety_ForwardReg.WriteTo(tree);
    RefMuon_Staco_BDTMedium_sumet_ForwardReg.WriteTo(tree);
    RefMuon_Staco_BDTMedium_phi_ForwardReg.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_etx.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_ety.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_phi.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_et.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_sumet.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg.WriteTo(tree);
    RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_etx.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_ety.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_phi.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_et.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_sumet.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg.WriteTo(tree);
    RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_etx.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_ety.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_phi.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_et.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_sumet.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg.WriteTo(tree);
    RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_etx.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_ety.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_phi.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_et.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_sumet.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg.WriteTo(tree);
    RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_etx.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_ety.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_phi.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_et.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_sumet.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg.WriteTo(tree);
    RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_et.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg.WriteTo(tree);
    RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void MET::SetActive(bool active)
  {
    if(active)
    {
     if(RefFinal_etx.IsAvailable()) RefFinal_etx.SetActive(active);
     if(RefFinal_ety.IsAvailable()) RefFinal_ety.SetActive(active);
     if(RefFinal_phi.IsAvailable()) RefFinal_phi.SetActive(active);
     if(RefFinal_et.IsAvailable()) RefFinal_et.SetActive(active);
     if(RefFinal_sumet.IsAvailable()) RefFinal_sumet.SetActive(active);
     if(RefFinal_etx_CentralReg.IsAvailable()) RefFinal_etx_CentralReg.SetActive(active);
     if(RefFinal_ety_CentralReg.IsAvailable()) RefFinal_ety_CentralReg.SetActive(active);
     if(RefFinal_sumet_CentralReg.IsAvailable()) RefFinal_sumet_CentralReg.SetActive(active);
     if(RefFinal_phi_CentralReg.IsAvailable()) RefFinal_phi_CentralReg.SetActive(active);
     if(RefFinal_etx_EndcapRegion.IsAvailable()) RefFinal_etx_EndcapRegion.SetActive(active);
     if(RefFinal_ety_EndcapRegion.IsAvailable()) RefFinal_ety_EndcapRegion.SetActive(active);
     if(RefFinal_sumet_EndcapRegion.IsAvailable()) RefFinal_sumet_EndcapRegion.SetActive(active);
     if(RefFinal_phi_EndcapRegion.IsAvailable()) RefFinal_phi_EndcapRegion.SetActive(active);
     if(RefFinal_etx_ForwardReg.IsAvailable()) RefFinal_etx_ForwardReg.SetActive(active);
     if(RefFinal_ety_ForwardReg.IsAvailable()) RefFinal_ety_ForwardReg.SetActive(active);
     if(RefFinal_sumet_ForwardReg.IsAvailable()) RefFinal_sumet_ForwardReg.SetActive(active);
     if(RefFinal_phi_ForwardReg.IsAvailable()) RefFinal_phi_ForwardReg.SetActive(active);
     if(RefEle_etx.IsAvailable()) RefEle_etx.SetActive(active);
     if(RefEle_ety.IsAvailable()) RefEle_ety.SetActive(active);
     if(RefEle_phi.IsAvailable()) RefEle_phi.SetActive(active);
     if(RefEle_et.IsAvailable()) RefEle_et.SetActive(active);
     if(RefEle_sumet.IsAvailable()) RefEle_sumet.SetActive(active);
     if(RefEle_etx_CentralReg.IsAvailable()) RefEle_etx_CentralReg.SetActive(active);
     if(RefEle_ety_CentralReg.IsAvailable()) RefEle_ety_CentralReg.SetActive(active);
     if(RefEle_sumet_CentralReg.IsAvailable()) RefEle_sumet_CentralReg.SetActive(active);
     if(RefEle_phi_CentralReg.IsAvailable()) RefEle_phi_CentralReg.SetActive(active);
     if(RefEle_etx_EndcapRegion.IsAvailable()) RefEle_etx_EndcapRegion.SetActive(active);
     if(RefEle_ety_EndcapRegion.IsAvailable()) RefEle_ety_EndcapRegion.SetActive(active);
     if(RefEle_sumet_EndcapRegion.IsAvailable()) RefEle_sumet_EndcapRegion.SetActive(active);
     if(RefEle_phi_EndcapRegion.IsAvailable()) RefEle_phi_EndcapRegion.SetActive(active);
     if(RefEle_etx_ForwardReg.IsAvailable()) RefEle_etx_ForwardReg.SetActive(active);
     if(RefEle_ety_ForwardReg.IsAvailable()) RefEle_ety_ForwardReg.SetActive(active);
     if(RefEle_sumet_ForwardReg.IsAvailable()) RefEle_sumet_ForwardReg.SetActive(active);
     if(RefEle_phi_ForwardReg.IsAvailable()) RefEle_phi_ForwardReg.SetActive(active);
     if(RefJet_etx.IsAvailable()) RefJet_etx.SetActive(active);
     if(RefJet_ety.IsAvailable()) RefJet_ety.SetActive(active);
     if(RefJet_phi.IsAvailable()) RefJet_phi.SetActive(active);
     if(RefJet_et.IsAvailable()) RefJet_et.SetActive(active);
     if(RefJet_sumet.IsAvailable()) RefJet_sumet.SetActive(active);
     if(RefJet_etx_CentralReg.IsAvailable()) RefJet_etx_CentralReg.SetActive(active);
     if(RefJet_ety_CentralReg.IsAvailable()) RefJet_ety_CentralReg.SetActive(active);
     if(RefJet_sumet_CentralReg.IsAvailable()) RefJet_sumet_CentralReg.SetActive(active);
     if(RefJet_phi_CentralReg.IsAvailable()) RefJet_phi_CentralReg.SetActive(active);
     if(RefJet_etx_EndcapRegion.IsAvailable()) RefJet_etx_EndcapRegion.SetActive(active);
     if(RefJet_ety_EndcapRegion.IsAvailable()) RefJet_ety_EndcapRegion.SetActive(active);
     if(RefJet_sumet_EndcapRegion.IsAvailable()) RefJet_sumet_EndcapRegion.SetActive(active);
     if(RefJet_phi_EndcapRegion.IsAvailable()) RefJet_phi_EndcapRegion.SetActive(active);
     if(RefJet_etx_ForwardReg.IsAvailable()) RefJet_etx_ForwardReg.SetActive(active);
     if(RefJet_ety_ForwardReg.IsAvailable()) RefJet_ety_ForwardReg.SetActive(active);
     if(RefJet_sumet_ForwardReg.IsAvailable()) RefJet_sumet_ForwardReg.SetActive(active);
     if(RefJet_phi_ForwardReg.IsAvailable()) RefJet_phi_ForwardReg.SetActive(active);
     if(RefMuon_etx.IsAvailable()) RefMuon_etx.SetActive(active);
     if(RefMuon_ety.IsAvailable()) RefMuon_ety.SetActive(active);
     if(RefMuon_phi.IsAvailable()) RefMuon_phi.SetActive(active);
     if(RefMuon_et.IsAvailable()) RefMuon_et.SetActive(active);
     if(RefMuon_sumet.IsAvailable()) RefMuon_sumet.SetActive(active);
     if(RefMuon_etx_CentralReg.IsAvailable()) RefMuon_etx_CentralReg.SetActive(active);
     if(RefMuon_ety_CentralReg.IsAvailable()) RefMuon_ety_CentralReg.SetActive(active);
     if(RefMuon_sumet_CentralReg.IsAvailable()) RefMuon_sumet_CentralReg.SetActive(active);
     if(RefMuon_phi_CentralReg.IsAvailable()) RefMuon_phi_CentralReg.SetActive(active);
     if(RefMuon_etx_EndcapRegion.IsAvailable()) RefMuon_etx_EndcapRegion.SetActive(active);
     if(RefMuon_ety_EndcapRegion.IsAvailable()) RefMuon_ety_EndcapRegion.SetActive(active);
     if(RefMuon_sumet_EndcapRegion.IsAvailable()) RefMuon_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_phi_EndcapRegion.IsAvailable()) RefMuon_phi_EndcapRegion.SetActive(active);
     if(RefMuon_etx_ForwardReg.IsAvailable()) RefMuon_etx_ForwardReg.SetActive(active);
     if(RefMuon_ety_ForwardReg.IsAvailable()) RefMuon_ety_ForwardReg.SetActive(active);
     if(RefMuon_sumet_ForwardReg.IsAvailable()) RefMuon_sumet_ForwardReg.SetActive(active);
     if(RefMuon_phi_ForwardReg.IsAvailable()) RefMuon_phi_ForwardReg.SetActive(active);
     if(RefMuon_Staco_etx.IsAvailable()) RefMuon_Staco_etx.SetActive(active);
     if(RefMuon_Staco_ety.IsAvailable()) RefMuon_Staco_ety.SetActive(active);
     if(RefMuon_Staco_phi.IsAvailable()) RefMuon_Staco_phi.SetActive(active);
     if(RefMuon_Staco_et.IsAvailable()) RefMuon_Staco_et.SetActive(active);
     if(RefMuon_Staco_sumet.IsAvailable()) RefMuon_Staco_sumet.SetActive(active);
     if(RefMuon_Staco_etx_CentralReg.IsAvailable()) RefMuon_Staco_etx_CentralReg.SetActive(active);
     if(RefMuon_Staco_ety_CentralReg.IsAvailable()) RefMuon_Staco_ety_CentralReg.SetActive(active);
     if(RefMuon_Staco_sumet_CentralReg.IsAvailable()) RefMuon_Staco_sumet_CentralReg.SetActive(active);
     if(RefMuon_Staco_phi_CentralReg.IsAvailable()) RefMuon_Staco_phi_CentralReg.SetActive(active);
     if(RefMuon_Staco_etx_EndcapRegion.IsAvailable()) RefMuon_Staco_etx_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_ety_EndcapRegion.IsAvailable()) RefMuon_Staco_ety_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_sumet_EndcapRegion.IsAvailable()) RefMuon_Staco_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_phi_EndcapRegion.IsAvailable()) RefMuon_Staco_phi_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_etx_ForwardReg.IsAvailable()) RefMuon_Staco_etx_ForwardReg.SetActive(active);
     if(RefMuon_Staco_ety_ForwardReg.IsAvailable()) RefMuon_Staco_ety_ForwardReg.SetActive(active);
     if(RefMuon_Staco_sumet_ForwardReg.IsAvailable()) RefMuon_Staco_sumet_ForwardReg.SetActive(active);
     if(RefMuon_Staco_phi_ForwardReg.IsAvailable()) RefMuon_Staco_phi_ForwardReg.SetActive(active);
     if(RefMuon_Muid_etx.IsAvailable()) RefMuon_Muid_etx.SetActive(active);
     if(RefMuon_Muid_ety.IsAvailable()) RefMuon_Muid_ety.SetActive(active);
     if(RefMuon_Muid_phi.IsAvailable()) RefMuon_Muid_phi.SetActive(active);
     if(RefMuon_Muid_et.IsAvailable()) RefMuon_Muid_et.SetActive(active);
     if(RefMuon_Muid_sumet.IsAvailable()) RefMuon_Muid_sumet.SetActive(active);
     if(RefMuon_Muid_etx_CentralReg.IsAvailable()) RefMuon_Muid_etx_CentralReg.SetActive(active);
     if(RefMuon_Muid_ety_CentralReg.IsAvailable()) RefMuon_Muid_ety_CentralReg.SetActive(active);
     if(RefMuon_Muid_sumet_CentralReg.IsAvailable()) RefMuon_Muid_sumet_CentralReg.SetActive(active);
     if(RefMuon_Muid_phi_CentralReg.IsAvailable()) RefMuon_Muid_phi_CentralReg.SetActive(active);
     if(RefMuon_Muid_etx_EndcapRegion.IsAvailable()) RefMuon_Muid_etx_EndcapRegion.SetActive(active);
     if(RefMuon_Muid_ety_EndcapRegion.IsAvailable()) RefMuon_Muid_ety_EndcapRegion.SetActive(active);
     if(RefMuon_Muid_sumet_EndcapRegion.IsAvailable()) RefMuon_Muid_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_Muid_phi_EndcapRegion.IsAvailable()) RefMuon_Muid_phi_EndcapRegion.SetActive(active);
     if(RefMuon_Muid_etx_ForwardReg.IsAvailable()) RefMuon_Muid_etx_ForwardReg.SetActive(active);
     if(RefMuon_Muid_ety_ForwardReg.IsAvailable()) RefMuon_Muid_ety_ForwardReg.SetActive(active);
     if(RefMuon_Muid_sumet_ForwardReg.IsAvailable()) RefMuon_Muid_sumet_ForwardReg.SetActive(active);
     if(RefMuon_Muid_phi_ForwardReg.IsAvailable()) RefMuon_Muid_phi_ForwardReg.SetActive(active);
     if(RefMuons_etx.IsAvailable()) RefMuons_etx.SetActive(active);
     if(RefMuons_ety.IsAvailable()) RefMuons_ety.SetActive(active);
     if(RefMuons_phi.IsAvailable()) RefMuons_phi.SetActive(active);
     if(RefMuons_et.IsAvailable()) RefMuons_et.SetActive(active);
     if(RefMuons_sumet.IsAvailable()) RefMuons_sumet.SetActive(active);
     if(RefMuons_etx_CentralReg.IsAvailable()) RefMuons_etx_CentralReg.SetActive(active);
     if(RefMuons_ety_CentralReg.IsAvailable()) RefMuons_ety_CentralReg.SetActive(active);
     if(RefMuons_sumet_CentralReg.IsAvailable()) RefMuons_sumet_CentralReg.SetActive(active);
     if(RefMuons_phi_CentralReg.IsAvailable()) RefMuons_phi_CentralReg.SetActive(active);
     if(RefMuons_etx_EndcapRegion.IsAvailable()) RefMuons_etx_EndcapRegion.SetActive(active);
     if(RefMuons_ety_EndcapRegion.IsAvailable()) RefMuons_ety_EndcapRegion.SetActive(active);
     if(RefMuons_sumet_EndcapRegion.IsAvailable()) RefMuons_sumet_EndcapRegion.SetActive(active);
     if(RefMuons_phi_EndcapRegion.IsAvailable()) RefMuons_phi_EndcapRegion.SetActive(active);
     if(RefMuons_etx_ForwardReg.IsAvailable()) RefMuons_etx_ForwardReg.SetActive(active);
     if(RefMuons_ety_ForwardReg.IsAvailable()) RefMuons_ety_ForwardReg.SetActive(active);
     if(RefMuons_sumet_ForwardReg.IsAvailable()) RefMuons_sumet_ForwardReg.SetActive(active);
     if(RefMuons_phi_ForwardReg.IsAvailable()) RefMuons_phi_ForwardReg.SetActive(active);
     if(RefGamma_etx.IsAvailable()) RefGamma_etx.SetActive(active);
     if(RefGamma_ety.IsAvailable()) RefGamma_ety.SetActive(active);
     if(RefGamma_phi.IsAvailable()) RefGamma_phi.SetActive(active);
     if(RefGamma_et.IsAvailable()) RefGamma_et.SetActive(active);
     if(RefGamma_sumet.IsAvailable()) RefGamma_sumet.SetActive(active);
     if(RefGamma_etx_CentralReg.IsAvailable()) RefGamma_etx_CentralReg.SetActive(active);
     if(RefGamma_ety_CentralReg.IsAvailable()) RefGamma_ety_CentralReg.SetActive(active);
     if(RefGamma_sumet_CentralReg.IsAvailable()) RefGamma_sumet_CentralReg.SetActive(active);
     if(RefGamma_phi_CentralReg.IsAvailable()) RefGamma_phi_CentralReg.SetActive(active);
     if(RefGamma_etx_EndcapRegion.IsAvailable()) RefGamma_etx_EndcapRegion.SetActive(active);
     if(RefGamma_ety_EndcapRegion.IsAvailable()) RefGamma_ety_EndcapRegion.SetActive(active);
     if(RefGamma_sumet_EndcapRegion.IsAvailable()) RefGamma_sumet_EndcapRegion.SetActive(active);
     if(RefGamma_phi_EndcapRegion.IsAvailable()) RefGamma_phi_EndcapRegion.SetActive(active);
     if(RefGamma_etx_ForwardReg.IsAvailable()) RefGamma_etx_ForwardReg.SetActive(active);
     if(RefGamma_ety_ForwardReg.IsAvailable()) RefGamma_ety_ForwardReg.SetActive(active);
     if(RefGamma_sumet_ForwardReg.IsAvailable()) RefGamma_sumet_ForwardReg.SetActive(active);
     if(RefGamma_phi_ForwardReg.IsAvailable()) RefGamma_phi_ForwardReg.SetActive(active);
     if(RefTau_etx.IsAvailable()) RefTau_etx.SetActive(active);
     if(RefTau_ety.IsAvailable()) RefTau_ety.SetActive(active);
     if(RefTau_phi.IsAvailable()) RefTau_phi.SetActive(active);
     if(RefTau_et.IsAvailable()) RefTau_et.SetActive(active);
     if(RefTau_sumet.IsAvailable()) RefTau_sumet.SetActive(active);
     if(RefTau_etx_CentralReg.IsAvailable()) RefTau_etx_CentralReg.SetActive(active);
     if(RefTau_ety_CentralReg.IsAvailable()) RefTau_ety_CentralReg.SetActive(active);
     if(RefTau_sumet_CentralReg.IsAvailable()) RefTau_sumet_CentralReg.SetActive(active);
     if(RefTau_phi_CentralReg.IsAvailable()) RefTau_phi_CentralReg.SetActive(active);
     if(RefTau_etx_EndcapRegion.IsAvailable()) RefTau_etx_EndcapRegion.SetActive(active);
     if(RefTau_ety_EndcapRegion.IsAvailable()) RefTau_ety_EndcapRegion.SetActive(active);
     if(RefTau_sumet_EndcapRegion.IsAvailable()) RefTau_sumet_EndcapRegion.SetActive(active);
     if(RefTau_phi_EndcapRegion.IsAvailable()) RefTau_phi_EndcapRegion.SetActive(active);
     if(RefTau_etx_ForwardReg.IsAvailable()) RefTau_etx_ForwardReg.SetActive(active);
     if(RefTau_ety_ForwardReg.IsAvailable()) RefTau_ety_ForwardReg.SetActive(active);
     if(RefTau_sumet_ForwardReg.IsAvailable()) RefTau_sumet_ForwardReg.SetActive(active);
     if(RefTau_phi_ForwardReg.IsAvailable()) RefTau_phi_ForwardReg.SetActive(active);
     if(RefFinal_em_etx.IsAvailable()) RefFinal_em_etx.SetActive(active);
     if(RefFinal_em_ety.IsAvailable()) RefFinal_em_ety.SetActive(active);
     if(RefFinal_em_phi.IsAvailable()) RefFinal_em_phi.SetActive(active);
     if(RefFinal_em_et.IsAvailable()) RefFinal_em_et.SetActive(active);
     if(RefFinal_em_sumet.IsAvailable()) RefFinal_em_sumet.SetActive(active);
     if(RefFinal_em_etx_CentralReg.IsAvailable()) RefFinal_em_etx_CentralReg.SetActive(active);
     if(RefFinal_em_ety_CentralReg.IsAvailable()) RefFinal_em_ety_CentralReg.SetActive(active);
     if(RefFinal_em_sumet_CentralReg.IsAvailable()) RefFinal_em_sumet_CentralReg.SetActive(active);
     if(RefFinal_em_phi_CentralReg.IsAvailable()) RefFinal_em_phi_CentralReg.SetActive(active);
     if(RefFinal_em_etx_EndcapRegion.IsAvailable()) RefFinal_em_etx_EndcapRegion.SetActive(active);
     if(RefFinal_em_ety_EndcapRegion.IsAvailable()) RefFinal_em_ety_EndcapRegion.SetActive(active);
     if(RefFinal_em_sumet_EndcapRegion.IsAvailable()) RefFinal_em_sumet_EndcapRegion.SetActive(active);
     if(RefFinal_em_phi_EndcapRegion.IsAvailable()) RefFinal_em_phi_EndcapRegion.SetActive(active);
     if(RefFinal_em_etx_ForwardReg.IsAvailable()) RefFinal_em_etx_ForwardReg.SetActive(active);
     if(RefFinal_em_ety_ForwardReg.IsAvailable()) RefFinal_em_ety_ForwardReg.SetActive(active);
     if(RefFinal_em_sumet_ForwardReg.IsAvailable()) RefFinal_em_sumet_ForwardReg.SetActive(active);
     if(RefFinal_em_phi_ForwardReg.IsAvailable()) RefFinal_em_phi_ForwardReg.SetActive(active);
     if(RefEle_em_etx.IsAvailable()) RefEle_em_etx.SetActive(active);
     if(RefEle_em_ety.IsAvailable()) RefEle_em_ety.SetActive(active);
     if(RefEle_em_phi.IsAvailable()) RefEle_em_phi.SetActive(active);
     if(RefEle_em_et.IsAvailable()) RefEle_em_et.SetActive(active);
     if(RefEle_em_sumet.IsAvailable()) RefEle_em_sumet.SetActive(active);
     if(RefEle_em_etx_CentralReg.IsAvailable()) RefEle_em_etx_CentralReg.SetActive(active);
     if(RefEle_em_ety_CentralReg.IsAvailable()) RefEle_em_ety_CentralReg.SetActive(active);
     if(RefEle_em_sumet_CentralReg.IsAvailable()) RefEle_em_sumet_CentralReg.SetActive(active);
     if(RefEle_em_phi_CentralReg.IsAvailable()) RefEle_em_phi_CentralReg.SetActive(active);
     if(RefEle_em_etx_EndcapRegion.IsAvailable()) RefEle_em_etx_EndcapRegion.SetActive(active);
     if(RefEle_em_ety_EndcapRegion.IsAvailable()) RefEle_em_ety_EndcapRegion.SetActive(active);
     if(RefEle_em_sumet_EndcapRegion.IsAvailable()) RefEle_em_sumet_EndcapRegion.SetActive(active);
     if(RefEle_em_phi_EndcapRegion.IsAvailable()) RefEle_em_phi_EndcapRegion.SetActive(active);
     if(RefEle_em_etx_ForwardReg.IsAvailable()) RefEle_em_etx_ForwardReg.SetActive(active);
     if(RefEle_em_ety_ForwardReg.IsAvailable()) RefEle_em_ety_ForwardReg.SetActive(active);
     if(RefEle_em_sumet_ForwardReg.IsAvailable()) RefEle_em_sumet_ForwardReg.SetActive(active);
     if(RefEle_em_phi_ForwardReg.IsAvailable()) RefEle_em_phi_ForwardReg.SetActive(active);
     if(RefJet_em_etx.IsAvailable()) RefJet_em_etx.SetActive(active);
     if(RefJet_em_ety.IsAvailable()) RefJet_em_ety.SetActive(active);
     if(RefJet_em_phi.IsAvailable()) RefJet_em_phi.SetActive(active);
     if(RefJet_em_et.IsAvailable()) RefJet_em_et.SetActive(active);
     if(RefJet_em_sumet.IsAvailable()) RefJet_em_sumet.SetActive(active);
     if(RefJet_em_etx_CentralReg.IsAvailable()) RefJet_em_etx_CentralReg.SetActive(active);
     if(RefJet_em_ety_CentralReg.IsAvailable()) RefJet_em_ety_CentralReg.SetActive(active);
     if(RefJet_em_sumet_CentralReg.IsAvailable()) RefJet_em_sumet_CentralReg.SetActive(active);
     if(RefJet_em_phi_CentralReg.IsAvailable()) RefJet_em_phi_CentralReg.SetActive(active);
     if(RefJet_em_etx_EndcapRegion.IsAvailable()) RefJet_em_etx_EndcapRegion.SetActive(active);
     if(RefJet_em_ety_EndcapRegion.IsAvailable()) RefJet_em_ety_EndcapRegion.SetActive(active);
     if(RefJet_em_sumet_EndcapRegion.IsAvailable()) RefJet_em_sumet_EndcapRegion.SetActive(active);
     if(RefJet_em_phi_EndcapRegion.IsAvailable()) RefJet_em_phi_EndcapRegion.SetActive(active);
     if(RefJet_em_etx_ForwardReg.IsAvailable()) RefJet_em_etx_ForwardReg.SetActive(active);
     if(RefJet_em_ety_ForwardReg.IsAvailable()) RefJet_em_ety_ForwardReg.SetActive(active);
     if(RefJet_em_sumet_ForwardReg.IsAvailable()) RefJet_em_sumet_ForwardReg.SetActive(active);
     if(RefJet_em_phi_ForwardReg.IsAvailable()) RefJet_em_phi_ForwardReg.SetActive(active);
     if(RefMuon_em_etx.IsAvailable()) RefMuon_em_etx.SetActive(active);
     if(RefMuon_em_ety.IsAvailable()) RefMuon_em_ety.SetActive(active);
     if(RefMuon_em_phi.IsAvailable()) RefMuon_em_phi.SetActive(active);
     if(RefMuon_em_et.IsAvailable()) RefMuon_em_et.SetActive(active);
     if(RefMuon_em_sumet.IsAvailable()) RefMuon_em_sumet.SetActive(active);
     if(RefMuon_em_etx_CentralReg.IsAvailable()) RefMuon_em_etx_CentralReg.SetActive(active);
     if(RefMuon_em_ety_CentralReg.IsAvailable()) RefMuon_em_ety_CentralReg.SetActive(active);
     if(RefMuon_em_sumet_CentralReg.IsAvailable()) RefMuon_em_sumet_CentralReg.SetActive(active);
     if(RefMuon_em_phi_CentralReg.IsAvailable()) RefMuon_em_phi_CentralReg.SetActive(active);
     if(RefMuon_em_etx_EndcapRegion.IsAvailable()) RefMuon_em_etx_EndcapRegion.SetActive(active);
     if(RefMuon_em_ety_EndcapRegion.IsAvailable()) RefMuon_em_ety_EndcapRegion.SetActive(active);
     if(RefMuon_em_sumet_EndcapRegion.IsAvailable()) RefMuon_em_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_em_phi_EndcapRegion.IsAvailable()) RefMuon_em_phi_EndcapRegion.SetActive(active);
     if(RefMuon_em_etx_ForwardReg.IsAvailable()) RefMuon_em_etx_ForwardReg.SetActive(active);
     if(RefMuon_em_ety_ForwardReg.IsAvailable()) RefMuon_em_ety_ForwardReg.SetActive(active);
     if(RefMuon_em_sumet_ForwardReg.IsAvailable()) RefMuon_em_sumet_ForwardReg.SetActive(active);
     if(RefMuon_em_phi_ForwardReg.IsAvailable()) RefMuon_em_phi_ForwardReg.SetActive(active);
     if(RefMuon_Track_em_etx.IsAvailable()) RefMuon_Track_em_etx.SetActive(active);
     if(RefMuon_Track_em_ety.IsAvailable()) RefMuon_Track_em_ety.SetActive(active);
     if(RefMuon_Track_em_phi.IsAvailable()) RefMuon_Track_em_phi.SetActive(active);
     if(RefMuon_Track_em_et.IsAvailable()) RefMuon_Track_em_et.SetActive(active);
     if(RefMuon_Track_em_sumet.IsAvailable()) RefMuon_Track_em_sumet.SetActive(active);
     if(RefMuon_Track_em_etx_CentralReg.IsAvailable()) RefMuon_Track_em_etx_CentralReg.SetActive(active);
     if(RefMuon_Track_em_ety_CentralReg.IsAvailable()) RefMuon_Track_em_ety_CentralReg.SetActive(active);
     if(RefMuon_Track_em_sumet_CentralReg.IsAvailable()) RefMuon_Track_em_sumet_CentralReg.SetActive(active);
     if(RefMuon_Track_em_phi_CentralReg.IsAvailable()) RefMuon_Track_em_phi_CentralReg.SetActive(active);
     if(RefMuon_Track_em_etx_EndcapRegion.IsAvailable()) RefMuon_Track_em_etx_EndcapRegion.SetActive(active);
     if(RefMuon_Track_em_ety_EndcapRegion.IsAvailable()) RefMuon_Track_em_ety_EndcapRegion.SetActive(active);
     if(RefMuon_Track_em_sumet_EndcapRegion.IsAvailable()) RefMuon_Track_em_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_Track_em_phi_EndcapRegion.IsAvailable()) RefMuon_Track_em_phi_EndcapRegion.SetActive(active);
     if(RefMuon_Track_em_etx_ForwardReg.IsAvailable()) RefMuon_Track_em_etx_ForwardReg.SetActive(active);
     if(RefMuon_Track_em_ety_ForwardReg.IsAvailable()) RefMuon_Track_em_ety_ForwardReg.SetActive(active);
     if(RefMuon_Track_em_sumet_ForwardReg.IsAvailable()) RefMuon_Track_em_sumet_ForwardReg.SetActive(active);
     if(RefMuon_Track_em_phi_ForwardReg.IsAvailable()) RefMuon_Track_em_phi_ForwardReg.SetActive(active);
     if(RefGamma_em_etx.IsAvailable()) RefGamma_em_etx.SetActive(active);
     if(RefGamma_em_ety.IsAvailable()) RefGamma_em_ety.SetActive(active);
     if(RefGamma_em_phi.IsAvailable()) RefGamma_em_phi.SetActive(active);
     if(RefGamma_em_et.IsAvailable()) RefGamma_em_et.SetActive(active);
     if(RefGamma_em_sumet.IsAvailable()) RefGamma_em_sumet.SetActive(active);
     if(RefGamma_em_etx_CentralReg.IsAvailable()) RefGamma_em_etx_CentralReg.SetActive(active);
     if(RefGamma_em_ety_CentralReg.IsAvailable()) RefGamma_em_ety_CentralReg.SetActive(active);
     if(RefGamma_em_sumet_CentralReg.IsAvailable()) RefGamma_em_sumet_CentralReg.SetActive(active);
     if(RefGamma_em_phi_CentralReg.IsAvailable()) RefGamma_em_phi_CentralReg.SetActive(active);
     if(RefGamma_em_etx_EndcapRegion.IsAvailable()) RefGamma_em_etx_EndcapRegion.SetActive(active);
     if(RefGamma_em_ety_EndcapRegion.IsAvailable()) RefGamma_em_ety_EndcapRegion.SetActive(active);
     if(RefGamma_em_sumet_EndcapRegion.IsAvailable()) RefGamma_em_sumet_EndcapRegion.SetActive(active);
     if(RefGamma_em_phi_EndcapRegion.IsAvailable()) RefGamma_em_phi_EndcapRegion.SetActive(active);
     if(RefGamma_em_etx_ForwardReg.IsAvailable()) RefGamma_em_etx_ForwardReg.SetActive(active);
     if(RefGamma_em_ety_ForwardReg.IsAvailable()) RefGamma_em_ety_ForwardReg.SetActive(active);
     if(RefGamma_em_sumet_ForwardReg.IsAvailable()) RefGamma_em_sumet_ForwardReg.SetActive(active);
     if(RefGamma_em_phi_ForwardReg.IsAvailable()) RefGamma_em_phi_ForwardReg.SetActive(active);
     if(RefTau_em_etx.IsAvailable()) RefTau_em_etx.SetActive(active);
     if(RefTau_em_ety.IsAvailable()) RefTau_em_ety.SetActive(active);
     if(RefTau_em_phi.IsAvailable()) RefTau_em_phi.SetActive(active);
     if(RefTau_em_et.IsAvailable()) RefTau_em_et.SetActive(active);
     if(RefTau_em_sumet.IsAvailable()) RefTau_em_sumet.SetActive(active);
     if(RefTau_em_etx_CentralReg.IsAvailable()) RefTau_em_etx_CentralReg.SetActive(active);
     if(RefTau_em_ety_CentralReg.IsAvailable()) RefTau_em_ety_CentralReg.SetActive(active);
     if(RefTau_em_sumet_CentralReg.IsAvailable()) RefTau_em_sumet_CentralReg.SetActive(active);
     if(RefTau_em_phi_CentralReg.IsAvailable()) RefTau_em_phi_CentralReg.SetActive(active);
     if(RefTau_em_etx_EndcapRegion.IsAvailable()) RefTau_em_etx_EndcapRegion.SetActive(active);
     if(RefTau_em_ety_EndcapRegion.IsAvailable()) RefTau_em_ety_EndcapRegion.SetActive(active);
     if(RefTau_em_sumet_EndcapRegion.IsAvailable()) RefTau_em_sumet_EndcapRegion.SetActive(active);
     if(RefTau_em_phi_EndcapRegion.IsAvailable()) RefTau_em_phi_EndcapRegion.SetActive(active);
     if(RefTau_em_etx_ForwardReg.IsAvailable()) RefTau_em_etx_ForwardReg.SetActive(active);
     if(RefTau_em_ety_ForwardReg.IsAvailable()) RefTau_em_ety_ForwardReg.SetActive(active);
     if(RefTau_em_sumet_ForwardReg.IsAvailable()) RefTau_em_sumet_ForwardReg.SetActive(active);
     if(RefTau_em_phi_ForwardReg.IsAvailable()) RefTau_em_phi_ForwardReg.SetActive(active);
     if(RefJet_JVF_etx.IsAvailable()) RefJet_JVF_etx.SetActive(active);
     if(RefJet_JVF_ety.IsAvailable()) RefJet_JVF_ety.SetActive(active);
     if(RefJet_JVF_phi.IsAvailable()) RefJet_JVF_phi.SetActive(active);
     if(RefJet_JVF_et.IsAvailable()) RefJet_JVF_et.SetActive(active);
     if(RefJet_JVF_sumet.IsAvailable()) RefJet_JVF_sumet.SetActive(active);
     if(RefJet_JVF_etx_CentralReg.IsAvailable()) RefJet_JVF_etx_CentralReg.SetActive(active);
     if(RefJet_JVF_ety_CentralReg.IsAvailable()) RefJet_JVF_ety_CentralReg.SetActive(active);
     if(RefJet_JVF_sumet_CentralReg.IsAvailable()) RefJet_JVF_sumet_CentralReg.SetActive(active);
     if(RefJet_JVF_phi_CentralReg.IsAvailable()) RefJet_JVF_phi_CentralReg.SetActive(active);
     if(RefJet_JVF_etx_EndcapRegion.IsAvailable()) RefJet_JVF_etx_EndcapRegion.SetActive(active);
     if(RefJet_JVF_ety_EndcapRegion.IsAvailable()) RefJet_JVF_ety_EndcapRegion.SetActive(active);
     if(RefJet_JVF_sumet_EndcapRegion.IsAvailable()) RefJet_JVF_sumet_EndcapRegion.SetActive(active);
     if(RefJet_JVF_phi_EndcapRegion.IsAvailable()) RefJet_JVF_phi_EndcapRegion.SetActive(active);
     if(RefJet_JVF_etx_ForwardReg.IsAvailable()) RefJet_JVF_etx_ForwardReg.SetActive(active);
     if(RefJet_JVF_ety_ForwardReg.IsAvailable()) RefJet_JVF_ety_ForwardReg.SetActive(active);
     if(RefJet_JVF_sumet_ForwardReg.IsAvailable()) RefJet_JVF_sumet_ForwardReg.SetActive(active);
     if(RefJet_JVF_phi_ForwardReg.IsAvailable()) RefJet_JVF_phi_ForwardReg.SetActive(active);
     if(RefJet_JVFCut_etx.IsAvailable()) RefJet_JVFCut_etx.SetActive(active);
     if(RefJet_JVFCut_ety.IsAvailable()) RefJet_JVFCut_ety.SetActive(active);
     if(RefJet_JVFCut_phi.IsAvailable()) RefJet_JVFCut_phi.SetActive(active);
     if(RefJet_JVFCut_et.IsAvailable()) RefJet_JVFCut_et.SetActive(active);
     if(RefJet_JVFCut_sumet.IsAvailable()) RefJet_JVFCut_sumet.SetActive(active);
     if(RefJet_JVFCut_etx_CentralReg.IsAvailable()) RefJet_JVFCut_etx_CentralReg.SetActive(active);
     if(RefJet_JVFCut_ety_CentralReg.IsAvailable()) RefJet_JVFCut_ety_CentralReg.SetActive(active);
     if(RefJet_JVFCut_sumet_CentralReg.IsAvailable()) RefJet_JVFCut_sumet_CentralReg.SetActive(active);
     if(RefJet_JVFCut_phi_CentralReg.IsAvailable()) RefJet_JVFCut_phi_CentralReg.SetActive(active);
     if(RefJet_JVFCut_etx_EndcapRegion.IsAvailable()) RefJet_JVFCut_etx_EndcapRegion.SetActive(active);
     if(RefJet_JVFCut_ety_EndcapRegion.IsAvailable()) RefJet_JVFCut_ety_EndcapRegion.SetActive(active);
     if(RefJet_JVFCut_sumet_EndcapRegion.IsAvailable()) RefJet_JVFCut_sumet_EndcapRegion.SetActive(active);
     if(RefJet_JVFCut_phi_EndcapRegion.IsAvailable()) RefJet_JVFCut_phi_EndcapRegion.SetActive(active);
     if(RefJet_JVFCut_etx_ForwardReg.IsAvailable()) RefJet_JVFCut_etx_ForwardReg.SetActive(active);
     if(RefJet_JVFCut_ety_ForwardReg.IsAvailable()) RefJet_JVFCut_ety_ForwardReg.SetActive(active);
     if(RefJet_JVFCut_sumet_ForwardReg.IsAvailable()) RefJet_JVFCut_sumet_ForwardReg.SetActive(active);
     if(RefJet_JVFCut_phi_ForwardReg.IsAvailable()) RefJet_JVFCut_phi_ForwardReg.SetActive(active);
     if(CellOut_Eflow_STVF_etx.IsAvailable()) CellOut_Eflow_STVF_etx.SetActive(active);
     if(CellOut_Eflow_STVF_ety.IsAvailable()) CellOut_Eflow_STVF_ety.SetActive(active);
     if(CellOut_Eflow_STVF_phi.IsAvailable()) CellOut_Eflow_STVF_phi.SetActive(active);
     if(CellOut_Eflow_STVF_et.IsAvailable()) CellOut_Eflow_STVF_et.SetActive(active);
     if(CellOut_Eflow_STVF_sumet.IsAvailable()) CellOut_Eflow_STVF_sumet.SetActive(active);
     if(CellOut_Eflow_STVF_etx_CentralReg.IsAvailable()) CellOut_Eflow_STVF_etx_CentralReg.SetActive(active);
     if(CellOut_Eflow_STVF_ety_CentralReg.IsAvailable()) CellOut_Eflow_STVF_ety_CentralReg.SetActive(active);
     if(CellOut_Eflow_STVF_sumet_CentralReg.IsAvailable()) CellOut_Eflow_STVF_sumet_CentralReg.SetActive(active);
     if(CellOut_Eflow_STVF_phi_CentralReg.IsAvailable()) CellOut_Eflow_STVF_phi_CentralReg.SetActive(active);
     if(CellOut_Eflow_STVF_etx_EndcapRegion.IsAvailable()) CellOut_Eflow_STVF_etx_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_STVF_ety_EndcapRegion.IsAvailable()) CellOut_Eflow_STVF_ety_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_STVF_sumet_EndcapRegion.IsAvailable()) CellOut_Eflow_STVF_sumet_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_STVF_phi_EndcapRegion.IsAvailable()) CellOut_Eflow_STVF_phi_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_STVF_etx_ForwardReg.IsAvailable()) CellOut_Eflow_STVF_etx_ForwardReg.SetActive(active);
     if(CellOut_Eflow_STVF_ety_ForwardReg.IsAvailable()) CellOut_Eflow_STVF_ety_ForwardReg.SetActive(active);
     if(CellOut_Eflow_STVF_sumet_ForwardReg.IsAvailable()) CellOut_Eflow_STVF_sumet_ForwardReg.SetActive(active);
     if(CellOut_Eflow_STVF_phi_ForwardReg.IsAvailable()) CellOut_Eflow_STVF_phi_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetArea_etx.IsAvailable()) CellOut_Eflow_JetArea_etx.SetActive(active);
     if(CellOut_Eflow_JetArea_ety.IsAvailable()) CellOut_Eflow_JetArea_ety.SetActive(active);
     if(CellOut_Eflow_JetArea_phi.IsAvailable()) CellOut_Eflow_JetArea_phi.SetActive(active);
     if(CellOut_Eflow_JetArea_et.IsAvailable()) CellOut_Eflow_JetArea_et.SetActive(active);
     if(CellOut_Eflow_JetArea_sumet.IsAvailable()) CellOut_Eflow_JetArea_sumet.SetActive(active);
     if(CellOut_Eflow_JetArea_etx_CentralReg.IsAvailable()) CellOut_Eflow_JetArea_etx_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetArea_ety_CentralReg.IsAvailable()) CellOut_Eflow_JetArea_ety_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetArea_sumet_CentralReg.IsAvailable()) CellOut_Eflow_JetArea_sumet_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetArea_phi_CentralReg.IsAvailable()) CellOut_Eflow_JetArea_phi_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetArea_etx_EndcapRegion.IsAvailable()) CellOut_Eflow_JetArea_etx_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetArea_ety_EndcapRegion.IsAvailable()) CellOut_Eflow_JetArea_ety_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetArea_sumet_EndcapRegion.IsAvailable()) CellOut_Eflow_JetArea_sumet_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetArea_phi_EndcapRegion.IsAvailable()) CellOut_Eflow_JetArea_phi_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetArea_etx_ForwardReg.IsAvailable()) CellOut_Eflow_JetArea_etx_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetArea_ety_ForwardReg.IsAvailable()) CellOut_Eflow_JetArea_ety_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetArea_sumet_ForwardReg.IsAvailable()) CellOut_Eflow_JetArea_sumet_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetArea_phi_ForwardReg.IsAvailable()) CellOut_Eflow_JetArea_phi_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_etx.IsAvailable()) CellOut_Eflow_JetAreaJVF_etx.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_ety.IsAvailable()) CellOut_Eflow_JetAreaJVF_ety.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_phi.IsAvailable()) CellOut_Eflow_JetAreaJVF_phi.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_et.IsAvailable()) CellOut_Eflow_JetAreaJVF_et.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_sumet.IsAvailable()) CellOut_Eflow_JetAreaJVF_sumet.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_etx_CentralReg.IsAvailable()) CellOut_Eflow_JetAreaJVF_etx_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_ety_CentralReg.IsAvailable()) CellOut_Eflow_JetAreaJVF_ety_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_sumet_CentralReg.IsAvailable()) CellOut_Eflow_JetAreaJVF_sumet_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_phi_CentralReg.IsAvailable()) CellOut_Eflow_JetAreaJVF_phi_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_etx_EndcapRegion.IsAvailable()) CellOut_Eflow_JetAreaJVF_etx_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_ety_EndcapRegion.IsAvailable()) CellOut_Eflow_JetAreaJVF_ety_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion.IsAvailable()) CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_phi_EndcapRegion.IsAvailable()) CellOut_Eflow_JetAreaJVF_phi_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_etx_ForwardReg.IsAvailable()) CellOut_Eflow_JetAreaJVF_etx_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_ety_ForwardReg.IsAvailable()) CellOut_Eflow_JetAreaJVF_ety_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_sumet_ForwardReg.IsAvailable()) CellOut_Eflow_JetAreaJVF_sumet_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetAreaJVF_phi_ForwardReg.IsAvailable()) CellOut_Eflow_JetAreaJVF_phi_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_etx.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_etx.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_ety.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_ety.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_phi.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_phi.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_et.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_et.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_sumet.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_sumet.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg.SetActive(active);
     if(CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg.IsAvailable()) CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg.SetActive(active);
     if(RefFinal_STVF_etx.IsAvailable()) RefFinal_STVF_etx.SetActive(active);
     if(RefFinal_STVF_ety.IsAvailable()) RefFinal_STVF_ety.SetActive(active);
     if(RefFinal_STVF_phi.IsAvailable()) RefFinal_STVF_phi.SetActive(active);
     if(RefFinal_STVF_et.IsAvailable()) RefFinal_STVF_et.SetActive(active);
     if(RefFinal_STVF_sumet.IsAvailable()) RefFinal_STVF_sumet.SetActive(active);
     if(RefFinal_STVF_etx_CentralReg.IsAvailable()) RefFinal_STVF_etx_CentralReg.SetActive(active);
     if(RefFinal_STVF_ety_CentralReg.IsAvailable()) RefFinal_STVF_ety_CentralReg.SetActive(active);
     if(RefFinal_STVF_sumet_CentralReg.IsAvailable()) RefFinal_STVF_sumet_CentralReg.SetActive(active);
     if(RefFinal_STVF_phi_CentralReg.IsAvailable()) RefFinal_STVF_phi_CentralReg.SetActive(active);
     if(RefFinal_STVF_etx_EndcapRegion.IsAvailable()) RefFinal_STVF_etx_EndcapRegion.SetActive(active);
     if(RefFinal_STVF_ety_EndcapRegion.IsAvailable()) RefFinal_STVF_ety_EndcapRegion.SetActive(active);
     if(RefFinal_STVF_sumet_EndcapRegion.IsAvailable()) RefFinal_STVF_sumet_EndcapRegion.SetActive(active);
     if(RefFinal_STVF_phi_EndcapRegion.IsAvailable()) RefFinal_STVF_phi_EndcapRegion.SetActive(active);
     if(RefFinal_STVF_etx_ForwardReg.IsAvailable()) RefFinal_STVF_etx_ForwardReg.SetActive(active);
     if(RefFinal_STVF_ety_ForwardReg.IsAvailable()) RefFinal_STVF_ety_ForwardReg.SetActive(active);
     if(RefFinal_STVF_sumet_ForwardReg.IsAvailable()) RefFinal_STVF_sumet_ForwardReg.SetActive(active);
     if(RefFinal_STVF_phi_ForwardReg.IsAvailable()) RefFinal_STVF_phi_ForwardReg.SetActive(active);
     if(CellOut_Eflow_etx.IsAvailable()) CellOut_Eflow_etx.SetActive(active);
     if(CellOut_Eflow_ety.IsAvailable()) CellOut_Eflow_ety.SetActive(active);
     if(CellOut_Eflow_phi.IsAvailable()) CellOut_Eflow_phi.SetActive(active);
     if(CellOut_Eflow_et.IsAvailable()) CellOut_Eflow_et.SetActive(active);
     if(CellOut_Eflow_sumet.IsAvailable()) CellOut_Eflow_sumet.SetActive(active);
     if(CellOut_Eflow_etx_CentralReg.IsAvailable()) CellOut_Eflow_etx_CentralReg.SetActive(active);
     if(CellOut_Eflow_ety_CentralReg.IsAvailable()) CellOut_Eflow_ety_CentralReg.SetActive(active);
     if(CellOut_Eflow_sumet_CentralReg.IsAvailable()) CellOut_Eflow_sumet_CentralReg.SetActive(active);
     if(CellOut_Eflow_phi_CentralReg.IsAvailable()) CellOut_Eflow_phi_CentralReg.SetActive(active);
     if(CellOut_Eflow_etx_EndcapRegion.IsAvailable()) CellOut_Eflow_etx_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_ety_EndcapRegion.IsAvailable()) CellOut_Eflow_ety_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_sumet_EndcapRegion.IsAvailable()) CellOut_Eflow_sumet_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_phi_EndcapRegion.IsAvailable()) CellOut_Eflow_phi_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_etx_ForwardReg.IsAvailable()) CellOut_Eflow_etx_ForwardReg.SetActive(active);
     if(CellOut_Eflow_ety_ForwardReg.IsAvailable()) CellOut_Eflow_ety_ForwardReg.SetActive(active);
     if(CellOut_Eflow_sumet_ForwardReg.IsAvailable()) CellOut_Eflow_sumet_ForwardReg.SetActive(active);
     if(CellOut_Eflow_phi_ForwardReg.IsAvailable()) CellOut_Eflow_phi_ForwardReg.SetActive(active);
     if(CellOut_Eflow_Muid_etx.IsAvailable()) CellOut_Eflow_Muid_etx.SetActive(active);
     if(CellOut_Eflow_Muid_ety.IsAvailable()) CellOut_Eflow_Muid_ety.SetActive(active);
     if(CellOut_Eflow_Muid_phi.IsAvailable()) CellOut_Eflow_Muid_phi.SetActive(active);
     if(CellOut_Eflow_Muid_et.IsAvailable()) CellOut_Eflow_Muid_et.SetActive(active);
     if(CellOut_Eflow_Muid_sumet.IsAvailable()) CellOut_Eflow_Muid_sumet.SetActive(active);
     if(CellOut_Eflow_Muid_etx_CentralReg.IsAvailable()) CellOut_Eflow_Muid_etx_CentralReg.SetActive(active);
     if(CellOut_Eflow_Muid_ety_CentralReg.IsAvailable()) CellOut_Eflow_Muid_ety_CentralReg.SetActive(active);
     if(CellOut_Eflow_Muid_sumet_CentralReg.IsAvailable()) CellOut_Eflow_Muid_sumet_CentralReg.SetActive(active);
     if(CellOut_Eflow_Muid_phi_CentralReg.IsAvailable()) CellOut_Eflow_Muid_phi_CentralReg.SetActive(active);
     if(CellOut_Eflow_Muid_etx_EndcapRegion.IsAvailable()) CellOut_Eflow_Muid_etx_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_Muid_ety_EndcapRegion.IsAvailable()) CellOut_Eflow_Muid_ety_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_Muid_sumet_EndcapRegion.IsAvailable()) CellOut_Eflow_Muid_sumet_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_Muid_phi_EndcapRegion.IsAvailable()) CellOut_Eflow_Muid_phi_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_Muid_etx_ForwardReg.IsAvailable()) CellOut_Eflow_Muid_etx_ForwardReg.SetActive(active);
     if(CellOut_Eflow_Muid_ety_ForwardReg.IsAvailable()) CellOut_Eflow_Muid_ety_ForwardReg.SetActive(active);
     if(CellOut_Eflow_Muid_sumet_ForwardReg.IsAvailable()) CellOut_Eflow_Muid_sumet_ForwardReg.SetActive(active);
     if(CellOut_Eflow_Muid_phi_ForwardReg.IsAvailable()) CellOut_Eflow_Muid_phi_ForwardReg.SetActive(active);
     if(CellOut_Eflow_Muons_etx.IsAvailable()) CellOut_Eflow_Muons_etx.SetActive(active);
     if(CellOut_Eflow_Muons_ety.IsAvailable()) CellOut_Eflow_Muons_ety.SetActive(active);
     if(CellOut_Eflow_Muons_phi.IsAvailable()) CellOut_Eflow_Muons_phi.SetActive(active);
     if(CellOut_Eflow_Muons_et.IsAvailable()) CellOut_Eflow_Muons_et.SetActive(active);
     if(CellOut_Eflow_Muons_sumet.IsAvailable()) CellOut_Eflow_Muons_sumet.SetActive(active);
     if(CellOut_Eflow_Muons_etx_CentralReg.IsAvailable()) CellOut_Eflow_Muons_etx_CentralReg.SetActive(active);
     if(CellOut_Eflow_Muons_ety_CentralReg.IsAvailable()) CellOut_Eflow_Muons_ety_CentralReg.SetActive(active);
     if(CellOut_Eflow_Muons_sumet_CentralReg.IsAvailable()) CellOut_Eflow_Muons_sumet_CentralReg.SetActive(active);
     if(CellOut_Eflow_Muons_phi_CentralReg.IsAvailable()) CellOut_Eflow_Muons_phi_CentralReg.SetActive(active);
     if(CellOut_Eflow_Muons_etx_EndcapRegion.IsAvailable()) CellOut_Eflow_Muons_etx_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_Muons_ety_EndcapRegion.IsAvailable()) CellOut_Eflow_Muons_ety_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_Muons_sumet_EndcapRegion.IsAvailable()) CellOut_Eflow_Muons_sumet_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_Muons_phi_EndcapRegion.IsAvailable()) CellOut_Eflow_Muons_phi_EndcapRegion.SetActive(active);
     if(CellOut_Eflow_Muons_etx_ForwardReg.IsAvailable()) CellOut_Eflow_Muons_etx_ForwardReg.SetActive(active);
     if(CellOut_Eflow_Muons_ety_ForwardReg.IsAvailable()) CellOut_Eflow_Muons_ety_ForwardReg.SetActive(active);
     if(CellOut_Eflow_Muons_sumet_ForwardReg.IsAvailable()) CellOut_Eflow_Muons_sumet_ForwardReg.SetActive(active);
     if(CellOut_Eflow_Muons_phi_ForwardReg.IsAvailable()) CellOut_Eflow_Muons_phi_ForwardReg.SetActive(active);
     if(RefMuon_Track_etx.IsAvailable()) RefMuon_Track_etx.SetActive(active);
     if(RefMuon_Track_ety.IsAvailable()) RefMuon_Track_ety.SetActive(active);
     if(RefMuon_Track_phi.IsAvailable()) RefMuon_Track_phi.SetActive(active);
     if(RefMuon_Track_et.IsAvailable()) RefMuon_Track_et.SetActive(active);
     if(RefMuon_Track_sumet.IsAvailable()) RefMuon_Track_sumet.SetActive(active);
     if(RefMuon_Track_etx_CentralReg.IsAvailable()) RefMuon_Track_etx_CentralReg.SetActive(active);
     if(RefMuon_Track_ety_CentralReg.IsAvailable()) RefMuon_Track_ety_CentralReg.SetActive(active);
     if(RefMuon_Track_sumet_CentralReg.IsAvailable()) RefMuon_Track_sumet_CentralReg.SetActive(active);
     if(RefMuon_Track_phi_CentralReg.IsAvailable()) RefMuon_Track_phi_CentralReg.SetActive(active);
     if(RefMuon_Track_etx_EndcapRegion.IsAvailable()) RefMuon_Track_etx_EndcapRegion.SetActive(active);
     if(RefMuon_Track_ety_EndcapRegion.IsAvailable()) RefMuon_Track_ety_EndcapRegion.SetActive(active);
     if(RefMuon_Track_sumet_EndcapRegion.IsAvailable()) RefMuon_Track_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_Track_phi_EndcapRegion.IsAvailable()) RefMuon_Track_phi_EndcapRegion.SetActive(active);
     if(RefMuon_Track_etx_ForwardReg.IsAvailable()) RefMuon_Track_etx_ForwardReg.SetActive(active);
     if(RefMuon_Track_ety_ForwardReg.IsAvailable()) RefMuon_Track_ety_ForwardReg.SetActive(active);
     if(RefMuon_Track_sumet_ForwardReg.IsAvailable()) RefMuon_Track_sumet_ForwardReg.SetActive(active);
     if(RefMuon_Track_phi_ForwardReg.IsAvailable()) RefMuon_Track_phi_ForwardReg.SetActive(active);
     if(RefMuon_Track_Staco_etx.IsAvailable()) RefMuon_Track_Staco_etx.SetActive(active);
     if(RefMuon_Track_Staco_ety.IsAvailable()) RefMuon_Track_Staco_ety.SetActive(active);
     if(RefMuon_Track_Staco_phi.IsAvailable()) RefMuon_Track_Staco_phi.SetActive(active);
     if(RefMuon_Track_Staco_et.IsAvailable()) RefMuon_Track_Staco_et.SetActive(active);
     if(RefMuon_Track_Staco_sumet.IsAvailable()) RefMuon_Track_Staco_sumet.SetActive(active);
     if(RefMuon_Track_Staco_etx_CentralReg.IsAvailable()) RefMuon_Track_Staco_etx_CentralReg.SetActive(active);
     if(RefMuon_Track_Staco_ety_CentralReg.IsAvailable()) RefMuon_Track_Staco_ety_CentralReg.SetActive(active);
     if(RefMuon_Track_Staco_sumet_CentralReg.IsAvailable()) RefMuon_Track_Staco_sumet_CentralReg.SetActive(active);
     if(RefMuon_Track_Staco_phi_CentralReg.IsAvailable()) RefMuon_Track_Staco_phi_CentralReg.SetActive(active);
     if(RefMuon_Track_Staco_etx_EndcapRegion.IsAvailable()) RefMuon_Track_Staco_etx_EndcapRegion.SetActive(active);
     if(RefMuon_Track_Staco_ety_EndcapRegion.IsAvailable()) RefMuon_Track_Staco_ety_EndcapRegion.SetActive(active);
     if(RefMuon_Track_Staco_sumet_EndcapRegion.IsAvailable()) RefMuon_Track_Staco_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_Track_Staco_phi_EndcapRegion.IsAvailable()) RefMuon_Track_Staco_phi_EndcapRegion.SetActive(active);
     if(RefMuon_Track_Staco_etx_ForwardReg.IsAvailable()) RefMuon_Track_Staco_etx_ForwardReg.SetActive(active);
     if(RefMuon_Track_Staco_ety_ForwardReg.IsAvailable()) RefMuon_Track_Staco_ety_ForwardReg.SetActive(active);
     if(RefMuon_Track_Staco_sumet_ForwardReg.IsAvailable()) RefMuon_Track_Staco_sumet_ForwardReg.SetActive(active);
     if(RefMuon_Track_Staco_phi_ForwardReg.IsAvailable()) RefMuon_Track_Staco_phi_ForwardReg.SetActive(active);
     if(RefMuon_Track_Muid_etx.IsAvailable()) RefMuon_Track_Muid_etx.SetActive(active);
     if(RefMuon_Track_Muid_ety.IsAvailable()) RefMuon_Track_Muid_ety.SetActive(active);
     if(RefMuon_Track_Muid_phi.IsAvailable()) RefMuon_Track_Muid_phi.SetActive(active);
     if(RefMuon_Track_Muid_et.IsAvailable()) RefMuon_Track_Muid_et.SetActive(active);
     if(RefMuon_Track_Muid_sumet.IsAvailable()) RefMuon_Track_Muid_sumet.SetActive(active);
     if(RefMuon_Track_Muid_etx_CentralReg.IsAvailable()) RefMuon_Track_Muid_etx_CentralReg.SetActive(active);
     if(RefMuon_Track_Muid_ety_CentralReg.IsAvailable()) RefMuon_Track_Muid_ety_CentralReg.SetActive(active);
     if(RefMuon_Track_Muid_sumet_CentralReg.IsAvailable()) RefMuon_Track_Muid_sumet_CentralReg.SetActive(active);
     if(RefMuon_Track_Muid_phi_CentralReg.IsAvailable()) RefMuon_Track_Muid_phi_CentralReg.SetActive(active);
     if(RefMuon_Track_Muid_etx_EndcapRegion.IsAvailable()) RefMuon_Track_Muid_etx_EndcapRegion.SetActive(active);
     if(RefMuon_Track_Muid_ety_EndcapRegion.IsAvailable()) RefMuon_Track_Muid_ety_EndcapRegion.SetActive(active);
     if(RefMuon_Track_Muid_sumet_EndcapRegion.IsAvailable()) RefMuon_Track_Muid_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_Track_Muid_phi_EndcapRegion.IsAvailable()) RefMuon_Track_Muid_phi_EndcapRegion.SetActive(active);
     if(RefMuon_Track_Muid_etx_ForwardReg.IsAvailable()) RefMuon_Track_Muid_etx_ForwardReg.SetActive(active);
     if(RefMuon_Track_Muid_ety_ForwardReg.IsAvailable()) RefMuon_Track_Muid_ety_ForwardReg.SetActive(active);
     if(RefMuon_Track_Muid_sumet_ForwardReg.IsAvailable()) RefMuon_Track_Muid_sumet_ForwardReg.SetActive(active);
     if(RefMuon_Track_Muid_phi_ForwardReg.IsAvailable()) RefMuon_Track_Muid_phi_ForwardReg.SetActive(active);
     if(RefMuons_Track_etx.IsAvailable()) RefMuons_Track_etx.SetActive(active);
     if(RefMuons_Track_ety.IsAvailable()) RefMuons_Track_ety.SetActive(active);
     if(RefMuons_Track_phi.IsAvailable()) RefMuons_Track_phi.SetActive(active);
     if(RefMuons_Track_et.IsAvailable()) RefMuons_Track_et.SetActive(active);
     if(RefMuons_Track_sumet.IsAvailable()) RefMuons_Track_sumet.SetActive(active);
     if(RefMuons_Track_etx_CentralReg.IsAvailable()) RefMuons_Track_etx_CentralReg.SetActive(active);
     if(RefMuons_Track_ety_CentralReg.IsAvailable()) RefMuons_Track_ety_CentralReg.SetActive(active);
     if(RefMuons_Track_sumet_CentralReg.IsAvailable()) RefMuons_Track_sumet_CentralReg.SetActive(active);
     if(RefMuons_Track_phi_CentralReg.IsAvailable()) RefMuons_Track_phi_CentralReg.SetActive(active);
     if(RefMuons_Track_etx_EndcapRegion.IsAvailable()) RefMuons_Track_etx_EndcapRegion.SetActive(active);
     if(RefMuons_Track_ety_EndcapRegion.IsAvailable()) RefMuons_Track_ety_EndcapRegion.SetActive(active);
     if(RefMuons_Track_sumet_EndcapRegion.IsAvailable()) RefMuons_Track_sumet_EndcapRegion.SetActive(active);
     if(RefMuons_Track_phi_EndcapRegion.IsAvailable()) RefMuons_Track_phi_EndcapRegion.SetActive(active);
     if(RefMuons_Track_etx_ForwardReg.IsAvailable()) RefMuons_Track_etx_ForwardReg.SetActive(active);
     if(RefMuons_Track_ety_ForwardReg.IsAvailable()) RefMuons_Track_ety_ForwardReg.SetActive(active);
     if(RefMuons_Track_sumet_ForwardReg.IsAvailable()) RefMuons_Track_sumet_ForwardReg.SetActive(active);
     if(RefMuons_Track_phi_ForwardReg.IsAvailable()) RefMuons_Track_phi_ForwardReg.SetActive(active);
     if(RefFinal_BDTMedium_etx.IsAvailable()) RefFinal_BDTMedium_etx.SetActive(active);
     if(RefFinal_BDTMedium_ety.IsAvailable()) RefFinal_BDTMedium_ety.SetActive(active);
     if(RefFinal_BDTMedium_phi.IsAvailable()) RefFinal_BDTMedium_phi.SetActive(active);
     if(RefFinal_BDTMedium_et.IsAvailable()) RefFinal_BDTMedium_et.SetActive(active);
     if(RefFinal_BDTMedium_sumet.IsAvailable()) RefFinal_BDTMedium_sumet.SetActive(active);
     if(RefFinal_BDTMedium_etx_CentralReg.IsAvailable()) RefFinal_BDTMedium_etx_CentralReg.SetActive(active);
     if(RefFinal_BDTMedium_ety_CentralReg.IsAvailable()) RefFinal_BDTMedium_ety_CentralReg.SetActive(active);
     if(RefFinal_BDTMedium_sumet_CentralReg.IsAvailable()) RefFinal_BDTMedium_sumet_CentralReg.SetActive(active);
     if(RefFinal_BDTMedium_phi_CentralReg.IsAvailable()) RefFinal_BDTMedium_phi_CentralReg.SetActive(active);
     if(RefFinal_BDTMedium_etx_EndcapRegion.IsAvailable()) RefFinal_BDTMedium_etx_EndcapRegion.SetActive(active);
     if(RefFinal_BDTMedium_ety_EndcapRegion.IsAvailable()) RefFinal_BDTMedium_ety_EndcapRegion.SetActive(active);
     if(RefFinal_BDTMedium_sumet_EndcapRegion.IsAvailable()) RefFinal_BDTMedium_sumet_EndcapRegion.SetActive(active);
     if(RefFinal_BDTMedium_phi_EndcapRegion.IsAvailable()) RefFinal_BDTMedium_phi_EndcapRegion.SetActive(active);
     if(RefFinal_BDTMedium_etx_ForwardReg.IsAvailable()) RefFinal_BDTMedium_etx_ForwardReg.SetActive(active);
     if(RefFinal_BDTMedium_ety_ForwardReg.IsAvailable()) RefFinal_BDTMedium_ety_ForwardReg.SetActive(active);
     if(RefFinal_BDTMedium_sumet_ForwardReg.IsAvailable()) RefFinal_BDTMedium_sumet_ForwardReg.SetActive(active);
     if(RefFinal_BDTMedium_phi_ForwardReg.IsAvailable()) RefFinal_BDTMedium_phi_ForwardReg.SetActive(active);
     if(RefGamma_BDTMedium_etx.IsAvailable()) RefGamma_BDTMedium_etx.SetActive(active);
     if(RefGamma_BDTMedium_ety.IsAvailable()) RefGamma_BDTMedium_ety.SetActive(active);
     if(RefGamma_BDTMedium_phi.IsAvailable()) RefGamma_BDTMedium_phi.SetActive(active);
     if(RefGamma_BDTMedium_et.IsAvailable()) RefGamma_BDTMedium_et.SetActive(active);
     if(RefGamma_BDTMedium_sumet.IsAvailable()) RefGamma_BDTMedium_sumet.SetActive(active);
     if(RefGamma_BDTMedium_etx_CentralReg.IsAvailable()) RefGamma_BDTMedium_etx_CentralReg.SetActive(active);
     if(RefGamma_BDTMedium_ety_CentralReg.IsAvailable()) RefGamma_BDTMedium_ety_CentralReg.SetActive(active);
     if(RefGamma_BDTMedium_sumet_CentralReg.IsAvailable()) RefGamma_BDTMedium_sumet_CentralReg.SetActive(active);
     if(RefGamma_BDTMedium_phi_CentralReg.IsAvailable()) RefGamma_BDTMedium_phi_CentralReg.SetActive(active);
     if(RefGamma_BDTMedium_etx_EndcapRegion.IsAvailable()) RefGamma_BDTMedium_etx_EndcapRegion.SetActive(active);
     if(RefGamma_BDTMedium_ety_EndcapRegion.IsAvailable()) RefGamma_BDTMedium_ety_EndcapRegion.SetActive(active);
     if(RefGamma_BDTMedium_sumet_EndcapRegion.IsAvailable()) RefGamma_BDTMedium_sumet_EndcapRegion.SetActive(active);
     if(RefGamma_BDTMedium_phi_EndcapRegion.IsAvailable()) RefGamma_BDTMedium_phi_EndcapRegion.SetActive(active);
     if(RefGamma_BDTMedium_etx_ForwardReg.IsAvailable()) RefGamma_BDTMedium_etx_ForwardReg.SetActive(active);
     if(RefGamma_BDTMedium_ety_ForwardReg.IsAvailable()) RefGamma_BDTMedium_ety_ForwardReg.SetActive(active);
     if(RefGamma_BDTMedium_sumet_ForwardReg.IsAvailable()) RefGamma_BDTMedium_sumet_ForwardReg.SetActive(active);
     if(RefGamma_BDTMedium_phi_ForwardReg.IsAvailable()) RefGamma_BDTMedium_phi_ForwardReg.SetActive(active);
     if(RefEle_BDTMedium_etx.IsAvailable()) RefEle_BDTMedium_etx.SetActive(active);
     if(RefEle_BDTMedium_ety.IsAvailable()) RefEle_BDTMedium_ety.SetActive(active);
     if(RefEle_BDTMedium_phi.IsAvailable()) RefEle_BDTMedium_phi.SetActive(active);
     if(RefEle_BDTMedium_et.IsAvailable()) RefEle_BDTMedium_et.SetActive(active);
     if(RefEle_BDTMedium_sumet.IsAvailable()) RefEle_BDTMedium_sumet.SetActive(active);
     if(RefEle_BDTMedium_etx_CentralReg.IsAvailable()) RefEle_BDTMedium_etx_CentralReg.SetActive(active);
     if(RefEle_BDTMedium_ety_CentralReg.IsAvailable()) RefEle_BDTMedium_ety_CentralReg.SetActive(active);
     if(RefEle_BDTMedium_sumet_CentralReg.IsAvailable()) RefEle_BDTMedium_sumet_CentralReg.SetActive(active);
     if(RefEle_BDTMedium_phi_CentralReg.IsAvailable()) RefEle_BDTMedium_phi_CentralReg.SetActive(active);
     if(RefEle_BDTMedium_etx_EndcapRegion.IsAvailable()) RefEle_BDTMedium_etx_EndcapRegion.SetActive(active);
     if(RefEle_BDTMedium_ety_EndcapRegion.IsAvailable()) RefEle_BDTMedium_ety_EndcapRegion.SetActive(active);
     if(RefEle_BDTMedium_sumet_EndcapRegion.IsAvailable()) RefEle_BDTMedium_sumet_EndcapRegion.SetActive(active);
     if(RefEle_BDTMedium_phi_EndcapRegion.IsAvailable()) RefEle_BDTMedium_phi_EndcapRegion.SetActive(active);
     if(RefEle_BDTMedium_etx_ForwardReg.IsAvailable()) RefEle_BDTMedium_etx_ForwardReg.SetActive(active);
     if(RefEle_BDTMedium_ety_ForwardReg.IsAvailable()) RefEle_BDTMedium_ety_ForwardReg.SetActive(active);
     if(RefEle_BDTMedium_sumet_ForwardReg.IsAvailable()) RefEle_BDTMedium_sumet_ForwardReg.SetActive(active);
     if(RefEle_BDTMedium_phi_ForwardReg.IsAvailable()) RefEle_BDTMedium_phi_ForwardReg.SetActive(active);
     if(RefTau_BDTMedium_etx.IsAvailable()) RefTau_BDTMedium_etx.SetActive(active);
     if(RefTau_BDTMedium_ety.IsAvailable()) RefTau_BDTMedium_ety.SetActive(active);
     if(RefTau_BDTMedium_phi.IsAvailable()) RefTau_BDTMedium_phi.SetActive(active);
     if(RefTau_BDTMedium_et.IsAvailable()) RefTau_BDTMedium_et.SetActive(active);
     if(RefTau_BDTMedium_sumet.IsAvailable()) RefTau_BDTMedium_sumet.SetActive(active);
     if(RefTau_BDTMedium_etx_CentralReg.IsAvailable()) RefTau_BDTMedium_etx_CentralReg.SetActive(active);
     if(RefTau_BDTMedium_ety_CentralReg.IsAvailable()) RefTau_BDTMedium_ety_CentralReg.SetActive(active);
     if(RefTau_BDTMedium_sumet_CentralReg.IsAvailable()) RefTau_BDTMedium_sumet_CentralReg.SetActive(active);
     if(RefTau_BDTMedium_phi_CentralReg.IsAvailable()) RefTau_BDTMedium_phi_CentralReg.SetActive(active);
     if(RefTau_BDTMedium_etx_EndcapRegion.IsAvailable()) RefTau_BDTMedium_etx_EndcapRegion.SetActive(active);
     if(RefTau_BDTMedium_ety_EndcapRegion.IsAvailable()) RefTau_BDTMedium_ety_EndcapRegion.SetActive(active);
     if(RefTau_BDTMedium_sumet_EndcapRegion.IsAvailable()) RefTau_BDTMedium_sumet_EndcapRegion.SetActive(active);
     if(RefTau_BDTMedium_phi_EndcapRegion.IsAvailable()) RefTau_BDTMedium_phi_EndcapRegion.SetActive(active);
     if(RefTau_BDTMedium_etx_ForwardReg.IsAvailable()) RefTau_BDTMedium_etx_ForwardReg.SetActive(active);
     if(RefTau_BDTMedium_ety_ForwardReg.IsAvailable()) RefTau_BDTMedium_ety_ForwardReg.SetActive(active);
     if(RefTau_BDTMedium_sumet_ForwardReg.IsAvailable()) RefTau_BDTMedium_sumet_ForwardReg.SetActive(active);
     if(RefTau_BDTMedium_phi_ForwardReg.IsAvailable()) RefTau_BDTMedium_phi_ForwardReg.SetActive(active);
     if(RefJet_BDTMedium_etx.IsAvailable()) RefJet_BDTMedium_etx.SetActive(active);
     if(RefJet_BDTMedium_ety.IsAvailable()) RefJet_BDTMedium_ety.SetActive(active);
     if(RefJet_BDTMedium_phi.IsAvailable()) RefJet_BDTMedium_phi.SetActive(active);
     if(RefJet_BDTMedium_et.IsAvailable()) RefJet_BDTMedium_et.SetActive(active);
     if(RefJet_BDTMedium_sumet.IsAvailable()) RefJet_BDTMedium_sumet.SetActive(active);
     if(RefJet_BDTMedium_etx_CentralReg.IsAvailable()) RefJet_BDTMedium_etx_CentralReg.SetActive(active);
     if(RefJet_BDTMedium_ety_CentralReg.IsAvailable()) RefJet_BDTMedium_ety_CentralReg.SetActive(active);
     if(RefJet_BDTMedium_sumet_CentralReg.IsAvailable()) RefJet_BDTMedium_sumet_CentralReg.SetActive(active);
     if(RefJet_BDTMedium_phi_CentralReg.IsAvailable()) RefJet_BDTMedium_phi_CentralReg.SetActive(active);
     if(RefJet_BDTMedium_etx_EndcapRegion.IsAvailable()) RefJet_BDTMedium_etx_EndcapRegion.SetActive(active);
     if(RefJet_BDTMedium_ety_EndcapRegion.IsAvailable()) RefJet_BDTMedium_ety_EndcapRegion.SetActive(active);
     if(RefJet_BDTMedium_sumet_EndcapRegion.IsAvailable()) RefJet_BDTMedium_sumet_EndcapRegion.SetActive(active);
     if(RefJet_BDTMedium_phi_EndcapRegion.IsAvailable()) RefJet_BDTMedium_phi_EndcapRegion.SetActive(active);
     if(RefJet_BDTMedium_etx_ForwardReg.IsAvailable()) RefJet_BDTMedium_etx_ForwardReg.SetActive(active);
     if(RefJet_BDTMedium_ety_ForwardReg.IsAvailable()) RefJet_BDTMedium_ety_ForwardReg.SetActive(active);
     if(RefJet_BDTMedium_sumet_ForwardReg.IsAvailable()) RefJet_BDTMedium_sumet_ForwardReg.SetActive(active);
     if(RefJet_BDTMedium_phi_ForwardReg.IsAvailable()) RefJet_BDTMedium_phi_ForwardReg.SetActive(active);
     if(RefMuon_Staco_BDTMedium_etx.IsAvailable()) RefMuon_Staco_BDTMedium_etx.SetActive(active);
     if(RefMuon_Staco_BDTMedium_ety.IsAvailable()) RefMuon_Staco_BDTMedium_ety.SetActive(active);
     if(RefMuon_Staco_BDTMedium_phi.IsAvailable()) RefMuon_Staco_BDTMedium_phi.SetActive(active);
     if(RefMuon_Staco_BDTMedium_et.IsAvailable()) RefMuon_Staco_BDTMedium_et.SetActive(active);
     if(RefMuon_Staco_BDTMedium_sumet.IsAvailable()) RefMuon_Staco_BDTMedium_sumet.SetActive(active);
     if(RefMuon_Staco_BDTMedium_etx_CentralReg.IsAvailable()) RefMuon_Staco_BDTMedium_etx_CentralReg.SetActive(active);
     if(RefMuon_Staco_BDTMedium_ety_CentralReg.IsAvailable()) RefMuon_Staco_BDTMedium_ety_CentralReg.SetActive(active);
     if(RefMuon_Staco_BDTMedium_sumet_CentralReg.IsAvailable()) RefMuon_Staco_BDTMedium_sumet_CentralReg.SetActive(active);
     if(RefMuon_Staco_BDTMedium_phi_CentralReg.IsAvailable()) RefMuon_Staco_BDTMedium_phi_CentralReg.SetActive(active);
     if(RefMuon_Staco_BDTMedium_etx_EndcapRegion.IsAvailable()) RefMuon_Staco_BDTMedium_etx_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_BDTMedium_ety_EndcapRegion.IsAvailable()) RefMuon_Staco_BDTMedium_ety_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_BDTMedium_sumet_EndcapRegion.IsAvailable()) RefMuon_Staco_BDTMedium_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_BDTMedium_phi_EndcapRegion.IsAvailable()) RefMuon_Staco_BDTMedium_phi_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_BDTMedium_etx_ForwardReg.IsAvailable()) RefMuon_Staco_BDTMedium_etx_ForwardReg.SetActive(active);
     if(RefMuon_Staco_BDTMedium_ety_ForwardReg.IsAvailable()) RefMuon_Staco_BDTMedium_ety_ForwardReg.SetActive(active);
     if(RefMuon_Staco_BDTMedium_sumet_ForwardReg.IsAvailable()) RefMuon_Staco_BDTMedium_sumet_ForwardReg.SetActive(active);
     if(RefMuon_Staco_BDTMedium_phi_ForwardReg.IsAvailable()) RefMuon_Staco_BDTMedium_phi_ForwardReg.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_etx.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_etx.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_ety.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_ety.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_phi.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_phi.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_et.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_et.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_sumet.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
     if(RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsAvailable()) RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_etx.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_etx.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_ety.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_ety.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_phi.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_phi.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_et.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_et.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_sumet.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
     if(RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsAvailable()) RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_etx.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_etx.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_ety.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_ety.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_phi.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_phi.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_et.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_et.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_sumet.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
     if(RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsAvailable()) RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_etx.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_etx.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_ety.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_ety.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_phi.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_phi.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_et.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_et.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_sumet.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
     if(RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsAvailable()) RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_etx.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_etx.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_ety.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_ety.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_phi.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_phi.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_et.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_et.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_sumet.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
     if(RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsAvailable()) RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_etx.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_etx.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_ety.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_ety.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_phi.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_phi.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_et.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_et.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
     if(RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsAvailable()) RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
    }
    else
    {
      RefFinal_etx.SetActive(active);
      RefFinal_ety.SetActive(active);
      RefFinal_phi.SetActive(active);
      RefFinal_et.SetActive(active);
      RefFinal_sumet.SetActive(active);
      RefFinal_etx_CentralReg.SetActive(active);
      RefFinal_ety_CentralReg.SetActive(active);
      RefFinal_sumet_CentralReg.SetActive(active);
      RefFinal_phi_CentralReg.SetActive(active);
      RefFinal_etx_EndcapRegion.SetActive(active);
      RefFinal_ety_EndcapRegion.SetActive(active);
      RefFinal_sumet_EndcapRegion.SetActive(active);
      RefFinal_phi_EndcapRegion.SetActive(active);
      RefFinal_etx_ForwardReg.SetActive(active);
      RefFinal_ety_ForwardReg.SetActive(active);
      RefFinal_sumet_ForwardReg.SetActive(active);
      RefFinal_phi_ForwardReg.SetActive(active);
      RefEle_etx.SetActive(active);
      RefEle_ety.SetActive(active);
      RefEle_phi.SetActive(active);
      RefEle_et.SetActive(active);
      RefEle_sumet.SetActive(active);
      RefEle_etx_CentralReg.SetActive(active);
      RefEle_ety_CentralReg.SetActive(active);
      RefEle_sumet_CentralReg.SetActive(active);
      RefEle_phi_CentralReg.SetActive(active);
      RefEle_etx_EndcapRegion.SetActive(active);
      RefEle_ety_EndcapRegion.SetActive(active);
      RefEle_sumet_EndcapRegion.SetActive(active);
      RefEle_phi_EndcapRegion.SetActive(active);
      RefEle_etx_ForwardReg.SetActive(active);
      RefEle_ety_ForwardReg.SetActive(active);
      RefEle_sumet_ForwardReg.SetActive(active);
      RefEle_phi_ForwardReg.SetActive(active);
      RefJet_etx.SetActive(active);
      RefJet_ety.SetActive(active);
      RefJet_phi.SetActive(active);
      RefJet_et.SetActive(active);
      RefJet_sumet.SetActive(active);
      RefJet_etx_CentralReg.SetActive(active);
      RefJet_ety_CentralReg.SetActive(active);
      RefJet_sumet_CentralReg.SetActive(active);
      RefJet_phi_CentralReg.SetActive(active);
      RefJet_etx_EndcapRegion.SetActive(active);
      RefJet_ety_EndcapRegion.SetActive(active);
      RefJet_sumet_EndcapRegion.SetActive(active);
      RefJet_phi_EndcapRegion.SetActive(active);
      RefJet_etx_ForwardReg.SetActive(active);
      RefJet_ety_ForwardReg.SetActive(active);
      RefJet_sumet_ForwardReg.SetActive(active);
      RefJet_phi_ForwardReg.SetActive(active);
      RefMuon_etx.SetActive(active);
      RefMuon_ety.SetActive(active);
      RefMuon_phi.SetActive(active);
      RefMuon_et.SetActive(active);
      RefMuon_sumet.SetActive(active);
      RefMuon_etx_CentralReg.SetActive(active);
      RefMuon_ety_CentralReg.SetActive(active);
      RefMuon_sumet_CentralReg.SetActive(active);
      RefMuon_phi_CentralReg.SetActive(active);
      RefMuon_etx_EndcapRegion.SetActive(active);
      RefMuon_ety_EndcapRegion.SetActive(active);
      RefMuon_sumet_EndcapRegion.SetActive(active);
      RefMuon_phi_EndcapRegion.SetActive(active);
      RefMuon_etx_ForwardReg.SetActive(active);
      RefMuon_ety_ForwardReg.SetActive(active);
      RefMuon_sumet_ForwardReg.SetActive(active);
      RefMuon_phi_ForwardReg.SetActive(active);
      RefMuon_Staco_etx.SetActive(active);
      RefMuon_Staco_ety.SetActive(active);
      RefMuon_Staco_phi.SetActive(active);
      RefMuon_Staco_et.SetActive(active);
      RefMuon_Staco_sumet.SetActive(active);
      RefMuon_Staco_etx_CentralReg.SetActive(active);
      RefMuon_Staco_ety_CentralReg.SetActive(active);
      RefMuon_Staco_sumet_CentralReg.SetActive(active);
      RefMuon_Staco_phi_CentralReg.SetActive(active);
      RefMuon_Staco_etx_EndcapRegion.SetActive(active);
      RefMuon_Staco_ety_EndcapRegion.SetActive(active);
      RefMuon_Staco_sumet_EndcapRegion.SetActive(active);
      RefMuon_Staco_phi_EndcapRegion.SetActive(active);
      RefMuon_Staco_etx_ForwardReg.SetActive(active);
      RefMuon_Staco_ety_ForwardReg.SetActive(active);
      RefMuon_Staco_sumet_ForwardReg.SetActive(active);
      RefMuon_Staco_phi_ForwardReg.SetActive(active);
      RefMuon_Muid_etx.SetActive(active);
      RefMuon_Muid_ety.SetActive(active);
      RefMuon_Muid_phi.SetActive(active);
      RefMuon_Muid_et.SetActive(active);
      RefMuon_Muid_sumet.SetActive(active);
      RefMuon_Muid_etx_CentralReg.SetActive(active);
      RefMuon_Muid_ety_CentralReg.SetActive(active);
      RefMuon_Muid_sumet_CentralReg.SetActive(active);
      RefMuon_Muid_phi_CentralReg.SetActive(active);
      RefMuon_Muid_etx_EndcapRegion.SetActive(active);
      RefMuon_Muid_ety_EndcapRegion.SetActive(active);
      RefMuon_Muid_sumet_EndcapRegion.SetActive(active);
      RefMuon_Muid_phi_EndcapRegion.SetActive(active);
      RefMuon_Muid_etx_ForwardReg.SetActive(active);
      RefMuon_Muid_ety_ForwardReg.SetActive(active);
      RefMuon_Muid_sumet_ForwardReg.SetActive(active);
      RefMuon_Muid_phi_ForwardReg.SetActive(active);
      RefMuons_etx.SetActive(active);
      RefMuons_ety.SetActive(active);
      RefMuons_phi.SetActive(active);
      RefMuons_et.SetActive(active);
      RefMuons_sumet.SetActive(active);
      RefMuons_etx_CentralReg.SetActive(active);
      RefMuons_ety_CentralReg.SetActive(active);
      RefMuons_sumet_CentralReg.SetActive(active);
      RefMuons_phi_CentralReg.SetActive(active);
      RefMuons_etx_EndcapRegion.SetActive(active);
      RefMuons_ety_EndcapRegion.SetActive(active);
      RefMuons_sumet_EndcapRegion.SetActive(active);
      RefMuons_phi_EndcapRegion.SetActive(active);
      RefMuons_etx_ForwardReg.SetActive(active);
      RefMuons_ety_ForwardReg.SetActive(active);
      RefMuons_sumet_ForwardReg.SetActive(active);
      RefMuons_phi_ForwardReg.SetActive(active);
      RefGamma_etx.SetActive(active);
      RefGamma_ety.SetActive(active);
      RefGamma_phi.SetActive(active);
      RefGamma_et.SetActive(active);
      RefGamma_sumet.SetActive(active);
      RefGamma_etx_CentralReg.SetActive(active);
      RefGamma_ety_CentralReg.SetActive(active);
      RefGamma_sumet_CentralReg.SetActive(active);
      RefGamma_phi_CentralReg.SetActive(active);
      RefGamma_etx_EndcapRegion.SetActive(active);
      RefGamma_ety_EndcapRegion.SetActive(active);
      RefGamma_sumet_EndcapRegion.SetActive(active);
      RefGamma_phi_EndcapRegion.SetActive(active);
      RefGamma_etx_ForwardReg.SetActive(active);
      RefGamma_ety_ForwardReg.SetActive(active);
      RefGamma_sumet_ForwardReg.SetActive(active);
      RefGamma_phi_ForwardReg.SetActive(active);
      RefTau_etx.SetActive(active);
      RefTau_ety.SetActive(active);
      RefTau_phi.SetActive(active);
      RefTau_et.SetActive(active);
      RefTau_sumet.SetActive(active);
      RefTau_etx_CentralReg.SetActive(active);
      RefTau_ety_CentralReg.SetActive(active);
      RefTau_sumet_CentralReg.SetActive(active);
      RefTau_phi_CentralReg.SetActive(active);
      RefTau_etx_EndcapRegion.SetActive(active);
      RefTau_ety_EndcapRegion.SetActive(active);
      RefTau_sumet_EndcapRegion.SetActive(active);
      RefTau_phi_EndcapRegion.SetActive(active);
      RefTau_etx_ForwardReg.SetActive(active);
      RefTau_ety_ForwardReg.SetActive(active);
      RefTau_sumet_ForwardReg.SetActive(active);
      RefTau_phi_ForwardReg.SetActive(active);
      RefFinal_em_etx.SetActive(active);
      RefFinal_em_ety.SetActive(active);
      RefFinal_em_phi.SetActive(active);
      RefFinal_em_et.SetActive(active);
      RefFinal_em_sumet.SetActive(active);
      RefFinal_em_etx_CentralReg.SetActive(active);
      RefFinal_em_ety_CentralReg.SetActive(active);
      RefFinal_em_sumet_CentralReg.SetActive(active);
      RefFinal_em_phi_CentralReg.SetActive(active);
      RefFinal_em_etx_EndcapRegion.SetActive(active);
      RefFinal_em_ety_EndcapRegion.SetActive(active);
      RefFinal_em_sumet_EndcapRegion.SetActive(active);
      RefFinal_em_phi_EndcapRegion.SetActive(active);
      RefFinal_em_etx_ForwardReg.SetActive(active);
      RefFinal_em_ety_ForwardReg.SetActive(active);
      RefFinal_em_sumet_ForwardReg.SetActive(active);
      RefFinal_em_phi_ForwardReg.SetActive(active);
      RefEle_em_etx.SetActive(active);
      RefEle_em_ety.SetActive(active);
      RefEle_em_phi.SetActive(active);
      RefEle_em_et.SetActive(active);
      RefEle_em_sumet.SetActive(active);
      RefEle_em_etx_CentralReg.SetActive(active);
      RefEle_em_ety_CentralReg.SetActive(active);
      RefEle_em_sumet_CentralReg.SetActive(active);
      RefEle_em_phi_CentralReg.SetActive(active);
      RefEle_em_etx_EndcapRegion.SetActive(active);
      RefEle_em_ety_EndcapRegion.SetActive(active);
      RefEle_em_sumet_EndcapRegion.SetActive(active);
      RefEle_em_phi_EndcapRegion.SetActive(active);
      RefEle_em_etx_ForwardReg.SetActive(active);
      RefEle_em_ety_ForwardReg.SetActive(active);
      RefEle_em_sumet_ForwardReg.SetActive(active);
      RefEle_em_phi_ForwardReg.SetActive(active);
      RefJet_em_etx.SetActive(active);
      RefJet_em_ety.SetActive(active);
      RefJet_em_phi.SetActive(active);
      RefJet_em_et.SetActive(active);
      RefJet_em_sumet.SetActive(active);
      RefJet_em_etx_CentralReg.SetActive(active);
      RefJet_em_ety_CentralReg.SetActive(active);
      RefJet_em_sumet_CentralReg.SetActive(active);
      RefJet_em_phi_CentralReg.SetActive(active);
      RefJet_em_etx_EndcapRegion.SetActive(active);
      RefJet_em_ety_EndcapRegion.SetActive(active);
      RefJet_em_sumet_EndcapRegion.SetActive(active);
      RefJet_em_phi_EndcapRegion.SetActive(active);
      RefJet_em_etx_ForwardReg.SetActive(active);
      RefJet_em_ety_ForwardReg.SetActive(active);
      RefJet_em_sumet_ForwardReg.SetActive(active);
      RefJet_em_phi_ForwardReg.SetActive(active);
      RefMuon_em_etx.SetActive(active);
      RefMuon_em_ety.SetActive(active);
      RefMuon_em_phi.SetActive(active);
      RefMuon_em_et.SetActive(active);
      RefMuon_em_sumet.SetActive(active);
      RefMuon_em_etx_CentralReg.SetActive(active);
      RefMuon_em_ety_CentralReg.SetActive(active);
      RefMuon_em_sumet_CentralReg.SetActive(active);
      RefMuon_em_phi_CentralReg.SetActive(active);
      RefMuon_em_etx_EndcapRegion.SetActive(active);
      RefMuon_em_ety_EndcapRegion.SetActive(active);
      RefMuon_em_sumet_EndcapRegion.SetActive(active);
      RefMuon_em_phi_EndcapRegion.SetActive(active);
      RefMuon_em_etx_ForwardReg.SetActive(active);
      RefMuon_em_ety_ForwardReg.SetActive(active);
      RefMuon_em_sumet_ForwardReg.SetActive(active);
      RefMuon_em_phi_ForwardReg.SetActive(active);
      RefMuon_Track_em_etx.SetActive(active);
      RefMuon_Track_em_ety.SetActive(active);
      RefMuon_Track_em_phi.SetActive(active);
      RefMuon_Track_em_et.SetActive(active);
      RefMuon_Track_em_sumet.SetActive(active);
      RefMuon_Track_em_etx_CentralReg.SetActive(active);
      RefMuon_Track_em_ety_CentralReg.SetActive(active);
      RefMuon_Track_em_sumet_CentralReg.SetActive(active);
      RefMuon_Track_em_phi_CentralReg.SetActive(active);
      RefMuon_Track_em_etx_EndcapRegion.SetActive(active);
      RefMuon_Track_em_ety_EndcapRegion.SetActive(active);
      RefMuon_Track_em_sumet_EndcapRegion.SetActive(active);
      RefMuon_Track_em_phi_EndcapRegion.SetActive(active);
      RefMuon_Track_em_etx_ForwardReg.SetActive(active);
      RefMuon_Track_em_ety_ForwardReg.SetActive(active);
      RefMuon_Track_em_sumet_ForwardReg.SetActive(active);
      RefMuon_Track_em_phi_ForwardReg.SetActive(active);
      RefGamma_em_etx.SetActive(active);
      RefGamma_em_ety.SetActive(active);
      RefGamma_em_phi.SetActive(active);
      RefGamma_em_et.SetActive(active);
      RefGamma_em_sumet.SetActive(active);
      RefGamma_em_etx_CentralReg.SetActive(active);
      RefGamma_em_ety_CentralReg.SetActive(active);
      RefGamma_em_sumet_CentralReg.SetActive(active);
      RefGamma_em_phi_CentralReg.SetActive(active);
      RefGamma_em_etx_EndcapRegion.SetActive(active);
      RefGamma_em_ety_EndcapRegion.SetActive(active);
      RefGamma_em_sumet_EndcapRegion.SetActive(active);
      RefGamma_em_phi_EndcapRegion.SetActive(active);
      RefGamma_em_etx_ForwardReg.SetActive(active);
      RefGamma_em_ety_ForwardReg.SetActive(active);
      RefGamma_em_sumet_ForwardReg.SetActive(active);
      RefGamma_em_phi_ForwardReg.SetActive(active);
      RefTau_em_etx.SetActive(active);
      RefTau_em_ety.SetActive(active);
      RefTau_em_phi.SetActive(active);
      RefTau_em_et.SetActive(active);
      RefTau_em_sumet.SetActive(active);
      RefTau_em_etx_CentralReg.SetActive(active);
      RefTau_em_ety_CentralReg.SetActive(active);
      RefTau_em_sumet_CentralReg.SetActive(active);
      RefTau_em_phi_CentralReg.SetActive(active);
      RefTau_em_etx_EndcapRegion.SetActive(active);
      RefTau_em_ety_EndcapRegion.SetActive(active);
      RefTau_em_sumet_EndcapRegion.SetActive(active);
      RefTau_em_phi_EndcapRegion.SetActive(active);
      RefTau_em_etx_ForwardReg.SetActive(active);
      RefTau_em_ety_ForwardReg.SetActive(active);
      RefTau_em_sumet_ForwardReg.SetActive(active);
      RefTau_em_phi_ForwardReg.SetActive(active);
      RefJet_JVF_etx.SetActive(active);
      RefJet_JVF_ety.SetActive(active);
      RefJet_JVF_phi.SetActive(active);
      RefJet_JVF_et.SetActive(active);
      RefJet_JVF_sumet.SetActive(active);
      RefJet_JVF_etx_CentralReg.SetActive(active);
      RefJet_JVF_ety_CentralReg.SetActive(active);
      RefJet_JVF_sumet_CentralReg.SetActive(active);
      RefJet_JVF_phi_CentralReg.SetActive(active);
      RefJet_JVF_etx_EndcapRegion.SetActive(active);
      RefJet_JVF_ety_EndcapRegion.SetActive(active);
      RefJet_JVF_sumet_EndcapRegion.SetActive(active);
      RefJet_JVF_phi_EndcapRegion.SetActive(active);
      RefJet_JVF_etx_ForwardReg.SetActive(active);
      RefJet_JVF_ety_ForwardReg.SetActive(active);
      RefJet_JVF_sumet_ForwardReg.SetActive(active);
      RefJet_JVF_phi_ForwardReg.SetActive(active);
      RefJet_JVFCut_etx.SetActive(active);
      RefJet_JVFCut_ety.SetActive(active);
      RefJet_JVFCut_phi.SetActive(active);
      RefJet_JVFCut_et.SetActive(active);
      RefJet_JVFCut_sumet.SetActive(active);
      RefJet_JVFCut_etx_CentralReg.SetActive(active);
      RefJet_JVFCut_ety_CentralReg.SetActive(active);
      RefJet_JVFCut_sumet_CentralReg.SetActive(active);
      RefJet_JVFCut_phi_CentralReg.SetActive(active);
      RefJet_JVFCut_etx_EndcapRegion.SetActive(active);
      RefJet_JVFCut_ety_EndcapRegion.SetActive(active);
      RefJet_JVFCut_sumet_EndcapRegion.SetActive(active);
      RefJet_JVFCut_phi_EndcapRegion.SetActive(active);
      RefJet_JVFCut_etx_ForwardReg.SetActive(active);
      RefJet_JVFCut_ety_ForwardReg.SetActive(active);
      RefJet_JVFCut_sumet_ForwardReg.SetActive(active);
      RefJet_JVFCut_phi_ForwardReg.SetActive(active);
      CellOut_Eflow_STVF_etx.SetActive(active);
      CellOut_Eflow_STVF_ety.SetActive(active);
      CellOut_Eflow_STVF_phi.SetActive(active);
      CellOut_Eflow_STVF_et.SetActive(active);
      CellOut_Eflow_STVF_sumet.SetActive(active);
      CellOut_Eflow_STVF_etx_CentralReg.SetActive(active);
      CellOut_Eflow_STVF_ety_CentralReg.SetActive(active);
      CellOut_Eflow_STVF_sumet_CentralReg.SetActive(active);
      CellOut_Eflow_STVF_phi_CentralReg.SetActive(active);
      CellOut_Eflow_STVF_etx_EndcapRegion.SetActive(active);
      CellOut_Eflow_STVF_ety_EndcapRegion.SetActive(active);
      CellOut_Eflow_STVF_sumet_EndcapRegion.SetActive(active);
      CellOut_Eflow_STVF_phi_EndcapRegion.SetActive(active);
      CellOut_Eflow_STVF_etx_ForwardReg.SetActive(active);
      CellOut_Eflow_STVF_ety_ForwardReg.SetActive(active);
      CellOut_Eflow_STVF_sumet_ForwardReg.SetActive(active);
      CellOut_Eflow_STVF_phi_ForwardReg.SetActive(active);
      CellOut_Eflow_JetArea_etx.SetActive(active);
      CellOut_Eflow_JetArea_ety.SetActive(active);
      CellOut_Eflow_JetArea_phi.SetActive(active);
      CellOut_Eflow_JetArea_et.SetActive(active);
      CellOut_Eflow_JetArea_sumet.SetActive(active);
      CellOut_Eflow_JetArea_etx_CentralReg.SetActive(active);
      CellOut_Eflow_JetArea_ety_CentralReg.SetActive(active);
      CellOut_Eflow_JetArea_sumet_CentralReg.SetActive(active);
      CellOut_Eflow_JetArea_phi_CentralReg.SetActive(active);
      CellOut_Eflow_JetArea_etx_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetArea_ety_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetArea_sumet_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetArea_phi_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetArea_etx_ForwardReg.SetActive(active);
      CellOut_Eflow_JetArea_ety_ForwardReg.SetActive(active);
      CellOut_Eflow_JetArea_sumet_ForwardReg.SetActive(active);
      CellOut_Eflow_JetArea_phi_ForwardReg.SetActive(active);
      CellOut_Eflow_JetAreaJVF_etx.SetActive(active);
      CellOut_Eflow_JetAreaJVF_ety.SetActive(active);
      CellOut_Eflow_JetAreaJVF_phi.SetActive(active);
      CellOut_Eflow_JetAreaJVF_et.SetActive(active);
      CellOut_Eflow_JetAreaJVF_sumet.SetActive(active);
      CellOut_Eflow_JetAreaJVF_etx_CentralReg.SetActive(active);
      CellOut_Eflow_JetAreaJVF_ety_CentralReg.SetActive(active);
      CellOut_Eflow_JetAreaJVF_sumet_CentralReg.SetActive(active);
      CellOut_Eflow_JetAreaJVF_phi_CentralReg.SetActive(active);
      CellOut_Eflow_JetAreaJVF_etx_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetAreaJVF_ety_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetAreaJVF_phi_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetAreaJVF_etx_ForwardReg.SetActive(active);
      CellOut_Eflow_JetAreaJVF_ety_ForwardReg.SetActive(active);
      CellOut_Eflow_JetAreaJVF_sumet_ForwardReg.SetActive(active);
      CellOut_Eflow_JetAreaJVF_phi_ForwardReg.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_etx.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_ety.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_phi.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_et.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_sumet.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg.SetActive(active);
      CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg.SetActive(active);
      RefFinal_STVF_etx.SetActive(active);
      RefFinal_STVF_ety.SetActive(active);
      RefFinal_STVF_phi.SetActive(active);
      RefFinal_STVF_et.SetActive(active);
      RefFinal_STVF_sumet.SetActive(active);
      RefFinal_STVF_etx_CentralReg.SetActive(active);
      RefFinal_STVF_ety_CentralReg.SetActive(active);
      RefFinal_STVF_sumet_CentralReg.SetActive(active);
      RefFinal_STVF_phi_CentralReg.SetActive(active);
      RefFinal_STVF_etx_EndcapRegion.SetActive(active);
      RefFinal_STVF_ety_EndcapRegion.SetActive(active);
      RefFinal_STVF_sumet_EndcapRegion.SetActive(active);
      RefFinal_STVF_phi_EndcapRegion.SetActive(active);
      RefFinal_STVF_etx_ForwardReg.SetActive(active);
      RefFinal_STVF_ety_ForwardReg.SetActive(active);
      RefFinal_STVF_sumet_ForwardReg.SetActive(active);
      RefFinal_STVF_phi_ForwardReg.SetActive(active);
      CellOut_Eflow_etx.SetActive(active);
      CellOut_Eflow_ety.SetActive(active);
      CellOut_Eflow_phi.SetActive(active);
      CellOut_Eflow_et.SetActive(active);
      CellOut_Eflow_sumet.SetActive(active);
      CellOut_Eflow_etx_CentralReg.SetActive(active);
      CellOut_Eflow_ety_CentralReg.SetActive(active);
      CellOut_Eflow_sumet_CentralReg.SetActive(active);
      CellOut_Eflow_phi_CentralReg.SetActive(active);
      CellOut_Eflow_etx_EndcapRegion.SetActive(active);
      CellOut_Eflow_ety_EndcapRegion.SetActive(active);
      CellOut_Eflow_sumet_EndcapRegion.SetActive(active);
      CellOut_Eflow_phi_EndcapRegion.SetActive(active);
      CellOut_Eflow_etx_ForwardReg.SetActive(active);
      CellOut_Eflow_ety_ForwardReg.SetActive(active);
      CellOut_Eflow_sumet_ForwardReg.SetActive(active);
      CellOut_Eflow_phi_ForwardReg.SetActive(active);
      CellOut_Eflow_Muid_etx.SetActive(active);
      CellOut_Eflow_Muid_ety.SetActive(active);
      CellOut_Eflow_Muid_phi.SetActive(active);
      CellOut_Eflow_Muid_et.SetActive(active);
      CellOut_Eflow_Muid_sumet.SetActive(active);
      CellOut_Eflow_Muid_etx_CentralReg.SetActive(active);
      CellOut_Eflow_Muid_ety_CentralReg.SetActive(active);
      CellOut_Eflow_Muid_sumet_CentralReg.SetActive(active);
      CellOut_Eflow_Muid_phi_CentralReg.SetActive(active);
      CellOut_Eflow_Muid_etx_EndcapRegion.SetActive(active);
      CellOut_Eflow_Muid_ety_EndcapRegion.SetActive(active);
      CellOut_Eflow_Muid_sumet_EndcapRegion.SetActive(active);
      CellOut_Eflow_Muid_phi_EndcapRegion.SetActive(active);
      CellOut_Eflow_Muid_etx_ForwardReg.SetActive(active);
      CellOut_Eflow_Muid_ety_ForwardReg.SetActive(active);
      CellOut_Eflow_Muid_sumet_ForwardReg.SetActive(active);
      CellOut_Eflow_Muid_phi_ForwardReg.SetActive(active);
      CellOut_Eflow_Muons_etx.SetActive(active);
      CellOut_Eflow_Muons_ety.SetActive(active);
      CellOut_Eflow_Muons_phi.SetActive(active);
      CellOut_Eflow_Muons_et.SetActive(active);
      CellOut_Eflow_Muons_sumet.SetActive(active);
      CellOut_Eflow_Muons_etx_CentralReg.SetActive(active);
      CellOut_Eflow_Muons_ety_CentralReg.SetActive(active);
      CellOut_Eflow_Muons_sumet_CentralReg.SetActive(active);
      CellOut_Eflow_Muons_phi_CentralReg.SetActive(active);
      CellOut_Eflow_Muons_etx_EndcapRegion.SetActive(active);
      CellOut_Eflow_Muons_ety_EndcapRegion.SetActive(active);
      CellOut_Eflow_Muons_sumet_EndcapRegion.SetActive(active);
      CellOut_Eflow_Muons_phi_EndcapRegion.SetActive(active);
      CellOut_Eflow_Muons_etx_ForwardReg.SetActive(active);
      CellOut_Eflow_Muons_ety_ForwardReg.SetActive(active);
      CellOut_Eflow_Muons_sumet_ForwardReg.SetActive(active);
      CellOut_Eflow_Muons_phi_ForwardReg.SetActive(active);
      RefMuon_Track_etx.SetActive(active);
      RefMuon_Track_ety.SetActive(active);
      RefMuon_Track_phi.SetActive(active);
      RefMuon_Track_et.SetActive(active);
      RefMuon_Track_sumet.SetActive(active);
      RefMuon_Track_etx_CentralReg.SetActive(active);
      RefMuon_Track_ety_CentralReg.SetActive(active);
      RefMuon_Track_sumet_CentralReg.SetActive(active);
      RefMuon_Track_phi_CentralReg.SetActive(active);
      RefMuon_Track_etx_EndcapRegion.SetActive(active);
      RefMuon_Track_ety_EndcapRegion.SetActive(active);
      RefMuon_Track_sumet_EndcapRegion.SetActive(active);
      RefMuon_Track_phi_EndcapRegion.SetActive(active);
      RefMuon_Track_etx_ForwardReg.SetActive(active);
      RefMuon_Track_ety_ForwardReg.SetActive(active);
      RefMuon_Track_sumet_ForwardReg.SetActive(active);
      RefMuon_Track_phi_ForwardReg.SetActive(active);
      RefMuon_Track_Staco_etx.SetActive(active);
      RefMuon_Track_Staco_ety.SetActive(active);
      RefMuon_Track_Staco_phi.SetActive(active);
      RefMuon_Track_Staco_et.SetActive(active);
      RefMuon_Track_Staco_sumet.SetActive(active);
      RefMuon_Track_Staco_etx_CentralReg.SetActive(active);
      RefMuon_Track_Staco_ety_CentralReg.SetActive(active);
      RefMuon_Track_Staco_sumet_CentralReg.SetActive(active);
      RefMuon_Track_Staco_phi_CentralReg.SetActive(active);
      RefMuon_Track_Staco_etx_EndcapRegion.SetActive(active);
      RefMuon_Track_Staco_ety_EndcapRegion.SetActive(active);
      RefMuon_Track_Staco_sumet_EndcapRegion.SetActive(active);
      RefMuon_Track_Staco_phi_EndcapRegion.SetActive(active);
      RefMuon_Track_Staco_etx_ForwardReg.SetActive(active);
      RefMuon_Track_Staco_ety_ForwardReg.SetActive(active);
      RefMuon_Track_Staco_sumet_ForwardReg.SetActive(active);
      RefMuon_Track_Staco_phi_ForwardReg.SetActive(active);
      RefMuon_Track_Muid_etx.SetActive(active);
      RefMuon_Track_Muid_ety.SetActive(active);
      RefMuon_Track_Muid_phi.SetActive(active);
      RefMuon_Track_Muid_et.SetActive(active);
      RefMuon_Track_Muid_sumet.SetActive(active);
      RefMuon_Track_Muid_etx_CentralReg.SetActive(active);
      RefMuon_Track_Muid_ety_CentralReg.SetActive(active);
      RefMuon_Track_Muid_sumet_CentralReg.SetActive(active);
      RefMuon_Track_Muid_phi_CentralReg.SetActive(active);
      RefMuon_Track_Muid_etx_EndcapRegion.SetActive(active);
      RefMuon_Track_Muid_ety_EndcapRegion.SetActive(active);
      RefMuon_Track_Muid_sumet_EndcapRegion.SetActive(active);
      RefMuon_Track_Muid_phi_EndcapRegion.SetActive(active);
      RefMuon_Track_Muid_etx_ForwardReg.SetActive(active);
      RefMuon_Track_Muid_ety_ForwardReg.SetActive(active);
      RefMuon_Track_Muid_sumet_ForwardReg.SetActive(active);
      RefMuon_Track_Muid_phi_ForwardReg.SetActive(active);
      RefMuons_Track_etx.SetActive(active);
      RefMuons_Track_ety.SetActive(active);
      RefMuons_Track_phi.SetActive(active);
      RefMuons_Track_et.SetActive(active);
      RefMuons_Track_sumet.SetActive(active);
      RefMuons_Track_etx_CentralReg.SetActive(active);
      RefMuons_Track_ety_CentralReg.SetActive(active);
      RefMuons_Track_sumet_CentralReg.SetActive(active);
      RefMuons_Track_phi_CentralReg.SetActive(active);
      RefMuons_Track_etx_EndcapRegion.SetActive(active);
      RefMuons_Track_ety_EndcapRegion.SetActive(active);
      RefMuons_Track_sumet_EndcapRegion.SetActive(active);
      RefMuons_Track_phi_EndcapRegion.SetActive(active);
      RefMuons_Track_etx_ForwardReg.SetActive(active);
      RefMuons_Track_ety_ForwardReg.SetActive(active);
      RefMuons_Track_sumet_ForwardReg.SetActive(active);
      RefMuons_Track_phi_ForwardReg.SetActive(active);
      RefFinal_BDTMedium_etx.SetActive(active);
      RefFinal_BDTMedium_ety.SetActive(active);
      RefFinal_BDTMedium_phi.SetActive(active);
      RefFinal_BDTMedium_et.SetActive(active);
      RefFinal_BDTMedium_sumet.SetActive(active);
      RefFinal_BDTMedium_etx_CentralReg.SetActive(active);
      RefFinal_BDTMedium_ety_CentralReg.SetActive(active);
      RefFinal_BDTMedium_sumet_CentralReg.SetActive(active);
      RefFinal_BDTMedium_phi_CentralReg.SetActive(active);
      RefFinal_BDTMedium_etx_EndcapRegion.SetActive(active);
      RefFinal_BDTMedium_ety_EndcapRegion.SetActive(active);
      RefFinal_BDTMedium_sumet_EndcapRegion.SetActive(active);
      RefFinal_BDTMedium_phi_EndcapRegion.SetActive(active);
      RefFinal_BDTMedium_etx_ForwardReg.SetActive(active);
      RefFinal_BDTMedium_ety_ForwardReg.SetActive(active);
      RefFinal_BDTMedium_sumet_ForwardReg.SetActive(active);
      RefFinal_BDTMedium_phi_ForwardReg.SetActive(active);
      RefGamma_BDTMedium_etx.SetActive(active);
      RefGamma_BDTMedium_ety.SetActive(active);
      RefGamma_BDTMedium_phi.SetActive(active);
      RefGamma_BDTMedium_et.SetActive(active);
      RefGamma_BDTMedium_sumet.SetActive(active);
      RefGamma_BDTMedium_etx_CentralReg.SetActive(active);
      RefGamma_BDTMedium_ety_CentralReg.SetActive(active);
      RefGamma_BDTMedium_sumet_CentralReg.SetActive(active);
      RefGamma_BDTMedium_phi_CentralReg.SetActive(active);
      RefGamma_BDTMedium_etx_EndcapRegion.SetActive(active);
      RefGamma_BDTMedium_ety_EndcapRegion.SetActive(active);
      RefGamma_BDTMedium_sumet_EndcapRegion.SetActive(active);
      RefGamma_BDTMedium_phi_EndcapRegion.SetActive(active);
      RefGamma_BDTMedium_etx_ForwardReg.SetActive(active);
      RefGamma_BDTMedium_ety_ForwardReg.SetActive(active);
      RefGamma_BDTMedium_sumet_ForwardReg.SetActive(active);
      RefGamma_BDTMedium_phi_ForwardReg.SetActive(active);
      RefEle_BDTMedium_etx.SetActive(active);
      RefEle_BDTMedium_ety.SetActive(active);
      RefEle_BDTMedium_phi.SetActive(active);
      RefEle_BDTMedium_et.SetActive(active);
      RefEle_BDTMedium_sumet.SetActive(active);
      RefEle_BDTMedium_etx_CentralReg.SetActive(active);
      RefEle_BDTMedium_ety_CentralReg.SetActive(active);
      RefEle_BDTMedium_sumet_CentralReg.SetActive(active);
      RefEle_BDTMedium_phi_CentralReg.SetActive(active);
      RefEle_BDTMedium_etx_EndcapRegion.SetActive(active);
      RefEle_BDTMedium_ety_EndcapRegion.SetActive(active);
      RefEle_BDTMedium_sumet_EndcapRegion.SetActive(active);
      RefEle_BDTMedium_phi_EndcapRegion.SetActive(active);
      RefEle_BDTMedium_etx_ForwardReg.SetActive(active);
      RefEle_BDTMedium_ety_ForwardReg.SetActive(active);
      RefEle_BDTMedium_sumet_ForwardReg.SetActive(active);
      RefEle_BDTMedium_phi_ForwardReg.SetActive(active);
      RefTau_BDTMedium_etx.SetActive(active);
      RefTau_BDTMedium_ety.SetActive(active);
      RefTau_BDTMedium_phi.SetActive(active);
      RefTau_BDTMedium_et.SetActive(active);
      RefTau_BDTMedium_sumet.SetActive(active);
      RefTau_BDTMedium_etx_CentralReg.SetActive(active);
      RefTau_BDTMedium_ety_CentralReg.SetActive(active);
      RefTau_BDTMedium_sumet_CentralReg.SetActive(active);
      RefTau_BDTMedium_phi_CentralReg.SetActive(active);
      RefTau_BDTMedium_etx_EndcapRegion.SetActive(active);
      RefTau_BDTMedium_ety_EndcapRegion.SetActive(active);
      RefTau_BDTMedium_sumet_EndcapRegion.SetActive(active);
      RefTau_BDTMedium_phi_EndcapRegion.SetActive(active);
      RefTau_BDTMedium_etx_ForwardReg.SetActive(active);
      RefTau_BDTMedium_ety_ForwardReg.SetActive(active);
      RefTau_BDTMedium_sumet_ForwardReg.SetActive(active);
      RefTau_BDTMedium_phi_ForwardReg.SetActive(active);
      RefJet_BDTMedium_etx.SetActive(active);
      RefJet_BDTMedium_ety.SetActive(active);
      RefJet_BDTMedium_phi.SetActive(active);
      RefJet_BDTMedium_et.SetActive(active);
      RefJet_BDTMedium_sumet.SetActive(active);
      RefJet_BDTMedium_etx_CentralReg.SetActive(active);
      RefJet_BDTMedium_ety_CentralReg.SetActive(active);
      RefJet_BDTMedium_sumet_CentralReg.SetActive(active);
      RefJet_BDTMedium_phi_CentralReg.SetActive(active);
      RefJet_BDTMedium_etx_EndcapRegion.SetActive(active);
      RefJet_BDTMedium_ety_EndcapRegion.SetActive(active);
      RefJet_BDTMedium_sumet_EndcapRegion.SetActive(active);
      RefJet_BDTMedium_phi_EndcapRegion.SetActive(active);
      RefJet_BDTMedium_etx_ForwardReg.SetActive(active);
      RefJet_BDTMedium_ety_ForwardReg.SetActive(active);
      RefJet_BDTMedium_sumet_ForwardReg.SetActive(active);
      RefJet_BDTMedium_phi_ForwardReg.SetActive(active);
      RefMuon_Staco_BDTMedium_etx.SetActive(active);
      RefMuon_Staco_BDTMedium_ety.SetActive(active);
      RefMuon_Staco_BDTMedium_phi.SetActive(active);
      RefMuon_Staco_BDTMedium_et.SetActive(active);
      RefMuon_Staco_BDTMedium_sumet.SetActive(active);
      RefMuon_Staco_BDTMedium_etx_CentralReg.SetActive(active);
      RefMuon_Staco_BDTMedium_ety_CentralReg.SetActive(active);
      RefMuon_Staco_BDTMedium_sumet_CentralReg.SetActive(active);
      RefMuon_Staco_BDTMedium_phi_CentralReg.SetActive(active);
      RefMuon_Staco_BDTMedium_etx_EndcapRegion.SetActive(active);
      RefMuon_Staco_BDTMedium_ety_EndcapRegion.SetActive(active);
      RefMuon_Staco_BDTMedium_sumet_EndcapRegion.SetActive(active);
      RefMuon_Staco_BDTMedium_phi_EndcapRegion.SetActive(active);
      RefMuon_Staco_BDTMedium_etx_ForwardReg.SetActive(active);
      RefMuon_Staco_BDTMedium_ety_ForwardReg.SetActive(active);
      RefMuon_Staco_BDTMedium_sumet_ForwardReg.SetActive(active);
      RefMuon_Staco_BDTMedium_phi_ForwardReg.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_etx.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_ety.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_phi.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_et.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
      RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_etx.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_ety.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_phi.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_et.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
      RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_etx.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_ety.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_phi.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_et.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
      RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_etx.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_ety.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_phi.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_et.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
      RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_etx.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_ety.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_phi.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_et.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
      RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_etx.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_ety.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_phi.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_et.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg.SetActive(active);
      RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void MET::ReadAllActive()
  {
    if(RefFinal_etx.IsActive()) RefFinal_etx();
    if(RefFinal_ety.IsActive()) RefFinal_ety();
    if(RefFinal_phi.IsActive()) RefFinal_phi();
    if(RefFinal_et.IsActive()) RefFinal_et();
    if(RefFinal_sumet.IsActive()) RefFinal_sumet();
    if(RefFinal_etx_CentralReg.IsActive()) RefFinal_etx_CentralReg();
    if(RefFinal_ety_CentralReg.IsActive()) RefFinal_ety_CentralReg();
    if(RefFinal_sumet_CentralReg.IsActive()) RefFinal_sumet_CentralReg();
    if(RefFinal_phi_CentralReg.IsActive()) RefFinal_phi_CentralReg();
    if(RefFinal_etx_EndcapRegion.IsActive()) RefFinal_etx_EndcapRegion();
    if(RefFinal_ety_EndcapRegion.IsActive()) RefFinal_ety_EndcapRegion();
    if(RefFinal_sumet_EndcapRegion.IsActive()) RefFinal_sumet_EndcapRegion();
    if(RefFinal_phi_EndcapRegion.IsActive()) RefFinal_phi_EndcapRegion();
    if(RefFinal_etx_ForwardReg.IsActive()) RefFinal_etx_ForwardReg();
    if(RefFinal_ety_ForwardReg.IsActive()) RefFinal_ety_ForwardReg();
    if(RefFinal_sumet_ForwardReg.IsActive()) RefFinal_sumet_ForwardReg();
    if(RefFinal_phi_ForwardReg.IsActive()) RefFinal_phi_ForwardReg();
    if(RefEle_etx.IsActive()) RefEle_etx();
    if(RefEle_ety.IsActive()) RefEle_ety();
    if(RefEle_phi.IsActive()) RefEle_phi();
    if(RefEle_et.IsActive()) RefEle_et();
    if(RefEle_sumet.IsActive()) RefEle_sumet();
    if(RefEle_etx_CentralReg.IsActive()) RefEle_etx_CentralReg();
    if(RefEle_ety_CentralReg.IsActive()) RefEle_ety_CentralReg();
    if(RefEle_sumet_CentralReg.IsActive()) RefEle_sumet_CentralReg();
    if(RefEle_phi_CentralReg.IsActive()) RefEle_phi_CentralReg();
    if(RefEle_etx_EndcapRegion.IsActive()) RefEle_etx_EndcapRegion();
    if(RefEle_ety_EndcapRegion.IsActive()) RefEle_ety_EndcapRegion();
    if(RefEle_sumet_EndcapRegion.IsActive()) RefEle_sumet_EndcapRegion();
    if(RefEle_phi_EndcapRegion.IsActive()) RefEle_phi_EndcapRegion();
    if(RefEle_etx_ForwardReg.IsActive()) RefEle_etx_ForwardReg();
    if(RefEle_ety_ForwardReg.IsActive()) RefEle_ety_ForwardReg();
    if(RefEle_sumet_ForwardReg.IsActive()) RefEle_sumet_ForwardReg();
    if(RefEle_phi_ForwardReg.IsActive()) RefEle_phi_ForwardReg();
    if(RefJet_etx.IsActive()) RefJet_etx();
    if(RefJet_ety.IsActive()) RefJet_ety();
    if(RefJet_phi.IsActive()) RefJet_phi();
    if(RefJet_et.IsActive()) RefJet_et();
    if(RefJet_sumet.IsActive()) RefJet_sumet();
    if(RefJet_etx_CentralReg.IsActive()) RefJet_etx_CentralReg();
    if(RefJet_ety_CentralReg.IsActive()) RefJet_ety_CentralReg();
    if(RefJet_sumet_CentralReg.IsActive()) RefJet_sumet_CentralReg();
    if(RefJet_phi_CentralReg.IsActive()) RefJet_phi_CentralReg();
    if(RefJet_etx_EndcapRegion.IsActive()) RefJet_etx_EndcapRegion();
    if(RefJet_ety_EndcapRegion.IsActive()) RefJet_ety_EndcapRegion();
    if(RefJet_sumet_EndcapRegion.IsActive()) RefJet_sumet_EndcapRegion();
    if(RefJet_phi_EndcapRegion.IsActive()) RefJet_phi_EndcapRegion();
    if(RefJet_etx_ForwardReg.IsActive()) RefJet_etx_ForwardReg();
    if(RefJet_ety_ForwardReg.IsActive()) RefJet_ety_ForwardReg();
    if(RefJet_sumet_ForwardReg.IsActive()) RefJet_sumet_ForwardReg();
    if(RefJet_phi_ForwardReg.IsActive()) RefJet_phi_ForwardReg();
    if(RefMuon_etx.IsActive()) RefMuon_etx();
    if(RefMuon_ety.IsActive()) RefMuon_ety();
    if(RefMuon_phi.IsActive()) RefMuon_phi();
    if(RefMuon_et.IsActive()) RefMuon_et();
    if(RefMuon_sumet.IsActive()) RefMuon_sumet();
    if(RefMuon_etx_CentralReg.IsActive()) RefMuon_etx_CentralReg();
    if(RefMuon_ety_CentralReg.IsActive()) RefMuon_ety_CentralReg();
    if(RefMuon_sumet_CentralReg.IsActive()) RefMuon_sumet_CentralReg();
    if(RefMuon_phi_CentralReg.IsActive()) RefMuon_phi_CentralReg();
    if(RefMuon_etx_EndcapRegion.IsActive()) RefMuon_etx_EndcapRegion();
    if(RefMuon_ety_EndcapRegion.IsActive()) RefMuon_ety_EndcapRegion();
    if(RefMuon_sumet_EndcapRegion.IsActive()) RefMuon_sumet_EndcapRegion();
    if(RefMuon_phi_EndcapRegion.IsActive()) RefMuon_phi_EndcapRegion();
    if(RefMuon_etx_ForwardReg.IsActive()) RefMuon_etx_ForwardReg();
    if(RefMuon_ety_ForwardReg.IsActive()) RefMuon_ety_ForwardReg();
    if(RefMuon_sumet_ForwardReg.IsActive()) RefMuon_sumet_ForwardReg();
    if(RefMuon_phi_ForwardReg.IsActive()) RefMuon_phi_ForwardReg();
    if(RefMuon_Staco_etx.IsActive()) RefMuon_Staco_etx();
    if(RefMuon_Staco_ety.IsActive()) RefMuon_Staco_ety();
    if(RefMuon_Staco_phi.IsActive()) RefMuon_Staco_phi();
    if(RefMuon_Staco_et.IsActive()) RefMuon_Staco_et();
    if(RefMuon_Staco_sumet.IsActive()) RefMuon_Staco_sumet();
    if(RefMuon_Staco_etx_CentralReg.IsActive()) RefMuon_Staco_etx_CentralReg();
    if(RefMuon_Staco_ety_CentralReg.IsActive()) RefMuon_Staco_ety_CentralReg();
    if(RefMuon_Staco_sumet_CentralReg.IsActive()) RefMuon_Staco_sumet_CentralReg();
    if(RefMuon_Staco_phi_CentralReg.IsActive()) RefMuon_Staco_phi_CentralReg();
    if(RefMuon_Staco_etx_EndcapRegion.IsActive()) RefMuon_Staco_etx_EndcapRegion();
    if(RefMuon_Staco_ety_EndcapRegion.IsActive()) RefMuon_Staco_ety_EndcapRegion();
    if(RefMuon_Staco_sumet_EndcapRegion.IsActive()) RefMuon_Staco_sumet_EndcapRegion();
    if(RefMuon_Staco_phi_EndcapRegion.IsActive()) RefMuon_Staco_phi_EndcapRegion();
    if(RefMuon_Staco_etx_ForwardReg.IsActive()) RefMuon_Staco_etx_ForwardReg();
    if(RefMuon_Staco_ety_ForwardReg.IsActive()) RefMuon_Staco_ety_ForwardReg();
    if(RefMuon_Staco_sumet_ForwardReg.IsActive()) RefMuon_Staco_sumet_ForwardReg();
    if(RefMuon_Staco_phi_ForwardReg.IsActive()) RefMuon_Staco_phi_ForwardReg();
    if(RefMuon_Muid_etx.IsActive()) RefMuon_Muid_etx();
    if(RefMuon_Muid_ety.IsActive()) RefMuon_Muid_ety();
    if(RefMuon_Muid_phi.IsActive()) RefMuon_Muid_phi();
    if(RefMuon_Muid_et.IsActive()) RefMuon_Muid_et();
    if(RefMuon_Muid_sumet.IsActive()) RefMuon_Muid_sumet();
    if(RefMuon_Muid_etx_CentralReg.IsActive()) RefMuon_Muid_etx_CentralReg();
    if(RefMuon_Muid_ety_CentralReg.IsActive()) RefMuon_Muid_ety_CentralReg();
    if(RefMuon_Muid_sumet_CentralReg.IsActive()) RefMuon_Muid_sumet_CentralReg();
    if(RefMuon_Muid_phi_CentralReg.IsActive()) RefMuon_Muid_phi_CentralReg();
    if(RefMuon_Muid_etx_EndcapRegion.IsActive()) RefMuon_Muid_etx_EndcapRegion();
    if(RefMuon_Muid_ety_EndcapRegion.IsActive()) RefMuon_Muid_ety_EndcapRegion();
    if(RefMuon_Muid_sumet_EndcapRegion.IsActive()) RefMuon_Muid_sumet_EndcapRegion();
    if(RefMuon_Muid_phi_EndcapRegion.IsActive()) RefMuon_Muid_phi_EndcapRegion();
    if(RefMuon_Muid_etx_ForwardReg.IsActive()) RefMuon_Muid_etx_ForwardReg();
    if(RefMuon_Muid_ety_ForwardReg.IsActive()) RefMuon_Muid_ety_ForwardReg();
    if(RefMuon_Muid_sumet_ForwardReg.IsActive()) RefMuon_Muid_sumet_ForwardReg();
    if(RefMuon_Muid_phi_ForwardReg.IsActive()) RefMuon_Muid_phi_ForwardReg();
    if(RefMuons_etx.IsActive()) RefMuons_etx();
    if(RefMuons_ety.IsActive()) RefMuons_ety();
    if(RefMuons_phi.IsActive()) RefMuons_phi();
    if(RefMuons_et.IsActive()) RefMuons_et();
    if(RefMuons_sumet.IsActive()) RefMuons_sumet();
    if(RefMuons_etx_CentralReg.IsActive()) RefMuons_etx_CentralReg();
    if(RefMuons_ety_CentralReg.IsActive()) RefMuons_ety_CentralReg();
    if(RefMuons_sumet_CentralReg.IsActive()) RefMuons_sumet_CentralReg();
    if(RefMuons_phi_CentralReg.IsActive()) RefMuons_phi_CentralReg();
    if(RefMuons_etx_EndcapRegion.IsActive()) RefMuons_etx_EndcapRegion();
    if(RefMuons_ety_EndcapRegion.IsActive()) RefMuons_ety_EndcapRegion();
    if(RefMuons_sumet_EndcapRegion.IsActive()) RefMuons_sumet_EndcapRegion();
    if(RefMuons_phi_EndcapRegion.IsActive()) RefMuons_phi_EndcapRegion();
    if(RefMuons_etx_ForwardReg.IsActive()) RefMuons_etx_ForwardReg();
    if(RefMuons_ety_ForwardReg.IsActive()) RefMuons_ety_ForwardReg();
    if(RefMuons_sumet_ForwardReg.IsActive()) RefMuons_sumet_ForwardReg();
    if(RefMuons_phi_ForwardReg.IsActive()) RefMuons_phi_ForwardReg();
    if(RefGamma_etx.IsActive()) RefGamma_etx();
    if(RefGamma_ety.IsActive()) RefGamma_ety();
    if(RefGamma_phi.IsActive()) RefGamma_phi();
    if(RefGamma_et.IsActive()) RefGamma_et();
    if(RefGamma_sumet.IsActive()) RefGamma_sumet();
    if(RefGamma_etx_CentralReg.IsActive()) RefGamma_etx_CentralReg();
    if(RefGamma_ety_CentralReg.IsActive()) RefGamma_ety_CentralReg();
    if(RefGamma_sumet_CentralReg.IsActive()) RefGamma_sumet_CentralReg();
    if(RefGamma_phi_CentralReg.IsActive()) RefGamma_phi_CentralReg();
    if(RefGamma_etx_EndcapRegion.IsActive()) RefGamma_etx_EndcapRegion();
    if(RefGamma_ety_EndcapRegion.IsActive()) RefGamma_ety_EndcapRegion();
    if(RefGamma_sumet_EndcapRegion.IsActive()) RefGamma_sumet_EndcapRegion();
    if(RefGamma_phi_EndcapRegion.IsActive()) RefGamma_phi_EndcapRegion();
    if(RefGamma_etx_ForwardReg.IsActive()) RefGamma_etx_ForwardReg();
    if(RefGamma_ety_ForwardReg.IsActive()) RefGamma_ety_ForwardReg();
    if(RefGamma_sumet_ForwardReg.IsActive()) RefGamma_sumet_ForwardReg();
    if(RefGamma_phi_ForwardReg.IsActive()) RefGamma_phi_ForwardReg();
    if(RefTau_etx.IsActive()) RefTau_etx();
    if(RefTau_ety.IsActive()) RefTau_ety();
    if(RefTau_phi.IsActive()) RefTau_phi();
    if(RefTau_et.IsActive()) RefTau_et();
    if(RefTau_sumet.IsActive()) RefTau_sumet();
    if(RefTau_etx_CentralReg.IsActive()) RefTau_etx_CentralReg();
    if(RefTau_ety_CentralReg.IsActive()) RefTau_ety_CentralReg();
    if(RefTau_sumet_CentralReg.IsActive()) RefTau_sumet_CentralReg();
    if(RefTau_phi_CentralReg.IsActive()) RefTau_phi_CentralReg();
    if(RefTau_etx_EndcapRegion.IsActive()) RefTau_etx_EndcapRegion();
    if(RefTau_ety_EndcapRegion.IsActive()) RefTau_ety_EndcapRegion();
    if(RefTau_sumet_EndcapRegion.IsActive()) RefTau_sumet_EndcapRegion();
    if(RefTau_phi_EndcapRegion.IsActive()) RefTau_phi_EndcapRegion();
    if(RefTau_etx_ForwardReg.IsActive()) RefTau_etx_ForwardReg();
    if(RefTau_ety_ForwardReg.IsActive()) RefTau_ety_ForwardReg();
    if(RefTau_sumet_ForwardReg.IsActive()) RefTau_sumet_ForwardReg();
    if(RefTau_phi_ForwardReg.IsActive()) RefTau_phi_ForwardReg();
    if(RefFinal_em_etx.IsActive()) RefFinal_em_etx();
    if(RefFinal_em_ety.IsActive()) RefFinal_em_ety();
    if(RefFinal_em_phi.IsActive()) RefFinal_em_phi();
    if(RefFinal_em_et.IsActive()) RefFinal_em_et();
    if(RefFinal_em_sumet.IsActive()) RefFinal_em_sumet();
    if(RefFinal_em_etx_CentralReg.IsActive()) RefFinal_em_etx_CentralReg();
    if(RefFinal_em_ety_CentralReg.IsActive()) RefFinal_em_ety_CentralReg();
    if(RefFinal_em_sumet_CentralReg.IsActive()) RefFinal_em_sumet_CentralReg();
    if(RefFinal_em_phi_CentralReg.IsActive()) RefFinal_em_phi_CentralReg();
    if(RefFinal_em_etx_EndcapRegion.IsActive()) RefFinal_em_etx_EndcapRegion();
    if(RefFinal_em_ety_EndcapRegion.IsActive()) RefFinal_em_ety_EndcapRegion();
    if(RefFinal_em_sumet_EndcapRegion.IsActive()) RefFinal_em_sumet_EndcapRegion();
    if(RefFinal_em_phi_EndcapRegion.IsActive()) RefFinal_em_phi_EndcapRegion();
    if(RefFinal_em_etx_ForwardReg.IsActive()) RefFinal_em_etx_ForwardReg();
    if(RefFinal_em_ety_ForwardReg.IsActive()) RefFinal_em_ety_ForwardReg();
    if(RefFinal_em_sumet_ForwardReg.IsActive()) RefFinal_em_sumet_ForwardReg();
    if(RefFinal_em_phi_ForwardReg.IsActive()) RefFinal_em_phi_ForwardReg();
    if(RefEle_em_etx.IsActive()) RefEle_em_etx();
    if(RefEle_em_ety.IsActive()) RefEle_em_ety();
    if(RefEle_em_phi.IsActive()) RefEle_em_phi();
    if(RefEle_em_et.IsActive()) RefEle_em_et();
    if(RefEle_em_sumet.IsActive()) RefEle_em_sumet();
    if(RefEle_em_etx_CentralReg.IsActive()) RefEle_em_etx_CentralReg();
    if(RefEle_em_ety_CentralReg.IsActive()) RefEle_em_ety_CentralReg();
    if(RefEle_em_sumet_CentralReg.IsActive()) RefEle_em_sumet_CentralReg();
    if(RefEle_em_phi_CentralReg.IsActive()) RefEle_em_phi_CentralReg();
    if(RefEle_em_etx_EndcapRegion.IsActive()) RefEle_em_etx_EndcapRegion();
    if(RefEle_em_ety_EndcapRegion.IsActive()) RefEle_em_ety_EndcapRegion();
    if(RefEle_em_sumet_EndcapRegion.IsActive()) RefEle_em_sumet_EndcapRegion();
    if(RefEle_em_phi_EndcapRegion.IsActive()) RefEle_em_phi_EndcapRegion();
    if(RefEle_em_etx_ForwardReg.IsActive()) RefEle_em_etx_ForwardReg();
    if(RefEle_em_ety_ForwardReg.IsActive()) RefEle_em_ety_ForwardReg();
    if(RefEle_em_sumet_ForwardReg.IsActive()) RefEle_em_sumet_ForwardReg();
    if(RefEle_em_phi_ForwardReg.IsActive()) RefEle_em_phi_ForwardReg();
    if(RefJet_em_etx.IsActive()) RefJet_em_etx();
    if(RefJet_em_ety.IsActive()) RefJet_em_ety();
    if(RefJet_em_phi.IsActive()) RefJet_em_phi();
    if(RefJet_em_et.IsActive()) RefJet_em_et();
    if(RefJet_em_sumet.IsActive()) RefJet_em_sumet();
    if(RefJet_em_etx_CentralReg.IsActive()) RefJet_em_etx_CentralReg();
    if(RefJet_em_ety_CentralReg.IsActive()) RefJet_em_ety_CentralReg();
    if(RefJet_em_sumet_CentralReg.IsActive()) RefJet_em_sumet_CentralReg();
    if(RefJet_em_phi_CentralReg.IsActive()) RefJet_em_phi_CentralReg();
    if(RefJet_em_etx_EndcapRegion.IsActive()) RefJet_em_etx_EndcapRegion();
    if(RefJet_em_ety_EndcapRegion.IsActive()) RefJet_em_ety_EndcapRegion();
    if(RefJet_em_sumet_EndcapRegion.IsActive()) RefJet_em_sumet_EndcapRegion();
    if(RefJet_em_phi_EndcapRegion.IsActive()) RefJet_em_phi_EndcapRegion();
    if(RefJet_em_etx_ForwardReg.IsActive()) RefJet_em_etx_ForwardReg();
    if(RefJet_em_ety_ForwardReg.IsActive()) RefJet_em_ety_ForwardReg();
    if(RefJet_em_sumet_ForwardReg.IsActive()) RefJet_em_sumet_ForwardReg();
    if(RefJet_em_phi_ForwardReg.IsActive()) RefJet_em_phi_ForwardReg();
    if(RefMuon_em_etx.IsActive()) RefMuon_em_etx();
    if(RefMuon_em_ety.IsActive()) RefMuon_em_ety();
    if(RefMuon_em_phi.IsActive()) RefMuon_em_phi();
    if(RefMuon_em_et.IsActive()) RefMuon_em_et();
    if(RefMuon_em_sumet.IsActive()) RefMuon_em_sumet();
    if(RefMuon_em_etx_CentralReg.IsActive()) RefMuon_em_etx_CentralReg();
    if(RefMuon_em_ety_CentralReg.IsActive()) RefMuon_em_ety_CentralReg();
    if(RefMuon_em_sumet_CentralReg.IsActive()) RefMuon_em_sumet_CentralReg();
    if(RefMuon_em_phi_CentralReg.IsActive()) RefMuon_em_phi_CentralReg();
    if(RefMuon_em_etx_EndcapRegion.IsActive()) RefMuon_em_etx_EndcapRegion();
    if(RefMuon_em_ety_EndcapRegion.IsActive()) RefMuon_em_ety_EndcapRegion();
    if(RefMuon_em_sumet_EndcapRegion.IsActive()) RefMuon_em_sumet_EndcapRegion();
    if(RefMuon_em_phi_EndcapRegion.IsActive()) RefMuon_em_phi_EndcapRegion();
    if(RefMuon_em_etx_ForwardReg.IsActive()) RefMuon_em_etx_ForwardReg();
    if(RefMuon_em_ety_ForwardReg.IsActive()) RefMuon_em_ety_ForwardReg();
    if(RefMuon_em_sumet_ForwardReg.IsActive()) RefMuon_em_sumet_ForwardReg();
    if(RefMuon_em_phi_ForwardReg.IsActive()) RefMuon_em_phi_ForwardReg();
    if(RefMuon_Track_em_etx.IsActive()) RefMuon_Track_em_etx();
    if(RefMuon_Track_em_ety.IsActive()) RefMuon_Track_em_ety();
    if(RefMuon_Track_em_phi.IsActive()) RefMuon_Track_em_phi();
    if(RefMuon_Track_em_et.IsActive()) RefMuon_Track_em_et();
    if(RefMuon_Track_em_sumet.IsActive()) RefMuon_Track_em_sumet();
    if(RefMuon_Track_em_etx_CentralReg.IsActive()) RefMuon_Track_em_etx_CentralReg();
    if(RefMuon_Track_em_ety_CentralReg.IsActive()) RefMuon_Track_em_ety_CentralReg();
    if(RefMuon_Track_em_sumet_CentralReg.IsActive()) RefMuon_Track_em_sumet_CentralReg();
    if(RefMuon_Track_em_phi_CentralReg.IsActive()) RefMuon_Track_em_phi_CentralReg();
    if(RefMuon_Track_em_etx_EndcapRegion.IsActive()) RefMuon_Track_em_etx_EndcapRegion();
    if(RefMuon_Track_em_ety_EndcapRegion.IsActive()) RefMuon_Track_em_ety_EndcapRegion();
    if(RefMuon_Track_em_sumet_EndcapRegion.IsActive()) RefMuon_Track_em_sumet_EndcapRegion();
    if(RefMuon_Track_em_phi_EndcapRegion.IsActive()) RefMuon_Track_em_phi_EndcapRegion();
    if(RefMuon_Track_em_etx_ForwardReg.IsActive()) RefMuon_Track_em_etx_ForwardReg();
    if(RefMuon_Track_em_ety_ForwardReg.IsActive()) RefMuon_Track_em_ety_ForwardReg();
    if(RefMuon_Track_em_sumet_ForwardReg.IsActive()) RefMuon_Track_em_sumet_ForwardReg();
    if(RefMuon_Track_em_phi_ForwardReg.IsActive()) RefMuon_Track_em_phi_ForwardReg();
    if(RefGamma_em_etx.IsActive()) RefGamma_em_etx();
    if(RefGamma_em_ety.IsActive()) RefGamma_em_ety();
    if(RefGamma_em_phi.IsActive()) RefGamma_em_phi();
    if(RefGamma_em_et.IsActive()) RefGamma_em_et();
    if(RefGamma_em_sumet.IsActive()) RefGamma_em_sumet();
    if(RefGamma_em_etx_CentralReg.IsActive()) RefGamma_em_etx_CentralReg();
    if(RefGamma_em_ety_CentralReg.IsActive()) RefGamma_em_ety_CentralReg();
    if(RefGamma_em_sumet_CentralReg.IsActive()) RefGamma_em_sumet_CentralReg();
    if(RefGamma_em_phi_CentralReg.IsActive()) RefGamma_em_phi_CentralReg();
    if(RefGamma_em_etx_EndcapRegion.IsActive()) RefGamma_em_etx_EndcapRegion();
    if(RefGamma_em_ety_EndcapRegion.IsActive()) RefGamma_em_ety_EndcapRegion();
    if(RefGamma_em_sumet_EndcapRegion.IsActive()) RefGamma_em_sumet_EndcapRegion();
    if(RefGamma_em_phi_EndcapRegion.IsActive()) RefGamma_em_phi_EndcapRegion();
    if(RefGamma_em_etx_ForwardReg.IsActive()) RefGamma_em_etx_ForwardReg();
    if(RefGamma_em_ety_ForwardReg.IsActive()) RefGamma_em_ety_ForwardReg();
    if(RefGamma_em_sumet_ForwardReg.IsActive()) RefGamma_em_sumet_ForwardReg();
    if(RefGamma_em_phi_ForwardReg.IsActive()) RefGamma_em_phi_ForwardReg();
    if(RefTau_em_etx.IsActive()) RefTau_em_etx();
    if(RefTau_em_ety.IsActive()) RefTau_em_ety();
    if(RefTau_em_phi.IsActive()) RefTau_em_phi();
    if(RefTau_em_et.IsActive()) RefTau_em_et();
    if(RefTau_em_sumet.IsActive()) RefTau_em_sumet();
    if(RefTau_em_etx_CentralReg.IsActive()) RefTau_em_etx_CentralReg();
    if(RefTau_em_ety_CentralReg.IsActive()) RefTau_em_ety_CentralReg();
    if(RefTau_em_sumet_CentralReg.IsActive()) RefTau_em_sumet_CentralReg();
    if(RefTau_em_phi_CentralReg.IsActive()) RefTau_em_phi_CentralReg();
    if(RefTau_em_etx_EndcapRegion.IsActive()) RefTau_em_etx_EndcapRegion();
    if(RefTau_em_ety_EndcapRegion.IsActive()) RefTau_em_ety_EndcapRegion();
    if(RefTau_em_sumet_EndcapRegion.IsActive()) RefTau_em_sumet_EndcapRegion();
    if(RefTau_em_phi_EndcapRegion.IsActive()) RefTau_em_phi_EndcapRegion();
    if(RefTau_em_etx_ForwardReg.IsActive()) RefTau_em_etx_ForwardReg();
    if(RefTau_em_ety_ForwardReg.IsActive()) RefTau_em_ety_ForwardReg();
    if(RefTau_em_sumet_ForwardReg.IsActive()) RefTau_em_sumet_ForwardReg();
    if(RefTau_em_phi_ForwardReg.IsActive()) RefTau_em_phi_ForwardReg();
    if(RefJet_JVF_etx.IsActive()) RefJet_JVF_etx();
    if(RefJet_JVF_ety.IsActive()) RefJet_JVF_ety();
    if(RefJet_JVF_phi.IsActive()) RefJet_JVF_phi();
    if(RefJet_JVF_et.IsActive()) RefJet_JVF_et();
    if(RefJet_JVF_sumet.IsActive()) RefJet_JVF_sumet();
    if(RefJet_JVF_etx_CentralReg.IsActive()) RefJet_JVF_etx_CentralReg();
    if(RefJet_JVF_ety_CentralReg.IsActive()) RefJet_JVF_ety_CentralReg();
    if(RefJet_JVF_sumet_CentralReg.IsActive()) RefJet_JVF_sumet_CentralReg();
    if(RefJet_JVF_phi_CentralReg.IsActive()) RefJet_JVF_phi_CentralReg();
    if(RefJet_JVF_etx_EndcapRegion.IsActive()) RefJet_JVF_etx_EndcapRegion();
    if(RefJet_JVF_ety_EndcapRegion.IsActive()) RefJet_JVF_ety_EndcapRegion();
    if(RefJet_JVF_sumet_EndcapRegion.IsActive()) RefJet_JVF_sumet_EndcapRegion();
    if(RefJet_JVF_phi_EndcapRegion.IsActive()) RefJet_JVF_phi_EndcapRegion();
    if(RefJet_JVF_etx_ForwardReg.IsActive()) RefJet_JVF_etx_ForwardReg();
    if(RefJet_JVF_ety_ForwardReg.IsActive()) RefJet_JVF_ety_ForwardReg();
    if(RefJet_JVF_sumet_ForwardReg.IsActive()) RefJet_JVF_sumet_ForwardReg();
    if(RefJet_JVF_phi_ForwardReg.IsActive()) RefJet_JVF_phi_ForwardReg();
    if(RefJet_JVFCut_etx.IsActive()) RefJet_JVFCut_etx();
    if(RefJet_JVFCut_ety.IsActive()) RefJet_JVFCut_ety();
    if(RefJet_JVFCut_phi.IsActive()) RefJet_JVFCut_phi();
    if(RefJet_JVFCut_et.IsActive()) RefJet_JVFCut_et();
    if(RefJet_JVFCut_sumet.IsActive()) RefJet_JVFCut_sumet();
    if(RefJet_JVFCut_etx_CentralReg.IsActive()) RefJet_JVFCut_etx_CentralReg();
    if(RefJet_JVFCut_ety_CentralReg.IsActive()) RefJet_JVFCut_ety_CentralReg();
    if(RefJet_JVFCut_sumet_CentralReg.IsActive()) RefJet_JVFCut_sumet_CentralReg();
    if(RefJet_JVFCut_phi_CentralReg.IsActive()) RefJet_JVFCut_phi_CentralReg();
    if(RefJet_JVFCut_etx_EndcapRegion.IsActive()) RefJet_JVFCut_etx_EndcapRegion();
    if(RefJet_JVFCut_ety_EndcapRegion.IsActive()) RefJet_JVFCut_ety_EndcapRegion();
    if(RefJet_JVFCut_sumet_EndcapRegion.IsActive()) RefJet_JVFCut_sumet_EndcapRegion();
    if(RefJet_JVFCut_phi_EndcapRegion.IsActive()) RefJet_JVFCut_phi_EndcapRegion();
    if(RefJet_JVFCut_etx_ForwardReg.IsActive()) RefJet_JVFCut_etx_ForwardReg();
    if(RefJet_JVFCut_ety_ForwardReg.IsActive()) RefJet_JVFCut_ety_ForwardReg();
    if(RefJet_JVFCut_sumet_ForwardReg.IsActive()) RefJet_JVFCut_sumet_ForwardReg();
    if(RefJet_JVFCut_phi_ForwardReg.IsActive()) RefJet_JVFCut_phi_ForwardReg();
    if(CellOut_Eflow_STVF_etx.IsActive()) CellOut_Eflow_STVF_etx();
    if(CellOut_Eflow_STVF_ety.IsActive()) CellOut_Eflow_STVF_ety();
    if(CellOut_Eflow_STVF_phi.IsActive()) CellOut_Eflow_STVF_phi();
    if(CellOut_Eflow_STVF_et.IsActive()) CellOut_Eflow_STVF_et();
    if(CellOut_Eflow_STVF_sumet.IsActive()) CellOut_Eflow_STVF_sumet();
    if(CellOut_Eflow_STVF_etx_CentralReg.IsActive()) CellOut_Eflow_STVF_etx_CentralReg();
    if(CellOut_Eflow_STVF_ety_CentralReg.IsActive()) CellOut_Eflow_STVF_ety_CentralReg();
    if(CellOut_Eflow_STVF_sumet_CentralReg.IsActive()) CellOut_Eflow_STVF_sumet_CentralReg();
    if(CellOut_Eflow_STVF_phi_CentralReg.IsActive()) CellOut_Eflow_STVF_phi_CentralReg();
    if(CellOut_Eflow_STVF_etx_EndcapRegion.IsActive()) CellOut_Eflow_STVF_etx_EndcapRegion();
    if(CellOut_Eflow_STVF_ety_EndcapRegion.IsActive()) CellOut_Eflow_STVF_ety_EndcapRegion();
    if(CellOut_Eflow_STVF_sumet_EndcapRegion.IsActive()) CellOut_Eflow_STVF_sumet_EndcapRegion();
    if(CellOut_Eflow_STVF_phi_EndcapRegion.IsActive()) CellOut_Eflow_STVF_phi_EndcapRegion();
    if(CellOut_Eflow_STVF_etx_ForwardReg.IsActive()) CellOut_Eflow_STVF_etx_ForwardReg();
    if(CellOut_Eflow_STVF_ety_ForwardReg.IsActive()) CellOut_Eflow_STVF_ety_ForwardReg();
    if(CellOut_Eflow_STVF_sumet_ForwardReg.IsActive()) CellOut_Eflow_STVF_sumet_ForwardReg();
    if(CellOut_Eflow_STVF_phi_ForwardReg.IsActive()) CellOut_Eflow_STVF_phi_ForwardReg();
    if(CellOut_Eflow_JetArea_etx.IsActive()) CellOut_Eflow_JetArea_etx();
    if(CellOut_Eflow_JetArea_ety.IsActive()) CellOut_Eflow_JetArea_ety();
    if(CellOut_Eflow_JetArea_phi.IsActive()) CellOut_Eflow_JetArea_phi();
    if(CellOut_Eflow_JetArea_et.IsActive()) CellOut_Eflow_JetArea_et();
    if(CellOut_Eflow_JetArea_sumet.IsActive()) CellOut_Eflow_JetArea_sumet();
    if(CellOut_Eflow_JetArea_etx_CentralReg.IsActive()) CellOut_Eflow_JetArea_etx_CentralReg();
    if(CellOut_Eflow_JetArea_ety_CentralReg.IsActive()) CellOut_Eflow_JetArea_ety_CentralReg();
    if(CellOut_Eflow_JetArea_sumet_CentralReg.IsActive()) CellOut_Eflow_JetArea_sumet_CentralReg();
    if(CellOut_Eflow_JetArea_phi_CentralReg.IsActive()) CellOut_Eflow_JetArea_phi_CentralReg();
    if(CellOut_Eflow_JetArea_etx_EndcapRegion.IsActive()) CellOut_Eflow_JetArea_etx_EndcapRegion();
    if(CellOut_Eflow_JetArea_ety_EndcapRegion.IsActive()) CellOut_Eflow_JetArea_ety_EndcapRegion();
    if(CellOut_Eflow_JetArea_sumet_EndcapRegion.IsActive()) CellOut_Eflow_JetArea_sumet_EndcapRegion();
    if(CellOut_Eflow_JetArea_phi_EndcapRegion.IsActive()) CellOut_Eflow_JetArea_phi_EndcapRegion();
    if(CellOut_Eflow_JetArea_etx_ForwardReg.IsActive()) CellOut_Eflow_JetArea_etx_ForwardReg();
    if(CellOut_Eflow_JetArea_ety_ForwardReg.IsActive()) CellOut_Eflow_JetArea_ety_ForwardReg();
    if(CellOut_Eflow_JetArea_sumet_ForwardReg.IsActive()) CellOut_Eflow_JetArea_sumet_ForwardReg();
    if(CellOut_Eflow_JetArea_phi_ForwardReg.IsActive()) CellOut_Eflow_JetArea_phi_ForwardReg();
    if(CellOut_Eflow_JetAreaJVF_etx.IsActive()) CellOut_Eflow_JetAreaJVF_etx();
    if(CellOut_Eflow_JetAreaJVF_ety.IsActive()) CellOut_Eflow_JetAreaJVF_ety();
    if(CellOut_Eflow_JetAreaJVF_phi.IsActive()) CellOut_Eflow_JetAreaJVF_phi();
    if(CellOut_Eflow_JetAreaJVF_et.IsActive()) CellOut_Eflow_JetAreaJVF_et();
    if(CellOut_Eflow_JetAreaJVF_sumet.IsActive()) CellOut_Eflow_JetAreaJVF_sumet();
    if(CellOut_Eflow_JetAreaJVF_etx_CentralReg.IsActive()) CellOut_Eflow_JetAreaJVF_etx_CentralReg();
    if(CellOut_Eflow_JetAreaJVF_ety_CentralReg.IsActive()) CellOut_Eflow_JetAreaJVF_ety_CentralReg();
    if(CellOut_Eflow_JetAreaJVF_sumet_CentralReg.IsActive()) CellOut_Eflow_JetAreaJVF_sumet_CentralReg();
    if(CellOut_Eflow_JetAreaJVF_phi_CentralReg.IsActive()) CellOut_Eflow_JetAreaJVF_phi_CentralReg();
    if(CellOut_Eflow_JetAreaJVF_etx_EndcapRegion.IsActive()) CellOut_Eflow_JetAreaJVF_etx_EndcapRegion();
    if(CellOut_Eflow_JetAreaJVF_ety_EndcapRegion.IsActive()) CellOut_Eflow_JetAreaJVF_ety_EndcapRegion();
    if(CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion.IsActive()) CellOut_Eflow_JetAreaJVF_sumet_EndcapRegion();
    if(CellOut_Eflow_JetAreaJVF_phi_EndcapRegion.IsActive()) CellOut_Eflow_JetAreaJVF_phi_EndcapRegion();
    if(CellOut_Eflow_JetAreaJVF_etx_ForwardReg.IsActive()) CellOut_Eflow_JetAreaJVF_etx_ForwardReg();
    if(CellOut_Eflow_JetAreaJVF_ety_ForwardReg.IsActive()) CellOut_Eflow_JetAreaJVF_ety_ForwardReg();
    if(CellOut_Eflow_JetAreaJVF_sumet_ForwardReg.IsActive()) CellOut_Eflow_JetAreaJVF_sumet_ForwardReg();
    if(CellOut_Eflow_JetAreaJVF_phi_ForwardReg.IsActive()) CellOut_Eflow_JetAreaJVF_phi_ForwardReg();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_etx.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_etx();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_ety.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_ety();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_phi.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_phi();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_et.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_et();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_sumet.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_sumet();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_etx_CentralReg();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_ety_CentralReg();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_sumet_CentralReg();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_phi_CentralReg();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_etx_EndcapRegion();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_ety_EndcapRegion();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_sumet_EndcapRegion();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_phi_EndcapRegion();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_etx_ForwardReg();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_ety_ForwardReg();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_sumet_ForwardReg();
    if(CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg.IsActive()) CellOut_Eflow_JetAreaRhoEta5JVF_phi_ForwardReg();
    if(RefFinal_STVF_etx.IsActive()) RefFinal_STVF_etx();
    if(RefFinal_STVF_ety.IsActive()) RefFinal_STVF_ety();
    if(RefFinal_STVF_phi.IsActive()) RefFinal_STVF_phi();
    if(RefFinal_STVF_et.IsActive()) RefFinal_STVF_et();
    if(RefFinal_STVF_sumet.IsActive()) RefFinal_STVF_sumet();
    if(RefFinal_STVF_etx_CentralReg.IsActive()) RefFinal_STVF_etx_CentralReg();
    if(RefFinal_STVF_ety_CentralReg.IsActive()) RefFinal_STVF_ety_CentralReg();
    if(RefFinal_STVF_sumet_CentralReg.IsActive()) RefFinal_STVF_sumet_CentralReg();
    if(RefFinal_STVF_phi_CentralReg.IsActive()) RefFinal_STVF_phi_CentralReg();
    if(RefFinal_STVF_etx_EndcapRegion.IsActive()) RefFinal_STVF_etx_EndcapRegion();
    if(RefFinal_STVF_ety_EndcapRegion.IsActive()) RefFinal_STVF_ety_EndcapRegion();
    if(RefFinal_STVF_sumet_EndcapRegion.IsActive()) RefFinal_STVF_sumet_EndcapRegion();
    if(RefFinal_STVF_phi_EndcapRegion.IsActive()) RefFinal_STVF_phi_EndcapRegion();
    if(RefFinal_STVF_etx_ForwardReg.IsActive()) RefFinal_STVF_etx_ForwardReg();
    if(RefFinal_STVF_ety_ForwardReg.IsActive()) RefFinal_STVF_ety_ForwardReg();
    if(RefFinal_STVF_sumet_ForwardReg.IsActive()) RefFinal_STVF_sumet_ForwardReg();
    if(RefFinal_STVF_phi_ForwardReg.IsActive()) RefFinal_STVF_phi_ForwardReg();
    if(CellOut_Eflow_etx.IsActive()) CellOut_Eflow_etx();
    if(CellOut_Eflow_ety.IsActive()) CellOut_Eflow_ety();
    if(CellOut_Eflow_phi.IsActive()) CellOut_Eflow_phi();
    if(CellOut_Eflow_et.IsActive()) CellOut_Eflow_et();
    if(CellOut_Eflow_sumet.IsActive()) CellOut_Eflow_sumet();
    if(CellOut_Eflow_etx_CentralReg.IsActive()) CellOut_Eflow_etx_CentralReg();
    if(CellOut_Eflow_ety_CentralReg.IsActive()) CellOut_Eflow_ety_CentralReg();
    if(CellOut_Eflow_sumet_CentralReg.IsActive()) CellOut_Eflow_sumet_CentralReg();
    if(CellOut_Eflow_phi_CentralReg.IsActive()) CellOut_Eflow_phi_CentralReg();
    if(CellOut_Eflow_etx_EndcapRegion.IsActive()) CellOut_Eflow_etx_EndcapRegion();
    if(CellOut_Eflow_ety_EndcapRegion.IsActive()) CellOut_Eflow_ety_EndcapRegion();
    if(CellOut_Eflow_sumet_EndcapRegion.IsActive()) CellOut_Eflow_sumet_EndcapRegion();
    if(CellOut_Eflow_phi_EndcapRegion.IsActive()) CellOut_Eflow_phi_EndcapRegion();
    if(CellOut_Eflow_etx_ForwardReg.IsActive()) CellOut_Eflow_etx_ForwardReg();
    if(CellOut_Eflow_ety_ForwardReg.IsActive()) CellOut_Eflow_ety_ForwardReg();
    if(CellOut_Eflow_sumet_ForwardReg.IsActive()) CellOut_Eflow_sumet_ForwardReg();
    if(CellOut_Eflow_phi_ForwardReg.IsActive()) CellOut_Eflow_phi_ForwardReg();
    if(CellOut_Eflow_Muid_etx.IsActive()) CellOut_Eflow_Muid_etx();
    if(CellOut_Eflow_Muid_ety.IsActive()) CellOut_Eflow_Muid_ety();
    if(CellOut_Eflow_Muid_phi.IsActive()) CellOut_Eflow_Muid_phi();
    if(CellOut_Eflow_Muid_et.IsActive()) CellOut_Eflow_Muid_et();
    if(CellOut_Eflow_Muid_sumet.IsActive()) CellOut_Eflow_Muid_sumet();
    if(CellOut_Eflow_Muid_etx_CentralReg.IsActive()) CellOut_Eflow_Muid_etx_CentralReg();
    if(CellOut_Eflow_Muid_ety_CentralReg.IsActive()) CellOut_Eflow_Muid_ety_CentralReg();
    if(CellOut_Eflow_Muid_sumet_CentralReg.IsActive()) CellOut_Eflow_Muid_sumet_CentralReg();
    if(CellOut_Eflow_Muid_phi_CentralReg.IsActive()) CellOut_Eflow_Muid_phi_CentralReg();
    if(CellOut_Eflow_Muid_etx_EndcapRegion.IsActive()) CellOut_Eflow_Muid_etx_EndcapRegion();
    if(CellOut_Eflow_Muid_ety_EndcapRegion.IsActive()) CellOut_Eflow_Muid_ety_EndcapRegion();
    if(CellOut_Eflow_Muid_sumet_EndcapRegion.IsActive()) CellOut_Eflow_Muid_sumet_EndcapRegion();
    if(CellOut_Eflow_Muid_phi_EndcapRegion.IsActive()) CellOut_Eflow_Muid_phi_EndcapRegion();
    if(CellOut_Eflow_Muid_etx_ForwardReg.IsActive()) CellOut_Eflow_Muid_etx_ForwardReg();
    if(CellOut_Eflow_Muid_ety_ForwardReg.IsActive()) CellOut_Eflow_Muid_ety_ForwardReg();
    if(CellOut_Eflow_Muid_sumet_ForwardReg.IsActive()) CellOut_Eflow_Muid_sumet_ForwardReg();
    if(CellOut_Eflow_Muid_phi_ForwardReg.IsActive()) CellOut_Eflow_Muid_phi_ForwardReg();
    if(CellOut_Eflow_Muons_etx.IsActive()) CellOut_Eflow_Muons_etx();
    if(CellOut_Eflow_Muons_ety.IsActive()) CellOut_Eflow_Muons_ety();
    if(CellOut_Eflow_Muons_phi.IsActive()) CellOut_Eflow_Muons_phi();
    if(CellOut_Eflow_Muons_et.IsActive()) CellOut_Eflow_Muons_et();
    if(CellOut_Eflow_Muons_sumet.IsActive()) CellOut_Eflow_Muons_sumet();
    if(CellOut_Eflow_Muons_etx_CentralReg.IsActive()) CellOut_Eflow_Muons_etx_CentralReg();
    if(CellOut_Eflow_Muons_ety_CentralReg.IsActive()) CellOut_Eflow_Muons_ety_CentralReg();
    if(CellOut_Eflow_Muons_sumet_CentralReg.IsActive()) CellOut_Eflow_Muons_sumet_CentralReg();
    if(CellOut_Eflow_Muons_phi_CentralReg.IsActive()) CellOut_Eflow_Muons_phi_CentralReg();
    if(CellOut_Eflow_Muons_etx_EndcapRegion.IsActive()) CellOut_Eflow_Muons_etx_EndcapRegion();
    if(CellOut_Eflow_Muons_ety_EndcapRegion.IsActive()) CellOut_Eflow_Muons_ety_EndcapRegion();
    if(CellOut_Eflow_Muons_sumet_EndcapRegion.IsActive()) CellOut_Eflow_Muons_sumet_EndcapRegion();
    if(CellOut_Eflow_Muons_phi_EndcapRegion.IsActive()) CellOut_Eflow_Muons_phi_EndcapRegion();
    if(CellOut_Eflow_Muons_etx_ForwardReg.IsActive()) CellOut_Eflow_Muons_etx_ForwardReg();
    if(CellOut_Eflow_Muons_ety_ForwardReg.IsActive()) CellOut_Eflow_Muons_ety_ForwardReg();
    if(CellOut_Eflow_Muons_sumet_ForwardReg.IsActive()) CellOut_Eflow_Muons_sumet_ForwardReg();
    if(CellOut_Eflow_Muons_phi_ForwardReg.IsActive()) CellOut_Eflow_Muons_phi_ForwardReg();
    if(RefMuon_Track_etx.IsActive()) RefMuon_Track_etx();
    if(RefMuon_Track_ety.IsActive()) RefMuon_Track_ety();
    if(RefMuon_Track_phi.IsActive()) RefMuon_Track_phi();
    if(RefMuon_Track_et.IsActive()) RefMuon_Track_et();
    if(RefMuon_Track_sumet.IsActive()) RefMuon_Track_sumet();
    if(RefMuon_Track_etx_CentralReg.IsActive()) RefMuon_Track_etx_CentralReg();
    if(RefMuon_Track_ety_CentralReg.IsActive()) RefMuon_Track_ety_CentralReg();
    if(RefMuon_Track_sumet_CentralReg.IsActive()) RefMuon_Track_sumet_CentralReg();
    if(RefMuon_Track_phi_CentralReg.IsActive()) RefMuon_Track_phi_CentralReg();
    if(RefMuon_Track_etx_EndcapRegion.IsActive()) RefMuon_Track_etx_EndcapRegion();
    if(RefMuon_Track_ety_EndcapRegion.IsActive()) RefMuon_Track_ety_EndcapRegion();
    if(RefMuon_Track_sumet_EndcapRegion.IsActive()) RefMuon_Track_sumet_EndcapRegion();
    if(RefMuon_Track_phi_EndcapRegion.IsActive()) RefMuon_Track_phi_EndcapRegion();
    if(RefMuon_Track_etx_ForwardReg.IsActive()) RefMuon_Track_etx_ForwardReg();
    if(RefMuon_Track_ety_ForwardReg.IsActive()) RefMuon_Track_ety_ForwardReg();
    if(RefMuon_Track_sumet_ForwardReg.IsActive()) RefMuon_Track_sumet_ForwardReg();
    if(RefMuon_Track_phi_ForwardReg.IsActive()) RefMuon_Track_phi_ForwardReg();
    if(RefMuon_Track_Staco_etx.IsActive()) RefMuon_Track_Staco_etx();
    if(RefMuon_Track_Staco_ety.IsActive()) RefMuon_Track_Staco_ety();
    if(RefMuon_Track_Staco_phi.IsActive()) RefMuon_Track_Staco_phi();
    if(RefMuon_Track_Staco_et.IsActive()) RefMuon_Track_Staco_et();
    if(RefMuon_Track_Staco_sumet.IsActive()) RefMuon_Track_Staco_sumet();
    if(RefMuon_Track_Staco_etx_CentralReg.IsActive()) RefMuon_Track_Staco_etx_CentralReg();
    if(RefMuon_Track_Staco_ety_CentralReg.IsActive()) RefMuon_Track_Staco_ety_CentralReg();
    if(RefMuon_Track_Staco_sumet_CentralReg.IsActive()) RefMuon_Track_Staco_sumet_CentralReg();
    if(RefMuon_Track_Staco_phi_CentralReg.IsActive()) RefMuon_Track_Staco_phi_CentralReg();
    if(RefMuon_Track_Staco_etx_EndcapRegion.IsActive()) RefMuon_Track_Staco_etx_EndcapRegion();
    if(RefMuon_Track_Staco_ety_EndcapRegion.IsActive()) RefMuon_Track_Staco_ety_EndcapRegion();
    if(RefMuon_Track_Staco_sumet_EndcapRegion.IsActive()) RefMuon_Track_Staco_sumet_EndcapRegion();
    if(RefMuon_Track_Staco_phi_EndcapRegion.IsActive()) RefMuon_Track_Staco_phi_EndcapRegion();
    if(RefMuon_Track_Staco_etx_ForwardReg.IsActive()) RefMuon_Track_Staco_etx_ForwardReg();
    if(RefMuon_Track_Staco_ety_ForwardReg.IsActive()) RefMuon_Track_Staco_ety_ForwardReg();
    if(RefMuon_Track_Staco_sumet_ForwardReg.IsActive()) RefMuon_Track_Staco_sumet_ForwardReg();
    if(RefMuon_Track_Staco_phi_ForwardReg.IsActive()) RefMuon_Track_Staco_phi_ForwardReg();
    if(RefMuon_Track_Muid_etx.IsActive()) RefMuon_Track_Muid_etx();
    if(RefMuon_Track_Muid_ety.IsActive()) RefMuon_Track_Muid_ety();
    if(RefMuon_Track_Muid_phi.IsActive()) RefMuon_Track_Muid_phi();
    if(RefMuon_Track_Muid_et.IsActive()) RefMuon_Track_Muid_et();
    if(RefMuon_Track_Muid_sumet.IsActive()) RefMuon_Track_Muid_sumet();
    if(RefMuon_Track_Muid_etx_CentralReg.IsActive()) RefMuon_Track_Muid_etx_CentralReg();
    if(RefMuon_Track_Muid_ety_CentralReg.IsActive()) RefMuon_Track_Muid_ety_CentralReg();
    if(RefMuon_Track_Muid_sumet_CentralReg.IsActive()) RefMuon_Track_Muid_sumet_CentralReg();
    if(RefMuon_Track_Muid_phi_CentralReg.IsActive()) RefMuon_Track_Muid_phi_CentralReg();
    if(RefMuon_Track_Muid_etx_EndcapRegion.IsActive()) RefMuon_Track_Muid_etx_EndcapRegion();
    if(RefMuon_Track_Muid_ety_EndcapRegion.IsActive()) RefMuon_Track_Muid_ety_EndcapRegion();
    if(RefMuon_Track_Muid_sumet_EndcapRegion.IsActive()) RefMuon_Track_Muid_sumet_EndcapRegion();
    if(RefMuon_Track_Muid_phi_EndcapRegion.IsActive()) RefMuon_Track_Muid_phi_EndcapRegion();
    if(RefMuon_Track_Muid_etx_ForwardReg.IsActive()) RefMuon_Track_Muid_etx_ForwardReg();
    if(RefMuon_Track_Muid_ety_ForwardReg.IsActive()) RefMuon_Track_Muid_ety_ForwardReg();
    if(RefMuon_Track_Muid_sumet_ForwardReg.IsActive()) RefMuon_Track_Muid_sumet_ForwardReg();
    if(RefMuon_Track_Muid_phi_ForwardReg.IsActive()) RefMuon_Track_Muid_phi_ForwardReg();
    if(RefMuons_Track_etx.IsActive()) RefMuons_Track_etx();
    if(RefMuons_Track_ety.IsActive()) RefMuons_Track_ety();
    if(RefMuons_Track_phi.IsActive()) RefMuons_Track_phi();
    if(RefMuons_Track_et.IsActive()) RefMuons_Track_et();
    if(RefMuons_Track_sumet.IsActive()) RefMuons_Track_sumet();
    if(RefMuons_Track_etx_CentralReg.IsActive()) RefMuons_Track_etx_CentralReg();
    if(RefMuons_Track_ety_CentralReg.IsActive()) RefMuons_Track_ety_CentralReg();
    if(RefMuons_Track_sumet_CentralReg.IsActive()) RefMuons_Track_sumet_CentralReg();
    if(RefMuons_Track_phi_CentralReg.IsActive()) RefMuons_Track_phi_CentralReg();
    if(RefMuons_Track_etx_EndcapRegion.IsActive()) RefMuons_Track_etx_EndcapRegion();
    if(RefMuons_Track_ety_EndcapRegion.IsActive()) RefMuons_Track_ety_EndcapRegion();
    if(RefMuons_Track_sumet_EndcapRegion.IsActive()) RefMuons_Track_sumet_EndcapRegion();
    if(RefMuons_Track_phi_EndcapRegion.IsActive()) RefMuons_Track_phi_EndcapRegion();
    if(RefMuons_Track_etx_ForwardReg.IsActive()) RefMuons_Track_etx_ForwardReg();
    if(RefMuons_Track_ety_ForwardReg.IsActive()) RefMuons_Track_ety_ForwardReg();
    if(RefMuons_Track_sumet_ForwardReg.IsActive()) RefMuons_Track_sumet_ForwardReg();
    if(RefMuons_Track_phi_ForwardReg.IsActive()) RefMuons_Track_phi_ForwardReg();
    if(RefFinal_BDTMedium_etx.IsActive()) RefFinal_BDTMedium_etx();
    if(RefFinal_BDTMedium_ety.IsActive()) RefFinal_BDTMedium_ety();
    if(RefFinal_BDTMedium_phi.IsActive()) RefFinal_BDTMedium_phi();
    if(RefFinal_BDTMedium_et.IsActive()) RefFinal_BDTMedium_et();
    if(RefFinal_BDTMedium_sumet.IsActive()) RefFinal_BDTMedium_sumet();
    if(RefFinal_BDTMedium_etx_CentralReg.IsActive()) RefFinal_BDTMedium_etx_CentralReg();
    if(RefFinal_BDTMedium_ety_CentralReg.IsActive()) RefFinal_BDTMedium_ety_CentralReg();
    if(RefFinal_BDTMedium_sumet_CentralReg.IsActive()) RefFinal_BDTMedium_sumet_CentralReg();
    if(RefFinal_BDTMedium_phi_CentralReg.IsActive()) RefFinal_BDTMedium_phi_CentralReg();
    if(RefFinal_BDTMedium_etx_EndcapRegion.IsActive()) RefFinal_BDTMedium_etx_EndcapRegion();
    if(RefFinal_BDTMedium_ety_EndcapRegion.IsActive()) RefFinal_BDTMedium_ety_EndcapRegion();
    if(RefFinal_BDTMedium_sumet_EndcapRegion.IsActive()) RefFinal_BDTMedium_sumet_EndcapRegion();
    if(RefFinal_BDTMedium_phi_EndcapRegion.IsActive()) RefFinal_BDTMedium_phi_EndcapRegion();
    if(RefFinal_BDTMedium_etx_ForwardReg.IsActive()) RefFinal_BDTMedium_etx_ForwardReg();
    if(RefFinal_BDTMedium_ety_ForwardReg.IsActive()) RefFinal_BDTMedium_ety_ForwardReg();
    if(RefFinal_BDTMedium_sumet_ForwardReg.IsActive()) RefFinal_BDTMedium_sumet_ForwardReg();
    if(RefFinal_BDTMedium_phi_ForwardReg.IsActive()) RefFinal_BDTMedium_phi_ForwardReg();
    if(RefGamma_BDTMedium_etx.IsActive()) RefGamma_BDTMedium_etx();
    if(RefGamma_BDTMedium_ety.IsActive()) RefGamma_BDTMedium_ety();
    if(RefGamma_BDTMedium_phi.IsActive()) RefGamma_BDTMedium_phi();
    if(RefGamma_BDTMedium_et.IsActive()) RefGamma_BDTMedium_et();
    if(RefGamma_BDTMedium_sumet.IsActive()) RefGamma_BDTMedium_sumet();
    if(RefGamma_BDTMedium_etx_CentralReg.IsActive()) RefGamma_BDTMedium_etx_CentralReg();
    if(RefGamma_BDTMedium_ety_CentralReg.IsActive()) RefGamma_BDTMedium_ety_CentralReg();
    if(RefGamma_BDTMedium_sumet_CentralReg.IsActive()) RefGamma_BDTMedium_sumet_CentralReg();
    if(RefGamma_BDTMedium_phi_CentralReg.IsActive()) RefGamma_BDTMedium_phi_CentralReg();
    if(RefGamma_BDTMedium_etx_EndcapRegion.IsActive()) RefGamma_BDTMedium_etx_EndcapRegion();
    if(RefGamma_BDTMedium_ety_EndcapRegion.IsActive()) RefGamma_BDTMedium_ety_EndcapRegion();
    if(RefGamma_BDTMedium_sumet_EndcapRegion.IsActive()) RefGamma_BDTMedium_sumet_EndcapRegion();
    if(RefGamma_BDTMedium_phi_EndcapRegion.IsActive()) RefGamma_BDTMedium_phi_EndcapRegion();
    if(RefGamma_BDTMedium_etx_ForwardReg.IsActive()) RefGamma_BDTMedium_etx_ForwardReg();
    if(RefGamma_BDTMedium_ety_ForwardReg.IsActive()) RefGamma_BDTMedium_ety_ForwardReg();
    if(RefGamma_BDTMedium_sumet_ForwardReg.IsActive()) RefGamma_BDTMedium_sumet_ForwardReg();
    if(RefGamma_BDTMedium_phi_ForwardReg.IsActive()) RefGamma_BDTMedium_phi_ForwardReg();
    if(RefEle_BDTMedium_etx.IsActive()) RefEle_BDTMedium_etx();
    if(RefEle_BDTMedium_ety.IsActive()) RefEle_BDTMedium_ety();
    if(RefEle_BDTMedium_phi.IsActive()) RefEle_BDTMedium_phi();
    if(RefEle_BDTMedium_et.IsActive()) RefEle_BDTMedium_et();
    if(RefEle_BDTMedium_sumet.IsActive()) RefEle_BDTMedium_sumet();
    if(RefEle_BDTMedium_etx_CentralReg.IsActive()) RefEle_BDTMedium_etx_CentralReg();
    if(RefEle_BDTMedium_ety_CentralReg.IsActive()) RefEle_BDTMedium_ety_CentralReg();
    if(RefEle_BDTMedium_sumet_CentralReg.IsActive()) RefEle_BDTMedium_sumet_CentralReg();
    if(RefEle_BDTMedium_phi_CentralReg.IsActive()) RefEle_BDTMedium_phi_CentralReg();
    if(RefEle_BDTMedium_etx_EndcapRegion.IsActive()) RefEle_BDTMedium_etx_EndcapRegion();
    if(RefEle_BDTMedium_ety_EndcapRegion.IsActive()) RefEle_BDTMedium_ety_EndcapRegion();
    if(RefEle_BDTMedium_sumet_EndcapRegion.IsActive()) RefEle_BDTMedium_sumet_EndcapRegion();
    if(RefEle_BDTMedium_phi_EndcapRegion.IsActive()) RefEle_BDTMedium_phi_EndcapRegion();
    if(RefEle_BDTMedium_etx_ForwardReg.IsActive()) RefEle_BDTMedium_etx_ForwardReg();
    if(RefEle_BDTMedium_ety_ForwardReg.IsActive()) RefEle_BDTMedium_ety_ForwardReg();
    if(RefEle_BDTMedium_sumet_ForwardReg.IsActive()) RefEle_BDTMedium_sumet_ForwardReg();
    if(RefEle_BDTMedium_phi_ForwardReg.IsActive()) RefEle_BDTMedium_phi_ForwardReg();
    if(RefTau_BDTMedium_etx.IsActive()) RefTau_BDTMedium_etx();
    if(RefTau_BDTMedium_ety.IsActive()) RefTau_BDTMedium_ety();
    if(RefTau_BDTMedium_phi.IsActive()) RefTau_BDTMedium_phi();
    if(RefTau_BDTMedium_et.IsActive()) RefTau_BDTMedium_et();
    if(RefTau_BDTMedium_sumet.IsActive()) RefTau_BDTMedium_sumet();
    if(RefTau_BDTMedium_etx_CentralReg.IsActive()) RefTau_BDTMedium_etx_CentralReg();
    if(RefTau_BDTMedium_ety_CentralReg.IsActive()) RefTau_BDTMedium_ety_CentralReg();
    if(RefTau_BDTMedium_sumet_CentralReg.IsActive()) RefTau_BDTMedium_sumet_CentralReg();
    if(RefTau_BDTMedium_phi_CentralReg.IsActive()) RefTau_BDTMedium_phi_CentralReg();
    if(RefTau_BDTMedium_etx_EndcapRegion.IsActive()) RefTau_BDTMedium_etx_EndcapRegion();
    if(RefTau_BDTMedium_ety_EndcapRegion.IsActive()) RefTau_BDTMedium_ety_EndcapRegion();
    if(RefTau_BDTMedium_sumet_EndcapRegion.IsActive()) RefTau_BDTMedium_sumet_EndcapRegion();
    if(RefTau_BDTMedium_phi_EndcapRegion.IsActive()) RefTau_BDTMedium_phi_EndcapRegion();
    if(RefTau_BDTMedium_etx_ForwardReg.IsActive()) RefTau_BDTMedium_etx_ForwardReg();
    if(RefTau_BDTMedium_ety_ForwardReg.IsActive()) RefTau_BDTMedium_ety_ForwardReg();
    if(RefTau_BDTMedium_sumet_ForwardReg.IsActive()) RefTau_BDTMedium_sumet_ForwardReg();
    if(RefTau_BDTMedium_phi_ForwardReg.IsActive()) RefTau_BDTMedium_phi_ForwardReg();
    if(RefJet_BDTMedium_etx.IsActive()) RefJet_BDTMedium_etx();
    if(RefJet_BDTMedium_ety.IsActive()) RefJet_BDTMedium_ety();
    if(RefJet_BDTMedium_phi.IsActive()) RefJet_BDTMedium_phi();
    if(RefJet_BDTMedium_et.IsActive()) RefJet_BDTMedium_et();
    if(RefJet_BDTMedium_sumet.IsActive()) RefJet_BDTMedium_sumet();
    if(RefJet_BDTMedium_etx_CentralReg.IsActive()) RefJet_BDTMedium_etx_CentralReg();
    if(RefJet_BDTMedium_ety_CentralReg.IsActive()) RefJet_BDTMedium_ety_CentralReg();
    if(RefJet_BDTMedium_sumet_CentralReg.IsActive()) RefJet_BDTMedium_sumet_CentralReg();
    if(RefJet_BDTMedium_phi_CentralReg.IsActive()) RefJet_BDTMedium_phi_CentralReg();
    if(RefJet_BDTMedium_etx_EndcapRegion.IsActive()) RefJet_BDTMedium_etx_EndcapRegion();
    if(RefJet_BDTMedium_ety_EndcapRegion.IsActive()) RefJet_BDTMedium_ety_EndcapRegion();
    if(RefJet_BDTMedium_sumet_EndcapRegion.IsActive()) RefJet_BDTMedium_sumet_EndcapRegion();
    if(RefJet_BDTMedium_phi_EndcapRegion.IsActive()) RefJet_BDTMedium_phi_EndcapRegion();
    if(RefJet_BDTMedium_etx_ForwardReg.IsActive()) RefJet_BDTMedium_etx_ForwardReg();
    if(RefJet_BDTMedium_ety_ForwardReg.IsActive()) RefJet_BDTMedium_ety_ForwardReg();
    if(RefJet_BDTMedium_sumet_ForwardReg.IsActive()) RefJet_BDTMedium_sumet_ForwardReg();
    if(RefJet_BDTMedium_phi_ForwardReg.IsActive()) RefJet_BDTMedium_phi_ForwardReg();
    if(RefMuon_Staco_BDTMedium_etx.IsActive()) RefMuon_Staco_BDTMedium_etx();
    if(RefMuon_Staco_BDTMedium_ety.IsActive()) RefMuon_Staco_BDTMedium_ety();
    if(RefMuon_Staco_BDTMedium_phi.IsActive()) RefMuon_Staco_BDTMedium_phi();
    if(RefMuon_Staco_BDTMedium_et.IsActive()) RefMuon_Staco_BDTMedium_et();
    if(RefMuon_Staco_BDTMedium_sumet.IsActive()) RefMuon_Staco_BDTMedium_sumet();
    if(RefMuon_Staco_BDTMedium_etx_CentralReg.IsActive()) RefMuon_Staco_BDTMedium_etx_CentralReg();
    if(RefMuon_Staco_BDTMedium_ety_CentralReg.IsActive()) RefMuon_Staco_BDTMedium_ety_CentralReg();
    if(RefMuon_Staco_BDTMedium_sumet_CentralReg.IsActive()) RefMuon_Staco_BDTMedium_sumet_CentralReg();
    if(RefMuon_Staco_BDTMedium_phi_CentralReg.IsActive()) RefMuon_Staco_BDTMedium_phi_CentralReg();
    if(RefMuon_Staco_BDTMedium_etx_EndcapRegion.IsActive()) RefMuon_Staco_BDTMedium_etx_EndcapRegion();
    if(RefMuon_Staco_BDTMedium_ety_EndcapRegion.IsActive()) RefMuon_Staco_BDTMedium_ety_EndcapRegion();
    if(RefMuon_Staco_BDTMedium_sumet_EndcapRegion.IsActive()) RefMuon_Staco_BDTMedium_sumet_EndcapRegion();
    if(RefMuon_Staco_BDTMedium_phi_EndcapRegion.IsActive()) RefMuon_Staco_BDTMedium_phi_EndcapRegion();
    if(RefMuon_Staco_BDTMedium_etx_ForwardReg.IsActive()) RefMuon_Staco_BDTMedium_etx_ForwardReg();
    if(RefMuon_Staco_BDTMedium_ety_ForwardReg.IsActive()) RefMuon_Staco_BDTMedium_ety_ForwardReg();
    if(RefMuon_Staco_BDTMedium_sumet_ForwardReg.IsActive()) RefMuon_Staco_BDTMedium_sumet_ForwardReg();
    if(RefMuon_Staco_BDTMedium_phi_ForwardReg.IsActive()) RefMuon_Staco_BDTMedium_phi_ForwardReg();
    if(RefFinal_AntiKt4LCTopo_tightpp_etx.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_etx();
    if(RefFinal_AntiKt4LCTopo_tightpp_ety.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_ety();
    if(RefFinal_AntiKt4LCTopo_tightpp_phi.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_phi();
    if(RefFinal_AntiKt4LCTopo_tightpp_et.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_et();
    if(RefFinal_AntiKt4LCTopo_tightpp_sumet.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_sumet();
    if(RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_etx_CentralReg();
    if(RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_ety_CentralReg();
    if(RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_sumet_CentralReg();
    if(RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_phi_CentralReg();
    if(RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_etx_EndcapRegion();
    if(RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_ety_EndcapRegion();
    if(RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_sumet_EndcapRegion();
    if(RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_phi_EndcapRegion();
    if(RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_etx_ForwardReg();
    if(RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_ety_ForwardReg();
    if(RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_sumet_ForwardReg();
    if(RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsActive()) RefFinal_AntiKt4LCTopo_tightpp_phi_ForwardReg();
    if(RefGamma_AntiKt4LCTopo_tightpp_etx.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_etx();
    if(RefGamma_AntiKt4LCTopo_tightpp_ety.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_ety();
    if(RefGamma_AntiKt4LCTopo_tightpp_phi.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_phi();
    if(RefGamma_AntiKt4LCTopo_tightpp_et.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_et();
    if(RefGamma_AntiKt4LCTopo_tightpp_sumet.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_sumet();
    if(RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_etx_CentralReg();
    if(RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_ety_CentralReg();
    if(RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_sumet_CentralReg();
    if(RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_phi_CentralReg();
    if(RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_etx_EndcapRegion();
    if(RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_ety_EndcapRegion();
    if(RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_sumet_EndcapRegion();
    if(RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_phi_EndcapRegion();
    if(RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_etx_ForwardReg();
    if(RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_ety_ForwardReg();
    if(RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_sumet_ForwardReg();
    if(RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsActive()) RefGamma_AntiKt4LCTopo_tightpp_phi_ForwardReg();
    if(RefEle_AntiKt4LCTopo_tightpp_etx.IsActive()) RefEle_AntiKt4LCTopo_tightpp_etx();
    if(RefEle_AntiKt4LCTopo_tightpp_ety.IsActive()) RefEle_AntiKt4LCTopo_tightpp_ety();
    if(RefEle_AntiKt4LCTopo_tightpp_phi.IsActive()) RefEle_AntiKt4LCTopo_tightpp_phi();
    if(RefEle_AntiKt4LCTopo_tightpp_et.IsActive()) RefEle_AntiKt4LCTopo_tightpp_et();
    if(RefEle_AntiKt4LCTopo_tightpp_sumet.IsActive()) RefEle_AntiKt4LCTopo_tightpp_sumet();
    if(RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg.IsActive()) RefEle_AntiKt4LCTopo_tightpp_etx_CentralReg();
    if(RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg.IsActive()) RefEle_AntiKt4LCTopo_tightpp_ety_CentralReg();
    if(RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsActive()) RefEle_AntiKt4LCTopo_tightpp_sumet_CentralReg();
    if(RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg.IsActive()) RefEle_AntiKt4LCTopo_tightpp_phi_CentralReg();
    if(RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsActive()) RefEle_AntiKt4LCTopo_tightpp_etx_EndcapRegion();
    if(RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsActive()) RefEle_AntiKt4LCTopo_tightpp_ety_EndcapRegion();
    if(RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsActive()) RefEle_AntiKt4LCTopo_tightpp_sumet_EndcapRegion();
    if(RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsActive()) RefEle_AntiKt4LCTopo_tightpp_phi_EndcapRegion();
    if(RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsActive()) RefEle_AntiKt4LCTopo_tightpp_etx_ForwardReg();
    if(RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsActive()) RefEle_AntiKt4LCTopo_tightpp_ety_ForwardReg();
    if(RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsActive()) RefEle_AntiKt4LCTopo_tightpp_sumet_ForwardReg();
    if(RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsActive()) RefEle_AntiKt4LCTopo_tightpp_phi_ForwardReg();
    if(RefTau_AntiKt4LCTopo_tightpp_etx.IsActive()) RefTau_AntiKt4LCTopo_tightpp_etx();
    if(RefTau_AntiKt4LCTopo_tightpp_ety.IsActive()) RefTau_AntiKt4LCTopo_tightpp_ety();
    if(RefTau_AntiKt4LCTopo_tightpp_phi.IsActive()) RefTau_AntiKt4LCTopo_tightpp_phi();
    if(RefTau_AntiKt4LCTopo_tightpp_et.IsActive()) RefTau_AntiKt4LCTopo_tightpp_et();
    if(RefTau_AntiKt4LCTopo_tightpp_sumet.IsActive()) RefTau_AntiKt4LCTopo_tightpp_sumet();
    if(RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg.IsActive()) RefTau_AntiKt4LCTopo_tightpp_etx_CentralReg();
    if(RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg.IsActive()) RefTau_AntiKt4LCTopo_tightpp_ety_CentralReg();
    if(RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsActive()) RefTau_AntiKt4LCTopo_tightpp_sumet_CentralReg();
    if(RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg.IsActive()) RefTau_AntiKt4LCTopo_tightpp_phi_CentralReg();
    if(RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsActive()) RefTau_AntiKt4LCTopo_tightpp_etx_EndcapRegion();
    if(RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsActive()) RefTau_AntiKt4LCTopo_tightpp_ety_EndcapRegion();
    if(RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsActive()) RefTau_AntiKt4LCTopo_tightpp_sumet_EndcapRegion();
    if(RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsActive()) RefTau_AntiKt4LCTopo_tightpp_phi_EndcapRegion();
    if(RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsActive()) RefTau_AntiKt4LCTopo_tightpp_etx_ForwardReg();
    if(RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsActive()) RefTau_AntiKt4LCTopo_tightpp_ety_ForwardReg();
    if(RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsActive()) RefTau_AntiKt4LCTopo_tightpp_sumet_ForwardReg();
    if(RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsActive()) RefTau_AntiKt4LCTopo_tightpp_phi_ForwardReg();
    if(RefJet_AntiKt4LCTopo_tightpp_etx.IsActive()) RefJet_AntiKt4LCTopo_tightpp_etx();
    if(RefJet_AntiKt4LCTopo_tightpp_ety.IsActive()) RefJet_AntiKt4LCTopo_tightpp_ety();
    if(RefJet_AntiKt4LCTopo_tightpp_phi.IsActive()) RefJet_AntiKt4LCTopo_tightpp_phi();
    if(RefJet_AntiKt4LCTopo_tightpp_et.IsActive()) RefJet_AntiKt4LCTopo_tightpp_et();
    if(RefJet_AntiKt4LCTopo_tightpp_sumet.IsActive()) RefJet_AntiKt4LCTopo_tightpp_sumet();
    if(RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg.IsActive()) RefJet_AntiKt4LCTopo_tightpp_etx_CentralReg();
    if(RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg.IsActive()) RefJet_AntiKt4LCTopo_tightpp_ety_CentralReg();
    if(RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsActive()) RefJet_AntiKt4LCTopo_tightpp_sumet_CentralReg();
    if(RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg.IsActive()) RefJet_AntiKt4LCTopo_tightpp_phi_CentralReg();
    if(RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsActive()) RefJet_AntiKt4LCTopo_tightpp_etx_EndcapRegion();
    if(RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsActive()) RefJet_AntiKt4LCTopo_tightpp_ety_EndcapRegion();
    if(RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsActive()) RefJet_AntiKt4LCTopo_tightpp_sumet_EndcapRegion();
    if(RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsActive()) RefJet_AntiKt4LCTopo_tightpp_phi_EndcapRegion();
    if(RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsActive()) RefJet_AntiKt4LCTopo_tightpp_etx_ForwardReg();
    if(RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsActive()) RefJet_AntiKt4LCTopo_tightpp_ety_ForwardReg();
    if(RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsActive()) RefJet_AntiKt4LCTopo_tightpp_sumet_ForwardReg();
    if(RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsActive()) RefJet_AntiKt4LCTopo_tightpp_phi_ForwardReg();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_etx.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_etx();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_ety.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_ety();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_phi.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_phi();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_et.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_et();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_CentralReg();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_CentralReg();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_CentralReg();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_CentralReg();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_EndcapRegion();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_EndcapRegion();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_EndcapRegion();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_EndcapRegion();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_etx_ForwardReg();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_ety_ForwardReg();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_sumet_ForwardReg();
    if(RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg.IsActive()) RefMuon_Staco_AntiKt4LCTopo_tightpp_phi_ForwardReg();
  }

} // namespace D3PDReader
#endif // D3PDREADER_MET_CXX
