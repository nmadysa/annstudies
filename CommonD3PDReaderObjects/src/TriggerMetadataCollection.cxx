// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TriggerMetadataCollection_CXX
#define D3PDREADER_TriggerMetadataCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TriggerMetadataCollection.h"

ClassImp(D3PDReader::TriggerMetadataCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TriggerMetadataCollection::TriggerMetadataCollection(const long int& master,const std::string& prefix):
    TObject(),
    SMK(prefix + "SMK",&master),
    L1PSK(prefix + "L1PSK",&master),
    HLTPSK(prefix + "HLTPSK",&master),
    LVL1NameMap(prefix + "LVL1NameMap",&master),
    LVL1PrescaleMap(prefix + "LVL1PrescaleMap",&master),
    HLTNameMap(prefix + "HLTNameMap",&master),
    HLTPrescaleMap(prefix + "HLTPrescaleMap",&master),
    HLTRerunPrescaleMap(prefix + "HLTRerunPrescaleMap",&master),
    HLTPassthroughMap(prefix + "HLTPassthroughMap",&master),
    HLTLowerChainNameMap(prefix + "HLTLowerChainNameMap",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TriggerMetadataCollection::TriggerMetadataCollection(const std::string& prefix):
    TObject(),
    SMK(prefix + "SMK",0),
    L1PSK(prefix + "L1PSK",0),
    HLTPSK(prefix + "HLTPSK",0),
    LVL1NameMap(prefix + "LVL1NameMap",0),
    LVL1PrescaleMap(prefix + "LVL1PrescaleMap",0),
    HLTNameMap(prefix + "HLTNameMap",0),
    HLTPrescaleMap(prefix + "HLTPrescaleMap",0),
    HLTRerunPrescaleMap(prefix + "HLTRerunPrescaleMap",0),
    HLTPassthroughMap(prefix + "HLTPassthroughMap",0),
    HLTLowerChainNameMap(prefix + "HLTLowerChainNameMap",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TriggerMetadataCollection::ReadFrom(TTree* tree)
  {
    SMK.ReadFrom(tree);
    L1PSK.ReadFrom(tree);
    HLTPSK.ReadFrom(tree);
    LVL1NameMap.ReadFrom(tree);
    LVL1PrescaleMap.ReadFrom(tree);
    HLTNameMap.ReadFrom(tree);
    HLTPrescaleMap.ReadFrom(tree);
    HLTRerunPrescaleMap.ReadFrom(tree);
    HLTPassthroughMap.ReadFrom(tree);
    HLTLowerChainNameMap.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TriggerMetadataCollection::WriteTo(TTree* tree)
  {
    SMK.WriteTo(tree);
    L1PSK.WriteTo(tree);
    HLTPSK.WriteTo(tree);
    LVL1NameMap.WriteTo(tree);
    LVL1PrescaleMap.WriteTo(tree);
    HLTNameMap.WriteTo(tree);
    HLTPrescaleMap.WriteTo(tree);
    HLTRerunPrescaleMap.WriteTo(tree);
    HLTPassthroughMap.WriteTo(tree);
    HLTLowerChainNameMap.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TriggerMetadataCollection::SetActive(bool active)
  {
    if(active)
    {
     if(SMK.IsAvailable()) SMK.SetActive(active);
     if(L1PSK.IsAvailable()) L1PSK.SetActive(active);
     if(HLTPSK.IsAvailable()) HLTPSK.SetActive(active);
     if(LVL1NameMap.IsAvailable()) LVL1NameMap.SetActive(active);
     if(LVL1PrescaleMap.IsAvailable()) LVL1PrescaleMap.SetActive(active);
     if(HLTNameMap.IsAvailable()) HLTNameMap.SetActive(active);
     if(HLTPrescaleMap.IsAvailable()) HLTPrescaleMap.SetActive(active);
     if(HLTRerunPrescaleMap.IsAvailable()) HLTRerunPrescaleMap.SetActive(active);
     if(HLTPassthroughMap.IsAvailable()) HLTPassthroughMap.SetActive(active);
     if(HLTLowerChainNameMap.IsAvailable()) HLTLowerChainNameMap.SetActive(active);
    }
    else
    {
      SMK.SetActive(active);
      L1PSK.SetActive(active);
      HLTPSK.SetActive(active);
      LVL1NameMap.SetActive(active);
      LVL1PrescaleMap.SetActive(active);
      HLTNameMap.SetActive(active);
      HLTPrescaleMap.SetActive(active);
      HLTRerunPrescaleMap.SetActive(active);
      HLTPassthroughMap.SetActive(active);
      HLTLowerChainNameMap.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TriggerMetadataCollection::ReadAllActive()
  {
    if(SMK.IsActive()) SMK();
    if(L1PSK.IsActive()) L1PSK();
    if(HLTPSK.IsActive()) HLTPSK();
    if(LVL1NameMap.IsActive()) LVL1NameMap();
    if(LVL1PrescaleMap.IsActive()) LVL1PrescaleMap();
    if(HLTNameMap.IsActive()) HLTNameMap();
    if(HLTPrescaleMap.IsActive()) HLTPrescaleMap();
    if(HLTRerunPrescaleMap.IsActive()) HLTRerunPrescaleMap();
    if(HLTPassthroughMap.IsActive()) HLTPassthroughMap();
    if(HLTLowerChainNameMap.IsActive()) HLTLowerChainNameMap();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TriggerMetadataCollection_CXX
