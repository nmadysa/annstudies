// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TrigEFElectronD3PDCollection_CXX
#define D3PDREADER_TrigEFElectronD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TrigEFElectronD3PDCollection.h"

ClassImp(D3PDReader::TrigEFElectronD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigEFElectronD3PDCollection::TrigEFElectronD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    EF_2e12Tvh_loose1(prefix + "EF_2e12Tvh_loose1",&master),
    EF_2e5_tight1_Jpsi(prefix + "EF_2e5_tight1_Jpsi",&master),
    EF_2e7T_loose1_mu6(prefix + "EF_2e7T_loose1_mu6",&master),
    EF_2e7T_medium1_mu6(prefix + "EF_2e7T_medium1_mu6",&master),
    EF_e11_etcut(prefix + "EF_e11_etcut",&master),
    EF_e12Tvh_loose1(prefix + "EF_e12Tvh_loose1",&master),
    EF_e12Tvh_loose1_mu8(prefix + "EF_e12Tvh_loose1_mu8",&master),
    EF_e12Tvh_medium1(prefix + "EF_e12Tvh_medium1",&master),
    EF_e12Tvh_medium1_mu10(prefix + "EF_e12Tvh_medium1_mu10",&master),
    EF_e12Tvh_medium1_mu6(prefix + "EF_e12Tvh_medium1_mu6",&master),
    EF_e12Tvh_medium1_mu6_topo_medium(prefix + "EF_e12Tvh_medium1_mu6_topo_medium",&master),
    EF_e12Tvh_medium1_mu8(prefix + "EF_e12Tvh_medium1_mu8",&master),
    EF_e13_etcutTrk_xs60(prefix + "EF_e13_etcutTrk_xs60",&master),
    EF_e13_etcutTrk_xs60_dphi2j15xe20(prefix + "EF_e13_etcutTrk_xs60_dphi2j15xe20",&master),
    EF_e14_tight1_e4_etcut_Jpsi(prefix + "EF_e14_tight1_e4_etcut_Jpsi",&master),
    EF_e15vh_medium1(prefix + "EF_e15vh_medium1",&master),
    EF_e18_loose1(prefix + "EF_e18_loose1",&master),
    EF_e18_loose1_g25_medium(prefix + "EF_e18_loose1_g25_medium",&master),
    EF_e18_loose1_g35_loose(prefix + "EF_e18_loose1_g35_loose",&master),
    EF_e18_loose1_g35_medium(prefix + "EF_e18_loose1_g35_medium",&master),
    EF_e18_medium1(prefix + "EF_e18_medium1",&master),
    EF_e18_medium1_g25_loose(prefix + "EF_e18_medium1_g25_loose",&master),
    EF_e18_medium1_g25_medium(prefix + "EF_e18_medium1_g25_medium",&master),
    EF_e18_medium1_g35_loose(prefix + "EF_e18_medium1_g35_loose",&master),
    EF_e18_medium1_g35_medium(prefix + "EF_e18_medium1_g35_medium",&master),
    EF_e18vh_medium1(prefix + "EF_e18vh_medium1",&master),
    EF_e18vh_medium1_2e7T_medium1(prefix + "EF_e18vh_medium1_2e7T_medium1",&master),
    EF_e20_etcutTrk_xe30_dphi2j15xe20(prefix + "EF_e20_etcutTrk_xe30_dphi2j15xe20",&master),
    EF_e20_etcutTrk_xs60_dphi2j15xe20(prefix + "EF_e20_etcutTrk_xs60_dphi2j15xe20",&master),
    EF_e20vhT_medium1_g6T_etcut_Upsi(prefix + "EF_e20vhT_medium1_g6T_etcut_Upsi",&master),
    EF_e20vhT_tight1_g6T_etcut_Upsi(prefix + "EF_e20vhT_tight1_g6T_etcut_Upsi",&master),
    EF_e22vh_loose(prefix + "EF_e22vh_loose",&master),
    EF_e22vh_loose0(prefix + "EF_e22vh_loose0",&master),
    EF_e22vh_loose1(prefix + "EF_e22vh_loose1",&master),
    EF_e22vh_medium1(prefix + "EF_e22vh_medium1",&master),
    EF_e22vh_medium1_IDTrkNoCut(prefix + "EF_e22vh_medium1_IDTrkNoCut",&master),
    EF_e22vh_medium1_IdScan(prefix + "EF_e22vh_medium1_IdScan",&master),
    EF_e22vh_medium1_SiTrk(prefix + "EF_e22vh_medium1_SiTrk",&master),
    EF_e22vh_medium1_TRT(prefix + "EF_e22vh_medium1_TRT",&master),
    EF_e22vhi_medium1(prefix + "EF_e22vhi_medium1",&master),
    EF_e24vh_loose(prefix + "EF_e24vh_loose",&master),
    EF_e24vh_loose0(prefix + "EF_e24vh_loose0",&master),
    EF_e24vh_loose1(prefix + "EF_e24vh_loose1",&master),
    EF_e24vh_medium1(prefix + "EF_e24vh_medium1",&master),
    EF_e24vh_medium1_EFxe30(prefix + "EF_e24vh_medium1_EFxe30",&master),
    EF_e24vh_medium1_EFxe30_tcem(prefix + "EF_e24vh_medium1_EFxe30_tcem",&master),
    EF_e24vh_medium1_EFxe35_tcem(prefix + "EF_e24vh_medium1_EFxe35_tcem",&master),
    EF_e24vh_medium1_EFxe35_tclcw(prefix + "EF_e24vh_medium1_EFxe35_tclcw",&master),
    EF_e24vh_medium1_EFxe40(prefix + "EF_e24vh_medium1_EFxe40",&master),
    EF_e24vh_medium1_IDTrkNoCut(prefix + "EF_e24vh_medium1_IDTrkNoCut",&master),
    EF_e24vh_medium1_IdScan(prefix + "EF_e24vh_medium1_IdScan",&master),
    EF_e24vh_medium1_L2StarB(prefix + "EF_e24vh_medium1_L2StarB",&master),
    EF_e24vh_medium1_L2StarC(prefix + "EF_e24vh_medium1_L2StarC",&master),
    EF_e24vh_medium1_SiTrk(prefix + "EF_e24vh_medium1_SiTrk",&master),
    EF_e24vh_medium1_TRT(prefix + "EF_e24vh_medium1_TRT",&master),
    EF_e24vh_medium1_b35_mediumEF_j35_a4tchad(prefix + "EF_e24vh_medium1_b35_mediumEF_j35_a4tchad",&master),
    EF_e24vh_medium1_e7_medium1(prefix + "EF_e24vh_medium1_e7_medium1",&master),
    EF_e24vh_tight1_e15_NoCut_Zee(prefix + "EF_e24vh_tight1_e15_NoCut_Zee",&master),
    EF_e24vhi_loose1_mu8(prefix + "EF_e24vhi_loose1_mu8",&master),
    EF_e24vhi_medium1(prefix + "EF_e24vhi_medium1",&master),
    EF_e45_etcut(prefix + "EF_e45_etcut",&master),
    EF_e45_medium1(prefix + "EF_e45_medium1",&master),
    EF_e5_tight1(prefix + "EF_e5_tight1",&master),
    EF_e5_tight1_e14_etcut_Jpsi(prefix + "EF_e5_tight1_e14_etcut_Jpsi",&master),
    EF_e5_tight1_e4_etcut_Jpsi(prefix + "EF_e5_tight1_e4_etcut_Jpsi",&master),
    EF_e5_tight1_e4_etcut_Jpsi_IdScan(prefix + "EF_e5_tight1_e4_etcut_Jpsi_IdScan",&master),
    EF_e5_tight1_e4_etcut_Jpsi_L2StarB(prefix + "EF_e5_tight1_e4_etcut_Jpsi_L2StarB",&master),
    EF_e5_tight1_e4_etcut_Jpsi_L2StarC(prefix + "EF_e5_tight1_e4_etcut_Jpsi_L2StarC",&master),
    EF_e5_tight1_e4_etcut_Jpsi_SiTrk(prefix + "EF_e5_tight1_e4_etcut_Jpsi_SiTrk",&master),
    EF_e5_tight1_e4_etcut_Jpsi_TRT(prefix + "EF_e5_tight1_e4_etcut_Jpsi_TRT",&master),
    EF_e5_tight1_e5_NoCut(prefix + "EF_e5_tight1_e5_NoCut",&master),
    EF_e5_tight1_e9_etcut_Jpsi(prefix + "EF_e5_tight1_e9_etcut_Jpsi",&master),
    EF_e60_etcut(prefix + "EF_e60_etcut",&master),
    EF_e60_medium1(prefix + "EF_e60_medium1",&master),
    EF_e7T_loose1(prefix + "EF_e7T_loose1",&master),
    EF_e7T_loose1_2mu6(prefix + "EF_e7T_loose1_2mu6",&master),
    EF_e7T_medium1(prefix + "EF_e7T_medium1",&master),
    EF_e7T_medium1_2mu6(prefix + "EF_e7T_medium1_2mu6",&master),
    EF_e9_tight1_e4_etcut_Jpsi(prefix + "EF_e9_tight1_e4_etcut_Jpsi",&master),
    EF_eb_physics(prefix + "EF_eb_physics",&master),
    EF_eb_physics_empty(prefix + "EF_eb_physics_empty",&master),
    EF_eb_physics_firstempty(prefix + "EF_eb_physics_firstempty",&master),
    EF_eb_physics_noL1PS(prefix + "EF_eb_physics_noL1PS",&master),
    EF_eb_physics_unpaired_iso(prefix + "EF_eb_physics_unpaired_iso",&master),
    EF_eb_physics_unpaired_noniso(prefix + "EF_eb_physics_unpaired_noniso",&master),
    EF_eb_random(prefix + "EF_eb_random",&master),
    EF_eb_random_empty(prefix + "EF_eb_random_empty",&master),
    EF_eb_random_firstempty(prefix + "EF_eb_random_firstempty",&master),
    EF_eb_random_unpaired_iso(prefix + "EF_eb_random_unpaired_iso",&master),
    n(prefix + "n",&master),
    E(prefix + "E",&master),
    Et(prefix + "Et",&master),
    pt(prefix + "pt",&master),
    m(prefix + "m",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    px(prefix + "px",&master),
    py(prefix + "py",&master),
    pz(prefix + "pz",&master),
    charge(prefix + "charge",&master),
    author(prefix + "author",&master),
    isEM(prefix + "isEM",&master),
    isEMLoose(prefix + "isEMLoose",&master),
    isEMMedium(prefix + "isEMMedium",&master),
    isEMTight(prefix + "isEMTight",&master),
    loose(prefix + "loose",&master),
    looseIso(prefix + "looseIso",&master),
    medium(prefix + "medium",&master),
    mediumIso(prefix + "mediumIso",&master),
    mediumWithoutTrack(prefix + "mediumWithoutTrack",&master),
    mediumIsoWithoutTrack(prefix + "mediumIsoWithoutTrack",&master),
    tight(prefix + "tight",&master),
    tightIso(prefix + "tightIso",&master),
    tightWithoutTrack(prefix + "tightWithoutTrack",&master),
    tightIsoWithoutTrack(prefix + "tightIsoWithoutTrack",&master),
    loosePP(prefix + "loosePP",&master),
    loosePPIso(prefix + "loosePPIso",&master),
    mediumPP(prefix + "mediumPP",&master),
    mediumPPIso(prefix + "mediumPPIso",&master),
    tightPP(prefix + "tightPP",&master),
    tightPPIso(prefix + "tightPPIso",&master),
    vertweight(prefix + "vertweight",&master),
    hastrack(prefix + "hastrack",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigEFElectronD3PDCollection::TrigEFElectronD3PDCollection(const std::string& prefix):
    TObject(),
    EF_2e12Tvh_loose1(prefix + "EF_2e12Tvh_loose1",0),
    EF_2e5_tight1_Jpsi(prefix + "EF_2e5_tight1_Jpsi",0),
    EF_2e7T_loose1_mu6(prefix + "EF_2e7T_loose1_mu6",0),
    EF_2e7T_medium1_mu6(prefix + "EF_2e7T_medium1_mu6",0),
    EF_e11_etcut(prefix + "EF_e11_etcut",0),
    EF_e12Tvh_loose1(prefix + "EF_e12Tvh_loose1",0),
    EF_e12Tvh_loose1_mu8(prefix + "EF_e12Tvh_loose1_mu8",0),
    EF_e12Tvh_medium1(prefix + "EF_e12Tvh_medium1",0),
    EF_e12Tvh_medium1_mu10(prefix + "EF_e12Tvh_medium1_mu10",0),
    EF_e12Tvh_medium1_mu6(prefix + "EF_e12Tvh_medium1_mu6",0),
    EF_e12Tvh_medium1_mu6_topo_medium(prefix + "EF_e12Tvh_medium1_mu6_topo_medium",0),
    EF_e12Tvh_medium1_mu8(prefix + "EF_e12Tvh_medium1_mu8",0),
    EF_e13_etcutTrk_xs60(prefix + "EF_e13_etcutTrk_xs60",0),
    EF_e13_etcutTrk_xs60_dphi2j15xe20(prefix + "EF_e13_etcutTrk_xs60_dphi2j15xe20",0),
    EF_e14_tight1_e4_etcut_Jpsi(prefix + "EF_e14_tight1_e4_etcut_Jpsi",0),
    EF_e15vh_medium1(prefix + "EF_e15vh_medium1",0),
    EF_e18_loose1(prefix + "EF_e18_loose1",0),
    EF_e18_loose1_g25_medium(prefix + "EF_e18_loose1_g25_medium",0),
    EF_e18_loose1_g35_loose(prefix + "EF_e18_loose1_g35_loose",0),
    EF_e18_loose1_g35_medium(prefix + "EF_e18_loose1_g35_medium",0),
    EF_e18_medium1(prefix + "EF_e18_medium1",0),
    EF_e18_medium1_g25_loose(prefix + "EF_e18_medium1_g25_loose",0),
    EF_e18_medium1_g25_medium(prefix + "EF_e18_medium1_g25_medium",0),
    EF_e18_medium1_g35_loose(prefix + "EF_e18_medium1_g35_loose",0),
    EF_e18_medium1_g35_medium(prefix + "EF_e18_medium1_g35_medium",0),
    EF_e18vh_medium1(prefix + "EF_e18vh_medium1",0),
    EF_e18vh_medium1_2e7T_medium1(prefix + "EF_e18vh_medium1_2e7T_medium1",0),
    EF_e20_etcutTrk_xe30_dphi2j15xe20(prefix + "EF_e20_etcutTrk_xe30_dphi2j15xe20",0),
    EF_e20_etcutTrk_xs60_dphi2j15xe20(prefix + "EF_e20_etcutTrk_xs60_dphi2j15xe20",0),
    EF_e20vhT_medium1_g6T_etcut_Upsi(prefix + "EF_e20vhT_medium1_g6T_etcut_Upsi",0),
    EF_e20vhT_tight1_g6T_etcut_Upsi(prefix + "EF_e20vhT_tight1_g6T_etcut_Upsi",0),
    EF_e22vh_loose(prefix + "EF_e22vh_loose",0),
    EF_e22vh_loose0(prefix + "EF_e22vh_loose0",0),
    EF_e22vh_loose1(prefix + "EF_e22vh_loose1",0),
    EF_e22vh_medium1(prefix + "EF_e22vh_medium1",0),
    EF_e22vh_medium1_IDTrkNoCut(prefix + "EF_e22vh_medium1_IDTrkNoCut",0),
    EF_e22vh_medium1_IdScan(prefix + "EF_e22vh_medium1_IdScan",0),
    EF_e22vh_medium1_SiTrk(prefix + "EF_e22vh_medium1_SiTrk",0),
    EF_e22vh_medium1_TRT(prefix + "EF_e22vh_medium1_TRT",0),
    EF_e22vhi_medium1(prefix + "EF_e22vhi_medium1",0),
    EF_e24vh_loose(prefix + "EF_e24vh_loose",0),
    EF_e24vh_loose0(prefix + "EF_e24vh_loose0",0),
    EF_e24vh_loose1(prefix + "EF_e24vh_loose1",0),
    EF_e24vh_medium1(prefix + "EF_e24vh_medium1",0),
    EF_e24vh_medium1_EFxe30(prefix + "EF_e24vh_medium1_EFxe30",0),
    EF_e24vh_medium1_EFxe30_tcem(prefix + "EF_e24vh_medium1_EFxe30_tcem",0),
    EF_e24vh_medium1_EFxe35_tcem(prefix + "EF_e24vh_medium1_EFxe35_tcem",0),
    EF_e24vh_medium1_EFxe35_tclcw(prefix + "EF_e24vh_medium1_EFxe35_tclcw",0),
    EF_e24vh_medium1_EFxe40(prefix + "EF_e24vh_medium1_EFxe40",0),
    EF_e24vh_medium1_IDTrkNoCut(prefix + "EF_e24vh_medium1_IDTrkNoCut",0),
    EF_e24vh_medium1_IdScan(prefix + "EF_e24vh_medium1_IdScan",0),
    EF_e24vh_medium1_L2StarB(prefix + "EF_e24vh_medium1_L2StarB",0),
    EF_e24vh_medium1_L2StarC(prefix + "EF_e24vh_medium1_L2StarC",0),
    EF_e24vh_medium1_SiTrk(prefix + "EF_e24vh_medium1_SiTrk",0),
    EF_e24vh_medium1_TRT(prefix + "EF_e24vh_medium1_TRT",0),
    EF_e24vh_medium1_b35_mediumEF_j35_a4tchad(prefix + "EF_e24vh_medium1_b35_mediumEF_j35_a4tchad",0),
    EF_e24vh_medium1_e7_medium1(prefix + "EF_e24vh_medium1_e7_medium1",0),
    EF_e24vh_tight1_e15_NoCut_Zee(prefix + "EF_e24vh_tight1_e15_NoCut_Zee",0),
    EF_e24vhi_loose1_mu8(prefix + "EF_e24vhi_loose1_mu8",0),
    EF_e24vhi_medium1(prefix + "EF_e24vhi_medium1",0),
    EF_e45_etcut(prefix + "EF_e45_etcut",0),
    EF_e45_medium1(prefix + "EF_e45_medium1",0),
    EF_e5_tight1(prefix + "EF_e5_tight1",0),
    EF_e5_tight1_e14_etcut_Jpsi(prefix + "EF_e5_tight1_e14_etcut_Jpsi",0),
    EF_e5_tight1_e4_etcut_Jpsi(prefix + "EF_e5_tight1_e4_etcut_Jpsi",0),
    EF_e5_tight1_e4_etcut_Jpsi_IdScan(prefix + "EF_e5_tight1_e4_etcut_Jpsi_IdScan",0),
    EF_e5_tight1_e4_etcut_Jpsi_L2StarB(prefix + "EF_e5_tight1_e4_etcut_Jpsi_L2StarB",0),
    EF_e5_tight1_e4_etcut_Jpsi_L2StarC(prefix + "EF_e5_tight1_e4_etcut_Jpsi_L2StarC",0),
    EF_e5_tight1_e4_etcut_Jpsi_SiTrk(prefix + "EF_e5_tight1_e4_etcut_Jpsi_SiTrk",0),
    EF_e5_tight1_e4_etcut_Jpsi_TRT(prefix + "EF_e5_tight1_e4_etcut_Jpsi_TRT",0),
    EF_e5_tight1_e5_NoCut(prefix + "EF_e5_tight1_e5_NoCut",0),
    EF_e5_tight1_e9_etcut_Jpsi(prefix + "EF_e5_tight1_e9_etcut_Jpsi",0),
    EF_e60_etcut(prefix + "EF_e60_etcut",0),
    EF_e60_medium1(prefix + "EF_e60_medium1",0),
    EF_e7T_loose1(prefix + "EF_e7T_loose1",0),
    EF_e7T_loose1_2mu6(prefix + "EF_e7T_loose1_2mu6",0),
    EF_e7T_medium1(prefix + "EF_e7T_medium1",0),
    EF_e7T_medium1_2mu6(prefix + "EF_e7T_medium1_2mu6",0),
    EF_e9_tight1_e4_etcut_Jpsi(prefix + "EF_e9_tight1_e4_etcut_Jpsi",0),
    EF_eb_physics(prefix + "EF_eb_physics",0),
    EF_eb_physics_empty(prefix + "EF_eb_physics_empty",0),
    EF_eb_physics_firstempty(prefix + "EF_eb_physics_firstempty",0),
    EF_eb_physics_noL1PS(prefix + "EF_eb_physics_noL1PS",0),
    EF_eb_physics_unpaired_iso(prefix + "EF_eb_physics_unpaired_iso",0),
    EF_eb_physics_unpaired_noniso(prefix + "EF_eb_physics_unpaired_noniso",0),
    EF_eb_random(prefix + "EF_eb_random",0),
    EF_eb_random_empty(prefix + "EF_eb_random_empty",0),
    EF_eb_random_firstempty(prefix + "EF_eb_random_firstempty",0),
    EF_eb_random_unpaired_iso(prefix + "EF_eb_random_unpaired_iso",0),
    n(prefix + "n",0),
    E(prefix + "E",0),
    Et(prefix + "Et",0),
    pt(prefix + "pt",0),
    m(prefix + "m",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    px(prefix + "px",0),
    py(prefix + "py",0),
    pz(prefix + "pz",0),
    charge(prefix + "charge",0),
    author(prefix + "author",0),
    isEM(prefix + "isEM",0),
    isEMLoose(prefix + "isEMLoose",0),
    isEMMedium(prefix + "isEMMedium",0),
    isEMTight(prefix + "isEMTight",0),
    loose(prefix + "loose",0),
    looseIso(prefix + "looseIso",0),
    medium(prefix + "medium",0),
    mediumIso(prefix + "mediumIso",0),
    mediumWithoutTrack(prefix + "mediumWithoutTrack",0),
    mediumIsoWithoutTrack(prefix + "mediumIsoWithoutTrack",0),
    tight(prefix + "tight",0),
    tightIso(prefix + "tightIso",0),
    tightWithoutTrack(prefix + "tightWithoutTrack",0),
    tightIsoWithoutTrack(prefix + "tightIsoWithoutTrack",0),
    loosePP(prefix + "loosePP",0),
    loosePPIso(prefix + "loosePPIso",0),
    mediumPP(prefix + "mediumPP",0),
    mediumPPIso(prefix + "mediumPPIso",0),
    tightPP(prefix + "tightPP",0),
    tightPPIso(prefix + "tightPPIso",0),
    vertweight(prefix + "vertweight",0),
    hastrack(prefix + "hastrack",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TrigEFElectronD3PDCollection::ReadFrom(TTree* tree)
  {
    EF_2e12Tvh_loose1.ReadFrom(tree);
    EF_2e5_tight1_Jpsi.ReadFrom(tree);
    EF_2e7T_loose1_mu6.ReadFrom(tree);
    EF_2e7T_medium1_mu6.ReadFrom(tree);
    EF_e11_etcut.ReadFrom(tree);
    EF_e12Tvh_loose1.ReadFrom(tree);
    EF_e12Tvh_loose1_mu8.ReadFrom(tree);
    EF_e12Tvh_medium1.ReadFrom(tree);
    EF_e12Tvh_medium1_mu10.ReadFrom(tree);
    EF_e12Tvh_medium1_mu6.ReadFrom(tree);
    EF_e12Tvh_medium1_mu6_topo_medium.ReadFrom(tree);
    EF_e12Tvh_medium1_mu8.ReadFrom(tree);
    EF_e13_etcutTrk_xs60.ReadFrom(tree);
    EF_e13_etcutTrk_xs60_dphi2j15xe20.ReadFrom(tree);
    EF_e14_tight1_e4_etcut_Jpsi.ReadFrom(tree);
    EF_e15vh_medium1.ReadFrom(tree);
    EF_e18_loose1.ReadFrom(tree);
    EF_e18_loose1_g25_medium.ReadFrom(tree);
    EF_e18_loose1_g35_loose.ReadFrom(tree);
    EF_e18_loose1_g35_medium.ReadFrom(tree);
    EF_e18_medium1.ReadFrom(tree);
    EF_e18_medium1_g25_loose.ReadFrom(tree);
    EF_e18_medium1_g25_medium.ReadFrom(tree);
    EF_e18_medium1_g35_loose.ReadFrom(tree);
    EF_e18_medium1_g35_medium.ReadFrom(tree);
    EF_e18vh_medium1.ReadFrom(tree);
    EF_e18vh_medium1_2e7T_medium1.ReadFrom(tree);
    EF_e20_etcutTrk_xe30_dphi2j15xe20.ReadFrom(tree);
    EF_e20_etcutTrk_xs60_dphi2j15xe20.ReadFrom(tree);
    EF_e20vhT_medium1_g6T_etcut_Upsi.ReadFrom(tree);
    EF_e20vhT_tight1_g6T_etcut_Upsi.ReadFrom(tree);
    EF_e22vh_loose.ReadFrom(tree);
    EF_e22vh_loose0.ReadFrom(tree);
    EF_e22vh_loose1.ReadFrom(tree);
    EF_e22vh_medium1.ReadFrom(tree);
    EF_e22vh_medium1_IDTrkNoCut.ReadFrom(tree);
    EF_e22vh_medium1_IdScan.ReadFrom(tree);
    EF_e22vh_medium1_SiTrk.ReadFrom(tree);
    EF_e22vh_medium1_TRT.ReadFrom(tree);
    EF_e22vhi_medium1.ReadFrom(tree);
    EF_e24vh_loose.ReadFrom(tree);
    EF_e24vh_loose0.ReadFrom(tree);
    EF_e24vh_loose1.ReadFrom(tree);
    EF_e24vh_medium1.ReadFrom(tree);
    EF_e24vh_medium1_EFxe30.ReadFrom(tree);
    EF_e24vh_medium1_EFxe30_tcem.ReadFrom(tree);
    EF_e24vh_medium1_EFxe35_tcem.ReadFrom(tree);
    EF_e24vh_medium1_EFxe35_tclcw.ReadFrom(tree);
    EF_e24vh_medium1_EFxe40.ReadFrom(tree);
    EF_e24vh_medium1_IDTrkNoCut.ReadFrom(tree);
    EF_e24vh_medium1_IdScan.ReadFrom(tree);
    EF_e24vh_medium1_L2StarB.ReadFrom(tree);
    EF_e24vh_medium1_L2StarC.ReadFrom(tree);
    EF_e24vh_medium1_SiTrk.ReadFrom(tree);
    EF_e24vh_medium1_TRT.ReadFrom(tree);
    EF_e24vh_medium1_b35_mediumEF_j35_a4tchad.ReadFrom(tree);
    EF_e24vh_medium1_e7_medium1.ReadFrom(tree);
    EF_e24vh_tight1_e15_NoCut_Zee.ReadFrom(tree);
    EF_e24vhi_loose1_mu8.ReadFrom(tree);
    EF_e24vhi_medium1.ReadFrom(tree);
    EF_e45_etcut.ReadFrom(tree);
    EF_e45_medium1.ReadFrom(tree);
    EF_e5_tight1.ReadFrom(tree);
    EF_e5_tight1_e14_etcut_Jpsi.ReadFrom(tree);
    EF_e5_tight1_e4_etcut_Jpsi.ReadFrom(tree);
    EF_e5_tight1_e4_etcut_Jpsi_IdScan.ReadFrom(tree);
    EF_e5_tight1_e4_etcut_Jpsi_L2StarB.ReadFrom(tree);
    EF_e5_tight1_e4_etcut_Jpsi_L2StarC.ReadFrom(tree);
    EF_e5_tight1_e4_etcut_Jpsi_SiTrk.ReadFrom(tree);
    EF_e5_tight1_e4_etcut_Jpsi_TRT.ReadFrom(tree);
    EF_e5_tight1_e5_NoCut.ReadFrom(tree);
    EF_e5_tight1_e9_etcut_Jpsi.ReadFrom(tree);
    EF_e60_etcut.ReadFrom(tree);
    EF_e60_medium1.ReadFrom(tree);
    EF_e7T_loose1.ReadFrom(tree);
    EF_e7T_loose1_2mu6.ReadFrom(tree);
    EF_e7T_medium1.ReadFrom(tree);
    EF_e7T_medium1_2mu6.ReadFrom(tree);
    EF_e9_tight1_e4_etcut_Jpsi.ReadFrom(tree);
    EF_eb_physics.ReadFrom(tree);
    EF_eb_physics_empty.ReadFrom(tree);
    EF_eb_physics_firstempty.ReadFrom(tree);
    EF_eb_physics_noL1PS.ReadFrom(tree);
    EF_eb_physics_unpaired_iso.ReadFrom(tree);
    EF_eb_physics_unpaired_noniso.ReadFrom(tree);
    EF_eb_random.ReadFrom(tree);
    EF_eb_random_empty.ReadFrom(tree);
    EF_eb_random_firstempty.ReadFrom(tree);
    EF_eb_random_unpaired_iso.ReadFrom(tree);
    n.ReadFrom(tree);
    E.ReadFrom(tree);
    Et.ReadFrom(tree);
    pt.ReadFrom(tree);
    m.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    px.ReadFrom(tree);
    py.ReadFrom(tree);
    pz.ReadFrom(tree);
    charge.ReadFrom(tree);
    author.ReadFrom(tree);
    isEM.ReadFrom(tree);
    isEMLoose.ReadFrom(tree);
    isEMMedium.ReadFrom(tree);
    isEMTight.ReadFrom(tree);
    loose.ReadFrom(tree);
    looseIso.ReadFrom(tree);
    medium.ReadFrom(tree);
    mediumIso.ReadFrom(tree);
    mediumWithoutTrack.ReadFrom(tree);
    mediumIsoWithoutTrack.ReadFrom(tree);
    tight.ReadFrom(tree);
    tightIso.ReadFrom(tree);
    tightWithoutTrack.ReadFrom(tree);
    tightIsoWithoutTrack.ReadFrom(tree);
    loosePP.ReadFrom(tree);
    loosePPIso.ReadFrom(tree);
    mediumPP.ReadFrom(tree);
    mediumPPIso.ReadFrom(tree);
    tightPP.ReadFrom(tree);
    tightPPIso.ReadFrom(tree);
    vertweight.ReadFrom(tree);
    hastrack.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TrigEFElectronD3PDCollection::WriteTo(TTree* tree)
  {
    EF_2e12Tvh_loose1.WriteTo(tree);
    EF_2e5_tight1_Jpsi.WriteTo(tree);
    EF_2e7T_loose1_mu6.WriteTo(tree);
    EF_2e7T_medium1_mu6.WriteTo(tree);
    EF_e11_etcut.WriteTo(tree);
    EF_e12Tvh_loose1.WriteTo(tree);
    EF_e12Tvh_loose1_mu8.WriteTo(tree);
    EF_e12Tvh_medium1.WriteTo(tree);
    EF_e12Tvh_medium1_mu10.WriteTo(tree);
    EF_e12Tvh_medium1_mu6.WriteTo(tree);
    EF_e12Tvh_medium1_mu6_topo_medium.WriteTo(tree);
    EF_e12Tvh_medium1_mu8.WriteTo(tree);
    EF_e13_etcutTrk_xs60.WriteTo(tree);
    EF_e13_etcutTrk_xs60_dphi2j15xe20.WriteTo(tree);
    EF_e14_tight1_e4_etcut_Jpsi.WriteTo(tree);
    EF_e15vh_medium1.WriteTo(tree);
    EF_e18_loose1.WriteTo(tree);
    EF_e18_loose1_g25_medium.WriteTo(tree);
    EF_e18_loose1_g35_loose.WriteTo(tree);
    EF_e18_loose1_g35_medium.WriteTo(tree);
    EF_e18_medium1.WriteTo(tree);
    EF_e18_medium1_g25_loose.WriteTo(tree);
    EF_e18_medium1_g25_medium.WriteTo(tree);
    EF_e18_medium1_g35_loose.WriteTo(tree);
    EF_e18_medium1_g35_medium.WriteTo(tree);
    EF_e18vh_medium1.WriteTo(tree);
    EF_e18vh_medium1_2e7T_medium1.WriteTo(tree);
    EF_e20_etcutTrk_xe30_dphi2j15xe20.WriteTo(tree);
    EF_e20_etcutTrk_xs60_dphi2j15xe20.WriteTo(tree);
    EF_e20vhT_medium1_g6T_etcut_Upsi.WriteTo(tree);
    EF_e20vhT_tight1_g6T_etcut_Upsi.WriteTo(tree);
    EF_e22vh_loose.WriteTo(tree);
    EF_e22vh_loose0.WriteTo(tree);
    EF_e22vh_loose1.WriteTo(tree);
    EF_e22vh_medium1.WriteTo(tree);
    EF_e22vh_medium1_IDTrkNoCut.WriteTo(tree);
    EF_e22vh_medium1_IdScan.WriteTo(tree);
    EF_e22vh_medium1_SiTrk.WriteTo(tree);
    EF_e22vh_medium1_TRT.WriteTo(tree);
    EF_e22vhi_medium1.WriteTo(tree);
    EF_e24vh_loose.WriteTo(tree);
    EF_e24vh_loose0.WriteTo(tree);
    EF_e24vh_loose1.WriteTo(tree);
    EF_e24vh_medium1.WriteTo(tree);
    EF_e24vh_medium1_EFxe30.WriteTo(tree);
    EF_e24vh_medium1_EFxe30_tcem.WriteTo(tree);
    EF_e24vh_medium1_EFxe35_tcem.WriteTo(tree);
    EF_e24vh_medium1_EFxe35_tclcw.WriteTo(tree);
    EF_e24vh_medium1_EFxe40.WriteTo(tree);
    EF_e24vh_medium1_IDTrkNoCut.WriteTo(tree);
    EF_e24vh_medium1_IdScan.WriteTo(tree);
    EF_e24vh_medium1_L2StarB.WriteTo(tree);
    EF_e24vh_medium1_L2StarC.WriteTo(tree);
    EF_e24vh_medium1_SiTrk.WriteTo(tree);
    EF_e24vh_medium1_TRT.WriteTo(tree);
    EF_e24vh_medium1_b35_mediumEF_j35_a4tchad.WriteTo(tree);
    EF_e24vh_medium1_e7_medium1.WriteTo(tree);
    EF_e24vh_tight1_e15_NoCut_Zee.WriteTo(tree);
    EF_e24vhi_loose1_mu8.WriteTo(tree);
    EF_e24vhi_medium1.WriteTo(tree);
    EF_e45_etcut.WriteTo(tree);
    EF_e45_medium1.WriteTo(tree);
    EF_e5_tight1.WriteTo(tree);
    EF_e5_tight1_e14_etcut_Jpsi.WriteTo(tree);
    EF_e5_tight1_e4_etcut_Jpsi.WriteTo(tree);
    EF_e5_tight1_e4_etcut_Jpsi_IdScan.WriteTo(tree);
    EF_e5_tight1_e4_etcut_Jpsi_L2StarB.WriteTo(tree);
    EF_e5_tight1_e4_etcut_Jpsi_L2StarC.WriteTo(tree);
    EF_e5_tight1_e4_etcut_Jpsi_SiTrk.WriteTo(tree);
    EF_e5_tight1_e4_etcut_Jpsi_TRT.WriteTo(tree);
    EF_e5_tight1_e5_NoCut.WriteTo(tree);
    EF_e5_tight1_e9_etcut_Jpsi.WriteTo(tree);
    EF_e60_etcut.WriteTo(tree);
    EF_e60_medium1.WriteTo(tree);
    EF_e7T_loose1.WriteTo(tree);
    EF_e7T_loose1_2mu6.WriteTo(tree);
    EF_e7T_medium1.WriteTo(tree);
    EF_e7T_medium1_2mu6.WriteTo(tree);
    EF_e9_tight1_e4_etcut_Jpsi.WriteTo(tree);
    EF_eb_physics.WriteTo(tree);
    EF_eb_physics_empty.WriteTo(tree);
    EF_eb_physics_firstempty.WriteTo(tree);
    EF_eb_physics_noL1PS.WriteTo(tree);
    EF_eb_physics_unpaired_iso.WriteTo(tree);
    EF_eb_physics_unpaired_noniso.WriteTo(tree);
    EF_eb_random.WriteTo(tree);
    EF_eb_random_empty.WriteTo(tree);
    EF_eb_random_firstempty.WriteTo(tree);
    EF_eb_random_unpaired_iso.WriteTo(tree);
    n.WriteTo(tree);
    E.WriteTo(tree);
    Et.WriteTo(tree);
    pt.WriteTo(tree);
    m.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    px.WriteTo(tree);
    py.WriteTo(tree);
    pz.WriteTo(tree);
    charge.WriteTo(tree);
    author.WriteTo(tree);
    isEM.WriteTo(tree);
    isEMLoose.WriteTo(tree);
    isEMMedium.WriteTo(tree);
    isEMTight.WriteTo(tree);
    loose.WriteTo(tree);
    looseIso.WriteTo(tree);
    medium.WriteTo(tree);
    mediumIso.WriteTo(tree);
    mediumWithoutTrack.WriteTo(tree);
    mediumIsoWithoutTrack.WriteTo(tree);
    tight.WriteTo(tree);
    tightIso.WriteTo(tree);
    tightWithoutTrack.WriteTo(tree);
    tightIsoWithoutTrack.WriteTo(tree);
    loosePP.WriteTo(tree);
    loosePPIso.WriteTo(tree);
    mediumPP.WriteTo(tree);
    mediumPPIso.WriteTo(tree);
    tightPP.WriteTo(tree);
    tightPPIso.WriteTo(tree);
    vertweight.WriteTo(tree);
    hastrack.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TrigEFElectronD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(EF_2e12Tvh_loose1.IsAvailable()) EF_2e12Tvh_loose1.SetActive(active);
     if(EF_2e5_tight1_Jpsi.IsAvailable()) EF_2e5_tight1_Jpsi.SetActive(active);
     if(EF_2e7T_loose1_mu6.IsAvailable()) EF_2e7T_loose1_mu6.SetActive(active);
     if(EF_2e7T_medium1_mu6.IsAvailable()) EF_2e7T_medium1_mu6.SetActive(active);
     if(EF_e11_etcut.IsAvailable()) EF_e11_etcut.SetActive(active);
     if(EF_e12Tvh_loose1.IsAvailable()) EF_e12Tvh_loose1.SetActive(active);
     if(EF_e12Tvh_loose1_mu8.IsAvailable()) EF_e12Tvh_loose1_mu8.SetActive(active);
     if(EF_e12Tvh_medium1.IsAvailable()) EF_e12Tvh_medium1.SetActive(active);
     if(EF_e12Tvh_medium1_mu10.IsAvailable()) EF_e12Tvh_medium1_mu10.SetActive(active);
     if(EF_e12Tvh_medium1_mu6.IsAvailable()) EF_e12Tvh_medium1_mu6.SetActive(active);
     if(EF_e12Tvh_medium1_mu6_topo_medium.IsAvailable()) EF_e12Tvh_medium1_mu6_topo_medium.SetActive(active);
     if(EF_e12Tvh_medium1_mu8.IsAvailable()) EF_e12Tvh_medium1_mu8.SetActive(active);
     if(EF_e13_etcutTrk_xs60.IsAvailable()) EF_e13_etcutTrk_xs60.SetActive(active);
     if(EF_e13_etcutTrk_xs60_dphi2j15xe20.IsAvailable()) EF_e13_etcutTrk_xs60_dphi2j15xe20.SetActive(active);
     if(EF_e14_tight1_e4_etcut_Jpsi.IsAvailable()) EF_e14_tight1_e4_etcut_Jpsi.SetActive(active);
     if(EF_e15vh_medium1.IsAvailable()) EF_e15vh_medium1.SetActive(active);
     if(EF_e18_loose1.IsAvailable()) EF_e18_loose1.SetActive(active);
     if(EF_e18_loose1_g25_medium.IsAvailable()) EF_e18_loose1_g25_medium.SetActive(active);
     if(EF_e18_loose1_g35_loose.IsAvailable()) EF_e18_loose1_g35_loose.SetActive(active);
     if(EF_e18_loose1_g35_medium.IsAvailable()) EF_e18_loose1_g35_medium.SetActive(active);
     if(EF_e18_medium1.IsAvailable()) EF_e18_medium1.SetActive(active);
     if(EF_e18_medium1_g25_loose.IsAvailable()) EF_e18_medium1_g25_loose.SetActive(active);
     if(EF_e18_medium1_g25_medium.IsAvailable()) EF_e18_medium1_g25_medium.SetActive(active);
     if(EF_e18_medium1_g35_loose.IsAvailable()) EF_e18_medium1_g35_loose.SetActive(active);
     if(EF_e18_medium1_g35_medium.IsAvailable()) EF_e18_medium1_g35_medium.SetActive(active);
     if(EF_e18vh_medium1.IsAvailable()) EF_e18vh_medium1.SetActive(active);
     if(EF_e18vh_medium1_2e7T_medium1.IsAvailable()) EF_e18vh_medium1_2e7T_medium1.SetActive(active);
     if(EF_e20_etcutTrk_xe30_dphi2j15xe20.IsAvailable()) EF_e20_etcutTrk_xe30_dphi2j15xe20.SetActive(active);
     if(EF_e20_etcutTrk_xs60_dphi2j15xe20.IsAvailable()) EF_e20_etcutTrk_xs60_dphi2j15xe20.SetActive(active);
     if(EF_e20vhT_medium1_g6T_etcut_Upsi.IsAvailable()) EF_e20vhT_medium1_g6T_etcut_Upsi.SetActive(active);
     if(EF_e20vhT_tight1_g6T_etcut_Upsi.IsAvailable()) EF_e20vhT_tight1_g6T_etcut_Upsi.SetActive(active);
     if(EF_e22vh_loose.IsAvailable()) EF_e22vh_loose.SetActive(active);
     if(EF_e22vh_loose0.IsAvailable()) EF_e22vh_loose0.SetActive(active);
     if(EF_e22vh_loose1.IsAvailable()) EF_e22vh_loose1.SetActive(active);
     if(EF_e22vh_medium1.IsAvailable()) EF_e22vh_medium1.SetActive(active);
     if(EF_e22vh_medium1_IDTrkNoCut.IsAvailable()) EF_e22vh_medium1_IDTrkNoCut.SetActive(active);
     if(EF_e22vh_medium1_IdScan.IsAvailable()) EF_e22vh_medium1_IdScan.SetActive(active);
     if(EF_e22vh_medium1_SiTrk.IsAvailable()) EF_e22vh_medium1_SiTrk.SetActive(active);
     if(EF_e22vh_medium1_TRT.IsAvailable()) EF_e22vh_medium1_TRT.SetActive(active);
     if(EF_e22vhi_medium1.IsAvailable()) EF_e22vhi_medium1.SetActive(active);
     if(EF_e24vh_loose.IsAvailable()) EF_e24vh_loose.SetActive(active);
     if(EF_e24vh_loose0.IsAvailable()) EF_e24vh_loose0.SetActive(active);
     if(EF_e24vh_loose1.IsAvailable()) EF_e24vh_loose1.SetActive(active);
     if(EF_e24vh_medium1.IsAvailable()) EF_e24vh_medium1.SetActive(active);
     if(EF_e24vh_medium1_EFxe30.IsAvailable()) EF_e24vh_medium1_EFxe30.SetActive(active);
     if(EF_e24vh_medium1_EFxe30_tcem.IsAvailable()) EF_e24vh_medium1_EFxe30_tcem.SetActive(active);
     if(EF_e24vh_medium1_EFxe35_tcem.IsAvailable()) EF_e24vh_medium1_EFxe35_tcem.SetActive(active);
     if(EF_e24vh_medium1_EFxe35_tclcw.IsAvailable()) EF_e24vh_medium1_EFxe35_tclcw.SetActive(active);
     if(EF_e24vh_medium1_EFxe40.IsAvailable()) EF_e24vh_medium1_EFxe40.SetActive(active);
     if(EF_e24vh_medium1_IDTrkNoCut.IsAvailable()) EF_e24vh_medium1_IDTrkNoCut.SetActive(active);
     if(EF_e24vh_medium1_IdScan.IsAvailable()) EF_e24vh_medium1_IdScan.SetActive(active);
     if(EF_e24vh_medium1_L2StarB.IsAvailable()) EF_e24vh_medium1_L2StarB.SetActive(active);
     if(EF_e24vh_medium1_L2StarC.IsAvailable()) EF_e24vh_medium1_L2StarC.SetActive(active);
     if(EF_e24vh_medium1_SiTrk.IsAvailable()) EF_e24vh_medium1_SiTrk.SetActive(active);
     if(EF_e24vh_medium1_TRT.IsAvailable()) EF_e24vh_medium1_TRT.SetActive(active);
     if(EF_e24vh_medium1_b35_mediumEF_j35_a4tchad.IsAvailable()) EF_e24vh_medium1_b35_mediumEF_j35_a4tchad.SetActive(active);
     if(EF_e24vh_medium1_e7_medium1.IsAvailable()) EF_e24vh_medium1_e7_medium1.SetActive(active);
     if(EF_e24vh_tight1_e15_NoCut_Zee.IsAvailable()) EF_e24vh_tight1_e15_NoCut_Zee.SetActive(active);
     if(EF_e24vhi_loose1_mu8.IsAvailable()) EF_e24vhi_loose1_mu8.SetActive(active);
     if(EF_e24vhi_medium1.IsAvailable()) EF_e24vhi_medium1.SetActive(active);
     if(EF_e45_etcut.IsAvailable()) EF_e45_etcut.SetActive(active);
     if(EF_e45_medium1.IsAvailable()) EF_e45_medium1.SetActive(active);
     if(EF_e5_tight1.IsAvailable()) EF_e5_tight1.SetActive(active);
     if(EF_e5_tight1_e14_etcut_Jpsi.IsAvailable()) EF_e5_tight1_e14_etcut_Jpsi.SetActive(active);
     if(EF_e5_tight1_e4_etcut_Jpsi.IsAvailable()) EF_e5_tight1_e4_etcut_Jpsi.SetActive(active);
     if(EF_e5_tight1_e4_etcut_Jpsi_IdScan.IsAvailable()) EF_e5_tight1_e4_etcut_Jpsi_IdScan.SetActive(active);
     if(EF_e5_tight1_e4_etcut_Jpsi_L2StarB.IsAvailable()) EF_e5_tight1_e4_etcut_Jpsi_L2StarB.SetActive(active);
     if(EF_e5_tight1_e4_etcut_Jpsi_L2StarC.IsAvailable()) EF_e5_tight1_e4_etcut_Jpsi_L2StarC.SetActive(active);
     if(EF_e5_tight1_e4_etcut_Jpsi_SiTrk.IsAvailable()) EF_e5_tight1_e4_etcut_Jpsi_SiTrk.SetActive(active);
     if(EF_e5_tight1_e4_etcut_Jpsi_TRT.IsAvailable()) EF_e5_tight1_e4_etcut_Jpsi_TRT.SetActive(active);
     if(EF_e5_tight1_e5_NoCut.IsAvailable()) EF_e5_tight1_e5_NoCut.SetActive(active);
     if(EF_e5_tight1_e9_etcut_Jpsi.IsAvailable()) EF_e5_tight1_e9_etcut_Jpsi.SetActive(active);
     if(EF_e60_etcut.IsAvailable()) EF_e60_etcut.SetActive(active);
     if(EF_e60_medium1.IsAvailable()) EF_e60_medium1.SetActive(active);
     if(EF_e7T_loose1.IsAvailable()) EF_e7T_loose1.SetActive(active);
     if(EF_e7T_loose1_2mu6.IsAvailable()) EF_e7T_loose1_2mu6.SetActive(active);
     if(EF_e7T_medium1.IsAvailable()) EF_e7T_medium1.SetActive(active);
     if(EF_e7T_medium1_2mu6.IsAvailable()) EF_e7T_medium1_2mu6.SetActive(active);
     if(EF_e9_tight1_e4_etcut_Jpsi.IsAvailable()) EF_e9_tight1_e4_etcut_Jpsi.SetActive(active);
     if(EF_eb_physics.IsAvailable()) EF_eb_physics.SetActive(active);
     if(EF_eb_physics_empty.IsAvailable()) EF_eb_physics_empty.SetActive(active);
     if(EF_eb_physics_firstempty.IsAvailable()) EF_eb_physics_firstempty.SetActive(active);
     if(EF_eb_physics_noL1PS.IsAvailable()) EF_eb_physics_noL1PS.SetActive(active);
     if(EF_eb_physics_unpaired_iso.IsAvailable()) EF_eb_physics_unpaired_iso.SetActive(active);
     if(EF_eb_physics_unpaired_noniso.IsAvailable()) EF_eb_physics_unpaired_noniso.SetActive(active);
     if(EF_eb_random.IsAvailable()) EF_eb_random.SetActive(active);
     if(EF_eb_random_empty.IsAvailable()) EF_eb_random_empty.SetActive(active);
     if(EF_eb_random_firstempty.IsAvailable()) EF_eb_random_firstempty.SetActive(active);
     if(EF_eb_random_unpaired_iso.IsAvailable()) EF_eb_random_unpaired_iso.SetActive(active);
     if(n.IsAvailable()) n.SetActive(active);
     if(E.IsAvailable()) E.SetActive(active);
     if(Et.IsAvailable()) Et.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(m.IsAvailable()) m.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(px.IsAvailable()) px.SetActive(active);
     if(py.IsAvailable()) py.SetActive(active);
     if(pz.IsAvailable()) pz.SetActive(active);
     if(charge.IsAvailable()) charge.SetActive(active);
     if(author.IsAvailable()) author.SetActive(active);
     if(isEM.IsAvailable()) isEM.SetActive(active);
     if(isEMLoose.IsAvailable()) isEMLoose.SetActive(active);
     if(isEMMedium.IsAvailable()) isEMMedium.SetActive(active);
     if(isEMTight.IsAvailable()) isEMTight.SetActive(active);
     if(loose.IsAvailable()) loose.SetActive(active);
     if(looseIso.IsAvailable()) looseIso.SetActive(active);
     if(medium.IsAvailable()) medium.SetActive(active);
     if(mediumIso.IsAvailable()) mediumIso.SetActive(active);
     if(mediumWithoutTrack.IsAvailable()) mediumWithoutTrack.SetActive(active);
     if(mediumIsoWithoutTrack.IsAvailable()) mediumIsoWithoutTrack.SetActive(active);
     if(tight.IsAvailable()) tight.SetActive(active);
     if(tightIso.IsAvailable()) tightIso.SetActive(active);
     if(tightWithoutTrack.IsAvailable()) tightWithoutTrack.SetActive(active);
     if(tightIsoWithoutTrack.IsAvailable()) tightIsoWithoutTrack.SetActive(active);
     if(loosePP.IsAvailable()) loosePP.SetActive(active);
     if(loosePPIso.IsAvailable()) loosePPIso.SetActive(active);
     if(mediumPP.IsAvailable()) mediumPP.SetActive(active);
     if(mediumPPIso.IsAvailable()) mediumPPIso.SetActive(active);
     if(tightPP.IsAvailable()) tightPP.SetActive(active);
     if(tightPPIso.IsAvailable()) tightPPIso.SetActive(active);
     if(vertweight.IsAvailable()) vertweight.SetActive(active);
     if(hastrack.IsAvailable()) hastrack.SetActive(active);
    }
    else
    {
      EF_2e12Tvh_loose1.SetActive(active);
      EF_2e5_tight1_Jpsi.SetActive(active);
      EF_2e7T_loose1_mu6.SetActive(active);
      EF_2e7T_medium1_mu6.SetActive(active);
      EF_e11_etcut.SetActive(active);
      EF_e12Tvh_loose1.SetActive(active);
      EF_e12Tvh_loose1_mu8.SetActive(active);
      EF_e12Tvh_medium1.SetActive(active);
      EF_e12Tvh_medium1_mu10.SetActive(active);
      EF_e12Tvh_medium1_mu6.SetActive(active);
      EF_e12Tvh_medium1_mu6_topo_medium.SetActive(active);
      EF_e12Tvh_medium1_mu8.SetActive(active);
      EF_e13_etcutTrk_xs60.SetActive(active);
      EF_e13_etcutTrk_xs60_dphi2j15xe20.SetActive(active);
      EF_e14_tight1_e4_etcut_Jpsi.SetActive(active);
      EF_e15vh_medium1.SetActive(active);
      EF_e18_loose1.SetActive(active);
      EF_e18_loose1_g25_medium.SetActive(active);
      EF_e18_loose1_g35_loose.SetActive(active);
      EF_e18_loose1_g35_medium.SetActive(active);
      EF_e18_medium1.SetActive(active);
      EF_e18_medium1_g25_loose.SetActive(active);
      EF_e18_medium1_g25_medium.SetActive(active);
      EF_e18_medium1_g35_loose.SetActive(active);
      EF_e18_medium1_g35_medium.SetActive(active);
      EF_e18vh_medium1.SetActive(active);
      EF_e18vh_medium1_2e7T_medium1.SetActive(active);
      EF_e20_etcutTrk_xe30_dphi2j15xe20.SetActive(active);
      EF_e20_etcutTrk_xs60_dphi2j15xe20.SetActive(active);
      EF_e20vhT_medium1_g6T_etcut_Upsi.SetActive(active);
      EF_e20vhT_tight1_g6T_etcut_Upsi.SetActive(active);
      EF_e22vh_loose.SetActive(active);
      EF_e22vh_loose0.SetActive(active);
      EF_e22vh_loose1.SetActive(active);
      EF_e22vh_medium1.SetActive(active);
      EF_e22vh_medium1_IDTrkNoCut.SetActive(active);
      EF_e22vh_medium1_IdScan.SetActive(active);
      EF_e22vh_medium1_SiTrk.SetActive(active);
      EF_e22vh_medium1_TRT.SetActive(active);
      EF_e22vhi_medium1.SetActive(active);
      EF_e24vh_loose.SetActive(active);
      EF_e24vh_loose0.SetActive(active);
      EF_e24vh_loose1.SetActive(active);
      EF_e24vh_medium1.SetActive(active);
      EF_e24vh_medium1_EFxe30.SetActive(active);
      EF_e24vh_medium1_EFxe30_tcem.SetActive(active);
      EF_e24vh_medium1_EFxe35_tcem.SetActive(active);
      EF_e24vh_medium1_EFxe35_tclcw.SetActive(active);
      EF_e24vh_medium1_EFxe40.SetActive(active);
      EF_e24vh_medium1_IDTrkNoCut.SetActive(active);
      EF_e24vh_medium1_IdScan.SetActive(active);
      EF_e24vh_medium1_L2StarB.SetActive(active);
      EF_e24vh_medium1_L2StarC.SetActive(active);
      EF_e24vh_medium1_SiTrk.SetActive(active);
      EF_e24vh_medium1_TRT.SetActive(active);
      EF_e24vh_medium1_b35_mediumEF_j35_a4tchad.SetActive(active);
      EF_e24vh_medium1_e7_medium1.SetActive(active);
      EF_e24vh_tight1_e15_NoCut_Zee.SetActive(active);
      EF_e24vhi_loose1_mu8.SetActive(active);
      EF_e24vhi_medium1.SetActive(active);
      EF_e45_etcut.SetActive(active);
      EF_e45_medium1.SetActive(active);
      EF_e5_tight1.SetActive(active);
      EF_e5_tight1_e14_etcut_Jpsi.SetActive(active);
      EF_e5_tight1_e4_etcut_Jpsi.SetActive(active);
      EF_e5_tight1_e4_etcut_Jpsi_IdScan.SetActive(active);
      EF_e5_tight1_e4_etcut_Jpsi_L2StarB.SetActive(active);
      EF_e5_tight1_e4_etcut_Jpsi_L2StarC.SetActive(active);
      EF_e5_tight1_e4_etcut_Jpsi_SiTrk.SetActive(active);
      EF_e5_tight1_e4_etcut_Jpsi_TRT.SetActive(active);
      EF_e5_tight1_e5_NoCut.SetActive(active);
      EF_e5_tight1_e9_etcut_Jpsi.SetActive(active);
      EF_e60_etcut.SetActive(active);
      EF_e60_medium1.SetActive(active);
      EF_e7T_loose1.SetActive(active);
      EF_e7T_loose1_2mu6.SetActive(active);
      EF_e7T_medium1.SetActive(active);
      EF_e7T_medium1_2mu6.SetActive(active);
      EF_e9_tight1_e4_etcut_Jpsi.SetActive(active);
      EF_eb_physics.SetActive(active);
      EF_eb_physics_empty.SetActive(active);
      EF_eb_physics_firstempty.SetActive(active);
      EF_eb_physics_noL1PS.SetActive(active);
      EF_eb_physics_unpaired_iso.SetActive(active);
      EF_eb_physics_unpaired_noniso.SetActive(active);
      EF_eb_random.SetActive(active);
      EF_eb_random_empty.SetActive(active);
      EF_eb_random_firstempty.SetActive(active);
      EF_eb_random_unpaired_iso.SetActive(active);
      n.SetActive(active);
      E.SetActive(active);
      Et.SetActive(active);
      pt.SetActive(active);
      m.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      px.SetActive(active);
      py.SetActive(active);
      pz.SetActive(active);
      charge.SetActive(active);
      author.SetActive(active);
      isEM.SetActive(active);
      isEMLoose.SetActive(active);
      isEMMedium.SetActive(active);
      isEMTight.SetActive(active);
      loose.SetActive(active);
      looseIso.SetActive(active);
      medium.SetActive(active);
      mediumIso.SetActive(active);
      mediumWithoutTrack.SetActive(active);
      mediumIsoWithoutTrack.SetActive(active);
      tight.SetActive(active);
      tightIso.SetActive(active);
      tightWithoutTrack.SetActive(active);
      tightIsoWithoutTrack.SetActive(active);
      loosePP.SetActive(active);
      loosePPIso.SetActive(active);
      mediumPP.SetActive(active);
      mediumPPIso.SetActive(active);
      tightPP.SetActive(active);
      tightPPIso.SetActive(active);
      vertweight.SetActive(active);
      hastrack.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TrigEFElectronD3PDCollection::ReadAllActive()
  {
    if(EF_2e12Tvh_loose1.IsActive()) EF_2e12Tvh_loose1();
    if(EF_2e5_tight1_Jpsi.IsActive()) EF_2e5_tight1_Jpsi();
    if(EF_2e7T_loose1_mu6.IsActive()) EF_2e7T_loose1_mu6();
    if(EF_2e7T_medium1_mu6.IsActive()) EF_2e7T_medium1_mu6();
    if(EF_e11_etcut.IsActive()) EF_e11_etcut();
    if(EF_e12Tvh_loose1.IsActive()) EF_e12Tvh_loose1();
    if(EF_e12Tvh_loose1_mu8.IsActive()) EF_e12Tvh_loose1_mu8();
    if(EF_e12Tvh_medium1.IsActive()) EF_e12Tvh_medium1();
    if(EF_e12Tvh_medium1_mu10.IsActive()) EF_e12Tvh_medium1_mu10();
    if(EF_e12Tvh_medium1_mu6.IsActive()) EF_e12Tvh_medium1_mu6();
    if(EF_e12Tvh_medium1_mu6_topo_medium.IsActive()) EF_e12Tvh_medium1_mu6_topo_medium();
    if(EF_e12Tvh_medium1_mu8.IsActive()) EF_e12Tvh_medium1_mu8();
    if(EF_e13_etcutTrk_xs60.IsActive()) EF_e13_etcutTrk_xs60();
    if(EF_e13_etcutTrk_xs60_dphi2j15xe20.IsActive()) EF_e13_etcutTrk_xs60_dphi2j15xe20();
    if(EF_e14_tight1_e4_etcut_Jpsi.IsActive()) EF_e14_tight1_e4_etcut_Jpsi();
    if(EF_e15vh_medium1.IsActive()) EF_e15vh_medium1();
    if(EF_e18_loose1.IsActive()) EF_e18_loose1();
    if(EF_e18_loose1_g25_medium.IsActive()) EF_e18_loose1_g25_medium();
    if(EF_e18_loose1_g35_loose.IsActive()) EF_e18_loose1_g35_loose();
    if(EF_e18_loose1_g35_medium.IsActive()) EF_e18_loose1_g35_medium();
    if(EF_e18_medium1.IsActive()) EF_e18_medium1();
    if(EF_e18_medium1_g25_loose.IsActive()) EF_e18_medium1_g25_loose();
    if(EF_e18_medium1_g25_medium.IsActive()) EF_e18_medium1_g25_medium();
    if(EF_e18_medium1_g35_loose.IsActive()) EF_e18_medium1_g35_loose();
    if(EF_e18_medium1_g35_medium.IsActive()) EF_e18_medium1_g35_medium();
    if(EF_e18vh_medium1.IsActive()) EF_e18vh_medium1();
    if(EF_e18vh_medium1_2e7T_medium1.IsActive()) EF_e18vh_medium1_2e7T_medium1();
    if(EF_e20_etcutTrk_xe30_dphi2j15xe20.IsActive()) EF_e20_etcutTrk_xe30_dphi2j15xe20();
    if(EF_e20_etcutTrk_xs60_dphi2j15xe20.IsActive()) EF_e20_etcutTrk_xs60_dphi2j15xe20();
    if(EF_e20vhT_medium1_g6T_etcut_Upsi.IsActive()) EF_e20vhT_medium1_g6T_etcut_Upsi();
    if(EF_e20vhT_tight1_g6T_etcut_Upsi.IsActive()) EF_e20vhT_tight1_g6T_etcut_Upsi();
    if(EF_e22vh_loose.IsActive()) EF_e22vh_loose();
    if(EF_e22vh_loose0.IsActive()) EF_e22vh_loose0();
    if(EF_e22vh_loose1.IsActive()) EF_e22vh_loose1();
    if(EF_e22vh_medium1.IsActive()) EF_e22vh_medium1();
    if(EF_e22vh_medium1_IDTrkNoCut.IsActive()) EF_e22vh_medium1_IDTrkNoCut();
    if(EF_e22vh_medium1_IdScan.IsActive()) EF_e22vh_medium1_IdScan();
    if(EF_e22vh_medium1_SiTrk.IsActive()) EF_e22vh_medium1_SiTrk();
    if(EF_e22vh_medium1_TRT.IsActive()) EF_e22vh_medium1_TRT();
    if(EF_e22vhi_medium1.IsActive()) EF_e22vhi_medium1();
    if(EF_e24vh_loose.IsActive()) EF_e24vh_loose();
    if(EF_e24vh_loose0.IsActive()) EF_e24vh_loose0();
    if(EF_e24vh_loose1.IsActive()) EF_e24vh_loose1();
    if(EF_e24vh_medium1.IsActive()) EF_e24vh_medium1();
    if(EF_e24vh_medium1_EFxe30.IsActive()) EF_e24vh_medium1_EFxe30();
    if(EF_e24vh_medium1_EFxe30_tcem.IsActive()) EF_e24vh_medium1_EFxe30_tcem();
    if(EF_e24vh_medium1_EFxe35_tcem.IsActive()) EF_e24vh_medium1_EFxe35_tcem();
    if(EF_e24vh_medium1_EFxe35_tclcw.IsActive()) EF_e24vh_medium1_EFxe35_tclcw();
    if(EF_e24vh_medium1_EFxe40.IsActive()) EF_e24vh_medium1_EFxe40();
    if(EF_e24vh_medium1_IDTrkNoCut.IsActive()) EF_e24vh_medium1_IDTrkNoCut();
    if(EF_e24vh_medium1_IdScan.IsActive()) EF_e24vh_medium1_IdScan();
    if(EF_e24vh_medium1_L2StarB.IsActive()) EF_e24vh_medium1_L2StarB();
    if(EF_e24vh_medium1_L2StarC.IsActive()) EF_e24vh_medium1_L2StarC();
    if(EF_e24vh_medium1_SiTrk.IsActive()) EF_e24vh_medium1_SiTrk();
    if(EF_e24vh_medium1_TRT.IsActive()) EF_e24vh_medium1_TRT();
    if(EF_e24vh_medium1_b35_mediumEF_j35_a4tchad.IsActive()) EF_e24vh_medium1_b35_mediumEF_j35_a4tchad();
    if(EF_e24vh_medium1_e7_medium1.IsActive()) EF_e24vh_medium1_e7_medium1();
    if(EF_e24vh_tight1_e15_NoCut_Zee.IsActive()) EF_e24vh_tight1_e15_NoCut_Zee();
    if(EF_e24vhi_loose1_mu8.IsActive()) EF_e24vhi_loose1_mu8();
    if(EF_e24vhi_medium1.IsActive()) EF_e24vhi_medium1();
    if(EF_e45_etcut.IsActive()) EF_e45_etcut();
    if(EF_e45_medium1.IsActive()) EF_e45_medium1();
    if(EF_e5_tight1.IsActive()) EF_e5_tight1();
    if(EF_e5_tight1_e14_etcut_Jpsi.IsActive()) EF_e5_tight1_e14_etcut_Jpsi();
    if(EF_e5_tight1_e4_etcut_Jpsi.IsActive()) EF_e5_tight1_e4_etcut_Jpsi();
    if(EF_e5_tight1_e4_etcut_Jpsi_IdScan.IsActive()) EF_e5_tight1_e4_etcut_Jpsi_IdScan();
    if(EF_e5_tight1_e4_etcut_Jpsi_L2StarB.IsActive()) EF_e5_tight1_e4_etcut_Jpsi_L2StarB();
    if(EF_e5_tight1_e4_etcut_Jpsi_L2StarC.IsActive()) EF_e5_tight1_e4_etcut_Jpsi_L2StarC();
    if(EF_e5_tight1_e4_etcut_Jpsi_SiTrk.IsActive()) EF_e5_tight1_e4_etcut_Jpsi_SiTrk();
    if(EF_e5_tight1_e4_etcut_Jpsi_TRT.IsActive()) EF_e5_tight1_e4_etcut_Jpsi_TRT();
    if(EF_e5_tight1_e5_NoCut.IsActive()) EF_e5_tight1_e5_NoCut();
    if(EF_e5_tight1_e9_etcut_Jpsi.IsActive()) EF_e5_tight1_e9_etcut_Jpsi();
    if(EF_e60_etcut.IsActive()) EF_e60_etcut();
    if(EF_e60_medium1.IsActive()) EF_e60_medium1();
    if(EF_e7T_loose1.IsActive()) EF_e7T_loose1();
    if(EF_e7T_loose1_2mu6.IsActive()) EF_e7T_loose1_2mu6();
    if(EF_e7T_medium1.IsActive()) EF_e7T_medium1();
    if(EF_e7T_medium1_2mu6.IsActive()) EF_e7T_medium1_2mu6();
    if(EF_e9_tight1_e4_etcut_Jpsi.IsActive()) EF_e9_tight1_e4_etcut_Jpsi();
    if(EF_eb_physics.IsActive()) EF_eb_physics();
    if(EF_eb_physics_empty.IsActive()) EF_eb_physics_empty();
    if(EF_eb_physics_firstempty.IsActive()) EF_eb_physics_firstempty();
    if(EF_eb_physics_noL1PS.IsActive()) EF_eb_physics_noL1PS();
    if(EF_eb_physics_unpaired_iso.IsActive()) EF_eb_physics_unpaired_iso();
    if(EF_eb_physics_unpaired_noniso.IsActive()) EF_eb_physics_unpaired_noniso();
    if(EF_eb_random.IsActive()) EF_eb_random();
    if(EF_eb_random_empty.IsActive()) EF_eb_random_empty();
    if(EF_eb_random_firstempty.IsActive()) EF_eb_random_firstempty();
    if(EF_eb_random_unpaired_iso.IsActive()) EF_eb_random_unpaired_iso();
    if(n.IsActive()) n();
    if(E.IsActive()) E();
    if(Et.IsActive()) Et();
    if(pt.IsActive()) pt();
    if(m.IsActive()) m();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(px.IsActive()) px();
    if(py.IsActive()) py();
    if(pz.IsActive()) pz();
    if(charge.IsActive()) charge();
    if(author.IsActive()) author();
    if(isEM.IsActive()) isEM();
    if(isEMLoose.IsActive()) isEMLoose();
    if(isEMMedium.IsActive()) isEMMedium();
    if(isEMTight.IsActive()) isEMTight();
    if(loose.IsActive()) loose();
    if(looseIso.IsActive()) looseIso();
    if(medium.IsActive()) medium();
    if(mediumIso.IsActive()) mediumIso();
    if(mediumWithoutTrack.IsActive()) mediumWithoutTrack();
    if(mediumIsoWithoutTrack.IsActive()) mediumIsoWithoutTrack();
    if(tight.IsActive()) tight();
    if(tightIso.IsActive()) tightIso();
    if(tightWithoutTrack.IsActive()) tightWithoutTrack();
    if(tightIsoWithoutTrack.IsActive()) tightIsoWithoutTrack();
    if(loosePP.IsActive()) loosePP();
    if(loosePPIso.IsActive()) loosePPIso();
    if(mediumPP.IsActive()) mediumPP();
    if(mediumPPIso.IsActive()) mediumPPIso();
    if(tightPP.IsActive()) tightPP();
    if(tightPPIso.IsActive()) tightPPIso();
    if(vertweight.IsActive()) vertweight();
    if(hastrack.IsActive()) hastrack();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TrigEFElectronD3PDCollection_CXX
