// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_MuonD3PDCollection_CXX
#define D3PDREADER_MuonD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "MuonD3PDCollection.h"

ClassImp(D3PDReader::MuonD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  MuonD3PDCollection::MuonD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    MET_n(prefix + "MET_n",&master),
    MET_wpx(prefix + "MET_wpx",&master),
    MET_wpy(prefix + "MET_wpy",&master),
    MET_wet(prefix + "MET_wet",&master),
    MET_statusWord(prefix + "MET_statusWord",&master),
    MET_BDTMedium_n(prefix + "MET_BDTMedium_n",&master),
    MET_BDTMedium_wpx(prefix + "MET_BDTMedium_wpx",&master),
    MET_BDTMedium_wpy(prefix + "MET_BDTMedium_wpy",&master),
    MET_BDTMedium_wet(prefix + "MET_BDTMedium_wet",&master),
    MET_BDTMedium_statusWord(prefix + "MET_BDTMedium_statusWord",&master),
    MET_AntiKt4LCTopo_tightpp_n(prefix + "MET_AntiKt4LCTopo_tightpp_n",&master),
    MET_AntiKt4LCTopo_tightpp_wpx(prefix + "MET_AntiKt4LCTopo_tightpp_wpx",&master),
    MET_AntiKt4LCTopo_tightpp_wpy(prefix + "MET_AntiKt4LCTopo_tightpp_wpy",&master),
    MET_AntiKt4LCTopo_tightpp_wet(prefix + "MET_AntiKt4LCTopo_tightpp_wet",&master),
    MET_AntiKt4LCTopo_tightpp_statusWord(prefix + "MET_AntiKt4LCTopo_tightpp_statusWord",&master),
    n(prefix + "n",&master),
    E(prefix + "E",&master),
    pt(prefix + "pt",&master),
    m(prefix + "m",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    px(prefix + "px",&master),
    py(prefix + "py",&master),
    pz(prefix + "pz",&master),
    charge(prefix + "charge",&master),
    allauthor(prefix + "allauthor",&master),
    author(prefix + "author",&master),
    beta(prefix + "beta",&master),
    isMuonLikelihood(prefix + "isMuonLikelihood",&master),
    matchchi2(prefix + "matchchi2",&master),
    matchndof(prefix + "matchndof",&master),
    etcone20(prefix + "etcone20",&master),
    etcone30(prefix + "etcone30",&master),
    etcone40(prefix + "etcone40",&master),
    nucone20(prefix + "nucone20",&master),
    nucone30(prefix + "nucone30",&master),
    nucone40(prefix + "nucone40",&master),
    ptcone20(prefix + "ptcone20",&master),
    ptcone30(prefix + "ptcone30",&master),
    ptcone40(prefix + "ptcone40",&master),
    etconeNoEm10(prefix + "etconeNoEm10",&master),
    etconeNoEm20(prefix + "etconeNoEm20",&master),
    etconeNoEm30(prefix + "etconeNoEm30",&master),
    etconeNoEm40(prefix + "etconeNoEm40",&master),
    momentumBalanceSignificance(prefix + "momentumBalanceSignificance",&master),
    energyLossPar(prefix + "energyLossPar",&master),
    energyLossErr(prefix + "energyLossErr",&master),
    etCore(prefix + "etCore",&master),
    energyLossType(prefix + "energyLossType",&master),
    caloMuonIdTag(prefix + "caloMuonIdTag",&master),
    caloLRLikelihood(prefix + "caloLRLikelihood",&master),
    bestMatch(prefix + "bestMatch",&master),
    isStandAloneMuon(prefix + "isStandAloneMuon",&master),
    isCombinedMuon(prefix + "isCombinedMuon",&master),
    isLowPtReconstructedMuon(prefix + "isLowPtReconstructedMuon",&master),
    isSegmentTaggedMuon(prefix + "isSegmentTaggedMuon",&master),
    isCaloMuonId(prefix + "isCaloMuonId",&master),
    alsoFoundByLowPt(prefix + "alsoFoundByLowPt",&master),
    alsoFoundByCaloMuonId(prefix + "alsoFoundByCaloMuonId",&master),
    isSiliconAssociatedForwardMuon(prefix + "isSiliconAssociatedForwardMuon",&master),
    loose(prefix + "loose",&master),
    medium(prefix + "medium",&master),
    tight(prefix + "tight",&master),
    d0_exPV(prefix + "d0_exPV",&master),
    z0_exPV(prefix + "z0_exPV",&master),
    phi_exPV(prefix + "phi_exPV",&master),
    theta_exPV(prefix + "theta_exPV",&master),
    qoverp_exPV(prefix + "qoverp_exPV",&master),
    cb_d0_exPV(prefix + "cb_d0_exPV",&master),
    cb_z0_exPV(prefix + "cb_z0_exPV",&master),
    cb_phi_exPV(prefix + "cb_phi_exPV",&master),
    cb_theta_exPV(prefix + "cb_theta_exPV",&master),
    cb_qoverp_exPV(prefix + "cb_qoverp_exPV",&master),
    id_d0_exPV(prefix + "id_d0_exPV",&master),
    id_z0_exPV(prefix + "id_z0_exPV",&master),
    id_phi_exPV(prefix + "id_phi_exPV",&master),
    id_theta_exPV(prefix + "id_theta_exPV",&master),
    id_qoverp_exPV(prefix + "id_qoverp_exPV",&master),
    me_d0_exPV(prefix + "me_d0_exPV",&master),
    me_z0_exPV(prefix + "me_z0_exPV",&master),
    me_phi_exPV(prefix + "me_phi_exPV",&master),
    me_theta_exPV(prefix + "me_theta_exPV",&master),
    me_qoverp_exPV(prefix + "me_qoverp_exPV",&master),
    cov_d0_exPV(prefix + "cov_d0_exPV",&master),
    cov_z0_exPV(prefix + "cov_z0_exPV",&master),
    cov_phi_exPV(prefix + "cov_phi_exPV",&master),
    cov_theta_exPV(prefix + "cov_theta_exPV",&master),
    cov_qoverp_exPV(prefix + "cov_qoverp_exPV",&master),
    cov_d0_z0_exPV(prefix + "cov_d0_z0_exPV",&master),
    cov_d0_phi_exPV(prefix + "cov_d0_phi_exPV",&master),
    cov_d0_theta_exPV(prefix + "cov_d0_theta_exPV",&master),
    cov_d0_qoverp_exPV(prefix + "cov_d0_qoverp_exPV",&master),
    cov_z0_phi_exPV(prefix + "cov_z0_phi_exPV",&master),
    cov_z0_theta_exPV(prefix + "cov_z0_theta_exPV",&master),
    cov_z0_qoverp_exPV(prefix + "cov_z0_qoverp_exPV",&master),
    cov_phi_theta_exPV(prefix + "cov_phi_theta_exPV",&master),
    cov_phi_qoverp_exPV(prefix + "cov_phi_qoverp_exPV",&master),
    cov_theta_qoverp_exPV(prefix + "cov_theta_qoverp_exPV",&master),
    id_cov_d0_exPV(prefix + "id_cov_d0_exPV",&master),
    id_cov_z0_exPV(prefix + "id_cov_z0_exPV",&master),
    id_cov_phi_exPV(prefix + "id_cov_phi_exPV",&master),
    id_cov_theta_exPV(prefix + "id_cov_theta_exPV",&master),
    id_cov_qoverp_exPV(prefix + "id_cov_qoverp_exPV",&master),
    id_cov_d0_z0_exPV(prefix + "id_cov_d0_z0_exPV",&master),
    id_cov_d0_phi_exPV(prefix + "id_cov_d0_phi_exPV",&master),
    id_cov_d0_theta_exPV(prefix + "id_cov_d0_theta_exPV",&master),
    id_cov_d0_qoverp_exPV(prefix + "id_cov_d0_qoverp_exPV",&master),
    id_cov_z0_phi_exPV(prefix + "id_cov_z0_phi_exPV",&master),
    id_cov_z0_theta_exPV(prefix + "id_cov_z0_theta_exPV",&master),
    id_cov_z0_qoverp_exPV(prefix + "id_cov_z0_qoverp_exPV",&master),
    id_cov_phi_theta_exPV(prefix + "id_cov_phi_theta_exPV",&master),
    id_cov_phi_qoverp_exPV(prefix + "id_cov_phi_qoverp_exPV",&master),
    id_cov_theta_qoverp_exPV(prefix + "id_cov_theta_qoverp_exPV",&master),
    me_cov_d0_exPV(prefix + "me_cov_d0_exPV",&master),
    me_cov_z0_exPV(prefix + "me_cov_z0_exPV",&master),
    me_cov_phi_exPV(prefix + "me_cov_phi_exPV",&master),
    me_cov_theta_exPV(prefix + "me_cov_theta_exPV",&master),
    me_cov_qoverp_exPV(prefix + "me_cov_qoverp_exPV",&master),
    me_cov_d0_z0_exPV(prefix + "me_cov_d0_z0_exPV",&master),
    me_cov_d0_phi_exPV(prefix + "me_cov_d0_phi_exPV",&master),
    me_cov_d0_theta_exPV(prefix + "me_cov_d0_theta_exPV",&master),
    me_cov_d0_qoverp_exPV(prefix + "me_cov_d0_qoverp_exPV",&master),
    me_cov_z0_phi_exPV(prefix + "me_cov_z0_phi_exPV",&master),
    me_cov_z0_theta_exPV(prefix + "me_cov_z0_theta_exPV",&master),
    me_cov_z0_qoverp_exPV(prefix + "me_cov_z0_qoverp_exPV",&master),
    me_cov_phi_theta_exPV(prefix + "me_cov_phi_theta_exPV",&master),
    me_cov_phi_qoverp_exPV(prefix + "me_cov_phi_qoverp_exPV",&master),
    me_cov_theta_qoverp_exPV(prefix + "me_cov_theta_qoverp_exPV",&master),
    ms_d0(prefix + "ms_d0",&master),
    ms_z0(prefix + "ms_z0",&master),
    ms_phi(prefix + "ms_phi",&master),
    ms_theta(prefix + "ms_theta",&master),
    ms_qoverp(prefix + "ms_qoverp",&master),
    id_d0(prefix + "id_d0",&master),
    id_z0(prefix + "id_z0",&master),
    id_phi(prefix + "id_phi",&master),
    id_theta(prefix + "id_theta",&master),
    id_qoverp(prefix + "id_qoverp",&master),
    me_d0(prefix + "me_d0",&master),
    me_z0(prefix + "me_z0",&master),
    me_phi(prefix + "me_phi",&master),
    me_theta(prefix + "me_theta",&master),
    me_qoverp(prefix + "me_qoverp",&master),
    nOutliersOnTrack(prefix + "nOutliersOnTrack",&master),
    nBLHits(prefix + "nBLHits",&master),
    nPixHits(prefix + "nPixHits",&master),
    nSCTHits(prefix + "nSCTHits",&master),
    nTRTHits(prefix + "nTRTHits",&master),
    nTRTHighTHits(prefix + "nTRTHighTHits",&master),
    nBLSharedHits(prefix + "nBLSharedHits",&master),
    nPixSharedHits(prefix + "nPixSharedHits",&master),
    nPixHoles(prefix + "nPixHoles",&master),
    nSCTSharedHits(prefix + "nSCTSharedHits",&master),
    nSCTHoles(prefix + "nSCTHoles",&master),
    nTRTOutliers(prefix + "nTRTOutliers",&master),
    nTRTHighTOutliers(prefix + "nTRTHighTOutliers",&master),
    nGangedPixels(prefix + "nGangedPixels",&master),
    nPixelDeadSensors(prefix + "nPixelDeadSensors",&master),
    nSCTDeadSensors(prefix + "nSCTDeadSensors",&master),
    nTRTDeadStraws(prefix + "nTRTDeadStraws",&master),
    expectBLayerHit(prefix + "expectBLayerHit",&master),
    nMDTHits(prefix + "nMDTHits",&master),
    nMDTHoles(prefix + "nMDTHoles",&master),
    nCSCEtaHits(prefix + "nCSCEtaHits",&master),
    nCSCEtaHoles(prefix + "nCSCEtaHoles",&master),
    nCSCUnspoiledEtaHits(prefix + "nCSCUnspoiledEtaHits",&master),
    nCSCPhiHits(prefix + "nCSCPhiHits",&master),
    nCSCPhiHoles(prefix + "nCSCPhiHoles",&master),
    nRPCEtaHits(prefix + "nRPCEtaHits",&master),
    nRPCEtaHoles(prefix + "nRPCEtaHoles",&master),
    nRPCPhiHits(prefix + "nRPCPhiHits",&master),
    nRPCPhiHoles(prefix + "nRPCPhiHoles",&master),
    nTGCEtaHits(prefix + "nTGCEtaHits",&master),
    nTGCEtaHoles(prefix + "nTGCEtaHoles",&master),
    nTGCPhiHits(prefix + "nTGCPhiHits",&master),
    nTGCPhiHoles(prefix + "nTGCPhiHoles",&master),
    nprecisionLayers(prefix + "nprecisionLayers",&master),
    nprecisionHoleLayers(prefix + "nprecisionHoleLayers",&master),
    nphiLayers(prefix + "nphiLayers",&master),
    ntrigEtaLayers(prefix + "ntrigEtaLayers",&master),
    nphiHoleLayers(prefix + "nphiHoleLayers",&master),
    ntrigEtaHoleLayers(prefix + "ntrigEtaHoleLayers",&master),
    nMDTBIHits(prefix + "nMDTBIHits",&master),
    nMDTBMHits(prefix + "nMDTBMHits",&master),
    nMDTBOHits(prefix + "nMDTBOHits",&master),
    nMDTBEEHits(prefix + "nMDTBEEHits",&master),
    nMDTBIS78Hits(prefix + "nMDTBIS78Hits",&master),
    nMDTEIHits(prefix + "nMDTEIHits",&master),
    nMDTEMHits(prefix + "nMDTEMHits",&master),
    nMDTEOHits(prefix + "nMDTEOHits",&master),
    nMDTEEHits(prefix + "nMDTEEHits",&master),
    nRPCLayer1EtaHits(prefix + "nRPCLayer1EtaHits",&master),
    nRPCLayer2EtaHits(prefix + "nRPCLayer2EtaHits",&master),
    nRPCLayer3EtaHits(prefix + "nRPCLayer3EtaHits",&master),
    nRPCLayer1PhiHits(prefix + "nRPCLayer1PhiHits",&master),
    nRPCLayer2PhiHits(prefix + "nRPCLayer2PhiHits",&master),
    nRPCLayer3PhiHits(prefix + "nRPCLayer3PhiHits",&master),
    nTGCLayer1EtaHits(prefix + "nTGCLayer1EtaHits",&master),
    nTGCLayer2EtaHits(prefix + "nTGCLayer2EtaHits",&master),
    nTGCLayer3EtaHits(prefix + "nTGCLayer3EtaHits",&master),
    nTGCLayer4EtaHits(prefix + "nTGCLayer4EtaHits",&master),
    nTGCLayer1PhiHits(prefix + "nTGCLayer1PhiHits",&master),
    nTGCLayer2PhiHits(prefix + "nTGCLayer2PhiHits",&master),
    nTGCLayer3PhiHits(prefix + "nTGCLayer3PhiHits",&master),
    nTGCLayer4PhiHits(prefix + "nTGCLayer4PhiHits",&master),
    barrelSectors(prefix + "barrelSectors",&master),
    endcapSectors(prefix + "endcapSectors",&master),
    spec_surf_px(prefix + "spec_surf_px",&master),
    spec_surf_py(prefix + "spec_surf_py",&master),
    spec_surf_pz(prefix + "spec_surf_pz",&master),
    spec_surf_x(prefix + "spec_surf_x",&master),
    spec_surf_y(prefix + "spec_surf_y",&master),
    spec_surf_z(prefix + "spec_surf_z",&master),
    trackd0(prefix + "trackd0",&master),
    trackz0(prefix + "trackz0",&master),
    trackphi(prefix + "trackphi",&master),
    tracktheta(prefix + "tracktheta",&master),
    trackqoverp(prefix + "trackqoverp",&master),
    trackcov_d0(prefix + "trackcov_d0",&master),
    trackcov_z0(prefix + "trackcov_z0",&master),
    trackcov_phi(prefix + "trackcov_phi",&master),
    trackcov_theta(prefix + "trackcov_theta",&master),
    trackcov_qoverp(prefix + "trackcov_qoverp",&master),
    trackcov_d0_z0(prefix + "trackcov_d0_z0",&master),
    trackcov_d0_phi(prefix + "trackcov_d0_phi",&master),
    trackcov_d0_theta(prefix + "trackcov_d0_theta",&master),
    trackcov_d0_qoverp(prefix + "trackcov_d0_qoverp",&master),
    trackcov_z0_phi(prefix + "trackcov_z0_phi",&master),
    trackcov_z0_theta(prefix + "trackcov_z0_theta",&master),
    trackcov_z0_qoverp(prefix + "trackcov_z0_qoverp",&master),
    trackcov_phi_theta(prefix + "trackcov_phi_theta",&master),
    trackcov_phi_qoverp(prefix + "trackcov_phi_qoverp",&master),
    trackcov_theta_qoverp(prefix + "trackcov_theta_qoverp",&master),
    trackfitchi2(prefix + "trackfitchi2",&master),
    trackfitndof(prefix + "trackfitndof",&master),
    hastrack(prefix + "hastrack",&master),
    trackd0beam(prefix + "trackd0beam",&master),
    trackz0beam(prefix + "trackz0beam",&master),
    tracksigd0beam(prefix + "tracksigd0beam",&master),
    tracksigz0beam(prefix + "tracksigz0beam",&master),
    trackd0pv(prefix + "trackd0pv",&master),
    trackz0pv(prefix + "trackz0pv",&master),
    tracksigd0pv(prefix + "tracksigd0pv",&master),
    tracksigz0pv(prefix + "tracksigz0pv",&master),
    trackd0pvunbiased(prefix + "trackd0pvunbiased",&master),
    trackz0pvunbiased(prefix + "trackz0pvunbiased",&master),
    tracksigd0pvunbiased(prefix + "tracksigd0pvunbiased",&master),
    tracksigz0pvunbiased(prefix + "tracksigz0pvunbiased",&master),
    type(prefix + "type",&master),
    origin(prefix + "origin",&master),
    truth_dr(prefix + "truth_dr",&master),
    truth_E(prefix + "truth_E",&master),
    truth_pt(prefix + "truth_pt",&master),
    truth_eta(prefix + "truth_eta",&master),
    truth_phi(prefix + "truth_phi",&master),
    truth_type(prefix + "truth_type",&master),
    truth_status(prefix + "truth_status",&master),
    truth_barcode(prefix + "truth_barcode",&master),
    truth_mothertype(prefix + "truth_mothertype",&master),
    truth_motherbarcode(prefix + "truth_motherbarcode",&master),
    truth_matched(prefix + "truth_matched",&master),
    L2CB_dr(prefix + "L2CB_dr",&master),
    L2CB_pt(prefix + "L2CB_pt",&master),
    L2CB_eta(prefix + "L2CB_eta",&master),
    L2CB_phi(prefix + "L2CB_phi",&master),
    L2CB_id_pt(prefix + "L2CB_id_pt",&master),
    L2CB_ms_pt(prefix + "L2CB_ms_pt",&master),
    L2CB_nPixHits(prefix + "L2CB_nPixHits",&master),
    L2CB_nSCTHits(prefix + "L2CB_nSCTHits",&master),
    L2CB_nTRTHits(prefix + "L2CB_nTRTHits",&master),
    L2CB_nTRTHighTHits(prefix + "L2CB_nTRTHighTHits",&master),
    L2CB_matched(prefix + "L2CB_matched",&master),
    L1_index(prefix + "L1_index",&master),
    L1_dr(prefix + "L1_dr",&master),
    L1_pt(prefix + "L1_pt",&master),
    L1_eta(prefix + "L1_eta",&master),
    L1_phi(prefix + "L1_phi",&master),
    L1_thrNumber(prefix + "L1_thrNumber",&master),
    L1_RoINumber(prefix + "L1_RoINumber",&master),
    L1_sectorAddress(prefix + "L1_sectorAddress",&master),
    L1_firstCandidate(prefix + "L1_firstCandidate",&master),
    L1_moreCandInRoI(prefix + "L1_moreCandInRoI",&master),
    L1_moreCandInSector(prefix + "L1_moreCandInSector",&master),
    L1_source(prefix + "L1_source",&master),
    L1_hemisphere(prefix + "L1_hemisphere",&master),
    L1_charge(prefix + "L1_charge",&master),
    L1_vetoed(prefix + "L1_vetoed",&master),
    L1_matched(prefix + "L1_matched",&master),
    truthAssoc_index(prefix + "truthAssoc_index",&master),
    MI10_max40_ptsum(prefix + "MI10_max40_ptsum",&master),
    MI10_max40_nTrks(prefix + "MI10_max40_nTrks",&master),
    MI10_max30_ptsum(prefix + "MI10_max30_ptsum",&master),
    MI10_max30_nTrks(prefix + "MI10_max30_nTrks",&master),
    MI15_max40_ptsum(prefix + "MI15_max40_ptsum",&master),
    MI15_max40_nTrks(prefix + "MI15_max40_nTrks",&master),
    MI15_max30_ptsum(prefix + "MI15_max30_ptsum",&master),
    MI15_max30_nTrks(prefix + "MI15_max30_nTrks",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  MuonD3PDCollection::MuonD3PDCollection(const std::string& prefix):
    TObject(),
    MET_n(prefix + "MET_n",0),
    MET_wpx(prefix + "MET_wpx",0),
    MET_wpy(prefix + "MET_wpy",0),
    MET_wet(prefix + "MET_wet",0),
    MET_statusWord(prefix + "MET_statusWord",0),
    MET_BDTMedium_n(prefix + "MET_BDTMedium_n",0),
    MET_BDTMedium_wpx(prefix + "MET_BDTMedium_wpx",0),
    MET_BDTMedium_wpy(prefix + "MET_BDTMedium_wpy",0),
    MET_BDTMedium_wet(prefix + "MET_BDTMedium_wet",0),
    MET_BDTMedium_statusWord(prefix + "MET_BDTMedium_statusWord",0),
    MET_AntiKt4LCTopo_tightpp_n(prefix + "MET_AntiKt4LCTopo_tightpp_n",0),
    MET_AntiKt4LCTopo_tightpp_wpx(prefix + "MET_AntiKt4LCTopo_tightpp_wpx",0),
    MET_AntiKt4LCTopo_tightpp_wpy(prefix + "MET_AntiKt4LCTopo_tightpp_wpy",0),
    MET_AntiKt4LCTopo_tightpp_wet(prefix + "MET_AntiKt4LCTopo_tightpp_wet",0),
    MET_AntiKt4LCTopo_tightpp_statusWord(prefix + "MET_AntiKt4LCTopo_tightpp_statusWord",0),
    n(prefix + "n",0),
    E(prefix + "E",0),
    pt(prefix + "pt",0),
    m(prefix + "m",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    px(prefix + "px",0),
    py(prefix + "py",0),
    pz(prefix + "pz",0),
    charge(prefix + "charge",0),
    allauthor(prefix + "allauthor",0),
    author(prefix + "author",0),
    beta(prefix + "beta",0),
    isMuonLikelihood(prefix + "isMuonLikelihood",0),
    matchchi2(prefix + "matchchi2",0),
    matchndof(prefix + "matchndof",0),
    etcone20(prefix + "etcone20",0),
    etcone30(prefix + "etcone30",0),
    etcone40(prefix + "etcone40",0),
    nucone20(prefix + "nucone20",0),
    nucone30(prefix + "nucone30",0),
    nucone40(prefix + "nucone40",0),
    ptcone20(prefix + "ptcone20",0),
    ptcone30(prefix + "ptcone30",0),
    ptcone40(prefix + "ptcone40",0),
    etconeNoEm10(prefix + "etconeNoEm10",0),
    etconeNoEm20(prefix + "etconeNoEm20",0),
    etconeNoEm30(prefix + "etconeNoEm30",0),
    etconeNoEm40(prefix + "etconeNoEm40",0),
    momentumBalanceSignificance(prefix + "momentumBalanceSignificance",0),
    energyLossPar(prefix + "energyLossPar",0),
    energyLossErr(prefix + "energyLossErr",0),
    etCore(prefix + "etCore",0),
    energyLossType(prefix + "energyLossType",0),
    caloMuonIdTag(prefix + "caloMuonIdTag",0),
    caloLRLikelihood(prefix + "caloLRLikelihood",0),
    bestMatch(prefix + "bestMatch",0),
    isStandAloneMuon(prefix + "isStandAloneMuon",0),
    isCombinedMuon(prefix + "isCombinedMuon",0),
    isLowPtReconstructedMuon(prefix + "isLowPtReconstructedMuon",0),
    isSegmentTaggedMuon(prefix + "isSegmentTaggedMuon",0),
    isCaloMuonId(prefix + "isCaloMuonId",0),
    alsoFoundByLowPt(prefix + "alsoFoundByLowPt",0),
    alsoFoundByCaloMuonId(prefix + "alsoFoundByCaloMuonId",0),
    isSiliconAssociatedForwardMuon(prefix + "isSiliconAssociatedForwardMuon",0),
    loose(prefix + "loose",0),
    medium(prefix + "medium",0),
    tight(prefix + "tight",0),
    d0_exPV(prefix + "d0_exPV",0),
    z0_exPV(prefix + "z0_exPV",0),
    phi_exPV(prefix + "phi_exPV",0),
    theta_exPV(prefix + "theta_exPV",0),
    qoverp_exPV(prefix + "qoverp_exPV",0),
    cb_d0_exPV(prefix + "cb_d0_exPV",0),
    cb_z0_exPV(prefix + "cb_z0_exPV",0),
    cb_phi_exPV(prefix + "cb_phi_exPV",0),
    cb_theta_exPV(prefix + "cb_theta_exPV",0),
    cb_qoverp_exPV(prefix + "cb_qoverp_exPV",0),
    id_d0_exPV(prefix + "id_d0_exPV",0),
    id_z0_exPV(prefix + "id_z0_exPV",0),
    id_phi_exPV(prefix + "id_phi_exPV",0),
    id_theta_exPV(prefix + "id_theta_exPV",0),
    id_qoverp_exPV(prefix + "id_qoverp_exPV",0),
    me_d0_exPV(prefix + "me_d0_exPV",0),
    me_z0_exPV(prefix + "me_z0_exPV",0),
    me_phi_exPV(prefix + "me_phi_exPV",0),
    me_theta_exPV(prefix + "me_theta_exPV",0),
    me_qoverp_exPV(prefix + "me_qoverp_exPV",0),
    cov_d0_exPV(prefix + "cov_d0_exPV",0),
    cov_z0_exPV(prefix + "cov_z0_exPV",0),
    cov_phi_exPV(prefix + "cov_phi_exPV",0),
    cov_theta_exPV(prefix + "cov_theta_exPV",0),
    cov_qoverp_exPV(prefix + "cov_qoverp_exPV",0),
    cov_d0_z0_exPV(prefix + "cov_d0_z0_exPV",0),
    cov_d0_phi_exPV(prefix + "cov_d0_phi_exPV",0),
    cov_d0_theta_exPV(prefix + "cov_d0_theta_exPV",0),
    cov_d0_qoverp_exPV(prefix + "cov_d0_qoverp_exPV",0),
    cov_z0_phi_exPV(prefix + "cov_z0_phi_exPV",0),
    cov_z0_theta_exPV(prefix + "cov_z0_theta_exPV",0),
    cov_z0_qoverp_exPV(prefix + "cov_z0_qoverp_exPV",0),
    cov_phi_theta_exPV(prefix + "cov_phi_theta_exPV",0),
    cov_phi_qoverp_exPV(prefix + "cov_phi_qoverp_exPV",0),
    cov_theta_qoverp_exPV(prefix + "cov_theta_qoverp_exPV",0),
    id_cov_d0_exPV(prefix + "id_cov_d0_exPV",0),
    id_cov_z0_exPV(prefix + "id_cov_z0_exPV",0),
    id_cov_phi_exPV(prefix + "id_cov_phi_exPV",0),
    id_cov_theta_exPV(prefix + "id_cov_theta_exPV",0),
    id_cov_qoverp_exPV(prefix + "id_cov_qoverp_exPV",0),
    id_cov_d0_z0_exPV(prefix + "id_cov_d0_z0_exPV",0),
    id_cov_d0_phi_exPV(prefix + "id_cov_d0_phi_exPV",0),
    id_cov_d0_theta_exPV(prefix + "id_cov_d0_theta_exPV",0),
    id_cov_d0_qoverp_exPV(prefix + "id_cov_d0_qoverp_exPV",0),
    id_cov_z0_phi_exPV(prefix + "id_cov_z0_phi_exPV",0),
    id_cov_z0_theta_exPV(prefix + "id_cov_z0_theta_exPV",0),
    id_cov_z0_qoverp_exPV(prefix + "id_cov_z0_qoverp_exPV",0),
    id_cov_phi_theta_exPV(prefix + "id_cov_phi_theta_exPV",0),
    id_cov_phi_qoverp_exPV(prefix + "id_cov_phi_qoverp_exPV",0),
    id_cov_theta_qoverp_exPV(prefix + "id_cov_theta_qoverp_exPV",0),
    me_cov_d0_exPV(prefix + "me_cov_d0_exPV",0),
    me_cov_z0_exPV(prefix + "me_cov_z0_exPV",0),
    me_cov_phi_exPV(prefix + "me_cov_phi_exPV",0),
    me_cov_theta_exPV(prefix + "me_cov_theta_exPV",0),
    me_cov_qoverp_exPV(prefix + "me_cov_qoverp_exPV",0),
    me_cov_d0_z0_exPV(prefix + "me_cov_d0_z0_exPV",0),
    me_cov_d0_phi_exPV(prefix + "me_cov_d0_phi_exPV",0),
    me_cov_d0_theta_exPV(prefix + "me_cov_d0_theta_exPV",0),
    me_cov_d0_qoverp_exPV(prefix + "me_cov_d0_qoverp_exPV",0),
    me_cov_z0_phi_exPV(prefix + "me_cov_z0_phi_exPV",0),
    me_cov_z0_theta_exPV(prefix + "me_cov_z0_theta_exPV",0),
    me_cov_z0_qoverp_exPV(prefix + "me_cov_z0_qoverp_exPV",0),
    me_cov_phi_theta_exPV(prefix + "me_cov_phi_theta_exPV",0),
    me_cov_phi_qoverp_exPV(prefix + "me_cov_phi_qoverp_exPV",0),
    me_cov_theta_qoverp_exPV(prefix + "me_cov_theta_qoverp_exPV",0),
    ms_d0(prefix + "ms_d0",0),
    ms_z0(prefix + "ms_z0",0),
    ms_phi(prefix + "ms_phi",0),
    ms_theta(prefix + "ms_theta",0),
    ms_qoverp(prefix + "ms_qoverp",0),
    id_d0(prefix + "id_d0",0),
    id_z0(prefix + "id_z0",0),
    id_phi(prefix + "id_phi",0),
    id_theta(prefix + "id_theta",0),
    id_qoverp(prefix + "id_qoverp",0),
    me_d0(prefix + "me_d0",0),
    me_z0(prefix + "me_z0",0),
    me_phi(prefix + "me_phi",0),
    me_theta(prefix + "me_theta",0),
    me_qoverp(prefix + "me_qoverp",0),
    nOutliersOnTrack(prefix + "nOutliersOnTrack",0),
    nBLHits(prefix + "nBLHits",0),
    nPixHits(prefix + "nPixHits",0),
    nSCTHits(prefix + "nSCTHits",0),
    nTRTHits(prefix + "nTRTHits",0),
    nTRTHighTHits(prefix + "nTRTHighTHits",0),
    nBLSharedHits(prefix + "nBLSharedHits",0),
    nPixSharedHits(prefix + "nPixSharedHits",0),
    nPixHoles(prefix + "nPixHoles",0),
    nSCTSharedHits(prefix + "nSCTSharedHits",0),
    nSCTHoles(prefix + "nSCTHoles",0),
    nTRTOutliers(prefix + "nTRTOutliers",0),
    nTRTHighTOutliers(prefix + "nTRTHighTOutliers",0),
    nGangedPixels(prefix + "nGangedPixels",0),
    nPixelDeadSensors(prefix + "nPixelDeadSensors",0),
    nSCTDeadSensors(prefix + "nSCTDeadSensors",0),
    nTRTDeadStraws(prefix + "nTRTDeadStraws",0),
    expectBLayerHit(prefix + "expectBLayerHit",0),
    nMDTHits(prefix + "nMDTHits",0),
    nMDTHoles(prefix + "nMDTHoles",0),
    nCSCEtaHits(prefix + "nCSCEtaHits",0),
    nCSCEtaHoles(prefix + "nCSCEtaHoles",0),
    nCSCUnspoiledEtaHits(prefix + "nCSCUnspoiledEtaHits",0),
    nCSCPhiHits(prefix + "nCSCPhiHits",0),
    nCSCPhiHoles(prefix + "nCSCPhiHoles",0),
    nRPCEtaHits(prefix + "nRPCEtaHits",0),
    nRPCEtaHoles(prefix + "nRPCEtaHoles",0),
    nRPCPhiHits(prefix + "nRPCPhiHits",0),
    nRPCPhiHoles(prefix + "nRPCPhiHoles",0),
    nTGCEtaHits(prefix + "nTGCEtaHits",0),
    nTGCEtaHoles(prefix + "nTGCEtaHoles",0),
    nTGCPhiHits(prefix + "nTGCPhiHits",0),
    nTGCPhiHoles(prefix + "nTGCPhiHoles",0),
    nprecisionLayers(prefix + "nprecisionLayers",0),
    nprecisionHoleLayers(prefix + "nprecisionHoleLayers",0),
    nphiLayers(prefix + "nphiLayers",0),
    ntrigEtaLayers(prefix + "ntrigEtaLayers",0),
    nphiHoleLayers(prefix + "nphiHoleLayers",0),
    ntrigEtaHoleLayers(prefix + "ntrigEtaHoleLayers",0),
    nMDTBIHits(prefix + "nMDTBIHits",0),
    nMDTBMHits(prefix + "nMDTBMHits",0),
    nMDTBOHits(prefix + "nMDTBOHits",0),
    nMDTBEEHits(prefix + "nMDTBEEHits",0),
    nMDTBIS78Hits(prefix + "nMDTBIS78Hits",0),
    nMDTEIHits(prefix + "nMDTEIHits",0),
    nMDTEMHits(prefix + "nMDTEMHits",0),
    nMDTEOHits(prefix + "nMDTEOHits",0),
    nMDTEEHits(prefix + "nMDTEEHits",0),
    nRPCLayer1EtaHits(prefix + "nRPCLayer1EtaHits",0),
    nRPCLayer2EtaHits(prefix + "nRPCLayer2EtaHits",0),
    nRPCLayer3EtaHits(prefix + "nRPCLayer3EtaHits",0),
    nRPCLayer1PhiHits(prefix + "nRPCLayer1PhiHits",0),
    nRPCLayer2PhiHits(prefix + "nRPCLayer2PhiHits",0),
    nRPCLayer3PhiHits(prefix + "nRPCLayer3PhiHits",0),
    nTGCLayer1EtaHits(prefix + "nTGCLayer1EtaHits",0),
    nTGCLayer2EtaHits(prefix + "nTGCLayer2EtaHits",0),
    nTGCLayer3EtaHits(prefix + "nTGCLayer3EtaHits",0),
    nTGCLayer4EtaHits(prefix + "nTGCLayer4EtaHits",0),
    nTGCLayer1PhiHits(prefix + "nTGCLayer1PhiHits",0),
    nTGCLayer2PhiHits(prefix + "nTGCLayer2PhiHits",0),
    nTGCLayer3PhiHits(prefix + "nTGCLayer3PhiHits",0),
    nTGCLayer4PhiHits(prefix + "nTGCLayer4PhiHits",0),
    barrelSectors(prefix + "barrelSectors",0),
    endcapSectors(prefix + "endcapSectors",0),
    spec_surf_px(prefix + "spec_surf_px",0),
    spec_surf_py(prefix + "spec_surf_py",0),
    spec_surf_pz(prefix + "spec_surf_pz",0),
    spec_surf_x(prefix + "spec_surf_x",0),
    spec_surf_y(prefix + "spec_surf_y",0),
    spec_surf_z(prefix + "spec_surf_z",0),
    trackd0(prefix + "trackd0",0),
    trackz0(prefix + "trackz0",0),
    trackphi(prefix + "trackphi",0),
    tracktheta(prefix + "tracktheta",0),
    trackqoverp(prefix + "trackqoverp",0),
    trackcov_d0(prefix + "trackcov_d0",0),
    trackcov_z0(prefix + "trackcov_z0",0),
    trackcov_phi(prefix + "trackcov_phi",0),
    trackcov_theta(prefix + "trackcov_theta",0),
    trackcov_qoverp(prefix + "trackcov_qoverp",0),
    trackcov_d0_z0(prefix + "trackcov_d0_z0",0),
    trackcov_d0_phi(prefix + "trackcov_d0_phi",0),
    trackcov_d0_theta(prefix + "trackcov_d0_theta",0),
    trackcov_d0_qoverp(prefix + "trackcov_d0_qoverp",0),
    trackcov_z0_phi(prefix + "trackcov_z0_phi",0),
    trackcov_z0_theta(prefix + "trackcov_z0_theta",0),
    trackcov_z0_qoverp(prefix + "trackcov_z0_qoverp",0),
    trackcov_phi_theta(prefix + "trackcov_phi_theta",0),
    trackcov_phi_qoverp(prefix + "trackcov_phi_qoverp",0),
    trackcov_theta_qoverp(prefix + "trackcov_theta_qoverp",0),
    trackfitchi2(prefix + "trackfitchi2",0),
    trackfitndof(prefix + "trackfitndof",0),
    hastrack(prefix + "hastrack",0),
    trackd0beam(prefix + "trackd0beam",0),
    trackz0beam(prefix + "trackz0beam",0),
    tracksigd0beam(prefix + "tracksigd0beam",0),
    tracksigz0beam(prefix + "tracksigz0beam",0),
    trackd0pv(prefix + "trackd0pv",0),
    trackz0pv(prefix + "trackz0pv",0),
    tracksigd0pv(prefix + "tracksigd0pv",0),
    tracksigz0pv(prefix + "tracksigz0pv",0),
    trackd0pvunbiased(prefix + "trackd0pvunbiased",0),
    trackz0pvunbiased(prefix + "trackz0pvunbiased",0),
    tracksigd0pvunbiased(prefix + "tracksigd0pvunbiased",0),
    tracksigz0pvunbiased(prefix + "tracksigz0pvunbiased",0),
    type(prefix + "type",0),
    origin(prefix + "origin",0),
    truth_dr(prefix + "truth_dr",0),
    truth_E(prefix + "truth_E",0),
    truth_pt(prefix + "truth_pt",0),
    truth_eta(prefix + "truth_eta",0),
    truth_phi(prefix + "truth_phi",0),
    truth_type(prefix + "truth_type",0),
    truth_status(prefix + "truth_status",0),
    truth_barcode(prefix + "truth_barcode",0),
    truth_mothertype(prefix + "truth_mothertype",0),
    truth_motherbarcode(prefix + "truth_motherbarcode",0),
    truth_matched(prefix + "truth_matched",0),
    L2CB_dr(prefix + "L2CB_dr",0),
    L2CB_pt(prefix + "L2CB_pt",0),
    L2CB_eta(prefix + "L2CB_eta",0),
    L2CB_phi(prefix + "L2CB_phi",0),
    L2CB_id_pt(prefix + "L2CB_id_pt",0),
    L2CB_ms_pt(prefix + "L2CB_ms_pt",0),
    L2CB_nPixHits(prefix + "L2CB_nPixHits",0),
    L2CB_nSCTHits(prefix + "L2CB_nSCTHits",0),
    L2CB_nTRTHits(prefix + "L2CB_nTRTHits",0),
    L2CB_nTRTHighTHits(prefix + "L2CB_nTRTHighTHits",0),
    L2CB_matched(prefix + "L2CB_matched",0),
    L1_index(prefix + "L1_index",0),
    L1_dr(prefix + "L1_dr",0),
    L1_pt(prefix + "L1_pt",0),
    L1_eta(prefix + "L1_eta",0),
    L1_phi(prefix + "L1_phi",0),
    L1_thrNumber(prefix + "L1_thrNumber",0),
    L1_RoINumber(prefix + "L1_RoINumber",0),
    L1_sectorAddress(prefix + "L1_sectorAddress",0),
    L1_firstCandidate(prefix + "L1_firstCandidate",0),
    L1_moreCandInRoI(prefix + "L1_moreCandInRoI",0),
    L1_moreCandInSector(prefix + "L1_moreCandInSector",0),
    L1_source(prefix + "L1_source",0),
    L1_hemisphere(prefix + "L1_hemisphere",0),
    L1_charge(prefix + "L1_charge",0),
    L1_vetoed(prefix + "L1_vetoed",0),
    L1_matched(prefix + "L1_matched",0),
    truthAssoc_index(prefix + "truthAssoc_index",0),
    MI10_max40_ptsum(prefix + "MI10_max40_ptsum",0),
    MI10_max40_nTrks(prefix + "MI10_max40_nTrks",0),
    MI10_max30_ptsum(prefix + "MI10_max30_ptsum",0),
    MI10_max30_nTrks(prefix + "MI10_max30_nTrks",0),
    MI15_max40_ptsum(prefix + "MI15_max40_ptsum",0),
    MI15_max40_nTrks(prefix + "MI15_max40_nTrks",0),
    MI15_max30_ptsum(prefix + "MI15_max30_ptsum",0),
    MI15_max30_nTrks(prefix + "MI15_max30_nTrks",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void MuonD3PDCollection::ReadFrom(TTree* tree)
  {
    MET_n.ReadFrom(tree);
    MET_wpx.ReadFrom(tree);
    MET_wpy.ReadFrom(tree);
    MET_wet.ReadFrom(tree);
    MET_statusWord.ReadFrom(tree);
    MET_BDTMedium_n.ReadFrom(tree);
    MET_BDTMedium_wpx.ReadFrom(tree);
    MET_BDTMedium_wpy.ReadFrom(tree);
    MET_BDTMedium_wet.ReadFrom(tree);
    MET_BDTMedium_statusWord.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_n.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_wpx.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_wpy.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_wet.ReadFrom(tree);
    MET_AntiKt4LCTopo_tightpp_statusWord.ReadFrom(tree);
    n.ReadFrom(tree);
    E.ReadFrom(tree);
    pt.ReadFrom(tree);
    m.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    px.ReadFrom(tree);
    py.ReadFrom(tree);
    pz.ReadFrom(tree);
    charge.ReadFrom(tree);
    allauthor.ReadFrom(tree);
    author.ReadFrom(tree);
    beta.ReadFrom(tree);
    isMuonLikelihood.ReadFrom(tree);
    matchchi2.ReadFrom(tree);
    matchndof.ReadFrom(tree);
    etcone20.ReadFrom(tree);
    etcone30.ReadFrom(tree);
    etcone40.ReadFrom(tree);
    nucone20.ReadFrom(tree);
    nucone30.ReadFrom(tree);
    nucone40.ReadFrom(tree);
    ptcone20.ReadFrom(tree);
    ptcone30.ReadFrom(tree);
    ptcone40.ReadFrom(tree);
    etconeNoEm10.ReadFrom(tree);
    etconeNoEm20.ReadFrom(tree);
    etconeNoEm30.ReadFrom(tree);
    etconeNoEm40.ReadFrom(tree);
    momentumBalanceSignificance.ReadFrom(tree);
    energyLossPar.ReadFrom(tree);
    energyLossErr.ReadFrom(tree);
    etCore.ReadFrom(tree);
    energyLossType.ReadFrom(tree);
    caloMuonIdTag.ReadFrom(tree);
    caloLRLikelihood.ReadFrom(tree);
    bestMatch.ReadFrom(tree);
    isStandAloneMuon.ReadFrom(tree);
    isCombinedMuon.ReadFrom(tree);
    isLowPtReconstructedMuon.ReadFrom(tree);
    isSegmentTaggedMuon.ReadFrom(tree);
    isCaloMuonId.ReadFrom(tree);
    alsoFoundByLowPt.ReadFrom(tree);
    alsoFoundByCaloMuonId.ReadFrom(tree);
    isSiliconAssociatedForwardMuon.ReadFrom(tree);
    loose.ReadFrom(tree);
    medium.ReadFrom(tree);
    tight.ReadFrom(tree);
    d0_exPV.ReadFrom(tree);
    z0_exPV.ReadFrom(tree);
    phi_exPV.ReadFrom(tree);
    theta_exPV.ReadFrom(tree);
    qoverp_exPV.ReadFrom(tree);
    cb_d0_exPV.ReadFrom(tree);
    cb_z0_exPV.ReadFrom(tree);
    cb_phi_exPV.ReadFrom(tree);
    cb_theta_exPV.ReadFrom(tree);
    cb_qoverp_exPV.ReadFrom(tree);
    id_d0_exPV.ReadFrom(tree);
    id_z0_exPV.ReadFrom(tree);
    id_phi_exPV.ReadFrom(tree);
    id_theta_exPV.ReadFrom(tree);
    id_qoverp_exPV.ReadFrom(tree);
    me_d0_exPV.ReadFrom(tree);
    me_z0_exPV.ReadFrom(tree);
    me_phi_exPV.ReadFrom(tree);
    me_theta_exPV.ReadFrom(tree);
    me_qoverp_exPV.ReadFrom(tree);
    cov_d0_exPV.ReadFrom(tree);
    cov_z0_exPV.ReadFrom(tree);
    cov_phi_exPV.ReadFrom(tree);
    cov_theta_exPV.ReadFrom(tree);
    cov_qoverp_exPV.ReadFrom(tree);
    cov_d0_z0_exPV.ReadFrom(tree);
    cov_d0_phi_exPV.ReadFrom(tree);
    cov_d0_theta_exPV.ReadFrom(tree);
    cov_d0_qoverp_exPV.ReadFrom(tree);
    cov_z0_phi_exPV.ReadFrom(tree);
    cov_z0_theta_exPV.ReadFrom(tree);
    cov_z0_qoverp_exPV.ReadFrom(tree);
    cov_phi_theta_exPV.ReadFrom(tree);
    cov_phi_qoverp_exPV.ReadFrom(tree);
    cov_theta_qoverp_exPV.ReadFrom(tree);
    id_cov_d0_exPV.ReadFrom(tree);
    id_cov_z0_exPV.ReadFrom(tree);
    id_cov_phi_exPV.ReadFrom(tree);
    id_cov_theta_exPV.ReadFrom(tree);
    id_cov_qoverp_exPV.ReadFrom(tree);
    id_cov_d0_z0_exPV.ReadFrom(tree);
    id_cov_d0_phi_exPV.ReadFrom(tree);
    id_cov_d0_theta_exPV.ReadFrom(tree);
    id_cov_d0_qoverp_exPV.ReadFrom(tree);
    id_cov_z0_phi_exPV.ReadFrom(tree);
    id_cov_z0_theta_exPV.ReadFrom(tree);
    id_cov_z0_qoverp_exPV.ReadFrom(tree);
    id_cov_phi_theta_exPV.ReadFrom(tree);
    id_cov_phi_qoverp_exPV.ReadFrom(tree);
    id_cov_theta_qoverp_exPV.ReadFrom(tree);
    me_cov_d0_exPV.ReadFrom(tree);
    me_cov_z0_exPV.ReadFrom(tree);
    me_cov_phi_exPV.ReadFrom(tree);
    me_cov_theta_exPV.ReadFrom(tree);
    me_cov_qoverp_exPV.ReadFrom(tree);
    me_cov_d0_z0_exPV.ReadFrom(tree);
    me_cov_d0_phi_exPV.ReadFrom(tree);
    me_cov_d0_theta_exPV.ReadFrom(tree);
    me_cov_d0_qoverp_exPV.ReadFrom(tree);
    me_cov_z0_phi_exPV.ReadFrom(tree);
    me_cov_z0_theta_exPV.ReadFrom(tree);
    me_cov_z0_qoverp_exPV.ReadFrom(tree);
    me_cov_phi_theta_exPV.ReadFrom(tree);
    me_cov_phi_qoverp_exPV.ReadFrom(tree);
    me_cov_theta_qoverp_exPV.ReadFrom(tree);
    ms_d0.ReadFrom(tree);
    ms_z0.ReadFrom(tree);
    ms_phi.ReadFrom(tree);
    ms_theta.ReadFrom(tree);
    ms_qoverp.ReadFrom(tree);
    id_d0.ReadFrom(tree);
    id_z0.ReadFrom(tree);
    id_phi.ReadFrom(tree);
    id_theta.ReadFrom(tree);
    id_qoverp.ReadFrom(tree);
    me_d0.ReadFrom(tree);
    me_z0.ReadFrom(tree);
    me_phi.ReadFrom(tree);
    me_theta.ReadFrom(tree);
    me_qoverp.ReadFrom(tree);
    nOutliersOnTrack.ReadFrom(tree);
    nBLHits.ReadFrom(tree);
    nPixHits.ReadFrom(tree);
    nSCTHits.ReadFrom(tree);
    nTRTHits.ReadFrom(tree);
    nTRTHighTHits.ReadFrom(tree);
    nBLSharedHits.ReadFrom(tree);
    nPixSharedHits.ReadFrom(tree);
    nPixHoles.ReadFrom(tree);
    nSCTSharedHits.ReadFrom(tree);
    nSCTHoles.ReadFrom(tree);
    nTRTOutliers.ReadFrom(tree);
    nTRTHighTOutliers.ReadFrom(tree);
    nGangedPixels.ReadFrom(tree);
    nPixelDeadSensors.ReadFrom(tree);
    nSCTDeadSensors.ReadFrom(tree);
    nTRTDeadStraws.ReadFrom(tree);
    expectBLayerHit.ReadFrom(tree);
    nMDTHits.ReadFrom(tree);
    nMDTHoles.ReadFrom(tree);
    nCSCEtaHits.ReadFrom(tree);
    nCSCEtaHoles.ReadFrom(tree);
    nCSCUnspoiledEtaHits.ReadFrom(tree);
    nCSCPhiHits.ReadFrom(tree);
    nCSCPhiHoles.ReadFrom(tree);
    nRPCEtaHits.ReadFrom(tree);
    nRPCEtaHoles.ReadFrom(tree);
    nRPCPhiHits.ReadFrom(tree);
    nRPCPhiHoles.ReadFrom(tree);
    nTGCEtaHits.ReadFrom(tree);
    nTGCEtaHoles.ReadFrom(tree);
    nTGCPhiHits.ReadFrom(tree);
    nTGCPhiHoles.ReadFrom(tree);
    nprecisionLayers.ReadFrom(tree);
    nprecisionHoleLayers.ReadFrom(tree);
    nphiLayers.ReadFrom(tree);
    ntrigEtaLayers.ReadFrom(tree);
    nphiHoleLayers.ReadFrom(tree);
    ntrigEtaHoleLayers.ReadFrom(tree);
    nMDTBIHits.ReadFrom(tree);
    nMDTBMHits.ReadFrom(tree);
    nMDTBOHits.ReadFrom(tree);
    nMDTBEEHits.ReadFrom(tree);
    nMDTBIS78Hits.ReadFrom(tree);
    nMDTEIHits.ReadFrom(tree);
    nMDTEMHits.ReadFrom(tree);
    nMDTEOHits.ReadFrom(tree);
    nMDTEEHits.ReadFrom(tree);
    nRPCLayer1EtaHits.ReadFrom(tree);
    nRPCLayer2EtaHits.ReadFrom(tree);
    nRPCLayer3EtaHits.ReadFrom(tree);
    nRPCLayer1PhiHits.ReadFrom(tree);
    nRPCLayer2PhiHits.ReadFrom(tree);
    nRPCLayer3PhiHits.ReadFrom(tree);
    nTGCLayer1EtaHits.ReadFrom(tree);
    nTGCLayer2EtaHits.ReadFrom(tree);
    nTGCLayer3EtaHits.ReadFrom(tree);
    nTGCLayer4EtaHits.ReadFrom(tree);
    nTGCLayer1PhiHits.ReadFrom(tree);
    nTGCLayer2PhiHits.ReadFrom(tree);
    nTGCLayer3PhiHits.ReadFrom(tree);
    nTGCLayer4PhiHits.ReadFrom(tree);
    barrelSectors.ReadFrom(tree);
    endcapSectors.ReadFrom(tree);
    spec_surf_px.ReadFrom(tree);
    spec_surf_py.ReadFrom(tree);
    spec_surf_pz.ReadFrom(tree);
    spec_surf_x.ReadFrom(tree);
    spec_surf_y.ReadFrom(tree);
    spec_surf_z.ReadFrom(tree);
    trackd0.ReadFrom(tree);
    trackz0.ReadFrom(tree);
    trackphi.ReadFrom(tree);
    tracktheta.ReadFrom(tree);
    trackqoverp.ReadFrom(tree);
    trackcov_d0.ReadFrom(tree);
    trackcov_z0.ReadFrom(tree);
    trackcov_phi.ReadFrom(tree);
    trackcov_theta.ReadFrom(tree);
    trackcov_qoverp.ReadFrom(tree);
    trackcov_d0_z0.ReadFrom(tree);
    trackcov_d0_phi.ReadFrom(tree);
    trackcov_d0_theta.ReadFrom(tree);
    trackcov_d0_qoverp.ReadFrom(tree);
    trackcov_z0_phi.ReadFrom(tree);
    trackcov_z0_theta.ReadFrom(tree);
    trackcov_z0_qoverp.ReadFrom(tree);
    trackcov_phi_theta.ReadFrom(tree);
    trackcov_phi_qoverp.ReadFrom(tree);
    trackcov_theta_qoverp.ReadFrom(tree);
    trackfitchi2.ReadFrom(tree);
    trackfitndof.ReadFrom(tree);
    hastrack.ReadFrom(tree);
    trackd0beam.ReadFrom(tree);
    trackz0beam.ReadFrom(tree);
    tracksigd0beam.ReadFrom(tree);
    tracksigz0beam.ReadFrom(tree);
    trackd0pv.ReadFrom(tree);
    trackz0pv.ReadFrom(tree);
    tracksigd0pv.ReadFrom(tree);
    tracksigz0pv.ReadFrom(tree);
    trackd0pvunbiased.ReadFrom(tree);
    trackz0pvunbiased.ReadFrom(tree);
    tracksigd0pvunbiased.ReadFrom(tree);
    tracksigz0pvunbiased.ReadFrom(tree);
    type.ReadFrom(tree);
    origin.ReadFrom(tree);
    truth_dr.ReadFrom(tree);
    truth_E.ReadFrom(tree);
    truth_pt.ReadFrom(tree);
    truth_eta.ReadFrom(tree);
    truth_phi.ReadFrom(tree);
    truth_type.ReadFrom(tree);
    truth_status.ReadFrom(tree);
    truth_barcode.ReadFrom(tree);
    truth_mothertype.ReadFrom(tree);
    truth_motherbarcode.ReadFrom(tree);
    truth_matched.ReadFrom(tree);
    L2CB_dr.ReadFrom(tree);
    L2CB_pt.ReadFrom(tree);
    L2CB_eta.ReadFrom(tree);
    L2CB_phi.ReadFrom(tree);
    L2CB_id_pt.ReadFrom(tree);
    L2CB_ms_pt.ReadFrom(tree);
    L2CB_nPixHits.ReadFrom(tree);
    L2CB_nSCTHits.ReadFrom(tree);
    L2CB_nTRTHits.ReadFrom(tree);
    L2CB_nTRTHighTHits.ReadFrom(tree);
    L2CB_matched.ReadFrom(tree);
    L1_index.ReadFrom(tree);
    L1_dr.ReadFrom(tree);
    L1_pt.ReadFrom(tree);
    L1_eta.ReadFrom(tree);
    L1_phi.ReadFrom(tree);
    L1_thrNumber.ReadFrom(tree);
    L1_RoINumber.ReadFrom(tree);
    L1_sectorAddress.ReadFrom(tree);
    L1_firstCandidate.ReadFrom(tree);
    L1_moreCandInRoI.ReadFrom(tree);
    L1_moreCandInSector.ReadFrom(tree);
    L1_source.ReadFrom(tree);
    L1_hemisphere.ReadFrom(tree);
    L1_charge.ReadFrom(tree);
    L1_vetoed.ReadFrom(tree);
    L1_matched.ReadFrom(tree);
    truthAssoc_index.ReadFrom(tree);
    MI10_max40_ptsum.ReadFrom(tree);
    MI10_max40_nTrks.ReadFrom(tree);
    MI10_max30_ptsum.ReadFrom(tree);
    MI10_max30_nTrks.ReadFrom(tree);
    MI15_max40_ptsum.ReadFrom(tree);
    MI15_max40_nTrks.ReadFrom(tree);
    MI15_max30_ptsum.ReadFrom(tree);
    MI15_max30_nTrks.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void MuonD3PDCollection::WriteTo(TTree* tree)
  {
    MET_n.WriteTo(tree);
    MET_wpx.WriteTo(tree);
    MET_wpy.WriteTo(tree);
    MET_wet.WriteTo(tree);
    MET_statusWord.WriteTo(tree);
    MET_BDTMedium_n.WriteTo(tree);
    MET_BDTMedium_wpx.WriteTo(tree);
    MET_BDTMedium_wpy.WriteTo(tree);
    MET_BDTMedium_wet.WriteTo(tree);
    MET_BDTMedium_statusWord.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_n.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_wpx.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_wpy.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_wet.WriteTo(tree);
    MET_AntiKt4LCTopo_tightpp_statusWord.WriteTo(tree);
    n.WriteTo(tree);
    E.WriteTo(tree);
    pt.WriteTo(tree);
    m.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    px.WriteTo(tree);
    py.WriteTo(tree);
    pz.WriteTo(tree);
    charge.WriteTo(tree);
    allauthor.WriteTo(tree);
    author.WriteTo(tree);
    beta.WriteTo(tree);
    isMuonLikelihood.WriteTo(tree);
    matchchi2.WriteTo(tree);
    matchndof.WriteTo(tree);
    etcone20.WriteTo(tree);
    etcone30.WriteTo(tree);
    etcone40.WriteTo(tree);
    nucone20.WriteTo(tree);
    nucone30.WriteTo(tree);
    nucone40.WriteTo(tree);
    ptcone20.WriteTo(tree);
    ptcone30.WriteTo(tree);
    ptcone40.WriteTo(tree);
    etconeNoEm10.WriteTo(tree);
    etconeNoEm20.WriteTo(tree);
    etconeNoEm30.WriteTo(tree);
    etconeNoEm40.WriteTo(tree);
    momentumBalanceSignificance.WriteTo(tree);
    energyLossPar.WriteTo(tree);
    energyLossErr.WriteTo(tree);
    etCore.WriteTo(tree);
    energyLossType.WriteTo(tree);
    caloMuonIdTag.WriteTo(tree);
    caloLRLikelihood.WriteTo(tree);
    bestMatch.WriteTo(tree);
    isStandAloneMuon.WriteTo(tree);
    isCombinedMuon.WriteTo(tree);
    isLowPtReconstructedMuon.WriteTo(tree);
    isSegmentTaggedMuon.WriteTo(tree);
    isCaloMuonId.WriteTo(tree);
    alsoFoundByLowPt.WriteTo(tree);
    alsoFoundByCaloMuonId.WriteTo(tree);
    isSiliconAssociatedForwardMuon.WriteTo(tree);
    loose.WriteTo(tree);
    medium.WriteTo(tree);
    tight.WriteTo(tree);
    d0_exPV.WriteTo(tree);
    z0_exPV.WriteTo(tree);
    phi_exPV.WriteTo(tree);
    theta_exPV.WriteTo(tree);
    qoverp_exPV.WriteTo(tree);
    cb_d0_exPV.WriteTo(tree);
    cb_z0_exPV.WriteTo(tree);
    cb_phi_exPV.WriteTo(tree);
    cb_theta_exPV.WriteTo(tree);
    cb_qoverp_exPV.WriteTo(tree);
    id_d0_exPV.WriteTo(tree);
    id_z0_exPV.WriteTo(tree);
    id_phi_exPV.WriteTo(tree);
    id_theta_exPV.WriteTo(tree);
    id_qoverp_exPV.WriteTo(tree);
    me_d0_exPV.WriteTo(tree);
    me_z0_exPV.WriteTo(tree);
    me_phi_exPV.WriteTo(tree);
    me_theta_exPV.WriteTo(tree);
    me_qoverp_exPV.WriteTo(tree);
    cov_d0_exPV.WriteTo(tree);
    cov_z0_exPV.WriteTo(tree);
    cov_phi_exPV.WriteTo(tree);
    cov_theta_exPV.WriteTo(tree);
    cov_qoverp_exPV.WriteTo(tree);
    cov_d0_z0_exPV.WriteTo(tree);
    cov_d0_phi_exPV.WriteTo(tree);
    cov_d0_theta_exPV.WriteTo(tree);
    cov_d0_qoverp_exPV.WriteTo(tree);
    cov_z0_phi_exPV.WriteTo(tree);
    cov_z0_theta_exPV.WriteTo(tree);
    cov_z0_qoverp_exPV.WriteTo(tree);
    cov_phi_theta_exPV.WriteTo(tree);
    cov_phi_qoverp_exPV.WriteTo(tree);
    cov_theta_qoverp_exPV.WriteTo(tree);
    id_cov_d0_exPV.WriteTo(tree);
    id_cov_z0_exPV.WriteTo(tree);
    id_cov_phi_exPV.WriteTo(tree);
    id_cov_theta_exPV.WriteTo(tree);
    id_cov_qoverp_exPV.WriteTo(tree);
    id_cov_d0_z0_exPV.WriteTo(tree);
    id_cov_d0_phi_exPV.WriteTo(tree);
    id_cov_d0_theta_exPV.WriteTo(tree);
    id_cov_d0_qoverp_exPV.WriteTo(tree);
    id_cov_z0_phi_exPV.WriteTo(tree);
    id_cov_z0_theta_exPV.WriteTo(tree);
    id_cov_z0_qoverp_exPV.WriteTo(tree);
    id_cov_phi_theta_exPV.WriteTo(tree);
    id_cov_phi_qoverp_exPV.WriteTo(tree);
    id_cov_theta_qoverp_exPV.WriteTo(tree);
    me_cov_d0_exPV.WriteTo(tree);
    me_cov_z0_exPV.WriteTo(tree);
    me_cov_phi_exPV.WriteTo(tree);
    me_cov_theta_exPV.WriteTo(tree);
    me_cov_qoverp_exPV.WriteTo(tree);
    me_cov_d0_z0_exPV.WriteTo(tree);
    me_cov_d0_phi_exPV.WriteTo(tree);
    me_cov_d0_theta_exPV.WriteTo(tree);
    me_cov_d0_qoverp_exPV.WriteTo(tree);
    me_cov_z0_phi_exPV.WriteTo(tree);
    me_cov_z0_theta_exPV.WriteTo(tree);
    me_cov_z0_qoverp_exPV.WriteTo(tree);
    me_cov_phi_theta_exPV.WriteTo(tree);
    me_cov_phi_qoverp_exPV.WriteTo(tree);
    me_cov_theta_qoverp_exPV.WriteTo(tree);
    ms_d0.WriteTo(tree);
    ms_z0.WriteTo(tree);
    ms_phi.WriteTo(tree);
    ms_theta.WriteTo(tree);
    ms_qoverp.WriteTo(tree);
    id_d0.WriteTo(tree);
    id_z0.WriteTo(tree);
    id_phi.WriteTo(tree);
    id_theta.WriteTo(tree);
    id_qoverp.WriteTo(tree);
    me_d0.WriteTo(tree);
    me_z0.WriteTo(tree);
    me_phi.WriteTo(tree);
    me_theta.WriteTo(tree);
    me_qoverp.WriteTo(tree);
    nOutliersOnTrack.WriteTo(tree);
    nBLHits.WriteTo(tree);
    nPixHits.WriteTo(tree);
    nSCTHits.WriteTo(tree);
    nTRTHits.WriteTo(tree);
    nTRTHighTHits.WriteTo(tree);
    nBLSharedHits.WriteTo(tree);
    nPixSharedHits.WriteTo(tree);
    nPixHoles.WriteTo(tree);
    nSCTSharedHits.WriteTo(tree);
    nSCTHoles.WriteTo(tree);
    nTRTOutliers.WriteTo(tree);
    nTRTHighTOutliers.WriteTo(tree);
    nGangedPixels.WriteTo(tree);
    nPixelDeadSensors.WriteTo(tree);
    nSCTDeadSensors.WriteTo(tree);
    nTRTDeadStraws.WriteTo(tree);
    expectBLayerHit.WriteTo(tree);
    nMDTHits.WriteTo(tree);
    nMDTHoles.WriteTo(tree);
    nCSCEtaHits.WriteTo(tree);
    nCSCEtaHoles.WriteTo(tree);
    nCSCUnspoiledEtaHits.WriteTo(tree);
    nCSCPhiHits.WriteTo(tree);
    nCSCPhiHoles.WriteTo(tree);
    nRPCEtaHits.WriteTo(tree);
    nRPCEtaHoles.WriteTo(tree);
    nRPCPhiHits.WriteTo(tree);
    nRPCPhiHoles.WriteTo(tree);
    nTGCEtaHits.WriteTo(tree);
    nTGCEtaHoles.WriteTo(tree);
    nTGCPhiHits.WriteTo(tree);
    nTGCPhiHoles.WriteTo(tree);
    nprecisionLayers.WriteTo(tree);
    nprecisionHoleLayers.WriteTo(tree);
    nphiLayers.WriteTo(tree);
    ntrigEtaLayers.WriteTo(tree);
    nphiHoleLayers.WriteTo(tree);
    ntrigEtaHoleLayers.WriteTo(tree);
    nMDTBIHits.WriteTo(tree);
    nMDTBMHits.WriteTo(tree);
    nMDTBOHits.WriteTo(tree);
    nMDTBEEHits.WriteTo(tree);
    nMDTBIS78Hits.WriteTo(tree);
    nMDTEIHits.WriteTo(tree);
    nMDTEMHits.WriteTo(tree);
    nMDTEOHits.WriteTo(tree);
    nMDTEEHits.WriteTo(tree);
    nRPCLayer1EtaHits.WriteTo(tree);
    nRPCLayer2EtaHits.WriteTo(tree);
    nRPCLayer3EtaHits.WriteTo(tree);
    nRPCLayer1PhiHits.WriteTo(tree);
    nRPCLayer2PhiHits.WriteTo(tree);
    nRPCLayer3PhiHits.WriteTo(tree);
    nTGCLayer1EtaHits.WriteTo(tree);
    nTGCLayer2EtaHits.WriteTo(tree);
    nTGCLayer3EtaHits.WriteTo(tree);
    nTGCLayer4EtaHits.WriteTo(tree);
    nTGCLayer1PhiHits.WriteTo(tree);
    nTGCLayer2PhiHits.WriteTo(tree);
    nTGCLayer3PhiHits.WriteTo(tree);
    nTGCLayer4PhiHits.WriteTo(tree);
    barrelSectors.WriteTo(tree);
    endcapSectors.WriteTo(tree);
    spec_surf_px.WriteTo(tree);
    spec_surf_py.WriteTo(tree);
    spec_surf_pz.WriteTo(tree);
    spec_surf_x.WriteTo(tree);
    spec_surf_y.WriteTo(tree);
    spec_surf_z.WriteTo(tree);
    trackd0.WriteTo(tree);
    trackz0.WriteTo(tree);
    trackphi.WriteTo(tree);
    tracktheta.WriteTo(tree);
    trackqoverp.WriteTo(tree);
    trackcov_d0.WriteTo(tree);
    trackcov_z0.WriteTo(tree);
    trackcov_phi.WriteTo(tree);
    trackcov_theta.WriteTo(tree);
    trackcov_qoverp.WriteTo(tree);
    trackcov_d0_z0.WriteTo(tree);
    trackcov_d0_phi.WriteTo(tree);
    trackcov_d0_theta.WriteTo(tree);
    trackcov_d0_qoverp.WriteTo(tree);
    trackcov_z0_phi.WriteTo(tree);
    trackcov_z0_theta.WriteTo(tree);
    trackcov_z0_qoverp.WriteTo(tree);
    trackcov_phi_theta.WriteTo(tree);
    trackcov_phi_qoverp.WriteTo(tree);
    trackcov_theta_qoverp.WriteTo(tree);
    trackfitchi2.WriteTo(tree);
    trackfitndof.WriteTo(tree);
    hastrack.WriteTo(tree);
    trackd0beam.WriteTo(tree);
    trackz0beam.WriteTo(tree);
    tracksigd0beam.WriteTo(tree);
    tracksigz0beam.WriteTo(tree);
    trackd0pv.WriteTo(tree);
    trackz0pv.WriteTo(tree);
    tracksigd0pv.WriteTo(tree);
    tracksigz0pv.WriteTo(tree);
    trackd0pvunbiased.WriteTo(tree);
    trackz0pvunbiased.WriteTo(tree);
    tracksigd0pvunbiased.WriteTo(tree);
    tracksigz0pvunbiased.WriteTo(tree);
    type.WriteTo(tree);
    origin.WriteTo(tree);
    truth_dr.WriteTo(tree);
    truth_E.WriteTo(tree);
    truth_pt.WriteTo(tree);
    truth_eta.WriteTo(tree);
    truth_phi.WriteTo(tree);
    truth_type.WriteTo(tree);
    truth_status.WriteTo(tree);
    truth_barcode.WriteTo(tree);
    truth_mothertype.WriteTo(tree);
    truth_motherbarcode.WriteTo(tree);
    truth_matched.WriteTo(tree);
    L2CB_dr.WriteTo(tree);
    L2CB_pt.WriteTo(tree);
    L2CB_eta.WriteTo(tree);
    L2CB_phi.WriteTo(tree);
    L2CB_id_pt.WriteTo(tree);
    L2CB_ms_pt.WriteTo(tree);
    L2CB_nPixHits.WriteTo(tree);
    L2CB_nSCTHits.WriteTo(tree);
    L2CB_nTRTHits.WriteTo(tree);
    L2CB_nTRTHighTHits.WriteTo(tree);
    L2CB_matched.WriteTo(tree);
    L1_index.WriteTo(tree);
    L1_dr.WriteTo(tree);
    L1_pt.WriteTo(tree);
    L1_eta.WriteTo(tree);
    L1_phi.WriteTo(tree);
    L1_thrNumber.WriteTo(tree);
    L1_RoINumber.WriteTo(tree);
    L1_sectorAddress.WriteTo(tree);
    L1_firstCandidate.WriteTo(tree);
    L1_moreCandInRoI.WriteTo(tree);
    L1_moreCandInSector.WriteTo(tree);
    L1_source.WriteTo(tree);
    L1_hemisphere.WriteTo(tree);
    L1_charge.WriteTo(tree);
    L1_vetoed.WriteTo(tree);
    L1_matched.WriteTo(tree);
    truthAssoc_index.WriteTo(tree);
    MI10_max40_ptsum.WriteTo(tree);
    MI10_max40_nTrks.WriteTo(tree);
    MI10_max30_ptsum.WriteTo(tree);
    MI10_max30_nTrks.WriteTo(tree);
    MI15_max40_ptsum.WriteTo(tree);
    MI15_max40_nTrks.WriteTo(tree);
    MI15_max30_ptsum.WriteTo(tree);
    MI15_max30_nTrks.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void MuonD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(MET_n.IsAvailable()) MET_n.SetActive(active);
     if(MET_wpx.IsAvailable()) MET_wpx.SetActive(active);
     if(MET_wpy.IsAvailable()) MET_wpy.SetActive(active);
     if(MET_wet.IsAvailable()) MET_wet.SetActive(active);
     if(MET_statusWord.IsAvailable()) MET_statusWord.SetActive(active);
     if(MET_BDTMedium_n.IsAvailable()) MET_BDTMedium_n.SetActive(active);
     if(MET_BDTMedium_wpx.IsAvailable()) MET_BDTMedium_wpx.SetActive(active);
     if(MET_BDTMedium_wpy.IsAvailable()) MET_BDTMedium_wpy.SetActive(active);
     if(MET_BDTMedium_wet.IsAvailable()) MET_BDTMedium_wet.SetActive(active);
     if(MET_BDTMedium_statusWord.IsAvailable()) MET_BDTMedium_statusWord.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_n.IsAvailable()) MET_AntiKt4LCTopo_tightpp_n.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_wpx.IsAvailable()) MET_AntiKt4LCTopo_tightpp_wpx.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_wpy.IsAvailable()) MET_AntiKt4LCTopo_tightpp_wpy.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_wet.IsAvailable()) MET_AntiKt4LCTopo_tightpp_wet.SetActive(active);
     if(MET_AntiKt4LCTopo_tightpp_statusWord.IsAvailable()) MET_AntiKt4LCTopo_tightpp_statusWord.SetActive(active);
     if(n.IsAvailable()) n.SetActive(active);
     if(E.IsAvailable()) E.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(m.IsAvailable()) m.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(px.IsAvailable()) px.SetActive(active);
     if(py.IsAvailable()) py.SetActive(active);
     if(pz.IsAvailable()) pz.SetActive(active);
     if(charge.IsAvailable()) charge.SetActive(active);
     if(allauthor.IsAvailable()) allauthor.SetActive(active);
     if(author.IsAvailable()) author.SetActive(active);
     if(beta.IsAvailable()) beta.SetActive(active);
     if(isMuonLikelihood.IsAvailable()) isMuonLikelihood.SetActive(active);
     if(matchchi2.IsAvailable()) matchchi2.SetActive(active);
     if(matchndof.IsAvailable()) matchndof.SetActive(active);
     if(etcone20.IsAvailable()) etcone20.SetActive(active);
     if(etcone30.IsAvailable()) etcone30.SetActive(active);
     if(etcone40.IsAvailable()) etcone40.SetActive(active);
     if(nucone20.IsAvailable()) nucone20.SetActive(active);
     if(nucone30.IsAvailable()) nucone30.SetActive(active);
     if(nucone40.IsAvailable()) nucone40.SetActive(active);
     if(ptcone20.IsAvailable()) ptcone20.SetActive(active);
     if(ptcone30.IsAvailable()) ptcone30.SetActive(active);
     if(ptcone40.IsAvailable()) ptcone40.SetActive(active);
     if(etconeNoEm10.IsAvailable()) etconeNoEm10.SetActive(active);
     if(etconeNoEm20.IsAvailable()) etconeNoEm20.SetActive(active);
     if(etconeNoEm30.IsAvailable()) etconeNoEm30.SetActive(active);
     if(etconeNoEm40.IsAvailable()) etconeNoEm40.SetActive(active);
     if(momentumBalanceSignificance.IsAvailable()) momentumBalanceSignificance.SetActive(active);
     if(energyLossPar.IsAvailable()) energyLossPar.SetActive(active);
     if(energyLossErr.IsAvailable()) energyLossErr.SetActive(active);
     if(etCore.IsAvailable()) etCore.SetActive(active);
     if(energyLossType.IsAvailable()) energyLossType.SetActive(active);
     if(caloMuonIdTag.IsAvailable()) caloMuonIdTag.SetActive(active);
     if(caloLRLikelihood.IsAvailable()) caloLRLikelihood.SetActive(active);
     if(bestMatch.IsAvailable()) bestMatch.SetActive(active);
     if(isStandAloneMuon.IsAvailable()) isStandAloneMuon.SetActive(active);
     if(isCombinedMuon.IsAvailable()) isCombinedMuon.SetActive(active);
     if(isLowPtReconstructedMuon.IsAvailable()) isLowPtReconstructedMuon.SetActive(active);
     if(isSegmentTaggedMuon.IsAvailable()) isSegmentTaggedMuon.SetActive(active);
     if(isCaloMuonId.IsAvailable()) isCaloMuonId.SetActive(active);
     if(alsoFoundByLowPt.IsAvailable()) alsoFoundByLowPt.SetActive(active);
     if(alsoFoundByCaloMuonId.IsAvailable()) alsoFoundByCaloMuonId.SetActive(active);
     if(isSiliconAssociatedForwardMuon.IsAvailable()) isSiliconAssociatedForwardMuon.SetActive(active);
     if(loose.IsAvailable()) loose.SetActive(active);
     if(medium.IsAvailable()) medium.SetActive(active);
     if(tight.IsAvailable()) tight.SetActive(active);
     if(d0_exPV.IsAvailable()) d0_exPV.SetActive(active);
     if(z0_exPV.IsAvailable()) z0_exPV.SetActive(active);
     if(phi_exPV.IsAvailable()) phi_exPV.SetActive(active);
     if(theta_exPV.IsAvailable()) theta_exPV.SetActive(active);
     if(qoverp_exPV.IsAvailable()) qoverp_exPV.SetActive(active);
     if(cb_d0_exPV.IsAvailable()) cb_d0_exPV.SetActive(active);
     if(cb_z0_exPV.IsAvailable()) cb_z0_exPV.SetActive(active);
     if(cb_phi_exPV.IsAvailable()) cb_phi_exPV.SetActive(active);
     if(cb_theta_exPV.IsAvailable()) cb_theta_exPV.SetActive(active);
     if(cb_qoverp_exPV.IsAvailable()) cb_qoverp_exPV.SetActive(active);
     if(id_d0_exPV.IsAvailable()) id_d0_exPV.SetActive(active);
     if(id_z0_exPV.IsAvailable()) id_z0_exPV.SetActive(active);
     if(id_phi_exPV.IsAvailable()) id_phi_exPV.SetActive(active);
     if(id_theta_exPV.IsAvailable()) id_theta_exPV.SetActive(active);
     if(id_qoverp_exPV.IsAvailable()) id_qoverp_exPV.SetActive(active);
     if(me_d0_exPV.IsAvailable()) me_d0_exPV.SetActive(active);
     if(me_z0_exPV.IsAvailable()) me_z0_exPV.SetActive(active);
     if(me_phi_exPV.IsAvailable()) me_phi_exPV.SetActive(active);
     if(me_theta_exPV.IsAvailable()) me_theta_exPV.SetActive(active);
     if(me_qoverp_exPV.IsAvailable()) me_qoverp_exPV.SetActive(active);
     if(cov_d0_exPV.IsAvailable()) cov_d0_exPV.SetActive(active);
     if(cov_z0_exPV.IsAvailable()) cov_z0_exPV.SetActive(active);
     if(cov_phi_exPV.IsAvailable()) cov_phi_exPV.SetActive(active);
     if(cov_theta_exPV.IsAvailable()) cov_theta_exPV.SetActive(active);
     if(cov_qoverp_exPV.IsAvailable()) cov_qoverp_exPV.SetActive(active);
     if(cov_d0_z0_exPV.IsAvailable()) cov_d0_z0_exPV.SetActive(active);
     if(cov_d0_phi_exPV.IsAvailable()) cov_d0_phi_exPV.SetActive(active);
     if(cov_d0_theta_exPV.IsAvailable()) cov_d0_theta_exPV.SetActive(active);
     if(cov_d0_qoverp_exPV.IsAvailable()) cov_d0_qoverp_exPV.SetActive(active);
     if(cov_z0_phi_exPV.IsAvailable()) cov_z0_phi_exPV.SetActive(active);
     if(cov_z0_theta_exPV.IsAvailable()) cov_z0_theta_exPV.SetActive(active);
     if(cov_z0_qoverp_exPV.IsAvailable()) cov_z0_qoverp_exPV.SetActive(active);
     if(cov_phi_theta_exPV.IsAvailable()) cov_phi_theta_exPV.SetActive(active);
     if(cov_phi_qoverp_exPV.IsAvailable()) cov_phi_qoverp_exPV.SetActive(active);
     if(cov_theta_qoverp_exPV.IsAvailable()) cov_theta_qoverp_exPV.SetActive(active);
     if(id_cov_d0_exPV.IsAvailable()) id_cov_d0_exPV.SetActive(active);
     if(id_cov_z0_exPV.IsAvailable()) id_cov_z0_exPV.SetActive(active);
     if(id_cov_phi_exPV.IsAvailable()) id_cov_phi_exPV.SetActive(active);
     if(id_cov_theta_exPV.IsAvailable()) id_cov_theta_exPV.SetActive(active);
     if(id_cov_qoverp_exPV.IsAvailable()) id_cov_qoverp_exPV.SetActive(active);
     if(id_cov_d0_z0_exPV.IsAvailable()) id_cov_d0_z0_exPV.SetActive(active);
     if(id_cov_d0_phi_exPV.IsAvailable()) id_cov_d0_phi_exPV.SetActive(active);
     if(id_cov_d0_theta_exPV.IsAvailable()) id_cov_d0_theta_exPV.SetActive(active);
     if(id_cov_d0_qoverp_exPV.IsAvailable()) id_cov_d0_qoverp_exPV.SetActive(active);
     if(id_cov_z0_phi_exPV.IsAvailable()) id_cov_z0_phi_exPV.SetActive(active);
     if(id_cov_z0_theta_exPV.IsAvailable()) id_cov_z0_theta_exPV.SetActive(active);
     if(id_cov_z0_qoverp_exPV.IsAvailable()) id_cov_z0_qoverp_exPV.SetActive(active);
     if(id_cov_phi_theta_exPV.IsAvailable()) id_cov_phi_theta_exPV.SetActive(active);
     if(id_cov_phi_qoverp_exPV.IsAvailable()) id_cov_phi_qoverp_exPV.SetActive(active);
     if(id_cov_theta_qoverp_exPV.IsAvailable()) id_cov_theta_qoverp_exPV.SetActive(active);
     if(me_cov_d0_exPV.IsAvailable()) me_cov_d0_exPV.SetActive(active);
     if(me_cov_z0_exPV.IsAvailable()) me_cov_z0_exPV.SetActive(active);
     if(me_cov_phi_exPV.IsAvailable()) me_cov_phi_exPV.SetActive(active);
     if(me_cov_theta_exPV.IsAvailable()) me_cov_theta_exPV.SetActive(active);
     if(me_cov_qoverp_exPV.IsAvailable()) me_cov_qoverp_exPV.SetActive(active);
     if(me_cov_d0_z0_exPV.IsAvailable()) me_cov_d0_z0_exPV.SetActive(active);
     if(me_cov_d0_phi_exPV.IsAvailable()) me_cov_d0_phi_exPV.SetActive(active);
     if(me_cov_d0_theta_exPV.IsAvailable()) me_cov_d0_theta_exPV.SetActive(active);
     if(me_cov_d0_qoverp_exPV.IsAvailable()) me_cov_d0_qoverp_exPV.SetActive(active);
     if(me_cov_z0_phi_exPV.IsAvailable()) me_cov_z0_phi_exPV.SetActive(active);
     if(me_cov_z0_theta_exPV.IsAvailable()) me_cov_z0_theta_exPV.SetActive(active);
     if(me_cov_z0_qoverp_exPV.IsAvailable()) me_cov_z0_qoverp_exPV.SetActive(active);
     if(me_cov_phi_theta_exPV.IsAvailable()) me_cov_phi_theta_exPV.SetActive(active);
     if(me_cov_phi_qoverp_exPV.IsAvailable()) me_cov_phi_qoverp_exPV.SetActive(active);
     if(me_cov_theta_qoverp_exPV.IsAvailable()) me_cov_theta_qoverp_exPV.SetActive(active);
     if(ms_d0.IsAvailable()) ms_d0.SetActive(active);
     if(ms_z0.IsAvailable()) ms_z0.SetActive(active);
     if(ms_phi.IsAvailable()) ms_phi.SetActive(active);
     if(ms_theta.IsAvailable()) ms_theta.SetActive(active);
     if(ms_qoverp.IsAvailable()) ms_qoverp.SetActive(active);
     if(id_d0.IsAvailable()) id_d0.SetActive(active);
     if(id_z0.IsAvailable()) id_z0.SetActive(active);
     if(id_phi.IsAvailable()) id_phi.SetActive(active);
     if(id_theta.IsAvailable()) id_theta.SetActive(active);
     if(id_qoverp.IsAvailable()) id_qoverp.SetActive(active);
     if(me_d0.IsAvailable()) me_d0.SetActive(active);
     if(me_z0.IsAvailable()) me_z0.SetActive(active);
     if(me_phi.IsAvailable()) me_phi.SetActive(active);
     if(me_theta.IsAvailable()) me_theta.SetActive(active);
     if(me_qoverp.IsAvailable()) me_qoverp.SetActive(active);
     if(nOutliersOnTrack.IsAvailable()) nOutliersOnTrack.SetActive(active);
     if(nBLHits.IsAvailable()) nBLHits.SetActive(active);
     if(nPixHits.IsAvailable()) nPixHits.SetActive(active);
     if(nSCTHits.IsAvailable()) nSCTHits.SetActive(active);
     if(nTRTHits.IsAvailable()) nTRTHits.SetActive(active);
     if(nTRTHighTHits.IsAvailable()) nTRTHighTHits.SetActive(active);
     if(nBLSharedHits.IsAvailable()) nBLSharedHits.SetActive(active);
     if(nPixSharedHits.IsAvailable()) nPixSharedHits.SetActive(active);
     if(nPixHoles.IsAvailable()) nPixHoles.SetActive(active);
     if(nSCTSharedHits.IsAvailable()) nSCTSharedHits.SetActive(active);
     if(nSCTHoles.IsAvailable()) nSCTHoles.SetActive(active);
     if(nTRTOutliers.IsAvailable()) nTRTOutliers.SetActive(active);
     if(nTRTHighTOutliers.IsAvailable()) nTRTHighTOutliers.SetActive(active);
     if(nGangedPixels.IsAvailable()) nGangedPixels.SetActive(active);
     if(nPixelDeadSensors.IsAvailable()) nPixelDeadSensors.SetActive(active);
     if(nSCTDeadSensors.IsAvailable()) nSCTDeadSensors.SetActive(active);
     if(nTRTDeadStraws.IsAvailable()) nTRTDeadStraws.SetActive(active);
     if(expectBLayerHit.IsAvailable()) expectBLayerHit.SetActive(active);
     if(nMDTHits.IsAvailable()) nMDTHits.SetActive(active);
     if(nMDTHoles.IsAvailable()) nMDTHoles.SetActive(active);
     if(nCSCEtaHits.IsAvailable()) nCSCEtaHits.SetActive(active);
     if(nCSCEtaHoles.IsAvailable()) nCSCEtaHoles.SetActive(active);
     if(nCSCUnspoiledEtaHits.IsAvailable()) nCSCUnspoiledEtaHits.SetActive(active);
     if(nCSCPhiHits.IsAvailable()) nCSCPhiHits.SetActive(active);
     if(nCSCPhiHoles.IsAvailable()) nCSCPhiHoles.SetActive(active);
     if(nRPCEtaHits.IsAvailable()) nRPCEtaHits.SetActive(active);
     if(nRPCEtaHoles.IsAvailable()) nRPCEtaHoles.SetActive(active);
     if(nRPCPhiHits.IsAvailable()) nRPCPhiHits.SetActive(active);
     if(nRPCPhiHoles.IsAvailable()) nRPCPhiHoles.SetActive(active);
     if(nTGCEtaHits.IsAvailable()) nTGCEtaHits.SetActive(active);
     if(nTGCEtaHoles.IsAvailable()) nTGCEtaHoles.SetActive(active);
     if(nTGCPhiHits.IsAvailable()) nTGCPhiHits.SetActive(active);
     if(nTGCPhiHoles.IsAvailable()) nTGCPhiHoles.SetActive(active);
     if(nprecisionLayers.IsAvailable()) nprecisionLayers.SetActive(active);
     if(nprecisionHoleLayers.IsAvailable()) nprecisionHoleLayers.SetActive(active);
     if(nphiLayers.IsAvailable()) nphiLayers.SetActive(active);
     if(ntrigEtaLayers.IsAvailable()) ntrigEtaLayers.SetActive(active);
     if(nphiHoleLayers.IsAvailable()) nphiHoleLayers.SetActive(active);
     if(ntrigEtaHoleLayers.IsAvailable()) ntrigEtaHoleLayers.SetActive(active);
     if(nMDTBIHits.IsAvailable()) nMDTBIHits.SetActive(active);
     if(nMDTBMHits.IsAvailable()) nMDTBMHits.SetActive(active);
     if(nMDTBOHits.IsAvailable()) nMDTBOHits.SetActive(active);
     if(nMDTBEEHits.IsAvailable()) nMDTBEEHits.SetActive(active);
     if(nMDTBIS78Hits.IsAvailable()) nMDTBIS78Hits.SetActive(active);
     if(nMDTEIHits.IsAvailable()) nMDTEIHits.SetActive(active);
     if(nMDTEMHits.IsAvailable()) nMDTEMHits.SetActive(active);
     if(nMDTEOHits.IsAvailable()) nMDTEOHits.SetActive(active);
     if(nMDTEEHits.IsAvailable()) nMDTEEHits.SetActive(active);
     if(nRPCLayer1EtaHits.IsAvailable()) nRPCLayer1EtaHits.SetActive(active);
     if(nRPCLayer2EtaHits.IsAvailable()) nRPCLayer2EtaHits.SetActive(active);
     if(nRPCLayer3EtaHits.IsAvailable()) nRPCLayer3EtaHits.SetActive(active);
     if(nRPCLayer1PhiHits.IsAvailable()) nRPCLayer1PhiHits.SetActive(active);
     if(nRPCLayer2PhiHits.IsAvailable()) nRPCLayer2PhiHits.SetActive(active);
     if(nRPCLayer3PhiHits.IsAvailable()) nRPCLayer3PhiHits.SetActive(active);
     if(nTGCLayer1EtaHits.IsAvailable()) nTGCLayer1EtaHits.SetActive(active);
     if(nTGCLayer2EtaHits.IsAvailable()) nTGCLayer2EtaHits.SetActive(active);
     if(nTGCLayer3EtaHits.IsAvailable()) nTGCLayer3EtaHits.SetActive(active);
     if(nTGCLayer4EtaHits.IsAvailable()) nTGCLayer4EtaHits.SetActive(active);
     if(nTGCLayer1PhiHits.IsAvailable()) nTGCLayer1PhiHits.SetActive(active);
     if(nTGCLayer2PhiHits.IsAvailable()) nTGCLayer2PhiHits.SetActive(active);
     if(nTGCLayer3PhiHits.IsAvailable()) nTGCLayer3PhiHits.SetActive(active);
     if(nTGCLayer4PhiHits.IsAvailable()) nTGCLayer4PhiHits.SetActive(active);
     if(barrelSectors.IsAvailable()) barrelSectors.SetActive(active);
     if(endcapSectors.IsAvailable()) endcapSectors.SetActive(active);
     if(spec_surf_px.IsAvailable()) spec_surf_px.SetActive(active);
     if(spec_surf_py.IsAvailable()) spec_surf_py.SetActive(active);
     if(spec_surf_pz.IsAvailable()) spec_surf_pz.SetActive(active);
     if(spec_surf_x.IsAvailable()) spec_surf_x.SetActive(active);
     if(spec_surf_y.IsAvailable()) spec_surf_y.SetActive(active);
     if(spec_surf_z.IsAvailable()) spec_surf_z.SetActive(active);
     if(trackd0.IsAvailable()) trackd0.SetActive(active);
     if(trackz0.IsAvailable()) trackz0.SetActive(active);
     if(trackphi.IsAvailable()) trackphi.SetActive(active);
     if(tracktheta.IsAvailable()) tracktheta.SetActive(active);
     if(trackqoverp.IsAvailable()) trackqoverp.SetActive(active);
     if(trackcov_d0.IsAvailable()) trackcov_d0.SetActive(active);
     if(trackcov_z0.IsAvailable()) trackcov_z0.SetActive(active);
     if(trackcov_phi.IsAvailable()) trackcov_phi.SetActive(active);
     if(trackcov_theta.IsAvailable()) trackcov_theta.SetActive(active);
     if(trackcov_qoverp.IsAvailable()) trackcov_qoverp.SetActive(active);
     if(trackcov_d0_z0.IsAvailable()) trackcov_d0_z0.SetActive(active);
     if(trackcov_d0_phi.IsAvailable()) trackcov_d0_phi.SetActive(active);
     if(trackcov_d0_theta.IsAvailable()) trackcov_d0_theta.SetActive(active);
     if(trackcov_d0_qoverp.IsAvailable()) trackcov_d0_qoverp.SetActive(active);
     if(trackcov_z0_phi.IsAvailable()) trackcov_z0_phi.SetActive(active);
     if(trackcov_z0_theta.IsAvailable()) trackcov_z0_theta.SetActive(active);
     if(trackcov_z0_qoverp.IsAvailable()) trackcov_z0_qoverp.SetActive(active);
     if(trackcov_phi_theta.IsAvailable()) trackcov_phi_theta.SetActive(active);
     if(trackcov_phi_qoverp.IsAvailable()) trackcov_phi_qoverp.SetActive(active);
     if(trackcov_theta_qoverp.IsAvailable()) trackcov_theta_qoverp.SetActive(active);
     if(trackfitchi2.IsAvailable()) trackfitchi2.SetActive(active);
     if(trackfitndof.IsAvailable()) trackfitndof.SetActive(active);
     if(hastrack.IsAvailable()) hastrack.SetActive(active);
     if(trackd0beam.IsAvailable()) trackd0beam.SetActive(active);
     if(trackz0beam.IsAvailable()) trackz0beam.SetActive(active);
     if(tracksigd0beam.IsAvailable()) tracksigd0beam.SetActive(active);
     if(tracksigz0beam.IsAvailable()) tracksigz0beam.SetActive(active);
     if(trackd0pv.IsAvailable()) trackd0pv.SetActive(active);
     if(trackz0pv.IsAvailable()) trackz0pv.SetActive(active);
     if(tracksigd0pv.IsAvailable()) tracksigd0pv.SetActive(active);
     if(tracksigz0pv.IsAvailable()) tracksigz0pv.SetActive(active);
     if(trackd0pvunbiased.IsAvailable()) trackd0pvunbiased.SetActive(active);
     if(trackz0pvunbiased.IsAvailable()) trackz0pvunbiased.SetActive(active);
     if(tracksigd0pvunbiased.IsAvailable()) tracksigd0pvunbiased.SetActive(active);
     if(tracksigz0pvunbiased.IsAvailable()) tracksigz0pvunbiased.SetActive(active);
     if(type.IsAvailable()) type.SetActive(active);
     if(origin.IsAvailable()) origin.SetActive(active);
     if(truth_dr.IsAvailable()) truth_dr.SetActive(active);
     if(truth_E.IsAvailable()) truth_E.SetActive(active);
     if(truth_pt.IsAvailable()) truth_pt.SetActive(active);
     if(truth_eta.IsAvailable()) truth_eta.SetActive(active);
     if(truth_phi.IsAvailable()) truth_phi.SetActive(active);
     if(truth_type.IsAvailable()) truth_type.SetActive(active);
     if(truth_status.IsAvailable()) truth_status.SetActive(active);
     if(truth_barcode.IsAvailable()) truth_barcode.SetActive(active);
     if(truth_mothertype.IsAvailable()) truth_mothertype.SetActive(active);
     if(truth_motherbarcode.IsAvailable()) truth_motherbarcode.SetActive(active);
     if(truth_matched.IsAvailable()) truth_matched.SetActive(active);
     if(L2CB_dr.IsAvailable()) L2CB_dr.SetActive(active);
     if(L2CB_pt.IsAvailable()) L2CB_pt.SetActive(active);
     if(L2CB_eta.IsAvailable()) L2CB_eta.SetActive(active);
     if(L2CB_phi.IsAvailable()) L2CB_phi.SetActive(active);
     if(L2CB_id_pt.IsAvailable()) L2CB_id_pt.SetActive(active);
     if(L2CB_ms_pt.IsAvailable()) L2CB_ms_pt.SetActive(active);
     if(L2CB_nPixHits.IsAvailable()) L2CB_nPixHits.SetActive(active);
     if(L2CB_nSCTHits.IsAvailable()) L2CB_nSCTHits.SetActive(active);
     if(L2CB_nTRTHits.IsAvailable()) L2CB_nTRTHits.SetActive(active);
     if(L2CB_nTRTHighTHits.IsAvailable()) L2CB_nTRTHighTHits.SetActive(active);
     if(L2CB_matched.IsAvailable()) L2CB_matched.SetActive(active);
     if(L1_index.IsAvailable()) L1_index.SetActive(active);
     if(L1_dr.IsAvailable()) L1_dr.SetActive(active);
     if(L1_pt.IsAvailable()) L1_pt.SetActive(active);
     if(L1_eta.IsAvailable()) L1_eta.SetActive(active);
     if(L1_phi.IsAvailable()) L1_phi.SetActive(active);
     if(L1_thrNumber.IsAvailable()) L1_thrNumber.SetActive(active);
     if(L1_RoINumber.IsAvailable()) L1_RoINumber.SetActive(active);
     if(L1_sectorAddress.IsAvailable()) L1_sectorAddress.SetActive(active);
     if(L1_firstCandidate.IsAvailable()) L1_firstCandidate.SetActive(active);
     if(L1_moreCandInRoI.IsAvailable()) L1_moreCandInRoI.SetActive(active);
     if(L1_moreCandInSector.IsAvailable()) L1_moreCandInSector.SetActive(active);
     if(L1_source.IsAvailable()) L1_source.SetActive(active);
     if(L1_hemisphere.IsAvailable()) L1_hemisphere.SetActive(active);
     if(L1_charge.IsAvailable()) L1_charge.SetActive(active);
     if(L1_vetoed.IsAvailable()) L1_vetoed.SetActive(active);
     if(L1_matched.IsAvailable()) L1_matched.SetActive(active);
     if(truthAssoc_index.IsAvailable()) truthAssoc_index.SetActive(active);
     if(MI10_max40_ptsum.IsAvailable()) MI10_max40_ptsum.SetActive(active);
     if(MI10_max40_nTrks.IsAvailable()) MI10_max40_nTrks.SetActive(active);
     if(MI10_max30_ptsum.IsAvailable()) MI10_max30_ptsum.SetActive(active);
     if(MI10_max30_nTrks.IsAvailable()) MI10_max30_nTrks.SetActive(active);
     if(MI15_max40_ptsum.IsAvailable()) MI15_max40_ptsum.SetActive(active);
     if(MI15_max40_nTrks.IsAvailable()) MI15_max40_nTrks.SetActive(active);
     if(MI15_max30_ptsum.IsAvailable()) MI15_max30_ptsum.SetActive(active);
     if(MI15_max30_nTrks.IsAvailable()) MI15_max30_nTrks.SetActive(active);
    }
    else
    {
      MET_n.SetActive(active);
      MET_wpx.SetActive(active);
      MET_wpy.SetActive(active);
      MET_wet.SetActive(active);
      MET_statusWord.SetActive(active);
      MET_BDTMedium_n.SetActive(active);
      MET_BDTMedium_wpx.SetActive(active);
      MET_BDTMedium_wpy.SetActive(active);
      MET_BDTMedium_wet.SetActive(active);
      MET_BDTMedium_statusWord.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_n.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_wpx.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_wpy.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_wet.SetActive(active);
      MET_AntiKt4LCTopo_tightpp_statusWord.SetActive(active);
      n.SetActive(active);
      E.SetActive(active);
      pt.SetActive(active);
      m.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      px.SetActive(active);
      py.SetActive(active);
      pz.SetActive(active);
      charge.SetActive(active);
      allauthor.SetActive(active);
      author.SetActive(active);
      beta.SetActive(active);
      isMuonLikelihood.SetActive(active);
      matchchi2.SetActive(active);
      matchndof.SetActive(active);
      etcone20.SetActive(active);
      etcone30.SetActive(active);
      etcone40.SetActive(active);
      nucone20.SetActive(active);
      nucone30.SetActive(active);
      nucone40.SetActive(active);
      ptcone20.SetActive(active);
      ptcone30.SetActive(active);
      ptcone40.SetActive(active);
      etconeNoEm10.SetActive(active);
      etconeNoEm20.SetActive(active);
      etconeNoEm30.SetActive(active);
      etconeNoEm40.SetActive(active);
      momentumBalanceSignificance.SetActive(active);
      energyLossPar.SetActive(active);
      energyLossErr.SetActive(active);
      etCore.SetActive(active);
      energyLossType.SetActive(active);
      caloMuonIdTag.SetActive(active);
      caloLRLikelihood.SetActive(active);
      bestMatch.SetActive(active);
      isStandAloneMuon.SetActive(active);
      isCombinedMuon.SetActive(active);
      isLowPtReconstructedMuon.SetActive(active);
      isSegmentTaggedMuon.SetActive(active);
      isCaloMuonId.SetActive(active);
      alsoFoundByLowPt.SetActive(active);
      alsoFoundByCaloMuonId.SetActive(active);
      isSiliconAssociatedForwardMuon.SetActive(active);
      loose.SetActive(active);
      medium.SetActive(active);
      tight.SetActive(active);
      d0_exPV.SetActive(active);
      z0_exPV.SetActive(active);
      phi_exPV.SetActive(active);
      theta_exPV.SetActive(active);
      qoverp_exPV.SetActive(active);
      cb_d0_exPV.SetActive(active);
      cb_z0_exPV.SetActive(active);
      cb_phi_exPV.SetActive(active);
      cb_theta_exPV.SetActive(active);
      cb_qoverp_exPV.SetActive(active);
      id_d0_exPV.SetActive(active);
      id_z0_exPV.SetActive(active);
      id_phi_exPV.SetActive(active);
      id_theta_exPV.SetActive(active);
      id_qoverp_exPV.SetActive(active);
      me_d0_exPV.SetActive(active);
      me_z0_exPV.SetActive(active);
      me_phi_exPV.SetActive(active);
      me_theta_exPV.SetActive(active);
      me_qoverp_exPV.SetActive(active);
      cov_d0_exPV.SetActive(active);
      cov_z0_exPV.SetActive(active);
      cov_phi_exPV.SetActive(active);
      cov_theta_exPV.SetActive(active);
      cov_qoverp_exPV.SetActive(active);
      cov_d0_z0_exPV.SetActive(active);
      cov_d0_phi_exPV.SetActive(active);
      cov_d0_theta_exPV.SetActive(active);
      cov_d0_qoverp_exPV.SetActive(active);
      cov_z0_phi_exPV.SetActive(active);
      cov_z0_theta_exPV.SetActive(active);
      cov_z0_qoverp_exPV.SetActive(active);
      cov_phi_theta_exPV.SetActive(active);
      cov_phi_qoverp_exPV.SetActive(active);
      cov_theta_qoverp_exPV.SetActive(active);
      id_cov_d0_exPV.SetActive(active);
      id_cov_z0_exPV.SetActive(active);
      id_cov_phi_exPV.SetActive(active);
      id_cov_theta_exPV.SetActive(active);
      id_cov_qoverp_exPV.SetActive(active);
      id_cov_d0_z0_exPV.SetActive(active);
      id_cov_d0_phi_exPV.SetActive(active);
      id_cov_d0_theta_exPV.SetActive(active);
      id_cov_d0_qoverp_exPV.SetActive(active);
      id_cov_z0_phi_exPV.SetActive(active);
      id_cov_z0_theta_exPV.SetActive(active);
      id_cov_z0_qoverp_exPV.SetActive(active);
      id_cov_phi_theta_exPV.SetActive(active);
      id_cov_phi_qoverp_exPV.SetActive(active);
      id_cov_theta_qoverp_exPV.SetActive(active);
      me_cov_d0_exPV.SetActive(active);
      me_cov_z0_exPV.SetActive(active);
      me_cov_phi_exPV.SetActive(active);
      me_cov_theta_exPV.SetActive(active);
      me_cov_qoverp_exPV.SetActive(active);
      me_cov_d0_z0_exPV.SetActive(active);
      me_cov_d0_phi_exPV.SetActive(active);
      me_cov_d0_theta_exPV.SetActive(active);
      me_cov_d0_qoverp_exPV.SetActive(active);
      me_cov_z0_phi_exPV.SetActive(active);
      me_cov_z0_theta_exPV.SetActive(active);
      me_cov_z0_qoverp_exPV.SetActive(active);
      me_cov_phi_theta_exPV.SetActive(active);
      me_cov_phi_qoverp_exPV.SetActive(active);
      me_cov_theta_qoverp_exPV.SetActive(active);
      ms_d0.SetActive(active);
      ms_z0.SetActive(active);
      ms_phi.SetActive(active);
      ms_theta.SetActive(active);
      ms_qoverp.SetActive(active);
      id_d0.SetActive(active);
      id_z0.SetActive(active);
      id_phi.SetActive(active);
      id_theta.SetActive(active);
      id_qoverp.SetActive(active);
      me_d0.SetActive(active);
      me_z0.SetActive(active);
      me_phi.SetActive(active);
      me_theta.SetActive(active);
      me_qoverp.SetActive(active);
      nOutliersOnTrack.SetActive(active);
      nBLHits.SetActive(active);
      nPixHits.SetActive(active);
      nSCTHits.SetActive(active);
      nTRTHits.SetActive(active);
      nTRTHighTHits.SetActive(active);
      nBLSharedHits.SetActive(active);
      nPixSharedHits.SetActive(active);
      nPixHoles.SetActive(active);
      nSCTSharedHits.SetActive(active);
      nSCTHoles.SetActive(active);
      nTRTOutliers.SetActive(active);
      nTRTHighTOutliers.SetActive(active);
      nGangedPixels.SetActive(active);
      nPixelDeadSensors.SetActive(active);
      nSCTDeadSensors.SetActive(active);
      nTRTDeadStraws.SetActive(active);
      expectBLayerHit.SetActive(active);
      nMDTHits.SetActive(active);
      nMDTHoles.SetActive(active);
      nCSCEtaHits.SetActive(active);
      nCSCEtaHoles.SetActive(active);
      nCSCUnspoiledEtaHits.SetActive(active);
      nCSCPhiHits.SetActive(active);
      nCSCPhiHoles.SetActive(active);
      nRPCEtaHits.SetActive(active);
      nRPCEtaHoles.SetActive(active);
      nRPCPhiHits.SetActive(active);
      nRPCPhiHoles.SetActive(active);
      nTGCEtaHits.SetActive(active);
      nTGCEtaHoles.SetActive(active);
      nTGCPhiHits.SetActive(active);
      nTGCPhiHoles.SetActive(active);
      nprecisionLayers.SetActive(active);
      nprecisionHoleLayers.SetActive(active);
      nphiLayers.SetActive(active);
      ntrigEtaLayers.SetActive(active);
      nphiHoleLayers.SetActive(active);
      ntrigEtaHoleLayers.SetActive(active);
      nMDTBIHits.SetActive(active);
      nMDTBMHits.SetActive(active);
      nMDTBOHits.SetActive(active);
      nMDTBEEHits.SetActive(active);
      nMDTBIS78Hits.SetActive(active);
      nMDTEIHits.SetActive(active);
      nMDTEMHits.SetActive(active);
      nMDTEOHits.SetActive(active);
      nMDTEEHits.SetActive(active);
      nRPCLayer1EtaHits.SetActive(active);
      nRPCLayer2EtaHits.SetActive(active);
      nRPCLayer3EtaHits.SetActive(active);
      nRPCLayer1PhiHits.SetActive(active);
      nRPCLayer2PhiHits.SetActive(active);
      nRPCLayer3PhiHits.SetActive(active);
      nTGCLayer1EtaHits.SetActive(active);
      nTGCLayer2EtaHits.SetActive(active);
      nTGCLayer3EtaHits.SetActive(active);
      nTGCLayer4EtaHits.SetActive(active);
      nTGCLayer1PhiHits.SetActive(active);
      nTGCLayer2PhiHits.SetActive(active);
      nTGCLayer3PhiHits.SetActive(active);
      nTGCLayer4PhiHits.SetActive(active);
      barrelSectors.SetActive(active);
      endcapSectors.SetActive(active);
      spec_surf_px.SetActive(active);
      spec_surf_py.SetActive(active);
      spec_surf_pz.SetActive(active);
      spec_surf_x.SetActive(active);
      spec_surf_y.SetActive(active);
      spec_surf_z.SetActive(active);
      trackd0.SetActive(active);
      trackz0.SetActive(active);
      trackphi.SetActive(active);
      tracktheta.SetActive(active);
      trackqoverp.SetActive(active);
      trackcov_d0.SetActive(active);
      trackcov_z0.SetActive(active);
      trackcov_phi.SetActive(active);
      trackcov_theta.SetActive(active);
      trackcov_qoverp.SetActive(active);
      trackcov_d0_z0.SetActive(active);
      trackcov_d0_phi.SetActive(active);
      trackcov_d0_theta.SetActive(active);
      trackcov_d0_qoverp.SetActive(active);
      trackcov_z0_phi.SetActive(active);
      trackcov_z0_theta.SetActive(active);
      trackcov_z0_qoverp.SetActive(active);
      trackcov_phi_theta.SetActive(active);
      trackcov_phi_qoverp.SetActive(active);
      trackcov_theta_qoverp.SetActive(active);
      trackfitchi2.SetActive(active);
      trackfitndof.SetActive(active);
      hastrack.SetActive(active);
      trackd0beam.SetActive(active);
      trackz0beam.SetActive(active);
      tracksigd0beam.SetActive(active);
      tracksigz0beam.SetActive(active);
      trackd0pv.SetActive(active);
      trackz0pv.SetActive(active);
      tracksigd0pv.SetActive(active);
      tracksigz0pv.SetActive(active);
      trackd0pvunbiased.SetActive(active);
      trackz0pvunbiased.SetActive(active);
      tracksigd0pvunbiased.SetActive(active);
      tracksigz0pvunbiased.SetActive(active);
      type.SetActive(active);
      origin.SetActive(active);
      truth_dr.SetActive(active);
      truth_E.SetActive(active);
      truth_pt.SetActive(active);
      truth_eta.SetActive(active);
      truth_phi.SetActive(active);
      truth_type.SetActive(active);
      truth_status.SetActive(active);
      truth_barcode.SetActive(active);
      truth_mothertype.SetActive(active);
      truth_motherbarcode.SetActive(active);
      truth_matched.SetActive(active);
      L2CB_dr.SetActive(active);
      L2CB_pt.SetActive(active);
      L2CB_eta.SetActive(active);
      L2CB_phi.SetActive(active);
      L2CB_id_pt.SetActive(active);
      L2CB_ms_pt.SetActive(active);
      L2CB_nPixHits.SetActive(active);
      L2CB_nSCTHits.SetActive(active);
      L2CB_nTRTHits.SetActive(active);
      L2CB_nTRTHighTHits.SetActive(active);
      L2CB_matched.SetActive(active);
      L1_index.SetActive(active);
      L1_dr.SetActive(active);
      L1_pt.SetActive(active);
      L1_eta.SetActive(active);
      L1_phi.SetActive(active);
      L1_thrNumber.SetActive(active);
      L1_RoINumber.SetActive(active);
      L1_sectorAddress.SetActive(active);
      L1_firstCandidate.SetActive(active);
      L1_moreCandInRoI.SetActive(active);
      L1_moreCandInSector.SetActive(active);
      L1_source.SetActive(active);
      L1_hemisphere.SetActive(active);
      L1_charge.SetActive(active);
      L1_vetoed.SetActive(active);
      L1_matched.SetActive(active);
      truthAssoc_index.SetActive(active);
      MI10_max40_ptsum.SetActive(active);
      MI10_max40_nTrks.SetActive(active);
      MI10_max30_ptsum.SetActive(active);
      MI10_max30_nTrks.SetActive(active);
      MI15_max40_ptsum.SetActive(active);
      MI15_max40_nTrks.SetActive(active);
      MI15_max30_ptsum.SetActive(active);
      MI15_max30_nTrks.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void MuonD3PDCollection::ReadAllActive()
  {
    if(MET_n.IsActive()) MET_n();
    if(MET_wpx.IsActive()) MET_wpx();
    if(MET_wpy.IsActive()) MET_wpy();
    if(MET_wet.IsActive()) MET_wet();
    if(MET_statusWord.IsActive()) MET_statusWord();
    if(MET_BDTMedium_n.IsActive()) MET_BDTMedium_n();
    if(MET_BDTMedium_wpx.IsActive()) MET_BDTMedium_wpx();
    if(MET_BDTMedium_wpy.IsActive()) MET_BDTMedium_wpy();
    if(MET_BDTMedium_wet.IsActive()) MET_BDTMedium_wet();
    if(MET_BDTMedium_statusWord.IsActive()) MET_BDTMedium_statusWord();
    if(MET_AntiKt4LCTopo_tightpp_n.IsActive()) MET_AntiKt4LCTopo_tightpp_n();
    if(MET_AntiKt4LCTopo_tightpp_wpx.IsActive()) MET_AntiKt4LCTopo_tightpp_wpx();
    if(MET_AntiKt4LCTopo_tightpp_wpy.IsActive()) MET_AntiKt4LCTopo_tightpp_wpy();
    if(MET_AntiKt4LCTopo_tightpp_wet.IsActive()) MET_AntiKt4LCTopo_tightpp_wet();
    if(MET_AntiKt4LCTopo_tightpp_statusWord.IsActive()) MET_AntiKt4LCTopo_tightpp_statusWord();
    if(n.IsActive()) n();
    if(E.IsActive()) E();
    if(pt.IsActive()) pt();
    if(m.IsActive()) m();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(px.IsActive()) px();
    if(py.IsActive()) py();
    if(pz.IsActive()) pz();
    if(charge.IsActive()) charge();
    if(allauthor.IsActive()) allauthor();
    if(author.IsActive()) author();
    if(beta.IsActive()) beta();
    if(isMuonLikelihood.IsActive()) isMuonLikelihood();
    if(matchchi2.IsActive()) matchchi2();
    if(matchndof.IsActive()) matchndof();
    if(etcone20.IsActive()) etcone20();
    if(etcone30.IsActive()) etcone30();
    if(etcone40.IsActive()) etcone40();
    if(nucone20.IsActive()) nucone20();
    if(nucone30.IsActive()) nucone30();
    if(nucone40.IsActive()) nucone40();
    if(ptcone20.IsActive()) ptcone20();
    if(ptcone30.IsActive()) ptcone30();
    if(ptcone40.IsActive()) ptcone40();
    if(etconeNoEm10.IsActive()) etconeNoEm10();
    if(etconeNoEm20.IsActive()) etconeNoEm20();
    if(etconeNoEm30.IsActive()) etconeNoEm30();
    if(etconeNoEm40.IsActive()) etconeNoEm40();
    if(momentumBalanceSignificance.IsActive()) momentumBalanceSignificance();
    if(energyLossPar.IsActive()) energyLossPar();
    if(energyLossErr.IsActive()) energyLossErr();
    if(etCore.IsActive()) etCore();
    if(energyLossType.IsActive()) energyLossType();
    if(caloMuonIdTag.IsActive()) caloMuonIdTag();
    if(caloLRLikelihood.IsActive()) caloLRLikelihood();
    if(bestMatch.IsActive()) bestMatch();
    if(isStandAloneMuon.IsActive()) isStandAloneMuon();
    if(isCombinedMuon.IsActive()) isCombinedMuon();
    if(isLowPtReconstructedMuon.IsActive()) isLowPtReconstructedMuon();
    if(isSegmentTaggedMuon.IsActive()) isSegmentTaggedMuon();
    if(isCaloMuonId.IsActive()) isCaloMuonId();
    if(alsoFoundByLowPt.IsActive()) alsoFoundByLowPt();
    if(alsoFoundByCaloMuonId.IsActive()) alsoFoundByCaloMuonId();
    if(isSiliconAssociatedForwardMuon.IsActive()) isSiliconAssociatedForwardMuon();
    if(loose.IsActive()) loose();
    if(medium.IsActive()) medium();
    if(tight.IsActive()) tight();
    if(d0_exPV.IsActive()) d0_exPV();
    if(z0_exPV.IsActive()) z0_exPV();
    if(phi_exPV.IsActive()) phi_exPV();
    if(theta_exPV.IsActive()) theta_exPV();
    if(qoverp_exPV.IsActive()) qoverp_exPV();
    if(cb_d0_exPV.IsActive()) cb_d0_exPV();
    if(cb_z0_exPV.IsActive()) cb_z0_exPV();
    if(cb_phi_exPV.IsActive()) cb_phi_exPV();
    if(cb_theta_exPV.IsActive()) cb_theta_exPV();
    if(cb_qoverp_exPV.IsActive()) cb_qoverp_exPV();
    if(id_d0_exPV.IsActive()) id_d0_exPV();
    if(id_z0_exPV.IsActive()) id_z0_exPV();
    if(id_phi_exPV.IsActive()) id_phi_exPV();
    if(id_theta_exPV.IsActive()) id_theta_exPV();
    if(id_qoverp_exPV.IsActive()) id_qoverp_exPV();
    if(me_d0_exPV.IsActive()) me_d0_exPV();
    if(me_z0_exPV.IsActive()) me_z0_exPV();
    if(me_phi_exPV.IsActive()) me_phi_exPV();
    if(me_theta_exPV.IsActive()) me_theta_exPV();
    if(me_qoverp_exPV.IsActive()) me_qoverp_exPV();
    if(cov_d0_exPV.IsActive()) cov_d0_exPV();
    if(cov_z0_exPV.IsActive()) cov_z0_exPV();
    if(cov_phi_exPV.IsActive()) cov_phi_exPV();
    if(cov_theta_exPV.IsActive()) cov_theta_exPV();
    if(cov_qoverp_exPV.IsActive()) cov_qoverp_exPV();
    if(cov_d0_z0_exPV.IsActive()) cov_d0_z0_exPV();
    if(cov_d0_phi_exPV.IsActive()) cov_d0_phi_exPV();
    if(cov_d0_theta_exPV.IsActive()) cov_d0_theta_exPV();
    if(cov_d0_qoverp_exPV.IsActive()) cov_d0_qoverp_exPV();
    if(cov_z0_phi_exPV.IsActive()) cov_z0_phi_exPV();
    if(cov_z0_theta_exPV.IsActive()) cov_z0_theta_exPV();
    if(cov_z0_qoverp_exPV.IsActive()) cov_z0_qoverp_exPV();
    if(cov_phi_theta_exPV.IsActive()) cov_phi_theta_exPV();
    if(cov_phi_qoverp_exPV.IsActive()) cov_phi_qoverp_exPV();
    if(cov_theta_qoverp_exPV.IsActive()) cov_theta_qoverp_exPV();
    if(id_cov_d0_exPV.IsActive()) id_cov_d0_exPV();
    if(id_cov_z0_exPV.IsActive()) id_cov_z0_exPV();
    if(id_cov_phi_exPV.IsActive()) id_cov_phi_exPV();
    if(id_cov_theta_exPV.IsActive()) id_cov_theta_exPV();
    if(id_cov_qoverp_exPV.IsActive()) id_cov_qoverp_exPV();
    if(id_cov_d0_z0_exPV.IsActive()) id_cov_d0_z0_exPV();
    if(id_cov_d0_phi_exPV.IsActive()) id_cov_d0_phi_exPV();
    if(id_cov_d0_theta_exPV.IsActive()) id_cov_d0_theta_exPV();
    if(id_cov_d0_qoverp_exPV.IsActive()) id_cov_d0_qoverp_exPV();
    if(id_cov_z0_phi_exPV.IsActive()) id_cov_z0_phi_exPV();
    if(id_cov_z0_theta_exPV.IsActive()) id_cov_z0_theta_exPV();
    if(id_cov_z0_qoverp_exPV.IsActive()) id_cov_z0_qoverp_exPV();
    if(id_cov_phi_theta_exPV.IsActive()) id_cov_phi_theta_exPV();
    if(id_cov_phi_qoverp_exPV.IsActive()) id_cov_phi_qoverp_exPV();
    if(id_cov_theta_qoverp_exPV.IsActive()) id_cov_theta_qoverp_exPV();
    if(me_cov_d0_exPV.IsActive()) me_cov_d0_exPV();
    if(me_cov_z0_exPV.IsActive()) me_cov_z0_exPV();
    if(me_cov_phi_exPV.IsActive()) me_cov_phi_exPV();
    if(me_cov_theta_exPV.IsActive()) me_cov_theta_exPV();
    if(me_cov_qoverp_exPV.IsActive()) me_cov_qoverp_exPV();
    if(me_cov_d0_z0_exPV.IsActive()) me_cov_d0_z0_exPV();
    if(me_cov_d0_phi_exPV.IsActive()) me_cov_d0_phi_exPV();
    if(me_cov_d0_theta_exPV.IsActive()) me_cov_d0_theta_exPV();
    if(me_cov_d0_qoverp_exPV.IsActive()) me_cov_d0_qoverp_exPV();
    if(me_cov_z0_phi_exPV.IsActive()) me_cov_z0_phi_exPV();
    if(me_cov_z0_theta_exPV.IsActive()) me_cov_z0_theta_exPV();
    if(me_cov_z0_qoverp_exPV.IsActive()) me_cov_z0_qoverp_exPV();
    if(me_cov_phi_theta_exPV.IsActive()) me_cov_phi_theta_exPV();
    if(me_cov_phi_qoverp_exPV.IsActive()) me_cov_phi_qoverp_exPV();
    if(me_cov_theta_qoverp_exPV.IsActive()) me_cov_theta_qoverp_exPV();
    if(ms_d0.IsActive()) ms_d0();
    if(ms_z0.IsActive()) ms_z0();
    if(ms_phi.IsActive()) ms_phi();
    if(ms_theta.IsActive()) ms_theta();
    if(ms_qoverp.IsActive()) ms_qoverp();
    if(id_d0.IsActive()) id_d0();
    if(id_z0.IsActive()) id_z0();
    if(id_phi.IsActive()) id_phi();
    if(id_theta.IsActive()) id_theta();
    if(id_qoverp.IsActive()) id_qoverp();
    if(me_d0.IsActive()) me_d0();
    if(me_z0.IsActive()) me_z0();
    if(me_phi.IsActive()) me_phi();
    if(me_theta.IsActive()) me_theta();
    if(me_qoverp.IsActive()) me_qoverp();
    if(nOutliersOnTrack.IsActive()) nOutliersOnTrack();
    if(nBLHits.IsActive()) nBLHits();
    if(nPixHits.IsActive()) nPixHits();
    if(nSCTHits.IsActive()) nSCTHits();
    if(nTRTHits.IsActive()) nTRTHits();
    if(nTRTHighTHits.IsActive()) nTRTHighTHits();
    if(nBLSharedHits.IsActive()) nBLSharedHits();
    if(nPixSharedHits.IsActive()) nPixSharedHits();
    if(nPixHoles.IsActive()) nPixHoles();
    if(nSCTSharedHits.IsActive()) nSCTSharedHits();
    if(nSCTHoles.IsActive()) nSCTHoles();
    if(nTRTOutliers.IsActive()) nTRTOutliers();
    if(nTRTHighTOutliers.IsActive()) nTRTHighTOutliers();
    if(nGangedPixels.IsActive()) nGangedPixels();
    if(nPixelDeadSensors.IsActive()) nPixelDeadSensors();
    if(nSCTDeadSensors.IsActive()) nSCTDeadSensors();
    if(nTRTDeadStraws.IsActive()) nTRTDeadStraws();
    if(expectBLayerHit.IsActive()) expectBLayerHit();
    if(nMDTHits.IsActive()) nMDTHits();
    if(nMDTHoles.IsActive()) nMDTHoles();
    if(nCSCEtaHits.IsActive()) nCSCEtaHits();
    if(nCSCEtaHoles.IsActive()) nCSCEtaHoles();
    if(nCSCUnspoiledEtaHits.IsActive()) nCSCUnspoiledEtaHits();
    if(nCSCPhiHits.IsActive()) nCSCPhiHits();
    if(nCSCPhiHoles.IsActive()) nCSCPhiHoles();
    if(nRPCEtaHits.IsActive()) nRPCEtaHits();
    if(nRPCEtaHoles.IsActive()) nRPCEtaHoles();
    if(nRPCPhiHits.IsActive()) nRPCPhiHits();
    if(nRPCPhiHoles.IsActive()) nRPCPhiHoles();
    if(nTGCEtaHits.IsActive()) nTGCEtaHits();
    if(nTGCEtaHoles.IsActive()) nTGCEtaHoles();
    if(nTGCPhiHits.IsActive()) nTGCPhiHits();
    if(nTGCPhiHoles.IsActive()) nTGCPhiHoles();
    if(nprecisionLayers.IsActive()) nprecisionLayers();
    if(nprecisionHoleLayers.IsActive()) nprecisionHoleLayers();
    if(nphiLayers.IsActive()) nphiLayers();
    if(ntrigEtaLayers.IsActive()) ntrigEtaLayers();
    if(nphiHoleLayers.IsActive()) nphiHoleLayers();
    if(ntrigEtaHoleLayers.IsActive()) ntrigEtaHoleLayers();
    if(nMDTBIHits.IsActive()) nMDTBIHits();
    if(nMDTBMHits.IsActive()) nMDTBMHits();
    if(nMDTBOHits.IsActive()) nMDTBOHits();
    if(nMDTBEEHits.IsActive()) nMDTBEEHits();
    if(nMDTBIS78Hits.IsActive()) nMDTBIS78Hits();
    if(nMDTEIHits.IsActive()) nMDTEIHits();
    if(nMDTEMHits.IsActive()) nMDTEMHits();
    if(nMDTEOHits.IsActive()) nMDTEOHits();
    if(nMDTEEHits.IsActive()) nMDTEEHits();
    if(nRPCLayer1EtaHits.IsActive()) nRPCLayer1EtaHits();
    if(nRPCLayer2EtaHits.IsActive()) nRPCLayer2EtaHits();
    if(nRPCLayer3EtaHits.IsActive()) nRPCLayer3EtaHits();
    if(nRPCLayer1PhiHits.IsActive()) nRPCLayer1PhiHits();
    if(nRPCLayer2PhiHits.IsActive()) nRPCLayer2PhiHits();
    if(nRPCLayer3PhiHits.IsActive()) nRPCLayer3PhiHits();
    if(nTGCLayer1EtaHits.IsActive()) nTGCLayer1EtaHits();
    if(nTGCLayer2EtaHits.IsActive()) nTGCLayer2EtaHits();
    if(nTGCLayer3EtaHits.IsActive()) nTGCLayer3EtaHits();
    if(nTGCLayer4EtaHits.IsActive()) nTGCLayer4EtaHits();
    if(nTGCLayer1PhiHits.IsActive()) nTGCLayer1PhiHits();
    if(nTGCLayer2PhiHits.IsActive()) nTGCLayer2PhiHits();
    if(nTGCLayer3PhiHits.IsActive()) nTGCLayer3PhiHits();
    if(nTGCLayer4PhiHits.IsActive()) nTGCLayer4PhiHits();
    if(barrelSectors.IsActive()) barrelSectors();
    if(endcapSectors.IsActive()) endcapSectors();
    if(spec_surf_px.IsActive()) spec_surf_px();
    if(spec_surf_py.IsActive()) spec_surf_py();
    if(spec_surf_pz.IsActive()) spec_surf_pz();
    if(spec_surf_x.IsActive()) spec_surf_x();
    if(spec_surf_y.IsActive()) spec_surf_y();
    if(spec_surf_z.IsActive()) spec_surf_z();
    if(trackd0.IsActive()) trackd0();
    if(trackz0.IsActive()) trackz0();
    if(trackphi.IsActive()) trackphi();
    if(tracktheta.IsActive()) tracktheta();
    if(trackqoverp.IsActive()) trackqoverp();
    if(trackcov_d0.IsActive()) trackcov_d0();
    if(trackcov_z0.IsActive()) trackcov_z0();
    if(trackcov_phi.IsActive()) trackcov_phi();
    if(trackcov_theta.IsActive()) trackcov_theta();
    if(trackcov_qoverp.IsActive()) trackcov_qoverp();
    if(trackcov_d0_z0.IsActive()) trackcov_d0_z0();
    if(trackcov_d0_phi.IsActive()) trackcov_d0_phi();
    if(trackcov_d0_theta.IsActive()) trackcov_d0_theta();
    if(trackcov_d0_qoverp.IsActive()) trackcov_d0_qoverp();
    if(trackcov_z0_phi.IsActive()) trackcov_z0_phi();
    if(trackcov_z0_theta.IsActive()) trackcov_z0_theta();
    if(trackcov_z0_qoverp.IsActive()) trackcov_z0_qoverp();
    if(trackcov_phi_theta.IsActive()) trackcov_phi_theta();
    if(trackcov_phi_qoverp.IsActive()) trackcov_phi_qoverp();
    if(trackcov_theta_qoverp.IsActive()) trackcov_theta_qoverp();
    if(trackfitchi2.IsActive()) trackfitchi2();
    if(trackfitndof.IsActive()) trackfitndof();
    if(hastrack.IsActive()) hastrack();
    if(trackd0beam.IsActive()) trackd0beam();
    if(trackz0beam.IsActive()) trackz0beam();
    if(tracksigd0beam.IsActive()) tracksigd0beam();
    if(tracksigz0beam.IsActive()) tracksigz0beam();
    if(trackd0pv.IsActive()) trackd0pv();
    if(trackz0pv.IsActive()) trackz0pv();
    if(tracksigd0pv.IsActive()) tracksigd0pv();
    if(tracksigz0pv.IsActive()) tracksigz0pv();
    if(trackd0pvunbiased.IsActive()) trackd0pvunbiased();
    if(trackz0pvunbiased.IsActive()) trackz0pvunbiased();
    if(tracksigd0pvunbiased.IsActive()) tracksigd0pvunbiased();
    if(tracksigz0pvunbiased.IsActive()) tracksigz0pvunbiased();
    if(type.IsActive()) type();
    if(origin.IsActive()) origin();
    if(truth_dr.IsActive()) truth_dr();
    if(truth_E.IsActive()) truth_E();
    if(truth_pt.IsActive()) truth_pt();
    if(truth_eta.IsActive()) truth_eta();
    if(truth_phi.IsActive()) truth_phi();
    if(truth_type.IsActive()) truth_type();
    if(truth_status.IsActive()) truth_status();
    if(truth_barcode.IsActive()) truth_barcode();
    if(truth_mothertype.IsActive()) truth_mothertype();
    if(truth_motherbarcode.IsActive()) truth_motherbarcode();
    if(truth_matched.IsActive()) truth_matched();
    if(L2CB_dr.IsActive()) L2CB_dr();
    if(L2CB_pt.IsActive()) L2CB_pt();
    if(L2CB_eta.IsActive()) L2CB_eta();
    if(L2CB_phi.IsActive()) L2CB_phi();
    if(L2CB_id_pt.IsActive()) L2CB_id_pt();
    if(L2CB_ms_pt.IsActive()) L2CB_ms_pt();
    if(L2CB_nPixHits.IsActive()) L2CB_nPixHits();
    if(L2CB_nSCTHits.IsActive()) L2CB_nSCTHits();
    if(L2CB_nTRTHits.IsActive()) L2CB_nTRTHits();
    if(L2CB_nTRTHighTHits.IsActive()) L2CB_nTRTHighTHits();
    if(L2CB_matched.IsActive()) L2CB_matched();
    if(L1_index.IsActive()) L1_index();
    if(L1_dr.IsActive()) L1_dr();
    if(L1_pt.IsActive()) L1_pt();
    if(L1_eta.IsActive()) L1_eta();
    if(L1_phi.IsActive()) L1_phi();
    if(L1_thrNumber.IsActive()) L1_thrNumber();
    if(L1_RoINumber.IsActive()) L1_RoINumber();
    if(L1_sectorAddress.IsActive()) L1_sectorAddress();
    if(L1_firstCandidate.IsActive()) L1_firstCandidate();
    if(L1_moreCandInRoI.IsActive()) L1_moreCandInRoI();
    if(L1_moreCandInSector.IsActive()) L1_moreCandInSector();
    if(L1_source.IsActive()) L1_source();
    if(L1_hemisphere.IsActive()) L1_hemisphere();
    if(L1_charge.IsActive()) L1_charge();
    if(L1_vetoed.IsActive()) L1_vetoed();
    if(L1_matched.IsActive()) L1_matched();
    if(truthAssoc_index.IsActive()) truthAssoc_index();
    if(MI10_max40_ptsum.IsActive()) MI10_max40_ptsum();
    if(MI10_max40_nTrks.IsActive()) MI10_max40_nTrks();
    if(MI10_max30_ptsum.IsActive()) MI10_max30_ptsum();
    if(MI10_max30_nTrks.IsActive()) MI10_max30_nTrks();
    if(MI15_max40_ptsum.IsActive()) MI15_max40_ptsum();
    if(MI15_max40_nTrks.IsActive()) MI15_max40_nTrks();
    if(MI15_max30_ptsum.IsActive()) MI15_max30_ptsum();
    if(MI15_max30_nTrks.IsActive()) MI15_max30_nTrks();
  }

} // namespace D3PDReader
#endif // D3PDREADER_MuonD3PDCollection_CXX
