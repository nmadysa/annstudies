// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_VertexD3PDCollection_CXX
#define D3PDREADER_VertexD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "VertexD3PDCollection.h"

ClassImp(D3PDReader::VertexD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  VertexD3PDCollection::VertexD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    n(prefix + "n",&master),
    x(prefix + "x",&master),
    y(prefix + "y",&master),
    z(prefix + "z",&master),
    type(prefix + "type",&master),
    chi2(prefix + "chi2",&master),
    ndof(prefix + "ndof",&master),
    px(prefix + "px",&master),
    py(prefix + "py",&master),
    pz(prefix + "pz",&master),
    E(prefix + "E",&master),
    m(prefix + "m",&master),
    nTracks(prefix + "nTracks",&master),
    sumPt(prefix + "sumPt",&master),
    trk_weight(prefix + "trk_weight",&master),
    trk_n(prefix + "trk_n",&master),
    trk_index(prefix + "trk_index",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  VertexD3PDCollection::VertexD3PDCollection(const std::string& prefix):
    TObject(),
    n(prefix + "n",0),
    x(prefix + "x",0),
    y(prefix + "y",0),
    z(prefix + "z",0),
    type(prefix + "type",0),
    chi2(prefix + "chi2",0),
    ndof(prefix + "ndof",0),
    px(prefix + "px",0),
    py(prefix + "py",0),
    pz(prefix + "pz",0),
    E(prefix + "E",0),
    m(prefix + "m",0),
    nTracks(prefix + "nTracks",0),
    sumPt(prefix + "sumPt",0),
    trk_weight(prefix + "trk_weight",0),
    trk_n(prefix + "trk_n",0),
    trk_index(prefix + "trk_index",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void VertexD3PDCollection::ReadFrom(TTree* tree)
  {
    n.ReadFrom(tree);
    x.ReadFrom(tree);
    y.ReadFrom(tree);
    z.ReadFrom(tree);
    type.ReadFrom(tree);
    chi2.ReadFrom(tree);
    ndof.ReadFrom(tree);
    px.ReadFrom(tree);
    py.ReadFrom(tree);
    pz.ReadFrom(tree);
    E.ReadFrom(tree);
    m.ReadFrom(tree);
    nTracks.ReadFrom(tree);
    sumPt.ReadFrom(tree);
    trk_weight.ReadFrom(tree);
    trk_n.ReadFrom(tree);
    trk_index.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void VertexD3PDCollection::WriteTo(TTree* tree)
  {
    n.WriteTo(tree);
    x.WriteTo(tree);
    y.WriteTo(tree);
    z.WriteTo(tree);
    type.WriteTo(tree);
    chi2.WriteTo(tree);
    ndof.WriteTo(tree);
    px.WriteTo(tree);
    py.WriteTo(tree);
    pz.WriteTo(tree);
    E.WriteTo(tree);
    m.WriteTo(tree);
    nTracks.WriteTo(tree);
    sumPt.WriteTo(tree);
    trk_weight.WriteTo(tree);
    trk_n.WriteTo(tree);
    trk_index.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void VertexD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(n.IsAvailable()) n.SetActive(active);
     if(x.IsAvailable()) x.SetActive(active);
     if(y.IsAvailable()) y.SetActive(active);
     if(z.IsAvailable()) z.SetActive(active);
     if(type.IsAvailable()) type.SetActive(active);
     if(chi2.IsAvailable()) chi2.SetActive(active);
     if(ndof.IsAvailable()) ndof.SetActive(active);
     if(px.IsAvailable()) px.SetActive(active);
     if(py.IsAvailable()) py.SetActive(active);
     if(pz.IsAvailable()) pz.SetActive(active);
     if(E.IsAvailable()) E.SetActive(active);
     if(m.IsAvailable()) m.SetActive(active);
     if(nTracks.IsAvailable()) nTracks.SetActive(active);
     if(sumPt.IsAvailable()) sumPt.SetActive(active);
     if(trk_weight.IsAvailable()) trk_weight.SetActive(active);
     if(trk_n.IsAvailable()) trk_n.SetActive(active);
     if(trk_index.IsAvailable()) trk_index.SetActive(active);
    }
    else
    {
      n.SetActive(active);
      x.SetActive(active);
      y.SetActive(active);
      z.SetActive(active);
      type.SetActive(active);
      chi2.SetActive(active);
      ndof.SetActive(active);
      px.SetActive(active);
      py.SetActive(active);
      pz.SetActive(active);
      E.SetActive(active);
      m.SetActive(active);
      nTracks.SetActive(active);
      sumPt.SetActive(active);
      trk_weight.SetActive(active);
      trk_n.SetActive(active);
      trk_index.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void VertexD3PDCollection::ReadAllActive()
  {
    if(n.IsActive()) n();
    if(x.IsActive()) x();
    if(y.IsActive()) y();
    if(z.IsActive()) z();
    if(type.IsActive()) type();
    if(chi2.IsActive()) chi2();
    if(ndof.IsActive()) ndof();
    if(px.IsActive()) px();
    if(py.IsActive()) py();
    if(pz.IsActive()) pz();
    if(E.IsActive()) E();
    if(m.IsActive()) m();
    if(nTracks.IsActive()) nTracks();
    if(sumPt.IsActive()) sumPt();
    if(trk_weight.IsActive()) trk_weight();
    if(trk_n.IsActive()) trk_n();
    if(trk_index.IsActive()) trk_index();
  }

} // namespace D3PDReader
#endif // D3PDREADER_VertexD3PDCollection_CXX
