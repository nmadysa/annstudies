// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_MCTruthD3PDCollection_CXX
#define D3PDREADER_MCTruthD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "MCTruthD3PDCollection.h"

ClassImp(D3PDReader::MCTruthD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  MCTruthD3PDCollection::MCTruthD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    n(prefix + "n",&master),
    pt(prefix + "pt",&master),
    m(prefix + "m",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    status(prefix + "status",&master),
    barcode(prefix + "barcode",&master),
    pdgId(prefix + "pdgId",&master),
    charge(prefix + "charge",&master),
    parents(prefix + "parents",&master),
    children(prefix + "children",&master),
    vx_x(prefix + "vx_x",&master),
    vx_y(prefix + "vx_y",&master),
    vx_z(prefix + "vx_z",&master),
    vx_barcode(prefix + "vx_barcode",&master),
    child_index(prefix + "child_index",&master),
    parent_index(prefix + "parent_index",&master),
    muAssoc_index(prefix + "muAssoc_index",&master),
    event_number(prefix + "event_number",&master),
    event_weight(prefix + "event_weight",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  MCTruthD3PDCollection::MCTruthD3PDCollection(const std::string& prefix):
    TObject(),
    n(prefix + "n",0),
    pt(prefix + "pt",0),
    m(prefix + "m",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    status(prefix + "status",0),
    barcode(prefix + "barcode",0),
    pdgId(prefix + "pdgId",0),
    charge(prefix + "charge",0),
    parents(prefix + "parents",0),
    children(prefix + "children",0),
    vx_x(prefix + "vx_x",0),
    vx_y(prefix + "vx_y",0),
    vx_z(prefix + "vx_z",0),
    vx_barcode(prefix + "vx_barcode",0),
    child_index(prefix + "child_index",0),
    parent_index(prefix + "parent_index",0),
    muAssoc_index(prefix + "muAssoc_index",0),
    event_number(prefix + "event_number",0),
    event_weight(prefix + "event_weight",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void MCTruthD3PDCollection::ReadFrom(TTree* tree)
  {
    n.ReadFrom(tree);
    pt.ReadFrom(tree);
    m.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    status.ReadFrom(tree);
    barcode.ReadFrom(tree);
    pdgId.ReadFrom(tree);
    charge.ReadFrom(tree);
    parents.ReadFrom(tree);
    children.ReadFrom(tree);
    vx_x.ReadFrom(tree);
    vx_y.ReadFrom(tree);
    vx_z.ReadFrom(tree);
    vx_barcode.ReadFrom(tree);
    child_index.ReadFrom(tree);
    parent_index.ReadFrom(tree);
    muAssoc_index.ReadFrom(tree);
    event_number.ReadFrom(tree);
    event_weight.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void MCTruthD3PDCollection::WriteTo(TTree* tree)
  {
    n.WriteTo(tree);
    pt.WriteTo(tree);
    m.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    status.WriteTo(tree);
    barcode.WriteTo(tree);
    pdgId.WriteTo(tree);
    charge.WriteTo(tree);
    parents.WriteTo(tree);
    children.WriteTo(tree);
    vx_x.WriteTo(tree);
    vx_y.WriteTo(tree);
    vx_z.WriteTo(tree);
    vx_barcode.WriteTo(tree);
    child_index.WriteTo(tree);
    parent_index.WriteTo(tree);
    muAssoc_index.WriteTo(tree);
    event_number.WriteTo(tree);
    event_weight.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void MCTruthD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(n.IsAvailable()) n.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(m.IsAvailable()) m.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(status.IsAvailable()) status.SetActive(active);
     if(barcode.IsAvailable()) barcode.SetActive(active);
     if(pdgId.IsAvailable()) pdgId.SetActive(active);
     if(charge.IsAvailable()) charge.SetActive(active);
     if(parents.IsAvailable()) parents.SetActive(active);
     if(children.IsAvailable()) children.SetActive(active);
     if(vx_x.IsAvailable()) vx_x.SetActive(active);
     if(vx_y.IsAvailable()) vx_y.SetActive(active);
     if(vx_z.IsAvailable()) vx_z.SetActive(active);
     if(vx_barcode.IsAvailable()) vx_barcode.SetActive(active);
     if(child_index.IsAvailable()) child_index.SetActive(active);
     if(parent_index.IsAvailable()) parent_index.SetActive(active);
     if(muAssoc_index.IsAvailable()) muAssoc_index.SetActive(active);
     if(event_number.IsAvailable()) event_number.SetActive(active);
     if(event_weight.IsAvailable()) event_weight.SetActive(active);
    }
    else
    {
      n.SetActive(active);
      pt.SetActive(active);
      m.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      status.SetActive(active);
      barcode.SetActive(active);
      pdgId.SetActive(active);
      charge.SetActive(active);
      parents.SetActive(active);
      children.SetActive(active);
      vx_x.SetActive(active);
      vx_y.SetActive(active);
      vx_z.SetActive(active);
      vx_barcode.SetActive(active);
      child_index.SetActive(active);
      parent_index.SetActive(active);
      muAssoc_index.SetActive(active);
      event_number.SetActive(active);
      event_weight.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void MCTruthD3PDCollection::ReadAllActive()
  {
    if(n.IsActive()) n();
    if(pt.IsActive()) pt();
    if(m.IsActive()) m();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(status.IsActive()) status();
    if(barcode.IsActive()) barcode();
    if(pdgId.IsActive()) pdgId();
    if(charge.IsActive()) charge();
    if(parents.IsActive()) parents();
    if(children.IsActive()) children();
    if(vx_x.IsActive()) vx_x();
    if(vx_y.IsActive()) vx_y();
    if(vx_z.IsActive()) vx_z();
    if(vx_barcode.IsActive()) vx_barcode();
    if(child_index.IsActive()) child_index();
    if(parent_index.IsActive()) parent_index();
    if(muAssoc_index.IsActive()) muAssoc_index();
    if(event_number.IsActive()) event_number();
    if(event_weight.IsActive()) event_weight();
  }

} // namespace D3PDReader
#endif // D3PDREADER_MCTruthD3PDCollection_CXX
