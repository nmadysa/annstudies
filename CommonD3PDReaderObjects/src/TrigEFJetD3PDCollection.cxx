// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TrigEFJetD3PDCollection_CXX
#define D3PDREADER_TrigEFJetD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TrigEFJetD3PDCollection.h"

ClassImp(D3PDReader::TrigEFJetD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigEFJetD3PDCollection::TrigEFJetD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    EF_2fj100_a4tc_EFFS_deta50_FB(prefix + "EF_2fj100_a4tc_EFFS_deta50_FB",&master),
    EF_2fj30_a4tc_EFFS_deta50_FB(prefix + "EF_2fj30_a4tc_EFFS_deta50_FB",&master),
    EF_2fj30_a4tc_EFFS_deta50_FC(prefix + "EF_2fj30_a4tc_EFFS_deta50_FC",&master),
    EF_2fj55_a4tc_EFFS_deta50_FB(prefix + "EF_2fj55_a4tc_EFFS_deta50_FB",&master),
    EF_2fj55_a4tc_EFFS_deta50_FC(prefix + "EF_2fj55_a4tc_EFFS_deta50_FC",&master),
    EF_2fj75_a4tc_EFFS_deta50_FB(prefix + "EF_2fj75_a4tc_EFFS_deta50_FB",&master),
    EF_2fj75_a4tc_EFFS_deta50_FC(prefix + "EF_2fj75_a4tc_EFFS_deta50_FC",&master),
    EF_2j100_a4tc_EFFS_deta35_FC(prefix + "EF_2j100_a4tc_EFFS_deta35_FC",&master),
    EF_2j135_a4tc_EFFS_deta35_FC(prefix + "EF_2j135_a4tc_EFFS_deta35_FC",&master),
    EF_2j180_a4tc_EFFS_deta35_FC(prefix + "EF_2j180_a4tc_EFFS_deta35_FC",&master),
    EF_2j240_a4tc_EFFS_deta35_FC(prefix + "EF_2j240_a4tc_EFFS_deta35_FC",&master),
    EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu(prefix + "EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu",&master),
    EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu(prefix + "EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu",&master),
    EF_3j100_a4tc_EFFS(prefix + "EF_3j100_a4tc_EFFS",&master),
    EF_3j100_a4tc_EFFS_L1J75(prefix + "EF_3j100_a4tc_EFFS_L1J75",&master),
    EF_3j30_a4tc_EFFS(prefix + "EF_3j30_a4tc_EFFS",&master),
    EF_3j40_a4tc_EFFS(prefix + "EF_3j40_a4tc_EFFS",&master),
    EF_3j45_a4tc_EFFS(prefix + "EF_3j45_a4tc_EFFS",&master),
    EF_3j75_a4tc_EFFS(prefix + "EF_3j75_a4tc_EFFS",&master),
    EF_4j30_a4tc_EFFS(prefix + "EF_4j30_a4tc_EFFS",&master),
    EF_4j40_a4tc_EFFS(prefix + "EF_4j40_a4tc_EFFS",&master),
    EF_4j40_a4tc_EFFS_ht350(prefix + "EF_4j40_a4tc_EFFS_ht350",&master),
    EF_4j40_a4tc_EFFS_ht400(prefix + "EF_4j40_a4tc_EFFS_ht400",&master),
    EF_4j45_a4tc_EFFS(prefix + "EF_4j45_a4tc_EFFS",&master),
    EF_4j55_a4tc_EFFS(prefix + "EF_4j55_a4tc_EFFS",&master),
    EF_4j60_a4tc_EFFS(prefix + "EF_4j60_a4tc_EFFS",&master),
    EF_5j30_a4tc_EFFS(prefix + "EF_5j30_a4tc_EFFS",&master),
    EF_5j40_a4tc_EFFS(prefix + "EF_5j40_a4tc_EFFS",&master),
    EF_5j45_a4tc_EFFS(prefix + "EF_5j45_a4tc_EFFS",&master),
    EF_6j30_a4tc_EFFS(prefix + "EF_6j30_a4tc_EFFS",&master),
    EF_6j30_a4tc_EFFS_L15J10(prefix + "EF_6j30_a4tc_EFFS_L15J10",&master),
    EF_6j40_a4tc_EFFS(prefix + "EF_6j40_a4tc_EFFS",&master),
    EF_6j45_a4tc_EFFS(prefix + "EF_6j45_a4tc_EFFS",&master),
    EF_7j30_a4tc_EFFS_L15J10(prefix + "EF_7j30_a4tc_EFFS_L15J10",&master),
    EF_7j30_a4tc_EFFS_L16J10(prefix + "EF_7j30_a4tc_EFFS_L16J10",&master),
    EF_fj100_a4tc_EFFS(prefix + "EF_fj100_a4tc_EFFS",&master),
    EF_fj10_a4tc_EFFS(prefix + "EF_fj10_a4tc_EFFS",&master),
    EF_fj10_a4tc_EFFS_1vx(prefix + "EF_fj10_a4tc_EFFS_1vx",&master),
    EF_fj135_a4tc_EFFS(prefix + "EF_fj135_a4tc_EFFS",&master),
    EF_fj15_a4tc_EFFS(prefix + "EF_fj15_a4tc_EFFS",&master),
    EF_fj20_a4tc_EFFS(prefix + "EF_fj20_a4tc_EFFS",&master),
    EF_fj30_a4tc_EFFS(prefix + "EF_fj30_a4tc_EFFS",&master),
    EF_fj30_a4tc_EFFS_l2cleanph(prefix + "EF_fj30_a4tc_EFFS_l2cleanph",&master),
    EF_fj55_a4tc_EFFS(prefix + "EF_fj55_a4tc_EFFS",&master),
    EF_fj75_a4tc_EFFS(prefix + "EF_fj75_a4tc_EFFS",&master),
    EF_j100_a4tc_EFFS(prefix + "EF_j100_a4tc_EFFS",&master),
    EF_j100_a4tc_EFFS_ht350(prefix + "EF_j100_a4tc_EFFS_ht350",&master),
    EF_j100_a4tc_EFFS_ht400(prefix + "EF_j100_a4tc_EFFS_ht400",&master),
    EF_j100_a4tc_EFFS_ht500(prefix + "EF_j100_a4tc_EFFS_ht500",&master),
    EF_j100_j30_a4tc_EFFS_L2dphi04(prefix + "EF_j100_j30_a4tc_EFFS_L2dphi04",&master),
    EF_j10_a4tc_EFFS(prefix + "EF_j10_a4tc_EFFS",&master),
    EF_j10_a4tc_EFFS_1vx(prefix + "EF_j10_a4tc_EFFS_1vx",&master),
    EF_j135_a4tc_EFFS(prefix + "EF_j135_a4tc_EFFS",&master),
    EF_j135_a4tc_EFFS_ht500(prefix + "EF_j135_a4tc_EFFS_ht500",&master),
    EF_j135_j30_a4tc_EFFS_L2dphi04(prefix + "EF_j135_j30_a4tc_EFFS_L2dphi04",&master),
    EF_j135_j30_a4tc_EFFS_dphi04(prefix + "EF_j135_j30_a4tc_EFFS_dphi04",&master),
    EF_j15_a4tc_EFFS(prefix + "EF_j15_a4tc_EFFS",&master),
    EF_j180_a4tc_EFFS(prefix + "EF_j180_a4tc_EFFS",&master),
    EF_j180_j30_a4tc_EFFS_dphi04(prefix + "EF_j180_j30_a4tc_EFFS_dphi04",&master),
    EF_j20_a4tc_EFFS(prefix + "EF_j20_a4tc_EFFS",&master),
    EF_j240_a10tc_EFFS(prefix + "EF_j240_a10tc_EFFS",&master),
    EF_j240_a4tc_EFFS(prefix + "EF_j240_a4tc_EFFS",&master),
    EF_j240_a4tc_EFFS_l2cleanph(prefix + "EF_j240_a4tc_EFFS_l2cleanph",&master),
    EF_j30_a4tc_EFFS(prefix + "EF_j30_a4tc_EFFS",&master),
    EF_j30_a4tc_EFFS_l2cleanph(prefix + "EF_j30_a4tc_EFFS_l2cleanph",&master),
    EF_j30_cosmic(prefix + "EF_j30_cosmic",&master),
    EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty(prefix + "EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty",&master),
    EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty(prefix + "EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty",&master),
    EF_j30_firstempty(prefix + "EF_j30_firstempty",&master),
    EF_j30_fj30_a4tc_EFFS(prefix + "EF_j30_fj30_a4tc_EFFS",&master),
    EF_j320_a10tc_EFFS(prefix + "EF_j320_a10tc_EFFS",&master),
    EF_j320_a4tc_EFFS(prefix + "EF_j320_a4tc_EFFS",&master),
    EF_j35_a4tc_EFFS(prefix + "EF_j35_a4tc_EFFS",&master),
    EF_j35_a4tc_EFFS_L1TAU_HV(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV",&master),
    EF_j35_a4tc_EFFS_L1TAU_HV_cosmic(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV_cosmic",&master),
    EF_j35_a4tc_EFFS_L1TAU_HV_firstempty(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV_firstempty",&master),
    EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso",&master),
    EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso",&master),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk",&master),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF",&master),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic",&master),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty",&master),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso",&master),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso",&master),
    EF_j40_a4tc_EFFS(prefix + "EF_j40_a4tc_EFFS",&master),
    EF_j40_fj40_a4tc_EFFS(prefix + "EF_j40_fj40_a4tc_EFFS",&master),
    EF_j425_a10tc_EFFS(prefix + "EF_j425_a10tc_EFFS",&master),
    EF_j425_a4tc_EFFS(prefix + "EF_j425_a4tc_EFFS",&master),
    EF_j45_a4tc_EFFS(prefix + "EF_j45_a4tc_EFFS",&master),
    EF_j50_a4tc_EFFS(prefix + "EF_j50_a4tc_EFFS",&master),
    EF_j50_cosmic(prefix + "EF_j50_cosmic",&master),
    EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty(prefix + "EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty",&master),
    EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty(prefix + "EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty",&master),
    EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty(prefix + "EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty",&master),
    EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty(prefix + "EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty",&master),
    EF_j50_firstempty(prefix + "EF_j50_firstempty",&master),
    EF_j55_a4tc_EFFS(prefix + "EF_j55_a4tc_EFFS",&master),
    EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10(prefix + "EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10",&master),
    EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons(prefix + "EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons",&master),
    EF_j55_fj55_a4tc_EFFS(prefix + "EF_j55_fj55_a4tc_EFFS",&master),
    EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10(prefix + "EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10",&master),
    EF_j70_j25_dphi03_NoEF(prefix + "EF_j70_j25_dphi03_NoEF",&master),
    EF_j75_2j30_a4tc_EFFS_ht350(prefix + "EF_j75_2j30_a4tc_EFFS_ht350",&master),
    EF_j75_a4tc_EFFS(prefix + "EF_j75_a4tc_EFFS",&master),
    EF_j75_a4tc_EFFS_xe40_loose_noMu(prefix + "EF_j75_a4tc_EFFS_xe40_loose_noMu",&master),
    EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03(prefix + "EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03",&master),
    EF_j75_a4tc_EFFS_xe45_loose_noMu(prefix + "EF_j75_a4tc_EFFS_xe45_loose_noMu",&master),
    EF_j75_a4tc_EFFS_xe55_loose_noMu(prefix + "EF_j75_a4tc_EFFS_xe55_loose_noMu",&master),
    EF_j75_a4tc_EFFS_xe55_noMu(prefix + "EF_j75_a4tc_EFFS_xe55_noMu",&master),
    EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons(prefix + "EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons",&master),
    EF_j75_a4tc_EFFS_xs35_noMu(prefix + "EF_j75_a4tc_EFFS_xs35_noMu",&master),
    EF_j75_fj75_a4tc_EFFS(prefix + "EF_j75_fj75_a4tc_EFFS",&master),
    EF_j75_j30_a4tc_EFFS(prefix + "EF_j75_j30_a4tc_EFFS",&master),
    EF_j75_j30_a4tc_EFFS_L2anymct100(prefix + "EF_j75_j30_a4tc_EFFS_L2anymct100",&master),
    EF_j75_j30_a4tc_EFFS_L2anymct150(prefix + "EF_j75_j30_a4tc_EFFS_L2anymct150",&master),
    EF_j75_j30_a4tc_EFFS_L2anymct175(prefix + "EF_j75_j30_a4tc_EFFS_L2anymct175",&master),
    EF_j75_j30_a4tc_EFFS_L2dphi04(prefix + "EF_j75_j30_a4tc_EFFS_L2dphi04",&master),
    EF_j75_j30_a4tc_EFFS_anymct150(prefix + "EF_j75_j30_a4tc_EFFS_anymct150",&master),
    EF_j75_j30_a4tc_EFFS_anymct175(prefix + "EF_j75_j30_a4tc_EFFS_anymct175",&master),
    EF_j75_j30_a4tc_EFFS_leadingmct150(prefix + "EF_j75_j30_a4tc_EFFS_leadingmct150",&master),
    EF_j80_a4tc_EFFS_xe60_noMu(prefix + "EF_j80_a4tc_EFFS_xe60_noMu",&master),
    EF_je195_NoEF(prefix + "EF_je195_NoEF",&master),
    EF_je255_NoEF(prefix + "EF_je255_NoEF",&master),
    EF_je300_NoEF(prefix + "EF_je300_NoEF",&master),
    EF_je350_NoEF(prefix + "EF_je350_NoEF",&master),
    EF_je420_NoEF(prefix + "EF_je420_NoEF",&master),
    EF_je500_NoEF(prefix + "EF_je500_NoEF",&master),
    n(prefix + "n",&master),
    emscale_E(prefix + "emscale_E",&master),
    emscale_pt(prefix + "emscale_pt",&master),
    emscale_m(prefix + "emscale_m",&master),
    emscale_eta(prefix + "emscale_eta",&master),
    emscale_phi(prefix + "emscale_phi",&master),
    a4(prefix + "a4",&master),
    a4tc(prefix + "a4tc",&master),
    a10tc(prefix + "a10tc",&master),
    a6(prefix + "a6",&master),
    a6tc(prefix + "a6tc",&master),
    RoIword(prefix + "RoIword",&master),
    EF_3j10_a4tc_EFFS(prefix + "EF_3j10_a4tc_EFFS",&master),
    EF_4j10_a4tc_EFFS(prefix + "EF_4j10_a4tc_EFFS",&master),
    EF_5j10_a4tc_EFFS(prefix + "EF_5j10_a4tc_EFFS",&master),
    EF_j15_a2hi_EFFS(prefix + "EF_j15_a2hi_EFFS",&master),
    EF_j15_a2tc_EFFS(prefix + "EF_j15_a2tc_EFFS",&master),
    EF_j20_a2hi_EFFS(prefix + "EF_j20_a2hi_EFFS",&master),
    EF_j20_a2tc_EFFS(prefix + "EF_j20_a2tc_EFFS",&master),
    EF_j20_a4hi_EFFS(prefix + "EF_j20_a4hi_EFFS",&master),
    EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10(prefix + "EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10",&master),
    EF_j75_j45_a4tc_EFFS_xe55_noMu(prefix + "EF_j75_j45_a4tc_EFFS_xe55_noMu",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TrigEFJetD3PDCollection::TrigEFJetD3PDCollection(const std::string& prefix):
    TObject(),
    EF_2fj100_a4tc_EFFS_deta50_FB(prefix + "EF_2fj100_a4tc_EFFS_deta50_FB",0),
    EF_2fj30_a4tc_EFFS_deta50_FB(prefix + "EF_2fj30_a4tc_EFFS_deta50_FB",0),
    EF_2fj30_a4tc_EFFS_deta50_FC(prefix + "EF_2fj30_a4tc_EFFS_deta50_FC",0),
    EF_2fj55_a4tc_EFFS_deta50_FB(prefix + "EF_2fj55_a4tc_EFFS_deta50_FB",0),
    EF_2fj55_a4tc_EFFS_deta50_FC(prefix + "EF_2fj55_a4tc_EFFS_deta50_FC",0),
    EF_2fj75_a4tc_EFFS_deta50_FB(prefix + "EF_2fj75_a4tc_EFFS_deta50_FB",0),
    EF_2fj75_a4tc_EFFS_deta50_FC(prefix + "EF_2fj75_a4tc_EFFS_deta50_FC",0),
    EF_2j100_a4tc_EFFS_deta35_FC(prefix + "EF_2j100_a4tc_EFFS_deta35_FC",0),
    EF_2j135_a4tc_EFFS_deta35_FC(prefix + "EF_2j135_a4tc_EFFS_deta35_FC",0),
    EF_2j180_a4tc_EFFS_deta35_FC(prefix + "EF_2j180_a4tc_EFFS_deta35_FC",0),
    EF_2j240_a4tc_EFFS_deta35_FC(prefix + "EF_2j240_a4tc_EFFS_deta35_FC",0),
    EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu(prefix + "EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu",0),
    EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu(prefix + "EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu",0),
    EF_3j100_a4tc_EFFS(prefix + "EF_3j100_a4tc_EFFS",0),
    EF_3j100_a4tc_EFFS_L1J75(prefix + "EF_3j100_a4tc_EFFS_L1J75",0),
    EF_3j30_a4tc_EFFS(prefix + "EF_3j30_a4tc_EFFS",0),
    EF_3j40_a4tc_EFFS(prefix + "EF_3j40_a4tc_EFFS",0),
    EF_3j45_a4tc_EFFS(prefix + "EF_3j45_a4tc_EFFS",0),
    EF_3j75_a4tc_EFFS(prefix + "EF_3j75_a4tc_EFFS",0),
    EF_4j30_a4tc_EFFS(prefix + "EF_4j30_a4tc_EFFS",0),
    EF_4j40_a4tc_EFFS(prefix + "EF_4j40_a4tc_EFFS",0),
    EF_4j40_a4tc_EFFS_ht350(prefix + "EF_4j40_a4tc_EFFS_ht350",0),
    EF_4j40_a4tc_EFFS_ht400(prefix + "EF_4j40_a4tc_EFFS_ht400",0),
    EF_4j45_a4tc_EFFS(prefix + "EF_4j45_a4tc_EFFS",0),
    EF_4j55_a4tc_EFFS(prefix + "EF_4j55_a4tc_EFFS",0),
    EF_4j60_a4tc_EFFS(prefix + "EF_4j60_a4tc_EFFS",0),
    EF_5j30_a4tc_EFFS(prefix + "EF_5j30_a4tc_EFFS",0),
    EF_5j40_a4tc_EFFS(prefix + "EF_5j40_a4tc_EFFS",0),
    EF_5j45_a4tc_EFFS(prefix + "EF_5j45_a4tc_EFFS",0),
    EF_6j30_a4tc_EFFS(prefix + "EF_6j30_a4tc_EFFS",0),
    EF_6j30_a4tc_EFFS_L15J10(prefix + "EF_6j30_a4tc_EFFS_L15J10",0),
    EF_6j40_a4tc_EFFS(prefix + "EF_6j40_a4tc_EFFS",0),
    EF_6j45_a4tc_EFFS(prefix + "EF_6j45_a4tc_EFFS",0),
    EF_7j30_a4tc_EFFS_L15J10(prefix + "EF_7j30_a4tc_EFFS_L15J10",0),
    EF_7j30_a4tc_EFFS_L16J10(prefix + "EF_7j30_a4tc_EFFS_L16J10",0),
    EF_fj100_a4tc_EFFS(prefix + "EF_fj100_a4tc_EFFS",0),
    EF_fj10_a4tc_EFFS(prefix + "EF_fj10_a4tc_EFFS",0),
    EF_fj10_a4tc_EFFS_1vx(prefix + "EF_fj10_a4tc_EFFS_1vx",0),
    EF_fj135_a4tc_EFFS(prefix + "EF_fj135_a4tc_EFFS",0),
    EF_fj15_a4tc_EFFS(prefix + "EF_fj15_a4tc_EFFS",0),
    EF_fj20_a4tc_EFFS(prefix + "EF_fj20_a4tc_EFFS",0),
    EF_fj30_a4tc_EFFS(prefix + "EF_fj30_a4tc_EFFS",0),
    EF_fj30_a4tc_EFFS_l2cleanph(prefix + "EF_fj30_a4tc_EFFS_l2cleanph",0),
    EF_fj55_a4tc_EFFS(prefix + "EF_fj55_a4tc_EFFS",0),
    EF_fj75_a4tc_EFFS(prefix + "EF_fj75_a4tc_EFFS",0),
    EF_j100_a4tc_EFFS(prefix + "EF_j100_a4tc_EFFS",0),
    EF_j100_a4tc_EFFS_ht350(prefix + "EF_j100_a4tc_EFFS_ht350",0),
    EF_j100_a4tc_EFFS_ht400(prefix + "EF_j100_a4tc_EFFS_ht400",0),
    EF_j100_a4tc_EFFS_ht500(prefix + "EF_j100_a4tc_EFFS_ht500",0),
    EF_j100_j30_a4tc_EFFS_L2dphi04(prefix + "EF_j100_j30_a4tc_EFFS_L2dphi04",0),
    EF_j10_a4tc_EFFS(prefix + "EF_j10_a4tc_EFFS",0),
    EF_j10_a4tc_EFFS_1vx(prefix + "EF_j10_a4tc_EFFS_1vx",0),
    EF_j135_a4tc_EFFS(prefix + "EF_j135_a4tc_EFFS",0),
    EF_j135_a4tc_EFFS_ht500(prefix + "EF_j135_a4tc_EFFS_ht500",0),
    EF_j135_j30_a4tc_EFFS_L2dphi04(prefix + "EF_j135_j30_a4tc_EFFS_L2dphi04",0),
    EF_j135_j30_a4tc_EFFS_dphi04(prefix + "EF_j135_j30_a4tc_EFFS_dphi04",0),
    EF_j15_a4tc_EFFS(prefix + "EF_j15_a4tc_EFFS",0),
    EF_j180_a4tc_EFFS(prefix + "EF_j180_a4tc_EFFS",0),
    EF_j180_j30_a4tc_EFFS_dphi04(prefix + "EF_j180_j30_a4tc_EFFS_dphi04",0),
    EF_j20_a4tc_EFFS(prefix + "EF_j20_a4tc_EFFS",0),
    EF_j240_a10tc_EFFS(prefix + "EF_j240_a10tc_EFFS",0),
    EF_j240_a4tc_EFFS(prefix + "EF_j240_a4tc_EFFS",0),
    EF_j240_a4tc_EFFS_l2cleanph(prefix + "EF_j240_a4tc_EFFS_l2cleanph",0),
    EF_j30_a4tc_EFFS(prefix + "EF_j30_a4tc_EFFS",0),
    EF_j30_a4tc_EFFS_l2cleanph(prefix + "EF_j30_a4tc_EFFS_l2cleanph",0),
    EF_j30_cosmic(prefix + "EF_j30_cosmic",0),
    EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty(prefix + "EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty",0),
    EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty(prefix + "EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty",0),
    EF_j30_firstempty(prefix + "EF_j30_firstempty",0),
    EF_j30_fj30_a4tc_EFFS(prefix + "EF_j30_fj30_a4tc_EFFS",0),
    EF_j320_a10tc_EFFS(prefix + "EF_j320_a10tc_EFFS",0),
    EF_j320_a4tc_EFFS(prefix + "EF_j320_a4tc_EFFS",0),
    EF_j35_a4tc_EFFS(prefix + "EF_j35_a4tc_EFFS",0),
    EF_j35_a4tc_EFFS_L1TAU_HV(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV",0),
    EF_j35_a4tc_EFFS_L1TAU_HV_cosmic(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV_cosmic",0),
    EF_j35_a4tc_EFFS_L1TAU_HV_firstempty(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV_firstempty",0),
    EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso",0),
    EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso(prefix + "EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso",0),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk",0),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF",0),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic",0),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty",0),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso",0),
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso(prefix + "EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso",0),
    EF_j40_a4tc_EFFS(prefix + "EF_j40_a4tc_EFFS",0),
    EF_j40_fj40_a4tc_EFFS(prefix + "EF_j40_fj40_a4tc_EFFS",0),
    EF_j425_a10tc_EFFS(prefix + "EF_j425_a10tc_EFFS",0),
    EF_j425_a4tc_EFFS(prefix + "EF_j425_a4tc_EFFS",0),
    EF_j45_a4tc_EFFS(prefix + "EF_j45_a4tc_EFFS",0),
    EF_j50_a4tc_EFFS(prefix + "EF_j50_a4tc_EFFS",0),
    EF_j50_cosmic(prefix + "EF_j50_cosmic",0),
    EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty(prefix + "EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty",0),
    EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty(prefix + "EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty",0),
    EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty(prefix + "EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty",0),
    EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty(prefix + "EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty",0),
    EF_j50_firstempty(prefix + "EF_j50_firstempty",0),
    EF_j55_a4tc_EFFS(prefix + "EF_j55_a4tc_EFFS",0),
    EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10(prefix + "EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10",0),
    EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons(prefix + "EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons",0),
    EF_j55_fj55_a4tc_EFFS(prefix + "EF_j55_fj55_a4tc_EFFS",0),
    EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10(prefix + "EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10",0),
    EF_j70_j25_dphi03_NoEF(prefix + "EF_j70_j25_dphi03_NoEF",0),
    EF_j75_2j30_a4tc_EFFS_ht350(prefix + "EF_j75_2j30_a4tc_EFFS_ht350",0),
    EF_j75_a4tc_EFFS(prefix + "EF_j75_a4tc_EFFS",0),
    EF_j75_a4tc_EFFS_xe40_loose_noMu(prefix + "EF_j75_a4tc_EFFS_xe40_loose_noMu",0),
    EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03(prefix + "EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03",0),
    EF_j75_a4tc_EFFS_xe45_loose_noMu(prefix + "EF_j75_a4tc_EFFS_xe45_loose_noMu",0),
    EF_j75_a4tc_EFFS_xe55_loose_noMu(prefix + "EF_j75_a4tc_EFFS_xe55_loose_noMu",0),
    EF_j75_a4tc_EFFS_xe55_noMu(prefix + "EF_j75_a4tc_EFFS_xe55_noMu",0),
    EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons(prefix + "EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons",0),
    EF_j75_a4tc_EFFS_xs35_noMu(prefix + "EF_j75_a4tc_EFFS_xs35_noMu",0),
    EF_j75_fj75_a4tc_EFFS(prefix + "EF_j75_fj75_a4tc_EFFS",0),
    EF_j75_j30_a4tc_EFFS(prefix + "EF_j75_j30_a4tc_EFFS",0),
    EF_j75_j30_a4tc_EFFS_L2anymct100(prefix + "EF_j75_j30_a4tc_EFFS_L2anymct100",0),
    EF_j75_j30_a4tc_EFFS_L2anymct150(prefix + "EF_j75_j30_a4tc_EFFS_L2anymct150",0),
    EF_j75_j30_a4tc_EFFS_L2anymct175(prefix + "EF_j75_j30_a4tc_EFFS_L2anymct175",0),
    EF_j75_j30_a4tc_EFFS_L2dphi04(prefix + "EF_j75_j30_a4tc_EFFS_L2dphi04",0),
    EF_j75_j30_a4tc_EFFS_anymct150(prefix + "EF_j75_j30_a4tc_EFFS_anymct150",0),
    EF_j75_j30_a4tc_EFFS_anymct175(prefix + "EF_j75_j30_a4tc_EFFS_anymct175",0),
    EF_j75_j30_a4tc_EFFS_leadingmct150(prefix + "EF_j75_j30_a4tc_EFFS_leadingmct150",0),
    EF_j80_a4tc_EFFS_xe60_noMu(prefix + "EF_j80_a4tc_EFFS_xe60_noMu",0),
    EF_je195_NoEF(prefix + "EF_je195_NoEF",0),
    EF_je255_NoEF(prefix + "EF_je255_NoEF",0),
    EF_je300_NoEF(prefix + "EF_je300_NoEF",0),
    EF_je350_NoEF(prefix + "EF_je350_NoEF",0),
    EF_je420_NoEF(prefix + "EF_je420_NoEF",0),
    EF_je500_NoEF(prefix + "EF_je500_NoEF",0),
    n(prefix + "n",0),
    emscale_E(prefix + "emscale_E",0),
    emscale_pt(prefix + "emscale_pt",0),
    emscale_m(prefix + "emscale_m",0),
    emscale_eta(prefix + "emscale_eta",0),
    emscale_phi(prefix + "emscale_phi",0),
    a4(prefix + "a4",0),
    a4tc(prefix + "a4tc",0),
    a10tc(prefix + "a10tc",0),
    a6(prefix + "a6",0),
    a6tc(prefix + "a6tc",0),
    RoIword(prefix + "RoIword",0),
    EF_3j10_a4tc_EFFS(prefix + "EF_3j10_a4tc_EFFS",0),
    EF_4j10_a4tc_EFFS(prefix + "EF_4j10_a4tc_EFFS",0),
    EF_5j10_a4tc_EFFS(prefix + "EF_5j10_a4tc_EFFS",0),
    EF_j15_a2hi_EFFS(prefix + "EF_j15_a2hi_EFFS",0),
    EF_j15_a2tc_EFFS(prefix + "EF_j15_a2tc_EFFS",0),
    EF_j20_a2hi_EFFS(prefix + "EF_j20_a2hi_EFFS",0),
    EF_j20_a2tc_EFFS(prefix + "EF_j20_a2tc_EFFS",0),
    EF_j20_a4hi_EFFS(prefix + "EF_j20_a4hi_EFFS",0),
    EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10(prefix + "EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10",0),
    EF_j75_j45_a4tc_EFFS_xe55_noMu(prefix + "EF_j75_j45_a4tc_EFFS_xe55_noMu",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TrigEFJetD3PDCollection::ReadFrom(TTree* tree)
  {
    EF_2fj100_a4tc_EFFS_deta50_FB.ReadFrom(tree);
    EF_2fj30_a4tc_EFFS_deta50_FB.ReadFrom(tree);
    EF_2fj30_a4tc_EFFS_deta50_FC.ReadFrom(tree);
    EF_2fj55_a4tc_EFFS_deta50_FB.ReadFrom(tree);
    EF_2fj55_a4tc_EFFS_deta50_FC.ReadFrom(tree);
    EF_2fj75_a4tc_EFFS_deta50_FB.ReadFrom(tree);
    EF_2fj75_a4tc_EFFS_deta50_FC.ReadFrom(tree);
    EF_2j100_a4tc_EFFS_deta35_FC.ReadFrom(tree);
    EF_2j135_a4tc_EFFS_deta35_FC.ReadFrom(tree);
    EF_2j180_a4tc_EFFS_deta35_FC.ReadFrom(tree);
    EF_2j240_a4tc_EFFS_deta35_FC.ReadFrom(tree);
    EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu.ReadFrom(tree);
    EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu.ReadFrom(tree);
    EF_3j100_a4tc_EFFS.ReadFrom(tree);
    EF_3j100_a4tc_EFFS_L1J75.ReadFrom(tree);
    EF_3j30_a4tc_EFFS.ReadFrom(tree);
    EF_3j40_a4tc_EFFS.ReadFrom(tree);
    EF_3j45_a4tc_EFFS.ReadFrom(tree);
    EF_3j75_a4tc_EFFS.ReadFrom(tree);
    EF_4j30_a4tc_EFFS.ReadFrom(tree);
    EF_4j40_a4tc_EFFS.ReadFrom(tree);
    EF_4j40_a4tc_EFFS_ht350.ReadFrom(tree);
    EF_4j40_a4tc_EFFS_ht400.ReadFrom(tree);
    EF_4j45_a4tc_EFFS.ReadFrom(tree);
    EF_4j55_a4tc_EFFS.ReadFrom(tree);
    EF_4j60_a4tc_EFFS.ReadFrom(tree);
    EF_5j30_a4tc_EFFS.ReadFrom(tree);
    EF_5j40_a4tc_EFFS.ReadFrom(tree);
    EF_5j45_a4tc_EFFS.ReadFrom(tree);
    EF_6j30_a4tc_EFFS.ReadFrom(tree);
    EF_6j30_a4tc_EFFS_L15J10.ReadFrom(tree);
    EF_6j40_a4tc_EFFS.ReadFrom(tree);
    EF_6j45_a4tc_EFFS.ReadFrom(tree);
    EF_7j30_a4tc_EFFS_L15J10.ReadFrom(tree);
    EF_7j30_a4tc_EFFS_L16J10.ReadFrom(tree);
    EF_fj100_a4tc_EFFS.ReadFrom(tree);
    EF_fj10_a4tc_EFFS.ReadFrom(tree);
    EF_fj10_a4tc_EFFS_1vx.ReadFrom(tree);
    EF_fj135_a4tc_EFFS.ReadFrom(tree);
    EF_fj15_a4tc_EFFS.ReadFrom(tree);
    EF_fj20_a4tc_EFFS.ReadFrom(tree);
    EF_fj30_a4tc_EFFS.ReadFrom(tree);
    EF_fj30_a4tc_EFFS_l2cleanph.ReadFrom(tree);
    EF_fj55_a4tc_EFFS.ReadFrom(tree);
    EF_fj75_a4tc_EFFS.ReadFrom(tree);
    EF_j100_a4tc_EFFS.ReadFrom(tree);
    EF_j100_a4tc_EFFS_ht350.ReadFrom(tree);
    EF_j100_a4tc_EFFS_ht400.ReadFrom(tree);
    EF_j100_a4tc_EFFS_ht500.ReadFrom(tree);
    EF_j100_j30_a4tc_EFFS_L2dphi04.ReadFrom(tree);
    EF_j10_a4tc_EFFS.ReadFrom(tree);
    EF_j10_a4tc_EFFS_1vx.ReadFrom(tree);
    EF_j135_a4tc_EFFS.ReadFrom(tree);
    EF_j135_a4tc_EFFS_ht500.ReadFrom(tree);
    EF_j135_j30_a4tc_EFFS_L2dphi04.ReadFrom(tree);
    EF_j135_j30_a4tc_EFFS_dphi04.ReadFrom(tree);
    EF_j15_a4tc_EFFS.ReadFrom(tree);
    EF_j180_a4tc_EFFS.ReadFrom(tree);
    EF_j180_j30_a4tc_EFFS_dphi04.ReadFrom(tree);
    EF_j20_a4tc_EFFS.ReadFrom(tree);
    EF_j240_a10tc_EFFS.ReadFrom(tree);
    EF_j240_a4tc_EFFS.ReadFrom(tree);
    EF_j240_a4tc_EFFS_l2cleanph.ReadFrom(tree);
    EF_j30_a4tc_EFFS.ReadFrom(tree);
    EF_j30_a4tc_EFFS_l2cleanph.ReadFrom(tree);
    EF_j30_cosmic.ReadFrom(tree);
    EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty.ReadFrom(tree);
    EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty.ReadFrom(tree);
    EF_j30_firstempty.ReadFrom(tree);
    EF_j30_fj30_a4tc_EFFS.ReadFrom(tree);
    EF_j320_a10tc_EFFS.ReadFrom(tree);
    EF_j320_a4tc_EFFS.ReadFrom(tree);
    EF_j35_a4tc_EFFS.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV_cosmic.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV_firstempty.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso.ReadFrom(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso.ReadFrom(tree);
    EF_j40_a4tc_EFFS.ReadFrom(tree);
    EF_j40_fj40_a4tc_EFFS.ReadFrom(tree);
    EF_j425_a10tc_EFFS.ReadFrom(tree);
    EF_j425_a4tc_EFFS.ReadFrom(tree);
    EF_j45_a4tc_EFFS.ReadFrom(tree);
    EF_j50_a4tc_EFFS.ReadFrom(tree);
    EF_j50_cosmic.ReadFrom(tree);
    EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty.ReadFrom(tree);
    EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty.ReadFrom(tree);
    EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty.ReadFrom(tree);
    EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty.ReadFrom(tree);
    EF_j50_firstempty.ReadFrom(tree);
    EF_j55_a4tc_EFFS.ReadFrom(tree);
    EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10.ReadFrom(tree);
    EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons.ReadFrom(tree);
    EF_j55_fj55_a4tc_EFFS.ReadFrom(tree);
    EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10.ReadFrom(tree);
    EF_j70_j25_dphi03_NoEF.ReadFrom(tree);
    EF_j75_2j30_a4tc_EFFS_ht350.ReadFrom(tree);
    EF_j75_a4tc_EFFS.ReadFrom(tree);
    EF_j75_a4tc_EFFS_xe40_loose_noMu.ReadFrom(tree);
    EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03.ReadFrom(tree);
    EF_j75_a4tc_EFFS_xe45_loose_noMu.ReadFrom(tree);
    EF_j75_a4tc_EFFS_xe55_loose_noMu.ReadFrom(tree);
    EF_j75_a4tc_EFFS_xe55_noMu.ReadFrom(tree);
    EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons.ReadFrom(tree);
    EF_j75_a4tc_EFFS_xs35_noMu.ReadFrom(tree);
    EF_j75_fj75_a4tc_EFFS.ReadFrom(tree);
    EF_j75_j30_a4tc_EFFS.ReadFrom(tree);
    EF_j75_j30_a4tc_EFFS_L2anymct100.ReadFrom(tree);
    EF_j75_j30_a4tc_EFFS_L2anymct150.ReadFrom(tree);
    EF_j75_j30_a4tc_EFFS_L2anymct175.ReadFrom(tree);
    EF_j75_j30_a4tc_EFFS_L2dphi04.ReadFrom(tree);
    EF_j75_j30_a4tc_EFFS_anymct150.ReadFrom(tree);
    EF_j75_j30_a4tc_EFFS_anymct175.ReadFrom(tree);
    EF_j75_j30_a4tc_EFFS_leadingmct150.ReadFrom(tree);
    EF_j80_a4tc_EFFS_xe60_noMu.ReadFrom(tree);
    EF_je195_NoEF.ReadFrom(tree);
    EF_je255_NoEF.ReadFrom(tree);
    EF_je300_NoEF.ReadFrom(tree);
    EF_je350_NoEF.ReadFrom(tree);
    EF_je420_NoEF.ReadFrom(tree);
    EF_je500_NoEF.ReadFrom(tree);
    n.ReadFrom(tree);
    emscale_E.ReadFrom(tree);
    emscale_pt.ReadFrom(tree);
    emscale_m.ReadFrom(tree);
    emscale_eta.ReadFrom(tree);
    emscale_phi.ReadFrom(tree);
    a4.ReadFrom(tree);
    a4tc.ReadFrom(tree);
    a10tc.ReadFrom(tree);
    a6.ReadFrom(tree);
    a6tc.ReadFrom(tree);
    RoIword.ReadFrom(tree);
    EF_3j10_a4tc_EFFS.ReadFrom(tree);
    EF_4j10_a4tc_EFFS.ReadFrom(tree);
    EF_5j10_a4tc_EFFS.ReadFrom(tree);
    EF_j15_a2hi_EFFS.ReadFrom(tree);
    EF_j15_a2tc_EFFS.ReadFrom(tree);
    EF_j20_a2hi_EFFS.ReadFrom(tree);
    EF_j20_a2tc_EFFS.ReadFrom(tree);
    EF_j20_a4hi_EFFS.ReadFrom(tree);
    EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10.ReadFrom(tree);
    EF_j75_j45_a4tc_EFFS_xe55_noMu.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TrigEFJetD3PDCollection::WriteTo(TTree* tree)
  {
    EF_2fj100_a4tc_EFFS_deta50_FB.WriteTo(tree);
    EF_2fj30_a4tc_EFFS_deta50_FB.WriteTo(tree);
    EF_2fj30_a4tc_EFFS_deta50_FC.WriteTo(tree);
    EF_2fj55_a4tc_EFFS_deta50_FB.WriteTo(tree);
    EF_2fj55_a4tc_EFFS_deta50_FC.WriteTo(tree);
    EF_2fj75_a4tc_EFFS_deta50_FB.WriteTo(tree);
    EF_2fj75_a4tc_EFFS_deta50_FC.WriteTo(tree);
    EF_2j100_a4tc_EFFS_deta35_FC.WriteTo(tree);
    EF_2j135_a4tc_EFFS_deta35_FC.WriteTo(tree);
    EF_2j180_a4tc_EFFS_deta35_FC.WriteTo(tree);
    EF_2j240_a4tc_EFFS_deta35_FC.WriteTo(tree);
    EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu.WriteTo(tree);
    EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu.WriteTo(tree);
    EF_3j100_a4tc_EFFS.WriteTo(tree);
    EF_3j100_a4tc_EFFS_L1J75.WriteTo(tree);
    EF_3j30_a4tc_EFFS.WriteTo(tree);
    EF_3j40_a4tc_EFFS.WriteTo(tree);
    EF_3j45_a4tc_EFFS.WriteTo(tree);
    EF_3j75_a4tc_EFFS.WriteTo(tree);
    EF_4j30_a4tc_EFFS.WriteTo(tree);
    EF_4j40_a4tc_EFFS.WriteTo(tree);
    EF_4j40_a4tc_EFFS_ht350.WriteTo(tree);
    EF_4j40_a4tc_EFFS_ht400.WriteTo(tree);
    EF_4j45_a4tc_EFFS.WriteTo(tree);
    EF_4j55_a4tc_EFFS.WriteTo(tree);
    EF_4j60_a4tc_EFFS.WriteTo(tree);
    EF_5j30_a4tc_EFFS.WriteTo(tree);
    EF_5j40_a4tc_EFFS.WriteTo(tree);
    EF_5j45_a4tc_EFFS.WriteTo(tree);
    EF_6j30_a4tc_EFFS.WriteTo(tree);
    EF_6j30_a4tc_EFFS_L15J10.WriteTo(tree);
    EF_6j40_a4tc_EFFS.WriteTo(tree);
    EF_6j45_a4tc_EFFS.WriteTo(tree);
    EF_7j30_a4tc_EFFS_L15J10.WriteTo(tree);
    EF_7j30_a4tc_EFFS_L16J10.WriteTo(tree);
    EF_fj100_a4tc_EFFS.WriteTo(tree);
    EF_fj10_a4tc_EFFS.WriteTo(tree);
    EF_fj10_a4tc_EFFS_1vx.WriteTo(tree);
    EF_fj135_a4tc_EFFS.WriteTo(tree);
    EF_fj15_a4tc_EFFS.WriteTo(tree);
    EF_fj20_a4tc_EFFS.WriteTo(tree);
    EF_fj30_a4tc_EFFS.WriteTo(tree);
    EF_fj30_a4tc_EFFS_l2cleanph.WriteTo(tree);
    EF_fj55_a4tc_EFFS.WriteTo(tree);
    EF_fj75_a4tc_EFFS.WriteTo(tree);
    EF_j100_a4tc_EFFS.WriteTo(tree);
    EF_j100_a4tc_EFFS_ht350.WriteTo(tree);
    EF_j100_a4tc_EFFS_ht400.WriteTo(tree);
    EF_j100_a4tc_EFFS_ht500.WriteTo(tree);
    EF_j100_j30_a4tc_EFFS_L2dphi04.WriteTo(tree);
    EF_j10_a4tc_EFFS.WriteTo(tree);
    EF_j10_a4tc_EFFS_1vx.WriteTo(tree);
    EF_j135_a4tc_EFFS.WriteTo(tree);
    EF_j135_a4tc_EFFS_ht500.WriteTo(tree);
    EF_j135_j30_a4tc_EFFS_L2dphi04.WriteTo(tree);
    EF_j135_j30_a4tc_EFFS_dphi04.WriteTo(tree);
    EF_j15_a4tc_EFFS.WriteTo(tree);
    EF_j180_a4tc_EFFS.WriteTo(tree);
    EF_j180_j30_a4tc_EFFS_dphi04.WriteTo(tree);
    EF_j20_a4tc_EFFS.WriteTo(tree);
    EF_j240_a10tc_EFFS.WriteTo(tree);
    EF_j240_a4tc_EFFS.WriteTo(tree);
    EF_j240_a4tc_EFFS_l2cleanph.WriteTo(tree);
    EF_j30_a4tc_EFFS.WriteTo(tree);
    EF_j30_a4tc_EFFS_l2cleanph.WriteTo(tree);
    EF_j30_cosmic.WriteTo(tree);
    EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty.WriteTo(tree);
    EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty.WriteTo(tree);
    EF_j30_firstempty.WriteTo(tree);
    EF_j30_fj30_a4tc_EFFS.WriteTo(tree);
    EF_j320_a10tc_EFFS.WriteTo(tree);
    EF_j320_a4tc_EFFS.WriteTo(tree);
    EF_j35_a4tc_EFFS.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV_cosmic.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV_firstempty.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso.WriteTo(tree);
    EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso.WriteTo(tree);
    EF_j40_a4tc_EFFS.WriteTo(tree);
    EF_j40_fj40_a4tc_EFFS.WriteTo(tree);
    EF_j425_a10tc_EFFS.WriteTo(tree);
    EF_j425_a4tc_EFFS.WriteTo(tree);
    EF_j45_a4tc_EFFS.WriteTo(tree);
    EF_j50_a4tc_EFFS.WriteTo(tree);
    EF_j50_cosmic.WriteTo(tree);
    EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty.WriteTo(tree);
    EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty.WriteTo(tree);
    EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty.WriteTo(tree);
    EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty.WriteTo(tree);
    EF_j50_firstempty.WriteTo(tree);
    EF_j55_a4tc_EFFS.WriteTo(tree);
    EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10.WriteTo(tree);
    EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons.WriteTo(tree);
    EF_j55_fj55_a4tc_EFFS.WriteTo(tree);
    EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10.WriteTo(tree);
    EF_j70_j25_dphi03_NoEF.WriteTo(tree);
    EF_j75_2j30_a4tc_EFFS_ht350.WriteTo(tree);
    EF_j75_a4tc_EFFS.WriteTo(tree);
    EF_j75_a4tc_EFFS_xe40_loose_noMu.WriteTo(tree);
    EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03.WriteTo(tree);
    EF_j75_a4tc_EFFS_xe45_loose_noMu.WriteTo(tree);
    EF_j75_a4tc_EFFS_xe55_loose_noMu.WriteTo(tree);
    EF_j75_a4tc_EFFS_xe55_noMu.WriteTo(tree);
    EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons.WriteTo(tree);
    EF_j75_a4tc_EFFS_xs35_noMu.WriteTo(tree);
    EF_j75_fj75_a4tc_EFFS.WriteTo(tree);
    EF_j75_j30_a4tc_EFFS.WriteTo(tree);
    EF_j75_j30_a4tc_EFFS_L2anymct100.WriteTo(tree);
    EF_j75_j30_a4tc_EFFS_L2anymct150.WriteTo(tree);
    EF_j75_j30_a4tc_EFFS_L2anymct175.WriteTo(tree);
    EF_j75_j30_a4tc_EFFS_L2dphi04.WriteTo(tree);
    EF_j75_j30_a4tc_EFFS_anymct150.WriteTo(tree);
    EF_j75_j30_a4tc_EFFS_anymct175.WriteTo(tree);
    EF_j75_j30_a4tc_EFFS_leadingmct150.WriteTo(tree);
    EF_j80_a4tc_EFFS_xe60_noMu.WriteTo(tree);
    EF_je195_NoEF.WriteTo(tree);
    EF_je255_NoEF.WriteTo(tree);
    EF_je300_NoEF.WriteTo(tree);
    EF_je350_NoEF.WriteTo(tree);
    EF_je420_NoEF.WriteTo(tree);
    EF_je500_NoEF.WriteTo(tree);
    n.WriteTo(tree);
    emscale_E.WriteTo(tree);
    emscale_pt.WriteTo(tree);
    emscale_m.WriteTo(tree);
    emscale_eta.WriteTo(tree);
    emscale_phi.WriteTo(tree);
    a4.WriteTo(tree);
    a4tc.WriteTo(tree);
    a10tc.WriteTo(tree);
    a6.WriteTo(tree);
    a6tc.WriteTo(tree);
    RoIword.WriteTo(tree);
    EF_3j10_a4tc_EFFS.WriteTo(tree);
    EF_4j10_a4tc_EFFS.WriteTo(tree);
    EF_5j10_a4tc_EFFS.WriteTo(tree);
    EF_j15_a2hi_EFFS.WriteTo(tree);
    EF_j15_a2tc_EFFS.WriteTo(tree);
    EF_j20_a2hi_EFFS.WriteTo(tree);
    EF_j20_a2tc_EFFS.WriteTo(tree);
    EF_j20_a4hi_EFFS.WriteTo(tree);
    EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10.WriteTo(tree);
    EF_j75_j45_a4tc_EFFS_xe55_noMu.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TrigEFJetD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(EF_2fj100_a4tc_EFFS_deta50_FB.IsAvailable()) EF_2fj100_a4tc_EFFS_deta50_FB.SetActive(active);
     if(EF_2fj30_a4tc_EFFS_deta50_FB.IsAvailable()) EF_2fj30_a4tc_EFFS_deta50_FB.SetActive(active);
     if(EF_2fj30_a4tc_EFFS_deta50_FC.IsAvailable()) EF_2fj30_a4tc_EFFS_deta50_FC.SetActive(active);
     if(EF_2fj55_a4tc_EFFS_deta50_FB.IsAvailable()) EF_2fj55_a4tc_EFFS_deta50_FB.SetActive(active);
     if(EF_2fj55_a4tc_EFFS_deta50_FC.IsAvailable()) EF_2fj55_a4tc_EFFS_deta50_FC.SetActive(active);
     if(EF_2fj75_a4tc_EFFS_deta50_FB.IsAvailable()) EF_2fj75_a4tc_EFFS_deta50_FB.SetActive(active);
     if(EF_2fj75_a4tc_EFFS_deta50_FC.IsAvailable()) EF_2fj75_a4tc_EFFS_deta50_FC.SetActive(active);
     if(EF_2j100_a4tc_EFFS_deta35_FC.IsAvailable()) EF_2j100_a4tc_EFFS_deta35_FC.SetActive(active);
     if(EF_2j135_a4tc_EFFS_deta35_FC.IsAvailable()) EF_2j135_a4tc_EFFS_deta35_FC.SetActive(active);
     if(EF_2j180_a4tc_EFFS_deta35_FC.IsAvailable()) EF_2j180_a4tc_EFFS_deta35_FC.SetActive(active);
     if(EF_2j240_a4tc_EFFS_deta35_FC.IsAvailable()) EF_2j240_a4tc_EFFS_deta35_FC.SetActive(active);
     if(EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu.IsAvailable()) EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu.SetActive(active);
     if(EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu.IsAvailable()) EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu.SetActive(active);
     if(EF_3j100_a4tc_EFFS.IsAvailable()) EF_3j100_a4tc_EFFS.SetActive(active);
     if(EF_3j100_a4tc_EFFS_L1J75.IsAvailable()) EF_3j100_a4tc_EFFS_L1J75.SetActive(active);
     if(EF_3j30_a4tc_EFFS.IsAvailable()) EF_3j30_a4tc_EFFS.SetActive(active);
     if(EF_3j40_a4tc_EFFS.IsAvailable()) EF_3j40_a4tc_EFFS.SetActive(active);
     if(EF_3j45_a4tc_EFFS.IsAvailable()) EF_3j45_a4tc_EFFS.SetActive(active);
     if(EF_3j75_a4tc_EFFS.IsAvailable()) EF_3j75_a4tc_EFFS.SetActive(active);
     if(EF_4j30_a4tc_EFFS.IsAvailable()) EF_4j30_a4tc_EFFS.SetActive(active);
     if(EF_4j40_a4tc_EFFS.IsAvailable()) EF_4j40_a4tc_EFFS.SetActive(active);
     if(EF_4j40_a4tc_EFFS_ht350.IsAvailable()) EF_4j40_a4tc_EFFS_ht350.SetActive(active);
     if(EF_4j40_a4tc_EFFS_ht400.IsAvailable()) EF_4j40_a4tc_EFFS_ht400.SetActive(active);
     if(EF_4j45_a4tc_EFFS.IsAvailable()) EF_4j45_a4tc_EFFS.SetActive(active);
     if(EF_4j55_a4tc_EFFS.IsAvailable()) EF_4j55_a4tc_EFFS.SetActive(active);
     if(EF_4j60_a4tc_EFFS.IsAvailable()) EF_4j60_a4tc_EFFS.SetActive(active);
     if(EF_5j30_a4tc_EFFS.IsAvailable()) EF_5j30_a4tc_EFFS.SetActive(active);
     if(EF_5j40_a4tc_EFFS.IsAvailable()) EF_5j40_a4tc_EFFS.SetActive(active);
     if(EF_5j45_a4tc_EFFS.IsAvailable()) EF_5j45_a4tc_EFFS.SetActive(active);
     if(EF_6j30_a4tc_EFFS.IsAvailable()) EF_6j30_a4tc_EFFS.SetActive(active);
     if(EF_6j30_a4tc_EFFS_L15J10.IsAvailable()) EF_6j30_a4tc_EFFS_L15J10.SetActive(active);
     if(EF_6j40_a4tc_EFFS.IsAvailable()) EF_6j40_a4tc_EFFS.SetActive(active);
     if(EF_6j45_a4tc_EFFS.IsAvailable()) EF_6j45_a4tc_EFFS.SetActive(active);
     if(EF_7j30_a4tc_EFFS_L15J10.IsAvailable()) EF_7j30_a4tc_EFFS_L15J10.SetActive(active);
     if(EF_7j30_a4tc_EFFS_L16J10.IsAvailable()) EF_7j30_a4tc_EFFS_L16J10.SetActive(active);
     if(EF_fj100_a4tc_EFFS.IsAvailable()) EF_fj100_a4tc_EFFS.SetActive(active);
     if(EF_fj10_a4tc_EFFS.IsAvailable()) EF_fj10_a4tc_EFFS.SetActive(active);
     if(EF_fj10_a4tc_EFFS_1vx.IsAvailable()) EF_fj10_a4tc_EFFS_1vx.SetActive(active);
     if(EF_fj135_a4tc_EFFS.IsAvailable()) EF_fj135_a4tc_EFFS.SetActive(active);
     if(EF_fj15_a4tc_EFFS.IsAvailable()) EF_fj15_a4tc_EFFS.SetActive(active);
     if(EF_fj20_a4tc_EFFS.IsAvailable()) EF_fj20_a4tc_EFFS.SetActive(active);
     if(EF_fj30_a4tc_EFFS.IsAvailable()) EF_fj30_a4tc_EFFS.SetActive(active);
     if(EF_fj30_a4tc_EFFS_l2cleanph.IsAvailable()) EF_fj30_a4tc_EFFS_l2cleanph.SetActive(active);
     if(EF_fj55_a4tc_EFFS.IsAvailable()) EF_fj55_a4tc_EFFS.SetActive(active);
     if(EF_fj75_a4tc_EFFS.IsAvailable()) EF_fj75_a4tc_EFFS.SetActive(active);
     if(EF_j100_a4tc_EFFS.IsAvailable()) EF_j100_a4tc_EFFS.SetActive(active);
     if(EF_j100_a4tc_EFFS_ht350.IsAvailable()) EF_j100_a4tc_EFFS_ht350.SetActive(active);
     if(EF_j100_a4tc_EFFS_ht400.IsAvailable()) EF_j100_a4tc_EFFS_ht400.SetActive(active);
     if(EF_j100_a4tc_EFFS_ht500.IsAvailable()) EF_j100_a4tc_EFFS_ht500.SetActive(active);
     if(EF_j100_j30_a4tc_EFFS_L2dphi04.IsAvailable()) EF_j100_j30_a4tc_EFFS_L2dphi04.SetActive(active);
     if(EF_j10_a4tc_EFFS.IsAvailable()) EF_j10_a4tc_EFFS.SetActive(active);
     if(EF_j10_a4tc_EFFS_1vx.IsAvailable()) EF_j10_a4tc_EFFS_1vx.SetActive(active);
     if(EF_j135_a4tc_EFFS.IsAvailable()) EF_j135_a4tc_EFFS.SetActive(active);
     if(EF_j135_a4tc_EFFS_ht500.IsAvailable()) EF_j135_a4tc_EFFS_ht500.SetActive(active);
     if(EF_j135_j30_a4tc_EFFS_L2dphi04.IsAvailable()) EF_j135_j30_a4tc_EFFS_L2dphi04.SetActive(active);
     if(EF_j135_j30_a4tc_EFFS_dphi04.IsAvailable()) EF_j135_j30_a4tc_EFFS_dphi04.SetActive(active);
     if(EF_j15_a4tc_EFFS.IsAvailable()) EF_j15_a4tc_EFFS.SetActive(active);
     if(EF_j180_a4tc_EFFS.IsAvailable()) EF_j180_a4tc_EFFS.SetActive(active);
     if(EF_j180_j30_a4tc_EFFS_dphi04.IsAvailable()) EF_j180_j30_a4tc_EFFS_dphi04.SetActive(active);
     if(EF_j20_a4tc_EFFS.IsAvailable()) EF_j20_a4tc_EFFS.SetActive(active);
     if(EF_j240_a10tc_EFFS.IsAvailable()) EF_j240_a10tc_EFFS.SetActive(active);
     if(EF_j240_a4tc_EFFS.IsAvailable()) EF_j240_a4tc_EFFS.SetActive(active);
     if(EF_j240_a4tc_EFFS_l2cleanph.IsAvailable()) EF_j240_a4tc_EFFS_l2cleanph.SetActive(active);
     if(EF_j30_a4tc_EFFS.IsAvailable()) EF_j30_a4tc_EFFS.SetActive(active);
     if(EF_j30_a4tc_EFFS_l2cleanph.IsAvailable()) EF_j30_a4tc_EFFS_l2cleanph.SetActive(active);
     if(EF_j30_cosmic.IsAvailable()) EF_j30_cosmic.SetActive(active);
     if(EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty.IsAvailable()) EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty.SetActive(active);
     if(EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty.IsAvailable()) EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty.SetActive(active);
     if(EF_j30_firstempty.IsAvailable()) EF_j30_firstempty.SetActive(active);
     if(EF_j30_fj30_a4tc_EFFS.IsAvailable()) EF_j30_fj30_a4tc_EFFS.SetActive(active);
     if(EF_j320_a10tc_EFFS.IsAvailable()) EF_j320_a10tc_EFFS.SetActive(active);
     if(EF_j320_a4tc_EFFS.IsAvailable()) EF_j320_a4tc_EFFS.SetActive(active);
     if(EF_j35_a4tc_EFFS.IsAvailable()) EF_j35_a4tc_EFFS.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HV.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HV.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HV_cosmic.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HV_cosmic.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HV_firstempty.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HV_firstempty.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HVtrk.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HVtrk.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso.SetActive(active);
     if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso.IsAvailable()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso.SetActive(active);
     if(EF_j40_a4tc_EFFS.IsAvailable()) EF_j40_a4tc_EFFS.SetActive(active);
     if(EF_j40_fj40_a4tc_EFFS.IsAvailable()) EF_j40_fj40_a4tc_EFFS.SetActive(active);
     if(EF_j425_a10tc_EFFS.IsAvailable()) EF_j425_a10tc_EFFS.SetActive(active);
     if(EF_j425_a4tc_EFFS.IsAvailable()) EF_j425_a4tc_EFFS.SetActive(active);
     if(EF_j45_a4tc_EFFS.IsAvailable()) EF_j45_a4tc_EFFS.SetActive(active);
     if(EF_j50_a4tc_EFFS.IsAvailable()) EF_j50_a4tc_EFFS.SetActive(active);
     if(EF_j50_cosmic.IsAvailable()) EF_j50_cosmic.SetActive(active);
     if(EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty.IsAvailable()) EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty.SetActive(active);
     if(EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty.IsAvailable()) EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty.SetActive(active);
     if(EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty.IsAvailable()) EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty.SetActive(active);
     if(EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty.IsAvailable()) EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty.SetActive(active);
     if(EF_j50_firstempty.IsAvailable()) EF_j50_firstempty.SetActive(active);
     if(EF_j55_a4tc_EFFS.IsAvailable()) EF_j55_a4tc_EFFS.SetActive(active);
     if(EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10.IsAvailable()) EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10.SetActive(active);
     if(EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons.IsAvailable()) EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons.SetActive(active);
     if(EF_j55_fj55_a4tc_EFFS.IsAvailable()) EF_j55_fj55_a4tc_EFFS.SetActive(active);
     if(EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10.IsAvailable()) EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10.SetActive(active);
     if(EF_j70_j25_dphi03_NoEF.IsAvailable()) EF_j70_j25_dphi03_NoEF.SetActive(active);
     if(EF_j75_2j30_a4tc_EFFS_ht350.IsAvailable()) EF_j75_2j30_a4tc_EFFS_ht350.SetActive(active);
     if(EF_j75_a4tc_EFFS.IsAvailable()) EF_j75_a4tc_EFFS.SetActive(active);
     if(EF_j75_a4tc_EFFS_xe40_loose_noMu.IsAvailable()) EF_j75_a4tc_EFFS_xe40_loose_noMu.SetActive(active);
     if(EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03.IsAvailable()) EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03.SetActive(active);
     if(EF_j75_a4tc_EFFS_xe45_loose_noMu.IsAvailable()) EF_j75_a4tc_EFFS_xe45_loose_noMu.SetActive(active);
     if(EF_j75_a4tc_EFFS_xe55_loose_noMu.IsAvailable()) EF_j75_a4tc_EFFS_xe55_loose_noMu.SetActive(active);
     if(EF_j75_a4tc_EFFS_xe55_noMu.IsAvailable()) EF_j75_a4tc_EFFS_xe55_noMu.SetActive(active);
     if(EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons.IsAvailable()) EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons.SetActive(active);
     if(EF_j75_a4tc_EFFS_xs35_noMu.IsAvailable()) EF_j75_a4tc_EFFS_xs35_noMu.SetActive(active);
     if(EF_j75_fj75_a4tc_EFFS.IsAvailable()) EF_j75_fj75_a4tc_EFFS.SetActive(active);
     if(EF_j75_j30_a4tc_EFFS.IsAvailable()) EF_j75_j30_a4tc_EFFS.SetActive(active);
     if(EF_j75_j30_a4tc_EFFS_L2anymct100.IsAvailable()) EF_j75_j30_a4tc_EFFS_L2anymct100.SetActive(active);
     if(EF_j75_j30_a4tc_EFFS_L2anymct150.IsAvailable()) EF_j75_j30_a4tc_EFFS_L2anymct150.SetActive(active);
     if(EF_j75_j30_a4tc_EFFS_L2anymct175.IsAvailable()) EF_j75_j30_a4tc_EFFS_L2anymct175.SetActive(active);
     if(EF_j75_j30_a4tc_EFFS_L2dphi04.IsAvailable()) EF_j75_j30_a4tc_EFFS_L2dphi04.SetActive(active);
     if(EF_j75_j30_a4tc_EFFS_anymct150.IsAvailable()) EF_j75_j30_a4tc_EFFS_anymct150.SetActive(active);
     if(EF_j75_j30_a4tc_EFFS_anymct175.IsAvailable()) EF_j75_j30_a4tc_EFFS_anymct175.SetActive(active);
     if(EF_j75_j30_a4tc_EFFS_leadingmct150.IsAvailable()) EF_j75_j30_a4tc_EFFS_leadingmct150.SetActive(active);
     if(EF_j80_a4tc_EFFS_xe60_noMu.IsAvailable()) EF_j80_a4tc_EFFS_xe60_noMu.SetActive(active);
     if(EF_je195_NoEF.IsAvailable()) EF_je195_NoEF.SetActive(active);
     if(EF_je255_NoEF.IsAvailable()) EF_je255_NoEF.SetActive(active);
     if(EF_je300_NoEF.IsAvailable()) EF_je300_NoEF.SetActive(active);
     if(EF_je350_NoEF.IsAvailable()) EF_je350_NoEF.SetActive(active);
     if(EF_je420_NoEF.IsAvailable()) EF_je420_NoEF.SetActive(active);
     if(EF_je500_NoEF.IsAvailable()) EF_je500_NoEF.SetActive(active);
     if(n.IsAvailable()) n.SetActive(active);
     if(emscale_E.IsAvailable()) emscale_E.SetActive(active);
     if(emscale_pt.IsAvailable()) emscale_pt.SetActive(active);
     if(emscale_m.IsAvailable()) emscale_m.SetActive(active);
     if(emscale_eta.IsAvailable()) emscale_eta.SetActive(active);
     if(emscale_phi.IsAvailable()) emscale_phi.SetActive(active);
     if(a4.IsAvailable()) a4.SetActive(active);
     if(a4tc.IsAvailable()) a4tc.SetActive(active);
     if(a10tc.IsAvailable()) a10tc.SetActive(active);
     if(a6.IsAvailable()) a6.SetActive(active);
     if(a6tc.IsAvailable()) a6tc.SetActive(active);
     if(RoIword.IsAvailable()) RoIword.SetActive(active);
     if(EF_3j10_a4tc_EFFS.IsAvailable()) EF_3j10_a4tc_EFFS.SetActive(active);
     if(EF_4j10_a4tc_EFFS.IsAvailable()) EF_4j10_a4tc_EFFS.SetActive(active);
     if(EF_5j10_a4tc_EFFS.IsAvailable()) EF_5j10_a4tc_EFFS.SetActive(active);
     if(EF_j15_a2hi_EFFS.IsAvailable()) EF_j15_a2hi_EFFS.SetActive(active);
     if(EF_j15_a2tc_EFFS.IsAvailable()) EF_j15_a2tc_EFFS.SetActive(active);
     if(EF_j20_a2hi_EFFS.IsAvailable()) EF_j20_a2hi_EFFS.SetActive(active);
     if(EF_j20_a2tc_EFFS.IsAvailable()) EF_j20_a2tc_EFFS.SetActive(active);
     if(EF_j20_a4hi_EFFS.IsAvailable()) EF_j20_a4hi_EFFS.SetActive(active);
     if(EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10.IsAvailable()) EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10.SetActive(active);
     if(EF_j75_j45_a4tc_EFFS_xe55_noMu.IsAvailable()) EF_j75_j45_a4tc_EFFS_xe55_noMu.SetActive(active);
    }
    else
    {
      EF_2fj100_a4tc_EFFS_deta50_FB.SetActive(active);
      EF_2fj30_a4tc_EFFS_deta50_FB.SetActive(active);
      EF_2fj30_a4tc_EFFS_deta50_FC.SetActive(active);
      EF_2fj55_a4tc_EFFS_deta50_FB.SetActive(active);
      EF_2fj55_a4tc_EFFS_deta50_FC.SetActive(active);
      EF_2fj75_a4tc_EFFS_deta50_FB.SetActive(active);
      EF_2fj75_a4tc_EFFS_deta50_FC.SetActive(active);
      EF_2j100_a4tc_EFFS_deta35_FC.SetActive(active);
      EF_2j135_a4tc_EFFS_deta35_FC.SetActive(active);
      EF_2j180_a4tc_EFFS_deta35_FC.SetActive(active);
      EF_2j240_a4tc_EFFS_deta35_FC.SetActive(active);
      EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu.SetActive(active);
      EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu.SetActive(active);
      EF_3j100_a4tc_EFFS.SetActive(active);
      EF_3j100_a4tc_EFFS_L1J75.SetActive(active);
      EF_3j30_a4tc_EFFS.SetActive(active);
      EF_3j40_a4tc_EFFS.SetActive(active);
      EF_3j45_a4tc_EFFS.SetActive(active);
      EF_3j75_a4tc_EFFS.SetActive(active);
      EF_4j30_a4tc_EFFS.SetActive(active);
      EF_4j40_a4tc_EFFS.SetActive(active);
      EF_4j40_a4tc_EFFS_ht350.SetActive(active);
      EF_4j40_a4tc_EFFS_ht400.SetActive(active);
      EF_4j45_a4tc_EFFS.SetActive(active);
      EF_4j55_a4tc_EFFS.SetActive(active);
      EF_4j60_a4tc_EFFS.SetActive(active);
      EF_5j30_a4tc_EFFS.SetActive(active);
      EF_5j40_a4tc_EFFS.SetActive(active);
      EF_5j45_a4tc_EFFS.SetActive(active);
      EF_6j30_a4tc_EFFS.SetActive(active);
      EF_6j30_a4tc_EFFS_L15J10.SetActive(active);
      EF_6j40_a4tc_EFFS.SetActive(active);
      EF_6j45_a4tc_EFFS.SetActive(active);
      EF_7j30_a4tc_EFFS_L15J10.SetActive(active);
      EF_7j30_a4tc_EFFS_L16J10.SetActive(active);
      EF_fj100_a4tc_EFFS.SetActive(active);
      EF_fj10_a4tc_EFFS.SetActive(active);
      EF_fj10_a4tc_EFFS_1vx.SetActive(active);
      EF_fj135_a4tc_EFFS.SetActive(active);
      EF_fj15_a4tc_EFFS.SetActive(active);
      EF_fj20_a4tc_EFFS.SetActive(active);
      EF_fj30_a4tc_EFFS.SetActive(active);
      EF_fj30_a4tc_EFFS_l2cleanph.SetActive(active);
      EF_fj55_a4tc_EFFS.SetActive(active);
      EF_fj75_a4tc_EFFS.SetActive(active);
      EF_j100_a4tc_EFFS.SetActive(active);
      EF_j100_a4tc_EFFS_ht350.SetActive(active);
      EF_j100_a4tc_EFFS_ht400.SetActive(active);
      EF_j100_a4tc_EFFS_ht500.SetActive(active);
      EF_j100_j30_a4tc_EFFS_L2dphi04.SetActive(active);
      EF_j10_a4tc_EFFS.SetActive(active);
      EF_j10_a4tc_EFFS_1vx.SetActive(active);
      EF_j135_a4tc_EFFS.SetActive(active);
      EF_j135_a4tc_EFFS_ht500.SetActive(active);
      EF_j135_j30_a4tc_EFFS_L2dphi04.SetActive(active);
      EF_j135_j30_a4tc_EFFS_dphi04.SetActive(active);
      EF_j15_a4tc_EFFS.SetActive(active);
      EF_j180_a4tc_EFFS.SetActive(active);
      EF_j180_j30_a4tc_EFFS_dphi04.SetActive(active);
      EF_j20_a4tc_EFFS.SetActive(active);
      EF_j240_a10tc_EFFS.SetActive(active);
      EF_j240_a4tc_EFFS.SetActive(active);
      EF_j240_a4tc_EFFS_l2cleanph.SetActive(active);
      EF_j30_a4tc_EFFS.SetActive(active);
      EF_j30_a4tc_EFFS_l2cleanph.SetActive(active);
      EF_j30_cosmic.SetActive(active);
      EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty.SetActive(active);
      EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty.SetActive(active);
      EF_j30_firstempty.SetActive(active);
      EF_j30_fj30_a4tc_EFFS.SetActive(active);
      EF_j320_a10tc_EFFS.SetActive(active);
      EF_j320_a4tc_EFFS.SetActive(active);
      EF_j35_a4tc_EFFS.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HV.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HV_cosmic.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HV_firstempty.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HVtrk.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso.SetActive(active);
      EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso.SetActive(active);
      EF_j40_a4tc_EFFS.SetActive(active);
      EF_j40_fj40_a4tc_EFFS.SetActive(active);
      EF_j425_a10tc_EFFS.SetActive(active);
      EF_j425_a4tc_EFFS.SetActive(active);
      EF_j45_a4tc_EFFS.SetActive(active);
      EF_j50_a4tc_EFFS.SetActive(active);
      EF_j50_cosmic.SetActive(active);
      EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty.SetActive(active);
      EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty.SetActive(active);
      EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty.SetActive(active);
      EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty.SetActive(active);
      EF_j50_firstempty.SetActive(active);
      EF_j55_a4tc_EFFS.SetActive(active);
      EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10.SetActive(active);
      EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons.SetActive(active);
      EF_j55_fj55_a4tc_EFFS.SetActive(active);
      EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10.SetActive(active);
      EF_j70_j25_dphi03_NoEF.SetActive(active);
      EF_j75_2j30_a4tc_EFFS_ht350.SetActive(active);
      EF_j75_a4tc_EFFS.SetActive(active);
      EF_j75_a4tc_EFFS_xe40_loose_noMu.SetActive(active);
      EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03.SetActive(active);
      EF_j75_a4tc_EFFS_xe45_loose_noMu.SetActive(active);
      EF_j75_a4tc_EFFS_xe55_loose_noMu.SetActive(active);
      EF_j75_a4tc_EFFS_xe55_noMu.SetActive(active);
      EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons.SetActive(active);
      EF_j75_a4tc_EFFS_xs35_noMu.SetActive(active);
      EF_j75_fj75_a4tc_EFFS.SetActive(active);
      EF_j75_j30_a4tc_EFFS.SetActive(active);
      EF_j75_j30_a4tc_EFFS_L2anymct100.SetActive(active);
      EF_j75_j30_a4tc_EFFS_L2anymct150.SetActive(active);
      EF_j75_j30_a4tc_EFFS_L2anymct175.SetActive(active);
      EF_j75_j30_a4tc_EFFS_L2dphi04.SetActive(active);
      EF_j75_j30_a4tc_EFFS_anymct150.SetActive(active);
      EF_j75_j30_a4tc_EFFS_anymct175.SetActive(active);
      EF_j75_j30_a4tc_EFFS_leadingmct150.SetActive(active);
      EF_j80_a4tc_EFFS_xe60_noMu.SetActive(active);
      EF_je195_NoEF.SetActive(active);
      EF_je255_NoEF.SetActive(active);
      EF_je300_NoEF.SetActive(active);
      EF_je350_NoEF.SetActive(active);
      EF_je420_NoEF.SetActive(active);
      EF_je500_NoEF.SetActive(active);
      n.SetActive(active);
      emscale_E.SetActive(active);
      emscale_pt.SetActive(active);
      emscale_m.SetActive(active);
      emscale_eta.SetActive(active);
      emscale_phi.SetActive(active);
      a4.SetActive(active);
      a4tc.SetActive(active);
      a10tc.SetActive(active);
      a6.SetActive(active);
      a6tc.SetActive(active);
      RoIword.SetActive(active);
      EF_3j10_a4tc_EFFS.SetActive(active);
      EF_4j10_a4tc_EFFS.SetActive(active);
      EF_5j10_a4tc_EFFS.SetActive(active);
      EF_j15_a2hi_EFFS.SetActive(active);
      EF_j15_a2tc_EFFS.SetActive(active);
      EF_j20_a2hi_EFFS.SetActive(active);
      EF_j20_a2tc_EFFS.SetActive(active);
      EF_j20_a4hi_EFFS.SetActive(active);
      EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10.SetActive(active);
      EF_j75_j45_a4tc_EFFS_xe55_noMu.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TrigEFJetD3PDCollection::ReadAllActive()
  {
    if(EF_2fj100_a4tc_EFFS_deta50_FB.IsActive()) EF_2fj100_a4tc_EFFS_deta50_FB();
    if(EF_2fj30_a4tc_EFFS_deta50_FB.IsActive()) EF_2fj30_a4tc_EFFS_deta50_FB();
    if(EF_2fj30_a4tc_EFFS_deta50_FC.IsActive()) EF_2fj30_a4tc_EFFS_deta50_FC();
    if(EF_2fj55_a4tc_EFFS_deta50_FB.IsActive()) EF_2fj55_a4tc_EFFS_deta50_FB();
    if(EF_2fj55_a4tc_EFFS_deta50_FC.IsActive()) EF_2fj55_a4tc_EFFS_deta50_FC();
    if(EF_2fj75_a4tc_EFFS_deta50_FB.IsActive()) EF_2fj75_a4tc_EFFS_deta50_FB();
    if(EF_2fj75_a4tc_EFFS_deta50_FC.IsActive()) EF_2fj75_a4tc_EFFS_deta50_FC();
    if(EF_2j100_a4tc_EFFS_deta35_FC.IsActive()) EF_2j100_a4tc_EFFS_deta35_FC();
    if(EF_2j135_a4tc_EFFS_deta35_FC.IsActive()) EF_2j135_a4tc_EFFS_deta35_FC();
    if(EF_2j180_a4tc_EFFS_deta35_FC.IsActive()) EF_2j180_a4tc_EFFS_deta35_FC();
    if(EF_2j240_a4tc_EFFS_deta35_FC.IsActive()) EF_2j240_a4tc_EFFS_deta35_FC();
    if(EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu.IsActive()) EF_2j45_a4tc_EFFS_leadingmct100_xe40_medium_noMu();
    if(EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu.IsActive()) EF_2j55_a4tc_EFFS_leadingmct100_xe40_medium_noMu();
    if(EF_3j100_a4tc_EFFS.IsActive()) EF_3j100_a4tc_EFFS();
    if(EF_3j100_a4tc_EFFS_L1J75.IsActive()) EF_3j100_a4tc_EFFS_L1J75();
    if(EF_3j30_a4tc_EFFS.IsActive()) EF_3j30_a4tc_EFFS();
    if(EF_3j40_a4tc_EFFS.IsActive()) EF_3j40_a4tc_EFFS();
    if(EF_3j45_a4tc_EFFS.IsActive()) EF_3j45_a4tc_EFFS();
    if(EF_3j75_a4tc_EFFS.IsActive()) EF_3j75_a4tc_EFFS();
    if(EF_4j30_a4tc_EFFS.IsActive()) EF_4j30_a4tc_EFFS();
    if(EF_4j40_a4tc_EFFS.IsActive()) EF_4j40_a4tc_EFFS();
    if(EF_4j40_a4tc_EFFS_ht350.IsActive()) EF_4j40_a4tc_EFFS_ht350();
    if(EF_4j40_a4tc_EFFS_ht400.IsActive()) EF_4j40_a4tc_EFFS_ht400();
    if(EF_4j45_a4tc_EFFS.IsActive()) EF_4j45_a4tc_EFFS();
    if(EF_4j55_a4tc_EFFS.IsActive()) EF_4j55_a4tc_EFFS();
    if(EF_4j60_a4tc_EFFS.IsActive()) EF_4j60_a4tc_EFFS();
    if(EF_5j30_a4tc_EFFS.IsActive()) EF_5j30_a4tc_EFFS();
    if(EF_5j40_a4tc_EFFS.IsActive()) EF_5j40_a4tc_EFFS();
    if(EF_5j45_a4tc_EFFS.IsActive()) EF_5j45_a4tc_EFFS();
    if(EF_6j30_a4tc_EFFS.IsActive()) EF_6j30_a4tc_EFFS();
    if(EF_6j30_a4tc_EFFS_L15J10.IsActive()) EF_6j30_a4tc_EFFS_L15J10();
    if(EF_6j40_a4tc_EFFS.IsActive()) EF_6j40_a4tc_EFFS();
    if(EF_6j45_a4tc_EFFS.IsActive()) EF_6j45_a4tc_EFFS();
    if(EF_7j30_a4tc_EFFS_L15J10.IsActive()) EF_7j30_a4tc_EFFS_L15J10();
    if(EF_7j30_a4tc_EFFS_L16J10.IsActive()) EF_7j30_a4tc_EFFS_L16J10();
    if(EF_fj100_a4tc_EFFS.IsActive()) EF_fj100_a4tc_EFFS();
    if(EF_fj10_a4tc_EFFS.IsActive()) EF_fj10_a4tc_EFFS();
    if(EF_fj10_a4tc_EFFS_1vx.IsActive()) EF_fj10_a4tc_EFFS_1vx();
    if(EF_fj135_a4tc_EFFS.IsActive()) EF_fj135_a4tc_EFFS();
    if(EF_fj15_a4tc_EFFS.IsActive()) EF_fj15_a4tc_EFFS();
    if(EF_fj20_a4tc_EFFS.IsActive()) EF_fj20_a4tc_EFFS();
    if(EF_fj30_a4tc_EFFS.IsActive()) EF_fj30_a4tc_EFFS();
    if(EF_fj30_a4tc_EFFS_l2cleanph.IsActive()) EF_fj30_a4tc_EFFS_l2cleanph();
    if(EF_fj55_a4tc_EFFS.IsActive()) EF_fj55_a4tc_EFFS();
    if(EF_fj75_a4tc_EFFS.IsActive()) EF_fj75_a4tc_EFFS();
    if(EF_j100_a4tc_EFFS.IsActive()) EF_j100_a4tc_EFFS();
    if(EF_j100_a4tc_EFFS_ht350.IsActive()) EF_j100_a4tc_EFFS_ht350();
    if(EF_j100_a4tc_EFFS_ht400.IsActive()) EF_j100_a4tc_EFFS_ht400();
    if(EF_j100_a4tc_EFFS_ht500.IsActive()) EF_j100_a4tc_EFFS_ht500();
    if(EF_j100_j30_a4tc_EFFS_L2dphi04.IsActive()) EF_j100_j30_a4tc_EFFS_L2dphi04();
    if(EF_j10_a4tc_EFFS.IsActive()) EF_j10_a4tc_EFFS();
    if(EF_j10_a4tc_EFFS_1vx.IsActive()) EF_j10_a4tc_EFFS_1vx();
    if(EF_j135_a4tc_EFFS.IsActive()) EF_j135_a4tc_EFFS();
    if(EF_j135_a4tc_EFFS_ht500.IsActive()) EF_j135_a4tc_EFFS_ht500();
    if(EF_j135_j30_a4tc_EFFS_L2dphi04.IsActive()) EF_j135_j30_a4tc_EFFS_L2dphi04();
    if(EF_j135_j30_a4tc_EFFS_dphi04.IsActive()) EF_j135_j30_a4tc_EFFS_dphi04();
    if(EF_j15_a4tc_EFFS.IsActive()) EF_j15_a4tc_EFFS();
    if(EF_j180_a4tc_EFFS.IsActive()) EF_j180_a4tc_EFFS();
    if(EF_j180_j30_a4tc_EFFS_dphi04.IsActive()) EF_j180_j30_a4tc_EFFS_dphi04();
    if(EF_j20_a4tc_EFFS.IsActive()) EF_j20_a4tc_EFFS();
    if(EF_j240_a10tc_EFFS.IsActive()) EF_j240_a10tc_EFFS();
    if(EF_j240_a4tc_EFFS.IsActive()) EF_j240_a4tc_EFFS();
    if(EF_j240_a4tc_EFFS_l2cleanph.IsActive()) EF_j240_a4tc_EFFS_l2cleanph();
    if(EF_j30_a4tc_EFFS.IsActive()) EF_j30_a4tc_EFFS();
    if(EF_j30_a4tc_EFFS_l2cleanph.IsActive()) EF_j30_a4tc_EFFS_l2cleanph();
    if(EF_j30_cosmic.IsActive()) EF_j30_cosmic();
    if(EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty.IsActive()) EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_empty();
    if(EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty.IsActive()) EF_j30_eta13_a4tc_EFFS_EFxe30_noMu_firstempty();
    if(EF_j30_firstempty.IsActive()) EF_j30_firstempty();
    if(EF_j30_fj30_a4tc_EFFS.IsActive()) EF_j30_fj30_a4tc_EFFS();
    if(EF_j320_a10tc_EFFS.IsActive()) EF_j320_a10tc_EFFS();
    if(EF_j320_a4tc_EFFS.IsActive()) EF_j320_a4tc_EFFS();
    if(EF_j35_a4tc_EFFS.IsActive()) EF_j35_a4tc_EFFS();
    if(EF_j35_a4tc_EFFS_L1TAU_HV.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HV();
    if(EF_j35_a4tc_EFFS_L1TAU_HV_cosmic.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HV_cosmic();
    if(EF_j35_a4tc_EFFS_L1TAU_HV_firstempty.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HV_firstempty();
    if(EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_iso();
    if(EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HV_unpaired_noniso();
    if(EF_j35_a4tc_EFFS_L1TAU_HVtrk.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HVtrk();
    if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_LOF();
    if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_cosmic();
    if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_firstempty();
    if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_iso();
    if(EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso.IsActive()) EF_j35_a4tc_EFFS_L1TAU_HVtrk_unpaired_noniso();
    if(EF_j40_a4tc_EFFS.IsActive()) EF_j40_a4tc_EFFS();
    if(EF_j40_fj40_a4tc_EFFS.IsActive()) EF_j40_fj40_a4tc_EFFS();
    if(EF_j425_a10tc_EFFS.IsActive()) EF_j425_a10tc_EFFS();
    if(EF_j425_a4tc_EFFS.IsActive()) EF_j425_a4tc_EFFS();
    if(EF_j45_a4tc_EFFS.IsActive()) EF_j45_a4tc_EFFS();
    if(EF_j50_a4tc_EFFS.IsActive()) EF_j50_a4tc_EFFS();
    if(EF_j50_cosmic.IsActive()) EF_j50_cosmic();
    if(EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty.IsActive()) EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_empty();
    if(EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty.IsActive()) EF_j50_eta13_a4tc_EFFS_EFxe50_noMu_firstempty();
    if(EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty.IsActive()) EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_empty();
    if(EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty.IsActive()) EF_j50_eta25_a4tc_EFFS_EFxe50_noMu_firstempty();
    if(EF_j50_firstempty.IsActive()) EF_j50_firstempty();
    if(EF_j55_a4tc_EFFS.IsActive()) EF_j55_a4tc_EFFS();
    if(EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10.IsActive()) EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10();
    if(EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons.IsActive()) EF_j55_a4tc_EFFS_xe55_medium_noMu_dphi2j30xe10_l2cleancons();
    if(EF_j55_fj55_a4tc_EFFS.IsActive()) EF_j55_fj55_a4tc_EFFS();
    if(EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10.IsActive()) EF_j65_a4tc_EFFS_xe65_noMu_dphi2j30xe10();
    if(EF_j70_j25_dphi03_NoEF.IsActive()) EF_j70_j25_dphi03_NoEF();
    if(EF_j75_2j30_a4tc_EFFS_ht350.IsActive()) EF_j75_2j30_a4tc_EFFS_ht350();
    if(EF_j75_a4tc_EFFS.IsActive()) EF_j75_a4tc_EFFS();
    if(EF_j75_a4tc_EFFS_xe40_loose_noMu.IsActive()) EF_j75_a4tc_EFFS_xe40_loose_noMu();
    if(EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03.IsActive()) EF_j75_a4tc_EFFS_xe40_loose_noMu_dphijxe03();
    if(EF_j75_a4tc_EFFS_xe45_loose_noMu.IsActive()) EF_j75_a4tc_EFFS_xe45_loose_noMu();
    if(EF_j75_a4tc_EFFS_xe55_loose_noMu.IsActive()) EF_j75_a4tc_EFFS_xe55_loose_noMu();
    if(EF_j75_a4tc_EFFS_xe55_noMu.IsActive()) EF_j75_a4tc_EFFS_xe55_noMu();
    if(EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons.IsActive()) EF_j75_a4tc_EFFS_xe55_noMu_l2cleancons();
    if(EF_j75_a4tc_EFFS_xs35_noMu.IsActive()) EF_j75_a4tc_EFFS_xs35_noMu();
    if(EF_j75_fj75_a4tc_EFFS.IsActive()) EF_j75_fj75_a4tc_EFFS();
    if(EF_j75_j30_a4tc_EFFS.IsActive()) EF_j75_j30_a4tc_EFFS();
    if(EF_j75_j30_a4tc_EFFS_L2anymct100.IsActive()) EF_j75_j30_a4tc_EFFS_L2anymct100();
    if(EF_j75_j30_a4tc_EFFS_L2anymct150.IsActive()) EF_j75_j30_a4tc_EFFS_L2anymct150();
    if(EF_j75_j30_a4tc_EFFS_L2anymct175.IsActive()) EF_j75_j30_a4tc_EFFS_L2anymct175();
    if(EF_j75_j30_a4tc_EFFS_L2dphi04.IsActive()) EF_j75_j30_a4tc_EFFS_L2dphi04();
    if(EF_j75_j30_a4tc_EFFS_anymct150.IsActive()) EF_j75_j30_a4tc_EFFS_anymct150();
    if(EF_j75_j30_a4tc_EFFS_anymct175.IsActive()) EF_j75_j30_a4tc_EFFS_anymct175();
    if(EF_j75_j30_a4tc_EFFS_leadingmct150.IsActive()) EF_j75_j30_a4tc_EFFS_leadingmct150();
    if(EF_j80_a4tc_EFFS_xe60_noMu.IsActive()) EF_j80_a4tc_EFFS_xe60_noMu();
    if(EF_je195_NoEF.IsActive()) EF_je195_NoEF();
    if(EF_je255_NoEF.IsActive()) EF_je255_NoEF();
    if(EF_je300_NoEF.IsActive()) EF_je300_NoEF();
    if(EF_je350_NoEF.IsActive()) EF_je350_NoEF();
    if(EF_je420_NoEF.IsActive()) EF_je420_NoEF();
    if(EF_je500_NoEF.IsActive()) EF_je500_NoEF();
    if(n.IsActive()) n();
    if(emscale_E.IsActive()) emscale_E();
    if(emscale_pt.IsActive()) emscale_pt();
    if(emscale_m.IsActive()) emscale_m();
    if(emscale_eta.IsActive()) emscale_eta();
    if(emscale_phi.IsActive()) emscale_phi();
    if(a4.IsActive()) a4();
    if(a4tc.IsActive()) a4tc();
    if(a10tc.IsActive()) a10tc();
    if(a6.IsActive()) a6();
    if(a6tc.IsActive()) a6tc();
    if(RoIword.IsActive()) RoIword();
    if(EF_3j10_a4tc_EFFS.IsActive()) EF_3j10_a4tc_EFFS();
    if(EF_4j10_a4tc_EFFS.IsActive()) EF_4j10_a4tc_EFFS();
    if(EF_5j10_a4tc_EFFS.IsActive()) EF_5j10_a4tc_EFFS();
    if(EF_j15_a2hi_EFFS.IsActive()) EF_j15_a2hi_EFFS();
    if(EF_j15_a2tc_EFFS.IsActive()) EF_j15_a2tc_EFFS();
    if(EF_j20_a2hi_EFFS.IsActive()) EF_j20_a2hi_EFFS();
    if(EF_j20_a2tc_EFFS.IsActive()) EF_j20_a2tc_EFFS();
    if(EF_j20_a4hi_EFFS.IsActive()) EF_j20_a4hi_EFFS();
    if(EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10.IsActive()) EF_j70_a4tc_EFFS_xe70_noMu_dphi2j30xe10();
    if(EF_j75_j45_a4tc_EFFS_xe55_noMu.IsActive()) EF_j75_j45_a4tc_EFFS_xe55_noMu();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TrigEFJetD3PDCollection_CXX
