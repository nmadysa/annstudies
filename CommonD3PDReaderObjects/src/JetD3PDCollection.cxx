// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_JetD3PDCollection_CXX
#define D3PDREADER_JetD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "JetD3PDCollection.h"

ClassImp(D3PDReader::JetD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  JetD3PDCollection::JetD3PDCollection(const long int& master,const std::string& prefix):
    TObject(),
    antikt4truth_n(prefix + "antikt4truth_n",&master),
    antikt4truth_E(prefix + "antikt4truth_E",&master),
    antikt4truth_pt(prefix + "antikt4truth_pt",&master),
    antikt4truth_m(prefix + "antikt4truth_m",&master),
    antikt4truth_eta(prefix + "antikt4truth_eta",&master),
    antikt4truth_phi(prefix + "antikt4truth_phi",&master),
    antikt4truth_EtaOrigin(prefix + "antikt4truth_EtaOrigin",&master),
    antikt4truth_PhiOrigin(prefix + "antikt4truth_PhiOrigin",&master),
    antikt4truth_MOrigin(prefix + "antikt4truth_MOrigin",&master),
    antikt4truth_WIDTH(prefix + "antikt4truth_WIDTH",&master),
    antikt4truth_n90(prefix + "antikt4truth_n90",&master),
    antikt4truth_Timing(prefix + "antikt4truth_Timing",&master),
    antikt4truth_LArQuality(prefix + "antikt4truth_LArQuality",&master),
    antikt4truth_nTrk(prefix + "antikt4truth_nTrk",&master),
    antikt4truth_sumPtTrk(prefix + "antikt4truth_sumPtTrk",&master),
    antikt4truth_OriginIndex(prefix + "antikt4truth_OriginIndex",&master),
    antikt4truth_HECQuality(prefix + "antikt4truth_HECQuality",&master),
    antikt4truth_NegativeE(prefix + "antikt4truth_NegativeE",&master),
    antikt4truth_AverageLArQF(prefix + "antikt4truth_AverageLArQF",&master),
    antikt4truth_BCH_CORR_CELL(prefix + "antikt4truth_BCH_CORR_CELL",&master),
    antikt4truth_BCH_CORR_DOTX(prefix + "antikt4truth_BCH_CORR_DOTX",&master),
    antikt4truth_BCH_CORR_JET(prefix + "antikt4truth_BCH_CORR_JET",&master),
    antikt4truth_BCH_CORR_JET_FORCELL(prefix + "antikt4truth_BCH_CORR_JET_FORCELL",&master),
    antikt4truth_ENG_BAD_CELLS(prefix + "antikt4truth_ENG_BAD_CELLS",&master),
    antikt4truth_N_BAD_CELLS(prefix + "antikt4truth_N_BAD_CELLS",&master),
    antikt4truth_N_BAD_CELLS_CORR(prefix + "antikt4truth_N_BAD_CELLS_CORR",&master),
    antikt4truth_BAD_CELLS_CORR_E(prefix + "antikt4truth_BAD_CELLS_CORR_E",&master),
    antikt4truth_NumTowers(prefix + "antikt4truth_NumTowers",&master),
    antikt4truth_ootFracCells5(prefix + "antikt4truth_ootFracCells5",&master),
    antikt4truth_ootFracCells10(prefix + "antikt4truth_ootFracCells10",&master),
    antikt4truth_ootFracClusters5(prefix + "antikt4truth_ootFracClusters5",&master),
    antikt4truth_ootFracClusters10(prefix + "antikt4truth_ootFracClusters10",&master),
    antikt4truth_SamplingMax(prefix + "antikt4truth_SamplingMax",&master),
    antikt4truth_fracSamplingMax(prefix + "antikt4truth_fracSamplingMax",&master),
    antikt4truth_hecf(prefix + "antikt4truth_hecf",&master),
    antikt4truth_tgap3f(prefix + "antikt4truth_tgap3f",&master),
    antikt4truth_isUgly(prefix + "antikt4truth_isUgly",&master),
    antikt4truth_isBadLooseMinus(prefix + "antikt4truth_isBadLooseMinus",&master),
    antikt4truth_isBadLoose(prefix + "antikt4truth_isBadLoose",&master),
    antikt4truth_isBadMedium(prefix + "antikt4truth_isBadMedium",&master),
    antikt4truth_isBadTight(prefix + "antikt4truth_isBadTight",&master),
    antikt4truth_emfrac(prefix + "antikt4truth_emfrac",&master),
    antikt4truth_Offset(prefix + "antikt4truth_Offset",&master),
    antikt4truth_EMJES(prefix + "antikt4truth_EMJES",&master),
    antikt4truth_EMJES_EtaCorr(prefix + "antikt4truth_EMJES_EtaCorr",&master),
    antikt4truth_EMJESnooffset(prefix + "antikt4truth_EMJESnooffset",&master),
    antikt4truth_LCJES(prefix + "antikt4truth_LCJES",&master),
    antikt4truth_LCJES_EtaCorr(prefix + "antikt4truth_LCJES_EtaCorr",&master),
    antikt4truth_emscale_E(prefix + "antikt4truth_emscale_E",&master),
    antikt4truth_emscale_pt(prefix + "antikt4truth_emscale_pt",&master),
    antikt4truth_emscale_m(prefix + "antikt4truth_emscale_m",&master),
    antikt4truth_emscale_eta(prefix + "antikt4truth_emscale_eta",&master),
    antikt4truth_emscale_phi(prefix + "antikt4truth_emscale_phi",&master),
    antikt4truth_ActiveArea(prefix + "antikt4truth_ActiveArea",&master),
    antikt4truth_ActiveAreaPx(prefix + "antikt4truth_ActiveAreaPx",&master),
    antikt4truth_ActiveAreaPy(prefix + "antikt4truth_ActiveAreaPy",&master),
    antikt4truth_ActiveAreaPz(prefix + "antikt4truth_ActiveAreaPz",&master),
    antikt4truth_ActiveAreaE(prefix + "antikt4truth_ActiveAreaE",&master),
    antikt4truth_jvtxf(prefix + "antikt4truth_jvtxf",&master),
    antikt4truth_jvtxfFull(prefix + "antikt4truth_jvtxfFull",&master),
    antikt4truth_jvtx_x(prefix + "antikt4truth_jvtx_x",&master),
    antikt4truth_jvtx_y(prefix + "antikt4truth_jvtx_y",&master),
    antikt4truth_jvtx_z(prefix + "antikt4truth_jvtx_z",&master),
    antikt4truth_TruthMFindex(prefix + "antikt4truth_TruthMFindex",&master),
    antikt4truth_TruthMF(prefix + "antikt4truth_TruthMF",&master),
    antikt4truth_GSCFactorF(prefix + "antikt4truth_GSCFactorF",&master),
    antikt4truth_WidthFraction(prefix + "antikt4truth_WidthFraction",&master),
    antikt4truth_el_dr(prefix + "antikt4truth_el_dr",&master),
    antikt4truth_el_matched(prefix + "antikt4truth_el_matched",&master),
    antikt4truth_mu_dr(prefix + "antikt4truth_mu_dr",&master),
    antikt4truth_mu_matched(prefix + "antikt4truth_mu_matched",&master),
    antikt4truth_L1_dr(prefix + "antikt4truth_L1_dr",&master),
    antikt4truth_L1_matched(prefix + "antikt4truth_L1_matched",&master),
    antikt4truth_L2_dr(prefix + "antikt4truth_L2_dr",&master),
    antikt4truth_L2_matched(prefix + "antikt4truth_L2_matched",&master),
    antikt4truth_EF_dr(prefix + "antikt4truth_EF_dr",&master),
    antikt4truth_EF_matched(prefix + "antikt4truth_EF_matched",&master),
    antikt4truth_muAssoc_index(prefix + "antikt4truth_muAssoc_index",&master),
    antikt4truth_elAssoc_index(prefix + "antikt4truth_elAssoc_index",&master),
    antikt4truth_tauAssoc_index(prefix + "antikt4truth_tauAssoc_index",&master),
    antikt4truth_truthAssoc_index(prefix + "antikt4truth_truthAssoc_index",&master),
    n(prefix + "n",&master),
    E(prefix + "E",&master),
    pt(prefix + "pt",&master),
    m(prefix + "m",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    EtaOrigin(prefix + "EtaOrigin",&master),
    PhiOrigin(prefix + "PhiOrigin",&master),
    MOrigin(prefix + "MOrigin",&master),
    WIDTH(prefix + "WIDTH",&master),
    n90(prefix + "n90",&master),
    Timing(prefix + "Timing",&master),
    LArQuality(prefix + "LArQuality",&master),
    nTrk(prefix + "nTrk",&master),
    sumPtTrk(prefix + "sumPtTrk",&master),
    OriginIndex(prefix + "OriginIndex",&master),
    HECQuality(prefix + "HECQuality",&master),
    NegativeE(prefix + "NegativeE",&master),
    AverageLArQF(prefix + "AverageLArQF",&master),
    BCH_CORR_CELL(prefix + "BCH_CORR_CELL",&master),
    BCH_CORR_DOTX(prefix + "BCH_CORR_DOTX",&master),
    BCH_CORR_JET(prefix + "BCH_CORR_JET",&master),
    BCH_CORR_JET_FORCELL(prefix + "BCH_CORR_JET_FORCELL",&master),
    ENG_BAD_CELLS(prefix + "ENG_BAD_CELLS",&master),
    N_BAD_CELLS(prefix + "N_BAD_CELLS",&master),
    N_BAD_CELLS_CORR(prefix + "N_BAD_CELLS_CORR",&master),
    BAD_CELLS_CORR_E(prefix + "BAD_CELLS_CORR_E",&master),
    NumTowers(prefix + "NumTowers",&master),
    ootFracCells5(prefix + "ootFracCells5",&master),
    ootFracCells10(prefix + "ootFracCells10",&master),
    ootFracClusters5(prefix + "ootFracClusters5",&master),
    ootFracClusters10(prefix + "ootFracClusters10",&master),
    SamplingMax(prefix + "SamplingMax",&master),
    fracSamplingMax(prefix + "fracSamplingMax",&master),
    hecf(prefix + "hecf",&master),
    tgap3f(prefix + "tgap3f",&master),
    isUgly(prefix + "isUgly",&master),
    isBadLooseMinus(prefix + "isBadLooseMinus",&master),
    isBadLoose(prefix + "isBadLoose",&master),
    isBadMedium(prefix + "isBadMedium",&master),
    isBadTight(prefix + "isBadTight",&master),
    emfrac(prefix + "emfrac",&master),
    Offset(prefix + "Offset",&master),
    EMJES(prefix + "EMJES",&master),
    EMJES_EtaCorr(prefix + "EMJES_EtaCorr",&master),
    EMJESnooffset(prefix + "EMJESnooffset",&master),
    LCJES(prefix + "LCJES",&master),
    LCJES_EtaCorr(prefix + "LCJES_EtaCorr",&master),
    emscale_E(prefix + "emscale_E",&master),
    emscale_pt(prefix + "emscale_pt",&master),
    emscale_m(prefix + "emscale_m",&master),
    emscale_eta(prefix + "emscale_eta",&master),
    emscale_phi(prefix + "emscale_phi",&master),
    ActiveArea(prefix + "ActiveArea",&master),
    ActiveAreaPx(prefix + "ActiveAreaPx",&master),
    ActiveAreaPy(prefix + "ActiveAreaPy",&master),
    ActiveAreaPz(prefix + "ActiveAreaPz",&master),
    ActiveAreaE(prefix + "ActiveAreaE",&master),
    jvtxf(prefix + "jvtxf",&master),
    jvtxfFull(prefix + "jvtxfFull",&master),
    jvtx_x(prefix + "jvtx_x",&master),
    jvtx_y(prefix + "jvtx_y",&master),
    jvtx_z(prefix + "jvtx_z",&master),
    TruthMFindex(prefix + "TruthMFindex",&master),
    TruthMF(prefix + "TruthMF",&master),
    e_PreSamplerB(prefix + "e_PreSamplerB",&master),
    e_EMB1(prefix + "e_EMB1",&master),
    e_EMB2(prefix + "e_EMB2",&master),
    e_EMB3(prefix + "e_EMB3",&master),
    e_PreSamplerE(prefix + "e_PreSamplerE",&master),
    e_EME1(prefix + "e_EME1",&master),
    e_EME2(prefix + "e_EME2",&master),
    e_EME3(prefix + "e_EME3",&master),
    e_HEC0(prefix + "e_HEC0",&master),
    e_HEC1(prefix + "e_HEC1",&master),
    e_HEC2(prefix + "e_HEC2",&master),
    e_HEC3(prefix + "e_HEC3",&master),
    e_TileBar0(prefix + "e_TileBar0",&master),
    e_TileBar1(prefix + "e_TileBar1",&master),
    e_TileBar2(prefix + "e_TileBar2",&master),
    e_TileGap1(prefix + "e_TileGap1",&master),
    e_TileGap2(prefix + "e_TileGap2",&master),
    e_TileGap3(prefix + "e_TileGap3",&master),
    e_TileExt0(prefix + "e_TileExt0",&master),
    e_TileExt1(prefix + "e_TileExt1",&master),
    e_TileExt2(prefix + "e_TileExt2",&master),
    e_FCAL0(prefix + "e_FCAL0",&master),
    e_FCAL1(prefix + "e_FCAL1",&master),
    e_FCAL2(prefix + "e_FCAL2",&master),
    constscale_E(prefix + "constscale_E",&master),
    constscale_pt(prefix + "constscale_pt",&master),
    constscale_m(prefix + "constscale_m",&master),
    constscale_eta(prefix + "constscale_eta",&master),
    constscale_phi(prefix + "constscale_phi",&master),
    flavor_weight_Comb(prefix + "flavor_weight_Comb",&master),
    flavor_weight_IP2D(prefix + "flavor_weight_IP2D",&master),
    flavor_weight_IP3D(prefix + "flavor_weight_IP3D",&master),
    flavor_weight_SV0(prefix + "flavor_weight_SV0",&master),
    flavor_weight_SV1(prefix + "flavor_weight_SV1",&master),
    flavor_weight_SV2(prefix + "flavor_weight_SV2",&master),
    flavor_weight_SoftMuonTagChi2(prefix + "flavor_weight_SoftMuonTagChi2",&master),
    flavor_weight_SecondSoftMuonTagChi2(prefix + "flavor_weight_SecondSoftMuonTagChi2",&master),
    flavor_weight_JetFitterTagNN(prefix + "flavor_weight_JetFitterTagNN",&master),
    flavor_weight_JetFitterCOMBNN(prefix + "flavor_weight_JetFitterCOMBNN",&master),
    flavor_weight_MV1(prefix + "flavor_weight_MV1",&master),
    flavor_weight_MV2(prefix + "flavor_weight_MV2",&master),
    flavor_weight_GbbNN(prefix + "flavor_weight_GbbNN",&master),
    flavor_weight_JetFitterCharm(prefix + "flavor_weight_JetFitterCharm",&master),
    flavor_weight_MV3_bVSu(prefix + "flavor_weight_MV3_bVSu",&master),
    flavor_weight_MV3_bVSc(prefix + "flavor_weight_MV3_bVSc",&master),
    flavor_weight_MV3_cVSu(prefix + "flavor_weight_MV3_cVSu",&master),
    flavor_truth_label(prefix + "flavor_truth_label",&master),
    flavor_truth_dRminToB(prefix + "flavor_truth_dRminToB",&master),
    flavor_truth_dRminToC(prefix + "flavor_truth_dRminToC",&master),
    flavor_truth_dRminToT(prefix + "flavor_truth_dRminToT",&master),
    flavor_truth_BHadronpdg(prefix + "flavor_truth_BHadronpdg",&master),
    flavor_truth_vx_x(prefix + "flavor_truth_vx_x",&master),
    flavor_truth_vx_y(prefix + "flavor_truth_vx_y",&master),
    flavor_truth_vx_z(prefix + "flavor_truth_vx_z",&master),
    flavor_component_jfitc_doublePropName(prefix + "flavor_component_jfitc_doublePropName",&master),
    flavor_component_jfitc_doublePropValue(prefix + "flavor_component_jfitc_doublePropValue",&master),
    flavor_component_jfitc_intPropName(prefix + "flavor_component_jfitc_intPropName",&master),
    flavor_component_jfitc_intPropValue(prefix + "flavor_component_jfitc_intPropValue",&master),
    flavor_component_jfitc_pu(prefix + "flavor_component_jfitc_pu",&master),
    flavor_component_jfitc_pb(prefix + "flavor_component_jfitc_pb",&master),
    flavor_component_jfitc_pc(prefix + "flavor_component_jfitc_pc",&master),
    flavor_component_jfitc_isValid(prefix + "flavor_component_jfitc_isValid",&master),
    el_dr(prefix + "el_dr",&master),
    el_matched(prefix + "el_matched",&master),
    mu_dr(prefix + "mu_dr",&master),
    mu_matched(prefix + "mu_matched",&master),
    L1_dr(prefix + "L1_dr",&master),
    L1_matched(prefix + "L1_matched",&master),
    L2_dr(prefix + "L2_dr",&master),
    L2_matched(prefix + "L2_matched",&master),
    EF_dr(prefix + "EF_dr",&master),
    EF_matched(prefix + "EF_matched",&master),
    nTrk_pv0_1GeV(prefix + "nTrk_pv0_1GeV",&master),
    sumPtTrk_pv0_1GeV(prefix + "sumPtTrk_pv0_1GeV",&master),
    nTrk_allpv_1GeV(prefix + "nTrk_allpv_1GeV",&master),
    sumPtTrk_allpv_1GeV(prefix + "sumPtTrk_allpv_1GeV",&master),
    nTrk_pv0_500MeV(prefix + "nTrk_pv0_500MeV",&master),
    sumPtTrk_pv0_500MeV(prefix + "sumPtTrk_pv0_500MeV",&master),
    trackWIDTH_pv0_1GeV(prefix + "trackWIDTH_pv0_1GeV",&master),
    trackWIDTH_allpv_1GeV(prefix + "trackWIDTH_allpv_1GeV",&master),
    muAssoc_index(prefix + "muAssoc_index",&master),
    elAssoc_index(prefix + "elAssoc_index",&master),
    tauAssoc_index(prefix + "tauAssoc_index",&master),
    truthAssoc_index(prefix + "truthAssoc_index",&master),
    AntiKt4LCTopo_MET_n(prefix + "AntiKt4LCTopo_MET_n",&master),
    AntiKt4LCTopo_MET_wpx(prefix + "AntiKt4LCTopo_MET_wpx",&master),
    AntiKt4LCTopo_MET_wpy(prefix + "AntiKt4LCTopo_MET_wpy",&master),
    AntiKt4LCTopo_MET_wet(prefix + "AntiKt4LCTopo_MET_wet",&master),
    AntiKt4LCTopo_MET_statusWord(prefix + "AntiKt4LCTopo_MET_statusWord",&master),
    AntiKt4LCTopo_MET_BDTMedium_n(prefix + "AntiKt4LCTopo_MET_BDTMedium_n",&master),
    AntiKt4LCTopo_MET_BDTMedium_wpx(prefix + "AntiKt4LCTopo_MET_BDTMedium_wpx",&master),
    AntiKt4LCTopo_MET_BDTMedium_wpy(prefix + "AntiKt4LCTopo_MET_BDTMedium_wpy",&master),
    AntiKt4LCTopo_MET_BDTMedium_wet(prefix + "AntiKt4LCTopo_MET_BDTMedium_wet",&master),
    AntiKt4LCTopo_MET_BDTMedium_statusWord(prefix + "AntiKt4LCTopo_MET_BDTMedium_statusWord",&master),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n",&master),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx",&master),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy",&master),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet",&master),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  JetD3PDCollection::JetD3PDCollection(const std::string& prefix):
    TObject(),
    antikt4truth_n(prefix + "antikt4truth_n",0),
    antikt4truth_E(prefix + "antikt4truth_E",0),
    antikt4truth_pt(prefix + "antikt4truth_pt",0),
    antikt4truth_m(prefix + "antikt4truth_m",0),
    antikt4truth_eta(prefix + "antikt4truth_eta",0),
    antikt4truth_phi(prefix + "antikt4truth_phi",0),
    antikt4truth_EtaOrigin(prefix + "antikt4truth_EtaOrigin",0),
    antikt4truth_PhiOrigin(prefix + "antikt4truth_PhiOrigin",0),
    antikt4truth_MOrigin(prefix + "antikt4truth_MOrigin",0),
    antikt4truth_WIDTH(prefix + "antikt4truth_WIDTH",0),
    antikt4truth_n90(prefix + "antikt4truth_n90",0),
    antikt4truth_Timing(prefix + "antikt4truth_Timing",0),
    antikt4truth_LArQuality(prefix + "antikt4truth_LArQuality",0),
    antikt4truth_nTrk(prefix + "antikt4truth_nTrk",0),
    antikt4truth_sumPtTrk(prefix + "antikt4truth_sumPtTrk",0),
    antikt4truth_OriginIndex(prefix + "antikt4truth_OriginIndex",0),
    antikt4truth_HECQuality(prefix + "antikt4truth_HECQuality",0),
    antikt4truth_NegativeE(prefix + "antikt4truth_NegativeE",0),
    antikt4truth_AverageLArQF(prefix + "antikt4truth_AverageLArQF",0),
    antikt4truth_BCH_CORR_CELL(prefix + "antikt4truth_BCH_CORR_CELL",0),
    antikt4truth_BCH_CORR_DOTX(prefix + "antikt4truth_BCH_CORR_DOTX",0),
    antikt4truth_BCH_CORR_JET(prefix + "antikt4truth_BCH_CORR_JET",0),
    antikt4truth_BCH_CORR_JET_FORCELL(prefix + "antikt4truth_BCH_CORR_JET_FORCELL",0),
    antikt4truth_ENG_BAD_CELLS(prefix + "antikt4truth_ENG_BAD_CELLS",0),
    antikt4truth_N_BAD_CELLS(prefix + "antikt4truth_N_BAD_CELLS",0),
    antikt4truth_N_BAD_CELLS_CORR(prefix + "antikt4truth_N_BAD_CELLS_CORR",0),
    antikt4truth_BAD_CELLS_CORR_E(prefix + "antikt4truth_BAD_CELLS_CORR_E",0),
    antikt4truth_NumTowers(prefix + "antikt4truth_NumTowers",0),
    antikt4truth_ootFracCells5(prefix + "antikt4truth_ootFracCells5",0),
    antikt4truth_ootFracCells10(prefix + "antikt4truth_ootFracCells10",0),
    antikt4truth_ootFracClusters5(prefix + "antikt4truth_ootFracClusters5",0),
    antikt4truth_ootFracClusters10(prefix + "antikt4truth_ootFracClusters10",0),
    antikt4truth_SamplingMax(prefix + "antikt4truth_SamplingMax",0),
    antikt4truth_fracSamplingMax(prefix + "antikt4truth_fracSamplingMax",0),
    antikt4truth_hecf(prefix + "antikt4truth_hecf",0),
    antikt4truth_tgap3f(prefix + "antikt4truth_tgap3f",0),
    antikt4truth_isUgly(prefix + "antikt4truth_isUgly",0),
    antikt4truth_isBadLooseMinus(prefix + "antikt4truth_isBadLooseMinus",0),
    antikt4truth_isBadLoose(prefix + "antikt4truth_isBadLoose",0),
    antikt4truth_isBadMedium(prefix + "antikt4truth_isBadMedium",0),
    antikt4truth_isBadTight(prefix + "antikt4truth_isBadTight",0),
    antikt4truth_emfrac(prefix + "antikt4truth_emfrac",0),
    antikt4truth_Offset(prefix + "antikt4truth_Offset",0),
    antikt4truth_EMJES(prefix + "antikt4truth_EMJES",0),
    antikt4truth_EMJES_EtaCorr(prefix + "antikt4truth_EMJES_EtaCorr",0),
    antikt4truth_EMJESnooffset(prefix + "antikt4truth_EMJESnooffset",0),
    antikt4truth_LCJES(prefix + "antikt4truth_LCJES",0),
    antikt4truth_LCJES_EtaCorr(prefix + "antikt4truth_LCJES_EtaCorr",0),
    antikt4truth_emscale_E(prefix + "antikt4truth_emscale_E",0),
    antikt4truth_emscale_pt(prefix + "antikt4truth_emscale_pt",0),
    antikt4truth_emscale_m(prefix + "antikt4truth_emscale_m",0),
    antikt4truth_emscale_eta(prefix + "antikt4truth_emscale_eta",0),
    antikt4truth_emscale_phi(prefix + "antikt4truth_emscale_phi",0),
    antikt4truth_ActiveArea(prefix + "antikt4truth_ActiveArea",0),
    antikt4truth_ActiveAreaPx(prefix + "antikt4truth_ActiveAreaPx",0),
    antikt4truth_ActiveAreaPy(prefix + "antikt4truth_ActiveAreaPy",0),
    antikt4truth_ActiveAreaPz(prefix + "antikt4truth_ActiveAreaPz",0),
    antikt4truth_ActiveAreaE(prefix + "antikt4truth_ActiveAreaE",0),
    antikt4truth_jvtxf(prefix + "antikt4truth_jvtxf",0),
    antikt4truth_jvtxfFull(prefix + "antikt4truth_jvtxfFull",0),
    antikt4truth_jvtx_x(prefix + "antikt4truth_jvtx_x",0),
    antikt4truth_jvtx_y(prefix + "antikt4truth_jvtx_y",0),
    antikt4truth_jvtx_z(prefix + "antikt4truth_jvtx_z",0),
    antikt4truth_TruthMFindex(prefix + "antikt4truth_TruthMFindex",0),
    antikt4truth_TruthMF(prefix + "antikt4truth_TruthMF",0),
    antikt4truth_GSCFactorF(prefix + "antikt4truth_GSCFactorF",0),
    antikt4truth_WidthFraction(prefix + "antikt4truth_WidthFraction",0),
    antikt4truth_el_dr(prefix + "antikt4truth_el_dr",0),
    antikt4truth_el_matched(prefix + "antikt4truth_el_matched",0),
    antikt4truth_mu_dr(prefix + "antikt4truth_mu_dr",0),
    antikt4truth_mu_matched(prefix + "antikt4truth_mu_matched",0),
    antikt4truth_L1_dr(prefix + "antikt4truth_L1_dr",0),
    antikt4truth_L1_matched(prefix + "antikt4truth_L1_matched",0),
    antikt4truth_L2_dr(prefix + "antikt4truth_L2_dr",0),
    antikt4truth_L2_matched(prefix + "antikt4truth_L2_matched",0),
    antikt4truth_EF_dr(prefix + "antikt4truth_EF_dr",0),
    antikt4truth_EF_matched(prefix + "antikt4truth_EF_matched",0),
    antikt4truth_muAssoc_index(prefix + "antikt4truth_muAssoc_index",0),
    antikt4truth_elAssoc_index(prefix + "antikt4truth_elAssoc_index",0),
    antikt4truth_tauAssoc_index(prefix + "antikt4truth_tauAssoc_index",0),
    antikt4truth_truthAssoc_index(prefix + "antikt4truth_truthAssoc_index",0),
    n(prefix + "n",0),
    E(prefix + "E",0),
    pt(prefix + "pt",0),
    m(prefix + "m",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    EtaOrigin(prefix + "EtaOrigin",0),
    PhiOrigin(prefix + "PhiOrigin",0),
    MOrigin(prefix + "MOrigin",0),
    WIDTH(prefix + "WIDTH",0),
    n90(prefix + "n90",0),
    Timing(prefix + "Timing",0),
    LArQuality(prefix + "LArQuality",0),
    nTrk(prefix + "nTrk",0),
    sumPtTrk(prefix + "sumPtTrk",0),
    OriginIndex(prefix + "OriginIndex",0),
    HECQuality(prefix + "HECQuality",0),
    NegativeE(prefix + "NegativeE",0),
    AverageLArQF(prefix + "AverageLArQF",0),
    BCH_CORR_CELL(prefix + "BCH_CORR_CELL",0),
    BCH_CORR_DOTX(prefix + "BCH_CORR_DOTX",0),
    BCH_CORR_JET(prefix + "BCH_CORR_JET",0),
    BCH_CORR_JET_FORCELL(prefix + "BCH_CORR_JET_FORCELL",0),
    ENG_BAD_CELLS(prefix + "ENG_BAD_CELLS",0),
    N_BAD_CELLS(prefix + "N_BAD_CELLS",0),
    N_BAD_CELLS_CORR(prefix + "N_BAD_CELLS_CORR",0),
    BAD_CELLS_CORR_E(prefix + "BAD_CELLS_CORR_E",0),
    NumTowers(prefix + "NumTowers",0),
    ootFracCells5(prefix + "ootFracCells5",0),
    ootFracCells10(prefix + "ootFracCells10",0),
    ootFracClusters5(prefix + "ootFracClusters5",0),
    ootFracClusters10(prefix + "ootFracClusters10",0),
    SamplingMax(prefix + "SamplingMax",0),
    fracSamplingMax(prefix + "fracSamplingMax",0),
    hecf(prefix + "hecf",0),
    tgap3f(prefix + "tgap3f",0),
    isUgly(prefix + "isUgly",0),
    isBadLooseMinus(prefix + "isBadLooseMinus",0),
    isBadLoose(prefix + "isBadLoose",0),
    isBadMedium(prefix + "isBadMedium",0),
    isBadTight(prefix + "isBadTight",0),
    emfrac(prefix + "emfrac",0),
    Offset(prefix + "Offset",0),
    EMJES(prefix + "EMJES",0),
    EMJES_EtaCorr(prefix + "EMJES_EtaCorr",0),
    EMJESnooffset(prefix + "EMJESnooffset",0),
    LCJES(prefix + "LCJES",0),
    LCJES_EtaCorr(prefix + "LCJES_EtaCorr",0),
    emscale_E(prefix + "emscale_E",0),
    emscale_pt(prefix + "emscale_pt",0),
    emscale_m(prefix + "emscale_m",0),
    emscale_eta(prefix + "emscale_eta",0),
    emscale_phi(prefix + "emscale_phi",0),
    ActiveArea(prefix + "ActiveArea",0),
    ActiveAreaPx(prefix + "ActiveAreaPx",0),
    ActiveAreaPy(prefix + "ActiveAreaPy",0),
    ActiveAreaPz(prefix + "ActiveAreaPz",0),
    ActiveAreaE(prefix + "ActiveAreaE",0),
    jvtxf(prefix + "jvtxf",0),
    jvtxfFull(prefix + "jvtxfFull",0),
    jvtx_x(prefix + "jvtx_x",0),
    jvtx_y(prefix + "jvtx_y",0),
    jvtx_z(prefix + "jvtx_z",0),
    TruthMFindex(prefix + "TruthMFindex",0),
    TruthMF(prefix + "TruthMF",0),
    e_PreSamplerB(prefix + "e_PreSamplerB",0),
    e_EMB1(prefix + "e_EMB1",0),
    e_EMB2(prefix + "e_EMB2",0),
    e_EMB3(prefix + "e_EMB3",0),
    e_PreSamplerE(prefix + "e_PreSamplerE",0),
    e_EME1(prefix + "e_EME1",0),
    e_EME2(prefix + "e_EME2",0),
    e_EME3(prefix + "e_EME3",0),
    e_HEC0(prefix + "e_HEC0",0),
    e_HEC1(prefix + "e_HEC1",0),
    e_HEC2(prefix + "e_HEC2",0),
    e_HEC3(prefix + "e_HEC3",0),
    e_TileBar0(prefix + "e_TileBar0",0),
    e_TileBar1(prefix + "e_TileBar1",0),
    e_TileBar2(prefix + "e_TileBar2",0),
    e_TileGap1(prefix + "e_TileGap1",0),
    e_TileGap2(prefix + "e_TileGap2",0),
    e_TileGap3(prefix + "e_TileGap3",0),
    e_TileExt0(prefix + "e_TileExt0",0),
    e_TileExt1(prefix + "e_TileExt1",0),
    e_TileExt2(prefix + "e_TileExt2",0),
    e_FCAL0(prefix + "e_FCAL0",0),
    e_FCAL1(prefix + "e_FCAL1",0),
    e_FCAL2(prefix + "e_FCAL2",0),
    constscale_E(prefix + "constscale_E",0),
    constscale_pt(prefix + "constscale_pt",0),
    constscale_m(prefix + "constscale_m",0),
    constscale_eta(prefix + "constscale_eta",0),
    constscale_phi(prefix + "constscale_phi",0),
    flavor_weight_Comb(prefix + "flavor_weight_Comb",0),
    flavor_weight_IP2D(prefix + "flavor_weight_IP2D",0),
    flavor_weight_IP3D(prefix + "flavor_weight_IP3D",0),
    flavor_weight_SV0(prefix + "flavor_weight_SV0",0),
    flavor_weight_SV1(prefix + "flavor_weight_SV1",0),
    flavor_weight_SV2(prefix + "flavor_weight_SV2",0),
    flavor_weight_SoftMuonTagChi2(prefix + "flavor_weight_SoftMuonTagChi2",0),
    flavor_weight_SecondSoftMuonTagChi2(prefix + "flavor_weight_SecondSoftMuonTagChi2",0),
    flavor_weight_JetFitterTagNN(prefix + "flavor_weight_JetFitterTagNN",0),
    flavor_weight_JetFitterCOMBNN(prefix + "flavor_weight_JetFitterCOMBNN",0),
    flavor_weight_MV1(prefix + "flavor_weight_MV1",0),
    flavor_weight_MV2(prefix + "flavor_weight_MV2",0),
    flavor_weight_GbbNN(prefix + "flavor_weight_GbbNN",0),
    flavor_weight_JetFitterCharm(prefix + "flavor_weight_JetFitterCharm",0),
    flavor_weight_MV3_bVSu(prefix + "flavor_weight_MV3_bVSu",0),
    flavor_weight_MV3_bVSc(prefix + "flavor_weight_MV3_bVSc",0),
    flavor_weight_MV3_cVSu(prefix + "flavor_weight_MV3_cVSu",0),
    flavor_truth_label(prefix + "flavor_truth_label",0),
    flavor_truth_dRminToB(prefix + "flavor_truth_dRminToB",0),
    flavor_truth_dRminToC(prefix + "flavor_truth_dRminToC",0),
    flavor_truth_dRminToT(prefix + "flavor_truth_dRminToT",0),
    flavor_truth_BHadronpdg(prefix + "flavor_truth_BHadronpdg",0),
    flavor_truth_vx_x(prefix + "flavor_truth_vx_x",0),
    flavor_truth_vx_y(prefix + "flavor_truth_vx_y",0),
    flavor_truth_vx_z(prefix + "flavor_truth_vx_z",0),
    flavor_component_jfitc_doublePropName(prefix + "flavor_component_jfitc_doublePropName",0),
    flavor_component_jfitc_doublePropValue(prefix + "flavor_component_jfitc_doublePropValue",0),
    flavor_component_jfitc_intPropName(prefix + "flavor_component_jfitc_intPropName",0),
    flavor_component_jfitc_intPropValue(prefix + "flavor_component_jfitc_intPropValue",0),
    flavor_component_jfitc_pu(prefix + "flavor_component_jfitc_pu",0),
    flavor_component_jfitc_pb(prefix + "flavor_component_jfitc_pb",0),
    flavor_component_jfitc_pc(prefix + "flavor_component_jfitc_pc",0),
    flavor_component_jfitc_isValid(prefix + "flavor_component_jfitc_isValid",0),
    el_dr(prefix + "el_dr",0),
    el_matched(prefix + "el_matched",0),
    mu_dr(prefix + "mu_dr",0),
    mu_matched(prefix + "mu_matched",0),
    L1_dr(prefix + "L1_dr",0),
    L1_matched(prefix + "L1_matched",0),
    L2_dr(prefix + "L2_dr",0),
    L2_matched(prefix + "L2_matched",0),
    EF_dr(prefix + "EF_dr",0),
    EF_matched(prefix + "EF_matched",0),
    nTrk_pv0_1GeV(prefix + "nTrk_pv0_1GeV",0),
    sumPtTrk_pv0_1GeV(prefix + "sumPtTrk_pv0_1GeV",0),
    nTrk_allpv_1GeV(prefix + "nTrk_allpv_1GeV",0),
    sumPtTrk_allpv_1GeV(prefix + "sumPtTrk_allpv_1GeV",0),
    nTrk_pv0_500MeV(prefix + "nTrk_pv0_500MeV",0),
    sumPtTrk_pv0_500MeV(prefix + "sumPtTrk_pv0_500MeV",0),
    trackWIDTH_pv0_1GeV(prefix + "trackWIDTH_pv0_1GeV",0),
    trackWIDTH_allpv_1GeV(prefix + "trackWIDTH_allpv_1GeV",0),
    muAssoc_index(prefix + "muAssoc_index",0),
    elAssoc_index(prefix + "elAssoc_index",0),
    tauAssoc_index(prefix + "tauAssoc_index",0),
    truthAssoc_index(prefix + "truthAssoc_index",0),
    AntiKt4LCTopo_MET_n(prefix + "AntiKt4LCTopo_MET_n",0),
    AntiKt4LCTopo_MET_wpx(prefix + "AntiKt4LCTopo_MET_wpx",0),
    AntiKt4LCTopo_MET_wpy(prefix + "AntiKt4LCTopo_MET_wpy",0),
    AntiKt4LCTopo_MET_wet(prefix + "AntiKt4LCTopo_MET_wet",0),
    AntiKt4LCTopo_MET_statusWord(prefix + "AntiKt4LCTopo_MET_statusWord",0),
    AntiKt4LCTopo_MET_BDTMedium_n(prefix + "AntiKt4LCTopo_MET_BDTMedium_n",0),
    AntiKt4LCTopo_MET_BDTMedium_wpx(prefix + "AntiKt4LCTopo_MET_BDTMedium_wpx",0),
    AntiKt4LCTopo_MET_BDTMedium_wpy(prefix + "AntiKt4LCTopo_MET_BDTMedium_wpy",0),
    AntiKt4LCTopo_MET_BDTMedium_wet(prefix + "AntiKt4LCTopo_MET_BDTMedium_wet",0),
    AntiKt4LCTopo_MET_BDTMedium_statusWord(prefix + "AntiKt4LCTopo_MET_BDTMedium_statusWord",0),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n",0),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx",0),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy",0),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet",0),
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord(prefix + "AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void JetD3PDCollection::ReadFrom(TTree* tree)
  {
    antikt4truth_n.ReadFrom(tree);
    antikt4truth_E.ReadFrom(tree);
    antikt4truth_pt.ReadFrom(tree);
    antikt4truth_m.ReadFrom(tree);
    antikt4truth_eta.ReadFrom(tree);
    antikt4truth_phi.ReadFrom(tree);
    antikt4truth_EtaOrigin.ReadFrom(tree);
    antikt4truth_PhiOrigin.ReadFrom(tree);
    antikt4truth_MOrigin.ReadFrom(tree);
    antikt4truth_WIDTH.ReadFrom(tree);
    antikt4truth_n90.ReadFrom(tree);
    antikt4truth_Timing.ReadFrom(tree);
    antikt4truth_LArQuality.ReadFrom(tree);
    antikt4truth_nTrk.ReadFrom(tree);
    antikt4truth_sumPtTrk.ReadFrom(tree);
    antikt4truth_OriginIndex.ReadFrom(tree);
    antikt4truth_HECQuality.ReadFrom(tree);
    antikt4truth_NegativeE.ReadFrom(tree);
    antikt4truth_AverageLArQF.ReadFrom(tree);
    antikt4truth_BCH_CORR_CELL.ReadFrom(tree);
    antikt4truth_BCH_CORR_DOTX.ReadFrom(tree);
    antikt4truth_BCH_CORR_JET.ReadFrom(tree);
    antikt4truth_BCH_CORR_JET_FORCELL.ReadFrom(tree);
    antikt4truth_ENG_BAD_CELLS.ReadFrom(tree);
    antikt4truth_N_BAD_CELLS.ReadFrom(tree);
    antikt4truth_N_BAD_CELLS_CORR.ReadFrom(tree);
    antikt4truth_BAD_CELLS_CORR_E.ReadFrom(tree);
    antikt4truth_NumTowers.ReadFrom(tree);
    antikt4truth_ootFracCells5.ReadFrom(tree);
    antikt4truth_ootFracCells10.ReadFrom(tree);
    antikt4truth_ootFracClusters5.ReadFrom(tree);
    antikt4truth_ootFracClusters10.ReadFrom(tree);
    antikt4truth_SamplingMax.ReadFrom(tree);
    antikt4truth_fracSamplingMax.ReadFrom(tree);
    antikt4truth_hecf.ReadFrom(tree);
    antikt4truth_tgap3f.ReadFrom(tree);
    antikt4truth_isUgly.ReadFrom(tree);
    antikt4truth_isBadLooseMinus.ReadFrom(tree);
    antikt4truth_isBadLoose.ReadFrom(tree);
    antikt4truth_isBadMedium.ReadFrom(tree);
    antikt4truth_isBadTight.ReadFrom(tree);
    antikt4truth_emfrac.ReadFrom(tree);
    antikt4truth_Offset.ReadFrom(tree);
    antikt4truth_EMJES.ReadFrom(tree);
    antikt4truth_EMJES_EtaCorr.ReadFrom(tree);
    antikt4truth_EMJESnooffset.ReadFrom(tree);
    antikt4truth_LCJES.ReadFrom(tree);
    antikt4truth_LCJES_EtaCorr.ReadFrom(tree);
    antikt4truth_emscale_E.ReadFrom(tree);
    antikt4truth_emscale_pt.ReadFrom(tree);
    antikt4truth_emscale_m.ReadFrom(tree);
    antikt4truth_emscale_eta.ReadFrom(tree);
    antikt4truth_emscale_phi.ReadFrom(tree);
    antikt4truth_ActiveArea.ReadFrom(tree);
    antikt4truth_ActiveAreaPx.ReadFrom(tree);
    antikt4truth_ActiveAreaPy.ReadFrom(tree);
    antikt4truth_ActiveAreaPz.ReadFrom(tree);
    antikt4truth_ActiveAreaE.ReadFrom(tree);
    antikt4truth_jvtxf.ReadFrom(tree);
    antikt4truth_jvtxfFull.ReadFrom(tree);
    antikt4truth_jvtx_x.ReadFrom(tree);
    antikt4truth_jvtx_y.ReadFrom(tree);
    antikt4truth_jvtx_z.ReadFrom(tree);
    antikt4truth_TruthMFindex.ReadFrom(tree);
    antikt4truth_TruthMF.ReadFrom(tree);
    antikt4truth_GSCFactorF.ReadFrom(tree);
    antikt4truth_WidthFraction.ReadFrom(tree);
    antikt4truth_el_dr.ReadFrom(tree);
    antikt4truth_el_matched.ReadFrom(tree);
    antikt4truth_mu_dr.ReadFrom(tree);
    antikt4truth_mu_matched.ReadFrom(tree);
    antikt4truth_L1_dr.ReadFrom(tree);
    antikt4truth_L1_matched.ReadFrom(tree);
    antikt4truth_L2_dr.ReadFrom(tree);
    antikt4truth_L2_matched.ReadFrom(tree);
    antikt4truth_EF_dr.ReadFrom(tree);
    antikt4truth_EF_matched.ReadFrom(tree);
    antikt4truth_muAssoc_index.ReadFrom(tree);
    antikt4truth_elAssoc_index.ReadFrom(tree);
    antikt4truth_tauAssoc_index.ReadFrom(tree);
    antikt4truth_truthAssoc_index.ReadFrom(tree);
    n.ReadFrom(tree);
    E.ReadFrom(tree);
    pt.ReadFrom(tree);
    m.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    EtaOrigin.ReadFrom(tree);
    PhiOrigin.ReadFrom(tree);
    MOrigin.ReadFrom(tree);
    WIDTH.ReadFrom(tree);
    n90.ReadFrom(tree);
    Timing.ReadFrom(tree);
    LArQuality.ReadFrom(tree);
    nTrk.ReadFrom(tree);
    sumPtTrk.ReadFrom(tree);
    OriginIndex.ReadFrom(tree);
    HECQuality.ReadFrom(tree);
    NegativeE.ReadFrom(tree);
    AverageLArQF.ReadFrom(tree);
    BCH_CORR_CELL.ReadFrom(tree);
    BCH_CORR_DOTX.ReadFrom(tree);
    BCH_CORR_JET.ReadFrom(tree);
    BCH_CORR_JET_FORCELL.ReadFrom(tree);
    ENG_BAD_CELLS.ReadFrom(tree);
    N_BAD_CELLS.ReadFrom(tree);
    N_BAD_CELLS_CORR.ReadFrom(tree);
    BAD_CELLS_CORR_E.ReadFrom(tree);
    NumTowers.ReadFrom(tree);
    ootFracCells5.ReadFrom(tree);
    ootFracCells10.ReadFrom(tree);
    ootFracClusters5.ReadFrom(tree);
    ootFracClusters10.ReadFrom(tree);
    SamplingMax.ReadFrom(tree);
    fracSamplingMax.ReadFrom(tree);
    hecf.ReadFrom(tree);
    tgap3f.ReadFrom(tree);
    isUgly.ReadFrom(tree);
    isBadLooseMinus.ReadFrom(tree);
    isBadLoose.ReadFrom(tree);
    isBadMedium.ReadFrom(tree);
    isBadTight.ReadFrom(tree);
    emfrac.ReadFrom(tree);
    Offset.ReadFrom(tree);
    EMJES.ReadFrom(tree);
    EMJES_EtaCorr.ReadFrom(tree);
    EMJESnooffset.ReadFrom(tree);
    LCJES.ReadFrom(tree);
    LCJES_EtaCorr.ReadFrom(tree);
    emscale_E.ReadFrom(tree);
    emscale_pt.ReadFrom(tree);
    emscale_m.ReadFrom(tree);
    emscale_eta.ReadFrom(tree);
    emscale_phi.ReadFrom(tree);
    ActiveArea.ReadFrom(tree);
    ActiveAreaPx.ReadFrom(tree);
    ActiveAreaPy.ReadFrom(tree);
    ActiveAreaPz.ReadFrom(tree);
    ActiveAreaE.ReadFrom(tree);
    jvtxf.ReadFrom(tree);
    jvtxfFull.ReadFrom(tree);
    jvtx_x.ReadFrom(tree);
    jvtx_y.ReadFrom(tree);
    jvtx_z.ReadFrom(tree);
    TruthMFindex.ReadFrom(tree);
    TruthMF.ReadFrom(tree);
    e_PreSamplerB.ReadFrom(tree);
    e_EMB1.ReadFrom(tree);
    e_EMB2.ReadFrom(tree);
    e_EMB3.ReadFrom(tree);
    e_PreSamplerE.ReadFrom(tree);
    e_EME1.ReadFrom(tree);
    e_EME2.ReadFrom(tree);
    e_EME3.ReadFrom(tree);
    e_HEC0.ReadFrom(tree);
    e_HEC1.ReadFrom(tree);
    e_HEC2.ReadFrom(tree);
    e_HEC3.ReadFrom(tree);
    e_TileBar0.ReadFrom(tree);
    e_TileBar1.ReadFrom(tree);
    e_TileBar2.ReadFrom(tree);
    e_TileGap1.ReadFrom(tree);
    e_TileGap2.ReadFrom(tree);
    e_TileGap3.ReadFrom(tree);
    e_TileExt0.ReadFrom(tree);
    e_TileExt1.ReadFrom(tree);
    e_TileExt2.ReadFrom(tree);
    e_FCAL0.ReadFrom(tree);
    e_FCAL1.ReadFrom(tree);
    e_FCAL2.ReadFrom(tree);
    constscale_E.ReadFrom(tree);
    constscale_pt.ReadFrom(tree);
    constscale_m.ReadFrom(tree);
    constscale_eta.ReadFrom(tree);
    constscale_phi.ReadFrom(tree);
    flavor_weight_Comb.ReadFrom(tree);
    flavor_weight_IP2D.ReadFrom(tree);
    flavor_weight_IP3D.ReadFrom(tree);
    flavor_weight_SV0.ReadFrom(tree);
    flavor_weight_SV1.ReadFrom(tree);
    flavor_weight_SV2.ReadFrom(tree);
    flavor_weight_SoftMuonTagChi2.ReadFrom(tree);
    flavor_weight_SecondSoftMuonTagChi2.ReadFrom(tree);
    flavor_weight_JetFitterTagNN.ReadFrom(tree);
    flavor_weight_JetFitterCOMBNN.ReadFrom(tree);
    flavor_weight_MV1.ReadFrom(tree);
    flavor_weight_MV2.ReadFrom(tree);
    flavor_weight_GbbNN.ReadFrom(tree);
    flavor_weight_JetFitterCharm.ReadFrom(tree);
    flavor_weight_MV3_bVSu.ReadFrom(tree);
    flavor_weight_MV3_bVSc.ReadFrom(tree);
    flavor_weight_MV3_cVSu.ReadFrom(tree);
    flavor_truth_label.ReadFrom(tree);
    flavor_truth_dRminToB.ReadFrom(tree);
    flavor_truth_dRminToC.ReadFrom(tree);
    flavor_truth_dRminToT.ReadFrom(tree);
    flavor_truth_BHadronpdg.ReadFrom(tree);
    flavor_truth_vx_x.ReadFrom(tree);
    flavor_truth_vx_y.ReadFrom(tree);
    flavor_truth_vx_z.ReadFrom(tree);
    flavor_component_jfitc_doublePropName.ReadFrom(tree);
    flavor_component_jfitc_doublePropValue.ReadFrom(tree);
    flavor_component_jfitc_intPropName.ReadFrom(tree);
    flavor_component_jfitc_intPropValue.ReadFrom(tree);
    flavor_component_jfitc_pu.ReadFrom(tree);
    flavor_component_jfitc_pb.ReadFrom(tree);
    flavor_component_jfitc_pc.ReadFrom(tree);
    flavor_component_jfitc_isValid.ReadFrom(tree);
    el_dr.ReadFrom(tree);
    el_matched.ReadFrom(tree);
    mu_dr.ReadFrom(tree);
    mu_matched.ReadFrom(tree);
    L1_dr.ReadFrom(tree);
    L1_matched.ReadFrom(tree);
    L2_dr.ReadFrom(tree);
    L2_matched.ReadFrom(tree);
    EF_dr.ReadFrom(tree);
    EF_matched.ReadFrom(tree);
    nTrk_pv0_1GeV.ReadFrom(tree);
    sumPtTrk_pv0_1GeV.ReadFrom(tree);
    nTrk_allpv_1GeV.ReadFrom(tree);
    sumPtTrk_allpv_1GeV.ReadFrom(tree);
    nTrk_pv0_500MeV.ReadFrom(tree);
    sumPtTrk_pv0_500MeV.ReadFrom(tree);
    trackWIDTH_pv0_1GeV.ReadFrom(tree);
    trackWIDTH_allpv_1GeV.ReadFrom(tree);
    muAssoc_index.ReadFrom(tree);
    elAssoc_index.ReadFrom(tree);
    tauAssoc_index.ReadFrom(tree);
    truthAssoc_index.ReadFrom(tree);
    AntiKt4LCTopo_MET_n.ReadFrom(tree);
    AntiKt4LCTopo_MET_wpx.ReadFrom(tree);
    AntiKt4LCTopo_MET_wpy.ReadFrom(tree);
    AntiKt4LCTopo_MET_wet.ReadFrom(tree);
    AntiKt4LCTopo_MET_statusWord.ReadFrom(tree);
    AntiKt4LCTopo_MET_BDTMedium_n.ReadFrom(tree);
    AntiKt4LCTopo_MET_BDTMedium_wpx.ReadFrom(tree);
    AntiKt4LCTopo_MET_BDTMedium_wpy.ReadFrom(tree);
    AntiKt4LCTopo_MET_BDTMedium_wet.ReadFrom(tree);
    AntiKt4LCTopo_MET_BDTMedium_statusWord.ReadFrom(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n.ReadFrom(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx.ReadFrom(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy.ReadFrom(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet.ReadFrom(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void JetD3PDCollection::WriteTo(TTree* tree)
  {
    antikt4truth_n.WriteTo(tree);
    antikt4truth_E.WriteTo(tree);
    antikt4truth_pt.WriteTo(tree);
    antikt4truth_m.WriteTo(tree);
    antikt4truth_eta.WriteTo(tree);
    antikt4truth_phi.WriteTo(tree);
    antikt4truth_EtaOrigin.WriteTo(tree);
    antikt4truth_PhiOrigin.WriteTo(tree);
    antikt4truth_MOrigin.WriteTo(tree);
    antikt4truth_WIDTH.WriteTo(tree);
    antikt4truth_n90.WriteTo(tree);
    antikt4truth_Timing.WriteTo(tree);
    antikt4truth_LArQuality.WriteTo(tree);
    antikt4truth_nTrk.WriteTo(tree);
    antikt4truth_sumPtTrk.WriteTo(tree);
    antikt4truth_OriginIndex.WriteTo(tree);
    antikt4truth_HECQuality.WriteTo(tree);
    antikt4truth_NegativeE.WriteTo(tree);
    antikt4truth_AverageLArQF.WriteTo(tree);
    antikt4truth_BCH_CORR_CELL.WriteTo(tree);
    antikt4truth_BCH_CORR_DOTX.WriteTo(tree);
    antikt4truth_BCH_CORR_JET.WriteTo(tree);
    antikt4truth_BCH_CORR_JET_FORCELL.WriteTo(tree);
    antikt4truth_ENG_BAD_CELLS.WriteTo(tree);
    antikt4truth_N_BAD_CELLS.WriteTo(tree);
    antikt4truth_N_BAD_CELLS_CORR.WriteTo(tree);
    antikt4truth_BAD_CELLS_CORR_E.WriteTo(tree);
    antikt4truth_NumTowers.WriteTo(tree);
    antikt4truth_ootFracCells5.WriteTo(tree);
    antikt4truth_ootFracCells10.WriteTo(tree);
    antikt4truth_ootFracClusters5.WriteTo(tree);
    antikt4truth_ootFracClusters10.WriteTo(tree);
    antikt4truth_SamplingMax.WriteTo(tree);
    antikt4truth_fracSamplingMax.WriteTo(tree);
    antikt4truth_hecf.WriteTo(tree);
    antikt4truth_tgap3f.WriteTo(tree);
    antikt4truth_isUgly.WriteTo(tree);
    antikt4truth_isBadLooseMinus.WriteTo(tree);
    antikt4truth_isBadLoose.WriteTo(tree);
    antikt4truth_isBadMedium.WriteTo(tree);
    antikt4truth_isBadTight.WriteTo(tree);
    antikt4truth_emfrac.WriteTo(tree);
    antikt4truth_Offset.WriteTo(tree);
    antikt4truth_EMJES.WriteTo(tree);
    antikt4truth_EMJES_EtaCorr.WriteTo(tree);
    antikt4truth_EMJESnooffset.WriteTo(tree);
    antikt4truth_LCJES.WriteTo(tree);
    antikt4truth_LCJES_EtaCorr.WriteTo(tree);
    antikt4truth_emscale_E.WriteTo(tree);
    antikt4truth_emscale_pt.WriteTo(tree);
    antikt4truth_emscale_m.WriteTo(tree);
    antikt4truth_emscale_eta.WriteTo(tree);
    antikt4truth_emscale_phi.WriteTo(tree);
    antikt4truth_ActiveArea.WriteTo(tree);
    antikt4truth_ActiveAreaPx.WriteTo(tree);
    antikt4truth_ActiveAreaPy.WriteTo(tree);
    antikt4truth_ActiveAreaPz.WriteTo(tree);
    antikt4truth_ActiveAreaE.WriteTo(tree);
    antikt4truth_jvtxf.WriteTo(tree);
    antikt4truth_jvtxfFull.WriteTo(tree);
    antikt4truth_jvtx_x.WriteTo(tree);
    antikt4truth_jvtx_y.WriteTo(tree);
    antikt4truth_jvtx_z.WriteTo(tree);
    antikt4truth_TruthMFindex.WriteTo(tree);
    antikt4truth_TruthMF.WriteTo(tree);
    antikt4truth_GSCFactorF.WriteTo(tree);
    antikt4truth_WidthFraction.WriteTo(tree);
    antikt4truth_el_dr.WriteTo(tree);
    antikt4truth_el_matched.WriteTo(tree);
    antikt4truth_mu_dr.WriteTo(tree);
    antikt4truth_mu_matched.WriteTo(tree);
    antikt4truth_L1_dr.WriteTo(tree);
    antikt4truth_L1_matched.WriteTo(tree);
    antikt4truth_L2_dr.WriteTo(tree);
    antikt4truth_L2_matched.WriteTo(tree);
    antikt4truth_EF_dr.WriteTo(tree);
    antikt4truth_EF_matched.WriteTo(tree);
    antikt4truth_muAssoc_index.WriteTo(tree);
    antikt4truth_elAssoc_index.WriteTo(tree);
    antikt4truth_tauAssoc_index.WriteTo(tree);
    antikt4truth_truthAssoc_index.WriteTo(tree);
    n.WriteTo(tree);
    E.WriteTo(tree);
    pt.WriteTo(tree);
    m.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    EtaOrigin.WriteTo(tree);
    PhiOrigin.WriteTo(tree);
    MOrigin.WriteTo(tree);
    WIDTH.WriteTo(tree);
    n90.WriteTo(tree);
    Timing.WriteTo(tree);
    LArQuality.WriteTo(tree);
    nTrk.WriteTo(tree);
    sumPtTrk.WriteTo(tree);
    OriginIndex.WriteTo(tree);
    HECQuality.WriteTo(tree);
    NegativeE.WriteTo(tree);
    AverageLArQF.WriteTo(tree);
    BCH_CORR_CELL.WriteTo(tree);
    BCH_CORR_DOTX.WriteTo(tree);
    BCH_CORR_JET.WriteTo(tree);
    BCH_CORR_JET_FORCELL.WriteTo(tree);
    ENG_BAD_CELLS.WriteTo(tree);
    N_BAD_CELLS.WriteTo(tree);
    N_BAD_CELLS_CORR.WriteTo(tree);
    BAD_CELLS_CORR_E.WriteTo(tree);
    NumTowers.WriteTo(tree);
    ootFracCells5.WriteTo(tree);
    ootFracCells10.WriteTo(tree);
    ootFracClusters5.WriteTo(tree);
    ootFracClusters10.WriteTo(tree);
    SamplingMax.WriteTo(tree);
    fracSamplingMax.WriteTo(tree);
    hecf.WriteTo(tree);
    tgap3f.WriteTo(tree);
    isUgly.WriteTo(tree);
    isBadLooseMinus.WriteTo(tree);
    isBadLoose.WriteTo(tree);
    isBadMedium.WriteTo(tree);
    isBadTight.WriteTo(tree);
    emfrac.WriteTo(tree);
    Offset.WriteTo(tree);
    EMJES.WriteTo(tree);
    EMJES_EtaCorr.WriteTo(tree);
    EMJESnooffset.WriteTo(tree);
    LCJES.WriteTo(tree);
    LCJES_EtaCorr.WriteTo(tree);
    emscale_E.WriteTo(tree);
    emscale_pt.WriteTo(tree);
    emscale_m.WriteTo(tree);
    emscale_eta.WriteTo(tree);
    emscale_phi.WriteTo(tree);
    ActiveArea.WriteTo(tree);
    ActiveAreaPx.WriteTo(tree);
    ActiveAreaPy.WriteTo(tree);
    ActiveAreaPz.WriteTo(tree);
    ActiveAreaE.WriteTo(tree);
    jvtxf.WriteTo(tree);
    jvtxfFull.WriteTo(tree);
    jvtx_x.WriteTo(tree);
    jvtx_y.WriteTo(tree);
    jvtx_z.WriteTo(tree);
    TruthMFindex.WriteTo(tree);
    TruthMF.WriteTo(tree);
    e_PreSamplerB.WriteTo(tree);
    e_EMB1.WriteTo(tree);
    e_EMB2.WriteTo(tree);
    e_EMB3.WriteTo(tree);
    e_PreSamplerE.WriteTo(tree);
    e_EME1.WriteTo(tree);
    e_EME2.WriteTo(tree);
    e_EME3.WriteTo(tree);
    e_HEC0.WriteTo(tree);
    e_HEC1.WriteTo(tree);
    e_HEC2.WriteTo(tree);
    e_HEC3.WriteTo(tree);
    e_TileBar0.WriteTo(tree);
    e_TileBar1.WriteTo(tree);
    e_TileBar2.WriteTo(tree);
    e_TileGap1.WriteTo(tree);
    e_TileGap2.WriteTo(tree);
    e_TileGap3.WriteTo(tree);
    e_TileExt0.WriteTo(tree);
    e_TileExt1.WriteTo(tree);
    e_TileExt2.WriteTo(tree);
    e_FCAL0.WriteTo(tree);
    e_FCAL1.WriteTo(tree);
    e_FCAL2.WriteTo(tree);
    constscale_E.WriteTo(tree);
    constscale_pt.WriteTo(tree);
    constscale_m.WriteTo(tree);
    constscale_eta.WriteTo(tree);
    constscale_phi.WriteTo(tree);
    flavor_weight_Comb.WriteTo(tree);
    flavor_weight_IP2D.WriteTo(tree);
    flavor_weight_IP3D.WriteTo(tree);
    flavor_weight_SV0.WriteTo(tree);
    flavor_weight_SV1.WriteTo(tree);
    flavor_weight_SV2.WriteTo(tree);
    flavor_weight_SoftMuonTagChi2.WriteTo(tree);
    flavor_weight_SecondSoftMuonTagChi2.WriteTo(tree);
    flavor_weight_JetFitterTagNN.WriteTo(tree);
    flavor_weight_JetFitterCOMBNN.WriteTo(tree);
    flavor_weight_MV1.WriteTo(tree);
    flavor_weight_MV2.WriteTo(tree);
    flavor_weight_GbbNN.WriteTo(tree);
    flavor_weight_JetFitterCharm.WriteTo(tree);
    flavor_weight_MV3_bVSu.WriteTo(tree);
    flavor_weight_MV3_bVSc.WriteTo(tree);
    flavor_weight_MV3_cVSu.WriteTo(tree);
    flavor_truth_label.WriteTo(tree);
    flavor_truth_dRminToB.WriteTo(tree);
    flavor_truth_dRminToC.WriteTo(tree);
    flavor_truth_dRminToT.WriteTo(tree);
    flavor_truth_BHadronpdg.WriteTo(tree);
    flavor_truth_vx_x.WriteTo(tree);
    flavor_truth_vx_y.WriteTo(tree);
    flavor_truth_vx_z.WriteTo(tree);
    flavor_component_jfitc_doublePropName.WriteTo(tree);
    flavor_component_jfitc_doublePropValue.WriteTo(tree);
    flavor_component_jfitc_intPropName.WriteTo(tree);
    flavor_component_jfitc_intPropValue.WriteTo(tree);
    flavor_component_jfitc_pu.WriteTo(tree);
    flavor_component_jfitc_pb.WriteTo(tree);
    flavor_component_jfitc_pc.WriteTo(tree);
    flavor_component_jfitc_isValid.WriteTo(tree);
    el_dr.WriteTo(tree);
    el_matched.WriteTo(tree);
    mu_dr.WriteTo(tree);
    mu_matched.WriteTo(tree);
    L1_dr.WriteTo(tree);
    L1_matched.WriteTo(tree);
    L2_dr.WriteTo(tree);
    L2_matched.WriteTo(tree);
    EF_dr.WriteTo(tree);
    EF_matched.WriteTo(tree);
    nTrk_pv0_1GeV.WriteTo(tree);
    sumPtTrk_pv0_1GeV.WriteTo(tree);
    nTrk_allpv_1GeV.WriteTo(tree);
    sumPtTrk_allpv_1GeV.WriteTo(tree);
    nTrk_pv0_500MeV.WriteTo(tree);
    sumPtTrk_pv0_500MeV.WriteTo(tree);
    trackWIDTH_pv0_1GeV.WriteTo(tree);
    trackWIDTH_allpv_1GeV.WriteTo(tree);
    muAssoc_index.WriteTo(tree);
    elAssoc_index.WriteTo(tree);
    tauAssoc_index.WriteTo(tree);
    truthAssoc_index.WriteTo(tree);
    AntiKt4LCTopo_MET_n.WriteTo(tree);
    AntiKt4LCTopo_MET_wpx.WriteTo(tree);
    AntiKt4LCTopo_MET_wpy.WriteTo(tree);
    AntiKt4LCTopo_MET_wet.WriteTo(tree);
    AntiKt4LCTopo_MET_statusWord.WriteTo(tree);
    AntiKt4LCTopo_MET_BDTMedium_n.WriteTo(tree);
    AntiKt4LCTopo_MET_BDTMedium_wpx.WriteTo(tree);
    AntiKt4LCTopo_MET_BDTMedium_wpy.WriteTo(tree);
    AntiKt4LCTopo_MET_BDTMedium_wet.WriteTo(tree);
    AntiKt4LCTopo_MET_BDTMedium_statusWord.WriteTo(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n.WriteTo(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx.WriteTo(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy.WriteTo(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet.WriteTo(tree);
    AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void JetD3PDCollection::SetActive(bool active)
  {
    if(active)
    {
     if(antikt4truth_n.IsAvailable()) antikt4truth_n.SetActive(active);
     if(antikt4truth_E.IsAvailable()) antikt4truth_E.SetActive(active);
     if(antikt4truth_pt.IsAvailable()) antikt4truth_pt.SetActive(active);
     if(antikt4truth_m.IsAvailable()) antikt4truth_m.SetActive(active);
     if(antikt4truth_eta.IsAvailable()) antikt4truth_eta.SetActive(active);
     if(antikt4truth_phi.IsAvailable()) antikt4truth_phi.SetActive(active);
     if(antikt4truth_EtaOrigin.IsAvailable()) antikt4truth_EtaOrigin.SetActive(active);
     if(antikt4truth_PhiOrigin.IsAvailable()) antikt4truth_PhiOrigin.SetActive(active);
     if(antikt4truth_MOrigin.IsAvailable()) antikt4truth_MOrigin.SetActive(active);
     if(antikt4truth_WIDTH.IsAvailable()) antikt4truth_WIDTH.SetActive(active);
     if(antikt4truth_n90.IsAvailable()) antikt4truth_n90.SetActive(active);
     if(antikt4truth_Timing.IsAvailable()) antikt4truth_Timing.SetActive(active);
     if(antikt4truth_LArQuality.IsAvailable()) antikt4truth_LArQuality.SetActive(active);
     if(antikt4truth_nTrk.IsAvailable()) antikt4truth_nTrk.SetActive(active);
     if(antikt4truth_sumPtTrk.IsAvailable()) antikt4truth_sumPtTrk.SetActive(active);
     if(antikt4truth_OriginIndex.IsAvailable()) antikt4truth_OriginIndex.SetActive(active);
     if(antikt4truth_HECQuality.IsAvailable()) antikt4truth_HECQuality.SetActive(active);
     if(antikt4truth_NegativeE.IsAvailable()) antikt4truth_NegativeE.SetActive(active);
     if(antikt4truth_AverageLArQF.IsAvailable()) antikt4truth_AverageLArQF.SetActive(active);
     if(antikt4truth_BCH_CORR_CELL.IsAvailable()) antikt4truth_BCH_CORR_CELL.SetActive(active);
     if(antikt4truth_BCH_CORR_DOTX.IsAvailable()) antikt4truth_BCH_CORR_DOTX.SetActive(active);
     if(antikt4truth_BCH_CORR_JET.IsAvailable()) antikt4truth_BCH_CORR_JET.SetActive(active);
     if(antikt4truth_BCH_CORR_JET_FORCELL.IsAvailable()) antikt4truth_BCH_CORR_JET_FORCELL.SetActive(active);
     if(antikt4truth_ENG_BAD_CELLS.IsAvailable()) antikt4truth_ENG_BAD_CELLS.SetActive(active);
     if(antikt4truth_N_BAD_CELLS.IsAvailable()) antikt4truth_N_BAD_CELLS.SetActive(active);
     if(antikt4truth_N_BAD_CELLS_CORR.IsAvailable()) antikt4truth_N_BAD_CELLS_CORR.SetActive(active);
     if(antikt4truth_BAD_CELLS_CORR_E.IsAvailable()) antikt4truth_BAD_CELLS_CORR_E.SetActive(active);
     if(antikt4truth_NumTowers.IsAvailable()) antikt4truth_NumTowers.SetActive(active);
     if(antikt4truth_ootFracCells5.IsAvailable()) antikt4truth_ootFracCells5.SetActive(active);
     if(antikt4truth_ootFracCells10.IsAvailable()) antikt4truth_ootFracCells10.SetActive(active);
     if(antikt4truth_ootFracClusters5.IsAvailable()) antikt4truth_ootFracClusters5.SetActive(active);
     if(antikt4truth_ootFracClusters10.IsAvailable()) antikt4truth_ootFracClusters10.SetActive(active);
     if(antikt4truth_SamplingMax.IsAvailable()) antikt4truth_SamplingMax.SetActive(active);
     if(antikt4truth_fracSamplingMax.IsAvailable()) antikt4truth_fracSamplingMax.SetActive(active);
     if(antikt4truth_hecf.IsAvailable()) antikt4truth_hecf.SetActive(active);
     if(antikt4truth_tgap3f.IsAvailable()) antikt4truth_tgap3f.SetActive(active);
     if(antikt4truth_isUgly.IsAvailable()) antikt4truth_isUgly.SetActive(active);
     if(antikt4truth_isBadLooseMinus.IsAvailable()) antikt4truth_isBadLooseMinus.SetActive(active);
     if(antikt4truth_isBadLoose.IsAvailable()) antikt4truth_isBadLoose.SetActive(active);
     if(antikt4truth_isBadMedium.IsAvailable()) antikt4truth_isBadMedium.SetActive(active);
     if(antikt4truth_isBadTight.IsAvailable()) antikt4truth_isBadTight.SetActive(active);
     if(antikt4truth_emfrac.IsAvailable()) antikt4truth_emfrac.SetActive(active);
     if(antikt4truth_Offset.IsAvailable()) antikt4truth_Offset.SetActive(active);
     if(antikt4truth_EMJES.IsAvailable()) antikt4truth_EMJES.SetActive(active);
     if(antikt4truth_EMJES_EtaCorr.IsAvailable()) antikt4truth_EMJES_EtaCorr.SetActive(active);
     if(antikt4truth_EMJESnooffset.IsAvailable()) antikt4truth_EMJESnooffset.SetActive(active);
     if(antikt4truth_LCJES.IsAvailable()) antikt4truth_LCJES.SetActive(active);
     if(antikt4truth_LCJES_EtaCorr.IsAvailable()) antikt4truth_LCJES_EtaCorr.SetActive(active);
     if(antikt4truth_emscale_E.IsAvailable()) antikt4truth_emscale_E.SetActive(active);
     if(antikt4truth_emscale_pt.IsAvailable()) antikt4truth_emscale_pt.SetActive(active);
     if(antikt4truth_emscale_m.IsAvailable()) antikt4truth_emscale_m.SetActive(active);
     if(antikt4truth_emscale_eta.IsAvailable()) antikt4truth_emscale_eta.SetActive(active);
     if(antikt4truth_emscale_phi.IsAvailable()) antikt4truth_emscale_phi.SetActive(active);
     if(antikt4truth_ActiveArea.IsAvailable()) antikt4truth_ActiveArea.SetActive(active);
     if(antikt4truth_ActiveAreaPx.IsAvailable()) antikt4truth_ActiveAreaPx.SetActive(active);
     if(antikt4truth_ActiveAreaPy.IsAvailable()) antikt4truth_ActiveAreaPy.SetActive(active);
     if(antikt4truth_ActiveAreaPz.IsAvailable()) antikt4truth_ActiveAreaPz.SetActive(active);
     if(antikt4truth_ActiveAreaE.IsAvailable()) antikt4truth_ActiveAreaE.SetActive(active);
     if(antikt4truth_jvtxf.IsAvailable()) antikt4truth_jvtxf.SetActive(active);
     if(antikt4truth_jvtxfFull.IsAvailable()) antikt4truth_jvtxfFull.SetActive(active);
     if(antikt4truth_jvtx_x.IsAvailable()) antikt4truth_jvtx_x.SetActive(active);
     if(antikt4truth_jvtx_y.IsAvailable()) antikt4truth_jvtx_y.SetActive(active);
     if(antikt4truth_jvtx_z.IsAvailable()) antikt4truth_jvtx_z.SetActive(active);
     if(antikt4truth_TruthMFindex.IsAvailable()) antikt4truth_TruthMFindex.SetActive(active);
     if(antikt4truth_TruthMF.IsAvailable()) antikt4truth_TruthMF.SetActive(active);
     if(antikt4truth_GSCFactorF.IsAvailable()) antikt4truth_GSCFactorF.SetActive(active);
     if(antikt4truth_WidthFraction.IsAvailable()) antikt4truth_WidthFraction.SetActive(active);
     if(antikt4truth_el_dr.IsAvailable()) antikt4truth_el_dr.SetActive(active);
     if(antikt4truth_el_matched.IsAvailable()) antikt4truth_el_matched.SetActive(active);
     if(antikt4truth_mu_dr.IsAvailable()) antikt4truth_mu_dr.SetActive(active);
     if(antikt4truth_mu_matched.IsAvailable()) antikt4truth_mu_matched.SetActive(active);
     if(antikt4truth_L1_dr.IsAvailable()) antikt4truth_L1_dr.SetActive(active);
     if(antikt4truth_L1_matched.IsAvailable()) antikt4truth_L1_matched.SetActive(active);
     if(antikt4truth_L2_dr.IsAvailable()) antikt4truth_L2_dr.SetActive(active);
     if(antikt4truth_L2_matched.IsAvailable()) antikt4truth_L2_matched.SetActive(active);
     if(antikt4truth_EF_dr.IsAvailable()) antikt4truth_EF_dr.SetActive(active);
     if(antikt4truth_EF_matched.IsAvailable()) antikt4truth_EF_matched.SetActive(active);
     if(antikt4truth_muAssoc_index.IsAvailable()) antikt4truth_muAssoc_index.SetActive(active);
     if(antikt4truth_elAssoc_index.IsAvailable()) antikt4truth_elAssoc_index.SetActive(active);
     if(antikt4truth_tauAssoc_index.IsAvailable()) antikt4truth_tauAssoc_index.SetActive(active);
     if(antikt4truth_truthAssoc_index.IsAvailable()) antikt4truth_truthAssoc_index.SetActive(active);
     if(n.IsAvailable()) n.SetActive(active);
     if(E.IsAvailable()) E.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(m.IsAvailable()) m.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(EtaOrigin.IsAvailable()) EtaOrigin.SetActive(active);
     if(PhiOrigin.IsAvailable()) PhiOrigin.SetActive(active);
     if(MOrigin.IsAvailable()) MOrigin.SetActive(active);
     if(WIDTH.IsAvailable()) WIDTH.SetActive(active);
     if(n90.IsAvailable()) n90.SetActive(active);
     if(Timing.IsAvailable()) Timing.SetActive(active);
     if(LArQuality.IsAvailable()) LArQuality.SetActive(active);
     if(nTrk.IsAvailable()) nTrk.SetActive(active);
     if(sumPtTrk.IsAvailable()) sumPtTrk.SetActive(active);
     if(OriginIndex.IsAvailable()) OriginIndex.SetActive(active);
     if(HECQuality.IsAvailable()) HECQuality.SetActive(active);
     if(NegativeE.IsAvailable()) NegativeE.SetActive(active);
     if(AverageLArQF.IsAvailable()) AverageLArQF.SetActive(active);
     if(BCH_CORR_CELL.IsAvailable()) BCH_CORR_CELL.SetActive(active);
     if(BCH_CORR_DOTX.IsAvailable()) BCH_CORR_DOTX.SetActive(active);
     if(BCH_CORR_JET.IsAvailable()) BCH_CORR_JET.SetActive(active);
     if(BCH_CORR_JET_FORCELL.IsAvailable()) BCH_CORR_JET_FORCELL.SetActive(active);
     if(ENG_BAD_CELLS.IsAvailable()) ENG_BAD_CELLS.SetActive(active);
     if(N_BAD_CELLS.IsAvailable()) N_BAD_CELLS.SetActive(active);
     if(N_BAD_CELLS_CORR.IsAvailable()) N_BAD_CELLS_CORR.SetActive(active);
     if(BAD_CELLS_CORR_E.IsAvailable()) BAD_CELLS_CORR_E.SetActive(active);
     if(NumTowers.IsAvailable()) NumTowers.SetActive(active);
     if(ootFracCells5.IsAvailable()) ootFracCells5.SetActive(active);
     if(ootFracCells10.IsAvailable()) ootFracCells10.SetActive(active);
     if(ootFracClusters5.IsAvailable()) ootFracClusters5.SetActive(active);
     if(ootFracClusters10.IsAvailable()) ootFracClusters10.SetActive(active);
     if(SamplingMax.IsAvailable()) SamplingMax.SetActive(active);
     if(fracSamplingMax.IsAvailable()) fracSamplingMax.SetActive(active);
     if(hecf.IsAvailable()) hecf.SetActive(active);
     if(tgap3f.IsAvailable()) tgap3f.SetActive(active);
     if(isUgly.IsAvailable()) isUgly.SetActive(active);
     if(isBadLooseMinus.IsAvailable()) isBadLooseMinus.SetActive(active);
     if(isBadLoose.IsAvailable()) isBadLoose.SetActive(active);
     if(isBadMedium.IsAvailable()) isBadMedium.SetActive(active);
     if(isBadTight.IsAvailable()) isBadTight.SetActive(active);
     if(emfrac.IsAvailable()) emfrac.SetActive(active);
     if(Offset.IsAvailable()) Offset.SetActive(active);
     if(EMJES.IsAvailable()) EMJES.SetActive(active);
     if(EMJES_EtaCorr.IsAvailable()) EMJES_EtaCorr.SetActive(active);
     if(EMJESnooffset.IsAvailable()) EMJESnooffset.SetActive(active);
     if(LCJES.IsAvailable()) LCJES.SetActive(active);
     if(LCJES_EtaCorr.IsAvailable()) LCJES_EtaCorr.SetActive(active);
     if(emscale_E.IsAvailable()) emscale_E.SetActive(active);
     if(emscale_pt.IsAvailable()) emscale_pt.SetActive(active);
     if(emscale_m.IsAvailable()) emscale_m.SetActive(active);
     if(emscale_eta.IsAvailable()) emscale_eta.SetActive(active);
     if(emscale_phi.IsAvailable()) emscale_phi.SetActive(active);
     if(ActiveArea.IsAvailable()) ActiveArea.SetActive(active);
     if(ActiveAreaPx.IsAvailable()) ActiveAreaPx.SetActive(active);
     if(ActiveAreaPy.IsAvailable()) ActiveAreaPy.SetActive(active);
     if(ActiveAreaPz.IsAvailable()) ActiveAreaPz.SetActive(active);
     if(ActiveAreaE.IsAvailable()) ActiveAreaE.SetActive(active);
     if(jvtxf.IsAvailable()) jvtxf.SetActive(active);
     if(jvtxfFull.IsAvailable()) jvtxfFull.SetActive(active);
     if(jvtx_x.IsAvailable()) jvtx_x.SetActive(active);
     if(jvtx_y.IsAvailable()) jvtx_y.SetActive(active);
     if(jvtx_z.IsAvailable()) jvtx_z.SetActive(active);
     if(TruthMFindex.IsAvailable()) TruthMFindex.SetActive(active);
     if(TruthMF.IsAvailable()) TruthMF.SetActive(active);
     if(e_PreSamplerB.IsAvailable()) e_PreSamplerB.SetActive(active);
     if(e_EMB1.IsAvailable()) e_EMB1.SetActive(active);
     if(e_EMB2.IsAvailable()) e_EMB2.SetActive(active);
     if(e_EMB3.IsAvailable()) e_EMB3.SetActive(active);
     if(e_PreSamplerE.IsAvailable()) e_PreSamplerE.SetActive(active);
     if(e_EME1.IsAvailable()) e_EME1.SetActive(active);
     if(e_EME2.IsAvailable()) e_EME2.SetActive(active);
     if(e_EME3.IsAvailable()) e_EME3.SetActive(active);
     if(e_HEC0.IsAvailable()) e_HEC0.SetActive(active);
     if(e_HEC1.IsAvailable()) e_HEC1.SetActive(active);
     if(e_HEC2.IsAvailable()) e_HEC2.SetActive(active);
     if(e_HEC3.IsAvailable()) e_HEC3.SetActive(active);
     if(e_TileBar0.IsAvailable()) e_TileBar0.SetActive(active);
     if(e_TileBar1.IsAvailable()) e_TileBar1.SetActive(active);
     if(e_TileBar2.IsAvailable()) e_TileBar2.SetActive(active);
     if(e_TileGap1.IsAvailable()) e_TileGap1.SetActive(active);
     if(e_TileGap2.IsAvailable()) e_TileGap2.SetActive(active);
     if(e_TileGap3.IsAvailable()) e_TileGap3.SetActive(active);
     if(e_TileExt0.IsAvailable()) e_TileExt0.SetActive(active);
     if(e_TileExt1.IsAvailable()) e_TileExt1.SetActive(active);
     if(e_TileExt2.IsAvailable()) e_TileExt2.SetActive(active);
     if(e_FCAL0.IsAvailable()) e_FCAL0.SetActive(active);
     if(e_FCAL1.IsAvailable()) e_FCAL1.SetActive(active);
     if(e_FCAL2.IsAvailable()) e_FCAL2.SetActive(active);
     if(constscale_E.IsAvailable()) constscale_E.SetActive(active);
     if(constscale_pt.IsAvailable()) constscale_pt.SetActive(active);
     if(constscale_m.IsAvailable()) constscale_m.SetActive(active);
     if(constscale_eta.IsAvailable()) constscale_eta.SetActive(active);
     if(constscale_phi.IsAvailable()) constscale_phi.SetActive(active);
     if(flavor_weight_Comb.IsAvailable()) flavor_weight_Comb.SetActive(active);
     if(flavor_weight_IP2D.IsAvailable()) flavor_weight_IP2D.SetActive(active);
     if(flavor_weight_IP3D.IsAvailable()) flavor_weight_IP3D.SetActive(active);
     if(flavor_weight_SV0.IsAvailable()) flavor_weight_SV0.SetActive(active);
     if(flavor_weight_SV1.IsAvailable()) flavor_weight_SV1.SetActive(active);
     if(flavor_weight_SV2.IsAvailable()) flavor_weight_SV2.SetActive(active);
     if(flavor_weight_SoftMuonTagChi2.IsAvailable()) flavor_weight_SoftMuonTagChi2.SetActive(active);
     if(flavor_weight_SecondSoftMuonTagChi2.IsAvailable()) flavor_weight_SecondSoftMuonTagChi2.SetActive(active);
     if(flavor_weight_JetFitterTagNN.IsAvailable()) flavor_weight_JetFitterTagNN.SetActive(active);
     if(flavor_weight_JetFitterCOMBNN.IsAvailable()) flavor_weight_JetFitterCOMBNN.SetActive(active);
     if(flavor_weight_MV1.IsAvailable()) flavor_weight_MV1.SetActive(active);
     if(flavor_weight_MV2.IsAvailable()) flavor_weight_MV2.SetActive(active);
     if(flavor_weight_GbbNN.IsAvailable()) flavor_weight_GbbNN.SetActive(active);
     if(flavor_weight_JetFitterCharm.IsAvailable()) flavor_weight_JetFitterCharm.SetActive(active);
     if(flavor_weight_MV3_bVSu.IsAvailable()) flavor_weight_MV3_bVSu.SetActive(active);
     if(flavor_weight_MV3_bVSc.IsAvailable()) flavor_weight_MV3_bVSc.SetActive(active);
     if(flavor_weight_MV3_cVSu.IsAvailable()) flavor_weight_MV3_cVSu.SetActive(active);
     if(flavor_truth_label.IsAvailable()) flavor_truth_label.SetActive(active);
     if(flavor_truth_dRminToB.IsAvailable()) flavor_truth_dRminToB.SetActive(active);
     if(flavor_truth_dRminToC.IsAvailable()) flavor_truth_dRminToC.SetActive(active);
     if(flavor_truth_dRminToT.IsAvailable()) flavor_truth_dRminToT.SetActive(active);
     if(flavor_truth_BHadronpdg.IsAvailable()) flavor_truth_BHadronpdg.SetActive(active);
     if(flavor_truth_vx_x.IsAvailable()) flavor_truth_vx_x.SetActive(active);
     if(flavor_truth_vx_y.IsAvailable()) flavor_truth_vx_y.SetActive(active);
     if(flavor_truth_vx_z.IsAvailable()) flavor_truth_vx_z.SetActive(active);
     if(flavor_component_jfitc_doublePropName.IsAvailable()) flavor_component_jfitc_doublePropName.SetActive(active);
     if(flavor_component_jfitc_doublePropValue.IsAvailable()) flavor_component_jfitc_doublePropValue.SetActive(active);
     if(flavor_component_jfitc_intPropName.IsAvailable()) flavor_component_jfitc_intPropName.SetActive(active);
     if(flavor_component_jfitc_intPropValue.IsAvailable()) flavor_component_jfitc_intPropValue.SetActive(active);
     if(flavor_component_jfitc_pu.IsAvailable()) flavor_component_jfitc_pu.SetActive(active);
     if(flavor_component_jfitc_pb.IsAvailable()) flavor_component_jfitc_pb.SetActive(active);
     if(flavor_component_jfitc_pc.IsAvailable()) flavor_component_jfitc_pc.SetActive(active);
     if(flavor_component_jfitc_isValid.IsAvailable()) flavor_component_jfitc_isValid.SetActive(active);
     if(el_dr.IsAvailable()) el_dr.SetActive(active);
     if(el_matched.IsAvailable()) el_matched.SetActive(active);
     if(mu_dr.IsAvailable()) mu_dr.SetActive(active);
     if(mu_matched.IsAvailable()) mu_matched.SetActive(active);
     if(L1_dr.IsAvailable()) L1_dr.SetActive(active);
     if(L1_matched.IsAvailable()) L1_matched.SetActive(active);
     if(L2_dr.IsAvailable()) L2_dr.SetActive(active);
     if(L2_matched.IsAvailable()) L2_matched.SetActive(active);
     if(EF_dr.IsAvailable()) EF_dr.SetActive(active);
     if(EF_matched.IsAvailable()) EF_matched.SetActive(active);
     if(nTrk_pv0_1GeV.IsAvailable()) nTrk_pv0_1GeV.SetActive(active);
     if(sumPtTrk_pv0_1GeV.IsAvailable()) sumPtTrk_pv0_1GeV.SetActive(active);
     if(nTrk_allpv_1GeV.IsAvailable()) nTrk_allpv_1GeV.SetActive(active);
     if(sumPtTrk_allpv_1GeV.IsAvailable()) sumPtTrk_allpv_1GeV.SetActive(active);
     if(nTrk_pv0_500MeV.IsAvailable()) nTrk_pv0_500MeV.SetActive(active);
     if(sumPtTrk_pv0_500MeV.IsAvailable()) sumPtTrk_pv0_500MeV.SetActive(active);
     if(trackWIDTH_pv0_1GeV.IsAvailable()) trackWIDTH_pv0_1GeV.SetActive(active);
     if(trackWIDTH_allpv_1GeV.IsAvailable()) trackWIDTH_allpv_1GeV.SetActive(active);
     if(muAssoc_index.IsAvailable()) muAssoc_index.SetActive(active);
     if(elAssoc_index.IsAvailable()) elAssoc_index.SetActive(active);
     if(tauAssoc_index.IsAvailable()) tauAssoc_index.SetActive(active);
     if(truthAssoc_index.IsAvailable()) truthAssoc_index.SetActive(active);
     if(AntiKt4LCTopo_MET_n.IsAvailable()) AntiKt4LCTopo_MET_n.SetActive(active);
     if(AntiKt4LCTopo_MET_wpx.IsAvailable()) AntiKt4LCTopo_MET_wpx.SetActive(active);
     if(AntiKt4LCTopo_MET_wpy.IsAvailable()) AntiKt4LCTopo_MET_wpy.SetActive(active);
     if(AntiKt4LCTopo_MET_wet.IsAvailable()) AntiKt4LCTopo_MET_wet.SetActive(active);
     if(AntiKt4LCTopo_MET_statusWord.IsAvailable()) AntiKt4LCTopo_MET_statusWord.SetActive(active);
     if(AntiKt4LCTopo_MET_BDTMedium_n.IsAvailable()) AntiKt4LCTopo_MET_BDTMedium_n.SetActive(active);
     if(AntiKt4LCTopo_MET_BDTMedium_wpx.IsAvailable()) AntiKt4LCTopo_MET_BDTMedium_wpx.SetActive(active);
     if(AntiKt4LCTopo_MET_BDTMedium_wpy.IsAvailable()) AntiKt4LCTopo_MET_BDTMedium_wpy.SetActive(active);
     if(AntiKt4LCTopo_MET_BDTMedium_wet.IsAvailable()) AntiKt4LCTopo_MET_BDTMedium_wet.SetActive(active);
     if(AntiKt4LCTopo_MET_BDTMedium_statusWord.IsAvailable()) AntiKt4LCTopo_MET_BDTMedium_statusWord.SetActive(active);
     if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n.IsAvailable()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n.SetActive(active);
     if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx.IsAvailable()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx.SetActive(active);
     if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy.IsAvailable()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy.SetActive(active);
     if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet.IsAvailable()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet.SetActive(active);
     if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord.IsAvailable()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord.SetActive(active);
    }
    else
    {
      antikt4truth_n.SetActive(active);
      antikt4truth_E.SetActive(active);
      antikt4truth_pt.SetActive(active);
      antikt4truth_m.SetActive(active);
      antikt4truth_eta.SetActive(active);
      antikt4truth_phi.SetActive(active);
      antikt4truth_EtaOrigin.SetActive(active);
      antikt4truth_PhiOrigin.SetActive(active);
      antikt4truth_MOrigin.SetActive(active);
      antikt4truth_WIDTH.SetActive(active);
      antikt4truth_n90.SetActive(active);
      antikt4truth_Timing.SetActive(active);
      antikt4truth_LArQuality.SetActive(active);
      antikt4truth_nTrk.SetActive(active);
      antikt4truth_sumPtTrk.SetActive(active);
      antikt4truth_OriginIndex.SetActive(active);
      antikt4truth_HECQuality.SetActive(active);
      antikt4truth_NegativeE.SetActive(active);
      antikt4truth_AverageLArQF.SetActive(active);
      antikt4truth_BCH_CORR_CELL.SetActive(active);
      antikt4truth_BCH_CORR_DOTX.SetActive(active);
      antikt4truth_BCH_CORR_JET.SetActive(active);
      antikt4truth_BCH_CORR_JET_FORCELL.SetActive(active);
      antikt4truth_ENG_BAD_CELLS.SetActive(active);
      antikt4truth_N_BAD_CELLS.SetActive(active);
      antikt4truth_N_BAD_CELLS_CORR.SetActive(active);
      antikt4truth_BAD_CELLS_CORR_E.SetActive(active);
      antikt4truth_NumTowers.SetActive(active);
      antikt4truth_ootFracCells5.SetActive(active);
      antikt4truth_ootFracCells10.SetActive(active);
      antikt4truth_ootFracClusters5.SetActive(active);
      antikt4truth_ootFracClusters10.SetActive(active);
      antikt4truth_SamplingMax.SetActive(active);
      antikt4truth_fracSamplingMax.SetActive(active);
      antikt4truth_hecf.SetActive(active);
      antikt4truth_tgap3f.SetActive(active);
      antikt4truth_isUgly.SetActive(active);
      antikt4truth_isBadLooseMinus.SetActive(active);
      antikt4truth_isBadLoose.SetActive(active);
      antikt4truth_isBadMedium.SetActive(active);
      antikt4truth_isBadTight.SetActive(active);
      antikt4truth_emfrac.SetActive(active);
      antikt4truth_Offset.SetActive(active);
      antikt4truth_EMJES.SetActive(active);
      antikt4truth_EMJES_EtaCorr.SetActive(active);
      antikt4truth_EMJESnooffset.SetActive(active);
      antikt4truth_LCJES.SetActive(active);
      antikt4truth_LCJES_EtaCorr.SetActive(active);
      antikt4truth_emscale_E.SetActive(active);
      antikt4truth_emscale_pt.SetActive(active);
      antikt4truth_emscale_m.SetActive(active);
      antikt4truth_emscale_eta.SetActive(active);
      antikt4truth_emscale_phi.SetActive(active);
      antikt4truth_ActiveArea.SetActive(active);
      antikt4truth_ActiveAreaPx.SetActive(active);
      antikt4truth_ActiveAreaPy.SetActive(active);
      antikt4truth_ActiveAreaPz.SetActive(active);
      antikt4truth_ActiveAreaE.SetActive(active);
      antikt4truth_jvtxf.SetActive(active);
      antikt4truth_jvtxfFull.SetActive(active);
      antikt4truth_jvtx_x.SetActive(active);
      antikt4truth_jvtx_y.SetActive(active);
      antikt4truth_jvtx_z.SetActive(active);
      antikt4truth_TruthMFindex.SetActive(active);
      antikt4truth_TruthMF.SetActive(active);
      antikt4truth_GSCFactorF.SetActive(active);
      antikt4truth_WidthFraction.SetActive(active);
      antikt4truth_el_dr.SetActive(active);
      antikt4truth_el_matched.SetActive(active);
      antikt4truth_mu_dr.SetActive(active);
      antikt4truth_mu_matched.SetActive(active);
      antikt4truth_L1_dr.SetActive(active);
      antikt4truth_L1_matched.SetActive(active);
      antikt4truth_L2_dr.SetActive(active);
      antikt4truth_L2_matched.SetActive(active);
      antikt4truth_EF_dr.SetActive(active);
      antikt4truth_EF_matched.SetActive(active);
      antikt4truth_muAssoc_index.SetActive(active);
      antikt4truth_elAssoc_index.SetActive(active);
      antikt4truth_tauAssoc_index.SetActive(active);
      antikt4truth_truthAssoc_index.SetActive(active);
      n.SetActive(active);
      E.SetActive(active);
      pt.SetActive(active);
      m.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      EtaOrigin.SetActive(active);
      PhiOrigin.SetActive(active);
      MOrigin.SetActive(active);
      WIDTH.SetActive(active);
      n90.SetActive(active);
      Timing.SetActive(active);
      LArQuality.SetActive(active);
      nTrk.SetActive(active);
      sumPtTrk.SetActive(active);
      OriginIndex.SetActive(active);
      HECQuality.SetActive(active);
      NegativeE.SetActive(active);
      AverageLArQF.SetActive(active);
      BCH_CORR_CELL.SetActive(active);
      BCH_CORR_DOTX.SetActive(active);
      BCH_CORR_JET.SetActive(active);
      BCH_CORR_JET_FORCELL.SetActive(active);
      ENG_BAD_CELLS.SetActive(active);
      N_BAD_CELLS.SetActive(active);
      N_BAD_CELLS_CORR.SetActive(active);
      BAD_CELLS_CORR_E.SetActive(active);
      NumTowers.SetActive(active);
      ootFracCells5.SetActive(active);
      ootFracCells10.SetActive(active);
      ootFracClusters5.SetActive(active);
      ootFracClusters10.SetActive(active);
      SamplingMax.SetActive(active);
      fracSamplingMax.SetActive(active);
      hecf.SetActive(active);
      tgap3f.SetActive(active);
      isUgly.SetActive(active);
      isBadLooseMinus.SetActive(active);
      isBadLoose.SetActive(active);
      isBadMedium.SetActive(active);
      isBadTight.SetActive(active);
      emfrac.SetActive(active);
      Offset.SetActive(active);
      EMJES.SetActive(active);
      EMJES_EtaCorr.SetActive(active);
      EMJESnooffset.SetActive(active);
      LCJES.SetActive(active);
      LCJES_EtaCorr.SetActive(active);
      emscale_E.SetActive(active);
      emscale_pt.SetActive(active);
      emscale_m.SetActive(active);
      emscale_eta.SetActive(active);
      emscale_phi.SetActive(active);
      ActiveArea.SetActive(active);
      ActiveAreaPx.SetActive(active);
      ActiveAreaPy.SetActive(active);
      ActiveAreaPz.SetActive(active);
      ActiveAreaE.SetActive(active);
      jvtxf.SetActive(active);
      jvtxfFull.SetActive(active);
      jvtx_x.SetActive(active);
      jvtx_y.SetActive(active);
      jvtx_z.SetActive(active);
      TruthMFindex.SetActive(active);
      TruthMF.SetActive(active);
      e_PreSamplerB.SetActive(active);
      e_EMB1.SetActive(active);
      e_EMB2.SetActive(active);
      e_EMB3.SetActive(active);
      e_PreSamplerE.SetActive(active);
      e_EME1.SetActive(active);
      e_EME2.SetActive(active);
      e_EME3.SetActive(active);
      e_HEC0.SetActive(active);
      e_HEC1.SetActive(active);
      e_HEC2.SetActive(active);
      e_HEC3.SetActive(active);
      e_TileBar0.SetActive(active);
      e_TileBar1.SetActive(active);
      e_TileBar2.SetActive(active);
      e_TileGap1.SetActive(active);
      e_TileGap2.SetActive(active);
      e_TileGap3.SetActive(active);
      e_TileExt0.SetActive(active);
      e_TileExt1.SetActive(active);
      e_TileExt2.SetActive(active);
      e_FCAL0.SetActive(active);
      e_FCAL1.SetActive(active);
      e_FCAL2.SetActive(active);
      constscale_E.SetActive(active);
      constscale_pt.SetActive(active);
      constscale_m.SetActive(active);
      constscale_eta.SetActive(active);
      constscale_phi.SetActive(active);
      flavor_weight_Comb.SetActive(active);
      flavor_weight_IP2D.SetActive(active);
      flavor_weight_IP3D.SetActive(active);
      flavor_weight_SV0.SetActive(active);
      flavor_weight_SV1.SetActive(active);
      flavor_weight_SV2.SetActive(active);
      flavor_weight_SoftMuonTagChi2.SetActive(active);
      flavor_weight_SecondSoftMuonTagChi2.SetActive(active);
      flavor_weight_JetFitterTagNN.SetActive(active);
      flavor_weight_JetFitterCOMBNN.SetActive(active);
      flavor_weight_MV1.SetActive(active);
      flavor_weight_MV2.SetActive(active);
      flavor_weight_GbbNN.SetActive(active);
      flavor_weight_JetFitterCharm.SetActive(active);
      flavor_weight_MV3_bVSu.SetActive(active);
      flavor_weight_MV3_bVSc.SetActive(active);
      flavor_weight_MV3_cVSu.SetActive(active);
      flavor_truth_label.SetActive(active);
      flavor_truth_dRminToB.SetActive(active);
      flavor_truth_dRminToC.SetActive(active);
      flavor_truth_dRminToT.SetActive(active);
      flavor_truth_BHadronpdg.SetActive(active);
      flavor_truth_vx_x.SetActive(active);
      flavor_truth_vx_y.SetActive(active);
      flavor_truth_vx_z.SetActive(active);
      flavor_component_jfitc_doublePropName.SetActive(active);
      flavor_component_jfitc_doublePropValue.SetActive(active);
      flavor_component_jfitc_intPropName.SetActive(active);
      flavor_component_jfitc_intPropValue.SetActive(active);
      flavor_component_jfitc_pu.SetActive(active);
      flavor_component_jfitc_pb.SetActive(active);
      flavor_component_jfitc_pc.SetActive(active);
      flavor_component_jfitc_isValid.SetActive(active);
      el_dr.SetActive(active);
      el_matched.SetActive(active);
      mu_dr.SetActive(active);
      mu_matched.SetActive(active);
      L1_dr.SetActive(active);
      L1_matched.SetActive(active);
      L2_dr.SetActive(active);
      L2_matched.SetActive(active);
      EF_dr.SetActive(active);
      EF_matched.SetActive(active);
      nTrk_pv0_1GeV.SetActive(active);
      sumPtTrk_pv0_1GeV.SetActive(active);
      nTrk_allpv_1GeV.SetActive(active);
      sumPtTrk_allpv_1GeV.SetActive(active);
      nTrk_pv0_500MeV.SetActive(active);
      sumPtTrk_pv0_500MeV.SetActive(active);
      trackWIDTH_pv0_1GeV.SetActive(active);
      trackWIDTH_allpv_1GeV.SetActive(active);
      muAssoc_index.SetActive(active);
      elAssoc_index.SetActive(active);
      tauAssoc_index.SetActive(active);
      truthAssoc_index.SetActive(active);
      AntiKt4LCTopo_MET_n.SetActive(active);
      AntiKt4LCTopo_MET_wpx.SetActive(active);
      AntiKt4LCTopo_MET_wpy.SetActive(active);
      AntiKt4LCTopo_MET_wet.SetActive(active);
      AntiKt4LCTopo_MET_statusWord.SetActive(active);
      AntiKt4LCTopo_MET_BDTMedium_n.SetActive(active);
      AntiKt4LCTopo_MET_BDTMedium_wpx.SetActive(active);
      AntiKt4LCTopo_MET_BDTMedium_wpy.SetActive(active);
      AntiKt4LCTopo_MET_BDTMedium_wet.SetActive(active);
      AntiKt4LCTopo_MET_BDTMedium_statusWord.SetActive(active);
      AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n.SetActive(active);
      AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx.SetActive(active);
      AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy.SetActive(active);
      AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet.SetActive(active);
      AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void JetD3PDCollection::ReadAllActive()
  {
    if(antikt4truth_n.IsActive()) antikt4truth_n();
    if(antikt4truth_E.IsActive()) antikt4truth_E();
    if(antikt4truth_pt.IsActive()) antikt4truth_pt();
    if(antikt4truth_m.IsActive()) antikt4truth_m();
    if(antikt4truth_eta.IsActive()) antikt4truth_eta();
    if(antikt4truth_phi.IsActive()) antikt4truth_phi();
    if(antikt4truth_EtaOrigin.IsActive()) antikt4truth_EtaOrigin();
    if(antikt4truth_PhiOrigin.IsActive()) antikt4truth_PhiOrigin();
    if(antikt4truth_MOrigin.IsActive()) antikt4truth_MOrigin();
    if(antikt4truth_WIDTH.IsActive()) antikt4truth_WIDTH();
    if(antikt4truth_n90.IsActive()) antikt4truth_n90();
    if(antikt4truth_Timing.IsActive()) antikt4truth_Timing();
    if(antikt4truth_LArQuality.IsActive()) antikt4truth_LArQuality();
    if(antikt4truth_nTrk.IsActive()) antikt4truth_nTrk();
    if(antikt4truth_sumPtTrk.IsActive()) antikt4truth_sumPtTrk();
    if(antikt4truth_OriginIndex.IsActive()) antikt4truth_OriginIndex();
    if(antikt4truth_HECQuality.IsActive()) antikt4truth_HECQuality();
    if(antikt4truth_NegativeE.IsActive()) antikt4truth_NegativeE();
    if(antikt4truth_AverageLArQF.IsActive()) antikt4truth_AverageLArQF();
    if(antikt4truth_BCH_CORR_CELL.IsActive()) antikt4truth_BCH_CORR_CELL();
    if(antikt4truth_BCH_CORR_DOTX.IsActive()) antikt4truth_BCH_CORR_DOTX();
    if(antikt4truth_BCH_CORR_JET.IsActive()) antikt4truth_BCH_CORR_JET();
    if(antikt4truth_BCH_CORR_JET_FORCELL.IsActive()) antikt4truth_BCH_CORR_JET_FORCELL();
    if(antikt4truth_ENG_BAD_CELLS.IsActive()) antikt4truth_ENG_BAD_CELLS();
    if(antikt4truth_N_BAD_CELLS.IsActive()) antikt4truth_N_BAD_CELLS();
    if(antikt4truth_N_BAD_CELLS_CORR.IsActive()) antikt4truth_N_BAD_CELLS_CORR();
    if(antikt4truth_BAD_CELLS_CORR_E.IsActive()) antikt4truth_BAD_CELLS_CORR_E();
    if(antikt4truth_NumTowers.IsActive()) antikt4truth_NumTowers();
    if(antikt4truth_ootFracCells5.IsActive()) antikt4truth_ootFracCells5();
    if(antikt4truth_ootFracCells10.IsActive()) antikt4truth_ootFracCells10();
    if(antikt4truth_ootFracClusters5.IsActive()) antikt4truth_ootFracClusters5();
    if(antikt4truth_ootFracClusters10.IsActive()) antikt4truth_ootFracClusters10();
    if(antikt4truth_SamplingMax.IsActive()) antikt4truth_SamplingMax();
    if(antikt4truth_fracSamplingMax.IsActive()) antikt4truth_fracSamplingMax();
    if(antikt4truth_hecf.IsActive()) antikt4truth_hecf();
    if(antikt4truth_tgap3f.IsActive()) antikt4truth_tgap3f();
    if(antikt4truth_isUgly.IsActive()) antikt4truth_isUgly();
    if(antikt4truth_isBadLooseMinus.IsActive()) antikt4truth_isBadLooseMinus();
    if(antikt4truth_isBadLoose.IsActive()) antikt4truth_isBadLoose();
    if(antikt4truth_isBadMedium.IsActive()) antikt4truth_isBadMedium();
    if(antikt4truth_isBadTight.IsActive()) antikt4truth_isBadTight();
    if(antikt4truth_emfrac.IsActive()) antikt4truth_emfrac();
    if(antikt4truth_Offset.IsActive()) antikt4truth_Offset();
    if(antikt4truth_EMJES.IsActive()) antikt4truth_EMJES();
    if(antikt4truth_EMJES_EtaCorr.IsActive()) antikt4truth_EMJES_EtaCorr();
    if(antikt4truth_EMJESnooffset.IsActive()) antikt4truth_EMJESnooffset();
    if(antikt4truth_LCJES.IsActive()) antikt4truth_LCJES();
    if(antikt4truth_LCJES_EtaCorr.IsActive()) antikt4truth_LCJES_EtaCorr();
    if(antikt4truth_emscale_E.IsActive()) antikt4truth_emscale_E();
    if(antikt4truth_emscale_pt.IsActive()) antikt4truth_emscale_pt();
    if(antikt4truth_emscale_m.IsActive()) antikt4truth_emscale_m();
    if(antikt4truth_emscale_eta.IsActive()) antikt4truth_emscale_eta();
    if(antikt4truth_emscale_phi.IsActive()) antikt4truth_emscale_phi();
    if(antikt4truth_ActiveArea.IsActive()) antikt4truth_ActiveArea();
    if(antikt4truth_ActiveAreaPx.IsActive()) antikt4truth_ActiveAreaPx();
    if(antikt4truth_ActiveAreaPy.IsActive()) antikt4truth_ActiveAreaPy();
    if(antikt4truth_ActiveAreaPz.IsActive()) antikt4truth_ActiveAreaPz();
    if(antikt4truth_ActiveAreaE.IsActive()) antikt4truth_ActiveAreaE();
    if(antikt4truth_jvtxf.IsActive()) antikt4truth_jvtxf();
    if(antikt4truth_jvtxfFull.IsActive()) antikt4truth_jvtxfFull();
    if(antikt4truth_jvtx_x.IsActive()) antikt4truth_jvtx_x();
    if(antikt4truth_jvtx_y.IsActive()) antikt4truth_jvtx_y();
    if(antikt4truth_jvtx_z.IsActive()) antikt4truth_jvtx_z();
    if(antikt4truth_TruthMFindex.IsActive()) antikt4truth_TruthMFindex();
    if(antikt4truth_TruthMF.IsActive()) antikt4truth_TruthMF();
    if(antikt4truth_GSCFactorF.IsActive()) antikt4truth_GSCFactorF();
    if(antikt4truth_WidthFraction.IsActive()) antikt4truth_WidthFraction();
    if(antikt4truth_el_dr.IsActive()) antikt4truth_el_dr();
    if(antikt4truth_el_matched.IsActive()) antikt4truth_el_matched();
    if(antikt4truth_mu_dr.IsActive()) antikt4truth_mu_dr();
    if(antikt4truth_mu_matched.IsActive()) antikt4truth_mu_matched();
    if(antikt4truth_L1_dr.IsActive()) antikt4truth_L1_dr();
    if(antikt4truth_L1_matched.IsActive()) antikt4truth_L1_matched();
    if(antikt4truth_L2_dr.IsActive()) antikt4truth_L2_dr();
    if(antikt4truth_L2_matched.IsActive()) antikt4truth_L2_matched();
    if(antikt4truth_EF_dr.IsActive()) antikt4truth_EF_dr();
    if(antikt4truth_EF_matched.IsActive()) antikt4truth_EF_matched();
    if(antikt4truth_muAssoc_index.IsActive()) antikt4truth_muAssoc_index();
    if(antikt4truth_elAssoc_index.IsActive()) antikt4truth_elAssoc_index();
    if(antikt4truth_tauAssoc_index.IsActive()) antikt4truth_tauAssoc_index();
    if(antikt4truth_truthAssoc_index.IsActive()) antikt4truth_truthAssoc_index();
    if(n.IsActive()) n();
    if(E.IsActive()) E();
    if(pt.IsActive()) pt();
    if(m.IsActive()) m();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(EtaOrigin.IsActive()) EtaOrigin();
    if(PhiOrigin.IsActive()) PhiOrigin();
    if(MOrigin.IsActive()) MOrigin();
    if(WIDTH.IsActive()) WIDTH();
    if(n90.IsActive()) n90();
    if(Timing.IsActive()) Timing();
    if(LArQuality.IsActive()) LArQuality();
    if(nTrk.IsActive()) nTrk();
    if(sumPtTrk.IsActive()) sumPtTrk();
    if(OriginIndex.IsActive()) OriginIndex();
    if(HECQuality.IsActive()) HECQuality();
    if(NegativeE.IsActive()) NegativeE();
    if(AverageLArQF.IsActive()) AverageLArQF();
    if(BCH_CORR_CELL.IsActive()) BCH_CORR_CELL();
    if(BCH_CORR_DOTX.IsActive()) BCH_CORR_DOTX();
    if(BCH_CORR_JET.IsActive()) BCH_CORR_JET();
    if(BCH_CORR_JET_FORCELL.IsActive()) BCH_CORR_JET_FORCELL();
    if(ENG_BAD_CELLS.IsActive()) ENG_BAD_CELLS();
    if(N_BAD_CELLS.IsActive()) N_BAD_CELLS();
    if(N_BAD_CELLS_CORR.IsActive()) N_BAD_CELLS_CORR();
    if(BAD_CELLS_CORR_E.IsActive()) BAD_CELLS_CORR_E();
    if(NumTowers.IsActive()) NumTowers();
    if(ootFracCells5.IsActive()) ootFracCells5();
    if(ootFracCells10.IsActive()) ootFracCells10();
    if(ootFracClusters5.IsActive()) ootFracClusters5();
    if(ootFracClusters10.IsActive()) ootFracClusters10();
    if(SamplingMax.IsActive()) SamplingMax();
    if(fracSamplingMax.IsActive()) fracSamplingMax();
    if(hecf.IsActive()) hecf();
    if(tgap3f.IsActive()) tgap3f();
    if(isUgly.IsActive()) isUgly();
    if(isBadLooseMinus.IsActive()) isBadLooseMinus();
    if(isBadLoose.IsActive()) isBadLoose();
    if(isBadMedium.IsActive()) isBadMedium();
    if(isBadTight.IsActive()) isBadTight();
    if(emfrac.IsActive()) emfrac();
    if(Offset.IsActive()) Offset();
    if(EMJES.IsActive()) EMJES();
    if(EMJES_EtaCorr.IsActive()) EMJES_EtaCorr();
    if(EMJESnooffset.IsActive()) EMJESnooffset();
    if(LCJES.IsActive()) LCJES();
    if(LCJES_EtaCorr.IsActive()) LCJES_EtaCorr();
    if(emscale_E.IsActive()) emscale_E();
    if(emscale_pt.IsActive()) emscale_pt();
    if(emscale_m.IsActive()) emscale_m();
    if(emscale_eta.IsActive()) emscale_eta();
    if(emscale_phi.IsActive()) emscale_phi();
    if(ActiveArea.IsActive()) ActiveArea();
    if(ActiveAreaPx.IsActive()) ActiveAreaPx();
    if(ActiveAreaPy.IsActive()) ActiveAreaPy();
    if(ActiveAreaPz.IsActive()) ActiveAreaPz();
    if(ActiveAreaE.IsActive()) ActiveAreaE();
    if(jvtxf.IsActive()) jvtxf();
    if(jvtxfFull.IsActive()) jvtxfFull();
    if(jvtx_x.IsActive()) jvtx_x();
    if(jvtx_y.IsActive()) jvtx_y();
    if(jvtx_z.IsActive()) jvtx_z();
    if(TruthMFindex.IsActive()) TruthMFindex();
    if(TruthMF.IsActive()) TruthMF();
    if(e_PreSamplerB.IsActive()) e_PreSamplerB();
    if(e_EMB1.IsActive()) e_EMB1();
    if(e_EMB2.IsActive()) e_EMB2();
    if(e_EMB3.IsActive()) e_EMB3();
    if(e_PreSamplerE.IsActive()) e_PreSamplerE();
    if(e_EME1.IsActive()) e_EME1();
    if(e_EME2.IsActive()) e_EME2();
    if(e_EME3.IsActive()) e_EME3();
    if(e_HEC0.IsActive()) e_HEC0();
    if(e_HEC1.IsActive()) e_HEC1();
    if(e_HEC2.IsActive()) e_HEC2();
    if(e_HEC3.IsActive()) e_HEC3();
    if(e_TileBar0.IsActive()) e_TileBar0();
    if(e_TileBar1.IsActive()) e_TileBar1();
    if(e_TileBar2.IsActive()) e_TileBar2();
    if(e_TileGap1.IsActive()) e_TileGap1();
    if(e_TileGap2.IsActive()) e_TileGap2();
    if(e_TileGap3.IsActive()) e_TileGap3();
    if(e_TileExt0.IsActive()) e_TileExt0();
    if(e_TileExt1.IsActive()) e_TileExt1();
    if(e_TileExt2.IsActive()) e_TileExt2();
    if(e_FCAL0.IsActive()) e_FCAL0();
    if(e_FCAL1.IsActive()) e_FCAL1();
    if(e_FCAL2.IsActive()) e_FCAL2();
    if(constscale_E.IsActive()) constscale_E();
    if(constscale_pt.IsActive()) constscale_pt();
    if(constscale_m.IsActive()) constscale_m();
    if(constscale_eta.IsActive()) constscale_eta();
    if(constscale_phi.IsActive()) constscale_phi();
    if(flavor_weight_Comb.IsActive()) flavor_weight_Comb();
    if(flavor_weight_IP2D.IsActive()) flavor_weight_IP2D();
    if(flavor_weight_IP3D.IsActive()) flavor_weight_IP3D();
    if(flavor_weight_SV0.IsActive()) flavor_weight_SV0();
    if(flavor_weight_SV1.IsActive()) flavor_weight_SV1();
    if(flavor_weight_SV2.IsActive()) flavor_weight_SV2();
    if(flavor_weight_SoftMuonTagChi2.IsActive()) flavor_weight_SoftMuonTagChi2();
    if(flavor_weight_SecondSoftMuonTagChi2.IsActive()) flavor_weight_SecondSoftMuonTagChi2();
    if(flavor_weight_JetFitterTagNN.IsActive()) flavor_weight_JetFitterTagNN();
    if(flavor_weight_JetFitterCOMBNN.IsActive()) flavor_weight_JetFitterCOMBNN();
    if(flavor_weight_MV1.IsActive()) flavor_weight_MV1();
    if(flavor_weight_MV2.IsActive()) flavor_weight_MV2();
    if(flavor_weight_GbbNN.IsActive()) flavor_weight_GbbNN();
    if(flavor_weight_JetFitterCharm.IsActive()) flavor_weight_JetFitterCharm();
    if(flavor_weight_MV3_bVSu.IsActive()) flavor_weight_MV3_bVSu();
    if(flavor_weight_MV3_bVSc.IsActive()) flavor_weight_MV3_bVSc();
    if(flavor_weight_MV3_cVSu.IsActive()) flavor_weight_MV3_cVSu();
    if(flavor_truth_label.IsActive()) flavor_truth_label();
    if(flavor_truth_dRminToB.IsActive()) flavor_truth_dRminToB();
    if(flavor_truth_dRminToC.IsActive()) flavor_truth_dRminToC();
    if(flavor_truth_dRminToT.IsActive()) flavor_truth_dRminToT();
    if(flavor_truth_BHadronpdg.IsActive()) flavor_truth_BHadronpdg();
    if(flavor_truth_vx_x.IsActive()) flavor_truth_vx_x();
    if(flavor_truth_vx_y.IsActive()) flavor_truth_vx_y();
    if(flavor_truth_vx_z.IsActive()) flavor_truth_vx_z();
    if(flavor_component_jfitc_doublePropName.IsActive()) flavor_component_jfitc_doublePropName();
    if(flavor_component_jfitc_doublePropValue.IsActive()) flavor_component_jfitc_doublePropValue();
    if(flavor_component_jfitc_intPropName.IsActive()) flavor_component_jfitc_intPropName();
    if(flavor_component_jfitc_intPropValue.IsActive()) flavor_component_jfitc_intPropValue();
    if(flavor_component_jfitc_pu.IsActive()) flavor_component_jfitc_pu();
    if(flavor_component_jfitc_pb.IsActive()) flavor_component_jfitc_pb();
    if(flavor_component_jfitc_pc.IsActive()) flavor_component_jfitc_pc();
    if(flavor_component_jfitc_isValid.IsActive()) flavor_component_jfitc_isValid();
    if(el_dr.IsActive()) el_dr();
    if(el_matched.IsActive()) el_matched();
    if(mu_dr.IsActive()) mu_dr();
    if(mu_matched.IsActive()) mu_matched();
    if(L1_dr.IsActive()) L1_dr();
    if(L1_matched.IsActive()) L1_matched();
    if(L2_dr.IsActive()) L2_dr();
    if(L2_matched.IsActive()) L2_matched();
    if(EF_dr.IsActive()) EF_dr();
    if(EF_matched.IsActive()) EF_matched();
    if(nTrk_pv0_1GeV.IsActive()) nTrk_pv0_1GeV();
    if(sumPtTrk_pv0_1GeV.IsActive()) sumPtTrk_pv0_1GeV();
    if(nTrk_allpv_1GeV.IsActive()) nTrk_allpv_1GeV();
    if(sumPtTrk_allpv_1GeV.IsActive()) sumPtTrk_allpv_1GeV();
    if(nTrk_pv0_500MeV.IsActive()) nTrk_pv0_500MeV();
    if(sumPtTrk_pv0_500MeV.IsActive()) sumPtTrk_pv0_500MeV();
    if(trackWIDTH_pv0_1GeV.IsActive()) trackWIDTH_pv0_1GeV();
    if(trackWIDTH_allpv_1GeV.IsActive()) trackWIDTH_allpv_1GeV();
    if(muAssoc_index.IsActive()) muAssoc_index();
    if(elAssoc_index.IsActive()) elAssoc_index();
    if(tauAssoc_index.IsActive()) tauAssoc_index();
    if(truthAssoc_index.IsActive()) truthAssoc_index();
    if(AntiKt4LCTopo_MET_n.IsActive()) AntiKt4LCTopo_MET_n();
    if(AntiKt4LCTopo_MET_wpx.IsActive()) AntiKt4LCTopo_MET_wpx();
    if(AntiKt4LCTopo_MET_wpy.IsActive()) AntiKt4LCTopo_MET_wpy();
    if(AntiKt4LCTopo_MET_wet.IsActive()) AntiKt4LCTopo_MET_wet();
    if(AntiKt4LCTopo_MET_statusWord.IsActive()) AntiKt4LCTopo_MET_statusWord();
    if(AntiKt4LCTopo_MET_BDTMedium_n.IsActive()) AntiKt4LCTopo_MET_BDTMedium_n();
    if(AntiKt4LCTopo_MET_BDTMedium_wpx.IsActive()) AntiKt4LCTopo_MET_BDTMedium_wpx();
    if(AntiKt4LCTopo_MET_BDTMedium_wpy.IsActive()) AntiKt4LCTopo_MET_BDTMedium_wpy();
    if(AntiKt4LCTopo_MET_BDTMedium_wet.IsActive()) AntiKt4LCTopo_MET_BDTMedium_wet();
    if(AntiKt4LCTopo_MET_BDTMedium_statusWord.IsActive()) AntiKt4LCTopo_MET_BDTMedium_statusWord();
    if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n.IsActive()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_n();
    if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx.IsActive()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpx();
    if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy.IsActive()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wpy();
    if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet.IsActive()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_wet();
    if(AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord.IsActive()) AntiKt4LCTopo_tightpp_MET_AntiKt4LCTopo_tightpp_statusWord();
  }

} // namespace D3PDReader
#endif // D3PDREADER_JetD3PDCollection_CXX
