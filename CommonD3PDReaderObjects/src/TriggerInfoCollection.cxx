// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_TriggerInfoCollection_CXX
#define D3PDREADER_TriggerInfoCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "TriggerInfoCollection.h"

ClassImp(D3PDReader::TriggerInfoCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  TriggerInfoCollection::TriggerInfoCollection(const long int& master,const std::string& prefix):
    TObject(),
    Nav_chain_ChainId(prefix + "Nav_chain_ChainId",&master),
    Nav_chain_RoIType(prefix + "Nav_chain_RoIType",&master),
    Nav_chain_RoIIndex(prefix + "Nav_chain_RoIIndex",&master),
    RoI_EF_mu_n(prefix + "RoI_EF_mu_n",&master),
    RoI_EF_mu_type(prefix + "RoI_EF_mu_type",&master),
    RoI_EF_mu_TrigMuonEFInfoContainer(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer",&master),
    RoI_EF_mu_TrigMuonEFInfoContainerStatus(prefix + "RoI_EF_mu_TrigMuonEFInfoContainerStatus",&master),
    RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl",&master),
    RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus",&master),
    RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF",&master),
    RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus",&master),
    RoI_EF_e_n(prefix + "RoI_EF_e_n",&master),
    RoI_EF_e_type(prefix + "RoI_EF_e_type",&master),
    RoI_EF_e_egammaContainer_egamma_Electrons(prefix + "RoI_EF_e_egammaContainer_egamma_Electrons",&master),
    RoI_EF_e_egammaContainer_egamma_ElectronsStatus(prefix + "RoI_EF_e_egammaContainer_egamma_ElectronsStatus",&master),
    DB_SMK(prefix + "DB_SMK",&master),
    DB_L1PSK(prefix + "DB_L1PSK",&master),
    DB_HLTPSK(prefix + "DB_HLTPSK",&master),
    RoI_L2_tau_n(prefix + "RoI_L2_tau_n",&master),
    RoI_L2_tau_type(prefix + "RoI_L2_tau_type",&master),
    RoI_EF_tau_n(prefix + "RoI_EF_tau_n",&master),
    RoI_EF_tau_type(prefix + "RoI_EF_tau_type",&master),
    RoI_EF_tau_Analysis__TauJetContainer(prefix + "RoI_EF_tau_Analysis::TauJetContainer",&master),
    RoI_EF_tau_Analysis__TauJetContainerStatus(prefix + "RoI_EF_tau_Analysis::TauJetContainerStatus",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  TriggerInfoCollection::TriggerInfoCollection(const std::string& prefix):
    TObject(),
    Nav_chain_ChainId(prefix + "Nav_chain_ChainId",0),
    Nav_chain_RoIType(prefix + "Nav_chain_RoIType",0),
    Nav_chain_RoIIndex(prefix + "Nav_chain_RoIIndex",0),
    RoI_EF_mu_n(prefix + "RoI_EF_mu_n",0),
    RoI_EF_mu_type(prefix + "RoI_EF_mu_type",0),
    RoI_EF_mu_TrigMuonEFInfoContainer(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer",0),
    RoI_EF_mu_TrigMuonEFInfoContainerStatus(prefix + "RoI_EF_mu_TrigMuonEFInfoContainerStatus",0),
    RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl",0),
    RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus",0),
    RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF",0),
    RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus(prefix + "RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus",0),
    RoI_EF_e_n(prefix + "RoI_EF_e_n",0),
    RoI_EF_e_type(prefix + "RoI_EF_e_type",0),
    RoI_EF_e_egammaContainer_egamma_Electrons(prefix + "RoI_EF_e_egammaContainer_egamma_Electrons",0),
    RoI_EF_e_egammaContainer_egamma_ElectronsStatus(prefix + "RoI_EF_e_egammaContainer_egamma_ElectronsStatus",0),
    DB_SMK(prefix + "DB_SMK",0),
    DB_L1PSK(prefix + "DB_L1PSK",0),
    DB_HLTPSK(prefix + "DB_HLTPSK",0),
    RoI_L2_tau_n(prefix + "RoI_L2_tau_n",0),
    RoI_L2_tau_type(prefix + "RoI_L2_tau_type",0),
    RoI_EF_tau_n(prefix + "RoI_EF_tau_n",0),
    RoI_EF_tau_type(prefix + "RoI_EF_tau_type",0),
    RoI_EF_tau_Analysis__TauJetContainer(prefix + "RoI_EF_tau_Analysis::TauJetContainer",0),
    RoI_EF_tau_Analysis__TauJetContainerStatus(prefix + "RoI_EF_tau_Analysis::TauJetContainerStatus",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void TriggerInfoCollection::ReadFrom(TTree* tree)
  {
    Nav_chain_ChainId.ReadFrom(tree);
    Nav_chain_RoIType.ReadFrom(tree);
    Nav_chain_RoIIndex.ReadFrom(tree);
    RoI_EF_mu_n.ReadFrom(tree);
    RoI_EF_mu_type.ReadFrom(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer.ReadFrom(tree);
    RoI_EF_mu_TrigMuonEFInfoContainerStatus.ReadFrom(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl.ReadFrom(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus.ReadFrom(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF.ReadFrom(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus.ReadFrom(tree);
    RoI_EF_e_n.ReadFrom(tree);
    RoI_EF_e_type.ReadFrom(tree);
    RoI_EF_e_egammaContainer_egamma_Electrons.ReadFrom(tree);
    RoI_EF_e_egammaContainer_egamma_ElectronsStatus.ReadFrom(tree);
    DB_SMK.ReadFrom(tree);
    DB_L1PSK.ReadFrom(tree);
    DB_HLTPSK.ReadFrom(tree);
    RoI_L2_tau_n.ReadFrom(tree);
    RoI_L2_tau_type.ReadFrom(tree);
    RoI_EF_tau_n.ReadFrom(tree);
    RoI_EF_tau_type.ReadFrom(tree);
    RoI_EF_tau_Analysis__TauJetContainer.ReadFrom(tree);
    RoI_EF_tau_Analysis__TauJetContainerStatus.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void TriggerInfoCollection::WriteTo(TTree* tree)
  {
    Nav_chain_ChainId.WriteTo(tree);
    Nav_chain_RoIType.WriteTo(tree);
    Nav_chain_RoIIndex.WriteTo(tree);
    RoI_EF_mu_n.WriteTo(tree);
    RoI_EF_mu_type.WriteTo(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer.WriteTo(tree);
    RoI_EF_mu_TrigMuonEFInfoContainerStatus.WriteTo(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl.WriteTo(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus.WriteTo(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF.WriteTo(tree);
    RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus.WriteTo(tree);
    RoI_EF_e_n.WriteTo(tree);
    RoI_EF_e_type.WriteTo(tree);
    RoI_EF_e_egammaContainer_egamma_Electrons.WriteTo(tree);
    RoI_EF_e_egammaContainer_egamma_ElectronsStatus.WriteTo(tree);
    DB_SMK.WriteTo(tree);
    DB_L1PSK.WriteTo(tree);
    DB_HLTPSK.WriteTo(tree);
    RoI_L2_tau_n.WriteTo(tree);
    RoI_L2_tau_type.WriteTo(tree);
    RoI_EF_tau_n.WriteTo(tree);
    RoI_EF_tau_type.WriteTo(tree);
    RoI_EF_tau_Analysis__TauJetContainer.WriteTo(tree);
    RoI_EF_tau_Analysis__TauJetContainerStatus.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void TriggerInfoCollection::SetActive(bool active)
  {
    if(active)
    {
     if(Nav_chain_ChainId.IsAvailable()) Nav_chain_ChainId.SetActive(active);
     if(Nav_chain_RoIType.IsAvailable()) Nav_chain_RoIType.SetActive(active);
     if(Nav_chain_RoIIndex.IsAvailable()) Nav_chain_RoIIndex.SetActive(active);
     if(RoI_EF_mu_n.IsAvailable()) RoI_EF_mu_n.SetActive(active);
     if(RoI_EF_mu_type.IsAvailable()) RoI_EF_mu_type.SetActive(active);
     if(RoI_EF_mu_TrigMuonEFInfoContainer.IsAvailable()) RoI_EF_mu_TrigMuonEFInfoContainer.SetActive(active);
     if(RoI_EF_mu_TrigMuonEFInfoContainerStatus.IsAvailable()) RoI_EF_mu_TrigMuonEFInfoContainerStatus.SetActive(active);
     if(RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl.IsAvailable()) RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl.SetActive(active);
     if(RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus.IsAvailable()) RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus.SetActive(active);
     if(RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF.IsAvailable()) RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF.SetActive(active);
     if(RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus.IsAvailable()) RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus.SetActive(active);
     if(RoI_EF_e_n.IsAvailable()) RoI_EF_e_n.SetActive(active);
     if(RoI_EF_e_type.IsAvailable()) RoI_EF_e_type.SetActive(active);
     if(RoI_EF_e_egammaContainer_egamma_Electrons.IsAvailable()) RoI_EF_e_egammaContainer_egamma_Electrons.SetActive(active);
     if(RoI_EF_e_egammaContainer_egamma_ElectronsStatus.IsAvailable()) RoI_EF_e_egammaContainer_egamma_ElectronsStatus.SetActive(active);
     if(DB_SMK.IsAvailable()) DB_SMK.SetActive(active);
     if(DB_L1PSK.IsAvailable()) DB_L1PSK.SetActive(active);
     if(DB_HLTPSK.IsAvailable()) DB_HLTPSK.SetActive(active);
     if(RoI_L2_tau_n.IsAvailable()) RoI_L2_tau_n.SetActive(active);
     if(RoI_L2_tau_type.IsAvailable()) RoI_L2_tau_type.SetActive(active);
     if(RoI_EF_tau_n.IsAvailable()) RoI_EF_tau_n.SetActive(active);
     if(RoI_EF_tau_type.IsAvailable()) RoI_EF_tau_type.SetActive(active);
     if(RoI_EF_tau_Analysis__TauJetContainer.IsAvailable()) RoI_EF_tau_Analysis__TauJetContainer.SetActive(active);
     if(RoI_EF_tau_Analysis__TauJetContainerStatus.IsAvailable()) RoI_EF_tau_Analysis__TauJetContainerStatus.SetActive(active);
    }
    else
    {
      Nav_chain_ChainId.SetActive(active);
      Nav_chain_RoIType.SetActive(active);
      Nav_chain_RoIIndex.SetActive(active);
      RoI_EF_mu_n.SetActive(active);
      RoI_EF_mu_type.SetActive(active);
      RoI_EF_mu_TrigMuonEFInfoContainer.SetActive(active);
      RoI_EF_mu_TrigMuonEFInfoContainerStatus.SetActive(active);
      RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl.SetActive(active);
      RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus.SetActive(active);
      RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF.SetActive(active);
      RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus.SetActive(active);
      RoI_EF_e_n.SetActive(active);
      RoI_EF_e_type.SetActive(active);
      RoI_EF_e_egammaContainer_egamma_Electrons.SetActive(active);
      RoI_EF_e_egammaContainer_egamma_ElectronsStatus.SetActive(active);
      DB_SMK.SetActive(active);
      DB_L1PSK.SetActive(active);
      DB_HLTPSK.SetActive(active);
      RoI_L2_tau_n.SetActive(active);
      RoI_L2_tau_type.SetActive(active);
      RoI_EF_tau_n.SetActive(active);
      RoI_EF_tau_type.SetActive(active);
      RoI_EF_tau_Analysis__TauJetContainer.SetActive(active);
      RoI_EF_tau_Analysis__TauJetContainerStatus.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void TriggerInfoCollection::ReadAllActive()
  {
    if(Nav_chain_ChainId.IsActive()) Nav_chain_ChainId();
    if(Nav_chain_RoIType.IsActive()) Nav_chain_RoIType();
    if(Nav_chain_RoIIndex.IsActive()) Nav_chain_RoIIndex();
    if(RoI_EF_mu_n.IsActive()) RoI_EF_mu_n();
    if(RoI_EF_mu_type.IsActive()) RoI_EF_mu_type();
    if(RoI_EF_mu_TrigMuonEFInfoContainer.IsActive()) RoI_EF_mu_TrigMuonEFInfoContainer();
    if(RoI_EF_mu_TrigMuonEFInfoContainerStatus.IsActive()) RoI_EF_mu_TrigMuonEFInfoContainerStatus();
    if(RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl.IsActive()) RoI_EF_mu_TrigMuonEFInfoContainer_MuGirl();
    if(RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus.IsActive()) RoI_EF_mu_TrigMuonEFInfoContainer_MuGirlStatus();
    if(RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF.IsActive()) RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EF();
    if(RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus.IsActive()) RoI_EF_mu_TrigMuonEFInfoContainer_MuTagIMO_EFStatus();
    if(RoI_EF_e_n.IsActive()) RoI_EF_e_n();
    if(RoI_EF_e_type.IsActive()) RoI_EF_e_type();
    if(RoI_EF_e_egammaContainer_egamma_Electrons.IsActive()) RoI_EF_e_egammaContainer_egamma_Electrons();
    if(RoI_EF_e_egammaContainer_egamma_ElectronsStatus.IsActive()) RoI_EF_e_egammaContainer_egamma_ElectronsStatus();
    if(DB_SMK.IsActive()) DB_SMK();
    if(DB_L1PSK.IsActive()) DB_L1PSK();
    if(DB_HLTPSK.IsActive()) DB_HLTPSK();
    if(RoI_L2_tau_n.IsActive()) RoI_L2_tau_n();
    if(RoI_L2_tau_type.IsActive()) RoI_L2_tau_type();
    if(RoI_EF_tau_n.IsActive()) RoI_EF_tau_n();
    if(RoI_EF_tau_type.IsActive()) RoI_EF_tau_type();
    if(RoI_EF_tau_Analysis__TauJetContainer.IsActive()) RoI_EF_tau_Analysis__TauJetContainer();
    if(RoI_EF_tau_Analysis__TauJetContainerStatus.IsActive()) RoI_EF_tau_Analysis__TauJetContainerStatus();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TriggerInfoCollection_CXX
