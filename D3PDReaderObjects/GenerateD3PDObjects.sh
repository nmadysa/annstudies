#!/bin/bash

# what should be generated
if [ $# -eq 0 ]
then
  WHAT=ALL
else
  WHAT="$1"
fi

# settings
OUT_DIR=temp
TREE_NAME=tau
ROOT_FILE=/ZIH.fast/users/hanisch/d3pd/user.mhodgkin.TauPi0Rec_D3PD.147818.Pythia8_AU2CTEQ6L1_Ztautau.recon.ESD.e1176_s1479_s1470_r3553_tid00999074_00.v06-00.140114115305/user.mhodgkin.006945.TauPERF._01504.root

#/ZIH.fast/users/hanisch/d3pd/group.perf-tau.TauPi0Rec_D3PD.periodA.physics_JetTauEtmiss.PhysCont.DESD_CALJET.repro14_v01.v05-01.131008014345/group.perf-tau.397105_002055.TauPERF._00418.root
TRIG_CONF_TREE_NAME=tauMeta/TrigConfTree

# general event infos
if [[ "$WHAT" == "EventInfo" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n EventInfo -v EventNumber RunNumber lbn trig_EF_passedPhysics trig_L2_passedPhysics EF_j20_a4tc_EFFS mcevt_weight averageIntPerXing actualIntPerXing
fi

# MET
if [[ "$WHAT" == "MET" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n MET -v ^MET_ -p MET_
fi

# Electrons
if [[ "$WHAT" == "Electron" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n ElectronD3PDCollection -v ^el_ -p el_ -c -e Electron
fi

# Electrons
if [[ "$WHAT" == "MC" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n MCParticleD3PDCollection -v ^mc_ -p mc_ -c -e MCParticle
fi

# Muons
if [[ "$WHAT" == "Muon" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n MuonD3PDCollection -v ^mu_staco_ -p mu_staco_ -c -e Muon
fi

# Truth Muons
if [[ "$WHAT" == "TruthMuon" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n TruthMuonD3PDCollection -v ^muonTruth_ -p muonTruth_ -c -e TruthMuon
fi

# Jets
if [[ "$WHAT" == "Jet" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker --particle -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n JetD3PDCollection -v "^jet_(?i)(?!anti)" -p jet_ -c -e Jet
fi

# Taus
if [[ "$WHAT" == "Tau" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker --particle -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n TauD3PDCollection -v ^tau_ -p tau_ -c -e Tau
fi

# Taus
if [[ "$WHAT" == "TruthTau" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n TruthTauD3PDCollection -v ^trueTau_ -p trueTau_ -c -e TruthTau
fi

#EF trig electrons
if [[ "$WHAT" == "TrigEFElectron" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n TrigEFElectronD3PDCollection -v ^trig_EF_el_ -p trig_EF_el_ -c -e TrigEFElectron
fi

#EF trig muons
if [[ "$WHAT" == "TrigEFMuon" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n TrigEFMuonD3PDCollection -v ^trig_EF_trigmuonef_ -p trig_EF_trigmuonef_ -c -e TrigEFMuon
fi

# L2 trig Taus
if [[ "$WHAT" == "TrigL2Tau" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker --particle -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n TrigL2TauD3PDCollection -v ^trig_L2_tau_ -p trig_L2_tau_ -c -e TrigL2Tau
fi

# EF trig Taus
if [[ "$WHAT" == "TrigEFTau" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker --particle -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n TrigEFTauD3PDCollection -v ^trig_EF_tau_ -p trig_EF_tau_ -c -e TrigEFTau
fi

# L1 trig Taus
if [[ "$WHAT" == "TrigL1Tau" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n TrigL1TauD3PDCollection -v '^trig_L1_emtau_[^L1]' -p trig_L1_emtau_ -c -e TrigL1Tau
fi

#Vertices
if [[ "$WHAT" == "Vertex" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n VertexD3PDCollection -v ^vxp_ -p vxp_ -c -e Vertex
fi

#TriggerInfoCollection
if [[ "$WHAT" == "TriggerInfoCollection" || "$WHAT" == "ALL" ]]
then
  TRIGGER_BRANCHES='^trig_DB_ ^trig_Nav_chain_(ChainId|RoI(Index|Type)) ^trig_RoI_(EF|L2)_(e|mu|tau)_(n|type) ^trig_RoI_EF_e_egammaContainer_egamma_(Electrons|ElectronsStatus) ^trig_RoI_EF_mu_TrigMuonEFInfo(Container|ContainerStatus) ^trig_RoI_EF_tau_Analysis::TauJet(Container|ContainerStatus) ^trig_RoI_L2_tau_TrigTau ^trig_RoI_L2_tau_TrigTauStatus'
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n TriggerInfoCollection -v $TRIGGER_BRANCHES -p trig_
fi

#TriggerMetadataCollection
if [[ "$WHAT" == "TriggerMetadataCollection" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TRIG_CONF_TREE_NAME -f $ROOT_FILE -n TriggerMetadataCollection
fi
