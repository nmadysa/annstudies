#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;

#pragma link C++ class D3PDReader::AnalysisEventInfo+;
#pragma link C++ class D3PDReader::AnalysisTau+;
#pragma link C++ class D3PDReader::AnalysisTauD3PDCollection+;

#pragma link C++ class D3PDReader::LLHTau+;

#pragma link C++ class std::vector<std::vector<int> >+;
#pragma link C++ class std::vector<std::vector<unsigned int> >+;
#pragma link C++ class std::vector<std::vector<long> >+;
#pragma link C++ class std::vector<std::vector<unsigned long> >+;
#pragma link C++ class std::vector<std::vector<float> >+;
#pragma link C++ class std::vector<std::vector<double> >+;
#pragma link C++ class std::vector<std::string>+;
#pragma link C++ class std::vector<std::vector<std::string> >+;

#pragma link C++ class D3PDReader::VarHandle<float>+;
#pragma link C++ class D3PDReader::VarHandle<int>+;
#pragma link C++ class D3PDReader::VarHandle<vector<float>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<int>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<vector<float> >*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<vector<int> >*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<vector<double> >*>+;
#pragma link C++ class D3PDReader::VarHandle<unsigned int>+;
#pragma link C++ class D3PDReader::VarHandle<vector<short>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<unsigned int>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<vector<unsigned int> >*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<unsigned short>*>+;
#pragma link C++ class D3PDReader::VarHandle<vector<double>*>+;

#endif // __CINT__
