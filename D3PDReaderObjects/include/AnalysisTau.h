#ifndef SKIM
#ifndef D3PDREADER_AnalysisTau_H
#define D3PDREADER_AnalysisTau_H

#include <iostream>
#include "Tau.h"
#include "AnalysisTauD3PDCollection.h"
#include "IDVarSet.h"

namespace D3PDReader
{
  class AnalysisTau : public Tau{

  public:
    AnalysisTau(unsigned int index,AnalysisTauD3PDCollection& parent) :
      Tau(index, parent),
      m_pParent(&parent),
      m_iIndex(index)
    {
      m_pTau = new D3PDReader::Tau(index, parent);
    }

    AnalysisTauD3PDCollection* m_pParent;
    int m_iIndex;
    D3PDReader::Tau* m_pTau;

    const int& hasTruthMatch() const {return m_pParent->hasTruthMatch()->at(m_iIndex);}
    const int& matchNProng() const {return m_pParent->matchNProng()->at(m_iIndex);}
    const float& matchPt() const {return m_pParent->matchPt()->at(m_iIndex);}
    const float& matchVisEt() const {return m_pParent->matchVisEt()->at(m_iIndex);}
    const float& ptRatio() const {return m_pParent->ptRatio()->at(m_iIndex);}
    const float& matchEta() const {return m_pParent->matchEta()->at(m_iIndex);}
    const float& matchPhi() const {return m_pParent->matchPhi()->at(m_iIndex);}
    const float& EtOverPtLeadTrk() const {return m_pParent->EtOverPtLeadTrk()->at(m_iIndex);}
    const float& DefaultLLHScore() const {return m_pParent->DefaultLLHScore()->at(m_iIndex);}
    const float& BDTScore() const {return m_pParent->BDTScore()->at(m_iIndex);}
    
    const void setIDVarSet(IDVarSet idVarSet){
      this->m_IDVarSet = idVarSet;
    	}
    const IDVarSet& getIDVarSet() const{
      return this->m_IDVarSet;
    }

  private:
    IDVarSet m_IDVarSet;
    
    ClassDef(AnalysisTau, 0);
  };
} //namespace D3PDReader
#endif
#endif
