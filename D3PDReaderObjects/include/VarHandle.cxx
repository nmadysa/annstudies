#ifndef D3PDREADER_VARHANDLE_CXX
#define D3PDREADER_VARHANDLE_CXX

// System include(s):
#include <iostream>
#include <string.h>
#include <assert.h>
// ROOT include(s):
#include <TTree.h>
#include <TBranch.h>

// custom include(s):
#include "VarHandle.h"

namespace D3PDReader
{
  VarHandleBase::VarHandleBase(const std::string& sName,const long int* const pMaster):
    m_pMaster(pMaster),
    m_sName(sName),
    m_bFromInput(false),
    m_pInTree(0),
    m_pInBranch(0),
#ifdef WRITE_BRANCHES_USED
    m_bWriteInfo(true),
#endif // WRITE_BRANCHES_USED
    m_bIsActive(false),
    m_eAvailable(UNKNOWN)
  {}

  void VarHandleBase::ReadFrom(TTree* pTree)
  {
    assert(m_pMaster);
    assert(pTree);
    m_pInTree = pTree;
    m_bFromInput = true;
    m_pInBranch = 0;
    m_eAvailable = UNKNOWN;

  }

  bool VarHandleBase::IsAvailable() const
  {
    if(!m_bFromInput)
      return true;
    switch(m_eAvailable)
    {
    case AVAILABLE:
      return true;
      break;
    case UNAVAILABLE:
      return false;
      break;
    case UNKNOWN:
      {
        bool temp = false;
        m_eAvailable = (temp = m_pInTree->GetBranch(m_sName.c_str())) ? AVAILABLE : UNAVAILABLE;
        return temp;
      }
      break;
    default:
      // This should really never be reached...
      break;
    }

    // It's just here to make the compiler happy:
    return false;
  }

  void VarHandleBase::SetActive(bool active)
  {
    m_bIsActive = active;
  }
} // namespace D3PDReader

#endif // D3PDREADER_VARHANDLE_CXX

