#ifndef SKIM
// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------
#ifndef D3PDREADER_AnalysisEventInfo_H
#define D3PDREADER_AnalysisEventInfo_H

// STL include(s)
#include <vector>
using std::vector;

// ROOT include(s)
#include <TObject.h>
#include <TString.h>

// custom include(s)
#include "EventInfo.h"

class TTree;

namespace D3PDReader
{
  /**
   * Code generated by CodeGenerator on:
   * time = Mon Feb 11 20:39:23 2013
   */
  class AnalysisEventInfo : public EventInfo
  {
  public:
    /// Constructor used when reading from a TTree
    AnalysisEventInfo(const long int& master,const std::string& prefix = "");
    /// Constructor when the object is only used for writing data out
    AnalysisEventInfo(const std::string& prefix = "");
    /// standard destructor
    ~AnalysisEventInfo() {}
    /// Get the currently configured prefix value
    const std::string& GetPrefix() const {return m_sPrefix;}
    /// Connect the object to an input TTree
    void ReadFrom(TTree* tree);
    /// Connect the object to an output TTree
    void WriteTo(TTree* tree);
    /// Turn all (available) branches either on or off
    void SetActive(bool active = true);
    /// Read in all the variables that we need to write out as well
    void ReadAllActive();

    VarHandle<Float_t> mu;
    VarHandle<Int_t> nVtx;

  private:
    const std::string m_sPrefix; ///< common prefix to the branch names

    ClassDef(AnalysisEventInfo,0)
  }; // class AnalysisEventInfo

} // namespace D3PDReader

#endif // D3PDREADER_AnalysisEventInfo_H
#endif //SKIM
