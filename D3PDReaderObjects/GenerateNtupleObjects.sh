#!/bin/bash

# what should be generated
if [ $# -eq 0 ]
then
  WHAT=ALL
else
  WHAT="$1"
fi

# settings
OUT_DIR=temp
TREE_NAME=MiniTree
ROOT_FILE=/home/hanisch/workarea/SubstructureID/OfflineLLH/out/ntuple/current_result/NtupleCycle.mc.Ztautau.Pileup.Weights.root

# general event infos
if [[ "$WHAT" == "LLHTau" || "$WHAT" == "ALL" ]]
then
  ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n LLHTau
fi

# if [[ "$WHAT" == "LLHTrigEFTau" || "$WHAT" == "ALL" ]]
# then
#   ./D3PDReaderMaker -o $OUT_DIR -t $TREE_NAME -f $ROOT_FILE -n LLHTrigEFTau
#fi
