// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_LLHTau_CXX
#define D3PDREADER_LLHTau_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "LLHTau.h"

ClassImp(D3PDReader::LLHTau)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  LLHTau::LLHTau(const long int& master,const std::string& prefix):
    TObject(),
    RunNumber(prefix + "RunNumber",&master),
    EventNumber(prefix + "EventNumber",&master),
    pt(prefix + "pt",&master),
    Et(prefix + "Et",&master),
    eta(prefix + "eta",&master),
    phi(prefix + "phi",&master),
    NumTrack(prefix + "NumTrack",&master),
    DefaultLLHScore(prefix + "DefaultLLHScore",&master),
    DefaultLLHLoose(prefix + "DefaultLLHLoose",&master),
    DefaultLLHMedium(prefix + "DefaultLLHMedium",&master),
    DefaultLLHTight(prefix + "DefaultLLHTight",&master),
    TruthPt(prefix + "TruthPt",&master),
    TruthEta(prefix + "TruthEta",&master),
    TruthPhi(prefix + "TruthPhi",&master),
    TruthNumTrack(prefix + "TruthNumTrack",&master),
    BDTScore(prefix + "BDTScore",&master),
    DefaultBDTLoose(prefix + "DefaultBDTLoose",&master),
    DefaultBDTMedium(prefix + "DefaultBDTMedium",&master),
    DefaultBDTTight(prefix + "DefaultBDTTight",&master),
    NVertices(prefix + "NVertices",&master),
    NumGoodVertices(prefix + "NumGoodVertices",&master),
    mu(prefix + "mu",&master),
    matchVisEt(prefix + "matchVisEt",&master),
    CentFrac0102(prefix + "CentFrac0102",&master),
    Var_CentFrac0102(prefix + "Var_CentFrac0102",&master),
    DrMax(prefix + "DrMax",&master),
    Var_DrMax(prefix + "Var_DrMax",&master),
    FTrk02(prefix + "FTrk02",&master),
    Var_FTrk02(prefix + "Var_FTrk02",&master),
    IpSigLeadTrk(prefix + "IpSigLeadTrk",&master),
    Var_IpSigLeadTrk(prefix + "Var_IpSigLeadTrk",&master),
    MassTrkSys(prefix + "MassTrkSys",&master),
    Var_MassTrkSys(prefix + "Var_MassTrkSys",&master),
    NTracksdrdR(prefix + "NTracksdrdR",&master),
    Var_NTracksdrdR(prefix + "Var_NTracksdrdR",&master),
    TrFligthPathSig(prefix + "TrFligthPathSig",&master),
    Var_TrFligthPathSig(prefix + "Var_TrFligthPathSig",&master),
    TrkAvgDist(prefix + "TrkAvgDist",&master),
    Var_TrkAvgDist(prefix + "Var_TrkAvgDist",&master),
    panTauCellBased_dRmax02(prefix + "panTauCellBased_dRmax02",&master),
    Var_panTauCellBased_dRmax02(prefix + "Var_panTauCellBased_dRmax02",&master),
    panTauCellBased_dRmax04(prefix + "panTauCellBased_dRmax04",&master),
    Var_panTauCellBased_dRmax04(prefix + "Var_panTauCellBased_dRmax04",&master),
    panTauCellBased_dRminmaxPtChrg04(prefix + "panTauCellBased_dRminmaxPtChrg04",&master),
    Var_panTauCellBased_dRminmaxPtChrg04(prefix + "Var_panTauCellBased_dRminmaxPtChrg04",&master),
    panTauCellBased_eFrac0102(prefix + "panTauCellBased_eFrac0102",&master),
    Var_panTauCellBased_eFrac0102(prefix + "Var_panTauCellBased_eFrac0102",&master),
    panTauCellBased_eFrac0104(prefix + "panTauCellBased_eFrac0104",&master),
    Var_panTauCellBased_eFrac0104(prefix + "Var_panTauCellBased_eFrac0104",&master),
    panTauCellBased_eFrac0204(prefix + "panTauCellBased_eFrac0204",&master),
    Var_panTauCellBased_eFrac0204(prefix + "Var_panTauCellBased_eFrac0204",&master),
    panTauCellBased_eFracChrg0104(prefix + "panTauCellBased_eFracChrg0104",&master),
    Var_panTauCellBased_eFracChrg0104(prefix + "Var_panTauCellBased_eFracChrg0104",&master),
    panTauCellBased_eFracChrg0204(prefix + "panTauCellBased_eFracChrg0204",&master),
    Var_panTauCellBased_eFracChrg0204(prefix + "Var_panTauCellBased_eFracChrg0204",&master),
    panTauCellBased_eFracNeut0104(prefix + "panTauCellBased_eFracNeut0104",&master),
    Var_panTauCellBased_eFracNeut0104(prefix + "Var_panTauCellBased_eFracNeut0104",&master),
    panTauCellBased_eFracNeut0204(prefix + "panTauCellBased_eFracNeut0204",&master),
    Var_panTauCellBased_eFracNeut0204(prefix + "Var_panTauCellBased_eFracNeut0204",&master),
    panTauCellBased_fLeadChrg01(prefix + "panTauCellBased_fLeadChrg01",&master),
    Var_panTauCellBased_fLeadChrg01(prefix + "Var_panTauCellBased_fLeadChrg01",&master),
    panTauCellBased_fLeadChrg02(prefix + "panTauCellBased_fLeadChrg02",&master),
    Var_panTauCellBased_fLeadChrg02(prefix + "Var_panTauCellBased_fLeadChrg02",&master),
    panTauCellBased_fLeadChrg04(prefix + "panTauCellBased_fLeadChrg04",&master),
    Var_panTauCellBased_fLeadChrg04(prefix + "Var_panTauCellBased_fLeadChrg04",&master),
    panTauCellBased_ipSigLeadTrk(prefix + "panTauCellBased_ipSigLeadTrk",&master),
    Var_panTauCellBased_ipSigLeadTrk(prefix + "Var_panTauCellBased_ipSigLeadTrk",&master),
    panTauCellBased_massChrgSys01(prefix + "panTauCellBased_massChrgSys01",&master),
    Var_panTauCellBased_massChrgSys01(prefix + "Var_panTauCellBased_massChrgSys01",&master),
    panTauCellBased_massChrgSys02(prefix + "panTauCellBased_massChrgSys02",&master),
    Var_panTauCellBased_massChrgSys02(prefix + "Var_panTauCellBased_massChrgSys02",&master),
    panTauCellBased_massChrgSys04(prefix + "panTauCellBased_massChrgSys04",&master),
    Var_panTauCellBased_massChrgSys04(prefix + "Var_panTauCellBased_massChrgSys04",&master),
    panTauCellBased_massNeutSys01(prefix + "panTauCellBased_massNeutSys01",&master),
    Var_panTauCellBased_massNeutSys01(prefix + "Var_panTauCellBased_massNeutSys01",&master),
    panTauCellBased_massNeutSys02(prefix + "panTauCellBased_massNeutSys02",&master),
    Var_panTauCellBased_massNeutSys02(prefix + "Var_panTauCellBased_massNeutSys02",&master),
    panTauCellBased_massNeutSys04(prefix + "panTauCellBased_massNeutSys04",&master),
    Var_panTauCellBased_massNeutSys04(prefix + "Var_panTauCellBased_massNeutSys04",&master),
    panTauCellBased_nChrg01(prefix + "panTauCellBased_nChrg01",&master),
    Var_panTauCellBased_nChrg01(prefix + "Var_panTauCellBased_nChrg01",&master),
    panTauCellBased_nChrg02(prefix + "panTauCellBased_nChrg02",&master),
    Var_panTauCellBased_nChrg02(prefix + "Var_panTauCellBased_nChrg02",&master),
    panTauCellBased_nChrg0204(prefix + "panTauCellBased_nChrg0204",&master),
    Var_panTauCellBased_nChrg0204(prefix + "Var_panTauCellBased_nChrg0204",&master),
    panTauCellBased_nNeut01(prefix + "panTauCellBased_nNeut01",&master),
    Var_panTauCellBased_nNeut01(prefix + "Var_panTauCellBased_nNeut01",&master),
    panTauCellBased_nNeut02(prefix + "panTauCellBased_nNeut02",&master),
    Var_panTauCellBased_nNeut02(prefix + "Var_panTauCellBased_nNeut02",&master),
    panTauCellBased_nNeut0204(prefix + "panTauCellBased_nNeut0204",&master),
    Var_panTauCellBased_nNeut0204(prefix + "Var_panTauCellBased_nNeut0204",&master),
    panTauCellBased_ptRatio01(prefix + "panTauCellBased_ptRatio01",&master),
    Var_panTauCellBased_ptRatio01(prefix + "Var_panTauCellBased_ptRatio01",&master),
    panTauCellBased_ptRatio02(prefix + "panTauCellBased_ptRatio02",&master),
    Var_panTauCellBased_ptRatio02(prefix + "Var_panTauCellBased_ptRatio02",&master),
    panTauCellBased_ptRatio04(prefix + "panTauCellBased_ptRatio04",&master),
    Var_panTauCellBased_ptRatio04(prefix + "Var_panTauCellBased_ptRatio04",&master),
    panTauCellBased_ptRatioChrg01(prefix + "panTauCellBased_ptRatioChrg01",&master),
    Var_panTauCellBased_ptRatioChrg01(prefix + "Var_panTauCellBased_ptRatioChrg01",&master),
    panTauCellBased_ptRatioChrg02(prefix + "panTauCellBased_ptRatioChrg02",&master),
    Var_panTauCellBased_ptRatioChrg02(prefix + "Var_panTauCellBased_ptRatioChrg02",&master),
    panTauCellBased_ptRatioChrg04(prefix + "panTauCellBased_ptRatioChrg04",&master),
    Var_panTauCellBased_ptRatioChrg04(prefix + "Var_panTauCellBased_ptRatioChrg04",&master),
    panTauCellBased_ptRatioNeut01(prefix + "panTauCellBased_ptRatioNeut01",&master),
    Var_panTauCellBased_ptRatioNeut01(prefix + "Var_panTauCellBased_ptRatioNeut01",&master),
    panTauCellBased_ptRatioNeut02(prefix + "panTauCellBased_ptRatioNeut02",&master),
    Var_panTauCellBased_ptRatioNeut02(prefix + "Var_panTauCellBased_ptRatioNeut02",&master),
    panTauCellBased_ptRatioNeut04(prefix + "panTauCellBased_ptRatioNeut04",&master),
    Var_panTauCellBased_ptRatioNeut04(prefix + "Var_panTauCellBased_ptRatioNeut04",&master),
    panTauCellBased_rCal02(prefix + "panTauCellBased_rCal02",&master),
    Var_panTauCellBased_rCal02(prefix + "Var_panTauCellBased_rCal02",&master),
    panTauCellBased_rCal04(prefix + "panTauCellBased_rCal04",&master),
    Var_panTauCellBased_rCal04(prefix + "Var_panTauCellBased_rCal04",&master),
    panTauCellBased_rCalChrg02(prefix + "panTauCellBased_rCalChrg02",&master),
    Var_panTauCellBased_rCalChrg02(prefix + "Var_panTauCellBased_rCalChrg02",&master),
    panTauCellBased_rCalChrg04(prefix + "panTauCellBased_rCalChrg04",&master),
    Var_panTauCellBased_rCalChrg04(prefix + "Var_panTauCellBased_rCalChrg04",&master),
    panTauCellBased_rCalNeut02(prefix + "panTauCellBased_rCalNeut02",&master),
    Var_panTauCellBased_rCalNeut02(prefix + "Var_panTauCellBased_rCalNeut02",&master),
    panTauCellBased_rCalNeut04(prefix + "panTauCellBased_rCalNeut04",&master),
    Var_panTauCellBased_rCalNeut04(prefix + "Var_panTauCellBased_rCalNeut04",&master),
    panTauCellBased_trFlightPathSig(prefix + "panTauCellBased_trFlightPathSig",&master),
    Var_panTauCellBased_trFlightPathSig(prefix + "Var_panTauCellBased_trFlightPathSig",&master),
    panTauCellBased_visTauM01(prefix + "panTauCellBased_visTauM01",&master),
    Var_panTauCellBased_visTauM01(prefix + "Var_panTauCellBased_visTauM01",&master),
    panTauCellBased_visTauM02(prefix + "panTauCellBased_visTauM02",&master),
    Var_panTauCellBased_visTauM02(prefix + "Var_panTauCellBased_visTauM02",&master),
    panTauCellBased_visTauM04(prefix + "panTauCellBased_visTauM04",&master),
    Var_panTauCellBased_visTauM04(prefix + "Var_panTauCellBased_visTauM04",&master),
    pi0_n(prefix + "pi0_n",&master),
    Var_pi0_n(prefix + "Var_pi0_n",&master),
    pi0_vistau_m(prefix + "pi0_vistau_m",&master),
    Var_pi0_vistau_m(prefix + "Var_pi0_vistau_m",&master),
    ptRatio(prefix + "ptRatio",&master),
    Var_ptRatio(prefix + "Var_ptRatio",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  LLHTau::LLHTau(const std::string& prefix):
    TObject(),
    RunNumber(prefix + "RunNumber",0),
    EventNumber(prefix + "EventNumber",0),
    pt(prefix + "pt",0),
    Et(prefix + "Et",0),
    eta(prefix + "eta",0),
    phi(prefix + "phi",0),
    NumTrack(prefix + "NumTrack",0),
    DefaultLLHScore(prefix + "DefaultLLHScore",0),
    DefaultLLHLoose(prefix + "DefaultLLHLoose",0),
    DefaultLLHMedium(prefix + "DefaultLLHMedium",0),
    DefaultLLHTight(prefix + "DefaultLLHTight",0),
    TruthPt(prefix + "TruthPt",0),
    TruthEta(prefix + "TruthEta",0),
    TruthPhi(prefix + "TruthPhi",0),
    TruthNumTrack(prefix + "TruthNumTrack",0),
    BDTScore(prefix + "BDTScore",0),
    DefaultBDTLoose(prefix + "DefaultBDTLoose",0),
    DefaultBDTMedium(prefix + "DefaultBDTMedium",0),
    DefaultBDTTight(prefix + "DefaultBDTTight",0),
    NVertices(prefix + "NVertices",0),
    NumGoodVertices(prefix + "NumGoodVertices",0),
    mu(prefix + "mu",0),
    matchVisEt(prefix + "matchVisEt",0),
    CentFrac0102(prefix + "CentFrac0102",0),
    Var_CentFrac0102(prefix + "Var_CentFrac0102",0),
    DrMax(prefix + "DrMax",0),
    Var_DrMax(prefix + "Var_DrMax",0),
    FTrk02(prefix + "FTrk02",0),
    Var_FTrk02(prefix + "Var_FTrk02",0),
    IpSigLeadTrk(prefix + "IpSigLeadTrk",0),
    Var_IpSigLeadTrk(prefix + "Var_IpSigLeadTrk",0),
    MassTrkSys(prefix + "MassTrkSys",0),
    Var_MassTrkSys(prefix + "Var_MassTrkSys",0),
    NTracksdrdR(prefix + "NTracksdrdR",0),
    Var_NTracksdrdR(prefix + "Var_NTracksdrdR",0),
    TrFligthPathSig(prefix + "TrFligthPathSig",0),
    Var_TrFligthPathSig(prefix + "Var_TrFligthPathSig",0),
    TrkAvgDist(prefix + "TrkAvgDist",0),
    Var_TrkAvgDist(prefix + "Var_TrkAvgDist",0),
    panTauCellBased_dRmax02(prefix + "panTauCellBased_dRmax02",0),
    Var_panTauCellBased_dRmax02(prefix + "Var_panTauCellBased_dRmax02",0),
    panTauCellBased_dRmax04(prefix + "panTauCellBased_dRmax04",0),
    Var_panTauCellBased_dRmax04(prefix + "Var_panTauCellBased_dRmax04",0),
    panTauCellBased_dRminmaxPtChrg04(prefix + "panTauCellBased_dRminmaxPtChrg04",0),
    Var_panTauCellBased_dRminmaxPtChrg04(prefix + "Var_panTauCellBased_dRminmaxPtChrg04",0),
    panTauCellBased_eFrac0102(prefix + "panTauCellBased_eFrac0102",0),
    Var_panTauCellBased_eFrac0102(prefix + "Var_panTauCellBased_eFrac0102",0),
    panTauCellBased_eFrac0104(prefix + "panTauCellBased_eFrac0104",0),
    Var_panTauCellBased_eFrac0104(prefix + "Var_panTauCellBased_eFrac0104",0),
    panTauCellBased_eFrac0204(prefix + "panTauCellBased_eFrac0204",0),
    Var_panTauCellBased_eFrac0204(prefix + "Var_panTauCellBased_eFrac0204",0),
    panTauCellBased_eFracChrg0104(prefix + "panTauCellBased_eFracChrg0104",0),
    Var_panTauCellBased_eFracChrg0104(prefix + "Var_panTauCellBased_eFracChrg0104",0),
    panTauCellBased_eFracChrg0204(prefix + "panTauCellBased_eFracChrg0204",0),
    Var_panTauCellBased_eFracChrg0204(prefix + "Var_panTauCellBased_eFracChrg0204",0),
    panTauCellBased_eFracNeut0104(prefix + "panTauCellBased_eFracNeut0104",0),
    Var_panTauCellBased_eFracNeut0104(prefix + "Var_panTauCellBased_eFracNeut0104",0),
    panTauCellBased_eFracNeut0204(prefix + "panTauCellBased_eFracNeut0204",0),
    Var_panTauCellBased_eFracNeut0204(prefix + "Var_panTauCellBased_eFracNeut0204",0),
    panTauCellBased_fLeadChrg01(prefix + "panTauCellBased_fLeadChrg01",0),
    Var_panTauCellBased_fLeadChrg01(prefix + "Var_panTauCellBased_fLeadChrg01",0),
    panTauCellBased_fLeadChrg02(prefix + "panTauCellBased_fLeadChrg02",0),
    Var_panTauCellBased_fLeadChrg02(prefix + "Var_panTauCellBased_fLeadChrg02",0),
    panTauCellBased_fLeadChrg04(prefix + "panTauCellBased_fLeadChrg04",0),
    Var_panTauCellBased_fLeadChrg04(prefix + "Var_panTauCellBased_fLeadChrg04",0),
    panTauCellBased_ipSigLeadTrk(prefix + "panTauCellBased_ipSigLeadTrk",0),
    Var_panTauCellBased_ipSigLeadTrk(prefix + "Var_panTauCellBased_ipSigLeadTrk",0),
    panTauCellBased_massChrgSys01(prefix + "panTauCellBased_massChrgSys01",0),
    Var_panTauCellBased_massChrgSys01(prefix + "Var_panTauCellBased_massChrgSys01",0),
    panTauCellBased_massChrgSys02(prefix + "panTauCellBased_massChrgSys02",0),
    Var_panTauCellBased_massChrgSys02(prefix + "Var_panTauCellBased_massChrgSys02",0),
    panTauCellBased_massChrgSys04(prefix + "panTauCellBased_massChrgSys04",0),
    Var_panTauCellBased_massChrgSys04(prefix + "Var_panTauCellBased_massChrgSys04",0),
    panTauCellBased_massNeutSys01(prefix + "panTauCellBased_massNeutSys01",0),
    Var_panTauCellBased_massNeutSys01(prefix + "Var_panTauCellBased_massNeutSys01",0),
    panTauCellBased_massNeutSys02(prefix + "panTauCellBased_massNeutSys02",0),
    Var_panTauCellBased_massNeutSys02(prefix + "Var_panTauCellBased_massNeutSys02",0),
    panTauCellBased_massNeutSys04(prefix + "panTauCellBased_massNeutSys04",0),
    Var_panTauCellBased_massNeutSys04(prefix + "Var_panTauCellBased_massNeutSys04",0),
    panTauCellBased_nChrg01(prefix + "panTauCellBased_nChrg01",0),
    Var_panTauCellBased_nChrg01(prefix + "Var_panTauCellBased_nChrg01",0),
    panTauCellBased_nChrg02(prefix + "panTauCellBased_nChrg02",0),
    Var_panTauCellBased_nChrg02(prefix + "Var_panTauCellBased_nChrg02",0),
    panTauCellBased_nChrg0204(prefix + "panTauCellBased_nChrg0204",0),
    Var_panTauCellBased_nChrg0204(prefix + "Var_panTauCellBased_nChrg0204",0),
    panTauCellBased_nNeut01(prefix + "panTauCellBased_nNeut01",0),
    Var_panTauCellBased_nNeut01(prefix + "Var_panTauCellBased_nNeut01",0),
    panTauCellBased_nNeut02(prefix + "panTauCellBased_nNeut02",0),
    Var_panTauCellBased_nNeut02(prefix + "Var_panTauCellBased_nNeut02",0),
    panTauCellBased_nNeut0204(prefix + "panTauCellBased_nNeut0204",0),
    Var_panTauCellBased_nNeut0204(prefix + "Var_panTauCellBased_nNeut0204",0),
    panTauCellBased_ptRatio01(prefix + "panTauCellBased_ptRatio01",0),
    Var_panTauCellBased_ptRatio01(prefix + "Var_panTauCellBased_ptRatio01",0),
    panTauCellBased_ptRatio02(prefix + "panTauCellBased_ptRatio02",0),
    Var_panTauCellBased_ptRatio02(prefix + "Var_panTauCellBased_ptRatio02",0),
    panTauCellBased_ptRatio04(prefix + "panTauCellBased_ptRatio04",0),
    Var_panTauCellBased_ptRatio04(prefix + "Var_panTauCellBased_ptRatio04",0),
    panTauCellBased_ptRatioChrg01(prefix + "panTauCellBased_ptRatioChrg01",0),
    Var_panTauCellBased_ptRatioChrg01(prefix + "Var_panTauCellBased_ptRatioChrg01",0),
    panTauCellBased_ptRatioChrg02(prefix + "panTauCellBased_ptRatioChrg02",0),
    Var_panTauCellBased_ptRatioChrg02(prefix + "Var_panTauCellBased_ptRatioChrg02",0),
    panTauCellBased_ptRatioChrg04(prefix + "panTauCellBased_ptRatioChrg04",0),
    Var_panTauCellBased_ptRatioChrg04(prefix + "Var_panTauCellBased_ptRatioChrg04",0),
    panTauCellBased_ptRatioNeut01(prefix + "panTauCellBased_ptRatioNeut01",0),
    Var_panTauCellBased_ptRatioNeut01(prefix + "Var_panTauCellBased_ptRatioNeut01",0),
    panTauCellBased_ptRatioNeut02(prefix + "panTauCellBased_ptRatioNeut02",0),
    Var_panTauCellBased_ptRatioNeut02(prefix + "Var_panTauCellBased_ptRatioNeut02",0),
    panTauCellBased_ptRatioNeut04(prefix + "panTauCellBased_ptRatioNeut04",0),
    Var_panTauCellBased_ptRatioNeut04(prefix + "Var_panTauCellBased_ptRatioNeut04",0),
    panTauCellBased_rCal02(prefix + "panTauCellBased_rCal02",0),
    Var_panTauCellBased_rCal02(prefix + "Var_panTauCellBased_rCal02",0),
    panTauCellBased_rCal04(prefix + "panTauCellBased_rCal04",0),
    Var_panTauCellBased_rCal04(prefix + "Var_panTauCellBased_rCal04",0),
    panTauCellBased_rCalChrg02(prefix + "panTauCellBased_rCalChrg02",0),
    Var_panTauCellBased_rCalChrg02(prefix + "Var_panTauCellBased_rCalChrg02",0),
    panTauCellBased_rCalChrg04(prefix + "panTauCellBased_rCalChrg04",0),
    Var_panTauCellBased_rCalChrg04(prefix + "Var_panTauCellBased_rCalChrg04",0),
    panTauCellBased_rCalNeut02(prefix + "panTauCellBased_rCalNeut02",0),
    Var_panTauCellBased_rCalNeut02(prefix + "Var_panTauCellBased_rCalNeut02",0),
    panTauCellBased_rCalNeut04(prefix + "panTauCellBased_rCalNeut04",0),
    Var_panTauCellBased_rCalNeut04(prefix + "Var_panTauCellBased_rCalNeut04",0),
    panTauCellBased_trFlightPathSig(prefix + "panTauCellBased_trFlightPathSig",0),
    Var_panTauCellBased_trFlightPathSig(prefix + "Var_panTauCellBased_trFlightPathSig",0),
    panTauCellBased_visTauM01(prefix + "panTauCellBased_visTauM01",0),
    Var_panTauCellBased_visTauM01(prefix + "Var_panTauCellBased_visTauM01",0),
    panTauCellBased_visTauM02(prefix + "panTauCellBased_visTauM02",0),
    Var_panTauCellBased_visTauM02(prefix + "Var_panTauCellBased_visTauM02",0),
    panTauCellBased_visTauM04(prefix + "panTauCellBased_visTauM04",0),
    Var_panTauCellBased_visTauM04(prefix + "Var_panTauCellBased_visTauM04",0),
    pi0_n(prefix + "pi0_n",0),
    Var_pi0_n(prefix + "Var_pi0_n",0),
    pi0_vistau_m(prefix + "pi0_vistau_m",0),
    Var_pi0_vistau_m(prefix + "Var_pi0_vistau_m",0),
    ptRatio(prefix + "ptRatio",0),
    Var_ptRatio(prefix + "Var_ptRatio",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void LLHTau::ReadFrom(TTree* tree)
  {
    RunNumber.ReadFrom(tree);
    EventNumber.ReadFrom(tree);
    pt.ReadFrom(tree);
    Et.ReadFrom(tree);
    eta.ReadFrom(tree);
    phi.ReadFrom(tree);
    NumTrack.ReadFrom(tree);
    DefaultLLHScore.ReadFrom(tree);
    DefaultLLHLoose.ReadFrom(tree);
    DefaultLLHMedium.ReadFrom(tree);
    DefaultLLHTight.ReadFrom(tree);
    TruthPt.ReadFrom(tree);
    TruthEta.ReadFrom(tree);
    TruthPhi.ReadFrom(tree);
    TruthNumTrack.ReadFrom(tree);
    BDTScore.ReadFrom(tree);
    DefaultBDTLoose.ReadFrom(tree);
    DefaultBDTMedium.ReadFrom(tree);
    DefaultBDTTight.ReadFrom(tree);
    NVertices.ReadFrom(tree);
    NumGoodVertices.ReadFrom(tree);
    mu.ReadFrom(tree);
    matchVisEt.ReadFrom(tree);
    CentFrac0102.ReadFrom(tree);
    Var_CentFrac0102.ReadFrom(tree);
    DrMax.ReadFrom(tree);
    Var_DrMax.ReadFrom(tree);
    FTrk02.ReadFrom(tree);
    Var_FTrk02.ReadFrom(tree);
    IpSigLeadTrk.ReadFrom(tree);
    Var_IpSigLeadTrk.ReadFrom(tree);
    MassTrkSys.ReadFrom(tree);
    Var_MassTrkSys.ReadFrom(tree);
    NTracksdrdR.ReadFrom(tree);
    Var_NTracksdrdR.ReadFrom(tree);
    TrFligthPathSig.ReadFrom(tree);
    Var_TrFligthPathSig.ReadFrom(tree);
    TrkAvgDist.ReadFrom(tree);
    Var_TrkAvgDist.ReadFrom(tree);
    panTauCellBased_dRmax02.ReadFrom(tree);
    Var_panTauCellBased_dRmax02.ReadFrom(tree);
    panTauCellBased_dRmax04.ReadFrom(tree);
    Var_panTauCellBased_dRmax04.ReadFrom(tree);
    panTauCellBased_dRminmaxPtChrg04.ReadFrom(tree);
    Var_panTauCellBased_dRminmaxPtChrg04.ReadFrom(tree);
    panTauCellBased_eFrac0102.ReadFrom(tree);
    Var_panTauCellBased_eFrac0102.ReadFrom(tree);
    panTauCellBased_eFrac0104.ReadFrom(tree);
    Var_panTauCellBased_eFrac0104.ReadFrom(tree);
    panTauCellBased_eFrac0204.ReadFrom(tree);
    Var_panTauCellBased_eFrac0204.ReadFrom(tree);
    panTauCellBased_eFracChrg0104.ReadFrom(tree);
    Var_panTauCellBased_eFracChrg0104.ReadFrom(tree);
    panTauCellBased_eFracChrg0204.ReadFrom(tree);
    Var_panTauCellBased_eFracChrg0204.ReadFrom(tree);
    panTauCellBased_eFracNeut0104.ReadFrom(tree);
    Var_panTauCellBased_eFracNeut0104.ReadFrom(tree);
    panTauCellBased_eFracNeut0204.ReadFrom(tree);
    Var_panTauCellBased_eFracNeut0204.ReadFrom(tree);
    panTauCellBased_fLeadChrg01.ReadFrom(tree);
    Var_panTauCellBased_fLeadChrg01.ReadFrom(tree);
    panTauCellBased_fLeadChrg02.ReadFrom(tree);
    Var_panTauCellBased_fLeadChrg02.ReadFrom(tree);
    panTauCellBased_fLeadChrg04.ReadFrom(tree);
    Var_panTauCellBased_fLeadChrg04.ReadFrom(tree);
    panTauCellBased_ipSigLeadTrk.ReadFrom(tree);
    Var_panTauCellBased_ipSigLeadTrk.ReadFrom(tree);
    panTauCellBased_massChrgSys01.ReadFrom(tree);
    Var_panTauCellBased_massChrgSys01.ReadFrom(tree);
    panTauCellBased_massChrgSys02.ReadFrom(tree);
    Var_panTauCellBased_massChrgSys02.ReadFrom(tree);
    panTauCellBased_massChrgSys04.ReadFrom(tree);
    Var_panTauCellBased_massChrgSys04.ReadFrom(tree);
    panTauCellBased_massNeutSys01.ReadFrom(tree);
    Var_panTauCellBased_massNeutSys01.ReadFrom(tree);
    panTauCellBased_massNeutSys02.ReadFrom(tree);
    Var_panTauCellBased_massNeutSys02.ReadFrom(tree);
    panTauCellBased_massNeutSys04.ReadFrom(tree);
    Var_panTauCellBased_massNeutSys04.ReadFrom(tree);
    panTauCellBased_nChrg01.ReadFrom(tree);
    Var_panTauCellBased_nChrg01.ReadFrom(tree);
    panTauCellBased_nChrg02.ReadFrom(tree);
    Var_panTauCellBased_nChrg02.ReadFrom(tree);
    panTauCellBased_nChrg0204.ReadFrom(tree);
    Var_panTauCellBased_nChrg0204.ReadFrom(tree);
    panTauCellBased_nNeut01.ReadFrom(tree);
    Var_panTauCellBased_nNeut01.ReadFrom(tree);
    panTauCellBased_nNeut02.ReadFrom(tree);
    Var_panTauCellBased_nNeut02.ReadFrom(tree);
    panTauCellBased_nNeut0204.ReadFrom(tree);
    Var_panTauCellBased_nNeut0204.ReadFrom(tree);
    panTauCellBased_ptRatio01.ReadFrom(tree);
    Var_panTauCellBased_ptRatio01.ReadFrom(tree);
    panTauCellBased_ptRatio02.ReadFrom(tree);
    Var_panTauCellBased_ptRatio02.ReadFrom(tree);
    panTauCellBased_ptRatio04.ReadFrom(tree);
    Var_panTauCellBased_ptRatio04.ReadFrom(tree);
    panTauCellBased_ptRatioChrg01.ReadFrom(tree);
    Var_panTauCellBased_ptRatioChrg01.ReadFrom(tree);
    panTauCellBased_ptRatioChrg02.ReadFrom(tree);
    Var_panTauCellBased_ptRatioChrg02.ReadFrom(tree);
    panTauCellBased_ptRatioChrg04.ReadFrom(tree);
    Var_panTauCellBased_ptRatioChrg04.ReadFrom(tree);
    panTauCellBased_ptRatioNeut01.ReadFrom(tree);
    Var_panTauCellBased_ptRatioNeut01.ReadFrom(tree);
    panTauCellBased_ptRatioNeut02.ReadFrom(tree);
    Var_panTauCellBased_ptRatioNeut02.ReadFrom(tree);
    panTauCellBased_ptRatioNeut04.ReadFrom(tree);
    Var_panTauCellBased_ptRatioNeut04.ReadFrom(tree);
    panTauCellBased_rCal02.ReadFrom(tree);
    Var_panTauCellBased_rCal02.ReadFrom(tree);
    panTauCellBased_rCal04.ReadFrom(tree);
    Var_panTauCellBased_rCal04.ReadFrom(tree);
    panTauCellBased_rCalChrg02.ReadFrom(tree);
    Var_panTauCellBased_rCalChrg02.ReadFrom(tree);
    panTauCellBased_rCalChrg04.ReadFrom(tree);
    Var_panTauCellBased_rCalChrg04.ReadFrom(tree);
    panTauCellBased_rCalNeut02.ReadFrom(tree);
    Var_panTauCellBased_rCalNeut02.ReadFrom(tree);
    panTauCellBased_rCalNeut04.ReadFrom(tree);
    Var_panTauCellBased_rCalNeut04.ReadFrom(tree);
    panTauCellBased_trFlightPathSig.ReadFrom(tree);
    Var_panTauCellBased_trFlightPathSig.ReadFrom(tree);
    panTauCellBased_visTauM01.ReadFrom(tree);
    Var_panTauCellBased_visTauM01.ReadFrom(tree);
    panTauCellBased_visTauM02.ReadFrom(tree);
    Var_panTauCellBased_visTauM02.ReadFrom(tree);
    panTauCellBased_visTauM04.ReadFrom(tree);
    Var_panTauCellBased_visTauM04.ReadFrom(tree);
    pi0_n.ReadFrom(tree);
    Var_pi0_n.ReadFrom(tree);
    pi0_vistau_m.ReadFrom(tree);
    Var_pi0_vistau_m.ReadFrom(tree);
    ptRatio.ReadFrom(tree);
    Var_ptRatio.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void LLHTau::WriteTo(TTree* tree)
  {
    RunNumber.WriteTo(tree);
    EventNumber.WriteTo(tree);
    pt.WriteTo(tree);
    Et.WriteTo(tree);
    eta.WriteTo(tree);
    phi.WriteTo(tree);
    NumTrack.WriteTo(tree);
    DefaultLLHScore.WriteTo(tree);
    DefaultLLHLoose.WriteTo(tree);
    DefaultLLHMedium.WriteTo(tree);
    DefaultLLHTight.WriteTo(tree);
    TruthPt.WriteTo(tree);
    TruthEta.WriteTo(tree);
    TruthPhi.WriteTo(tree);
    TruthNumTrack.WriteTo(tree);
    BDTScore.WriteTo(tree);
    DefaultBDTLoose.WriteTo(tree);
    DefaultBDTMedium.WriteTo(tree);
    DefaultBDTTight.WriteTo(tree);
    NVertices.WriteTo(tree);
    NumGoodVertices.WriteTo(tree);
    mu.WriteTo(tree);
    matchVisEt.WriteTo(tree);
    CentFrac0102.WriteTo(tree);
    Var_CentFrac0102.WriteTo(tree);
    DrMax.WriteTo(tree);
    Var_DrMax.WriteTo(tree);
    FTrk02.WriteTo(tree);
    Var_FTrk02.WriteTo(tree);
    IpSigLeadTrk.WriteTo(tree);
    Var_IpSigLeadTrk.WriteTo(tree);
    MassTrkSys.WriteTo(tree);
    Var_MassTrkSys.WriteTo(tree);
    NTracksdrdR.WriteTo(tree);
    Var_NTracksdrdR.WriteTo(tree);
    TrFligthPathSig.WriteTo(tree);
    Var_TrFligthPathSig.WriteTo(tree);
    TrkAvgDist.WriteTo(tree);
    Var_TrkAvgDist.WriteTo(tree);
    panTauCellBased_dRmax02.WriteTo(tree);
    Var_panTauCellBased_dRmax02.WriteTo(tree);
    panTauCellBased_dRmax04.WriteTo(tree);
    Var_panTauCellBased_dRmax04.WriteTo(tree);
    panTauCellBased_dRminmaxPtChrg04.WriteTo(tree);
    Var_panTauCellBased_dRminmaxPtChrg04.WriteTo(tree);
    panTauCellBased_eFrac0102.WriteTo(tree);
    Var_panTauCellBased_eFrac0102.WriteTo(tree);
    panTauCellBased_eFrac0104.WriteTo(tree);
    Var_panTauCellBased_eFrac0104.WriteTo(tree);
    panTauCellBased_eFrac0204.WriteTo(tree);
    Var_panTauCellBased_eFrac0204.WriteTo(tree);
    panTauCellBased_eFracChrg0104.WriteTo(tree);
    Var_panTauCellBased_eFracChrg0104.WriteTo(tree);
    panTauCellBased_eFracChrg0204.WriteTo(tree);
    Var_panTauCellBased_eFracChrg0204.WriteTo(tree);
    panTauCellBased_eFracNeut0104.WriteTo(tree);
    Var_panTauCellBased_eFracNeut0104.WriteTo(tree);
    panTauCellBased_eFracNeut0204.WriteTo(tree);
    Var_panTauCellBased_eFracNeut0204.WriteTo(tree);
    panTauCellBased_fLeadChrg01.WriteTo(tree);
    Var_panTauCellBased_fLeadChrg01.WriteTo(tree);
    panTauCellBased_fLeadChrg02.WriteTo(tree);
    Var_panTauCellBased_fLeadChrg02.WriteTo(tree);
    panTauCellBased_fLeadChrg04.WriteTo(tree);
    Var_panTauCellBased_fLeadChrg04.WriteTo(tree);
    panTauCellBased_ipSigLeadTrk.WriteTo(tree);
    Var_panTauCellBased_ipSigLeadTrk.WriteTo(tree);
    panTauCellBased_massChrgSys01.WriteTo(tree);
    Var_panTauCellBased_massChrgSys01.WriteTo(tree);
    panTauCellBased_massChrgSys02.WriteTo(tree);
    Var_panTauCellBased_massChrgSys02.WriteTo(tree);
    panTauCellBased_massChrgSys04.WriteTo(tree);
    Var_panTauCellBased_massChrgSys04.WriteTo(tree);
    panTauCellBased_massNeutSys01.WriteTo(tree);
    Var_panTauCellBased_massNeutSys01.WriteTo(tree);
    panTauCellBased_massNeutSys02.WriteTo(tree);
    Var_panTauCellBased_massNeutSys02.WriteTo(tree);
    panTauCellBased_massNeutSys04.WriteTo(tree);
    Var_panTauCellBased_massNeutSys04.WriteTo(tree);
    panTauCellBased_nChrg01.WriteTo(tree);
    Var_panTauCellBased_nChrg01.WriteTo(tree);
    panTauCellBased_nChrg02.WriteTo(tree);
    Var_panTauCellBased_nChrg02.WriteTo(tree);
    panTauCellBased_nChrg0204.WriteTo(tree);
    Var_panTauCellBased_nChrg0204.WriteTo(tree);
    panTauCellBased_nNeut01.WriteTo(tree);
    Var_panTauCellBased_nNeut01.WriteTo(tree);
    panTauCellBased_nNeut02.WriteTo(tree);
    Var_panTauCellBased_nNeut02.WriteTo(tree);
    panTauCellBased_nNeut0204.WriteTo(tree);
    Var_panTauCellBased_nNeut0204.WriteTo(tree);
    panTauCellBased_ptRatio01.WriteTo(tree);
    Var_panTauCellBased_ptRatio01.WriteTo(tree);
    panTauCellBased_ptRatio02.WriteTo(tree);
    Var_panTauCellBased_ptRatio02.WriteTo(tree);
    panTauCellBased_ptRatio04.WriteTo(tree);
    Var_panTauCellBased_ptRatio04.WriteTo(tree);
    panTauCellBased_ptRatioChrg01.WriteTo(tree);
    Var_panTauCellBased_ptRatioChrg01.WriteTo(tree);
    panTauCellBased_ptRatioChrg02.WriteTo(tree);
    Var_panTauCellBased_ptRatioChrg02.WriteTo(tree);
    panTauCellBased_ptRatioChrg04.WriteTo(tree);
    Var_panTauCellBased_ptRatioChrg04.WriteTo(tree);
    panTauCellBased_ptRatioNeut01.WriteTo(tree);
    Var_panTauCellBased_ptRatioNeut01.WriteTo(tree);
    panTauCellBased_ptRatioNeut02.WriteTo(tree);
    Var_panTauCellBased_ptRatioNeut02.WriteTo(tree);
    panTauCellBased_ptRatioNeut04.WriteTo(tree);
    Var_panTauCellBased_ptRatioNeut04.WriteTo(tree);
    panTauCellBased_rCal02.WriteTo(tree);
    Var_panTauCellBased_rCal02.WriteTo(tree);
    panTauCellBased_rCal04.WriteTo(tree);
    Var_panTauCellBased_rCal04.WriteTo(tree);
    panTauCellBased_rCalChrg02.WriteTo(tree);
    Var_panTauCellBased_rCalChrg02.WriteTo(tree);
    panTauCellBased_rCalChrg04.WriteTo(tree);
    Var_panTauCellBased_rCalChrg04.WriteTo(tree);
    panTauCellBased_rCalNeut02.WriteTo(tree);
    Var_panTauCellBased_rCalNeut02.WriteTo(tree);
    panTauCellBased_rCalNeut04.WriteTo(tree);
    Var_panTauCellBased_rCalNeut04.WriteTo(tree);
    panTauCellBased_trFlightPathSig.WriteTo(tree);
    Var_panTauCellBased_trFlightPathSig.WriteTo(tree);
    panTauCellBased_visTauM01.WriteTo(tree);
    Var_panTauCellBased_visTauM01.WriteTo(tree);
    panTauCellBased_visTauM02.WriteTo(tree);
    Var_panTauCellBased_visTauM02.WriteTo(tree);
    panTauCellBased_visTauM04.WriteTo(tree);
    Var_panTauCellBased_visTauM04.WriteTo(tree);
    pi0_n.WriteTo(tree);
    Var_pi0_n.WriteTo(tree);
    pi0_vistau_m.WriteTo(tree);
    Var_pi0_vistau_m.WriteTo(tree);
    ptRatio.WriteTo(tree);
    Var_ptRatio.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void LLHTau::SetActive(bool active)
  {
    if(active)
    {
     if(RunNumber.IsAvailable()) RunNumber.SetActive(active);
     if(EventNumber.IsAvailable()) EventNumber.SetActive(active);
     if(pt.IsAvailable()) pt.SetActive(active);
     if(Et.IsAvailable()) Et.SetActive(active);
     if(eta.IsAvailable()) eta.SetActive(active);
     if(phi.IsAvailable()) phi.SetActive(active);
     if(NumTrack.IsAvailable()) NumTrack.SetActive(active);
     if(DefaultLLHScore.IsAvailable()) DefaultLLHScore.SetActive(active);
     if(DefaultLLHLoose.IsAvailable()) DefaultLLHLoose.SetActive(active);
     if(DefaultLLHMedium.IsAvailable()) DefaultLLHMedium.SetActive(active);
     if(DefaultLLHTight.IsAvailable()) DefaultLLHTight.SetActive(active);
     if(TruthPt.IsAvailable()) TruthPt.SetActive(active);
     if(TruthEta.IsAvailable()) TruthEta.SetActive(active);
     if(TruthPhi.IsAvailable()) TruthPhi.SetActive(active);
     if(TruthNumTrack.IsAvailable()) TruthNumTrack.SetActive(active);
     if(BDTScore.IsAvailable()) BDTScore.SetActive(active);
     if(DefaultBDTLoose.IsAvailable()) DefaultBDTLoose.SetActive(active);
     if(DefaultBDTMedium.IsAvailable()) DefaultBDTMedium.SetActive(active);
     if(DefaultBDTTight.IsAvailable()) DefaultBDTTight.SetActive(active);
     if(NVertices.IsAvailable()) NVertices.SetActive(active);
     if(NumGoodVertices.IsAvailable()) NumGoodVertices.SetActive(active);
     if(mu.IsAvailable()) mu.SetActive(active);
     if(matchVisEt.IsAvailable()) matchVisEt.SetActive(active);
     if(CentFrac0102.IsAvailable()) CentFrac0102.SetActive(active);
     if(Var_CentFrac0102.IsAvailable()) Var_CentFrac0102.SetActive(active);
     if(DrMax.IsAvailable()) DrMax.SetActive(active);
     if(Var_DrMax.IsAvailable()) Var_DrMax.SetActive(active);
     if(FTrk02.IsAvailable()) FTrk02.SetActive(active);
     if(Var_FTrk02.IsAvailable()) Var_FTrk02.SetActive(active);
     if(IpSigLeadTrk.IsAvailable()) IpSigLeadTrk.SetActive(active);
     if(Var_IpSigLeadTrk.IsAvailable()) Var_IpSigLeadTrk.SetActive(active);
     if(MassTrkSys.IsAvailable()) MassTrkSys.SetActive(active);
     if(Var_MassTrkSys.IsAvailable()) Var_MassTrkSys.SetActive(active);
     if(NTracksdrdR.IsAvailable()) NTracksdrdR.SetActive(active);
     if(Var_NTracksdrdR.IsAvailable()) Var_NTracksdrdR.SetActive(active);
     if(TrFligthPathSig.IsAvailable()) TrFligthPathSig.SetActive(active);
     if(Var_TrFligthPathSig.IsAvailable()) Var_TrFligthPathSig.SetActive(active);
     if(TrkAvgDist.IsAvailable()) TrkAvgDist.SetActive(active);
     if(Var_TrkAvgDist.IsAvailable()) Var_TrkAvgDist.SetActive(active);
     if(panTauCellBased_dRmax02.IsAvailable()) panTauCellBased_dRmax02.SetActive(active);
     if(Var_panTauCellBased_dRmax02.IsAvailable()) Var_panTauCellBased_dRmax02.SetActive(active);
     if(panTauCellBased_dRmax04.IsAvailable()) panTauCellBased_dRmax04.SetActive(active);
     if(Var_panTauCellBased_dRmax04.IsAvailable()) Var_panTauCellBased_dRmax04.SetActive(active);
     if(panTauCellBased_dRminmaxPtChrg04.IsAvailable()) panTauCellBased_dRminmaxPtChrg04.SetActive(active);
     if(Var_panTauCellBased_dRminmaxPtChrg04.IsAvailable()) Var_panTauCellBased_dRminmaxPtChrg04.SetActive(active);
     if(panTauCellBased_eFrac0102.IsAvailable()) panTauCellBased_eFrac0102.SetActive(active);
     if(Var_panTauCellBased_eFrac0102.IsAvailable()) Var_panTauCellBased_eFrac0102.SetActive(active);
     if(panTauCellBased_eFrac0104.IsAvailable()) panTauCellBased_eFrac0104.SetActive(active);
     if(Var_panTauCellBased_eFrac0104.IsAvailable()) Var_panTauCellBased_eFrac0104.SetActive(active);
     if(panTauCellBased_eFrac0204.IsAvailable()) panTauCellBased_eFrac0204.SetActive(active);
     if(Var_panTauCellBased_eFrac0204.IsAvailable()) Var_panTauCellBased_eFrac0204.SetActive(active);
     if(panTauCellBased_eFracChrg0104.IsAvailable()) panTauCellBased_eFracChrg0104.SetActive(active);
     if(Var_panTauCellBased_eFracChrg0104.IsAvailable()) Var_panTauCellBased_eFracChrg0104.SetActive(active);
     if(panTauCellBased_eFracChrg0204.IsAvailable()) panTauCellBased_eFracChrg0204.SetActive(active);
     if(Var_panTauCellBased_eFracChrg0204.IsAvailable()) Var_panTauCellBased_eFracChrg0204.SetActive(active);
     if(panTauCellBased_eFracNeut0104.IsAvailable()) panTauCellBased_eFracNeut0104.SetActive(active);
     if(Var_panTauCellBased_eFracNeut0104.IsAvailable()) Var_panTauCellBased_eFracNeut0104.SetActive(active);
     if(panTauCellBased_eFracNeut0204.IsAvailable()) panTauCellBased_eFracNeut0204.SetActive(active);
     if(Var_panTauCellBased_eFracNeut0204.IsAvailable()) Var_panTauCellBased_eFracNeut0204.SetActive(active);
     if(panTauCellBased_fLeadChrg01.IsAvailable()) panTauCellBased_fLeadChrg01.SetActive(active);
     if(Var_panTauCellBased_fLeadChrg01.IsAvailable()) Var_panTauCellBased_fLeadChrg01.SetActive(active);
     if(panTauCellBased_fLeadChrg02.IsAvailable()) panTauCellBased_fLeadChrg02.SetActive(active);
     if(Var_panTauCellBased_fLeadChrg02.IsAvailable()) Var_panTauCellBased_fLeadChrg02.SetActive(active);
     if(panTauCellBased_fLeadChrg04.IsAvailable()) panTauCellBased_fLeadChrg04.SetActive(active);
     if(Var_panTauCellBased_fLeadChrg04.IsAvailable()) Var_panTauCellBased_fLeadChrg04.SetActive(active);
     if(panTauCellBased_ipSigLeadTrk.IsAvailable()) panTauCellBased_ipSigLeadTrk.SetActive(active);
     if(Var_panTauCellBased_ipSigLeadTrk.IsAvailable()) Var_panTauCellBased_ipSigLeadTrk.SetActive(active);
     if(panTauCellBased_massChrgSys01.IsAvailable()) panTauCellBased_massChrgSys01.SetActive(active);
     if(Var_panTauCellBased_massChrgSys01.IsAvailable()) Var_panTauCellBased_massChrgSys01.SetActive(active);
     if(panTauCellBased_massChrgSys02.IsAvailable()) panTauCellBased_massChrgSys02.SetActive(active);
     if(Var_panTauCellBased_massChrgSys02.IsAvailable()) Var_panTauCellBased_massChrgSys02.SetActive(active);
     if(panTauCellBased_massChrgSys04.IsAvailable()) panTauCellBased_massChrgSys04.SetActive(active);
     if(Var_panTauCellBased_massChrgSys04.IsAvailable()) Var_panTauCellBased_massChrgSys04.SetActive(active);
     if(panTauCellBased_massNeutSys01.IsAvailable()) panTauCellBased_massNeutSys01.SetActive(active);
     if(Var_panTauCellBased_massNeutSys01.IsAvailable()) Var_panTauCellBased_massNeutSys01.SetActive(active);
     if(panTauCellBased_massNeutSys02.IsAvailable()) panTauCellBased_massNeutSys02.SetActive(active);
     if(Var_panTauCellBased_massNeutSys02.IsAvailable()) Var_panTauCellBased_massNeutSys02.SetActive(active);
     if(panTauCellBased_massNeutSys04.IsAvailable()) panTauCellBased_massNeutSys04.SetActive(active);
     if(Var_panTauCellBased_massNeutSys04.IsAvailable()) Var_panTauCellBased_massNeutSys04.SetActive(active);
     if(panTauCellBased_nChrg01.IsAvailable()) panTauCellBased_nChrg01.SetActive(active);
     if(Var_panTauCellBased_nChrg01.IsAvailable()) Var_panTauCellBased_nChrg01.SetActive(active);
     if(panTauCellBased_nChrg02.IsAvailable()) panTauCellBased_nChrg02.SetActive(active);
     if(Var_panTauCellBased_nChrg02.IsAvailable()) Var_panTauCellBased_nChrg02.SetActive(active);
     if(panTauCellBased_nChrg0204.IsAvailable()) panTauCellBased_nChrg0204.SetActive(active);
     if(Var_panTauCellBased_nChrg0204.IsAvailable()) Var_panTauCellBased_nChrg0204.SetActive(active);
     if(panTauCellBased_nNeut01.IsAvailable()) panTauCellBased_nNeut01.SetActive(active);
     if(Var_panTauCellBased_nNeut01.IsAvailable()) Var_panTauCellBased_nNeut01.SetActive(active);
     if(panTauCellBased_nNeut02.IsAvailable()) panTauCellBased_nNeut02.SetActive(active);
     if(Var_panTauCellBased_nNeut02.IsAvailable()) Var_panTauCellBased_nNeut02.SetActive(active);
     if(panTauCellBased_nNeut0204.IsAvailable()) panTauCellBased_nNeut0204.SetActive(active);
     if(Var_panTauCellBased_nNeut0204.IsAvailable()) Var_panTauCellBased_nNeut0204.SetActive(active);
     if(panTauCellBased_ptRatio01.IsAvailable()) panTauCellBased_ptRatio01.SetActive(active);
     if(Var_panTauCellBased_ptRatio01.IsAvailable()) Var_panTauCellBased_ptRatio01.SetActive(active);
     if(panTauCellBased_ptRatio02.IsAvailable()) panTauCellBased_ptRatio02.SetActive(active);
     if(Var_panTauCellBased_ptRatio02.IsAvailable()) Var_panTauCellBased_ptRatio02.SetActive(active);
     if(panTauCellBased_ptRatio04.IsAvailable()) panTauCellBased_ptRatio04.SetActive(active);
     if(Var_panTauCellBased_ptRatio04.IsAvailable()) Var_panTauCellBased_ptRatio04.SetActive(active);
     if(panTauCellBased_ptRatioChrg01.IsAvailable()) panTauCellBased_ptRatioChrg01.SetActive(active);
     if(Var_panTauCellBased_ptRatioChrg01.IsAvailable()) Var_panTauCellBased_ptRatioChrg01.SetActive(active);
     if(panTauCellBased_ptRatioChrg02.IsAvailable()) panTauCellBased_ptRatioChrg02.SetActive(active);
     if(Var_panTauCellBased_ptRatioChrg02.IsAvailable()) Var_panTauCellBased_ptRatioChrg02.SetActive(active);
     if(panTauCellBased_ptRatioChrg04.IsAvailable()) panTauCellBased_ptRatioChrg04.SetActive(active);
     if(Var_panTauCellBased_ptRatioChrg04.IsAvailable()) Var_panTauCellBased_ptRatioChrg04.SetActive(active);
     if(panTauCellBased_ptRatioNeut01.IsAvailable()) panTauCellBased_ptRatioNeut01.SetActive(active);
     if(Var_panTauCellBased_ptRatioNeut01.IsAvailable()) Var_panTauCellBased_ptRatioNeut01.SetActive(active);
     if(panTauCellBased_ptRatioNeut02.IsAvailable()) panTauCellBased_ptRatioNeut02.SetActive(active);
     if(Var_panTauCellBased_ptRatioNeut02.IsAvailable()) Var_panTauCellBased_ptRatioNeut02.SetActive(active);
     if(panTauCellBased_ptRatioNeut04.IsAvailable()) panTauCellBased_ptRatioNeut04.SetActive(active);
     if(Var_panTauCellBased_ptRatioNeut04.IsAvailable()) Var_panTauCellBased_ptRatioNeut04.SetActive(active);
     if(panTauCellBased_rCal02.IsAvailable()) panTauCellBased_rCal02.SetActive(active);
     if(Var_panTauCellBased_rCal02.IsAvailable()) Var_panTauCellBased_rCal02.SetActive(active);
     if(panTauCellBased_rCal04.IsAvailable()) panTauCellBased_rCal04.SetActive(active);
     if(Var_panTauCellBased_rCal04.IsAvailable()) Var_panTauCellBased_rCal04.SetActive(active);
     if(panTauCellBased_rCalChrg02.IsAvailable()) panTauCellBased_rCalChrg02.SetActive(active);
     if(Var_panTauCellBased_rCalChrg02.IsAvailable()) Var_panTauCellBased_rCalChrg02.SetActive(active);
     if(panTauCellBased_rCalChrg04.IsAvailable()) panTauCellBased_rCalChrg04.SetActive(active);
     if(Var_panTauCellBased_rCalChrg04.IsAvailable()) Var_panTauCellBased_rCalChrg04.SetActive(active);
     if(panTauCellBased_rCalNeut02.IsAvailable()) panTauCellBased_rCalNeut02.SetActive(active);
     if(Var_panTauCellBased_rCalNeut02.IsAvailable()) Var_panTauCellBased_rCalNeut02.SetActive(active);
     if(panTauCellBased_rCalNeut04.IsAvailable()) panTauCellBased_rCalNeut04.SetActive(active);
     if(Var_panTauCellBased_rCalNeut04.IsAvailable()) Var_panTauCellBased_rCalNeut04.SetActive(active);
     if(panTauCellBased_trFlightPathSig.IsAvailable()) panTauCellBased_trFlightPathSig.SetActive(active);
     if(Var_panTauCellBased_trFlightPathSig.IsAvailable()) Var_panTauCellBased_trFlightPathSig.SetActive(active);
     if(panTauCellBased_visTauM01.IsAvailable()) panTauCellBased_visTauM01.SetActive(active);
     if(Var_panTauCellBased_visTauM01.IsAvailable()) Var_panTauCellBased_visTauM01.SetActive(active);
     if(panTauCellBased_visTauM02.IsAvailable()) panTauCellBased_visTauM02.SetActive(active);
     if(Var_panTauCellBased_visTauM02.IsAvailable()) Var_panTauCellBased_visTauM02.SetActive(active);
     if(panTauCellBased_visTauM04.IsAvailable()) panTauCellBased_visTauM04.SetActive(active);
     if(Var_panTauCellBased_visTauM04.IsAvailable()) Var_panTauCellBased_visTauM04.SetActive(active);
     if(pi0_n.IsAvailable()) pi0_n.SetActive(active);
     if(Var_pi0_n.IsAvailable()) Var_pi0_n.SetActive(active);
     if(pi0_vistau_m.IsAvailable()) pi0_vistau_m.SetActive(active);
     if(Var_pi0_vistau_m.IsAvailable()) Var_pi0_vistau_m.SetActive(active);
     if(ptRatio.IsAvailable()) ptRatio.SetActive(active);
     if(Var_ptRatio.IsAvailable()) Var_ptRatio.SetActive(active);
    }
    else
    {
      RunNumber.SetActive(active);
      EventNumber.SetActive(active);
      pt.SetActive(active);
      Et.SetActive(active);
      eta.SetActive(active);
      phi.SetActive(active);
      NumTrack.SetActive(active);
      DefaultLLHScore.SetActive(active);
      DefaultLLHLoose.SetActive(active);
      DefaultLLHMedium.SetActive(active);
      DefaultLLHTight.SetActive(active);
      TruthPt.SetActive(active);
      TruthEta.SetActive(active);
      TruthPhi.SetActive(active);
      TruthNumTrack.SetActive(active);
      BDTScore.SetActive(active);
      DefaultBDTLoose.SetActive(active);
      DefaultBDTMedium.SetActive(active);
      DefaultBDTTight.SetActive(active);
      NVertices.SetActive(active);
      NumGoodVertices.SetActive(active);
      mu.SetActive(active);
      matchVisEt.SetActive(active);
      CentFrac0102.SetActive(active);
      Var_CentFrac0102.SetActive(active);
      DrMax.SetActive(active);
      Var_DrMax.SetActive(active);
      FTrk02.SetActive(active);
      Var_FTrk02.SetActive(active);
      IpSigLeadTrk.SetActive(active);
      Var_IpSigLeadTrk.SetActive(active);
      MassTrkSys.SetActive(active);
      Var_MassTrkSys.SetActive(active);
      NTracksdrdR.SetActive(active);
      Var_NTracksdrdR.SetActive(active);
      TrFligthPathSig.SetActive(active);
      Var_TrFligthPathSig.SetActive(active);
      TrkAvgDist.SetActive(active);
      Var_TrkAvgDist.SetActive(active);
      panTauCellBased_dRmax02.SetActive(active);
      Var_panTauCellBased_dRmax02.SetActive(active);
      panTauCellBased_dRmax04.SetActive(active);
      Var_panTauCellBased_dRmax04.SetActive(active);
      panTauCellBased_dRminmaxPtChrg04.SetActive(active);
      Var_panTauCellBased_dRminmaxPtChrg04.SetActive(active);
      panTauCellBased_eFrac0102.SetActive(active);
      Var_panTauCellBased_eFrac0102.SetActive(active);
      panTauCellBased_eFrac0104.SetActive(active);
      Var_panTauCellBased_eFrac0104.SetActive(active);
      panTauCellBased_eFrac0204.SetActive(active);
      Var_panTauCellBased_eFrac0204.SetActive(active);
      panTauCellBased_eFracChrg0104.SetActive(active);
      Var_panTauCellBased_eFracChrg0104.SetActive(active);
      panTauCellBased_eFracChrg0204.SetActive(active);
      Var_panTauCellBased_eFracChrg0204.SetActive(active);
      panTauCellBased_eFracNeut0104.SetActive(active);
      Var_panTauCellBased_eFracNeut0104.SetActive(active);
      panTauCellBased_eFracNeut0204.SetActive(active);
      Var_panTauCellBased_eFracNeut0204.SetActive(active);
      panTauCellBased_fLeadChrg01.SetActive(active);
      Var_panTauCellBased_fLeadChrg01.SetActive(active);
      panTauCellBased_fLeadChrg02.SetActive(active);
      Var_panTauCellBased_fLeadChrg02.SetActive(active);
      panTauCellBased_fLeadChrg04.SetActive(active);
      Var_panTauCellBased_fLeadChrg04.SetActive(active);
      panTauCellBased_ipSigLeadTrk.SetActive(active);
      Var_panTauCellBased_ipSigLeadTrk.SetActive(active);
      panTauCellBased_massChrgSys01.SetActive(active);
      Var_panTauCellBased_massChrgSys01.SetActive(active);
      panTauCellBased_massChrgSys02.SetActive(active);
      Var_panTauCellBased_massChrgSys02.SetActive(active);
      panTauCellBased_massChrgSys04.SetActive(active);
      Var_panTauCellBased_massChrgSys04.SetActive(active);
      panTauCellBased_massNeutSys01.SetActive(active);
      Var_panTauCellBased_massNeutSys01.SetActive(active);
      panTauCellBased_massNeutSys02.SetActive(active);
      Var_panTauCellBased_massNeutSys02.SetActive(active);
      panTauCellBased_massNeutSys04.SetActive(active);
      Var_panTauCellBased_massNeutSys04.SetActive(active);
      panTauCellBased_nChrg01.SetActive(active);
      Var_panTauCellBased_nChrg01.SetActive(active);
      panTauCellBased_nChrg02.SetActive(active);
      Var_panTauCellBased_nChrg02.SetActive(active);
      panTauCellBased_nChrg0204.SetActive(active);
      Var_panTauCellBased_nChrg0204.SetActive(active);
      panTauCellBased_nNeut01.SetActive(active);
      Var_panTauCellBased_nNeut01.SetActive(active);
      panTauCellBased_nNeut02.SetActive(active);
      Var_panTauCellBased_nNeut02.SetActive(active);
      panTauCellBased_nNeut0204.SetActive(active);
      Var_panTauCellBased_nNeut0204.SetActive(active);
      panTauCellBased_ptRatio01.SetActive(active);
      Var_panTauCellBased_ptRatio01.SetActive(active);
      panTauCellBased_ptRatio02.SetActive(active);
      Var_panTauCellBased_ptRatio02.SetActive(active);
      panTauCellBased_ptRatio04.SetActive(active);
      Var_panTauCellBased_ptRatio04.SetActive(active);
      panTauCellBased_ptRatioChrg01.SetActive(active);
      Var_panTauCellBased_ptRatioChrg01.SetActive(active);
      panTauCellBased_ptRatioChrg02.SetActive(active);
      Var_panTauCellBased_ptRatioChrg02.SetActive(active);
      panTauCellBased_ptRatioChrg04.SetActive(active);
      Var_panTauCellBased_ptRatioChrg04.SetActive(active);
      panTauCellBased_ptRatioNeut01.SetActive(active);
      Var_panTauCellBased_ptRatioNeut01.SetActive(active);
      panTauCellBased_ptRatioNeut02.SetActive(active);
      Var_panTauCellBased_ptRatioNeut02.SetActive(active);
      panTauCellBased_ptRatioNeut04.SetActive(active);
      Var_panTauCellBased_ptRatioNeut04.SetActive(active);
      panTauCellBased_rCal02.SetActive(active);
      Var_panTauCellBased_rCal02.SetActive(active);
      panTauCellBased_rCal04.SetActive(active);
      Var_panTauCellBased_rCal04.SetActive(active);
      panTauCellBased_rCalChrg02.SetActive(active);
      Var_panTauCellBased_rCalChrg02.SetActive(active);
      panTauCellBased_rCalChrg04.SetActive(active);
      Var_panTauCellBased_rCalChrg04.SetActive(active);
      panTauCellBased_rCalNeut02.SetActive(active);
      Var_panTauCellBased_rCalNeut02.SetActive(active);
      panTauCellBased_rCalNeut04.SetActive(active);
      Var_panTauCellBased_rCalNeut04.SetActive(active);
      panTauCellBased_trFlightPathSig.SetActive(active);
      Var_panTauCellBased_trFlightPathSig.SetActive(active);
      panTauCellBased_visTauM01.SetActive(active);
      Var_panTauCellBased_visTauM01.SetActive(active);
      panTauCellBased_visTauM02.SetActive(active);
      Var_panTauCellBased_visTauM02.SetActive(active);
      panTauCellBased_visTauM04.SetActive(active);
      Var_panTauCellBased_visTauM04.SetActive(active);
      pi0_n.SetActive(active);
      Var_pi0_n.SetActive(active);
      pi0_vistau_m.SetActive(active);
      Var_pi0_vistau_m.SetActive(active);
      ptRatio.SetActive(active);
      Var_ptRatio.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void LLHTau::ReadAllActive()
  {
    if(RunNumber.IsActive()) RunNumber();
    if(EventNumber.IsActive()) EventNumber();
    if(pt.IsActive()) pt();
    if(Et.IsActive()) Et();
    if(eta.IsActive()) eta();
    if(phi.IsActive()) phi();
    if(NumTrack.IsActive()) NumTrack();
    if(DefaultLLHScore.IsActive()) DefaultLLHScore();
    if(DefaultLLHLoose.IsActive()) DefaultLLHLoose();
    if(DefaultLLHMedium.IsActive()) DefaultLLHMedium();
    if(DefaultLLHTight.IsActive()) DefaultLLHTight();
    if(TruthPt.IsActive()) TruthPt();
    if(TruthEta.IsActive()) TruthEta();
    if(TruthPhi.IsActive()) TruthPhi();
    if(TruthNumTrack.IsActive()) TruthNumTrack();
    if(BDTScore.IsActive()) BDTScore();
    if(DefaultBDTLoose.IsActive()) DefaultBDTLoose();
    if(DefaultBDTMedium.IsActive()) DefaultBDTMedium();
    if(DefaultBDTTight.IsActive()) DefaultBDTTight();
    if(NVertices.IsActive()) NVertices();
    if(NumGoodVertices.IsActive()) NumGoodVertices();
    if(mu.IsActive()) mu();
    if(matchVisEt.IsActive()) matchVisEt();
    if(CentFrac0102.IsActive()) CentFrac0102();
    if(Var_CentFrac0102.IsActive()) Var_CentFrac0102();
    if(DrMax.IsActive()) DrMax();
    if(Var_DrMax.IsActive()) Var_DrMax();
    if(FTrk02.IsActive()) FTrk02();
    if(Var_FTrk02.IsActive()) Var_FTrk02();
    if(IpSigLeadTrk.IsActive()) IpSigLeadTrk();
    if(Var_IpSigLeadTrk.IsActive()) Var_IpSigLeadTrk();
    if(MassTrkSys.IsActive()) MassTrkSys();
    if(Var_MassTrkSys.IsActive()) Var_MassTrkSys();
    if(NTracksdrdR.IsActive()) NTracksdrdR();
    if(Var_NTracksdrdR.IsActive()) Var_NTracksdrdR();
    if(TrFligthPathSig.IsActive()) TrFligthPathSig();
    if(Var_TrFligthPathSig.IsActive()) Var_TrFligthPathSig();
    if(TrkAvgDist.IsActive()) TrkAvgDist();
    if(Var_TrkAvgDist.IsActive()) Var_TrkAvgDist();
    if(panTauCellBased_dRmax02.IsActive()) panTauCellBased_dRmax02();
    if(Var_panTauCellBased_dRmax02.IsActive()) Var_panTauCellBased_dRmax02();
    if(panTauCellBased_dRmax04.IsActive()) panTauCellBased_dRmax04();
    if(Var_panTauCellBased_dRmax04.IsActive()) Var_panTauCellBased_dRmax04();
    if(panTauCellBased_dRminmaxPtChrg04.IsActive()) panTauCellBased_dRminmaxPtChrg04();
    if(Var_panTauCellBased_dRminmaxPtChrg04.IsActive()) Var_panTauCellBased_dRminmaxPtChrg04();
    if(panTauCellBased_eFrac0102.IsActive()) panTauCellBased_eFrac0102();
    if(Var_panTauCellBased_eFrac0102.IsActive()) Var_panTauCellBased_eFrac0102();
    if(panTauCellBased_eFrac0104.IsActive()) panTauCellBased_eFrac0104();
    if(Var_panTauCellBased_eFrac0104.IsActive()) Var_panTauCellBased_eFrac0104();
    if(panTauCellBased_eFrac0204.IsActive()) panTauCellBased_eFrac0204();
    if(Var_panTauCellBased_eFrac0204.IsActive()) Var_panTauCellBased_eFrac0204();
    if(panTauCellBased_eFracChrg0104.IsActive()) panTauCellBased_eFracChrg0104();
    if(Var_panTauCellBased_eFracChrg0104.IsActive()) Var_panTauCellBased_eFracChrg0104();
    if(panTauCellBased_eFracChrg0204.IsActive()) panTauCellBased_eFracChrg0204();
    if(Var_panTauCellBased_eFracChrg0204.IsActive()) Var_panTauCellBased_eFracChrg0204();
    if(panTauCellBased_eFracNeut0104.IsActive()) panTauCellBased_eFracNeut0104();
    if(Var_panTauCellBased_eFracNeut0104.IsActive()) Var_panTauCellBased_eFracNeut0104();
    if(panTauCellBased_eFracNeut0204.IsActive()) panTauCellBased_eFracNeut0204();
    if(Var_panTauCellBased_eFracNeut0204.IsActive()) Var_panTauCellBased_eFracNeut0204();
    if(panTauCellBased_fLeadChrg01.IsActive()) panTauCellBased_fLeadChrg01();
    if(Var_panTauCellBased_fLeadChrg01.IsActive()) Var_panTauCellBased_fLeadChrg01();
    if(panTauCellBased_fLeadChrg02.IsActive()) panTauCellBased_fLeadChrg02();
    if(Var_panTauCellBased_fLeadChrg02.IsActive()) Var_panTauCellBased_fLeadChrg02();
    if(panTauCellBased_fLeadChrg04.IsActive()) panTauCellBased_fLeadChrg04();
    if(Var_panTauCellBased_fLeadChrg04.IsActive()) Var_panTauCellBased_fLeadChrg04();
    if(panTauCellBased_ipSigLeadTrk.IsActive()) panTauCellBased_ipSigLeadTrk();
    if(Var_panTauCellBased_ipSigLeadTrk.IsActive()) Var_panTauCellBased_ipSigLeadTrk();
    if(panTauCellBased_massChrgSys01.IsActive()) panTauCellBased_massChrgSys01();
    if(Var_panTauCellBased_massChrgSys01.IsActive()) Var_panTauCellBased_massChrgSys01();
    if(panTauCellBased_massChrgSys02.IsActive()) panTauCellBased_massChrgSys02();
    if(Var_panTauCellBased_massChrgSys02.IsActive()) Var_panTauCellBased_massChrgSys02();
    if(panTauCellBased_massChrgSys04.IsActive()) panTauCellBased_massChrgSys04();
    if(Var_panTauCellBased_massChrgSys04.IsActive()) Var_panTauCellBased_massChrgSys04();
    if(panTauCellBased_massNeutSys01.IsActive()) panTauCellBased_massNeutSys01();
    if(Var_panTauCellBased_massNeutSys01.IsActive()) Var_panTauCellBased_massNeutSys01();
    if(panTauCellBased_massNeutSys02.IsActive()) panTauCellBased_massNeutSys02();
    if(Var_panTauCellBased_massNeutSys02.IsActive()) Var_panTauCellBased_massNeutSys02();
    if(panTauCellBased_massNeutSys04.IsActive()) panTauCellBased_massNeutSys04();
    if(Var_panTauCellBased_massNeutSys04.IsActive()) Var_panTauCellBased_massNeutSys04();
    if(panTauCellBased_nChrg01.IsActive()) panTauCellBased_nChrg01();
    if(Var_panTauCellBased_nChrg01.IsActive()) Var_panTauCellBased_nChrg01();
    if(panTauCellBased_nChrg02.IsActive()) panTauCellBased_nChrg02();
    if(Var_panTauCellBased_nChrg02.IsActive()) Var_panTauCellBased_nChrg02();
    if(panTauCellBased_nChrg0204.IsActive()) panTauCellBased_nChrg0204();
    if(Var_panTauCellBased_nChrg0204.IsActive()) Var_panTauCellBased_nChrg0204();
    if(panTauCellBased_nNeut01.IsActive()) panTauCellBased_nNeut01();
    if(Var_panTauCellBased_nNeut01.IsActive()) Var_panTauCellBased_nNeut01();
    if(panTauCellBased_nNeut02.IsActive()) panTauCellBased_nNeut02();
    if(Var_panTauCellBased_nNeut02.IsActive()) Var_panTauCellBased_nNeut02();
    if(panTauCellBased_nNeut0204.IsActive()) panTauCellBased_nNeut0204();
    if(Var_panTauCellBased_nNeut0204.IsActive()) Var_panTauCellBased_nNeut0204();
    if(panTauCellBased_ptRatio01.IsActive()) panTauCellBased_ptRatio01();
    if(Var_panTauCellBased_ptRatio01.IsActive()) Var_panTauCellBased_ptRatio01();
    if(panTauCellBased_ptRatio02.IsActive()) panTauCellBased_ptRatio02();
    if(Var_panTauCellBased_ptRatio02.IsActive()) Var_panTauCellBased_ptRatio02();
    if(panTauCellBased_ptRatio04.IsActive()) panTauCellBased_ptRatio04();
    if(Var_panTauCellBased_ptRatio04.IsActive()) Var_panTauCellBased_ptRatio04();
    if(panTauCellBased_ptRatioChrg01.IsActive()) panTauCellBased_ptRatioChrg01();
    if(Var_panTauCellBased_ptRatioChrg01.IsActive()) Var_panTauCellBased_ptRatioChrg01();
    if(panTauCellBased_ptRatioChrg02.IsActive()) panTauCellBased_ptRatioChrg02();
    if(Var_panTauCellBased_ptRatioChrg02.IsActive()) Var_panTauCellBased_ptRatioChrg02();
    if(panTauCellBased_ptRatioChrg04.IsActive()) panTauCellBased_ptRatioChrg04();
    if(Var_panTauCellBased_ptRatioChrg04.IsActive()) Var_panTauCellBased_ptRatioChrg04();
    if(panTauCellBased_ptRatioNeut01.IsActive()) panTauCellBased_ptRatioNeut01();
    if(Var_panTauCellBased_ptRatioNeut01.IsActive()) Var_panTauCellBased_ptRatioNeut01();
    if(panTauCellBased_ptRatioNeut02.IsActive()) panTauCellBased_ptRatioNeut02();
    if(Var_panTauCellBased_ptRatioNeut02.IsActive()) Var_panTauCellBased_ptRatioNeut02();
    if(panTauCellBased_ptRatioNeut04.IsActive()) panTauCellBased_ptRatioNeut04();
    if(Var_panTauCellBased_ptRatioNeut04.IsActive()) Var_panTauCellBased_ptRatioNeut04();
    if(panTauCellBased_rCal02.IsActive()) panTauCellBased_rCal02();
    if(Var_panTauCellBased_rCal02.IsActive()) Var_panTauCellBased_rCal02();
    if(panTauCellBased_rCal04.IsActive()) panTauCellBased_rCal04();
    if(Var_panTauCellBased_rCal04.IsActive()) Var_panTauCellBased_rCal04();
    if(panTauCellBased_rCalChrg02.IsActive()) panTauCellBased_rCalChrg02();
    if(Var_panTauCellBased_rCalChrg02.IsActive()) Var_panTauCellBased_rCalChrg02();
    if(panTauCellBased_rCalChrg04.IsActive()) panTauCellBased_rCalChrg04();
    if(Var_panTauCellBased_rCalChrg04.IsActive()) Var_panTauCellBased_rCalChrg04();
    if(panTauCellBased_rCalNeut02.IsActive()) panTauCellBased_rCalNeut02();
    if(Var_panTauCellBased_rCalNeut02.IsActive()) Var_panTauCellBased_rCalNeut02();
    if(panTauCellBased_rCalNeut04.IsActive()) panTauCellBased_rCalNeut04();
    if(Var_panTauCellBased_rCalNeut04.IsActive()) Var_panTauCellBased_rCalNeut04();
    if(panTauCellBased_trFlightPathSig.IsActive()) panTauCellBased_trFlightPathSig();
    if(Var_panTauCellBased_trFlightPathSig.IsActive()) Var_panTauCellBased_trFlightPathSig();
    if(panTauCellBased_visTauM01.IsActive()) panTauCellBased_visTauM01();
    if(Var_panTauCellBased_visTauM01.IsActive()) Var_panTauCellBased_visTauM01();
    if(panTauCellBased_visTauM02.IsActive()) panTauCellBased_visTauM02();
    if(Var_panTauCellBased_visTauM02.IsActive()) Var_panTauCellBased_visTauM02();
    if(panTauCellBased_visTauM04.IsActive()) panTauCellBased_visTauM04();
    if(Var_panTauCellBased_visTauM04.IsActive()) Var_panTauCellBased_visTauM04();
    if(pi0_n.IsActive()) pi0_n();
    if(Var_pi0_n.IsActive()) Var_pi0_n();
    if(pi0_vistau_m.IsActive()) pi0_vistau_m();
    if(Var_pi0_vistau_m.IsActive()) Var_pi0_vistau_m();
    if(ptRatio.IsActive()) ptRatio();
    if(Var_ptRatio.IsActive()) Var_ptRatio();
  }

  /**
   * This function can be used to remove the element with the given index
   * from all available branches of type std::vector<...>*.
   */
  void LLHTau::RemoveIndex(unsigned int index)
  {
  }

} // namespace D3PDReader
#endif // D3PDREADER_LLHTau_CXX
