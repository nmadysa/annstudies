#ifndef SKIM
// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef D3PDREADER_AnalysisEventInfo_CXX
#define D3PDREADER_AnalysisEventInfo_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "AnalysisEventInfo.h"

ClassImp(D3PDReader::AnalysisEventInfo)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  AnalysisEventInfo::AnalysisEventInfo(const long int& master,const std::string& prefix):
    EventInfo(master, prefix),
    mu(prefix + "mu",&master),
    nVtx(prefix + "nVtx",&master),
    m_sPrefix(prefix)
  {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  AnalysisEventInfo::AnalysisEventInfo(const std::string& prefix):
    EventInfo(prefix),
    mu(prefix + "mu",0),
    nVtx(prefix + "nVtx",0),
    m_sPrefix(prefix)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void AnalysisEventInfo::ReadFrom(TTree* tree)
  {
    EventInfo::ReadFrom(tree);
    mu.ReadFrom(tree);
    nVtx.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void AnalysisEventInfo::WriteTo(TTree* tree)
  {
    EventInfo::WriteTo(tree);
    mu.WriteTo(tree);
    nVtx.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void AnalysisEventInfo::SetActive(bool active)
  {
    EventInfo::SetActive(active);
    if(active)
      {
	if(mu.IsAvailable()) mu.SetActive(active);
	if(nVtx.IsAvailable()) nVtx.SetActive(active);
      }
    else
      {
	mu.SetActive(active);
	nVtx.SetActive(active);
      }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void AnalysisEventInfo::ReadAllActive()
  {
    EventInfo::ReadAllActive();
    if(mu.IsActive()) mu();
    if(nVtx.IsActive()) nVtx();
  }

} // namespace D3PDReader
#endif // D3PDREADER_AnalysisEventInfo_CXX
#endif //SKIM
