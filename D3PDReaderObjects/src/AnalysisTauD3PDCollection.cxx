// -------------------------------------------------------------
//             Code produced by D3PDReaderMaker
//
//  author: Christian Gumpert <cgumpert@cern.ch>
// -------------------------------------------------------------

#ifndef SKIM
#ifndef D3PDREADER_AnalysisTauD3PDCollection_CXX
#define D3PDREADER_AnalysisTauD3PDCollection_CXX

// ROOT include(s)
#include "TTree.h"

// custom header(s)
#include "AnalysisTauD3PDCollection.h"
#include <iostream>
ClassImp(D3PDReader::AnalysisTauD3PDCollection)

namespace D3PDReader
{
  /**
   * This constructor should be used when the object will be used to read
   * variables from an existing ntuple. The object will also be able to
   * output variables, but it will also need to read them from somewhere.
   *
   * @param master Reference to the variable holding the current event number
   * @param prefix Prefix of the variables in the D3PD
   */
  AnalysisTauD3PDCollection::AnalysisTauD3PDCollection(const long int& master,const std::string& prefix):
    TauD3PDCollection(master, prefix),
    m_pTauD3PDCollection(new TauD3PDCollection(master, prefix)),
    hasTruthMatch(prefix + "hasTruthMatch",&master),
    ptRatio(prefix + "ptRatio",&master),
    matchPt(prefix + "matchPt",&master),
    matchVisEt(prefix + "matchVisEt",&master),
    matchNProng(prefix + "matchNProng",&master),
    matchEta(prefix + "matchEta",&master),
    matchPhi(prefix + "matchPhi",&master), 
    EtOverPtLeadTrk(prefix + "EtOverPtLeadTrk",&master),
    DefaultLLHScore(prefix + "DefaultLLHScore",&master),
    BDTScore(prefix + "BDTScore",&master)
 {}

  /**
   * This constructor can be used when the object will only have to output
   * (and temporarily store) new information into an output ntuple. For
   * instance when one wants to create a selected/modified list of information.
   *
   * @param prefix Prefix of the variables in the D3PD
   */
  AnalysisTauD3PDCollection::AnalysisTauD3PDCollection(const std::string& prefix):
    TauD3PDCollection(prefix),
    m_pTauD3PDCollection(new TauD3PDCollection(prefix)),
    hasTruthMatch(prefix + "hasTruthMatch",0),
    ptRatio(prefix + "ptRatio",0),
    matchPt(prefix + "matchPt",0),
    matchVisEt(prefix + "matchVisEt",0),
    matchNProng(prefix + "matchNProng",0),
    matchEta(prefix + "matchEta",0),
    matchPhi(prefix + "matchPhi",0),
    EtOverPtLeadTrk(prefix + "EtOverPtLeadTrk",0),
    DefaultLLHScore(prefix + "DefaultLLHScore",0),
    BDTScore(prefix + "BDTScore",0)
  {}

  /**
   * This function should be called every time a new TFile is opened
   * by your analysis code.
   *
   * @param tree Pointer to the TTree with the variables
   */
  void AnalysisTauD3PDCollection::ReadFrom(TTree* tree)
  {
    TauD3PDCollection::ReadFrom(tree);
    m_pTauD3PDCollection->ReadFrom(tree);
    hasTruthMatch.ReadFrom(tree);
    ptRatio.ReadFrom(tree);
    matchPt.ReadFrom(tree);
    matchVisEt.ReadFrom(tree);
    matchNProng.ReadFrom(tree);
    matchEta.ReadFrom(tree);
    matchPhi.ReadFrom(tree);
    EtOverPtLeadTrk.ReadFrom(tree);
    DefaultLLHScore.ReadFrom(tree);
    BDTScore.ReadFrom(tree);
  }

  /**
   * This function can be called to connect the active variables of the object
   * to an output TTree. It can be called multiple times, then the variables
   * will be written to multiple TTrees.
   *
   * @param tree Pointer to the TTree where the variables should be written
   */
  void AnalysisTauD3PDCollection::WriteTo(TTree* tree)
  {
    m_pTauD3PDCollection->WriteTo(tree);
    hasTruthMatch.WriteTo(tree);
    ptRatio.WriteTo(tree);
    matchPt.WriteTo(tree);
    matchVisEt.WriteTo(tree);
    matchNProng.WriteTo(tree);
    matchEta.WriteTo(tree);
    matchPhi.WriteTo(tree);
    EtOverPtLeadTrk.WriteTo(tree);
    DefaultLLHScore.WriteTo(tree);
    BDTScore.WriteTo(tree);
  }

  /**
   * This is a convenience function for turning all the branches active or
   * inactive at the same time. If the parameter is set to @c true
   * then all the branches available from the input are turned active.
   * When it's set to @c true then all the variables are turned
   * inactive.
   *
   * @param active Flag behaving as explained above
   */
  void AnalysisTauD3PDCollection::SetActive(bool active)
  {
    m_pTauD3PDCollection->SetActive();
    if(active)
    {
      if(n.IsAvailable()) n.SetActive(active);
      if(hasTruthMatch.IsAvailable()) hasTruthMatch.SetActive(active);
      if(ptRatio.IsAvailable()) ptRatio.SetActive(active);
      if(matchPt.IsAvailable()) matchPt.SetActive(active);
      if(matchVisEt.IsAvailable()) matchVisEt.SetActive(active);
      if(matchNProng.IsAvailable()) matchNProng.SetActive(active);
      if(matchEta.IsAvailable()) matchEta.SetActive(active);
      if(matchPhi.IsAvailable()) matchPhi.SetActive(active);
      if(EtOverPtLeadTrk.IsAvailable()) EtOverPtLeadTrk.SetActive(active);
      if(DefaultLLHScore.IsAvailable()) DefaultLLHScore.SetActive(active);
      if(BDTScore.IsAvailable()) BDTScore.SetActive(active);
    }

  }

  /**
   * This function can be used to read in all the branches from the input
   * TTree which are set active for writing out. This can simplify writing
   * event selector codes immensely. Remember to set the desired variable
   * active before calling this function.
   */
  void AnalysisTauD3PDCollection::ReadAllActive()
  {
    m_pTauD3PDCollection->ReadAllActive();
    //    if(n.IsActive()) n();
    if(hasTruthMatch.IsActive()) hasTruthMatch();
    if(ptRatio.IsActive()) ptRatio();
    if(matchPt.IsActive()) matchPt();
    if(matchVisEt.IsActive()) matchVisEt();
    if(matchNProng.IsActive()) matchNProng();
    if(matchEta.IsActive()) matchEta();
    if(matchPhi.IsActive()) matchPhi();
    if(EtOverPtLeadTrk.IsActive()) EtOverPtLeadTrk();
    if(DefaultLLHScore.IsActive()) DefaultLLHScore();
    if(BDTScore.IsActive()) BDTScore();
  }

} // namespace D3PDReader
#endif // D3PDREADER_TauD3PDCollection_CXX
#endif // SKIM
