#!/bin/bash

SKIMDIR=/ZIH.fast/users/morgenst/LLH/Skims

sh GenerateInput.sh input_Ztautau_skimmed.xml $SKIMDIR/Ztautau/
sh GenerateInput.sh input_ZPrime250_skimmed.xml $SKIMDIR/ZPrime250/
sh GenerateInput.sh input_ZPrime500_skimmed.xml $SKIMDIR/ZPrime500/
sh GenerateInput.sh input_ZPrime750_skimmed.xml $SKIMDIR/ZPrime750/
sh GenerateInput.sh input_ZPrime1250_skimmed.xml $SKIMDIR/ZPrime1250/

sh GenerateInput.sh input_Data_skimmed.xml /raid7/users/morgenst/workarea/PhD/TauAnalysis/LLHTool/out/Skims/data/
#sh GenerateInput.sh input_Data_muon_skimmed.xml /ZIH.fast/users/TauGroup/D3PDs/data/2011/Muons_Htautaull_Skim/
