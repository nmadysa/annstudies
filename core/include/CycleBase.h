#ifndef AnalysisModul_CycleBase_H
#define AnalysisModul_CycleBase_H

// STL
#include <list>

// SFrame
#include "core/include/SCycleBase.h"
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// custom headers
class ToolBase;

/**
 * @defgroup SFrame SFrame wrapper
 * @ingroup core
 *
 * @short SFrame wrappers
 *
 * The classes CycleBase and ToolBase act as wrapper around the SCycleBase and SToolBase respectively.
 * They inherit the functionality like steering with xml files and histogram booking. Furthermore the
 * CycleBase class proivdes the possibility of handling D3PDReader object with the help of the CycleBase::m_lEntryNumber
 * variable. The ToolBase class modifies the Book and Hist method in a way that the histograms are stored
 * in a directory with the name of tool in the output file by default. Both classes support printing the
 * current configuration to the standard output and saving it in a meta data output tree.
 */

/**
 * @short base class for an analysis cycle
 * @ingroup SFrame
 *
 * This class serves as a base class in the SFrame framework. It forwards several
 * features of the SFrame classes as the configuration with xml files and the bookkeeping
 * of ROOT objects.
 * Furthermore it takes care of the right initialisation of declared tools and properties.
 * It provides the basic functionality to read D3PD samples using the D3PDReader tools.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
class CycleBase: public SCycleBase
{
  friend class ToolBase;
public:
  /**
   * @name Constructor and destructor
   */
  //@{
  /// standard constructor
  CycleBase();

  /// standard destructor
  virtual ~CycleBase();
  //@}

  /// declares a configurable property
  template<class T>
  void           DeclareProperty(const std::string& sName,T& rValue);

  /**
   * @name information about analysed data and event
   */
  //@{
  /// is the input labelled as "data"
  bool           IsData() const {return m_bIsData;}

  /// is the current event declared as valid
  bool           IsValidEvent() const {return m_bIsValidEvent;}

  /// get current event weight
  float&         GetEventWeight() {return m_fEventWeight;}
  float          GetEventWeight() const {return m_fEventWeight;}

protected:

  /// checks whether the input data was labelled as "data"
  void           SetIsData(const SInputData& id) {m_bIsData = (id.GetType() == "data");}

  /// declares the current event as (in)valid
  void           SetIsValidEvent(bool b = true) {m_bIsValidEvent = b;}
  //@}

  /**
   * @name User hooks
   *
   * customize the following functions in derived classes to
   * hook in the SFrame event loop
   */
  //@{
  /// starts custom analysis of the current event
  virtual void   AnalyseEvent(const SInputData& id,Double_t fWeight) throw(SError) {}

  /// custom finalisation at the end of the cycle  
  virtual void   FinishCycle() throw(SError) {}

  /// custom finalisation called for each input data type on each worker node
  virtual void   FinishInputData(const SInputData& id) throw(SError) {}

  /// custom finalisation called for each input data type on the master node
  virtual void   FinishMasterInputData(const SInputData& id) throw(SError) {}

  /// custom initialisation called at the beginning of the full cycle
  virtual void   StartCycle() throw(SError) {}

  /// custom initialisation called for each input data type on each worker node
  virtual void   StartInputData(const SInputData& id) throw(SError) {}

  /// custom initialisation called for each input file
  virtual void   StartInputFile(const SInputData &) throw(SError) {}

  /// custom initialisation called for each input data type on the master node
  virtual void   StartMasterInputData(const SInputData& id) throw(SError) {}
  //@}

  /**
   * @name tool handling
   */
  //@{
  /// forwards functions to all tools
  void           CallTools(void (ToolBase::*)(const SInputData&),const SInputData& rID);

  /// forwards functions to all tools
  void           CallTools(void (ToolBase::*)());

  /// declares a new tool
  void           DeclareTool(ToolBase* pTool) throw(SError);

  /// looks for a tool with the given name
  ToolBase*      GetTool(const std::string & sName) throw(SError) ;
  //@}

  /**
   * @name handling configurable properties
   */
  //@{
  /// stores the values of configurable properties to the metadata tree in the output
  void           InsertMetadata(const std::list<std::string>& lConfigs,
				const std::string sName);
  //@}

  /**
   * @name bookkeeping of ROOT objects for output
   */
  //@{
  /// books a ROOT object in the output into several directories
  template< class T >
  void           MultiBook(const T& rHisto,const std::vector<std::string> vDirectories);
  //@}

  /**
   * @name information of the cycle and the input data/event
   */
  //@{
  /// prints the configuration of the cycle and all declared tools to the standard output
  virtual void   Print();
  //@}

  //--------------------------------------------------------------------
  // configurable properties
  //--------------------------------------------------------------------

  bool           c_do_metadata;      ///< property: store metadata in output tree
  bool           c_use_tree_weight;  ///< property: use weight of input tree as starting event weight
  std::string    c_tree_name;        ///< property: name of the tree in input files

  //--------------------------------------------------------------------
  // global entry of current event in current tree
  //--------------------------------------------------------------------

  long int       m_lEntryNumber;     ///< entry in the tree of the current event

  //--------------------------------------------------------------------
  // current event weight
  //--------------------------------------------------------------------

  float          m_fEventWeight;     ///< event weight

private:

  //------------------------------------------------------------
  // SFrame steering functions
  //------------------------------------------------------------

  /// initialisation called at the beginning of the full cycle
  void           BeginCycle() throw(SError);

  /// initialisation called for each input data type on each worker node
  void           BeginInputData(const SInputData& id) throw(SError);

  /// initialisation called for each input file
  void           BeginInputFile(const SInputData& id) throw(SError);

  /// initialisation called for each input data type on the master node
  void           BeginMasterInputData(const SInputData& id) throw(SError);

  /// finalisation at the end of the cycle
  void           EndCycle() throw(SError);

  /// finalisation called for each input data type on each worker node
  void           EndInputData(const SInputData& id) throw(SError);

  /// finalisation called for each input data type on the master node
  void           EndMasterInputData(const SInputData& id) throw(SError);

  /// starts actual analysis of the current event
  void           ExecuteEvent(const SInputData& id,Double_t fWeight) throw(SError);

  //--------------------------------------------------------------------
  // helpers to DeclareProperty locally
  //--------------------------------------------------------------------

  std::list<std::string> m_lConfigs;  ///< names of configurable properties of the cycle

  /// returns the value of a configurable property
  std::string GetPropertyValue(const std::string& sName) const;

  //--------------------------------------------------------------------
  // tools declared in the cycle
  //--------------------------------------------------------------------

  std::map<std::string,ToolBase*> m_mTools;   ///< declared tools in the cycle

  typedef std::map<std::string,ToolBase*>::iterator        tool_it;
  typedef std::map<std::string,ToolBase*>::const_iterator  tool_const_it;

  //--------------------------------------------------------------------
  // private members
  //--------------------------------------------------------------------

  bool                            m_bIsData;            ///< flag: input type labelled as "data"
  bool                            m_bIsValidEvent;      ///< flag: current event declared as valid
  float                           m_fTreeWeight;        ///< weight of input tree
  std::vector<std::string>*       m_pBranchMetadata;   

  ClassDef(CycleBase,0);
}; // class CycleBase        

#include "CycleBase.icc"

#endif // AnalysisModul_CycleBase_H        
