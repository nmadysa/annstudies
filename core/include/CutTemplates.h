#ifndef AnalysisModul_CutTemplates_H
#define AnalysisModul_CutTemplates_H

// STL include(s)
#include <string>
#include <vector>

// BOOST include(s)
#ifndef __CINT__
#include "boost/function.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/lexical_cast.hpp"
#include "boost/bind.hpp"
#endif

// core include(s)
#include "TypeList.h"


#ifndef __CINT__
typedef boost::function<bool (float&)> EventCut; ///< signature of EventCut
#endif

/**
 * @short abstract class representing template for event cuts
 * @ingroup core
 *
 * This class provides the interface to a generic cut template. An EventCut
 * is a function returning a boolean value and taking a certain number of arguments.
 * On the cut template level the values for the arguments are not yet fixed.
 * Instead it provides the functionality to return a fully specified EventCut object
 * by binding the (free) arguments to certain values which are parsed from a string.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

class AbsCutTemplate
{
public:
  /// standard destructor
  virtual ~AbsCutTemplate() {}

#ifndef __CINT__
  /// binds arguments to specific values parsed from a string
  virtual EventCut BindArguments(const std::string& sArgs) const = 0;
#endif
  /// returns the name of the cut template
  const std::string& GetName() const {return m_sName;}

protected:
  /// constructor setting the name
  AbsCutTemplate(const std::string& sName): m_sName(sName) {}
  /// copy constructor
  AbsCutTemplate(const AbsCutTemplate& rCopy): m_sName(rCopy.m_sName) {}
  /// assignment operator
  AbsCutTemplate& operator=(const AbsCutTemplate& rhs)
  {
    m_sName = rhs.m_sName;
    return *this;
  }

private:
  std::string m_sName; ///< name of the cut template
};

#ifndef __CINT__

// template magic ;-)

/// @cond
template<unsigned int i>
struct Int2Type
{
  enum {value = i};
};

template<class TypeList> struct TypeList2Function;

template<>
struct TypeList2Function<TYPELIST_0>
{
  typedef EventCut Result;
};

template<typename T1>
struct TypeList2Function<TYPELIST_1(T1)>
{
  typedef boost::function<bool (float&,T1)> Result;
};

template<typename T1,typename T2>
struct TypeList2Function<TYPELIST_2(T1,T2)>
{
  typedef typename boost::function<bool (float&,T1,T2)> Result;
};

template<typename T1,typename T2,typename T3>
struct TypeList2Function<TYPELIST_3(T1,T2,T3)>
{
  typedef typename boost::function<bool (float&,T1,T2,T3)> Result;
};

template<typename T1,typename T2,typename T3,typename T4>
struct TypeList2Function<TYPELIST_4(T1,T2,T3,T4)>
{
  typedef typename boost::function<bool (float&,T1,T2,T3,T4)> Result;
};

template<typename T1,typename T2,typename T3,typename T4,typename T5>
struct TypeList2Function<TYPELIST_5(T1,T2,T3,T4,T5)>
{
  typedef typename boost::function<bool (float&,T1,T2,T3,T4,T5)> Result;
};
/// @endcond

/**
 * @short abstract class representing template for event cuts
 * @ingroup core
 *
 * Realisation of a cut template. It contains a function object of the type
 * <code> boost::function<bool (arg1_type,arg2_type,...)> </code> where the types
 * (and the number) of arguments depend on the TypeList passed as template parameter.
 *
 * @param TypeList typelist object specifiying the types of the free arguments
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

template<class TypeList>
class CutTemplate : public AbsCutTemplate
{
public:
  /// standard constructor taking the name and template function
  CutTemplate(const std::string& sName,const typename TypeList2Function<TypeList>::Result& rCut):
    AbsCutTemplate(sName), m_oCut(rCut) {}
  /// copy constructor
  CutTemplate(const CutTemplate<TypeList>& rCopy): AbsCutTemplate(rCopy), m_oCut(rCopy.m_oCut) {}
  /// standard destructor
  virtual ~CutTemplate() {}
  /// assignment operator
  CutTemplate<TypeList>& operator=(const CutTemplate<TypeList>& rhs)
  {
    m_oCut = rhs.m_oCut;
    AbsCutTemplate::operator=(rhs);
    return *this;
  }

  /**
   * bind free arguments and return EventCut object
   *
   * @param sArgs values for arguments as string separated by ','
   * @return EventCut object (0 in case of error during argument binding)
   */
  virtual EventCut BindArguments(const std::string& sArgs) const
  {
    std::vector<std::string> vArgs;
    boost::split(vArgs,sArgs,boost::is_any_of(","));

    assert(vArgs.size() == Length<TypeList>::value);
    try
    {
      return BindHelper(vArgs,Int2Type<Length<TypeList>::value>());
    }
    catch(boost::bad_lexical_cast& exc)
    {
      return 0;
    }
  }

private:
  EventCut BindHelper(const std::vector<std::string>& vArgs,Int2Type<0>) const;
  EventCut BindHelper(const std::vector<std::string>& vArgs,Int2Type<1>) const;
  EventCut BindHelper(const std::vector<std::string>& vArgs,Int2Type<2>) const;
  EventCut BindHelper(const std::vector<std::string>& vArgs,Int2Type<3>) const;
  EventCut BindHelper(const std::vector<std::string>& vArgs,Int2Type<4>) const;
  EventCut BindHelper(const std::vector<std::string>& vArgs,Int2Type<5>) const;
  
  typename TypeList2Function<TypeList>::Result m_oCut;
};

/// @cond
template<class TypeList>
EventCut CutTemplate<TypeList>::BindHelper(const std::vector<std::string>&,Int2Type<0>) const
{
  return m_oCut;
}

template<class TypeList>
EventCut CutTemplate<TypeList>::BindHelper(const std::vector<std::string>& vArgs,Int2Type<1>) const
{
  return boost::bind(m_oCut,_1,
		     boost::lexical_cast<typename TypeAt<TypeList,0>::Result>(vArgs.at(0)));
}

template<class TypeList>
EventCut CutTemplate<TypeList>::BindHelper(const std::vector<std::string>& vArgs,Int2Type<2>) const
{
  return boost::bind(m_oCut,_1,
		     boost::lexical_cast<typename TypeAt<TypeList,0>::Result>(vArgs.at(0)),
		     boost::lexical_cast<typename TypeAt<TypeList,1>::Result>(vArgs.at(1)));
}

template<class TypeList>
EventCut CutTemplate<TypeList>::BindHelper(const std::vector<std::string>& vArgs,Int2Type<3>) const
{
  return boost::bind(m_oCut,_1,
		     boost::lexical_cast<typename TypeAt<TypeList,0>::Result>(vArgs.at(0)),
		     boost::lexical_cast<typename TypeAt<TypeList,1>::Result>(vArgs.at(1)),
		     boost::lexical_cast<typename TypeAt<TypeList,2>::Result>(vArgs.at(2)));
}

template<class TypeList>
EventCut CutTemplate<TypeList>::BindHelper(const std::vector<std::string>& vArgs,Int2Type<4>) const
{
  return boost::bind(m_oCut,_1,
		     boost::lexical_cast<typename TypeAt<TypeList,0>::Result>(vArgs.at(0)),
		     boost::lexical_cast<typename TypeAt<TypeList,1>::Result>(vArgs.at(1)),
		     boost::lexical_cast<typename TypeAt<TypeList,2>::Result>(vArgs.at(2)),
		     boost::lexical_cast<typename TypeAt<TypeList,3>::Result>(vArgs.at(3)));
}

template<class TypeList>
EventCut CutTemplate<TypeList>::BindHelper(const std::vector<std::string>& vArgs,Int2Type<5>) const
{
  return boost::bind(m_oCut,_1,
		     boost::lexical_cast<typename TypeAt<TypeList,0>::Result>(vArgs.at(0)),
		     boost::lexical_cast<typename TypeAt<TypeList,1>::Result>(vArgs.at(1)),
		     boost::lexical_cast<typename TypeAt<TypeList,2>::Result>(vArgs.at(2)),
		     boost::lexical_cast<typename TypeAt<TypeList,3>::Result>(vArgs.at(3)),
		     boost::lexical_cast<typename TypeAt<TypeList,4>::Result>(vArgs.at(4)));
}
/// @endcond
#endif

#endif // AnalysisModul_CutTemplates_H
