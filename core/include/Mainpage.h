// This is Doxygen documentation only

/**
 * @mainpage
 *
 * If you have any comments, suggestions, bug reports or questions, do not hesitate to contact me.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 *
 * @section Content Content
 *
 * -# @ref Overview
 * -# @ref Installation
 *    -# @ref ROOT
 *    -# @ref BOOST
 *    -# @ref SFrame
 *    -# @ref AModul
 * -# @ref Idea
 * -# @ref Features
 * -# @ref tipps
 * -# @ref config
 *
 * @section Overview Overview
 *
 * This package provides general functionality for performing a physics analysis. It is based on the SFrame package
 * and supports the idea of modular design. The key advantages of SFrame are kept and further features have been added:
 *
 * - dynamic configuration of your analysis
 * - central histogram book-keeping
 * - delegation of tasks to appropriate tools
 * - PROOF support
 * - base classes for common tasks (particle, object selection, baseline selection, control regions)
 * - automatic generation of cut flows
 *
 * The DD++ package consists of several sub-packages which are
 *
 * - core package (basic/generic functionality, interface to SFrame framework)
 * - AnalysisUtilities (tools useful for analyses)
 * - D3PDReaderObjects (classes representing different D3PD layouts)
 * - ExternalTools (further tools provided by third parties)
 *
 * @section Installation Getting started
 *
 * For running the package you need the following requirements:
 * - ROOT
 * - BOOST
 * - SFrame package
 *
 * If you miss of the prerequistes mentioned above, please have a look at the following subsections. There it is briefly explained
 * how to setup those packages.
 *
 * @subsection ROOT Setting up ROOT
 *
 * In order to setup SFrame and the package you need a correctly configured ROOT environment. Please choose the appropriate ROOT version
 * for your architecture. At our institute this can be done by:
 *
 * \code
 * module av                         // lists all available modules, look for your desired root version
 * module load root/your_version     // choose the right version
 * \endcode
 *
 * @subsection BOOST Setting up BOOST
 *
 * The Boost library can be loaded with <code>module load boost</code> which exports the environment variables <tt>BOOST_INC</tt> and <tt>BOOST_LIB</tt>.
 *
 * @subsection SFrame Setting up SFrame
 *
 * SFrame is an open-source project which can be found here http://sourceforge.net/projects/sframe/
 * This package has been tested with SFrame-03-04-20. To compile SFrame a ROOT version is needed. Make sure that
 * the SFrame package is compiled with the same ROOT version you will use with the AnalysisModul later.
 *
 * @b Instructions:
 *
 * -# go to the directory where the SFrame package should be placed
 * -# checkout a SFrame version: <tt>svn co https://sframe.svn.sourceforge.net/svnroot/sframe/SFrame/tags/SFrame-<TAG></tt> path/to/sframe (replace <TAG> by the desired version of SFrame)
 * -# go to the SFrame directory
 * -# execute: <tt>source setup.sh</tt>
 * -# build SFrame: <tt>make</tt>
 *
 * Your SFrame package should be ready now.
 *
 * @subsection AModul Installing the Software package
 *
 * In order to get the DD++ package you need to be registered as ATLAS user.
 *
 * @b Instructions:
 *
 * -# check out the needed sub-packages by doing (replace TAG by the desired versions):
 *    -# <tt> export SVNGRP=svn+ssh://YOUR_USERNAME@svn.cern.ch/reps/atlasgrp/Institutes/Dresden/SoftwarePackage/ </tt> (insert your CERN user name)
 *    -# <tt> svn co $SVNGRP/core/tags/TAG core </tt> 
 *    -# <tt> svn co $SVNGRP/AnalysisUtilities/tags/TAG AnalysisUtilities </tt> 
 *    -# <tt> svn co $SVNGRP/D3PDReaderObjects/YOUR_FORMAT/tags/TAG D3PDObjects </tt> (replace YOUR_FORMAT by either 'Tau_D3PD' or 'SMWZ_D3PD')
 *    -# <tt> svn co $SVNGRP/ExternalTool/YOUR_TOOL/tags/TAG YOUR_TOOL </tt> (replace YOUR_TOOL by the desired external tool)
 *    -# check out further external tools if needed
 *    -# important: It is expected that all sub-packages are within the same directory. If this is not the case for your directory structure, you may want to use sym links.
 * -# make sure that the SFrame <tt>setup.sh</tt> script has already been executed
 * -# go to the individual sub-package directories and build the packages with @c make
 * -# Remember to always source the SFrame setup script when you open a new shell/session before running(compiling your analysis.
 *
 * @section Idea Philosophy and workflow
 *
 * The main idea is that each cycle corresponds to one specific analysis. The analysis can be splitted into tasks which are carried out be different tools. This approach supports the re-usage of tools
 * for different analyses. Both, cycles and tools, can be configured with the help of xml files which is very convenient and let you modify your analysis quickly. The information from the config file is
 * NOT available before the Start(Begin)Cycle method (especially it is not available in the constructor of the cycle/tools).
 * Furthermore, the SFrame framework supports parallel processing of the input data using PROOF. Therefore the design of the CycleBase and TollBase class are quite similiar to that of the TSelector class.
 * When you run your cycle, the following methods are invoked in the order given below (with examples what to do there). Please note at which nodes the individual methods are executed. When you are
 * using the PROOF support, the input files are distributed to many worker nodes which carry out the actual analysis. On each worker node only part of the input data is analysed and at the end the results
 * of all worker nodes are merged. Therefore you have the ensure that all needed information are available on worker node level.
 *
 * -# StartCycle (invoked once per cycle on the master node): make cycle wide initialisation here
 * -# StartMasterInputData (invoked once on the master node): you can't do much here
 * -# StartInputData (invoked for each input data on each worker node): book histograms, make initialisation needed on each worker node
 * -# SartInputFile (invoked for each input file on each worker node): configure the reading of the input data (connect branches, set input tree)
 * -# AnalyseEvent (invoked on each worker node for all given events): implement the actual analysis of one event here
 * -# FinishInputData (invoked for each input data on each worker node): handle the result of this worker node (which might be only part of the input data)
 * -# FinishMasterInputData (invoked once on the master node): process the merged results of all worker nodes (calculate efficiencies, print cutflows)
 * -# FinishCycle (invoked once per cycle on the master node): print cycle statistics here, clean up
 *
 * @remark The methods for tools are named Begin... and End... respectively and invoked in the same order (except the AnalyseEvent method).
 *
 * You can make a cycle wide object which is configured in the Start(Begin)Cycle method available on all worker nodes in the following way:
 * - configure the ROOT object \c obj (it has to be derived from TObject) in StartCycle
 * - give this object a unique name <tt>obj.SetName("name")</tt>
 * - call <tt>AddConfigObject(&obj)</tt> (the object is copied during this call)
 * - retrieve the object on all worker nodes in the StartInputData method with: <tt>GetConfigObject("name")</tt>
 * - you have to cast the retrieved object to its actual type
 *
 * @section Features Features of the DD++ package
 *
 * Beside the advantages of the SFrame package:
 * - PROOF support
 * - histogram book-keeping
 * - configuration with XML files
 *
 * the DD++ package provides some additional base classes for common problems/tasks in a physics analysis.
 * These are in detail:
 *
 * - Particle: a very simple base class describing the four-vector properties of particles
 * - OverlapRemovalTool: exactly what the name says, a tool for removing spatial overlapping objects
 * - TPPair: a simple class representing a tag- and probe-pair
 * - BaseObjectSelector: generic class for applying a certain selection of objects and generating the resulting cut flow
 * - Region: a class representing a certain (control/signal) region defined by EventCut objects supporting cut flow generation and
 *   automatic filling of distributions of interest
 * - ToolBase and CycleBase are adaptors to the SFrame classes SCycleBase and SToolBase providing some additional
 *   support for handling D3PDReader objects and histogram booking
 *
 * @section tipps Tipps, tricks and warnings
 *
 * - You can specify the name of your library in the Makefile. Please make sure that the name given there is
 *   consistent with the settings in proof/SETUP.C (note: the actual library name is always 'lib' + name in Makefile).
 *   Furthermore load the right libraries/packages in your config file: <tt><LIBRARY Name="lib..." /></tt> and <tt><PACKAGE Name="name" /></tt>.
 * - Declare your tools in the constructor of your cycle giving them a unique name
 *   <tt>DeclareTool(new MyTool(this,"the_name"))</tt>. Then you can retrieve them later on with
 *   <tt>dynamic_cast<MyTool*>(GetTool("the_name"))</tt>.
 * - Configurable properties of tools and cycles should always be declared in the constructor. For properties of
 *   tools make sure that you add the name of the tool the property name:
 *   <tt>DeclareProperty(GetName() + "_the_property",m_property)</tt>. Now you can have several instances of one tool
 *   class (which have different names of course) with different property values. Otherwise all instances of one tool class
 *   would share the same property value.
 * - Do never rely on the IsData() method before the call of BeginInputData(). It will always return @c true.
 * - The @c DeclareProperty method does not support @c float types but only @c double.
 *
 * @section config XML config files
 *
 * Below an example config file is shown with explanations to the individual parts.
 *
 * @code
 * <?xml version="1.0" encoding="UTF-8" standalone="no" ?>
 * <!DOCTYPE JobConfiguration PUBLIC "" "JobConfig.dtd">
 * <!-- The following lines declare abbreviations for other config files which can be included. -->
 * <!-- The content of these files is copied at the position where they are used. -->
 * <!DOCTYPE JobConfiguration PUBLIC "" "JobConfig.dtd"[
 * <!ENTITY Common_libs    SYSTEM "Common_libs.xml">
 * <!ENTITY Common_config  SYSTEM "Common_config.xml"> 
 * <!ENTITY Input_data     SYSTEM "Input_data.xml">
 * ]>
 * 
 * <!--OutputLevel: Possibilities: VERBOSE, DEBUG, INFO, WARNING, ERROR, FATAL, ALWAYS -->
 * <JobConfiguration JobName="TestJob" OutputLevel="DEBUG">
 * 
 *   <!-- List of libraries to be loaded for the analysis.             -->
 *   <!-- Note the MathCore is needed to import the dictionary for the -->
 *   <!-- SParticle base class when using ROOT < 5.20.                 -->
 *   <!-- On ROOT 5.20 and newer versions you should load libGenVector -->
 *   <!-- for this purpose instead!                                    -->
 *   <!-- <Library Name="libMathCore" /> -->
 *   <Library Name="libGenVector" />
 *   <Library Name="libGraf" />
 *   <Library Name="libSFramePlugIns" />
 *   <Library Name="libSFrameUser" />
 * 
 *   <!-- Packages that have to be uploaded and enabled on the PROOF cluster. -->
 *   <!-- The standard SFrame libraries all create such a PAR package under   -->
 *   <!-- SFRAME_LIB_DIR. For each custom library that is loaded in the       -->
 *   <!-- configuration, a package also has to be defined...                  -->
 *   <Package Name="SFrameCore.par" />
 *   <Package Name="SFramePlugIns.par" />
 *   <Package Name="SFrameUser.par" />
 * 
 *   <!-- Enable the following lines if you have to use the ATLAS offline software -->
 *   <!-- environment with SFrame. The SFrameCintex library makes sure that the    -->
 *   <!-- code can use the offline Reflex dictionaries when reading the ntuples.   -->
 *   <Library Name="libCintex" />
 *   <Library Name="libSFrameCintex" />
 *   <Package Name="SFrameCintex.par" />
 * 
 *   <!-- List of cycles to be executed.                                       -->
 *   <!-- Name: specifies the class name to be executed in this cycle          -->
 *   <!-- name of Output-File: automically give by Name+Type+PostFix+".root"   -->
 *   <!-- OutputDirectory: directory to which output is written                -->
 *   <!--                  must finish with a "/"                              -->
 *   <!-- PostFix: A string that should be added to the output file name.      -->
 *   <!--          Can be useful for differentiating differently configured    -->
 *   <!--          instances of the same cycle class.                          -->
 *   <!-- RunMode: Can be "LOCAL" or "PROOF", depending on how you want to run -->
 *   <!--          your analysis.                                              -->
 *   <!-- ProofServer: Name of the PROOF server that you want to connect to.   -->
 *   <!--              Set it to "" or "lite" to run PROOF-Lite on your local  -->
 *   <!--              machine.                                                -->
 *   <!-- ProofWorkDir: When working on a "real" PROOF cluster, the worker     -->
 *   <!--               nodes write their output ntuple(s) into a local temp.  -->
 *   <!--               file. These files are merged into a single file in a   -->
 *   <!--               location accessible to all the nodes and the client    -->
 *   <!--               machine (running SFrame) as well. Such a location can  -->
 *   <!--               be specified here.                                     -->
 *   <!-- ProofNodes: Maximum number of nodes to use from the PROOF farm. (Or  -->
 *   <!--             the maximum number of cores to use in PROOF-Lite mode.)  -->
 *   <!--             When set to "-1" (default setting) all available workers -->
 *   <!--             are used.                                                -->
 *   <!-- TargetLumi: luminosity value the output of this cycle is weighted to -->
 *   <Cycle Name="FirstCycle" TargetLumi="1." RunMode="PROOF" ProofServer="lite"
 *          ProofWorkDir="" ProofNodes="-1" OutputDirectory="./" PostFix="" >
 * 
 *     <!-- list of input data for given data type                                   -->
 *     <!-- Type, Version: Type of the events. They have pretty free formats.        -->
 *     <!--       Exception is only for real data: for that use Type="data"!         -->
 *     <!--       If two InputData nodes are declared with the same Type and Version -->
 *     <!--       parameters, then their contents will end up in the same output     -->
 *     <!--       file. (Care is taken to calculate the event weights correctly in   -->
 *     <!--       case...                                                            -->
 *     <!-- Lumi: either specify it in InputData (ie. the luminosity sum of all <In> -->
 *     <!--       or specify it in each <In> separately                              -->
 *     <!--       In case both are filled the sum in <InputData> is used.            -->
 *     <!--       The values in <In> are ignored                                     -->
 *     <!--       units: [pb^-1]                                                     -->
 *     <!-- NEventsMax: optional, specifies the number of events that are looped     -->
 *     <!--             over.                                                        -->
 *     <!--             If not existant: all events of this InputData set are used   -->
 *     <!--             Please Note: NEventsMax is a mean to shorten the event loop  -->
 *     <!--                          for test runs;                                  -->
 *     <!--             the weights of the events are adapted.                       -->
 *     <!--             For final plots, i.e. maximum statistics, NEventsMax         -->
 *     <!--             shouldn't be used.                                           -->
 *     <!-- NEventsSkip: optional, specifies the number of events that should be     -->
 *     <!--              disregarded at the beginning of the InputData.              -->
 *     <!-- Cacheable: When set to "True" (the default value is "False"), SFrame     -->
 *     <!--            creates a small "cache file" describing the files in the      -->
 *     <!--            InputData. This cache is then used in consecutive runs to     -->
 *     <!--            "validate" the files before starting the execution. It can    -->
 *     <!--            speed up the startup of a job considerably when processing    -->
 *     <!--            a large number of files.                                      -->
 *     <!-- SkipValid: When set to "True" (default being "False"), the code doesn't  -->
 *     <!--            execute the regular validation of the input files at the job  -->
 *     <!--            start. It just assumes that all the specified files are there,-->
 *     <!--            and they all contain all the necessary input trees. Can only  -->
 *     <!--            be used when NEventsMax="-1" and NEventsSkip="0".             -->
 *     <!--                                                                          -->
 *     <!-- Some run-time checking is done on these parameters that they would make  -->
 *     <!-- sense, but in general be careful when using them.                        -->
 * 
 *     <InputData Type="MC" Version="Zee_1" Lumi="0." NEventsMax="2000" NEventsSkip="3000"
 *                Cacheable="True" SkipValid="False" >
 * 
 *       <!-- List of cuts that have been applied on generator level, needed for -->
 *       <!-- the weighting.                                                     -->
 *       <!-- For proper weighting the variable that was cut on must be stored,  -->
 *       <!-- in each event.                                                     -->
 *       <!-- We assume that it is stored directly in a "Tree"                   -->
 *       <!-- Formula: String formula of the cut that has been applied           -->
 *       <!--<GeneratorCut Tree="FullRec0" Formula="MissingEt>10000" />-->
 * 
 *       <!-- List of input files                -->
 *       <!-- Lumi: optional, see comments above -->
 *       <In FileName="/afs/cern.ch/atlas/maxidisk/d181/SFrame/StacoTau1p3p__dcache-pythiazeeSUSYView_1.AAN.root" Lumi="209.8" />
 * 
 *       <!-- Specification of the input and output trees. -->
 *       <!-- Name: Name of the tree in the ROOT file      -->
 *       <InputTree Name="FullRec0" />
 *       <InputTree Name="CollectionTree" />
 *       <OutputTree Name="FirstCycleTree" />
 *       <MetadataOutputTree Name="Electrons" />
 * 
 *     </InputData>
 *     <InputData Type="MC" Version="Zee_2" Lumi="0." NEventsMax="-1" SkipValid="True" >
 * 
 *       <!-- List of cuts that have been applied on generator level, needed for -->
 *       <!-- the weighting                                                      -->
 *       <!--<GeneratorCut Tree="FullRec0" Formula="MissingEt>5000" />
 *       <GeneratorCut Tree="FullRec0" Formula="SumEt>200000" />-->
 * 
 *       <!-- list of input files -->
 *       <In FileName="/afs/cern.ch/atlas/maxidisk/d181/SFrame/StacoTau1p3p__dcache-pythiazeeSUSYView_1.AAN.root" Lumi="209.8" />
 * 
 *       <!-- Specification of the input and output trees. -->
 *       <InputTree Name="FullRec0" />
 *       <InputTree Name="CollectionTree" />
 *       <OutputTree Name="FirstCycleTree" />
 *       <MetadataOutputTree Name="Electrons" />
 * 
 *     </InputData>
 * 
 *     <!-- User configuration: properties                                -->
 *     <!--  The user can assign various types of C++ objects to property -->
 *     <!--  names in the constructor of the cycle. The properties are    -->
 *     <!--  then set automatically in the C++ code according to the      -->
 *     <!--  configuration in the XML file.                               -->
 *     <!-- Name: Name of the property (as set in C++)                    -->
 *     <!-- Value: String representation of the value                     -->
 *     <UserConfig>
 *       <Item Name="TestString" Value="It works!" />
 *       <Item Name="TestInt" Value="666" />
 *       <Item Name="TestDouble" Value="3.141592" />
 *       <Item Name="TestBool" Value="True" />
 *       <Item Name="TestIntVector" Value="5 4 3 2 1" />
 *       <Item Name="TestDoubleVector" Value="3.141592 2.718281" />
 *       <Item Name="TestStringVector" Value="one two three" />
 *       <Item Name="TestBoolVector" Value="True False 1 0" />
 *       <Item Name="RecoTreeString" Value="FullRec0" />
 *       <Item Name="MetaTreeName" Value="Electrons" />
 *     </UserConfig>
 * 
 *   </Cycle>
 * 
 * </JobConfiguration>
 * @endcode
 */
