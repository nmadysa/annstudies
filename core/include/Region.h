#ifndef AnalysisModul_Region_H
#define AnalysisModul_Region_H

// STL
#include <vector>
#include <map>

// Boost
#ifndef __CINT__
#include "boost/function.hpp"
#include "boost/bind.hpp"
#endif // __CINT__

// SFrame
#include "core/include/SCycleBase.h"
#include "core/include/SInputData.h"
#include "core/include/SError.h"

// ROOT
#include "TH1.h"

// custom header
#include "ToolBase.h"
class CycleBase;
#include "CutTemplates.h"
#include "BoundHistogram.h"

/**
 * @short class describing a certain region in your analysis
 * @ingroup core
 *
 * This class encapsulate the description of regions in your analysis. A region is defined as all
 * events passing the set of associated cuts. The region object has a list of cut names which is used
 * assemble cut objects of type EventCut from the cut templates registered in the CutTemplateManager.
 * The cuts are defined in the xml file by
 *
 * <code>
 * <Item Name="REGION_NAME_cuts" value = "CutName1(Arg1,Arg2) CutName2() CutName3(Arg)" />
 * </code>
 *
 * The given arguments (type, order, number) have to be compatible with the signature of the CutTemplate
 * which is stored un the given name in the CutTemplateManager.
 *
 * A cut flow for the region is created automatically. Furthermore it is possible to define
 * distributions of interest which are filled automatically after successfully passed cuts.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
class Region : public ToolBase
{
public:
  /// constructor specifying the parent and name of the toolbase
  Region(CycleBase* pParent,const char* sName);

  /// standard destructor
  virtual ~Region();

  /// books histograms for cut flow
  virtual void                      BeginInputData(const SInputData& id) throw(SError);

  /// triggers the cut flow printing
  virtual void                      EndMasterInputData(const SInputData& id) throw(SError);
  
  /// returns list of cut names defining the region
  const std::vector<std::string>&   GetCutNames() const {return m_vCutNames;}

  /// checks all cuts and fills associated distributions
  bool                              IsInRegion(float& fWeight,bool bSkipEvent = false);

  /// remove all stored distributions
  void                              ClearDistributions();
  
  /// adds distribution of interest
  template<class FillPolicy>
  void                              AddDistribution(const TH1& rHist,const FillPolicy& rFill,std::string sCut="");

  /// adds distribution of interest
  template<class FillPolicy>
  void                              AddDistribution(const TH1& rHist,const FillPolicy& rFill,const std::vector<std::string>& vCuts);
  
protected:
  std::vector<std::string>                      m_vCutNames;      ///< list of cut names defining the region
  std::map<std::string,std::vector<AbsBH*> >    m_mDistributions; ///< map with histograms of interest for each cut name
#ifndef __CINT__
  std::vector<EventCut>                         m_vCuts;          ///< vector of cut objects defining the region
#endif
  
private:
  /// prints cut flow to standard output
  void    PrintCutflow(const SInputData& id);
};

/**
 * adds a distribution of interest after a certain cut
 *
 * @attention The given histogram serves only as template and is internally copied.
 *
 * @param rHist template for histogram (name has to be unique within the region)
 * @param rFill fill policy derived from AbsFillPolicy which defines how the associated histogram is filled
 * @param sCut name after which cut the given distribution should be filled (if empty, the distribution is filled after all cuts)
 */
template<class FillPolicy>
void Region::AddDistribution(const TH1& rHist,const FillPolicy& rFill,std::string sCut)
{
  // for convenience
#ifndef __CINT__
  const boost::function<TH1* (const char*)> THIST = boost::bind(&Region::Hist,this,_1,(const char*)0);
#endif // __CINT__  
  typedef BoundHistogram<_AccessByName,FillPolicy> myBH;

  // preparing all needed pointers and iterators
  std::vector<std::string>::iterator sit;
  std::map<std::string,std::vector<AbsBH*> >::iterator mit;
  AbsBH* pBH = 0;
  _AccessByName rAccess("",THIST);
  
  // book histograms for all cuts
  if(sCut.empty())
  {
    sit = m_vCutNames.begin();
    // book histogram for distribution before ALL cuts
    Book(rHist,"ALL");
    rAccess.SetName(std::string("ALL/") + rHist.GetName());
    m_mDistributions["ALL"].push_back(new myBH(rAccess,rFill));
    for(; sit != m_vCutNames.end(); ++sit)
    {
      // create bound histogram for current cut name
      Book(rHist,sit->c_str());
      rAccess.SetName(*sit + "/" + rHist.GetName());
      pBH = new myBH(rAccess,rFill);
      // look for already defined distributions after the current cut name
      mit = m_mDistributions.find(*sit);
      if(mit == m_mDistributions.end())
	m_mDistributions[*sit] = std::vector<AbsBH*>(1,pBH);
      else
	mit->second.push_back(pBH);
    }
  }
  // book distribution only after specific cuts
  else
  {
    bool bSubsequent = (sCut.at(sCut.length()-1) == '+');
    if(bSubsequent)
      sCut.erase(sCut.length()-1);

    bool bDone = false;
    sit = m_vCutNames.begin();
    while(sit != m_vCutNames.end())
    {
      if(sCut == sit->substr(0,sit->find_first_of("(")) || (bDone && bSubsequent))
      {
	bDone = true;
	
	Book(rHist,sit->c_str());
	rAccess.SetName(*sit + "/" + rHist.GetName()); 
	pBH = new myBH(rAccess,rFill);
	
	mit = m_mDistributions.find(*sit);
	if(mit == m_mDistributions.end())
	  m_mDistributions[*sit] = std::vector<AbsBH*>(1,pBH);
	else
	  mit->second.push_back(pBH);

	if(!bSubsequent)
	  break;
      }

      ++sit;
    }
    
    // given cut name does not exits
    if(!bDone)
    {
      m_logger << WARNING << "cut with name " << sCut << " not found in Region " << GetName() << SLogger::endmsg;
      m_logger << WARNING << "failed to book histogram " << rHist.GetName() << " after cut " << sCut << SLogger::endmsg;
    } 
  }
}

/**
 * adds a distribution of interest after a list of cuts
 *
 * @attention The given histogram serves only as template and is internally copied.
 *
 * @param rHist template for histogram (name has to be unique within the region)
 * @param rFill fill policy derived from AbsFillPolicy which defines how the associated histogram is filled
 * @param vCuts list of cut names after which the given distribution should be filled
 */
template<class FillPolicy>
void Region::AddDistribution(const TH1& rHist,const FillPolicy& rFill,const std::vector<std::string>& vCuts)
{
  std::vector<std::string>::const_iterator cit = vCuts.begin();
  for(; cit != vCuts.end(); ++cit)
    AddDistribution(rHist,rFill,*cit);
}

#endif // AnalysisModul_Region_H
