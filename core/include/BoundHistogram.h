#ifndef AnalysisModul_BoundHistogram_H
#define AnalysisModul_BoundHistogram_H

// STL include(s)
#include <string>
#include <assert.h>

// BOOST include(s)
#ifndef __CINT__
#include "boost/function.hpp"
#endif // __CINT__

// ROOT include(s)
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"

/**
 * @short abstract base class for acces policies
 * @ingroup core
 */
class AbsAccessPolicy
{
public:
  /// standard destructor
  virtual ~AbsAccessPolicy() {};
  /// accessing the underlying histogram
  virtual TH1* Hist() = 0;
};

/**
 * @short abstract base class for fill policies
 * @ingroup core
 */
class AbsFillPolicy
{
public:
  /// standard destructor
  virtual ~AbsFillPolicy() {}
  /// prescription how to fill the histogram with a given weight
  virtual void Fill(TH1*,float) = 0;
};

/**
 * @short abstract base class of bound histograms
 * @ingroup core
 *
 * In order to be able to store bound histograms objects with different fill and/or
 * access policies in the same container, this (template independent) base class
 * can be used.
 */
class AbsBH
{
public:
  /// standard destructor
  virtual ~AbsBH() {}
  /// fill the bound histogram with weight
  virtual void Fill(float) = 0;
};

#ifndef __CINT__

/**
 * @short class describing an histogram with bound axis values
 * @ingroup core
 *
 * This class provides the functionality of binding histograms. Histograms
 * are usually filled with different weights but "same" axis values. "Same" in
 * this sense means not that the values are identical but derived according
 * to the same prescription. For instance, an histogram showing the pt spectrum
 * of the leading electron is always filled with the pt of the hardest electron
 * (which of course can differ from event to event) and a certain weight (due to
 * some correction factors or 1). Whereas the weight can not be easily predicted,
 * it is possible to give a description on how to get the pt of the hardest electron.
 * This idea is implemented in the BoundHistogram class. It is assembled out of two
 * policies:
 * - AccessPolicy: how the underlying histogram can be accessed. In SFrame this is
 *   usually done by using the unique name of the histogram.
 * - FillPolicy: a prescription how to get the axis values when filling the histogram.
 *
 * By using this design, a bound histogram can be filled with the call BoundHistogram::Fill
 * where only the weight has to be passed. All other information necessary for doing a fill
 * are already stored inside the bound histogram object. This makes it much easier to handle
 * different histograms with complex filling routines in an uniform manner.
 *
 * This class encapsulates the idea of a bound histogram which means
 * that all information necessary for performing a fill except the fill
 * weight are assembled inside this class.
 *
 * @param AccessPolicy a class derived from AbsAccessPolicy implementing
 *                     the access to the underlying histogram
 * @param FillPolicy a class derived from AbsFillPolicy implementing a
 *                   prescription how to receive the axis values for filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<class AccessPolicy,class FillPolicy>
class BoundHistogram : public AccessPolicy, public FillPolicy, public AbsBH
{
  using AccessPolicy::Hist;
  using FillPolicy::Fill;
  
public:
  /// standard constructor
  BoundHistogram():AccessPolicy(),FillPolicy() {}
  /// constructor initialising the access and fill policies
  BoundHistogram(const AccessPolicy& rAccess,const FillPolicy& rFill):
    AccessPolicy(rAccess),FillPolicy(rFill)
  {}
  
  /// copy constructor
  BoundHistogram(const BoundHistogram& copy):
    AccessPolicy(copy),FillPolicy(copy)
  {}
  
  /// standard destructor
  virtual ~BoundHistogram() {}

  /// assignment operator
  BoundHistogram& operator=(const BoundHistogram& rhs)
  {
    AccessPolicy::operator=(rhs);
    FillPolicy::operator=(rhs);

    return *this;
  }

  /// fills the histogram with weight
  virtual void Fill(float fWeight = 1)
  {
    Fill(Hist(),fWeight);
  }
};

/**
 * @short implements an AccessPolicy for BoundHistogram based on the name of the histogram
 * @ingroup core
 *
 * The access is based on the knowledge of the histogram name and a functor of type
 * <code>boost::function<TH1* (const char*)></code> which returns a pointer
 * to an histogram identified by a certain name.
 *
 * The histogram is not owned by this class.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
class _AccessByName : public AbsAccessPolicy
{
protected:
  typedef boost::function<TH1* (const char*)> THIST; ///< functor for accessing the histogram
public:
  /// standard constructor
  _AccessByName() {}
  /// constructor setting name of the histogram and access method
  _AccessByName(const std::string& sName,const THIST& rHist):
    m_sName(sName),m_rHist(rHist)
  {}
  
  /// copy constructor
  _AccessByName(const _AccessByName& copy):
    m_sName(copy.m_sName),m_rHist(copy.m_rHist)
  {}
  
  /// standard constructor
  virtual ~_AccessByName() {}

  /// assignment operator
  _AccessByName& operator=(const _AccessByName& rhs)
  {
    m_sName = rhs.m_sName;
    m_rHist = rhs.m_rHist;

    return *this;
  }
  
  /// returns the histogram identifier
  std::string   GetName() const {return m_sName;}
  /// sets the histogram identifier
  void          SetName(const std::string& sName) {m_sName = sName;}
  /// sets the access functor
  void          SetRetrievalMethod(const THIST& rHist) {m_rHist = rHist;}

  /// returns the underlying histogram
  virtual TH1* Hist()
  {
    return m_rHist(m_sName.c_str());
  }
  
private:
  std::string   m_sName;    ///< unique identifier for underlying histogram
  THIST         m_rHist;    ///< histogram access functor
};

/**
 * @short implements an AccessPolicy for BoundHistogram based on direct access to the histogram
 * @ingroup core
 *
 * The class owns an histogram and can therefore grant direct access.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
class _DirectAccess : public AbsAccessPolicy
{
public:
  /// standard constructor
  _DirectAccess():m_pHist(0) {}
  /// constructor setting the histogram pointer (ownership is transferred to this class)
  _DirectAccess(TH1* pHist):m_pHist(pHist) {}
  /// copy constructor
  _DirectAccess(const _DirectAccess& copy):
    m_pHist((TH1*)(copy.m_pHist->Clone()))
  {}

  /// standard destructor deleting the associated histogram
  virtual ~_DirectAccess() {delete m_pHist;}

  /// assignment operator
  _DirectAccess& operator=(const _DirectAccess& rhs)
  {
    delete m_pHist;
    m_pHist = (TH1*)(rhs.m_pHist->Clone());

    return *this;
  }
  
  /// sets the histogram pointer (ownership is transferred to this class)
  void   SetHist(TH1* pHist) {m_pHist = pHist;}

  /// returns the histogram
  virtual TH1* Hist()
  {
    return m_pHist;
  }
  
protected:
  TH1* m_pHist;  ///< underlying histogram
};

/**
 * @short implements a FillPolicy for BoundHistogram
 * @ingroup core
 *
 * This FillPolicy can be used to bind the x-value of 1D histograms
 * to a simple value (functor, (member)function).
 *
 * @param T type of the x-value for filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<typename T = float>
class _Fill1DFunction : public AbsFillPolicy
{
protected:
  typedef boost::function<T ()> TXVALUE; ///< functor type returning an axis value
public:
  /// standard constructor
  _Fill1DFunction() {}
  /// constructor setting the functor for the x-value
  _Fill1DFunction(const TXVALUE& rXFunctor):m_rXFunctor(rXFunctor) {}
  /// copy constructor
  _Fill1DFunction(const _Fill1DFunction& copy):m_rXFunctor(copy.m_rXFunctor) {}
  /// standard destructor
  virtual ~_Fill1DFunction() {}

  /// assignement operator
  _Fill1DFunction& operator=(const _Fill1DFunction& rhs)
  {
    m_rXFunctor = rhs.m_rXFunctor;

    return *this;
  }
  
  /// sets the functor returning the x-value when filling the histogram
  void SetXFunctor(const TXVALUE& rXFunctor) {m_rXFunctor = rXFunctor;}
  
  /// fills given histogram with weight
  virtual void Fill(TH1* pHist,float fWeight = 1)
  {
    assert(pHist);
    pHist->Fill(m_rXFunctor(),fWeight);
  }

protected:
  TXVALUE m_rXFunctor; ///< functor returning the x-value for filling
};

typedef _Fill1DFunction<> Fill1DFunction;

/**
 * @short implements a FillPolicy for BoundHistogram
 * @ingroup core
 *
 * This FillPolicy can be used to bind the x- and y-value of 2D histograms
 * to simple values (functor, (member)function).
 *
 * @param T type of the x- for filling
 * @param U type of the y- for filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<typename T = float,typename U = T>
class _Fill2DFunction : public _Fill1DFunction<T>
{
protected:
  typedef typename _Fill1DFunction<T>::TXVALUE TXVALUE;
  typedef boost::function<U ()> TYVALUE; ///< functor type returning an y-axis value
public:
  /// standard constructor
  _Fill2DFunction() {}
  /// constructor setting the functor for the x- and y-value
  _Fill2DFunction(const TXVALUE& rXFunctor,const TYVALUE& rYFunctor):
    _Fill1DFunction<T>(rXFunctor),m_rYFunctor(rYFunctor)
  {}
  
  /// copy constructor
  _Fill2DFunction(const _Fill2DFunction& copy):
    _Fill1DFunction<T>(copy),m_rYFunctor(copy.m_rYFunctor)
  {}

  /// standard destructor
  virtual ~_Fill2DFunction() {}

  /// assignement operator
  _Fill2DFunction& operator=(const _Fill2DFunction& rhs)
  {
    _Fill1DFunction<T>::operator=(rhs);
    m_rYFunctor = rhs.m_rYFunctor;

    return *this;
  }

  /// sets the functor returning the y-value when filling the histogram
  void SetYFunctor(const TYVALUE& rYFunctor) {m_rYFunctor = rYFunctor;}
  
  /// fills given histogram with weight
  virtual void Fill(TH1* pHist,float fWeight = 1)
  {
    assert(pHist);
    assert(pHist->InheritsFrom("TH2"));
    ((TH2*)pHist)->Fill(this->m_rXFunctor(),m_rYFunctor(),fWeight);
  }

protected:
  TYVALUE m_rYFunctor; ///< functor returning the y-value for filling
};

typedef _Fill2DFunction<> Fill2DFunction;

/**
 * @short implements a FillPolicy for BoundHistogram
 * @ingroup core
 *
 * This FillPolicy can be used to bind the x-, y- and z-value of 3D histograms
 * to simple values (functor, (member)function).
 *
 * @param T type of the x--value for filling
 * @param U type of the y--value for filling
 * @param V type of the z--value for filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<typename T = float,typename U = T,typename V = U>
class _Fill3DFunction : public _Fill2DFunction<T,U>
{
protected:
  typedef typename _Fill2DFunction<T,U>::TXVALUE TXVALUE;
  typedef typename _Fill2DFunction<T,U>::TYVALUE TYVALUE;
  typedef boost::function<V ()> TZVALUE;  ///< functor type returning an z-axis value
public:
  /// standard constructor
  _Fill3DFunction() {}
  /// constructor setting the functor for the x-,  y-value and z-value
  _Fill3DFunction(const TXVALUE& rXFunctor,const TYVALUE& rYFunctor,const TZVALUE& rZFunctor):
    _Fill2DFunction<T,U>(rXFunctor,rYFunctor),m_rZFunctor(rZFunctor)
  {}
  
  /// copy constructor
  _Fill3DFunction(const _Fill3DFunction& copy):
    _Fill2DFunction<T,U>(copy),m_rZFunctor(copy.m_rZFunctor)
  {}

  /// standard destructor
  virtual ~_Fill3DFunction() {}

  /// assignement operator
  _Fill3DFunction& operator=(const _Fill3DFunction& rhs)
  {
    _Fill2DFunction<T,U>::operator=(rhs);
    m_rZFunctor = rhs.m_rZFunctor;

    return *this;
  }

  /// sets the functor returning the z-value when filling the histogram
  void SetZFunctor(const TZVALUE& rZFunctor) {m_rZFunctor = rZFunctor;}
  
  /// fills given histogram with weight
  virtual void Fill(TH1* pHist,float fWeight = 1)
  {
    assert(pHist);
    assert(pHist->InheritsFrom("TH3"));
    ((TH3*)pHist)->Fill(this->m_rXFunctor(),this->m_rYFunctor(),m_rZFunctor(),fWeight);
  }

protected:
  TZVALUE m_rZFunctor; ///< functor returning the z-value for filling
};

typedef _Fill3DFunction<> Fill3DFunction;

/**
 * @short implements a FillPolicy for BoundHistogram
 * @ingroup core
 *
 * This FillPolicy can be used to bind the x-value of 1D histograms
 * to a simple variable.
 *
 * @param T type of the x-value for filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<typename T = float>
class _Fill1DVariable : public AbsFillPolicy
{
public:
  /// standard constructor
  _Fill1DVariable():m_pXVar(0) {}
  /// constructor setting the pointer for the x-value
  _Fill1DVariable(T& rXVar):m_pXVar(&rXVar) {}
  /// copy constructor
  _Fill1DVariable(const _Fill1DVariable& copy):m_pXVar(copy.m_pXVar) {}
  /// standard destructor
  virtual ~_Fill1DVariable() {}

  /// assignement operator
  _Fill1DVariable& operator=(const _Fill1DVariable& rhs)
  {
    m_pXVar = rhs.m_pXVar;

    return *this;
  }
  
  /// sets the pointer to the x-value when filling the histogram
  void SetXVariable(T& rXVar) {m_pXVar = &rXVar;}
  
  /// fills given histogram with weight
  virtual void Fill(TH1* pHist,float fWeight = 1)
  {
    assert(pHist && m_pXVar);
    pHist->Fill(*m_pXVar,fWeight);
  }

protected:
  const T* m_pXVar; ///< pointer to the x-value for filling
};

typedef _Fill1DVariable<> Fill1DVariable;

/**
 * @short implements a FillPolicy for BoundHistogram
 * @ingroup core
 *
 * This FillPolicy can be used to bind the x- and y-value of 2D histograms
 * to simple variables.
 *
 * @param T type of the x-value for filling
 * @param U type of the y-value for filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<typename T = float,typename U = T>
class _Fill2DVariable : public _Fill1DVariable<T>
{
public:
  /// standard constructor
  _Fill2DVariable():_Fill1DVariable<T>(),m_pYVar(0) {}
  /// constructor setting the pointers for the x- and y-value
  _Fill2DVariable(T& rXVar,U& rYVar):
    _Fill1DVariable<T>(rXVar),m_pYVar(&rYVar)
  {}
  
  /// copy constructor
  _Fill2DVariable(const _Fill2DVariable& copy):
    _Fill1DVariable<T>(copy),m_pYVar(copy.m_pYVar)
  {}

  /// standard destructor
  virtual ~_Fill2DVariable() {}

  /// assignement operator
  _Fill2DVariable& operator=(const _Fill2DVariable& rhs)
  {
    _Fill1DVariable<T>::operator=(rhs);
    m_pYVar = rhs.m_pYVar;

    return *this;
  }

  /// sets the pointer to the y-value when filling the histogram
  void SetYVariable(U& rYVar) {m_pYVar = &rYVar;}
  
  /// fills given histogram with weight
  virtual void Fill(TH1* pHist,float fWeight = 1)
  {
    assert(pHist && this->m_pXVar && m_pYVar);
    assert(pHist->InheritsFrom("TH2"));
    ((TH2*)pHist)->Fill(*(this->m_pXVar),*m_pYVar,fWeight);
  }

protected:
  const U*   m_pYVar;  ///< pointer to the y-value for filling
};

typedef _Fill2DVariable<> Fill2DVariable;

/**
 * @short implements a FillPolicy for BoundHistogram
 * @ingroup core
 *
 * This FillPolicy can be used to bind the x-, y- and z-value of 3D histograms
 * to simple variables.
 *
 * @param T type of the x-value for filling
 * @param U type of the y-value for filling
 * @param V type of the z-value for filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<typename T = float,typename U = T,typename V = U>
class _Fill3DVariable : public _Fill2DVariable<T,U>
{
public:
  /// standard constructor
  _Fill3DVariable():_Fill2DVariable<T,U>(),m_pZVar(0) {}
  /// constructor setting the pointers for the x-, y-value and z-value
  _Fill3DVariable(T& rXVar,U& rYVar,V& rZVar):
    _Fill2DVariable<T,U>(rXVar,rYVar),m_pZVar(&rZVar)
  {}
  
  /// copy constructor
  _Fill3DVariable(const _Fill3DVariable& copy):
    _Fill2DVariable<T,U>(copy),m_pZVar(copy.m_pZVar)
  {}

  /// standard destructor
  virtual ~_Fill3DVariable() {}

  /// assignement operator
  _Fill3DVariable& operator=(const _Fill3DVariable& rhs)
  {
    _Fill2DVariable<T,U>::operator=(rhs);
    m_pZVar = rhs.m_pZVar;

    return *this;
  }

  /// sets the pointer to the z-value when filling the histogram
  void SetZVariable(V& rZVar) {m_pZVar = &rZVar;}
  
  /// fills given histogram with weight
  virtual void Fill(TH1* pHist,float fWeight = 1)
  {
    assert(pHist && this->m_pXVar && this->m_pYVar && m_pZVar);
    assert(pHist->InheritsFrom("TH3"));
    ((TH3*)pHist)->Fill(*(this->m_pXVar),*(this->m_pYVar),*m_pZVar,fWeight);
  }

protected:
  const V*   m_pZVar;   ///< pointer to the z-value for filling
};

typedef _Fill3DVariable<> Fill3DVariable;

/**
 * @short implements a FillPolicy for BoundHistogram
 * @ingroup core
 *
 * The class implements the filling of a 1D histogram multiple times
 * with values derived from elements in a container. For each element
 * in the container the stored functor is called which returns the
 * x-axis value (convertible to float) for this single element.
 *
 * @param Container underlying container type which is used for multiple filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<class Container,typename T = float>
class _Fill1DContainer : public AbsFillPolicy
{
protected:
  typedef typename Container::value_type Type; ///< type of objects stored in containter
  typedef boost::function<T (const Type&)> TFUNCX; ///< functor type
public:
  /// standard constructor
  _Fill1DContainer():m_pContainer(0),m_rXFunctor() {}
  /// constructor setting the underlying container and the functor
  _Fill1DContainer(const Container& rContainer,const TFUNCX& rXFunc):
    m_pContainer(&rContainer),
    m_rXFunctor(rXFunc)
  {}
  
  /// copy constructor
  _Fill1DContainer(const _Fill1DContainer& copy):
    m_pContainer(copy.m_pContainer),m_rXFunctor(copy.m_rXFunctor)
  {}

  /// standard destructor
  virtual ~_Fill1DContainer() {}

  /// assignment operator
  _Fill1DContainer& operator=(const _Fill1DContainer& rhs)
  {
    m_pContainer = rhs.m_pContainer;
    m_rXFunctor = rhs.m_rXFunctor;

    return *this;
  }

  /// sets the underlying container
  void SetContainer(const Container& rContainer) {m_pContainer = &rContainer;}
  /// sets the functor for retrieving x-axis value for each container element
  void SetXFunctor(const TFUNCX& rXFunc) {m_rXFunctor = rXFunc;}

  /// fills the histogram with a weight
  virtual void Fill(TH1* pHist,float fWeight)
  {
    assert(pHist);
    assert(m_pContainer);

    typename Container::const_iterator cit = m_pContainer->begin();
    for(; cit != m_pContainer->end(); ++cit)
      pHist->Fill(m_rXFunctor(*cit),fWeight);
  }
  
protected:
  const Container*   m_pContainer;  ///< the underlying container
  TFUNCX              m_rXFunctor;   ///< functor for retrieving x-axis values
};

/**
 * @short implements a FillPolicy for BoundHistogram
 * @ingroup core
 *
 * The class implements the filling of a 2D histogram multiple times
 * with values derived from elements in a container. For each element
 * in the container the stored functor is called which returns the
 * x- and y-axis value (convertible to float) for this single element.
 *
 * @param Container underlying container type which is used for multiple filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<class Container,typename T = float,typename U = T>
class _Fill2DContainer : public _Fill1DContainer<Container,T>
{
protected:
  typedef typename Container::value_type Type; ///< type of objects stored in containter
  typedef typename _Fill1DContainer<Container,T>::TFUNCX TFUNCX; ///< functor type
  typedef boost::function<U (const Type&)> TFUNCY; ///< functor type
public:
  /// standard constructor
  _Fill2DContainer():_Fill1DContainer<Container,T>(),m_rYFunctor() {}
  /// constructor setting the underlying container and the functor
  _Fill2DContainer(const Container& rContainer,const TFUNCX& rXFunc,const TFUNCY& rYFunc):
    _Fill1DContainer<Container,T>(rContainer,rXFunc),
    m_rYFunctor(rYFunc)
  {}
  
  /// copy constructor
  _Fill2DContainer(const _Fill2DContainer& copy):
    _Fill1DContainer<Container,T>(copy),
    m_rYFunctor(copy.m_rYFunctor)
  {}

  /// standard destructor
  virtual ~_Fill2DContainer() {}

  /// assignment operator
  _Fill2DContainer& operator=(const _Fill2DContainer& rhs)
  {
    _Fill1DContainer<Container,T>::operator=(rhs);
    m_rYFunctor = rhs.m_rYFunctor;

    return *this;
  }

  /// sets the functor for retrieving y-axis value for each container element
  void SetYFunctor(const TFUNCY& rYFunc) {m_rYFunctor = rYFunc;}

  /// fills the histogram with a weight
  virtual void Fill(TH1* pHist,float fWeight)
  {
    assert(pHist);
    assert(pHist->InheritsFrom("TH2"));
    assert(this->m_pContainer);

    typename Container::const_iterator cit = this->m_pContainer->begin();
    for(; cit != this->m_pContainer->end(); ++cit)
      ((TH2*)pHist)->Fill(this->m_rXFunctor(*cit),m_rYFunctor(*cit),fWeight);
  }
  
protected:
  TFUNCY             m_rYFunctor;   ///< functor for retrieving y-axis values
};

/**
 * @short implements a FillPolicy for BoundHistogram
 * @ingroup core
 *
 * The class implements the filling of a 3D histogram multiple times
 * with values derived from elements in a container. For each element
 * in the container the stored functor is called which returns the
 * x-, y- and z-axis value (convertible to float) for this single element.
 *
 * @param Container underlying container type which is used for multiple filling
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<class Container,typename T = float,typename U = T,typename V = U>
class _Fill3DContainer : public _Fill2DContainer<Container,T,U>
{
protected:
  typedef typename Container::value_type Type; ///< type of objects stored in containter
  typedef typename _Fill2DContainer<Container,T,U>::TFUNCX TFUNCX; ///< functor type
  typedef typename _Fill2DContainer<Container,T,U>::TFUNCY TFUNCY; ///< functor type
  typedef boost::function<V (const Type&)> TFUNCZ; ///< functor type
public:
  /// standard constructor
  _Fill3DContainer():
    _Fill2DContainer<Container,T,U>(),
    m_rZFunctor()
  {}
  
  /// constructor setting the underlying container and the functor
  _Fill3DContainer(const Container& rContainer,const TFUNCX& rXFunc,const TFUNCY& rYFunc,const TFUNCZ& rZFunc):
    _Fill2DContainer<Container,T,U>(rContainer,rXFunc,rYFunc),
    m_rZFunctor(rZFunc)
  {}
  
  /// copy constructor
  _Fill3DContainer(const _Fill3DContainer& copy):
    _Fill2DContainer<Container,T,U>(copy),
    m_rZFunctor(copy.m_rZFunctor)
  {}

  /// standard destructor
  virtual ~_Fill3DContainer() {}

  /// assignment operator
  _Fill3DContainer& operator=(const _Fill3DContainer& rhs)
  {
    _Fill2DContainer<Container,T,U>::operator=(rhs);
    m_rZFunctor = rhs.m_rZFunctor;

    return *this;
  }

  /// sets the functor for retrieving y-axis value for each container element
  void SetZFunctor(const TFUNCZ& rZFunc) {m_rZFunctor = rZFunc;}

  /// fills the histogram with a weight
  virtual void Fill(TH1* pHist,float fWeight)
  {
    assert(pHist);
    assert(pHist->InheritsFrom("TH3"));
    assert(this->m_pContainer);

    typename Container::const_iterator cit = this->m_pContainer->begin();
    for(; cit != this->m_pContainer->end(); ++cit)
      ((TH3*)pHist)->Fill(this->m_rXFunctor(*cit),this->m_rYFunctor(*cit),m_rZFunctor(*cit),fWeight);
  }
  
protected:
  TFUNCZ             m_rZFunctor;   ///< functor for retrieving z-axis values
};

#endif // __CINT__

#endif // AnalysisModul_BoundHistogram_H
