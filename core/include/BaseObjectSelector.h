#ifndef AnalysisModul_BaseObjectSelector_H
#define AnalysisModul_BaseObjectSelector_H

// STL
#include <vector>

// SFrame include
#include "core/include/SCycleBase.h"
#include "core/include/SError.h"
#include "core/include/SInputData.h"

// custom header
#include "ToolBase.h"
#include "CycleBase.h"
#include "Cuts/AbsObjectCut.h"

/**
 * @short object selector based on a sequence of AbsObjectCut objects
 * @ingroup core
 *
 * This class implements a basic object selection which is based on the successive
 * application of AbsObjectCut objects. It has to be used in an environment derived from the
 * SFrame framework.
 * The main features are the automatic generation of cutflow diagrams and its configuration
 * with xml files.
 *
 * @attention All AbsObjectCut objects of #m_vCuts are owned by this class.
 *
 * Usual workflow:
 *
 * - derive your specific selector from BaseObjectSelector
 * - add all needed AbsObjectCut objects to #m_vCuts in the BeginInputData method of your selector
 *
 * \param T type of the objects the selector is acting on
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<typename T>
class BaseObjectSelector : public ToolBase
{
 public:
  /// constructor specifying the parent cycle and the name of the tool
  BaseObjectSelector(CycleBase* pParent,const char* sName):ToolBase(pParent,sName) {}
  
  /// default destructor
  virtual ~BaseObjectSelector();

  /// book histograms for cutflow
  virtual void            BeginInputData(const SInputData& id) throw(SError);
  
  /// fill histograms for cut flow
  virtual void            EndInputData(const SInputData& id) throw(SError);
  
  /// trigger the cut flow printing
  virtual void            EndMasterInputData(const SInputData& id) throw(SError);
  
  /// apply selection on a collection of objects
  virtual void            operator()(std::vector<T*>& vObjects,float fWeight = 1);
  
  /// check a single object
  virtual bool            operator()(const T* pObject,float fWeight = 1);

 protected:
  std::vector<AbsObjectCut*>    m_vCuts;   ///< vector of abstract cut functors

  // for convenience
  typedef typename std::vector<AbsObjectCut*>::iterator cut_iter;              ///< iterator over cut vector
  typedef typename std::vector<AbsObjectCut*>::const_iterator const_cut_iter;  ///< const iterator over cut vector

 private:
  /// print cutflow
  void                    PrintCutflow();
};

#include "BaseObjectSelector.icc"

#endif // AnalysisModul_BaseObjectSelector_H
