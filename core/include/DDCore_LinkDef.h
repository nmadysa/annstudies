// file:        ZToTwoTausAnalysis_LinkDef.h
// author:      Ryan Reece       <ryan.reece@cern.ch>
//              Stan Lai         <stan.lai@cern.ch>
//              Justin Griffiths <griffith@cern.ch>
// created:     February 2010
//------------------------------------------------------------------------------
// ZToTwoTausAnalysis_LinkDef.h

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;

#pragma link C++ class CycleBase+;
#pragma link C++ class ToolBase+;

#pragma link C++ class std::vector<std::vector<int> >+;
#pragma link C++ class std::vector<std::vector<unsigned int> >+;
#pragma link C++ class std::vector<std::vector<long> >+;
#pragma link C++ class std::vector<std::vector<unsigned long> >+;
#pragma link C++ class std::vector<std::vector<float> >+;
#pragma link C++ class std::vector<std::vector<double> >+;
#pragma link C++ class std::vector<std::string>+;
#pragma link C++ class std::vector<std::vector<std::string> >+;
//#pragma link C++ class std::map<std::string, int> >+;

#endif // __CINT__
