#ifndef AnalysisModul_ToolBase_H
#define AnalysisModul_ToolBase_H

// STL
#include <list>
#include <algorithm>

// SFrame
#include "core/include/SCycleBase.h"
#include "core/include/SInputData.h"
#include "core/include/SError.h"
#include "plug-ins/include/SToolBase.h"

// ROOT
class TTree;


// core include(s)
#include "CycleBase.h"

/**
 * @Short Base class for tool which can be used in your analysis
 * @ingroup SFrame
 * @ingroup core
 *
 * This class supports the idea of modular code. It can be used to
 * split up your analysis into independent steps which are carried
 * out by different tool classes.
 * This base class provides the same convenience as the common SFrame
 * components: steering the process by xml input and histogram booking.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
class ToolBase: public SToolBaseT<CycleBase>
{
public:

  /**
   * @name Constructor and destructor
   */
  //@{
  /// standard constructor setting the log name for output
  ToolBase(CycleBase* pParent,const char* sName = "base");

  /// standard destructor
  virtual ~ToolBase(){};
  //@}

  /**
   * @name information about the tool
   */
  //@{
  /// returns the name of the current tool
  std::string       GetName() const {return m_sName;}

  /// stores all configuration attributes in a metadata tree
  void              InsertMetadata();

  /// prints the configuration of this tool
  virtual void      Print();
  //@}

  /// get event weight from parent cycle
  float&            GetEventWeight();
  float             GetEventWeight() const;
  
  /**
   * @name SFrame steering functions
   */
  //@{
  /// initialisation called at the beginning of the full cycle
  virtual void      BeginCycle() throw(SError) {}

  /// initialisation called for each input data type on each worker node
  virtual void      BeginInputData(const SInputData& id) throw(SError) {}

  /// initialisation called for each input file
  virtual void      BeginInputFile(const SInputData& id) throw(SError) {}

  /// initialisation called for each input data type on the master node
  virtual void      BeginMasterInputData(const SInputData& id) throw(SError) {}

  /// finalisation at the end of the cycle
  virtual void      EndCycle() throw(SError) {}

  /// finalisation called for each input data type on each worker node
  virtual void      EndInputData(const SInputData& id) throw(SError) {}

  /// finalisation called for each input data type on the master node
  virtual void      EndMasterInputData(const SInputData& id) throw(SError) {}
  //@}

  /**
   * @name Histogram book-keeping
   */
  //@{
  /// searches the output file for a histogram
  TH1*              Hist(const char* sName,const char* sDirectory = 0);

protected:

  //------------------------------------------------------------
  // bookkeeping of ROOT objects for output
  //------------------------------------------------------------
  
  /// books a ROOT object in the output
  template<class T>
  T*                Book(const T& rHisto,const char* sDirectory = 0) throw(SError);

  /// searches the output file for a ROOT object
  template<class T>
  T*                Retrieve(const char* sName,const char* sDirectory = 0) throw(SError);
  //@}

  /**
   * @name handling configurable properties
   */
  //@{
  /// declare a configurable property
  template<class T>
  void              DeclareProperty(const std::string& sNname,T& rValue);

  /// returns the value of a configurable property
  std::string       GetPropertyValue(const std::string& sName) const;
  //@}

  /**
   * @name access information of the parent cycle
   */
  //@{
  /// entry number in tree of current event
  const long int&   GetEntryNumber() const;

  /// looks for a tool of the parent cycle
  ToolBase*         GetTool(const std::string& sName) const throw(SError);

  /// is the input labelled as "data"
  bool              IsData() const;

  /// is the current event declared as valid
  bool              IsValidEvent() const;
  //@}

private:

  //--------------------------------------------------------------------
  // private members
  //--------------------------------------------------------------------

  std::string            m_sName;       ///< name of the tool
  std::list<std::string> m_lConfigs;    ///< names of configurable properties of the tool

  ClassDef(ToolBase,0);
}; // class ToolBase

/**
 * attaches a ROOT object to the output in the given directory
 *
 * @attention The directory is prepended by the name of the tool + '/'.
 *
 * @param rHisto the ROOT object which is attached to the output
 * @param sDirectory directory in the output in which the object is placed
 *
 * @return a pointer to the booked object
 */
template< class T >
T* ToolBase::Book(const T& rHisto,const char* sDirectory) throw(SError)
{
  // use tool name as directory
  std::string dir = m_sName;

  // use subfolder?
  if(sDirectory)
    dir += "/" + (std::string) sDirectory;
    
  return SToolBaseT<CycleBase>::Book(rHisto,dir.c_str());
}

/**
 * declares a configurable properties whose value is read in from the xml config file
 *
 * @attention The name of the property is prepended by the name of the tool + '/'.
 *
 * @param sName name of the property (has to be the same as in the xml file)
 * @param rValue default value which is overwritten by the value in the xml file
 */
template<class T>
void ToolBase::DeclareProperty(const std::string& sName,T& rValue) 
{
  // property not yet declared
  std::list<std::string>::iterator it = std::find(m_lConfigs.begin(),
						  m_lConfigs.end(),sName);
  if(it == m_lConfigs.end())
  {
    SToolBaseT<CycleBase>::DeclareProperty(sName,rValue);
    m_lConfigs.push_back(sName);
  }
  else
    m_logger << ERROR << GetName() << ": property with name " << sName << " already declared" << SLogger::endmsg;  
} 

#endif // AnalysisModul_ToolBase_H        

  
