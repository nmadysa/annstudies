#ifndef AnalysisModul_AbsObjectCut_H
#define AnalysisModul_AbsObjectCut_H

// STL
#include <functional>

/**
 * @short abstract class describing the basic features of an object cut
 * @ingroup core
 *
 * The basic idea is the generalisation of the behaviour of object cuts.
 * An object of interest is handed over to the cut and is accepted or rejected.
 * This behaviour is implemented in the operator().
 *
 * @attention Do NOT derive directly from this class but from CutWrapper. The cut
 *            method of this class (operator()) does not specify the type of object
 *            which is investigated. This design was chosen to keep the possibility
 *            of storing different cut objects in the same STL container.
 *
 * <b>Explanation</b>
 *
 * A pt cut object takes as input an object of type Particle. But a cut on the
 * electron quality criteria needs an electron object as input. When writing an
 * electron selector it would be convenient to have all electron cuts in ONE
 * container, including the pt and the quality cut. This would not be possible
 * if the AbsObjectCut class takes the type of object on which the cut is applied
 * as template parameter.
 * \code
 * AbsObjectCut<Particle> ptCut;
 * AbsObjectCut<Electron> QualityCut;
 *
 * list<AbsObjectCut<T> > ListOfCuts; // which parameter T should one choose here?
 * \endcode
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

class AbsObjectCut : public std::unary_function<void*,bool>
{
public:
  /// standard constructor setting the name of the cut object
  AbsObjectCut(std::string sName):m_fPassed(0),m_iPassedRaw(0),m_fTotal(0),
			   m_iTotalRaw(0),m_sName(sName) {}

  /// standard destructor
  virtual ~AbsObjectCut() {}

  /// returns the name of the cut object
  std::string    GetName() const {return m_sName;}

  /// returns the (weighted) number of passed objects
  float          GetPassed() const {return m_fPassed;}

  /// returns the (raw) number of passed objects
  unsigned int   GetPassedRaw() const {return m_iPassedRaw;}

  /// returns the (weighted) number of all objects tested
  float          GetTotal() const {return m_fTotal;}

  /// returns the (raw) number of all objects tested
  unsigned int   GetTotalRaw() const {return m_iTotalRaw;}

  /// applies the cut giving it a certain weight
  virtual bool   operator()(const void*,float weight = 1) = 0;

  /// resets the number of tested and passed objects
  void           Reset() {m_fPassed = 0; m_iPassedRaw = 0; m_fTotal = 0; m_iTotalRaw = 0;}

protected:
  float          m_fPassed;      ///< (weighted) number of objects passing the cut
  unsigned int   m_iPassedRaw;   ///< (raw) number of objects passing the cut
  float          m_fTotal;       ///< (weighted) number of objects being tested
  unsigned int   m_iTotalRaw;    ///< (raw) number of objects being tested
  std::string    m_sName;        ///< name of the cut object
};

#endif // AnalysisModul_AbsObjectCut_H
