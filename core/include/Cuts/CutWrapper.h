#ifndef AnalysisModul_CutWrapper_H
#define AnalysisModul_CutWrapper_H

// custom header
#include "AbsObjectCut.h"

/**
 * @short helper class forwarding the cut operator
 * @ingroup core
 *
 * This class is only used as intermediate step and forwards the generic cut
 * functor of the abstract AbsObjectCut class to an operator which a specific
 * type of the input object.
 *
 * Derive your own cut class from CutWrapper and implement the pure virtual
 * method PassCut(). It has to return @c true if the object passes the cut,
 * otherwise @c false.
 *
 * @param T type of object which the cut acts on
 */

template<typename T>
class CutWrapper : public AbsObjectCut
{
public:
  /// standard constructor setting name of the cut object
  CutWrapper(std::string name): AbsObjectCut(name) {}

  /// standard destructor
  virtual ~CutWrapper() {}

  /// forwards the generic cut method
  virtual bool operator()(const void* arg,float weight)
  {
    return operator()(static_cast<T*>(arg),weight);
  }
  
protected:
  /// handles cut with specific type of input object
  virtual bool operator()(const T*,float);

  /// defines actual cut on object
  virtual bool PassCut(const T*) = 0;
};

/**
 * applies the cut and handles the cut statistics
 *
 * @param pArg (valid) pointer to the object under investigation
 * @param fWeight current histogram weight
 *
 * @return result of the cut (@c true if the object passes the cut)
 */
template<typename T>
bool CutWrapper<T>::operator()(const T* pArg,float fWeight)
{
  m_fTotal += fWeight;
  ++m_iTotalRaw;

  if(PassCut(pArg))
  {
    m_fPassed += fWeight;
    ++m_iPassedRaw;
    return true;
  }
  else
    return false;
}

#endif // AnalysisModul_CutWrapper_H
