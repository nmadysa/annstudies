#ifndef AnalysisModul_CutTemplateManager_H
#define AnalysisModul_CutTemplateManager_H

// STL include(s)
#include <string>
#include <map>

// core include(s)
#include "CutTemplates.h"

/**
 * @short singleton class managing cut templates
 * @ingroup core
 *
 * This class is responsible for handling the various cut templates defined.
 * It is implemented as singleton.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */

class CutTemplateManager
{
public:
  /// access the CutTemplateManager instance
  static CutTemplateManager* GetInstance()
  {
    static MemoryGuard g;

    if(!m_pInstance)
      m_pInstance = new CutTemplateManager();
    
    return m_pInstance;
  }

  /**
   * register a cut template
   *
   * @attention The ownership of the AbsCutTemplate object is transferred to the CutTemplateManager.
   * @warning If there is already cut template stored with the same name, it gets overwritten. This
   *          might result in a memory leak.
   *
   * @param pTemplate (valid) pointer to cut template
   */
  void RegisterCutTemplate(const AbsCutTemplate* pTemplate)
  {
    assert(pTemplate);
    m_mTemplates[pTemplate->GetName()] = pTemplate;
  }

  /**
   * @param sName name of required cut template
   * @return pointer to cut template with given name (0 if no template with given name exists)
   */
  const AbsCutTemplate* GetTemplate(const std::string& sName) const
  {
    std::map<std::string,const AbsCutTemplate*>::const_iterator cit = m_mTemplates.find(sName);
    if(cit != m_mTemplates.end())
      return cit->second;
    else
      return 0;
  }
  
private:
  /// @cond
  CutTemplateManager() {}
  CutTemplateManager(const CutTemplateManager& rCopy) {}
  ~CutTemplateManager()
  {
    std::map<std::string,const AbsCutTemplate*>::iterator it = m_mTemplates.begin();
    for(; it != m_mTemplates.end(); ++it)
      delete it->second;

    m_mTemplates.clear();
  }
  
  CutTemplateManager& operator=(const CutTemplateManager&) {return *this;}

  class MemoryGuard
  {
  public:
    ~MemoryGuard()
    {
      if(CutTemplateManager::m_pInstance)
      {
	delete CutTemplateManager::m_pInstance;
	CutTemplateManager::m_pInstance = 0;
      }
    }
  };

  friend class MemoryGuard;
  static CutTemplateManager* m_pInstance;

  std::map<std::string,const AbsCutTemplate*> m_mTemplates;
  /// @endcond
};

#endif // AnalysisModul_CutTemplateManager_H
