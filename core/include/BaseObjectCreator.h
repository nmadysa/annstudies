#ifndef AnalysisModul_BaseObjectCreator_H
#define AnalysisModul_BaseObjectCreator_H

// STL
#include <vector>

/**
 * @defgroup core core package
 *
 * @short basic/generic functionality
 *
 * The core package contains classes which
 *
 * - provide generic functionality for analysis tasks
 * - handle information from D3PD input
 * - interface the SFrame framework.
 */

/**
 * @defgroup D3PDReader D3PDReader utilities
 * @ingroup core
 *
 * @short D3PD related classes
 *
 * The classes within this group are not designed for handling generic input formats but rather for
 * dealing with the D3PDReader concept. The main advantage of the D3PDReader approach is the read-on-
 * demand mechanism which greatly improves the performance. Especially one does not have to specify/activate
 * all needed branches at the beginning of the program. All branches in the tree are available and read
 * in when their information is needed.
 *
 * A tool generating D3PDReader classes from a given ROOT file/TTree object is available
 * <a href=http://iktp.tu-dresden.de/~software/D3PDReaderMaker/index.html >here</a>.
 */

/**
 * @short simple class for creating physics analysis objects
 * @ingroup D3PDReader
 *
 * This class is responsible for the ownership handling of the physics analysis
 * objects used in your analysis. It provides basic functionality for creating
 * and deleting objects. The physic analysis objects are created and connected
 * to the corresponding D3PDReader collection which handles the access to the
 * underlying TTree.
 *
 * @param T class of physics analysis object which supports a constructor with
 *          the following signature \code T(unsigned int,const Collection&) \endcode
 *
 * @attention All created objects are owned by an instance of this class.
 * @attention This class can not be copied.
 *
 * @warning The class is NOT designed for various input formats but rather for
 *          data in D3PD format based on the D3PDReader classes.
 *
 * @author Christian Gumpert <cgumpert@cern.ch>
 */
template<class T>
class BaseObjectCreator
{
public:
  /// standard constructor
  BaseObjectCreator() {}
  
  /// destructor destroys all owned objects
  ~BaseObjectCreator() {ClearAllObjects();}
  
  /// delete all stored objects
  void                     ClearAllObjects();
  
  /// create vector of objects from given collection
  template<typename Collection>
  unsigned int             CreateObjects(unsigned int n,Collection& rCollection,std::vector<T*>& vObjects);
  
  /// number of currently owned objects
  unsigned int             GetNumberOfObjects() const {return m_vObjects.size();}
  
  /// access vector of stored objects
  const std::vector<T*>&   GetObjects() const {return m_vObjects;}

private:
  /// private copy constructor
  BaseObjectCreator(const BaseObjectCreator&);
  /// private assignment operator
  BaseObjectCreator& operator=(const BaseObjectCreator& rhs);

  std::vector<T*> m_vObjects;   ///< vector with all owned objects
};

#include "BaseObjectCreator.icc"

#endif // AnalysisModul_BaseObjectCreator_H
