#ifndef DDCore_CutTemplates_CXX
#define DDCore_CutTemplates_CXX

// core include(s)
#include "CutTemplates.h"

template<>
EventCut CutTemplate<TYPELIST_0>::BindArguments(const std::string& sArgs) const
{
  assert(sArgs.empty());
  return m_oCut;
}

#endif // DDCore_CutTemplates_CXX
