#ifndef AnalysisModul_ToolBase_CXX
#define AnalysisModul_ToolBase_CXX

// STL
#include <list>

// ROOT
#include <TTree.h>

// SFrame
#include "core/include/SCycleBase.h"
#include "core/include/SInputData.h"
#include "core/include/SError.h"
#include "core/include/SCycleConfig.h"

// custom headers
#include "ToolBase.h"

ClassImp(ToolBase);

/**
 * standard constructor setting the logging name for the tool to its name
 *
 * All declarations of configurable properties of the tool should be placed here.
 *
 * @param pParent pointer to the cycle the tool belongs to
 * @param sName name of the tool
 */
ToolBase::ToolBase(CycleBase* pParent,const char* sName)
  :SToolBaseT<CycleBase>(pParent),m_sName(sName)
{
  // set tool name for log messages
  SetLogName(sName);
}

/**
 * @return const reference to entry number of current event in tree
 */
const long int& ToolBase::GetEntryNumber() const
{
  return GetParent()->m_lEntryNumber;
}

/**
 * @return reference to event weight variable in parent cycle
 */
float& ToolBase::GetEventWeight()
{
  return GetParent()->GetEventWeight();
}

/**
 * @return copy of event weight variable in parent cycle
 */
float ToolBase::GetEventWeight() const
{
  return GetParent()->GetEventWeight();
}

/**
 * @attention works only properly if the property has been initiliased within the xml config file
 *
 * @param sName name of the property of interest
 *
 * @return the stored value of the configurable property with the given name as string
 */
std::string ToolBase::GetPropertyValue(const std::string& sName) const
{
  SCycleConfig::property_type properties = GetParent()->GetConfig().GetProperties();

  for(SCycleConfig::property_type::iterator it = properties.begin();
      it != properties.end(); ++ it)
  {
    if(it->first == sName)
      return it->second;
  }
  
  m_logger << DEBUG << GetName() << " : property with name " << sName << " not found" << SLogger::endmsg;
  return std::string();
}

/**
 * @param sName name of the tool of interest
 *
 * @return pointer to tool of parent cycle with given name (0 if no tool is found)
 *
 * @sa CycleBase::GetTool()
 */
ToolBase* ToolBase::GetTool(const std::string& sName) const throw(SError)
{
  return GetParent()->GetTool(sName);
}

/**
 * @attention directory is prepended by the name of the tool + '/'
 *
 * @param sName name of the histogram in the output
 * @param sDirectory directory in which the histogram can be found
 *
 * @return pointer to histogram with a given name (0 if no histogram is found)
 *
 * @sa CycleBase::Hist()
 */
TH1* ToolBase::Hist(const char* sName,const char* sDirectory /* = 0 */ )
{
  // use tool name as directory
  std::string dir = m_sName;

  // use subdirectory?
  if(sDirectory)
    dir += "/" + (std::string) sDirectory;
    
  return SToolBaseT<CycleBase>::Hist(sName,dir.c_str());
}

/**
 * inserts the values of the configurable properties of the tool in the metadata tree
 * of the parent cycle
 *
 * @sa CycleBase::InsertMetadata()
 */
void ToolBase::InsertMetadata()
{
  GetParent()->InsertMetadata(m_lConfigs,GetName());
}

/**
 * @return true if the input data is labelled as "data"
 *
 * @sa CycleBase::IsData()
 */
bool ToolBase::IsData() const
{
  return GetParent()->IsData();
}

/**
 * @return the status of the current event
 *
 * @sa CycleBase::IsValidEvent()
 */
bool ToolBase::IsValidEvent() const
{
  return GetParent()->IsValidEvent();
}

/**
 * prints the configuration of the tool to the standard output
 */
void ToolBase::Print() 
{
  m_logger << ALWAYS << "******************************" << SLogger::endmsg;
  m_logger << ALWAYS << "Tool " << GetName() << SLogger::endmsg;
  m_logger << ALWAYS << "******************************" << SLogger::endmsg;

  for(std::list<std::string>::const_iterator it = m_lConfigs.begin();
      it != m_lConfigs.end(); ++it)
    m_logger << ALWAYS << *it << "  =  " << GetPropertyValue(*it) << SLogger::endmsg;

  m_logger << ALWAYS <<  SLogger::endmsg;
}

#endif // AnalysisModul_ToolBase_CXX
