#ifndef AnalysisModul_CycleBase_CXX
#define AnalysisModul_CycleBase_CXX

//STL
#include <list>
#include <iomanip>
#include <sstream>

// ROOT
#include "TTree.h"

// SFrame
#include "core/include/SInputData.h"
#include "core/include/SError.h"
#include "core/include/SCycleConfig.h"

// custom header
#include "CycleBase.h"
#include "ToolBase.h"

ClassImp(CycleBase);

/**
 * standard constructor
 *
 * The declaration of all configurable properties and tools should be placed here.
 */
CycleBase::CycleBase():
  m_lEntryNumber(0),
  m_fEventWeight(1),
  m_bIsData(true),
  m_bIsValidEvent(true),
  m_fTreeWeight(1.0),
  m_pBranchMetadata(0)
{
  // Declare the properties of the base cycle:
  DeclareProperty("tree_name",c_tree_name);
  DeclareProperty("do_metadata",c_do_metadata = false);    
  DeclareProperty("use_tree_weight",c_use_tree_weight = false);    
}

/**
 * standard destructor
 *
 * All associated tools are destroyed.
 */
CycleBase::~CycleBase() 
{
  //delete all tools
  for(tool_it itr = m_mTools.begin();itr != m_mTools.end();++itr)
    delete itr->second;

  //delete metadata branch
  if(c_do_metadata)
    delete m_pBranchMetadata;
}

/**
 * initialisation at the beginning of the cycle
 *
 * First the user-defined StartCycle() is called and then the BeginCycle() call is forwarded
 * to all declared tools.
 *
 * The configuration of the cycle and all tools is printed.
 */
void CycleBase::BeginCycle() throw(SError)
{
  // let user function hook in
  StartCycle();
  // forward call to all tools
  CallTools(&ToolBase::BeginCycle);
  // print configuration properties of cycle and all tools
  Print();
  CallTools(&ToolBase::Print);  
}

/**
 * initialisation for each input data types on all worker nodes
 *
 * - If enabled the configuration of the cycle and all tools is stored in the metadata tree.
 * - The input data is checked whether it is labelled as "data" \f$ \rightarrow \f$ IsData() returns the proper value afterwards
 *
 * First the user-defined StartInputData() is called and then the BeginInputData() call is forwarded
 * to all declared tools.
 */
void CycleBase::BeginInputData(const SInputData& id) throw(SError)
{
  // store configuration metadata
  if(c_do_metadata)
  {
    InsertMetadata(m_lConfigs,GetName());
    CallTools(&ToolBase::InsertMetadata);
    // have to write metadata by hand
    TTree* metadata = GetMetadataTree("config");
    if(metadata)
      metadata->Fill();
  }

  // check whether input comes from data
  SetIsData(id);
  // let user function hook in
  StartInputData(id);
  // forward call to all tools
  CallTools(&ToolBase::BeginInputData,id);
}

/**
 * initialisation for each input file
 *
 * First the user-defined StartInputFile() is called and then the BeginInputFile() call is forwarded
 * to all declared tools.
 */
void CycleBase::BeginInputFile(const SInputData& id) throw(SError)
{
  // read tree weight
  m_fTreeWeight = GetInputTree(c_tree_name.c_str())->GetWeight();
  // let user function hook in
  StartInputFile(id);
  // forward to all tools
  CallTools(&ToolBase::BeginInputFile,id);
}

/**
 * initialisation for each input data on the master node
 *
 * First the user-defined StartMasterInputData() is called and then the BeginMasterInputData() call is forwarded
 * to all declared tools.
 */
void CycleBase::BeginMasterInputData(const SInputData& id) throw(SError)
{
  // let user function hook in
  StartMasterInputData(id);
  // forward to all tools
  CallTools(&ToolBase::BeginMasterInputData,id);
}

/**
 * forwards a function call to all tools
 */
void CycleBase::CallTools(void (ToolBase::*pMemFun)(const SInputData& id),const SInputData& rID)
{
  tool_it it = m_mTools.begin();
  for(; it != m_mTools.end(); ++it)
    ((it->second)->*pMemFun)(rID);
}

/**
 * forwards a function call to all tools
 */
void CycleBase::CallTools(void (ToolBase::*pMemFun)())
{
  tool_it it = m_mTools.begin();
  for(; it != m_mTools.end(); ++it)
    ((it->second)->*pMemFun)();
}

/**
 * declares a new tool associated to the cycle
 *
 * @attention If there already exists another tool with the same name, the old tool will
 *            be replaced.
 * @attention The ownership of the tool is taken by the cycle.
 *
 * @param pTool pointer to the new tool
 */
void CycleBase::DeclareTool(ToolBase* pTool) throw(SError)
{
  // check previously declared tools
  tool_it itr = m_mTools.find(pTool->GetName());
  if(itr != m_mTools.end()) 
  {
    // throw warning and replace existing tool -> potential memory leak
    m_logger << WARNING << "Repacling Previously Declared Tool " << pTool->GetName() << SLogger::endmsg;
    itr->second = pTool;
    return;
  }
  // tool does not yet exist
  m_mTools.insert(std::make_pair(pTool->GetName(),pTool));  
}

/**
 * finalisation at the end of the cycle
 *
 * First the user-defined FinishCycle() is called and then the EndCycle() call is forwarded
 * to all declared tools.
 */
void CycleBase::EndCycle() throw(SError)
{
  // let user function hook in
  FinishCycle();
  // forward to all tools
  CallTools(&ToolBase::EndCycle);
}

/**
 * finalisation called for each input data on each worker node
 *
 * First the user-defined FinishInputData() is called and then the EndInputData() call is forwarded
 * to all declared tools.
 */
void CycleBase::EndInputData(const SInputData& id) throw(SError)
{
  // let user function hook in
  FinishInputData(id);
  // forward to all tools
  CallTools(&ToolBase::EndInputData,id);
}

/**
 * finalisation called for each input data on the master node
 *
 * First the user-defined FinishMasterInputData() is called and then the EndMasterInputData() call is forwarded
 * to all declared tools.
 */
void CycleBase::EndMasterInputData(const SInputData& id) throw(SError)
{
  // let user function hook in
  FinishMasterInputData(id);
  // forward to all tools
  CallTools(&ToolBase::EndMasterInputData,id);
}

/**
 * calls the user-defined AnalyseEvent() method to investigate the current event
 *
 * #m_lEntryNumber is set to the entry of the current event in the tree
 * #m_fEventWeight is set to 1.0
 */
void CycleBase::ExecuteEvent(const SInputData& id,Double_t fWeight) throw(SError)
{
  m_lEntryNumber = id.GetEventTreeEntry();
  m_fEventWeight = (c_use_tree_weight) ? m_fTreeWeight : 1.0;
  AnalyseEvent(id,fWeight);
}

/**
 * @attention works only properly if the property has been initiliased within the xml config file
 *
 * @param sName name of the configurable property
 *
 * @return the stored value of the configurable property with the given name as string
 */
std::string CycleBase::GetPropertyValue(const std::string& sName) const
{
  SCycleConfig::property_type properties = GetConfig().GetProperties();

  for(SCycleConfig::property_type::iterator it = properties.begin();
      it != properties.end(); ++ it)
  {
    if(it->first == sName)
      return it->second;
  }
  
  m_logger << DEBUG << GetName() << " : property with name " << sName << " not found" << SLogger::endmsg;
  return std::string();
}

/**
 * looks for a tool with the given name in the list of declared tools
 *
 * @param sName name of the tool
 *
 * @return pointer to the tool (0 is no tool with the given name exists)
 */
ToolBase* CycleBase::GetTool(const std::string & sName) throw(SError)
{
  // retrieve tool from map
  tool_it itr = m_mTools.find(sName);
  if(itr == m_mTools.end()) 
  {
    m_logger << FATAL << "Cannot Find Tool: " << sName << " In Tool List" << SLogger::endmsg;
    return 0;
  }
  return itr->second;
}

/**
 * inserts the stored values of the given properties to a metadata tree in the output
 *
 * @attention works onlt if an output metadata tree with the name "config" has been declared in the xml config file
 *            \code
 *            <InputData ...>
 *             ...
 *             <MetadataOutputTree Name="config" />
 *            </InputData>
 *            \endcode
 *
 * @param lConfigs list of the names of configurable properties whose values should be stored
 * @param sName name of the tool/cycle put into the metadata
 */
void CycleBase::InsertMetadata(const std::list<std::string>& lConfigs,const std::string sName)
{
  // store configuration in metadata tree
  TTree* metadata = GetMetadataTree("config");
  if(!metadata) return;
  if(!metadata->FindBranch("config"))
  {
    m_pBranchMetadata = new std::vector<std::string>;
    metadata->Branch("config", &m_pBranchMetadata);
  }
  std::list<std::string>::const_iterator it = lConfigs.begin();
  std::list<std::string>::const_iterator itr_end = lConfigs.end();
  for( ; it != itr_end; ++it)
  {
    std::string property = GetPropertyValue(*it);
    std::string line;
    std::stringstream ss;

    ss << std::left << std::setw(25) << sName << std::left << std::setw(25) << (": " + *it) << 
      " = " << std::left << std::setw(25) << property << std::endl;

    std::getline(ss, line);
    m_pBranchMetadata->push_back(line);
  }
  m_pBranchMetadata->push_back("****************************************");
}

/**
 * prints the current configuration of the cycle to the standard output
 */
void CycleBase::Print() 
{
  // print configuration properties of cycle
  m_logger << ALWAYS << "******************************" << SLogger::endmsg;
  m_logger << ALWAYS << "Cycle " << GetName() << SLogger::endmsg;
  m_logger << ALWAYS << "******************************" << SLogger::endmsg;

  for(std::list<std::string>::const_iterator it = m_lConfigs.begin();
      it != m_lConfigs.end(); ++it)
    m_logger << ALWAYS << *it << "  =  " << GetPropertyValue(*it) << SLogger::endmsg;

  m_logger << ALWAYS <<  SLogger::endmsg;
}

#endif // AnalysisModul_CycleBase_CXX
