#ifndef AnalysisModul_Region_CXX
#define AnalysisModul_Region_CXX

// STL
#include <iomanip>
#include <vector>
#include <algorithm>
#include <functional>

#ifndef __CINT__
#include "boost/bind.hpp"
#include "boost/lambda/bind.hpp"
#endif

// SFrame
#include "core/include/SError.h"
#include "core/include/SInputData.h"
#include "core/include/SCycleBase.h"

// core include(s)
#include "Region.h"
#include "BoundHistogram.h"
#include "CutTemplateManager.h"
#include "CutTemplates.h"

/**
 * standard constructor
 *
 * @param pParent pointer to cycle object owning the region
 * @param sName name of the region
 */
Region::Region(CycleBase* pParent,const char* sName):
  ToolBase(pParent,sName)
{
  DeclareProperty(GetName() + "_cuts",m_vCutNames);
}

/**
 * deletes all associated BoundHistogram objects
 */
Region::~Region()
{
  ClearDistributions();
}

/**
 * books the histograms for storing the cut flow
 */
void Region::BeginInputData(const SInputData& id) throw(SError)
{
  // book histogram for cutflow
  Book(TH1D("h_cut_flow","cut flow",m_vCutNames.size() + 1,0,m_vCutNames.size()+1));
  Book(TH1D("h_cut_flow_raw","raw cut flow",m_vCutNames.size() + 1,0,m_vCutNames.size()+1));
  // set bin labels
  Hist("h_cut_flow")->GetXaxis()->SetBinLabel(1,"ALL");
  Hist("h_cut_flow_raw")->GetXaxis()->SetBinLabel(1,"ALL");

  // clear cut list
  m_vCuts.clear();
  
  // get cut template manager
  CutTemplateManager* pManager = CutTemplateManager::GetInstance();
  // helper variables
  std::string sCutName;
  std::string sArgs;
  size_t pos1,pos2;
  bool bInvert;
  
  for(unsigned int i = 0; i < m_vCutNames.size(); ++i)
  {
    // set bin labels according to cut name (including parameters)
    Hist("h_cut_flow")->GetXaxis()->SetBinLabel(i+2,m_vCutNames.at(i).c_str());
    Hist("h_cut_flow_raw")->GetXaxis()->SetBinLabel(i+2,m_vCutNames.at(i).c_str());

    // init EventCuts
    pos1 = m_vCutNames.at(i).find_first_of('(');
    pos2 = m_vCutNames.at(i).find_last_of(')');
    // cut name has correct format?
    if((pos1 == std::string::npos) || (pos2 == std::string::npos) || (pos2 < pos1))
    {
      m_logger << FATAL << "invalid format of cut name " << m_vCutNames.at(i) << SLogger::endmsg;
      throw SError("invalid format of cut name",SError::StopExecution);
    }
    // derive name of cut template
    sCutName = m_vCutNames.at(i).substr(0,pos1);
    // invert the cut?
    bInvert = (sCutName.at(0) == '!');
    if(bInvert)
      sCutName.erase(0,1);
    // derive argument list (without parantheses)
    sArgs = m_vCutNames.at(i).substr(pos1+1,pos2-pos1-1);
    // get cut template
    const AbsCutTemplate* pTemp = pManager->GetTemplate(sCutName);
    if(pTemp)
    {
      // bin cut template to event cut
      EventCut EvCut = pTemp->BindArguments(sArgs);
      if(EvCut)
      {
	if(bInvert)
	  EvCut = boost::bind(std::logical_not<float>(),boost::bind(EvCut,_1));

	m_vCuts.push_back(EvCut);
      }
      else
      {
	m_logger << FATAL << "error when binding arguments '" << sArgs << "' to cut template '" << sCutName << "'" << SLogger::endmsg;
	throw SError("error during argument binding",SError::StopExecution);
      }
    }
    else
    {
      m_logger << FATAL << "could not find cut template with name " << sCutName << SLogger::endmsg;
      throw SError("cut template not found",SError::StopExecution);
    }
  }
}

/**
 * triggers the cut flow printing
 *
 * @sa PrintCutflow()
 */
void Region::EndMasterInputData(const SInputData& id) throw(SError)
{
  PrintCutflow(id);
}

#ifndef __CINT__
/**
 * checks whether the current event is in the region and fills the cut flow as well as associated distributions
 *
 * @param fWeight histogram weight (may get modified by the cuts)
 * @param bSkipEvent if set to @c true, the event is skipped as soon as one associated EventCut returns @c false
 *
 * @return @c true if the current event is in the region, otherwise @c false
 */
bool Region::IsInRegion(float& fWeight,bool bSkipEvent)
{
  // fill cut flow
  Hist("h_cut_flow")->Fill("ALL",fWeight);
  Hist("h_cut_flow_raw")->Fill("ALL",1);

  // fill all distributions of interest before any cut
  std::map<std::string,std::vector<AbsBH*> >::iterator mit = m_mDistributions.find("ALL");
  if(mit != m_mDistributions.end())
  {
    std::vector<AbsBH*>::iterator bit = mit->second.begin();
    for(; bit != mit->second.end(); ++bit)
      (*bit)->Fill(fWeight);
  }
  
  // loop over all cuts, fill cut flow and associated distributions
  bool bResult = true;
  float fCorr = 1;
  for(unsigned int i = 0; i < m_vCuts.size(); ++i)
  {
    fCorr = 1;
    bResult = (m_vCuts.at(i))(fCorr);

    if(bResult)
    {
      fWeight *= fCorr;

      Hist("h_cut_flow")->Fill(m_vCutNames.at(i).c_str(),fWeight);
      Hist("h_cut_flow_raw")->Fill(m_vCutNames.at(i).c_str(),1);

      // fill all distributions
      mit = m_mDistributions.find(m_vCutNames.at(i));
      if(mit != m_mDistributions.end())
      {
	std::vector<AbsBH*>::iterator bit = mit->second.begin();
	for(; bit != mit->second.end(); ++bit)
	  (*bit)->Fill(fWeight);
      }
    }
    else
    {
      if(bSkipEvent)
	throw SError(SError::SkipEvent);

      break;
    }
  }
  
  return bResult;
}
#endif

/**
 * deletes all associated distributions
 */
void Region::ClearDistributions()
{
  std::map<std::string,std::vector<AbsBH*> >::iterator it = m_mDistributions.begin();
  std::vector<AbsBH*>::iterator bit;
  for(; it != m_mDistributions.end(); ++it)
  {
    bit = it->second.begin();
    for(; bit != it->second.end(); ++bit)
      delete *bit;
    it->second.clear();
  }
  m_mDistributions.clear();
}

/**
 * prints the cut flow to the standard output
 */
void Region::PrintCutflow(const SInputData& id)
{
  // get total number of events (should be in the first bin)
  const float total = Hist("h_cut_flow")->GetBinContent(1);

  // no entries -> print nothing
  if(total <= 0)
    return;

  // print header of cutflow table
  m_logger << ALWAYS << id.GetType() << "." << id.GetVersion() << SLogger::endmsg;
  m_logger << ALWAYS << "----------------------------------------------------------------------" << SLogger::endmsg;
  m_logger << ALWAYS << "Cut Flow of " << GetName() << " total: " << total << SLogger::endmsg;
  m_logger << ALWAYS << "----------------------------------------------------------------------" << SLogger::endmsg;
  m_logger << ALWAYS << "  #             Cut Name           raw       weighted    Eff   CumEff " << SLogger::endmsg;

  float lastpassed = total;
  float passed;
  unsigned int passed_raw;
  for(int i = 1; i <= Hist("h_cut_flow")->GetNbinsX(); ++i)
  {
    passed = Hist("h_cut_flow")->GetBinContent(i);
    passed_raw = (unsigned int)Hist("h_cut_flow_raw")->GetBinContent(i);
    m_logger << ALWAYS << std::right << std::setw(4) << i;
    m_logger << std::right << std::setw(22) << Hist("h_cut_flow")->GetXaxis()->GetBinLabel(i);
    m_logger << std::right << std::setw(13) << std::setprecision(0) << std::fixed << passed_raw;
    m_logger << std::right << std::setw(14) << std::setprecision(2) << std::fixed << passed;
    m_logger << std::right << std::setw(8) << std::setprecision(3) << std::fixed << passed / lastpassed;
    m_logger << std::right << std::setw(8) << std::setprecision(3) << std::fixed << passed / total;
    m_logger << SLogger::endmsg;
    lastpassed = passed;
  }
  m_logger << SLogger::endmsg;
}

#endif // AnalysisModul_Region_CXX
