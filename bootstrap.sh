makeargs=$1

# Build order

(cd SFrame/ && make $makeargs) &&
source setup.sh &&
(cd core/ && make $makeargs) &&
(cd ExternalTools/ && make) &&
source ExternalTools/RootCore/scripts/setup.sh &&
(cd CommonD3PDReaderObjects/ && make $makeargs) &&
(cd D3PDReaderObjects/ && make $makeargs) &&
(cd AnalysisUtilities/ && make $makeargs) &&
(cd TauCommon/ && make $makeargs) &&
(cd Tools/ && make) &&
(cd LLHTool/ && make $makeargs) &&
(cd OfflineLLH/ && make src/OfflineLLH_Dict.cxx && make $makeargs <<<.q) &&
mkdir input && mkdir output &&
(cd output &&
    cp ../GridSearch/GridSearch/res/default_methods.ini methods.ini &&
    python -c 'from GridSearch.common import IDVariables as I; I().write(open("variables.ini", "w"))'
    )

